# This module locates the zzip library.
# http://zziplib.sourceforge.net/
#
# This module sets:
# ZZIP_LIBRARY the name of the zzip library.
# ZZIP_INCLUDE_DIR where to find the zzip.h
# ZZIP_FOUND this is set to TRUE if all the above variables were set.

INCLUDE(FindPackageHandleStandardArgs)

FIND_PATH(ZZIP_INCLUDE_DIR zzip.h 
  DOC "The path the the directory that contains zzip.h"
)

#MESSAGE("ZZIP_INCLUDE_DIR is ${ZZIP_INCLUDE_DIR}")

FIND_LIBRARY(ZZIP_LIBRARY
  NAMES zzip
  PATH_SUFFIXES lib64 lib lib32
  DOC "The file that corresponds to the base zzip library."
)

#MESSAGE("ZZIP_LIBRARY is ${ZZIP_LIBRARY}")

FIND_PACKAGE_HANDLE_STANDARD_ARGS(ZZIP DEFAULT_MSG ZZIP_LIBRARY ZZIP_INCLUDE_DIR)

