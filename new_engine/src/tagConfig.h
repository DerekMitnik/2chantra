/**
 * tagCONFIG()
 * {
 * memset(this, 0, sizeof(*this));
 * joystick_sensivity = 1000;
 * }
 */
class tagConfig {

public:
	unsigned int scr_width;
	unsigned int scr_height;
	unsigned int scr_bpp;
	float aspect_ratio;
	float near_z;
	float far_z;
	unsigned int window_width;
	unsigned int windows_height;
	unsigned int full_width;
	unsigned int full_height;
	unsigned int joystick_sensivity;
	bool fullscreen;
	bool vert_sync;
	bool debug;
	bool show_fps;
	float backcolor_r;
	float backcolor_g;
	float backcolor_b;
	float volume;
	float volume_music;
	float volume_sound;
	/**
	 * #ifdef GAMETICK_FROM_CONFIG
	 * 	UINT gametick;
	 * #endif // GAMETICK_FROM_CONFIG
	 */
	unsigned int gametick;
	unsigned int cfg_keys[KEY_SETS_NUMBER][cakLastKey];
	unsigned int gui_nav_mode;
	unsigned int gui_nav_cycled;
	unsigned int shadows;
	LogLevel log_level;
	char *language;
	bool weather;
	char *nick;
	float color_r;
	float color_g;
	float color_b;

	void tagConfig();
};
