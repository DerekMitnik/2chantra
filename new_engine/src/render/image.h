#ifndef _IMAGE_H_
#define _IMAGE_H_

class CImageData
{
public:
	unsigned char* data;
	unsigned int height;
	unsigned int width;
	unsigned char bpp;
	bool transparent;

	CImageData();
	~CImageData();
	/**
	*	Checks for color key within whole image
	*/
	bool CheckImageAlpha();
	bool LoadFromFile(const char* filename);
	bool LoadFromBuffer(const char* buffer, size_t buffer_size);
};

class CGLImageData : public CImageData
{
public:
	bool			doCleanData;
	CGLImageData();
	~CGLImageData();
	bool			MakeTexture();
	bool			LoadTexture(const char *filename);
	bool			LoadTexture(const unsigned char *buffer, size_t buffer_size);
	bool			SaveScreenshot(const char *filename); // ���
	virtual GLuint	GetTexID();
protected:
	GLuint			TexID;
};

#endif // _IMAGE_H_
