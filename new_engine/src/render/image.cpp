#include "StdAfx.h"
#include <IL/il.h>
#include <SDL/SDL_opengl.h>
#include <assert.h>
#include "image.h"
#include "../misc.h"

static void logIlError()
{
	ILenum err = IL_NO_ERROR;
	while ((err = ilGetError()) != IL_NO_ERROR)
	{
		//Log(DEFAULT_LOG_NAME, logLevelError, "IL error: %s", iluErrorString(err));
		Log(DEFAULT_LOG_NAME, logLevelError, "IL error: %d", (int)err);
	}
}


CImageData::CImageData() : 
	data(NULL), 
	height(0), 
	width(0), 
	bpp(0),
	transparent(false)
{

}

CImageData::~CImageData()
{
	if (data != NULL)
		delete [] data;
}

bool  CImageData::CheckImageAlpha()
{
	if (data ==  NULL)
		return false;

	const size_t r_off = 0;
	const size_t g_off = 1;
	const size_t b_off = 2;
	const size_t a_off = 3;

	for (size_t i = 0; i < width * height * bpp; i += bpp)
	{
		unsigned char* pixel = data + i;

		// Transparency checking
		if (0 < pixel[a_off] && pixel[a_off] < 255)
			this->transparent = true;

		// Color key
		if (pixel[r_off] == 157 && pixel[g_off] == 236 && pixel[b_off] == 0)
			pixel[a_off] = 0;
	}
	return true;
}

bool CImageData::LoadFromFile( const char* filename )
{
	unsigned int ILID = ilGenImage();
	ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
	ilEnable(IL_ORIGIN_SET);
	ilBindImage(ILID);
	if (!ilLoadImage(filename))
		return false;

	width = ilGetInteger(IL_IMAGE_WIDTH);
	height = ilGetInteger(IL_IMAGE_HEIGHT);
	UINT y_pad = 0;
	UINT x_pad = 0;
	if ( !(( width & (width-1)) == 0 ) || !( (height & (height-1)) == 0 )  )
	{
		x_pad = width;
		width--;
		width |= width >> 1;
		width |= width >> 2;
		width |= width >> 4;
		width |= width >> 8;
		width |= width >> 16;
		width++;
		x_pad = width - x_pad;
		y_pad = height;
		height--;
		height |= height >> 1;
		height |= height >> 2;
		height |= height >> 4;
		height |= height >> 8;
		height |= height >> 16;
		height++;
		y_pad = height - y_pad;
	}
	data = new unsigned char[width * height * 4];
	memset( data, 0, width * height * 4 );
	if ( x_pad > 0 || y_pad > 0 )
	{
		for ( UINT y = 0; y < height - y_pad; y++ )
			ilCopyPixels( 0, y, 0, width-x_pad, 1, 1, IL_RGBA, IL_UNSIGNED_BYTE, data + (y+y_pad)*width*4 );
	}
	else ilCopyPixels(0, 0, 0, width, height, 1, IL_RGBA, IL_UNSIGNED_BYTE, data);
	
	//bpp = ilGetInteger(IL_IMAGE_FORMAT);
	//bpp = ilGetInteger(IL_IMAGE_BPP);

// 	int il_error ;
// 	if (!ilSaveImage((Filename + "_ilmod.png").c_str()))
// 	{
// 		il_error = ilGetError();
// 	}
	//bpp = ilGetInteger(IL_IMAGE_BITS_PER_PIXEL)/8;
	bpp = 4;
	ilDeleteImage(ILID);
	return true;
}

bool CImageData::LoadFromBuffer(const char* buffer, size_t buffer_size)
{
	unsigned int ILID = ilGenImage();
	ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
	ilEnable(IL_ORIGIN_SET);
	ilBindImage(ILID);
	if (!ilLoadL(IL_PNG, buffer, static_cast<ILuint>(buffer_size)))
	{
		logIlError();
		return false;
	}

	width = ilGetInteger(IL_IMAGE_WIDTH);
	height = ilGetInteger(IL_IMAGE_HEIGHT);
	UINT y_pad = 0;
	UINT x_pad = 0;
	if ( !(( width & (width-1)) == 0 ) || !( (height & (height-1)) == 0 )  )
	{
		x_pad = width;
		width--;
		width |= width >> 1;
		width |= width >> 2;
		width |= width >> 4;
		width |= width >> 8;
		width |= width >> 16;
		width++;
		x_pad = width - x_pad;
		y_pad = height;
		height--;
		height |= height >> 1;
		height |= height >> 2;
		height |= height >> 4;
		height |= height >> 8;
		height |= height >> 16;
		height++;
		y_pad = height - y_pad;
	}
	data = new unsigned char[width * height * 4];
	memset( data, 0, width * height * 4 );
	if ( x_pad > 0 || y_pad > 0 )
	{
		for ( UINT y = 0; y < height - y_pad; y++ )
			ilCopyPixels( 0, y, 0, width-x_pad, 1, 1, IL_RGBA, IL_UNSIGNED_BYTE, data + (y+y_pad)*width*4 );
	}
	else ilCopyPixels(0, 0, 0, width, height, 1, IL_RGBA, IL_UNSIGNED_BYTE, data);

	//bpp = ilGetInteger(IL_IMAGE_FORMAT);
	//bpp = ilGetInteger(IL_IMAGE_BPP);

// 	int il_error ;
// 	if (!ilSaveImage((Filename + "_ilmod.png").c_str()))
// 	{
// 		il_error = ilGetError();
// 	}
	//bpp = ilGetInteger(IL_IMAGE_BITS_PER_PIXEL)/8;
	bpp = 4;
	ilDeleteImage(ILID);
	return true;
}

CGLImageData::CGLImageData()
{
	TexID = width = height = bpp = 0;
	doCleanData = true;
	data = NULL;
}

CGLImageData::~CGLImageData()
{
// 	if(glIsTexture(TexID))
// 		glDeleteTextures(1, &TexID);
}

bool CGLImageData::MakeTexture()
{
	if (data == NULL)
		return false;
	if ((width&(width-1)) != 0)		//	��� �� ������ �������, ���� ������ ��� ������ �� �������� �������� ������.
		return false;				//	Ultimate - ��� ������������ NOT_POWER_OF_TWO ���������, ���� �� ��������;
	if ((height&(height-1)) != 0)	//	����� - ���������� � ������ ����� ����������� �����, �� ������� �����, �����
		return false;				//	������ � ������ ����� ���������� ��������� ������. �� ��� �����. � ��� TODO.
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glGenTextures(1, &TexID);
	glBindTexture(GL_TEXTURE_2D, TexID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	int MODE = (bpp == 3) ? GL_RGB : GL_RGBA;
	glTexImage2D(GL_TEXTURE_2D, 0, bpp, width, height, 0, MODE, GL_UNSIGNED_BYTE, data);
	return true;
}

bool CGLImageData::LoadTexture(const char *filename)
{
	if (!LoadFromFile(filename))
	{
		//INFO ������ ��� ������ ��� �� ��������� ����� ������, �� � �������� ���� ��� ������� �� ������
		Log(DEFAULT_LOG_NAME, logLevelInfo, "Can't load image %s",filename);
		return false;
	}
	if(!CheckImageAlpha())
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "Can't load texture.");
		return false;
	}
	if (!MakeTexture())
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "Can't load texture into video memory.");
		return false;
	}
	if (doCleanData)
	{
		DELETEARRAY(data);
	}
	return true;
}

bool CGLImageData::LoadTexture(const unsigned char* buffer, size_t buffer_size)
{
	if (!LoadFromBuffer((const char*)buffer,buffer_size))
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "Can't load image->");
		return false;
	}
	if(!CheckImageAlpha())
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "Can't load texture.");
		return false;
	}
	if (!MakeTexture())
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "Can't load texture into video memory.");
		return false;
	}
	if (doCleanData)
	{
		DELETEARRAY(data);
	}
	return true;
}

GLuint CGLImageData::GetTexID()
{
	if (TexID == 0)
		Log(DEFAULT_LOG_NAME, logLevelError, "GLImageData. Trying to access TexID but it is 0");
	return TexID;
}




//! Takes a screenshot of the current OpenGL window.
// ����� �� ilut_opengl.cpp
ILboolean GLScreenToIL()
{
	ILuint	ViewPort[4];

	glGetIntegerv(GL_VIEWPORT, (GLint*)ViewPort);

	if (!ilTexImage(ViewPort[2], ViewPort[3], 1, 3, IL_RGB, IL_UNSIGNED_BYTE, NULL))
		return IL_FALSE;  // Error already set.
	ilOriginFunc(IL_ORIGIN_LOWER_LEFT);

	ILubyte* data = ilGetData();

	glPixelStorei(GL_PACK_ALIGNMENT, 1);
	glReadPixels(ViewPort[0], ViewPort[1], ViewPort[2], ViewPort[3], GL_RGB, GL_UNSIGNED_BYTE, data);

	return IL_TRUE;
}


bool CGLImageData::SaveScreenshot(const char *filename)
{
	unsigned int ILID = ilGenImage();
	ilBindImage(ILID);

	// ����� �� ������ ��� � ILUT ���� ����� ������� ilutGLScreen, � �� ���� ��� GLScreenToIL.
	//if (ilutGLScreen())
	if ( GLScreenToIL() )
	{
#ifdef BMP_SCREENSHOTS
		ilSave(IL_BMP, filename);
#else
		ilSave(IL_PNG, filename);
#endif
	}
	ilDeleteImage(ILID);

	return true;
}
