#include "StdAfx.h"

#include "../misc.h"

#include "texture.h"
#include "image.h"

#include "../script/script.h"

#include "../resource_mgr.h"

extern char path_textures[MAX_PATH];

extern ResourceMgr<Texture> * textureMgr;
extern ResourceMgr<TexFrames> * texFramesMgr;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

bool GenTexCoords(Texture* tex, FrameInfo* f, lua_State* L)
{
	if (!tex && !f && !L)
		return false;

	// ����: fname env main
	UINT i = 0;
	float x1 = 0.0f, y1 = 0.0f, x2 = 0.0f, y2 = 0.0f, w = 0.0f, h = 0.0f, ox = 0.0f, oy = 0.0f;

	lua_pushnil(L);  // ����: fname env main nil
	FrameInfo* fi = NULL;
	while (lua_next(L, -2) != 0 && i < tex->frames->framesCount)
	{
		fi = f + i;

		// ����: fname env main key main[i]
		SCRIPT::GetFieldByName(L, "x", x1);
		SCRIPT::GetFieldByName(L, "y", y1);
		SCRIPT::GetFieldByName(L, "w", w);
		SCRIPT::GetFieldByName(L, "h", h);
		SCRIPT::GetFieldByName(L, "ox", ox);
		SCRIPT::GetFieldByName(L, "oy", oy);

		x2 = x1 + w;
		y2 = y1 + h;

		//if (fi->size)
		{
			fi->size.x = w;
			fi->size.y = h;
		}

		//if (fi->offset)
		{
			fi->offset.x = ox;
			fi->offset.y = oy;
		}

		float CWidth = (float)tex->width;
		float CHeight = (float)tex->height;
		float cx1 = x1 / CWidth;
		float cx2 = x2 / CWidth;
		float cy1 = 1 - y1 / CHeight;
		float cy2 = 1 - y2 / CHeight;

		if (fi->coord)
		{
			fi->coord[0].x = cx1; fi->coord[0].y = cy1;
			fi->coord[1].x = cx2; fi->coord[1].y = cy1;
			fi->coord[2].x = cx2; fi->coord[2].y = cy2;
			fi->coord[3].x = cx1; fi->coord[3].y = cy2;
		}

		if (fi->coordMir)
		{
			fi->coordMir[0].x = cx2; fi->coordMir[0].y = cy1;
			fi->coordMir[1].x = cx1; fi->coordMir[1].y = cy1;
			fi->coordMir[2].x = cx1; fi->coordMir[2].y = cy2;
			fi->coordMir[3].x = cx2; fi->coordMir[3].y = cy2;
		}


		lua_pop(L, 1);	// ����: fname env main key
		i++;
	}

	if(i != tex->frames->framesCount)
	{
		return false;
	}

	return true;
}

bool TexFrames::Load()
{
	if( buffer != NULL )
	{
		_buffer_size = buffer_size;
		if ( _buffer ) DELETEARRAY( _buffer );
		_buffer = new char[ _buffer_size ];
		memcpy( _buffer, buffer, _buffer_size );
		return true;
	}
	return false;
}

bool TexFrames::LoadFrames()
{
	if( _buffer == NULL)
		return false;
	
	extern lua_State* lua;
	assert(lua);

	STACK_CHECK_INIT(lua);

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Loading the frames table for texture %s", name.c_str());

	// ���� �������� ����� �������� � ���������� ���������, ����� �� �������� ���-���� ����������.
	// ��������� ��������� ��� �����: http://community.livejournal.com/ru_lua/402.html
	lua_newtable(lua);				// ����: env
	lua_newtable(lua);				// ����: env meta
	lua_getglobal(lua, "_G");			// ����: env meta _G
	lua_setfield(lua, -2, "__index");	// ����: env meta
	lua_setmetatable(lua, -2);		// ����: env

	if( luaL_loadbuffer(lua, (const char*)_buffer, _buffer_size, name.c_str()) )
	{
		// �����-�� ������ �������� �����
		const char* err = lua_tostring(lua, -1);	// ����: env err
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, "Whilst loading texture frames for '%s': %s", name.c_str(), err);
		lua_pop(lua, 2);	// ����:

		STACK_CHECK(lua);

		return false;
	}

	// ����: env loadfile
	lua_pushvalue(lua, -2);			// ����: env loadfile env
	lua_setfenv(lua, -2);				// ����: env loadfile

	if(lua_pcall(lua, 0, 0, 0))
	{
		// �����-�� ������ ���������� �����
		const char* err = lua_tostring(lua, -1);	// ����: env err
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, err );
		lua_pop(lua, 2);	// ����:

		STACK_CHECK(lua);

		return false;
	}
	else
	{
		LoadFramesFromTable(lua);
	}

	lua_pop(lua, 1);	// ����:

	STACK_CHECK(lua);

	return true;
}

bool TexFrames::LoadFramesFromTable(lua_State* L)
{
	STACK_CHECK_INIT(L);

	assert(lua_istable(L, -1));
	// ����: env
	SCRIPT::GetFieldByName(L, "count", this->framesCount);
	SCRIPT::GetFieldByName(L, "dist", this->dist);

	lua_getfield(L, -1, "main");	// ����: env main
	if (lua_istable(L, -1))
	{
		size_t count = lua_objlen(L, -1);
		if ( count == 0 )
		{
			return false;
		}
		this->framesCount = static_cast<UINT>(count);
		DELETEARRAY(this->frame);
		this->frame = new FrameInfo[this->framesCount]();


		if(!GenTexCoords(texture, this->frame, L))
		{
			Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "In %s: the amount of defined frames is not eqeal to the value of variable 'count'", name.c_str());
		}
	}
	else
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, "In %s: 'main' is not table", name.c_str());
	lua_pop(L, 1);	// ����: env

	DELETEARRAY(this->overlay);
	lua_getfield(L, -1, "overlay");	// ����: env overlay
	if (lua_istable(L, -1))
	{
		this->overlayCount = (UINT)lua_objlen(L, -1);
		if (overlayCount > 0)
		{
			this->overlay = new FrameInfo[this->overlayCount*this->framesCount];
			int i = 0;
			for(lua_pushnil(L);
				lua_next(L, -2) != 0 && i < (int)this->overlayCount;
				i++, lua_pop(L,1))
			{
				if(!GenTexCoords(texture, &this->overlay[i*this->framesCount], L))
				{
					Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "In %s: the amount of defined frames is not equeal to the value of the variable 'count'", name.c_str());
				}
			}
		}
		else
		{
			Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "In %s: table 'overlay' is empty", name.c_str());
		}
	}
	else
	{
		this->overlayCount = 0;
	}
	lua_pop(L, 1);	// ����: env

	STACK_CHECK(L);
	return true;
}

int TexFrames::DumpTable(lua_State* L)
{
	if (!texture)
		return 0;

	float cw = (float)texture->width;
	float ch = (float)texture->height;

	lua_createtable(L, 0, 3); // st: tbl
	lua_pushinteger(L, framesCount); lua_setfield(L, -2, "count");
	lua_createtable(L, framesCount, 0); // st: tbl main
	{
		for (size_t i = 0; i < framesCount; i++)
		{
			const FrameInfo& fi = frame[i];
			lua_createtable(L, 0, 4); // st: tbl main fi
			{
				lua_pushnumber(L, fi.coord[0].x * cw);           lua_setfield(L, -2, "x");
				lua_pushnumber(L, (1.0f - fi.coord[0].y) * ch);  lua_setfield(L, -2, "y");
				lua_pushnumber(L, fi.size.x);                    lua_setfield(L, -2, "w");
				lua_pushnumber(L, fi.size.y);                    lua_setfield(L, -2, "h");
			}
			lua_rawseti(L, -2, i + 1);
		}
	}
	lua_setfield(L, -2, "main");  // st: tbl

	if (overlayCount > 0)
	{
		lua_createtable(L, overlayCount, 0); // st: tbl overlay
		{
			for (size_t o = 0; o < overlayCount; o++)
			{
				lua_createtable(L, framesCount, 0); // st: tbl overlay overlay[o]
				{
					for (size_t i = 0; i < framesCount; i++)
					{
						const FrameInfo& fi = overlay[i];
						lua_createtable(L, 0, 6); // st: tbl overlay overlay[o] fi
						{
							lua_pushnumber(L, fi.coord[0].x * cw);           lua_setfield(L, -2, "x");
							lua_pushnumber(L, (1.0f - fi.coord[0].y) * ch);  lua_setfield(L, -2, "y");
							lua_pushnumber(L, fi.size.x);                    lua_setfield(L, -2, "w");
							lua_pushnumber(L, fi.size.y);                    lua_setfield(L, -2, "h");
							lua_pushnumber(L, fi.offset.x);                  lua_setfield(L, -2, "ox");
							lua_pushnumber(L, fi.offset.y);                  lua_setfield(L, -2, "oy");
						}
						lua_rawseti(L, -2, i + 1);
					}		
				}
				lua_rawseti(L, -2, o + 1);
			}			
		}
		lua_setfield(L, -2, "overlay");  // st: tbl
	}

	return 1;
}

void TexFrames::MakeDummyFrame(float width, float height)
{
	DELETEARRAY(frame);
	framesCount = 1;
	frame = new FrameInfo[1];

	frame[0].size.x = width; frame[0].size.y = height;

	frame[0].coord[0].x = 0.0f; frame[0].coord[0].y = 1.0f;
	frame[0].coord[1].x = 1.0f; frame[0].coord[1].y = 1.0f;
	frame[0].coord[2].x = 1.0f; frame[0].coord[2].y = 0.0f;
	frame[0].coord[3].x = 0.0f; frame[0].coord[3].y = 0.0f;

	frame[0].coordMir[0].x = 1.0f; frame[0].coordMir[0].y = 1.0f;
	frame[0].coordMir[1].x = 0.0f; frame[0].coordMir[1].y = 1.0f;
	frame[0].coordMir[2].x = 0.0f; frame[0].coordMir[2].y = 0.0f;
	frame[0].coordMir[3].x = 1.0f; frame[0].coordMir[3].y = 0.0f;

	DELETEARRAY(overlay);
	overlayCount = 0;
}

bool Texture::Load()
{
	if (!LoadImage())
		return false;

	this->frames = texFramesMgr->GetByName(name);
	if ( this->frames )
	{
		this->frames->texture = this;
		this->frames->ReserveUsage();
		if (!this->frames->LoadFrames())
		{
			Log(DEFAULT_LOG_NAME, logLevelError, "Error: unable to load frames for texture '%s'. Using standard.", this->name.c_str());
			this->frames->MakeDummyFrame((float)width, (float)height);
		}
	}

	return true;
}

bool Texture::LoadWithTexFrames( TexFrames* frames )
{
	if (!LoadImage())
		return false;

	this->frames = frames;
	if (frames)
	{
		frames->texture = this;
		frames->ReserveUsage();
	}

	return true;
}

bool Texture::Recover()
{
	CGLImageData ImageData;

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Recovering texture %s", this->name.c_str());
	if ( _buffer != NULL )
	{
		if ( ImageData.LoadTexture(_buffer,_buffer_size) )
		{
			this->tex = ImageData.GetTexID();
			return true;
		}
	}

	return false;
}

bool Texture::LoadImage()
{
	if( buffer == NULL )
		return false;

	_buffer_size = buffer_size;
	if ( _buffer ) DELETEARRAY( _buffer );
	_buffer = new unsigned char[ _buffer_size ];
	memcpy( _buffer, buffer, _buffer_size );

	CGLImageData ImageData;
	if (!ImageData.LoadTexture(_buffer,_buffer_size))
		return false;

	this->width = ImageData.width;
	this->height = ImageData.height;
	this->tex = ImageData.GetTexID();
	this->isTransparent = ImageData.transparent;

	return true;
}
