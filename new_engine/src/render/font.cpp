#include "StdAfx.h"

#include "font.h"
#include "../misc.h"
#include "../resource_mgr.h"
#include <map>
#include <fstream>
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

extern char path_fonts[MAX_PATH];
extern ResourceMgr<Texture> * textureMgr;
extern ResourceMgr<TexFont> * fontMgr;

//////////////////////////////////////////////////////////////////////////

std::map<string,string> mapNameToFont;

//////////////////////////////////////////////////////////////////////////

UCHAR *IFont::buffer = NULL;
size_t IFont::buf_len = 0;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////



// �������� ������ �� ��������
// in_name - ��� ����� �������� ������
// out_name - ����� ����� �������������� � ���� ��� ����� ������
bool LoadTextureFont(const char* in_name, const char* out_name)
{
	if(!in_name || !out_name)
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Incorrect in_name or out_name for loading windows font");
		return false;
	}

	if( mapNameToFont.count(out_name) > 0 )
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Already exist");
		return false;
	}

	mapNameToFont[out_name] = in_name;
	TexFont* font = FontByName(out_name);

	if (!font->LoadFrom())
	{
		DELETESINGLE(font);
		return false;
	}

	return true;
}

// ������������� ��� ������.
void RecoverFonts()
{
	fontMgr->RecoverAll();
}

void FreeFont(const char* font_name)
{
	fontMgr->Unload(mapNameToFont[font_name]);
	mapNameToFont.erase(font_name);
}

// free all fonts
void FreeFonts(void)
{
	fontMgr->UnloadAll();
	mapNameToFont.clear();
}

// get font by name
TexFont* FontByName(const char* font_name)
{
	if ( mapNameToFont.count(font_name) == 0 )
		return NULL;
	return fontMgr->GetByName( mapNameToFont[font_name] );
}

//////////////////////////////////////////////////////////////////////////


size_t IFont::PrintMultiline( const char *text, CAABB &area, int cursor, int scroll, bool fake )
{
	if (text == NULL)
		return 0;

	size_t br = 1;				// ����� �������� �������.
	size_t back = 0;			
	float x_size = 0.0f;				// ������� ����� ������
	float y_size = 0.0f;
	float current_shift = 0;	// ����� ����� ������ ������. �������, ������� �������� ��������
	float old_x = this->p.x;
	float old_y = this->p.y;
	size_t max_br;

	size_t left_text_len = 0;	// ����� ������ ��� ������

	ControlSeqParser::CSeqType seq_type = ControlSeqParser::ENone;
	size_t seq_shift = 0;

	size_t text_len = strlen( text );
	SetBufLen(text_len + 2);

	int line = 1;

	while ( br <= text_len )
	{
		for (x_size = 0; x_size < 2*area.W - current_shift && br <= text_len; br++)
		{	
			seq_type = ControlSeqParser::CheckControlSeq(&text[br-1], seq_shift);
			if (seq_type != ControlSeqParser::ENone)
				break;

			x_size += GetWidthAndDist(text[br-1]) * scale;
		}

		br += seq_shift;
		left_text_len = br-seq_shift-back-1;

		switch(seq_type)
		{
		case ControlSeqParser::EColor:
			{
				// �������� ����� �����
				if (br-back-1 > 0)
				{
					memcpy(buffer, text + back, left_text_len);
					buffer[left_text_len] = 0;
					Draw(buffer, cursor - static_cast<int>(back) );
				}

				this->p.x += x_size;
				current_shift += x_size;

				ControlSeqParser::GetColor(&text[br - seq_shift - 1], this->tClr);

				back = br-1;
				continue;
			}
			break;
		case ControlSeqParser::ENewLine: break;
		default:
			{
				max_br = br;
				while ( text[br-1] != 32 && br-1 > back && br != text_len+1  )	//�������� ��������� �� �������
					br--;
				if ( br-1 == back ) br = max_br; //�������� ���.

				left_text_len = br-seq_shift-back-1;
			}
		}

		seq_type = ControlSeqParser::ENone;

		memcpy(buffer, text + back, left_text_len);
		buffer[left_text_len] = 0;
		if ( line > scroll && !fake )
		{
			Draw(buffer, cursor - static_cast<int>(back) );
		}

		y_size = GetStringHeight((char*)buffer) * scale;

		if ( scroll < 0 )
		{
			float y1 = area.Top();
			float y2 = max( area.Bottom(), area.Top() + this->p.y - old_y + y_size  );
			area.p.y = (y1 + y2) / 2.0f;
			area.H = (y2 - y1) / 2.0f;
		}

		if ( line > scroll )
		{
			this->p.y += y_size * 1.5f;
		}
		this->p.x = old_x;
		current_shift = 0;
		line++;
		if ( scroll >= 0 && p.y - old_y + y_size > area.Bottom() - area.Top() )
		{
			this->p.y = old_y;
			this->p.x = old_x;
			return line;
		}

		back = br-1;
		while ( text[back] == 32 && back < text_len )	//�������� ������� � ������ ����� ��������
		{
			back++;
			br++;
		}

	}

	this->p.y = old_y;
	this->p.x = old_x;

	return line;
}

//-------------------------------------------//
//				TexFont stuff					 //
//-------------------------------------------//
TexFont::TexFont(string name, unsigned char* buffer, size_t buffer_size) : Resource(name, buffer, buffer_size)
{
	_buffer = NULL;
	_buffer_size = 0;
	FontImageName = NULL;
	dist = TEXFONT_DEFAULT_DISTANCE;
	tClr = RGBAf(1.0f, 1.0f, 1.0f, 1.0f);
	// TODO: ������ ������ bbox
}

TexFont::~TexFont()
{
	if (FontImageName != NULL)
		delete [] FontImageName;
	if (_buffer != NULL)
		delete [] _buffer;
}

bool TexFont::Load()
{
	if( Resource::buffer != NULL )
	{
		_buffer_size = buffer_size;
		if ( _buffer ) DELETEARRAY( _buffer );
		_buffer = new char[ _buffer_size ];
		memcpy( _buffer, Resource::buffer, _buffer_size );
		return true;
	}
	return false;
}
// �������� ������ �� �����
// �������������� ���� ������ filename
bool TexFont::LoadFrom()
{
#ifdef NO_BINARY_FONTS
	FontImageName = new char[strlen((char*)_buffer)+1];
	strcpy(FontImageName,(char*)_buffer);
	tex = textureMgr->GetByName(FontImageName);
	if (!tex)
	{
		DELETEARRAY(_buffer);
		return false;
	}
	DELETEARRAY(_buffer);
	for (size_t i = 0; i < 256; i++)
	{
		width[i] = static_cast<BYTE>(tex->frames->frame[i].size.x);
		height[i] = static_cast<BYTE>(tex->frames->frame[i].size.y);
	}
#else
	if( _buffer == NULL )
		return false;

	BYTE* box = (BYTE*)_buffer;
	while( *box != 0 ) box++;
	box++;
	FontImageName = new char[strlen((char*)_buffer)+1];
	strcpy(FontImageName,(char*)_buffer);
	tex = textureMgr->GetByName(FontImageName);
	if (!tex)
	{
		DELETEARRAY(_buffer);
		return false;
	}
	memcpy(bbox,box,sizeof(bbox));
	DELETEARRAY(_buffer);

	float y;

	// TODO: ���� ���� - �������. ������ ��� �������� ������� � ������� ������
	// ��������� ��� ���������, ����� ����������� �� ����� ��������.
	for (size_t i = 0; i < sizeof(bbox) / sizeof(bbox[0]); i++)
	{
		y = (float)tex->height - bbox[i].Max.y;
		bbox[i].Max.y = (float)tex->height - bbox[i].Min.y;
		bbox[i].Min.y = y;

		width[i] = (BYTE)(bbox[i].Max.x - bbox[i].Min.x);
		height[i] = (BYTE)(bbox[i].Max.y - bbox[i].Min.y);
	}
#endif //NO_BINARY_FONTS

	return true;
}

bool TexFont::Recover()
{
	return false;
}

bool TexFont::SaveToFile(char* file_name)
{
	if ( file_name == NULL || strlen(file_name) == 0 )
		return false;
	ofstream file;
	file.open(file_name,fstream::out|fstream::binary);
	file.write(FontImageName, static_cast<std::streamsize>(strlen(FontImageName)));
	file.put((byte)0x00);
	file.write((char*)bbox, sizeof(bbox));
	file.close();
	return true;
}

void TexFont::Draw(const UCHAR *text)
{
	unsigned char c;
	float x = p.x;
	float y = p.y;
	float draw_x;
	float draw_y;
	for (const UCHAR* t = text; *t; t++)
	{
		draw_x = floor( x + 0.5f );
		draw_y = floor( y + 0.5f );
		if (*t < 32)
			continue;
		c = *t - 32;
		if ( outline.a > 0 )
		{
#ifdef NO_BINARY_FONTS
			const CAABB aabb = CAABB( 0, 0, width[c] * scale, height[c] * scale );
			RenderFrame( draw_x-1.0f, draw_y, z, aabb, tex->frames->frame+c, tex, outline, false, rsmStretch, 0, bmPostEffect );
			RenderFrame( draw_x+1.0f, draw_y, z, aabb, tex->frames->frame+c, tex, outline, false, rsmStretch, 0, bmPostEffect );
			RenderFrame( draw_x, draw_y-1.0f, z, aabb, tex->frames->frame+c, tex, outline, false, rsmStretch, 0, bmPostEffect );
			RenderFrame( draw_x, draw_y+1.0f, z, aabb, tex->frames->frame+c, tex, outline, false, rsmStretch, 0, bmPostEffect );
#else
			RenderFrameSimple(draw_x-1.0f, draw_y, z, bbox[c].Min.x, bbox[c].Min.y, bbox[c].Max.x, bbox[c].Max.y, tex, false, outline, scale, bmPostEffect, ratFonts);
			RenderFrameSimple(draw_x+1.0f, draw_y, z, bbox[c].Min.x, bbox[c].Min.y, bbox[c].Max.x, bbox[c].Max.y, tex, false, outline, scale, bmPostEffect, ratFonts);
			RenderFrameSimple(draw_x, draw_y-1.0f, z, bbox[c].Min.x, bbox[c].Min.y, bbox[c].Max.x, bbox[c].Max.y, tex, false, outline, scale, bmPostEffect, ratFonts);
			RenderFrameSimple(draw_x, draw_y+1.0f, z, bbox[c].Min.x, bbox[c].Min.y, bbox[c].Max.x, bbox[c].Max.y, tex, false, outline, scale, bmPostEffect, ratFonts);
#endif //NO_BINARY_FONTS
		}
#ifdef NO_BINARY_FONTS
		const CAABB aabb = CAABB( 0, 0, width[c] * scale, height[c] * scale );
		RenderFrame( draw_x, draw_y, z, aabb, tex->frames->frame+c, tex, tClr, false, rsmStretch, 0, bmPostEffect );
		x += (width[c] + tex->frames->dist) * scale;
#else
		RenderFrameSimple(draw_x, draw_y, z, bbox[c].Min.x, bbox[c].Min.y, bbox[c].Max.x, bbox[c].Max.y, tex, false, tClr, scale, bmPostEffect, ratFonts);
		x += (width[c] + dist) * scale;
#endif //NO_BINARY_FONTS
	}
}

extern UINT current_time;

void TexFont::Draw(const UCHAR *text, int cursor)
{
	unsigned char c;
	float x = p.x;
	float y = p.y;
	int pos = 0;
	for (const UCHAR* t = text; *t; t++)
	{
		if (*t < 32)
			continue;
		c = *t - 32;
		if ( pos == cursor )
		{
			//TODO:���� ���-�� ������� ��� ��.
#ifdef NO_BINARY_FONTS
			CAABB aabb = CAABB( 0, 0, width['|'-32] * scale, height['|'-32] * scale );
			if ( current_time % 1000 > 500 ) RenderFrame( x, y, z, aabb, tex->frames->frame+'|'-32, tex, tClr, false, rsmStretch, 0, bmPostEffect );
			x += (width['|'-32] + tex->frames->dist)*scale;
			aabb = CAABB( 0, 0, width[c] * scale, height[c] * scale );
			RenderFrame( x, y, z, aabb, tex->frames->frame+c, tex, tClr, false, rsmStretch, 0, bmPostEffect );
			x += (width[c] + tex->frames->dist)*scale;
#else
			if ( current_time % 1000 > 500 ) RenderFrameSimple(x, y, z, bbox['|'-32].Min.x, bbox['|'-32].Min.y, bbox['|'-32].Max.x, bbox['|'-32].Max.y, tex, false, tClr, scale);
			x += (width['|'-32] + dist)*scale;
			RenderFrameSimple(x, y, z, bbox[c].Min.x, bbox[c].Min.y, bbox[c].Max.x, bbox[c].Max.y, tex, false, tClr, scale);
			x += (width[c] + dist)*scale;
#endif //NO_BINARY_FONTS
		}
		else
		{
#ifdef NO_BINARY_FONTS
			const CAABB aabb = CAABB( 0, 0, width[c] * scale, height[c] * scale );
			RenderFrame( x, y, z, aabb, tex->frames->frame+c, tex, tClr, false, rsmStretch, 0, bmPostEffect );
#else
			RenderFrameSimple(x, y, z, bbox[c].Min.x, bbox[c].Min.y, bbox[c].Max.x, bbox[c].Max.y, tex, false, tClr, scale);
#endif //NO_BINARY_FONTS
			x += (width[c] + dist)*scale;
		}
		pos++;
	}
	if ( pos == cursor && current_time % 1000 > 500 )
	{
#ifdef NO_BINARY_FONTS
		CAABB aabb = CAABB( 0, 0, width['|'-32] * scale, height['|'-32] * scale );
		RenderFrame( x, y, z, aabb, tex->frames->frame+'|'-32, tex, tClr, false, rsmStretch, 0, bmPostEffect );
#else
		RenderFrameSimple(x, y, z, bbox['|'-32].Min.x, bbox['|'-32].Min.y, bbox['|'-32].Max.x, bbox['|'-32].Max.y, tex, false, tClr, scale);
#endif //NO_BINARY_FONTS
	}
}

void TexFont::Print( const char *text, ... )
{
	if (text == NULL)
		return;

	//static UCHAR	*temp;//[FONT_STRING_ADD_LENGTH];
	//temp = new UCHAR [strlen(text)+FONT_STRING_ADD_LENGTH];
	SetBufLen(strlen(text) + FONT_STRING_ADD_LENGTH);
	va_list	ap;
	va_start(ap, text);
	vsnprintf((char*)IFont::buffer, buf_len, text, ap);
	va_end(ap);
	Draw(IFont::buffer);
	//delete [] temp;
}




UINT TexFont::GetStringWidth(const char *text)
{
	if (text == NULL)
		return 0;
	UINT r = 0;
	for (; *text; text++)
		r += (UINT)width[(unsigned char)(*text - 32)] + dist;
	return r;
}

UINT TexFont::GetStringWidth(const char *text, size_t t1, size_t t2)
{
	if (!text || t1 > t2)
		return 0;
	UINT r = 0;
	text = text + t1;
	for (;*text && t1 <= t2; text++, t1++)
		r += (UINT)width[(unsigned char)(*text - 32)] + dist;
	return r;
}

UINT TexFont::GetStringHeight(const char *text)
{
	if (text == NULL)
		return 0;
	UINT r = height[32];
	for (; *text; text++)
		r = max((UINT)height[(unsigned char)(*text - 32)], r);
	return r;
}

UINT TexFont::GetStringHeight(const char *text, size_t t1, size_t t2)
{
	if (!text || t1 > t2)
		return 0;
	UINT r = height[32];
	text = text + t1;
	for (;*text && t1 <= t2; text++, t1++)
		r = max((UINT)height[(unsigned char)(*text - 32)], r);
	return r;
}

UINT TexFont::GetWidth(char c)
{
	return (UINT)width[(unsigned char)(c-32)];
}
UINT TexFont::GetWidthAndDist(char c)
{
	return (UINT)width[(unsigned char)(c-32)] + dist;
}

UINT TexFont::GetHeight(char c)
{
	return (UINT)height[(unsigned char)(c-32)];
}

// � ���� ��������� ��������� ��� ��� ��������� ����������� ������������������� ��� ��������� �����.
// ��������� ������������ � ������� ��������.
namespace ControlSeqParser
{
	// ����� ��������:
	//		[0]
	//	'/'  |
	//		[1]
	//	'c'	/ \ 'n'
	//	  [2] [3]
	//
	const int states_count = 5;				// ��������� ���������. ��������� states_count-1 ������ - ������.
	const size_t classes_count = 4;			// ���������� ������� ��������
	const CSeqType seqs[states_count] = {ENone, ENone, EColor, ENewLine, ENone};	// ����������� ������� ��������� ����� ����������� �������������������
	const bool end_state[states_count] = {false, false, true, true, false};		// ��������������� ��������
	const char classes[classes_count] = {'/', 'c', 'n', '\0'};					// ������
	const int table[states_count][classes_count] = {	// ������� ��������� ��������. ������ - ������� ���������, ������� - ������, � ������� - ����� ���������� ���������.
		{1, 4, 4, 4},
		{4, 2, 3, 4},
		{4, 4, 4, 4},
		{4, 4, 4, 4},
		{4, 4, 4, 4} };
	
	const size_t seq_len[] = {0, 8, 2};		// ����� ����������� ������������������� ��� ������ � ������. ������� �������������� CSeqType.

	// ���������� ����� ������ �������
	size_t GetClass(char c)
	{
		for (size_t i = 0; i < classes_count; i++)
		{
			if (classes[i] == c)
				return i;
		}
		return classes_count-1;
	}

	// ��� ��������, �������� ������������������ � ���������, ���� ��� ������� �� ����� �� ����� ������
	// � shift ���������� ����� ���������� ����� ������������������ �������.
	CSeqType CheckControlSeq(const char* text, size_t& shift)
	{
		shift = 0;
		int s = 0;
		size_t i;
		for (i = 0; text[i] != 0 && !end_state[s]; i++)
		{
			s = table[s][GetClass(text[i])];
			if (s == states_count - 1)
				break;
		}

		CSeqType t = seqs[s];
		if (t == ENone)
		{
			return ENone;
		}

		for (; text[i] != 0 && i < seq_len[t]; i++);

		if (i == seq_len[t])
		{
			shift = i;
			return t;
		}
		
		return ENone;
	}

	// ������ ������������������ ������� ����� � ������� � color �������������� ����
	void GetColor(const char* text, RGBAf& color)
	{
		assert(text[0] == '/' && text[1] == 'c');

		char number[3] = "\0\0";
		size_t shift = 2;
		for (int i = 0; i < 3; i++, shift+=2)
		{
			number[0] = text[shift];
			number[1] = text[shift+1];
			color[i] = strtoul(number, NULL, 16) / 255.0f;
		}
	}
}
