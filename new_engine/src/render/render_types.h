#ifndef __RENDER_TYPES_H_
#define __RENDER_TYPES_H_

struct  vertex2f_t{
	float x;
	float y;
	vertex2f_t():x(0),y(0){}
};

struct  vertex3f_t{
	float x;
	float y;
	float z;
	vertex3f_t():x(0),y(0),z(0){}
};

//struct  vertex3d_t{
//	double x;
//	double y;
//	double z;
//	vertex3d_t():x(0),y(0),z(0){}
//};


struct coord2f_t{
	float x;
	float y;
	coord2f_t():x(0),y(0){}
	coord2f_t(float x, float y):x(x),y(y){}
};

enum RenderSpriteMethod { rsmStandart, rsmStretch, rsmCrop, rsmRepeatX, rsmRepeatY, rsmRepeatXY };
// ���� ���������: �������, �����, ����������� ������
enum RenderArrayTypes { ratFirst = 0, 
				ratSprites = 0, 
				ratFilled = 1,
				ratLines = 2,  
				ratTriangles = 3,
				ratFonts = 4,
				ratLast = 5 };
// ������ ���������. bmLast ����� ��������� �� �������������� ����� ������.
enum BlendingMode { bmFirst = 0, 
			bmNone = 0, 
			bmSrcA_OneMinusSrcA = 1, 
			bmBright = 2,
			bmSrcA_One = 3, 
			bmPostEffect = 4,
		    bmLast = 5 };

#endif // __RENDER_TYPES_H_
