// This file contains the implementation details of type-to-lua binding system.
// All type specific declarations are in userdata_binding.hpp

#include "StdAfx.h"

#include <typeinfo>

#include "typelist.hpp"
#include "CUData.hpp"
#include "lua_pusher.hpp"

#include "udata.hpp"


// The block 1 declares the typelist.
#undef INCLUDE_BLOCK
#define INCLUDE_BLOCK 1
#include "userdata_binding.hpp"

// Array of references to userdata's metatables for each registerd class
// Those metatables are used to check types and to declare all methods of userdata
LuaRegRef udata_refs[TL::Length<ClassesList>::value] = { };
#define UD_TYPE_ID(T)  TL::IndexOf<ClassesList, T>::value
#define UD_META_REF(T) udata_refs[UD_TYPE_ID(T)]

LuaRegRef vector2_ref;
LuaRegRef rgbaf_ref;

// Function template for userdata creation.
// This function must be called from implementations of CUDataUser::createUData
template <typename T>
CUData* CreateUData(T& user)
{
	extern lua_State* lua;

	CUData* ud = static_cast<CUData*>(lua_newuserdata(lua, sizeof(CUData)));
	if (!ud)
		return NULL;
	// st: ud

	CUDataUser* p = static_cast<CUDataUser*>(&user);

	ud = new(ud) CUData(p);

	SCRIPT::GetFromRegistry(lua, UD_META_REF(T));   // st: ud meta
	lua_setmetatable(lua, -2);                      // st: ud

	ud->setRegRef(SCRIPT::AddToRegistry());       // st: 
	return ud;
}

// This macro is used to explicitly declare CreateUData for TYPE and 
// to implement CUDataUser::createUData for this TYPE.
// This also helps to not forget the createUData declaration in TYPE.
#define UDATA_CREATOR_FUNC_DECL(TYPE)                                  \
	template CUData* CreateUData<TYPE>(TYPE& user);                    \
	CUData* TYPE::createUData()                                        \
	{                                                                  \
		return CreateUData(*this);                                     \
	}

// The block 2 uses UDATA_CREATOR_FUNC_DECL macro to declare needed functions.
#undef INCLUDE_BLOCK
#define INCLUDE_BLOCK 2 
#include "userdata_binding.hpp"

// Used for check userdata's metatable.
// Will throw lua error if throw_error == true (default). Otherwise, will return NULL.
template <typename T>
CUData* check_userdata(lua_State* L, int ud, bool throw_error /*= true*/)
{
	CUData *p = reinterpret_cast<CUData*>(lua_touserdata(L, ud));
	if (p != NULL) {  /* value is a userdata? */
		if (lua_getmetatable(L, ud)) {  /* does it have a metatable? */
			SCRIPT::GetFromRegistry(L, UD_META_REF(T));  /* get correct metatable */
			if (lua_rawequal(L, -1, -2)) {  /* does it have the correct mt? */
				lua_pop(L, 2);  /* remove both metatables */
				return p;
			}
		}
	}
	if (throw_error)
		luaL_argerror(L, ud, "got wrong userdata");  /* else error */
	// luaL_argerror will not return.
	return NULL;
}

// Special standard methamethods. It is wise to use them for all methatables (at least, on_gc).
// Invoked on gc event. Used to make the user object forget the CUData existance.
template <typename T>
int on_gc(lua_State* L)
{
	CUData* ud = check_userdata<T>(L, 1);
	ud->onGC();
	return 0;
}

template <typename T>
int on_tostring(lua_State* L)
{
	CUData* ud = check_userdata<T>(L, 1);
	lua_pushfstring(L, "userdata: %p, %s", ud, typeid(T).name());
	return 1;
}

template <typename T>
int object_present(lua_State* L)
{
	CUData* ud = check_userdata<T>(L, 1);
	if (!ud->getUser()) lua_pushboolean(L, false);
	else lua_pushboolean(L, true);
	return 1;
}

// Vector2 metatable methods
int vector2_new(lua_State* L)
{
	float x = 0.0f;
	float y = 0.0f;
	if ( lua_isnumber( L, 1 ) )
	{
		x = static_cast<float>(lua_tonumber(L, 1));
		if ( lua_isnumber( L, 2 ) )
		{
			y = static_cast<float>(lua_tonumber(L, 2));
		}
	}
	else if ( lua_istable( L, 1 ) )
	{
		lua_pushstring( L, "x" );
		lua_rawget( L, 1 );
		if ( lua_isnumber(L, -1) )
		{
			x = static_cast<float>(lua_tonumber(L, -1));
			lua_pushstring( L, "y" );
			lua_rawget( L, 1 );
			if ( lua_isnumber(L, -1) )
			{
				y = static_cast<float>(lua_tonumber(L, -1));
			}
		}
		else
		{
			lua_pop( L, 1 );
			lua_rawgeti( L, 1, 1 );
			if ( lua_isnumber( L, -1 ) )
			x = static_cast<float>(lua_tonumber(L, -1));
			lua_rawgeti( L, 1, 2 );
			if ( lua_isnumber(L, -1) )
			{
				y = static_cast<float>(lua_tonumber(L, -1));
			}
		}
	}

	lua_createtable(L, 0, 2);
	pushToLua(L, x); lua_setfield(L, -2, "x");
	pushToLua(L, y); lua_setfield(L, -2, "y");
	SCRIPT::GetFromRegistry(L, vector2_ref);
	lua_setmetatable(L, -2);
	return 1;
}

int vector2_index(lua_State* L)
{
	if ( lua_isnumber( L, -1 ) )
	{
		size_t i = static_cast<size_t>( lua_tointeger(L, -1) );
		if ( i == 1 )
		{
			lua_pop( L, 1 );
			lua_pushstring( L, "x" );
		}
		else if ( i == 2 )
		{
			lua_pop( L, 1 );
			lua_pushstring( L, "y" );
		}
	}
	else if ( lua_isstring( L, -1 ) )
	{
		SCRIPT::GetFromRegistry(L, vector2_ref);
		lua_pushvalue( L, -2 );
		lua_rawget( L, -2 );
		if ( !lua_isnil( L, -1 ) )
		{
			return 1;
		}
		lua_pop( L, 2 );
	}
	lua_rawget( L, -2 );
	return 1;
}

int vector2_tostring(lua_State* L)
{
	float x = 0.0f;
	float y = 0.0f;
	lua_pushstring( L, "x" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		x = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "y" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		y = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	
	lua_pushfstring(L, "(%f, %f)", x, y ); 
	return 1;
}

int vector2_add(lua_State* L)
{
	if ( !lua_istable( L, -1 ) )
	{
		return 0;
	}
	float x = 0.0f;
	float y = 0.0f;
	lua_pushstring( L, "x" );
	lua_rawget( L, -3 );
	if ( lua_isnumber( L, -1 ) )
	{
		x = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "y" );
	lua_rawget( L, -3 );
	if ( lua_isnumber( L, -1 ) )
	{
		y = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "x" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		x += static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "y" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		y += static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );

	lua_createtable(L, 0, 2);
	pushToLua(L, x); lua_setfield(L, -2, "x");
	pushToLua(L, y); lua_setfield(L, -2, "y");
	SCRIPT::GetFromRegistry(L, vector2_ref);
	lua_setmetatable(L, -2);
	return 1;
}

int vector2_sub(lua_State* L)
{
	if ( !lua_istable( L, -1 ) )
	{
		return 0;
	}
	float x = 0.0f;
	float y = 0.0f;
	lua_pushstring( L, "x" );
	lua_rawget( L, -3 );
	if ( lua_isnumber( L, -1 ) )
	{
		x = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "y" );
	lua_rawget( L, -3 );
	if ( lua_isnumber( L, -1 ) )
	{
		y = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "x" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		x -= static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "y" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		y -= static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );

	lua_createtable(L, 0, 2);
	pushToLua(L, x); lua_setfield(L, -2, "x");
	pushToLua(L, y); lua_setfield(L, -2, "y");
	SCRIPT::GetFromRegistry(L, vector2_ref);
	lua_setmetatable(L, -2);
	return 1;
}

int vector2_unm(lua_State* L)
{
	float x = 0.0f;
	float y = 0.0f;
	lua_pushstring( L, "x" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		x = -static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "y" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		y = -static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );

	lua_createtable(L, 0, 2);
	pushToLua(L, x); lua_setfield(L, -2, "x");
	pushToLua(L, y); lua_setfield(L, -2, "y");
	SCRIPT::GetFromRegistry(L, vector2_ref);
	lua_setmetatable(L, -2);
	return 1;
}

int vector2_mul(lua_State* L)
{
	if ( !lua_isnumber( L, -1 ) )
	{
		return 0;
	}
	float x = 0.0f;
	float y = 0.0f;
	float s = static_cast<float>(lua_tonumber( L, -1 ) );
	lua_pushstring( L, "x" );
	lua_rawget( L, -3 );
	if ( lua_isnumber( L, -1 ) )
	{
		x = s * static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "y" );
	lua_rawget( L, -3 );
	if ( lua_isnumber( L, -1 ) )
	{
		y = s * static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );

	lua_createtable(L, 0, 2);
	pushToLua(L, x); lua_setfield(L, -2, "x");
	pushToLua(L, y); lua_setfield(L, -2, "y");
	SCRIPT::GetFromRegistry(L, vector2_ref);
	lua_setmetatable(L, -2);
	return 1;
}

int vector2_div(lua_State* L)
{
	if ( !lua_isnumber( L, -1 ) )
	{
		return 0;
	}
	float x = 0.0f;
	float y = 0.0f;
	float s = static_cast<float>(lua_tonumber( L, -1 ) );
	if ( s == 0.0f )
	{
		return 0;
	}
	lua_pushstring( L, "x" );
	lua_rawget( L, -3 );
	if ( lua_isnumber( L, -1 ) )
	{
		x = static_cast<float>(lua_tonumber(L, -1)) / s;
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "y" );
	lua_rawget( L, -3 );
	if ( lua_isnumber( L, -1 ) )
	{
		y = static_cast<float>(lua_tonumber(L, -1)) / s;
	}
	lua_pop( L, 1 );

	lua_createtable(L, 0, 2);
	pushToLua(L, x); lua_setfield(L, -2, "x");
	pushToLua(L, y); lua_setfield(L, -2, "y");
	SCRIPT::GetFromRegistry(L, vector2_ref);
	lua_setmetatable(L, -2);
	return 1;
}

int vector2_eq(lua_State* L)
{
	if ( !lua_istable( L, -1 ) )
	{
		lua_pushboolean( L, false );
		return 1;
	}
	float x = 0.0f;
	float y = 0.0f;
	bool equal = true;
	lua_pushstring( L, "x" );
	lua_rawget( L, -3 );
	if ( lua_isnumber( L, -1 ) )
	{
		x = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "y" );
	lua_rawget( L, -3 );
	if ( lua_isnumber( L, -1 ) )
	{
		y = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "x" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		if ( fabs( x - static_cast<float>(lua_tonumber(L, -1)) ) > std::numeric_limits<float>::epsilon() )
		{
			equal = false;
		}
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "y" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		if ( fabs( y - static_cast<float>(lua_tonumber(L, -1)) ) > std::numeric_limits<float>::epsilon() )
		{
			equal = false;
		}
	}
	lua_pop( L, 1 );

	lua_pushboolean( L, equal );
	return 1;
}

int vector2_len(lua_State* L)
{
	float x = 0.0f;
	float y = 0.0f;
	lua_pushstring( L, "x" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		x = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "y" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		y = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );

	lua_pushnumber( L, sqrt( pow( x, 2 ) + pow( y, 2 ) ) );
	return 1;
}

int vector2_angle(lua_State* L)
{
	float x = 0.0f;
	float y = 0.0f;
	lua_pushstring( L, "x" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		x = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "y" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		y = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );

	lua_pushnumber( L, atan2( y, x ) );
	return 1;
}

int vector2_normalize(lua_State* L)
{
	float x = 0.0f;
	float y = 0.0f;
	lua_pushstring( L, "x" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		x = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "y" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		y = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );

	Vector2 v = Vector2( x, y );
	v.Normalize();

	lua_createtable(L, 0, 2);
	pushToLua(L, v.x); lua_setfield(L, -2, "x");
	pushToLua(L, v.y); lua_setfield(L, -2, "y");
	SCRIPT::GetFromRegistry(L, vector2_ref);
	lua_setmetatable(L, -2);
	return 1;
}

int vector2_perp(lua_State* L)
{
	float x = 0.0f;
	float y = 0.0f;
	lua_pushstring( L, "x" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		x = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );
	lua_pushstring( L, "y" );
	lua_rawget( L, -2 );
	if ( lua_isnumber( L, -1 ) )
	{
		y = static_cast<float>(lua_tonumber(L, -1));
	}
	lua_pop( L, 1 );

	Vector2 v = Vector2( x, y ).GetPerpendicular();

	lua_createtable(L, 0, 2);
	pushToLua(L, v.x); lua_setfield(L, -2, "x");
	pushToLua(L, v.y); lua_setfield(L, -2, "y");
	SCRIPT::GetFromRegistry(L, vector2_ref);
	lua_setmetatable(L, -2);
	return 1;
}

// RGBAf metatable methods
int rgbaf_new(lua_State* L)
{
	float r = 0.0f;
	float g = 0.0f;
	float b = 0.0f;
	float a = 1.0f;
	bool not_byte = false;
	if ( lua_isboolean( L, 5 ) && ( lua_toboolean( L, 5 ) != 0 ) )
	{
		not_byte = true;
	}
	if ( lua_isnumber( L, 1 ) )
	{
		r = static_cast<float>(lua_tonumber(L, 1));
		if ( lua_isnumber( L, 2 ) )
		{
			g = static_cast<float>(lua_tonumber(L, 2));
		}
		if ( lua_isnumber( L, 3 ) )
		{
			b = static_cast<float>(lua_tonumber(L, 2));
		}
		if ( lua_isnumber( L, 4 ) )
		{
			a = static_cast<float>(lua_tonumber(L, 2));
		}
	}
	else if ( lua_istable( L, 1 ) )
	{
		lua_pushstring( L, "r" );
		lua_rawget( L, 1 );
		if ( lua_isnumber(L, -1) )
		{
			r = static_cast<float>(lua_tonumber(L, -1));
			lua_pushstring( L, "g" );
			lua_rawget( L, 1 );
			if ( lua_isnumber(L, -1) )
			{
				g = static_cast<float>(lua_tonumber(L, -1));
			}
			lua_pushstring( L, "b" );
			lua_rawget( L, 1 );
			if ( lua_isnumber(L, -1) )
			{
				b = static_cast<float>(lua_tonumber(L, -1));
			}
			lua_pushstring( L, "a" );
			lua_rawget( L, 1 );
			if ( lua_isnumber(L, -1) )
			{
				a = static_cast<float>(lua_tonumber(L, -1));
			}
		}
		else
		{
			lua_pop( L, 1 );
			lua_rawgeti( L, 1, 1 );
			if ( lua_isnumber( L, -1 ) )
			r = static_cast<float>(lua_tonumber(L, -1));
			lua_rawgeti( L, 1, 2 );
			if ( lua_isnumber(L, -1) )
			{
				g = static_cast<float>(lua_tonumber(L, -1));
			}
			lua_rawgeti( L, 1, 3 );
			if ( lua_isnumber(L, -1) )
			{
				b = static_cast<float>(lua_tonumber(L, -1));
			}
			lua_rawgeti( L, 1, 4 );
			if ( lua_isnumber(L, -1) )
			{
				a = static_cast<float>(lua_tonumber(L, -1));
			}
		}
	}
	if ( r > 1.0f && !not_byte )
	{
		r = r / 255.0f;
	}
	if ( g > 1.0f && !not_byte )
	{
		g = g / 255.0f;
	}
	if ( b > 1.0f && !not_byte )
	{
		b = b / 255.0f;
	}
	if ( a > 1.0f && !not_byte )
	{
		a = a / 255.0f;
	}

	lua_createtable(L, 3, 0);
	pushToLua(L, r); lua_rawseti(L, -2, 1);
	pushToLua(L, g); lua_rawseti(L, -2, 2);
	pushToLua(L, b); lua_rawseti(L, -2, 3);
	pushToLua(L, a); lua_rawseti(L, -2, 4);
	SCRIPT::GetFromRegistry(L, rgbaf_ref);
	lua_setmetatable(L, -2);
	return 1;
}

int rgbaf_tostring(lua_State* L)
{
	uint8_t r = 0;
	uint8_t g = 0;
	uint8_t b = 0;
	uint8_t a = 255;
	if ( lua_istable( L, 1 ) )
	{
		lua_rawgeti( L, 1, 1 );
		if ( lua_isnumber( L, -1 ) )
		{
			r = static_cast<uint8_t>( floor(static_cast<float>(lua_tonumber(L, -1))*255.0f) );
		}
		lua_rawgeti( L, 1, 2 );
		if ( lua_isnumber( L, -1 ) )
		{
			g = static_cast<uint8_t>( floor(static_cast<float>(lua_tonumber(L, -1))*255.0f) );
		}
		lua_rawgeti( L, 1, 3 );
		if ( lua_isnumber( L, -1 ) )
		{
			b = static_cast<uint8_t>( floor(static_cast<float>(lua_tonumber(L, -1))*255.0f) );
		}
		lua_rawgeti( L, 1, 4 );
		if ( lua_isnumber( L, -1 ) )
		{
			a = static_cast<uint8_t>( floor(static_cast<float>(lua_tonumber(L, -1))*255.0f) );
		}
	}
	char* result = new char[ 27 ];
	if ( a == 255 )
	{
		sprintf( result, "color: (%i, %i, %i)", r, g, b );
	}
	else
	{
		sprintf( result, "color: (%i, %i, %i, %i)", r, g, b, a );
	}
	lua_pushstring( L, result );
	DELETEARRAY( result );
	return 1;
}

int rgbaf_index(lua_State* L)
{
	if ( lua_isstring( L, 2 ) )
	{
		const char* str = lua_tostring( L, 2 );
		if ( strcmp( str, "r" ) == 0 )
		{
			lua_pop( L, 1 );
			lua_pushinteger( L, 1 );
		}
		else if ( strcmp( str, "g" ) == 0 )
		{
			lua_pop( L, 1 );
			lua_pushinteger( L, 2 );
		}
		else if ( strcmp( str, "b" ) == 0 )
		{
			lua_pop( L, 1 );
			lua_pushinteger( L, 3 );
		}
		else if ( strcmp( str, "a" ) == 0 )
		{
			lua_pop( L, 1 );
			lua_pushinteger( L, 4 );
		}
	}
	lua_rawget( L, -2 );
	return 1;
}

//////////////////////////////////////////////////////////////////////////
// Macros for declaration of getter method
#define GETTER_METHOD_DECL(FIELD)                                    \
	template <typename T>                                            \
	int getter_##FIELD(lua_State* L)                                 \
	{                                                                \
		CUData* ud = check_userdata<T>(L, 1);                        \
		if (!ud->getUser())                                          \
			luaL_error(L, "Object destroyed");                       \
		return pushToLua(L, static_cast<T*>(ud->getUser())->FIELD);  \
	}

// Macros for getting the pointer to getter method
#define GETTER_METHOD(ID, FIELD)                          \
	&getter_##FIELD<TL::TypeAt<ClassesList, ID>::Result>

// Macros, that puts new method entry in metatable methods array
#define GETTER_METHOD_ENTRY(ID, FIELD)    \
	{ #FIELD, GETTER_METHOD(ID, FIELD) }
//////////////////////////////////////////////////////////////////////////

// Macros for declaration of getter method with specified NAME
#define GETTER_NAMED_METHOD_DECL(NAME, FIELD)                        \
	template <typename T>                                            \
	int getter_##NAME(lua_State* L)                                  \
	{                                                                \
		CUData* ud = check_userdata<T>(L, 1);                        \
		if (!ud->getUser())                                          \
			luaL_error(L, "Object destroyed");                       \
		return pushToLua(L, static_cast<T*>(ud->getUser())->FIELD);  \
	}

// Macros for declaration of getter method with specified NAME
// If the CHECK fail, then getter will throw lua error.
#define GETTER_NAMED_CHECKED_METHOD_DECL(NAME, CHECK, FIELD)     \
	template <typename T>                                        \
	int getter_##NAME(lua_State* L)                              \
	{                                                            \
		CUData* ud = check_userdata<T>(L, 1);                    \
		T* obj = static_cast<T*>(ud->getUser());                 \
		if (!obj)                                                \
			luaL_error(L, "Object destroyed");                   \
		if (!(CHECK))                                            \
			luaL_error(L, "Check not passed");                   \
		return pushToLua(L, obj->FIELD);                         \
	}

// Macros for declaration of getter method with specified NAME
// If the CHECK fail, then getter will return nil.
#define GETTER_NAMED_CHECKED_NIL_METHOD_DECL(NAME, CHECK, FIELD) \
	template <typename T>                                        \
	int getter_##NAME(lua_State* L)                              \
	{                                                            \
		CUData* ud = check_userdata<T>(L, 1);                    \
		T* obj = static_cast<T*>(ud->getUser());                 \
		if (!obj)                                                \
			luaL_error(L, "Object destroyed");                   \
		if (!(CHECK))                                            \
		{                                                        \
			lua_pushnil(L);                                      \
			return 1;                                            \
		}                                                        \
		return pushToLua(L, obj->FIELD);                         \
	}


// Macros for getting the pointer to getter method with specified NAME
#define GETTER_NAMED_METHOD(ID, NAME)                         \
	&getter_##NAME<TL::TypeAt<ClassesList, ID>::Result>

// Macros, that puts new method entry in metatable methods array
#define GETTER_NAMED_METHOD_ENTRY(ID, NAME)    \
	{ #NAME, GETTER_NAMED_METHOD(ID, NAME) }
//////////////////////////////////////////////////////////////////////////

// Macros for declaration of getset method
#define GETSET_METHOD_DECL(FIELD)                                                               \
	template <typename T>                                                                       \
	int getset_##FIELD(lua_State* L)                                                            \
	{                                                                                           \
		CUData* ud = check_userdata<T>(L, 1);                                                   \
		if (!ud->getUser())                                                                     \
			luaL_error(L, "Object destroyed");                                                  \
		T* obj = static_cast<T*>(ud->getUser());                                                \
		bool typecheck = CHECK_INPUT_PARAM(L, 2, obj->FIELD);                                   \
		luaL_argcheck(L, typecheck || lua_isnone(L, 2), 2, "typecheck failed");                 \
		if (typecheck)                                                                          \
			getFromLua(L, 2, obj->FIELD);                                                       \
		return pushToLua(L, obj->FIELD);                                                        \
	}

// Macros for getting the pointer to getset method
#define GETSET_METHOD(ID, FIELD)                          \
	&getset_##FIELD<TL::TypeAt<ClassesList, ID>::Result>

// Macros, that puts new method entry in metatable methods array
#define GETSET_METHOD_ENTRY(ID, FIELD)    \
	{ #FIELD, GETSET_METHOD(ID, FIELD) }
//////////////////////////////////////////////////////////////////////////

// Macros for declaration of getset method with specified NAME
#define GETSET_NAMED_METHOD_DECL(NAME, FIELD)                                                    \
	template <typename T>                                                                        \
	int getset_##NAME(lua_State* L)                                                              \
	{                                                                                            \
		CUData* ud = check_userdata<T>(L, 1);                                                    \
		T* obj = static_cast<T*>(ud->getUser());                                                 \
		if (!obj)                                                                                \
			luaL_error(L, "Object destroyed");                                                   \
		bool typecheck = CHECK_INPUT_PARAM(L, 2, obj->FIELD);                                    \
		luaL_argcheck(L, typecheck || lua_isnone(L, 2), 2, "typecheck failed");   \
		if (typecheck)                                                                           \
			getFromLua(L, 2, obj->FIELD);                                                        \
		return pushToLua(L, obj->FIELD);                                                         \
	}

// Macros for declaration of getset method with specified NAME
// If the CHECK fail, then getter will throw lua error.
// TYPECHECK is function like lua_isnumber.
#define GETSET_NAMED_CHECKED_METHOD_DECL(NAME, CHECK, FIELD)                                     \
	template <typename T>                                                                        \
	int getset_##NAME(lua_State* L)                                                              \
	{                                                                                            \
		CUData* ud = check_userdata<T>(L, 1);                                                    \
		T* obj = static_cast<T*>(ud->getUser());                                                 \
		if (!obj)                                                                                \
			luaL_error(L, "Object destroyed");                                                   \
		if (!(CHECK))                                                                            \
			luaL_error(L, "Check not passed");                                                   \
		bool typecheck = CHECK_INPUT_PARAM(L, 2, obj->FIELD);                                    \
		luaL_argcheck(L, typecheck || lua_isnone(L, 2), 2, "typecheck failed");                  \
		if (typecheck)                                                                           \
			getFromLua(L, 2, obj->FIELD);                                                        \
		return pushToLua(L, obj->FIELD);                                                         \
	}

// Macros for declaration of getset method with specified NAME
// If the CHECK fail, then getter will return nil.
#define GETSET_NAMED_CHECKED_NIL_METHOD_DECL(NAME, CHECK, FIELD)                                 \
	template <typename T>                                                                        \
	int getset_##NAME(lua_State* L)                                                              \
	{                                                                                            \
		CUData* ud = check_userdata<T>(L, 1);                                                    \
		T* obj = static_cast<T*>(ud->getUser());                                                 \
		if (!obj)                                                                                \
			luaL_error(L, "Object destroyed");                                                   \
		if (!(CHECK))                                                                            \
		{                                                                                        \
			lua_pushnil(L);                                                                      \
			return 1;                                                                            \
		}                                                                                        \
		bool typecheck = CHECK_INPUT_PARAM(L, 2, obj->FIELD);                                                   \
		luaL_argcheck(L, typecheck || lua_isnone(L, 2), 2, "typecheck failed");   \
		if (typecheck)                                                                           \
			getFromLua(L, 2, obj->FIELD);                                                        \
		return pushToLua(L, obj->FIELD);                                                         \
	}

// Macros for declaration of getset method fro flag with specified NAME
// FLAG is flagname: in IsDead, the FLAG will be Dead
#define GETSET_FLAG_NAMED_METHOD_DECL(NAME, FLAG)                                                \
	template <typename T>                                                                        \
	int getset_##NAME(lua_State* L)                                                              \
	{                                                                                            \
		CUData* ud = check_userdata<T>(L, 1);                                                    \
		T* obj = static_cast<T*>(ud->getUser());                                                 \
		if (!obj)                                                                                \
			luaL_error(L, "Object destroyed");                                                   \
		bool typecheck = lua_isboolean(L, 2) != 0;                                               \
		luaL_argcheck(L, typecheck || lua_isnone(L, 2), 2, "typecheck lua_isboolean failed");    \
		if (typecheck)                                                                           \
			getFromLua<bool>(L, 2) ? obj->Set##FLAG() : obj->Clear##FLAG();                      \
		return pushToLua(L, obj->Is##FLAG());                                                    \
	}

// Macros for declaration of getset method with specified NAME
// If the CHECK fail, then getter will throw lua error.
// In obj->sprite->IsVisible(), the OBJ will be sprite
// FLAG is flagname: in IsVisible, the FLAG will be Visible
#define GETSET_FLAG_NAMED_CHECKED_METHOD_DECL(NAME, CHECK, OBJ, FLAG)                          \
	template <typename T>                                                                        \
	int getset_##NAME(lua_State* L)                                                              \
	{                                                                                            \
		CUData* ud = check_userdata<T>(L, 1);                                                    \
		T* obj = static_cast<T*>(ud->getUser());                                                 \
		if (!obj)                                                                                \
			luaL_error(L, "Object destroyed");                                                   \
		if (!(CHECK))                                                                            \
			luaL_error(L, "Check not passed");                                                   \
		bool typecheck = lua_isboolean(L, 2) != 0;                                               \
		luaL_argcheck(L, typecheck || lua_isnone(L, 2), 2, "typecheck lua_isboolean failed");    \
		if (typecheck)                                                                           \
			getFromLua<bool>(L, 2) ? obj->OBJ->Set##FLAG() : obj->OBJ->Clear##FLAG();            \
		return pushToLua(L, obj->OBJ->Is##FLAG());                                               \
	}

// Macros for declaration of getset method with specified NAME
// If the CHECK fail, then getter will return nil.
// In obj->sprite->IsVisible(), the OBJ will be sprite
// FLAG is flagname: in IsVisible, the FLAG will be Visible
#define GETSET_FLAG_NAMED_CHECKED_NIL_METHOD_DECL(NAME, CHECK, OBJ, FLAG)                        \
	template <typename T>                                                                        \
	int getset_##NAME(lua_State* L)                                                              \
	{                                                                                            \
		CUData* ud = check_userdata<T>(L, 1);                                                    \
		T* obj = static_cast<T*>(ud->getUser());                                                 \
		if (!obj)                                                                                \
			luaL_error(L, "Object destroyed");                                                   \
		if (!(CHECK))                                                                            \
		{                                                                                        \
			lua_pushnil(L);                                                                      \
			return 1;                                                                            \
		}                                                                                        \
		bool typecheck = lua_isboolean(L, 2) != 0;                                               \
		luaL_argcheck(L, typecheck || lua_isnone(L, 2), 2, "typecheck lua_isboolean failed");    \
		if (typecheck)                                                                           \
			getFromLua<bool>(L, 2) ? obj->OBJ->Set##FLAG() : obj->OBJ->Clear##FLAG();            \
		return pushToLua(L, obj->OBJ->Is##FLAG());                                               \
	}

// Macros for getting the pointer to getter method with specified NAME
#define GETSET_NAMED_METHOD(ID, NAME)                         \
	&getset_##NAME<TL::TypeAt<ClassesList, ID>::Result>

// Macros, that puts new method entry in metatable methods array
#define GETSET_NAMED_METHOD_ENTRY(ID, NAME)    \
	{ #NAME, GETSET_NAMED_METHOD(ID, NAME) }
//////////////////////////////////////////////////////////////////////////

// Standard methods for metatble
#define STD_METHODS(i)                                                   \
	{ "__gc",       &on_gc<TL::TypeAt<ClassesList, i>::Result> },        \
	{ "__tostring", &on_tostring<TL::TypeAt<ClassesList, i>::Result> },   \
	{ "object_present", &object_present<TL::TypeAt<ClassesList, i>::Result> }

// Sentinel
#define END \
	{ NULL, NULL }

// The block 4 is used to declare all the methods of metatables and the arrays for metatable registrations
#undef INCLUDE_BLOCK
#define INCLUDE_BLOCK 4
#include "userdata_binding.hpp"


// Registers metatable for type T
template <typename T>
void RegisterTypeMetatable(lua_State* L)
{
	STACK_CHECK_INIT(L);

	lua_newtable(L);                        // st: mt
	lua_pushstring(L, "__index");           // st: mt __index
	lua_pushvalue(L, -2);                   // st: mt __index mt
	lua_settable(L, -3);     /* metatable.__index = metatable */ 
	// st: mt

	luaL_register(L, NULL, ud_meta[UD_TYPE_ID(T)]);
	UD_META_REF(T) = SCRIPT::AddToRegistry();
	// st: 
	
	STACK_CHECK(L);
}

void RegisterCustomUserdataFunction(lua_State* L)
{
	for ( size_t i = 0; i < TL::Length<ClassesList>::value; i++ )
	{
		SCRIPT::GetFromRegistry(L, udata_refs[i]);
		if ( lua_istable( L, -1 ) )
		{
			lua_pushvalue(L, 1);
			lua_rawget(L, -2);
			if ( lua_isnil(L, -1 ) )
			{
				lua_pushvalue(L, 1);
				lua_pushvalue(L, 2);
				lua_rawset(L, -4);
			}
			lua_pop(L, 1);
		}
		lua_pop( L, 1 );
	}
}

void RegisterBasicMetatables(lua_State* L)
{
	lua_newtable( L ); //Vector2

	lua_pushstring(L, "__index" );
	lua_pushcfunction( L, &vector2_index );
	lua_settable(L, -3);
	lua_pushstring(L, "__tostring" );
	lua_pushcfunction( L, &vector2_tostring );
	lua_settable(L, -3);
	lua_pushstring(L, "__add" );
	lua_pushcfunction( L, &vector2_add );
	lua_settable(L, -3);
	lua_pushstring(L, "__sub" );
	lua_pushcfunction( L, &vector2_sub );
	lua_settable(L, -3);
	lua_pushstring(L, "__unm" );
	lua_pushcfunction( L, &vector2_unm );
	lua_settable(L, -3);
	lua_pushstring(L, "__mul" );
	lua_pushcfunction( L, &vector2_mul );
	lua_settable(L, -3);
	lua_pushstring(L, "__div" );
	lua_pushcfunction( L, &vector2_div );
	lua_settable(L, -3);
	lua_pushstring(L, "len" );
	lua_pushcfunction( L, &vector2_len );
	lua_settable(L, -3);
	lua_pushstring(L, "normalize" );
	lua_pushcfunction( L, &vector2_normalize );
	lua_settable(L, -3);
	lua_pushstring(L, "perp" );
	lua_pushcfunction( L, &vector2_perp );
	lua_settable(L, -3);
	lua_pushstring(L, "angle" );
	lua_pushcfunction( L, &vector2_angle );
	lua_settable(L, -3);
	lua_pushstring(L, "new" );
	lua_pushcfunction( L, &vector2_new );
	lua_settable(L, -3);

	vector2_ref = SCRIPT::AddToRegistry();

	lua_newtable( L ); //RGBAf;

	lua_pushstring(L, "__index" );
	lua_pushcfunction( L, &rgbaf_index );
	lua_settable(L, -3);
	lua_pushstring(L, "__tostring" );
	lua_pushcfunction( L, &rgbaf_tostring );
	lua_settable(L, -3);
	lua_pushstring(L, "new" );
	lua_pushcfunction( L, &rgbaf_new );
	lua_settable(L, -3);

	rgbaf_ref = SCRIPT::AddToRegistry();
}

int GetBasicMetatables(lua_State* L)
{
	SCRIPT::GetFromRegistry(L, vector2_ref);
	SCRIPT::GetFromRegistry(L, rgbaf_ref);
	
	return 2;
}

// The block 3 is used to register all the metatables using RegisterTypeMetatable function templates
#undef INCLUDE_BLOCK
#define INCLUDE_BLOCK 3
#include "userdata_binding.hpp"

