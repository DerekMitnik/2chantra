//////////////////////////////////////////////////////////////////////////
//
// How to add new class?
// 1. Put name of class in typelist ClassesList (the order of classes is important, they enumerated from 0). 
//    Class must implement createUData virtual function from CUDataUser class 
// 2. Declare the function for userdata creation (use UDATA_CREATOR_FUNC_DECL macro)
// 3. Add RegisterTypeMetatable call for new class in RegisterAllTypeMetatables function.
//
//////////////////////////////////////////////////////////////////////////
//
// 4-a. How to add new getter for field?
//    Add GETTER_METHOD_DECL(field_name)
//    Add GETTER_METHOD_ENTRY(number, field_name) in ud_meta array, where
//      number is number of class in ClassesList typelist;
//      field_name is name of public field in this class
// 4-b. How to add new getter/setter for field?
//    Add GETSET_METHOD_DECL(field_name)
//    Add GETSET_METHOD_ENTRY(number, field_name) in ud_meta array, where
//      number is number of class in ClassesList typelist;
//      field_name is name of public field in this class
//
// There are more declaration macros like GETSET_METHOD_DECL in udata.cpp.
//
// If there is field with equeal names in different classes, only one declaration must be.
// METAMETHODS_MAX_COUNT value can be increased, if needed.
//
//////////////////////////////////////////////////////////////////////////
//
// Structure of this file:
// This file consists of several include blocks. This helps to separate implementation details 
// from declarations and to simplify the maintance of the code.
// This file is included by the udata.cpp only.

#if INCLUDE_BLOCK == 1

// Includes of declarations of all types in typelist
#include "../game/objects/object.h"
#include "../game/objects/object_physic.h"
#include "../game/objects/object_dynamic.h"
#include "../game/objects/object_player.h"
#include "../game/objects/object_enemy.h"
#include "../game/objects/object_bullet.h"
#include "../game/objects/object_ray.h"
#include "../game/objects/object_environment.h"
#include "../game/objects/object_ribbon.h"
#include "../game/objects/object_spawner.h"
#include "../game/objects/object_waypoint.h"
#include "../game/objects/object_effect.h"

// Typelist with all types, that can be passed to lua as userdata
typedef TYPELIST_12(GameObject, ObjPhysic, ObjDynamic, ObjPlayer, ObjEnemy, ObjBullet, ObjRay, ObjEnvironment, ObjRibbon, ObjSpawner, ObjWaypoint, ObjEffect) ClassesList;

#elif INCLUDE_BLOCK == 2

// 2. For each type, it's needed to explicitly declare functions for userdata creation.
UDATA_CREATOR_FUNC_DECL(GameObject);
UDATA_CREATOR_FUNC_DECL(ObjPhysic);
UDATA_CREATOR_FUNC_DECL(ObjDynamic);
UDATA_CREATOR_FUNC_DECL(ObjPlayer);
UDATA_CREATOR_FUNC_DECL(ObjEnemy);
UDATA_CREATOR_FUNC_DECL(ObjBullet);
UDATA_CREATOR_FUNC_DECL(ObjRay);
UDATA_CREATOR_FUNC_DECL(ObjEnvironment);
UDATA_CREATOR_FUNC_DECL(ObjRibbon);
UDATA_CREATOR_FUNC_DECL(ObjSpawner);
UDATA_CREATOR_FUNC_DECL(ObjWaypoint);
UDATA_CREATOR_FUNC_DECL(ObjEffect);


#elif INCLUDE_BLOCK == 3

// 3. Registration of metatables
// This functions will be called on lua initialization in SCRIPT::InitScript
void RegisterAllTypeMetatables(lua_State* L)
{
	// Please, don't change this line
	std::fill(udata_refs, udata_refs + TL::Length<ClassesList>::value, LUA_NOREF);

	//Register metatables for Vector2, RGBAf and the like.
	RegisterBasicMetatables(L);
	
	// Make RegisterTypeMetatable calls for each type
	RegisterTypeMetatable<GameObject>(L);
	RegisterTypeMetatable<ObjPhysic>(L);
	RegisterTypeMetatable<ObjDynamic>(L);
	RegisterTypeMetatable<ObjPlayer>(L);
	RegisterTypeMetatable<ObjEnemy>(L);
	RegisterTypeMetatable<ObjBullet>(L);
	RegisterTypeMetatable<ObjRay>(L);
	RegisterTypeMetatable<ObjEnvironment>(L);
	RegisterTypeMetatable<ObjRibbon>(L);
	RegisterTypeMetatable<ObjSpawner>(L);
	RegisterTypeMetatable<ObjWaypoint>(L);
	RegisterTypeMetatable<ObjEffect>(L);
}


#elif INCLUDE_BLOCK == 4

// 4. Declaration of methods in metatables

#define DECL_SPRITE_METH                                                                     \
	GETSET_FLAG_NAMED_CHECKED_METHOD_DECL(sprite_mirrored, obj->sprite, sprite, Mirrored)    \
	GETSET_FLAG_NAMED_CHECKED_METHOD_DECL(sprite_fixed, obj->sprite, sprite, Fixed)          \
	GETSET_FLAG_NAMED_CHECKED_METHOD_DECL(sprite_visible, obj->sprite, sprite, Visible)      \
	GETSET_NAMED_CHECKED_METHOD_DECL(sprite_z, obj->sprite, sprite->z)                       \
	GETTER_NAMED_CHECKED_METHOD_DECL(sprite_animDone, obj->sprite, sprite->IsAnimDone())     \
	GETTER_NAMED_CHECKED_METHOD_DECL(sprite_angle, obj->sprite, sprite->getAngle())          \
	GETTER_NAMED_CHECKED_METHOD_DECL(sprite_animsCount, obj->sprite, sprite->animsCount)     \
	GETTER_NAMED_CHECKED_METHOD_DECL(sprite_cur_anim, obj->sprite, sprite->cur_anim.c_str()) \
	GETSET_NAMED_CHECKED_METHOD_DECL(sprite_color, obj->sprite, sprite->color)               \
	GETTER_NAMED_CHECKED_METHOD_DECL(sprite_frame, obj->sprite, sprite->getCurrentFrame())   \
	GETSET_NAMED_CHECKED_METHOD_DECL(sprite_frameWidth, obj->sprite, sprite->frameWidth)     \
	GETSET_NAMED_CHECKED_METHOD_DECL(sprite_frameHeight, obj->sprite, sprite->frameHeight)   \
	GETSET_NAMED_CHECKED_METHOD_DECL(sprite_blendingMode, obj->sprite, sprite->blendingMode) \
	GETSET_NAMED_CHECKED_METHOD_DECL(sprite_isometric_depth, obj->sprite, sprite->isometric_depth)

#ifdef MAP_EDITOR
#define DECL_GAMEOBJ_METH                          \
	DECL_SPRITE_METH                               \
	GETTER_METHOD_DECL(id)                         \
	GETSET_METHOD_DECL(aabb)                       \
	GETTER_METHOD_DECL(type)                       \
	GETSET_FLAG_NAMED_METHOD_DECL(show_border, editor.show_border)                                    \
	GETSET_NAMED_CHECKED_METHOD_DECL(border_color, true, editor.border_color)                         \
	GETSET_NAMED_CHECKED_METHOD_DECL(editor_color, true, editor.editor_color)                         \
	GETSET_NAMED_CHECKED_METHOD_DECL(min_difficulty, true, editor.min_difficulty)                     \
	GETSET_NAMED_CHECKED_METHOD_DECL(max_difficulty, true, editor.max_difficulty)                     \
	GETSET_FLAG_NAMED_METHOD_DECL(removed, obj.editor.removed)                                        \
	GETSET_FLAG_NAMED_METHOD_DECL(from_proto, obj.editor.from_proto)                                  \
	GETTER_NAMED_METHOD_DECL(physic, IsPhysic())   \
	GETSET_FLAG_NAMED_METHOD_DECL(dead, Dead)      \
	GETTER_NAMED_CHECKED_NIL_METHOD_DECL(parent, obj->parentConnection && !obj->parentConnection->orphan, parentConnection->parent)
#else
#define DECL_GAMEOBJ_METH                          \
	DECL_SPRITE_METH                               \
	GETTER_METHOD_DECL(id)                         \
	GETSET_METHOD_DECL(aabb)                       \
	GETTER_METHOD_DECL(type)                       \
	GETTER_NAMED_METHOD_DECL(physic, IsPhysic())   \
	GETSET_FLAG_NAMED_METHOD_DECL(dead, Dead)      \
	GETTER_NAMED_CHECKED_NIL_METHOD_DECL(parent, obj->parentConnection && !obj->parentConnection->orphan, parentConnection->parent)
#endif

#define DECL_OBJPHYS_METH                                                                              \
	GETSET_FLAG_NAMED_METHOD_DECL(solid, Solid)                                                        \
	GETSET_FLAG_NAMED_METHOD_DECL(bulletCollidable, BulletCollidable)                                  \
	GETTER_NAMED_METHOD_DECL(dynamic, IsDynamic())                                                     \
	GETSET_METHOD_DECL(solid_to)				                                                           \
	GETSET_NAMED_CHECKED_NIL_METHOD_DECL(material_type, obj->material, material->type)                 \
	GETSET_NAMED_CHECKED_NIL_METHOD_DECL(material_bounce_bonus, obj->material, material->bounce_bonus) \
	GETSET_NAMED_CHECKED_NIL_METHOD_DECL(material_friction, obj->material, material->friction)

#define DECL_OBJDYNAMIC_METH            \
	GETSET_METHOD_DECL(vel)             \
	GETSET_METHOD_DECL(own_vel)         \
	GETSET_METHOD_DECL(acc)             \
	GETSET_METHOD_DECL(gravity)         \
	GETSET_METHOD_DECL(trajectory)      \
	GETSET_METHOD_DECL(t_value)         \
	GETSET_METHOD_DECL(t_param1)        \
	GETSET_METHOD_DECL(t_param2)        \
	GETSET_METHOD_DECL(max_x_vel)       \
	GETSET_METHOD_DECL(max_y_vel)       \
	GETSET_METHOD_DECL(shadow_width)    \
	GETSET_METHOD_DECL(drops_shadow)    \
	GETSET_METHOD_DECL(lifetime)        \
	GETSET_METHOD_DECL(mass)            \
	GETSET_METHOD_DECL(walk_acc)        \
	GETSET_METHOD_DECL(jump_vel)        \
	GETSET_METHOD_DECL(ghostlike)       \
	GETSET_METHOD_DECL(facing)          \
	GETSET_FLAG_NAMED_METHOD_DECL(on_plane, OnPlane) \
	GETTER_NAMED_CHECKED_NIL_METHOD_DECL(suspected_plane, obj->suspected_plane && !obj->suspected_plane->orphan, suspected_plane->parent)

#define DECL_OBJCHAR_METH                       \
	GETSET_METHOD_DECL(is_invincible)           \
	GETSET_METHOD_DECL(health)                  \
	GETSET_METHOD_DECL(last_player_damage)      \
	GETSET_METHOD_DECL(last_player_damage_type) \
	GETSET_METHOD_DECL(health_max)              \
	GETSET_METHOD_DECL(last_hit_from)           \
	GETSET_METHOD_DECL(damage_mult) 

#define DECL_OBJPLAYER_METH                \
	GETSET_METHOD_DECL(controlEnabled)     \
	GETSET_METHOD_DECL(weapon_charge_time) \
	GETSET_METHOD_DECL(weapon_angle)       \
	GETSET_METHOD_DECL(target_weapon_angle)\
	GETSET_METHOD_DECL(ammo)               \
	GETSET_METHOD_DECL(additional_jumps)   \
	GETSET_METHOD_DECL(walljumps)          \
	GETSET_METHOD_DECL(recovery_time)      \
	GETSET_METHOD_DECL(air_control)        \
	GETSET_METHOD_DECL(camera_offset)      \
	GETTER_NAMED_CHECKED_METHOD_DECL(weapon_ready, obj->cur_weapon, WeaponReady(false)) \
	GETTER_NAMED_CHECKED_METHOD_DECL(alt_weapon_ready, obj->alt_weapon, WeaponReady(true))

#define DECL_OBJENEMY_METH                 \
	GETSET_METHOD_DECL(offscreen_behavior) \
	GETSET_METHOD_DECL(offscreen_distance) \
	GETSET_FLAG_NAMED_METHOD_DECL(touchable, Touchable) \
	GETTER_NAMED_CHECKED_NIL_METHOD_DECL(target, obj->target && !obj->target->orphan, target->parent)

#define DECL_OBJBULLET_METH               \
	GETSET_METHOD_DECL(damage)            \
	GETSET_METHOD_DECL(damage_type)       \
	GETSET_METHOD_DECL(charge)            \
	GETSET_METHOD_DECL(player_num)        \
	GETTER_NAMED_CHECKED_NIL_METHOD_DECL(shooter, obj->parentConnection && !obj->parentConnection->orphan, parentConnection->parent)

#define DECL_OBJRAY_METH      \
	// ���� ��� ������� ���, ��� ��������� ��� ����

#define DECL_OBJENV_METH      \
	// ���� �����, ������ ���������� ���������

#define DECL_OBJRIBBON_METH               \
	GETSET_METHOD_DECL(bl)            \
	GETSET_METHOD_DECL(br)            \
	GETSET_METHOD_DECL(bt)            \
	GETSET_METHOD_DECL(bb)            \
	GETSET_METHOD_DECL(ubl)           \
	GETSET_METHOD_DECL(ubr)           \
	GETSET_METHOD_DECL(ubt)           \
	GETSET_METHOD_DECL(ubb)           \
	GETSET_METHOD_DECL(repeat_x)      \
	GETSET_METHOD_DECL(repeat_y)      \
	GETSET_METHOD_DECL(k)       	  \
	GETSET_METHOD_DECL(spacing)

#define DECL_OBJSPAWNER_METH                 \
	GETSET_METHOD_DECL(spawnLimit)       \
	GETSET_METHOD_DECL(maximumEnemies)   \
	GETSET_METHOD_DECL(enemySpawnDelay)  \
	GETSET_METHOD_DECL(respawnDelay)     \
	GETSET_METHOD_DECL(size)             \
	GETSET_METHOD_DECL(respawn_dist)     \
	GETSET_METHOD_DECL(direction)        \
	GETSET_METHOD_DECL(charges)

#define DECL_WAYPOINT_METH                \

#define DECL_EFFECT_METH                  \
	GETSET_METHOD_DECL(disp)


DECL_GAMEOBJ_METH;
DECL_OBJPHYS_METH;
DECL_OBJDYNAMIC_METH;
DECL_OBJCHAR_METH;
DECL_OBJPLAYER_METH;
DECL_OBJENEMY_METH;
DECL_OBJBULLET_METH;
DECL_OBJRAY_METH;
DECL_OBJENV_METH;
DECL_OBJRIBBON_METH;
DECL_OBJSPAWNER_METH;
DECL_WAYPOINT_METH;
DECL_EFFECT_METH;

#define SPRITE_METH_ENTRY(ID)                          \
	GETSET_NAMED_METHOD_ENTRY(ID, sprite_mirrored),    \
	GETSET_NAMED_METHOD_ENTRY(ID, sprite_fixed),       \
	GETSET_NAMED_METHOD_ENTRY(ID, sprite_visible),     \
	GETSET_NAMED_METHOD_ENTRY(ID, sprite_z),           \
	GETTER_NAMED_METHOD_ENTRY(ID, sprite_animDone),    \
	GETTER_NAMED_METHOD_ENTRY(ID, sprite_angle),       \
	GETTER_NAMED_METHOD_ENTRY(ID, sprite_animsCount),  \
	GETTER_NAMED_METHOD_ENTRY(ID, sprite_cur_anim),    \
	GETSET_NAMED_METHOD_ENTRY(ID, sprite_color),       \
	GETTER_NAMED_METHOD_ENTRY(ID, sprite_frame),       \
	GETSET_NAMED_METHOD_ENTRY(ID, sprite_frameWidth),  \
	GETSET_NAMED_METHOD_ENTRY(ID, sprite_frameHeight), \
	GETSET_NAMED_METHOD_ENTRY(ID, sprite_blendingMode), \
	GETSET_NAMED_METHOD_ENTRY(ID, sprite_isometric_depth )

#define GAMEOBJ_METH_ENTRY(ID)         \
	SPRITE_METH_ENTRY(ID),             \
	GETTER_METHOD_ENTRY(ID, id),       \
	GETSET_METHOD_ENTRY(ID, aabb),     \
	GETTER_METHOD_ENTRY(ID, type),     \
	GETTER_METHOD_ENTRY(ID, physic),   \
	GETSET_METHOD_ENTRY(ID, dead),     \
	GETTER_NAMED_METHOD_ENTRY(ID, parent)

#define OBJPHYS_METH_ENTRY(ID)                             \
	GAMEOBJ_METH_ENTRY(ID),                                \
	GETSET_NAMED_METHOD_ENTRY(ID, solid),                  \
	GETSET_NAMED_METHOD_ENTRY(ID, bulletCollidable),       \
	GETTER_NAMED_METHOD_ENTRY(ID, dynamic),                \
	GETSET_METHOD_ENTRY(ID, solid_to),                     \
	GETSET_NAMED_METHOD_ENTRY(ID, material_type),          \
	GETSET_NAMED_METHOD_ENTRY(ID, material_bounce_bonus),  \
	GETSET_NAMED_METHOD_ENTRY(ID, material_friction)

#define OBJDYNAMIC_METH_ENTRY(ID)             \
	OBJPHYS_METH_ENTRY(ID),                   \
	GETSET_METHOD_ENTRY(ID, vel),             \
	GETSET_METHOD_ENTRY(ID, own_vel),         \
	GETSET_METHOD_ENTRY(ID, acc),             \
	GETSET_METHOD_ENTRY(ID, gravity),         \
	GETSET_METHOD_ENTRY(ID, trajectory),      \
	GETSET_METHOD_ENTRY(ID, t_value),         \
	GETSET_METHOD_ENTRY(ID, t_param1),        \
	GETSET_METHOD_ENTRY(ID, t_param2),        \
	GETSET_METHOD_ENTRY(ID, max_x_vel),       \
	GETSET_METHOD_ENTRY(ID, max_y_vel),       \
	GETSET_METHOD_ENTRY(ID, shadow_width),    \
	GETSET_METHOD_ENTRY(ID, drops_shadow),    \
	GETSET_METHOD_ENTRY(ID, lifetime),        \
	GETSET_METHOD_ENTRY(ID, mass),            \
	GETSET_METHOD_ENTRY(ID, walk_acc),        \
	GETSET_METHOD_ENTRY(ID, jump_vel),        \
	GETSET_METHOD_ENTRY(ID, ghostlike),       \
	GETSET_METHOD_ENTRY(ID, facing),          \
	GETSET_NAMED_METHOD_ENTRY(ID, on_plane),  \
	GETTER_NAMED_METHOD_ENTRY(ID, suspected_plane)

#define OBJCHAR_METH_ENTRY(ID)                        \
	OBJDYNAMIC_METH_ENTRY(ID),                        \
	GETSET_METHOD_ENTRY(ID, is_invincible),           \
	GETSET_METHOD_ENTRY(ID, health),                  \
	GETSET_METHOD_ENTRY(ID, last_player_damage),      \
	GETSET_METHOD_ENTRY(ID, last_player_damage_type), \
	GETSET_METHOD_ENTRY(ID, health_max),              \
	GETSET_METHOD_ENTRY(ID, last_hit_from),           \
	GETSET_METHOD_ENTRY(ID, damage_mult)

#define OBJPLAYER_METH_ENTRY(ID)                \
	OBJCHAR_METH_ENTRY(ID),                     \
	GETSET_METHOD_ENTRY(ID, controlEnabled),    \
	GETSET_METHOD_ENTRY(ID, weapon_charge_time),\
	GETSET_METHOD_ENTRY(ID, weapon_angle),      \
	GETSET_METHOD_ENTRY(ID, target_weapon_angle),\
	GETSET_METHOD_ENTRY(ID, ammo),              \
	GETSET_METHOD_ENTRY(ID, additional_jumps),  \
	GETSET_METHOD_ENTRY(ID, walljumps),         \
	GETSET_METHOD_ENTRY(ID, recovery_time),     \
	GETSET_METHOD_ENTRY(ID, air_control),       \
	GETSET_METHOD_ENTRY(ID, camera_offset),     \
	GETTER_METHOD_ENTRY(ID, weapon_ready),      \
	GETTER_METHOD_ENTRY(ID, alt_weapon_ready)

#define OBJENEMY_METH_ENTRY(ID)                 \
	OBJCHAR_METH_ENTRY(ID),                      \
	GETSET_METHOD_ENTRY(ID, offscreen_behavior), \
	GETSET_METHOD_ENTRY(ID, offscreen_distance), \
	GETSET_METHOD_ENTRY(ID, touchable),          \
	GETTER_NAMED_METHOD_ENTRY(ID, target)

#define OBJBULLET_METH_ENTRY(ID)                 \
	OBJDYNAMIC_METH_ENTRY(ID),                   \
	GETSET_METHOD_ENTRY(ID, damage),             \
	GETSET_METHOD_ENTRY(ID, damage_type),        \
	GETSET_METHOD_ENTRY(ID, charge),             \
	GETSET_METHOD_ENTRY(ID, player_num),         \
	GETTER_NAMED_METHOD_ENTRY(ID, shooter)

#define OBJRAY_METH_ENTRY(ID)                  \
	GAMEOBJ_METH_ENTRY(ID),                     \
	GETSET_METHOD_ENTRY(ID, damage),            \
	GETSET_METHOD_ENTRY(ID, damage_type),       \
	GETSET_METHOD_ENTRY(ID, charge),            \
	GETSET_METHOD_ENTRY(ID, player_num),        \
	GETTER_NAMED_METHOD_ENTRY(ID, shooter)

#define OBJENV_METH_ENTRY(ID)                  \
	OBJPHYS_METH_ENTRY(ID)

#define OBJRIBBON_METH_ENTRY(ID)                  \
	GAMEOBJ_METH_ENTRY(ID),                   \
	GETSET_METHOD_ENTRY(ID, bl),              \
	GETSET_METHOD_ENTRY(ID, br),              \
	GETSET_METHOD_ENTRY(ID, bt),              \
	GETSET_METHOD_ENTRY(ID, bb),              \
	GETSET_METHOD_ENTRY(ID, ubl),             \
	GETSET_METHOD_ENTRY(ID, ubr),             \
	GETSET_METHOD_ENTRY(ID, ubt),             \
	GETSET_METHOD_ENTRY(ID, ubb),             \
	GETSET_METHOD_ENTRY(ID, repeat_x),        \
	GETSET_METHOD_ENTRY(ID, repeat_y),        \
	GETSET_METHOD_ENTRY(ID, k),               \
	GETSET_METHOD_ENTRY(ID, spacing)

#define OBJSPAWNER_METH_ENTRY(ID)                 \
	GAMEOBJ_METH_ENTRY(ID),                   \
	GETSET_METHOD_ENTRY(ID, spawnLimit),      \
	GETSET_METHOD_ENTRY(ID, maximumEnemies),  \
	GETSET_METHOD_ENTRY(ID, enemySpawnDelay), \
	GETSET_METHOD_ENTRY(ID, respawnDelay),    \
	GETSET_METHOD_ENTRY(ID, size),            \
	GETSET_METHOD_ENTRY(ID, respawn_dist),    \
	GETSET_METHOD_ENTRY(ID, direction),       \
	GETSET_METHOD_ENTRY(ID, charges)

#define OBJWAYPOINT_METH_ENTRY(ID)                \
	GAMEOBJ_METH_ENTRY(ID)      

#define OBJEFFECT_METH_ENTRY(ID)                  \
	OBJDYNAMIC_METH_ENTRY(ID),                \
	GETSET_METHOD_ENTRY(ID, disp)

const size_t METAMETHODS_MAX_COUNT = 70;
static const struct luaL_reg ud_meta[TL::Length<ClassesList>::value][METAMETHODS_MAX_COUNT] = {
	{
		STD_METHODS(0),
		GAMEOBJ_METH_ENTRY(0),
		END
	},
	{
		STD_METHODS(1),
		OBJPHYS_METH_ENTRY(1),
		END
	},
	{
		STD_METHODS(2),
		OBJDYNAMIC_METH_ENTRY(2),
		END
	},
	{
		STD_METHODS(3),
		OBJPLAYER_METH_ENTRY(3),
		END
	},
	{
		STD_METHODS(4),
		OBJENEMY_METH_ENTRY(4),
		END
	},
	{
		STD_METHODS(5),
		OBJBULLET_METH_ENTRY(5),
		END
	},
	{
		STD_METHODS(6),
		OBJRAY_METH_ENTRY(6),
		END
	},
	{
		STD_METHODS(7),
		OBJENV_METH_ENTRY(7),
		END
	},
	{
		STD_METHODS(8),
		OBJRIBBON_METH_ENTRY(8),
		END
	},
	{
		STD_METHODS(9),
		OBJSPAWNER_METH_ENTRY(9),
		END
	},
	{
		STD_METHODS(10),
		OBJWAYPOINT_METH_ENTRY(10),
		END
	},
	{
		STD_METHODS(11),
		OBJEFFECT_METH_ENTRY(11),
		END
	}
};



#endif
