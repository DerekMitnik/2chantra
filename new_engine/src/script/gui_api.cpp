#include "StdAfx.h"

#include "../gui/gui.h"



#include "api.h"

//////////////////////////////////////////////////////////////////////////

extern Gui* gui;
extern lua_State* lua;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


int scriptApi::CreateWidget(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget type");
	luaL_argcheck(L, lua_isstring(L, 2), 2, "Must be widget name");
	luaL_argcheck(L, lua_isnumber(L, 3) || lua_isnil(L, 3), 3, "Must be parent");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "Must be x");
	luaL_argcheck(L, lua_isnumber(L, 5), 5, "Must be y");
	luaL_argcheck(L, lua_isnumber(L, 6), 6, "Must be w");
	luaL_argcheck(L, lua_isnumber(L, 7), 7, "Must be h");

	WidgetTypes wt = (WidgetTypes)lua_tointeger(L, 1);
	const char* name = lua_tostring(L, 2);
	UINT parent_id = 0;
	if ( lua_isnumber(L, 3) )
		parent_id = static_cast<UINT>(lua_tointeger(L, 3) );
	float x = (float)lua_tonumber(L, 4);
	float y = (float)lua_tonumber(L, 5);
	float w = (float)lua_tonumber(L, 6);
	float h = (float)lua_tonumber(L, 7);

	GuiWidget* parent = parent_id == 0 ? NULL : gui->GetWidget( parent_id );

	UINT id = gui->CreateWidget(wt, name, x, y, w, h, parent);
	lua_pop(L, lua_gettop(L));
	lua_pushinteger(L, id);
	return 1;
}

int scriptApi::DestroyWidget(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	gui->DestroyWidget((UINT)lua_tointeger(L, 1));
	return 0;
}

int scriptApi::WidgetSetParent(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be widget id");

	GuiWidget* wi1 = gui->GetWidget((UINT)lua_tointeger(L, 1));
	GuiWidget* wi2 = gui->GetWidget((UINT)lua_tointeger(L, 2));
	if (wi1 && wi2)
		wi1->SetParent( wi2 );

	return 0;
}

int scriptApi::WidgetSetBorder(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isboolean(L, 2), 2, "Must be bool");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
		wi->border = lua_toboolean(L, 2) != 0;
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetBorder: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetMaxTextfieldSize(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be size");

	size_t size = (size_t)lua_tointeger(L, 2);
	GuiTextfield* wt = dynamic_cast<GuiTextfield*>(gui->GetWidget((UINT)lua_tointeger(L, 1)));
	if (wt)
	{
		wt->max_size = size;
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetMaxTextfieldSize: wisget id=%d is not exist or not Textfield", (UINT)lua_tointeger(L, 1));
	}
	return 0;
}

int scriptApi::WidgetSetCaption(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isstring(L, 2), 2, "Must be caption");
	luaL_argcheck(L, lua_isboolean(L, 3)||lua_isnil(L, 3)||lua_isnone(L, 3), 2, "Must be bool multiline");

	bool multiline = false;
	if ( lua_isboolean(L, 3) ) multiline = lua_toboolean(L, 3) != 0;

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
		wi->SetCaption(lua_tostring(L, 2), multiline);
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetCaption: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}
	return 0;
}

int scriptApi::WidgetSetCaptionScroll(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be scrolling line");

	GuiWidget* wi = gui->GetWidget(static_cast<UINT>(lua_tointeger(L, 1)));
	if (wi)
	{
		wi->caption_multiline_scroll = static_cast<int>(lua_tointeger(L, 2));
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetScroll: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}
	return 0;
}

int scriptApi::WidgetGetCaption(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
		lua_pushstring(L, wi->caption);
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetGetCaption: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
		lua_pushnil(L);
	}
	return 1;
}

int scriptApi::WidgetGetName(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
		lua_pushstring(L, wi->name);
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetGetName: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
		lua_pushnil(L);
	}
	return 1;
}

int scriptApi::WidgetSetCaptionColor(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_istable(L, 2), 2, "Must be color table");
	luaL_argcheck(L, lua_isboolean(L, 3), 3, "Must be bool");
	luaL_argcheck(L, lua_istable(L, 4)|lua_isnone(L, 4)|lua_isnil(L, 4), 4, "outline");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		if (lua_toboolean(L, 3))
			SCRIPT::GetColorFromTable(L, 2, wi->caption_act_color);
		else
			SCRIPT::GetColorFromTable(L, 2, wi->caption_inact_color);
		if (lua_istable(L, 4))
			SCRIPT::GetColorFromTable(L, 4, wi->caption_outline);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetCaptionColor: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}
	return 0;
}

int scriptApi::WidgetSetBorderColor(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_istable(L, 2), 2, "Must be color table");
	luaL_argcheck(L, lua_isboolean(L, 3), 3, "Must be bool");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		if (lua_toboolean(L, 3))
			SCRIPT::GetColorFromTable(L, 2, wi->border_active_color);
		else
			SCRIPT::GetColorFromTable(L, 2, wi->border_inactive_color);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetBorderColor: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetCaptionFont(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isstring(L, 2), 2, "Must be fontname");
	luaL_argcheck(L, lua_isnumber(L, 3)||lua_isnone(L, 3)||lua_isnil(L, 3), 3, "Must be font size");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		wi->caption_font = FontByName(lua_tostring(L, 2));
		if ( lua_isnumber(L, 3) )
			wi->caption_scale = static_cast<float>(lua_tonumber(L, 3));
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetCaptionFont: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::GetCaptionSize(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be font");
	luaL_argcheck(L, lua_isstring(L, 2), 2, "Must be text");

	IFont* font = FontByName(lua_tostring(L, 1));
	if (font)
	{
		int result_y = font->GetStringHeight(lua_tostring(L, 2));
		int result_x = font->GetStringWidth(lua_tostring(L, 2));
		lua_pushinteger(L, result_x);
		lua_pushinteger(L, result_y);
		return 2;
	}
	return 0;
}

int scriptApi::GetMultilineCaptionSize(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be font");
	luaL_argcheck(L, lua_isstring(L, 2), 2, "Must be text");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be width");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "Must be scale");

	IFont* font = FontByName(lua_tostring(L, 1));
	if (font)
	{
		size_t lines = 0;
		RGBAf old_clr = font->tClr;
		CAABB area = CAABB(0, 0, static_cast<float>(lua_tonumber(L,3)), 1.0f);
		font->tClr = RGBAf( 0.0f, 0.0f, 0.0f, 0.0f );
		font->scale = static_cast<float>(lua_tonumber(L, 4));
		lines = font->PrintMultiline( lua_tostring(L, 2), area, -1, -1, true );
		lua_pushnumber(L, 2.0f*area.W);
		lua_pushnumber(L, 2.0f*area.H);
		lua_pushnumber(L, lines);
		font->tClr = old_clr;
		font->scale = 1.0f;
		return 3;
	}
	return 0;
}

int scriptApi::WidgetUseTyper(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isboolean(L, 2), 2, "Must be bool");
	

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		bool use = lua_toboolean(L, 2) != 0;
		if (use)
		{
			luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be int");
			luaL_argcheck(L, lua_isboolean(L, 4) || lua_isnil(L,4) || lua_isnone(L, 4), 4, "Must be pausable flag");
			bool pausable = lua_isboolean(L, 4) && lua_toboolean(L, 4);
			luaL_argcheck(L, lua_isstring(L, 5)||lua_isnil(L, 5)||lua_isnone(L, 5), 5, "Must be sound name");

			if ( lua_isstring(L, 5) )
			{
				wi->UseTyper(static_cast<UINT>(lua_tointeger(L, 3)), pausable, lua_tostring(L, 5));
			}
			else
			{
				wi->UseTyper(static_cast<UINT>(lua_tointeger(L, 3)), pausable);
			}
		}
		else
		{
			wi->UnUseTyper();
		}
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetUseTyper: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetStartTyper(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		if (!wi->StartTyper())
		{
			SCRIPT::LogLuaCallStack(L, DEFAULT_SCRIPT_LOG_NAME, logLevelError );
		}
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, "Error in WidgetStartTyper: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
		SCRIPT::LogLuaCallStack(L, DEFAULT_SCRIPT_LOG_NAME, logLevelError );
	}

	return 0;
}

int scriptApi::WidgetStopTyper(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		wi->StopTyper();
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetStopTyper: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetOnTyperEndedProc(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		SCRIPT::RegProc(L, &wi->typer->onTyperEnded, 2);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetMouseLeaveProc: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}


int scriptApi::WidgetSetVisible(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isboolean(L, 2), 2, "Must be bool");

	UINT id = static_cast<UINT>(lua_tointeger(L, 1));
	bool vis = lua_toboolean(L, 2) != 0;

	bool res = gui->SetWidgetVisibility(id, vis);
	if (!res)
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetVisible: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetGetVisible(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		lua_pushboolean(L, wi->IsVisible());
	}
	else
	{
		lua_pushnil(L);
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetGetVisible: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 1;
}

int scriptApi::WidgetSetFixedPosition(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isboolean(L, 2), 2, "Must be bool");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		wi->SetFixedPosition(lua_toboolean(L, 2) != 0);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetFixedPosition: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetGetSize(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		lua_pushnumber(L, 2*wi->aabb.W);
		lua_pushnumber(L, 2*wi->aabb.H);
	}
	else
	{
		lua_pushnil(L);
		lua_pushnil(L);
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetGetSize: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 2;
}

int scriptApi::WidgetGetPos(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		lua_pushnumber(L, wi->aabb.Left());
		lua_pushnumber(L, wi->aabb.Top());
	}
	else
	{
		lua_pushnil(L);
		lua_pushnil(L);
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetGetSize: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 2;
}

int scriptApi::WidgetSetFocusable(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isboolean(L, 2), 2, "Must be bool");
	
	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		wi->focusable = lua_toboolean(L, 2) != 0;
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetFocusable: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetActive(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isboolean(L, 2), 2, "Must be bool");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		wi->SetActivity( lua_toboolean(L, 2) != 0, false );
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetActive: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetGroup(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be widget group");
	
	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		wi->group = static_cast<int>(lua_tointeger(L, 2));
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetGroup: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}
	
	return 0;
}

int scriptApi::WidgetSetLMouseClickProc(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		SCRIPT::RegProc(L, &wi->onMouseLClickProc, 2);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetLMouseClickProc: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetRMouseClickProc(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		SCRIPT::RegProc(L, &wi->onMouseRClickProc, 2);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetRMouseClickProc: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetMouseEnterProc(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	
	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		SCRIPT::RegProc(L, &wi->onMouseEnterProc, 2);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetMouseEnterProc: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetMouseLeaveProc(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		SCRIPT::RegProc(L, &wi->onMouseLeaveProc, 2);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetMouseLeaveProc: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetKeyDownProc(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		SCRIPT::RegProc(L, &wi->onKeyDown, 2);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetKeyDownProc: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetKeyPressProc(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		SCRIPT::RegProc(L, &wi->onKeyPress, 2);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetKeyPressProc: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetKeyInputProc(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		SCRIPT::RegProc(L, &wi->onKeyInput, 2);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetKeyInputProc: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetFocusProc(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		SCRIPT::RegProc(L, &wi->onFocus, 2);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetFocusProc: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetUnFocusProc(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		SCRIPT::RegProc(L, &wi->onUnFocus, 2);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetUnFocusProc: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetResizeProc(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		SCRIPT::RegProc(L, &wi->onResize, 2);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetResizeProc: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetGainFocus(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	UINT id = (UINT)lua_tointeger(L, 1);

	gui->UnlockFocus();
	gui->SetFocus(id);

	return 0;
}

int scriptApi::GetFocusLock(lua_State* L)
{
	lua_pushboolean( L, gui->GetFocusLock() );

	return 1;
}

int scriptApi::WidgetBringToFront(lua_State* L)

{
	luaL_error(L, "WidgetBringToFront is not available");
	return 0;
	//luaL_argcheck(L, lua_islightuserdata(L, 1), 1, "Must be widget");

	//void* p = lua_touserdata(L, 1);
	//GuiWidget* wi = (GuiWidget*)p;

	//wi->BringToFront();

	//return 0;
}

int scriptApi::WidgetSetSize(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1) || lua_isnil(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be width");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be height");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		float w = (float)lua_tonumber(L, 2);
		float h = (float)lua_tonumber(L, 3);

		float top = wi->aabb.Top();
		float left = wi->aabb.Left();
		wi->aabb = CAABB(left, top, left + w, top + h);
		if ( wi->sprite && wi->sprite->tex == NULL )
		{
			wi->sprite->frameWidth = 2*((USHORT)wi->aabb.W);
			wi->sprite->frameHeight = 2*((USHORT)wi->aabb.H);
		}
		// TODO: ����� �� ����� ���� �����, �� �������. ���� ����������.
		//wi->aabb.W = w/2;
		//wi->aabb.H = w/2;
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetSize: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}


	return 0;
}

int scriptApi::WidgetSetPos(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1) || lua_isnil(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be x1");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be y1");
	luaL_argcheck(L, lua_isboolean(L, 4) || lua_isnil(L, 4) || lua_isnone(L, 4), 4, "Parent relativity bool");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		float x1 = (float)lua_tonumber(L, 2);
		float y1 = (float)lua_tonumber(L, 3);

		if ( lua_isboolean(L, 4) && lua_toboolean(L, 4) && wi->parent )
		{
			x1 += wi->parent->aabb.Left();
			y1 += wi->parent->aabb.Top();
		}

		wi->Move( x1, y1 );
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetPos: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetZ(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be z");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		wi->SetZ((float)lua_tonumber(L, 2) );
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetZ: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetSprite(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isstring(L, 2), 2, "Must be proto_name");
	luaL_argcheck(L, lua_isstring(L, 3) || lua_isboolean(L, 3) || lua_isnone(L, 3), 3, "Must be start_anim");
	luaL_argcheck(L, lua_isnumber(L, 4) || lua_isnil(L, 4) || lua_isnone(L, 4), 4, "Must be frame number");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		if ( lua_isboolean(L, 3) )
		{
			if ( lua_isnumber(L, 4) )
				wi->SetSprite( lua_tostring(L, 2), (UINT)lua_tointeger(L, 4) );
			else
				wi->SetSprite( lua_tostring(L, 2) );
		}
		else
			wi->SetSprite(lua_tostring(L, 2), lua_tostring(L, 3));
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetSprite: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetSpriteAngle(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Widget id");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "angle");

	float angle = static_cast<float>( lua_tonumber(L, 2) );

	GuiWidget* wi = gui->GetWidget(static_cast<UINT>(lua_tointeger(L, 1)));
	if (wi && wi->sprite)
	{
		wi->sprite->setAngle(angle);
	}
	
	return 0;
}

int scriptApi::WidgetSetSpriteColor(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_istable(L, 2), 2, "Must be color table");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	RGBAf color;
	SCRIPT::GetColorFromTable(L, 2, color);
	if (wi)
	{
		wi->SetSpriteColor( color );
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetSpriteColor: widget id=%d is not exist or has no sprite", (UINT)lua_tointeger(L, 1));
	}
	return 0;
}

int scriptApi::WidgetSetColorBox(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_istable(L, 2), 2, "Must be color table");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	RGBAf color;
	SCRIPT::GetColorFromTable(L, 2, color);
	if (wi)
	{
		wi->SetColorBox(color);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetColorBox: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}
	return 0;
}

int scriptApi::WidgetSetSpriteRenderMethod(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be render_method (constants.rsm*");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		wi->SetSpriteRenderMethod((RenderSpriteMethod)lua_tointeger(L, 2));
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetSpriteRenderMethod: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetSpriteBlendingMode(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be blending_mode (constants.bm*");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi)
	{
		wi->SetSpriteBlendingMode((BlendingMode)lua_tointeger(L, 2));
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetSpriteRenderMethod: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}

int scriptApi::WidgetSetAnim(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	luaL_argcheck(L, lua_isstring(L, 2), 2, "Must be anim_name");

	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (wi && wi->sprite)
	{
		wi->sprite->SetAnimation(string(lua_tostring(L, 2)));
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetAnim: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
	}

	return 0;
}


int scriptApi::GlobalSetKeyDownProc(lua_State* L)
{
	SCRIPT::RegProc(L, &gui->onKeyDownGlobal, 1);	
	return 0;
}

int scriptApi::GlobalSetKeyReleaseProc(lua_State* L)
{
	SCRIPT::RegProc(L, &gui->onKeyReleaseGlobal, 1);	
	return 0;
}

int scriptApi::GlobalGetKeyDownProc(lua_State* L)
{
	SCRIPT::GetFromRegistry(L, gui->onKeyDownGlobal);
	return 1;
}

int scriptApi::GlobalGetKeyReleaseProc(lua_State* L)
{
	SCRIPT::GetFromRegistry(L, gui->onKeyReleaseGlobal);
	return 1;
}

int scriptApi::GlobalSetMouseKeyDownProc(lua_State* L)
{
	SCRIPT::RegProc(L, &gui->onMouseKeyDownGlobal, 1);	
	return 0;
}

int scriptApi::GlobalSetMouseKeyReleaseProc(lua_State* L)
{
	SCRIPT::RegProc(L, &gui->onMouseKeyReleaseGlobal, 1);	
	return 0;
}

int scriptApi::GlobalGetMouseKeyDownProc(lua_State* L)
{
	SCRIPT::GetFromRegistry(L, gui->onMouseKeyDownGlobal);
	return 1;
}

int scriptApi::GlobalGetMouseKeyReleaseProc(lua_State* L)
{
	SCRIPT::GetFromRegistry(L, gui->onMouseKeyReleaseGlobal);
	return 1;
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

int scriptApi::PushWidget(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be widget id");
	GuiWidget* wi = gui->GetWidget((UINT)lua_tointeger(L, 1));
	if (!wi)
	{
		lua_pushnil(L);
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in WidgetSetAnim: widget id=%d is not exist", (UINT)lua_tointeger(L, 1));
		return 1;
	}

	lua_newtable(L);

	lua_pushnumber(L, wi->id);					lua_setfield(L, -2, "id");
	lua_pushstring(L, wi->name);				lua_setfield(L, -2, "name");

	lua_pushboolean(L, wi->active);				lua_setfield(L, -2, "active");
	lua_pushboolean(L, wi->IsVisible());		lua_setfield(L, -2, "visible");
	lua_pushboolean(L, wi->dead);				lua_setfield(L, -2, "dead");

	SCRIPT::PushAABB(L, wi->aabb);				lua_setfield(L, -2, "aabb");

	if(wi->sprite)
	{
		lua_newtable(L);	// ����: obj sprite
		lua_pushboolean(L, wi->sprite->IsMirrored());	lua_setfield(L, -2, "mirrored");
		lua_pushboolean(L, wi->sprite->IsFixed());		lua_setfield(L, -2, "fixed");
		lua_pushboolean(L, wi->sprite->IsVisible());	lua_setfield(L, -2, "visible");
		lua_pushboolean(L, wi->sprite->IsAnimDone());	lua_setfield(L, -2, "animDone");
		lua_pushstring(L, wi->sprite->cur_anim.c_str()); lua_setfield(L, -2, "cur_anim");
		lua_setfield(L, -2, "sprite");	// ����: obj
	}

	lua_pushboolean(L, wi->border);					lua_setfield(L, -2, "border");


	lua_pushstring(L, wi->caption);					lua_setfield(L, -2, "caption");
	if (wi->caption_font)
	{
		//lua_pushstring(L, wi->caption_font->name
	}

	lua_pushnumber(L, wi->lastMousePos);			lua_setfield(L, -2, "lastMousePos");

	{
		lua_newtable(L);
		lua_pushnumber(L, wi->lastmouseButtonState[InputMgr::MOUSE_BTN_LEFT]);			lua_setfield(L, -2, "left");
		lua_pushnumber(L, wi->lastmouseButtonState[InputMgr::MOUSE_BTN_RIGHT]);			lua_setfield(L, -2, "right");
		//lua_pushnumber(L, wi->lastmouseButtonState[InputMgr::MOUSE_BTN_MIDDLE]);		lua_setfield(L, -2, "middle");
		lua_setfield(L, -2, "lastmouseButtonState");
	}


	return 1;
}

