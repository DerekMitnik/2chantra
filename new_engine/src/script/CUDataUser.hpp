#pragma once

class CUData;

struct lua_State;

class CUDataUser
{
	CUData* pUdata;

protected:

	virtual CUData* createUData() = 0;

public:

	CUDataUser():
		pUdata(0)
	{
		
	}

	void setUData(CUData* pUdata) { this->pUdata = pUdata; }

	//LuaRegRef getUData();
	int pushUData(lua_State* L);


	virtual ~CUDataUser();
};