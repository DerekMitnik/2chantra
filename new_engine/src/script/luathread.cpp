#include "StdAfx.h"

#include "../misc.h"

#include "api.h"
#include "script.h"
#include "timerevent.h"

#include "luathread.h"

//////////////////////////////////////////////////////////////////////////

extern lua_State* lua;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// ������ ������
list<LuaThread*> threads;
typedef list<LuaThread*>::iterator ThreadIter;	// ��� ��������� �� ������ ������

// ���������� �������� �� ���� � ������
ThreadIter GetThread(lua_State* c)
{
	list<LuaThread*>::iterator it;
	for(list<LuaThread*>::iterator it = threads.begin();
		it != threads.end();
		it++)
	{
		LuaThread* lt = *it;
		if (lt->lThread == c)
			return it;
	}
	return threads.end();
}

// ������� ����� ����, ���� � �������� �������� L/
// ��������� ���������� coroutine.create, ������� � ���� ��������� coroutine.
// ���������� ������ �� coroutine, ����������� � ������� ���.
LuaRegRef NewThread(lua_State* L)
{	
	// ���� L: func pausable
	assert(lua_gettop(L) == 1 || lua_gettop(L) == 2);
	bool pausable = false;
	if (lua_gettop(L) == 2)
	{
		pausable = lua_toboolean(L, 2) != 0;
		lua_pop(L, 1);
	}

	// ��� �� ������� luaB_cocreate ����� lbaselib.c
	// ���� L: func
	lua_State *NL = lua_newthread(L);	// ���� L: func thread
										// ���� NL: 

	luaL_argcheck(L, lua_isfunction(L, 1) && !lua_iscfunction(L, 1), 1,
		"Lua function expected");

	lua_pushvalue(L, 1);  // ���� L: func thread func
	lua_xmove(L, NL, 1);  /* move function from L to NL */
	
	// ����� ���� �� ������� luaB_cocreate ����� lbaselib.c

	// ���� L: func thread
	// ���� NL: func
	
	LuaThread* lt = new LuaThread();

	lua_pushvalue(L, -1);					// ���� L: func thread thread 
	// � ���� L == lua, �� �������� �� �����, � ������� ����� ������ �������� 1 ����� thread
	lua_xmove(L, lua, 1);					// ���� L: func thread
											// ���� lua: thread
	lt->refKey = SCRIPT::AddToRegistry();	// ���� lua: 
	lt->lThread = NL;
	lt->pausable = pausable;
	threads.push_back(lt);

	Log(DEFAULT_SCRIPT_LOG_NAME, logLevelInfo, "created thread 0x%p, master_thread = 0x%p, refKey = %d", NL, L, lt->refKey);

	return lt->refKey;
}

// ������� ����� coroutine. ���������� ������� ������� ����������
// lua coroutines.create, �� ����������� ����, ���
// ���������� ��������� coroutine.
int scriptApi::NewThread(lua_State* L)
{
	luaL_argcheck(L, lua_isfunction(L, 1), 1, "function expected");
	luaL_argcheck(L, lua_isboolean(L, 2) || lua_isnoneornil(L, 2), 2, "boolean or none expected");
	// ���� L: func pausable
	::NewThread(L);
	// ���� L: func thread
	return 1;
}

// ������� ���� � ��� ������� �������.
void RemoveThread(ThreadIter it)
{
	LuaThread* lt = *it;
	DeleteTimerEvent(lt->refKey);
	Log(DEFAULT_SCRIPT_LOG_NAME, logLevelInfo, "removed thread 0x%p, refKey = %d", lt->lThread, lt->refKey);
	SCRIPT::RemoveFromRegistry(lt->refKey);
	DELETESINGLE(lt);
	threads.erase(it);
}

// ������� ��� �����.
void RemoveAllThreads()
{
	Log(DEFAULT_SCRIPT_LOG_NAME, logLevelInfo, "Destoying threads" );

	LuaThread* lt = NULL;
	ThreadIter it;
	for(it = threads.begin();
		it != threads.end();
		it++)
	{
		lt = *it;

		SCRIPT::RemoveFromRegistry(lt->refKey);
		DELETESINGLE(lt);
	}
	threads.clear();

	DeleteAllThreadEvents();
}

// ������� ��� �����. ������� ��� ���. 
int scriptApi::StopAllThreads(lua_State* L)
{
	UNUSED_ARG(L);
	::RemoveAllThreads();
	return 0;
}

// ������� ����. ������� ��� ���.
// ���������� true ��� false.
int scriptApi::StopThread(lua_State* L)
{
	luaL_argcheck(L, lua_isthread(L, 1), 1, "Thread expected");
	lua_State* t = lua_tothread(L, 1);
	ThreadIter it = GetThread(t);
	if (it != threads.end())
	{
		::RemoveThread(it);
		lua_pushboolean(L, 1);
		return 1;
	}
	else
	{
		lua_pushboolean(L, 0);
		lua_pushstring(L, "Thread is not registered");
		return 2;
	}	
}


// ������� ���������������� coroutine, �� ������� �������. ����������
// ������� ������� ���������� lua coroutines.yield, �� ������� ������
// �������� - ����� ����������, �� ������� coroutine ����� ��������������.
// ������, ���������� ��������� ��� �������� ������������, resume ������ �� ������.
int scriptApi::Wait(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1,
		"Number expected");

	if (lua_pushthread(L))
	{
		// lua_pushthread ������ true, ���� �� �������� ���������� �������� �����
		luaL_where(L, 0);
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelScript, "%s attempting to Wait on main thread", lua_tostring(L, -1));
		lua_pop(L, lua_gettop(L));
		return 0;
	}
	else
	{
		// lua_pushthread ������� false, �� �� � �������� ������, ������� �� ����� �� ������ ��������
		lua_pop(L, 1);
	}

	ThreadIter it = GetThread(L);
	if (it != threads.end())
	{
		AddThreadTimerEvent( static_cast<UINT>(lua_tointeger(L, 1)), (*it)->refKey, (*it)->pausable);
	}
	lua_remove(L, 1);


	return lua_yield(L, lua_gettop(L));
}

// ���������� ���������� ���������������� coroutine, ����������
// ������, �� ������� ������ ����.
// � ������� �� coroutine.resume, ������ �� ����������.
int Resume(ThreadIter it, lua_State* L)
{
	LuaThread* lt = *it;
	//SCRIPT::GetFromRegistry(lt->refKey);
	//SCRIPT::StackDumpToLog(lt->masterState);
	//UINT res = luaB_coresume(lt->masterState);
	
	if (L)
	{
		// ���� L:		thread arg1 arg2 ... argN
		// C��� thread: [func]
		// func �� ����� ������ � ������ ���
		lua_xmove(L, lt->lThread, lua_gettop(L)-1);
		// ���� L:		thread 
		// C��� thread: [func] arg1 arg2 ... argN
	}


	//SCRIPT::StackDumpToLog(lt->masterState);
	//SCRIPT::StackDumpToLog(lt->lThread);
	lua_resume(lt->lThread, lua_gettop(lt->lThread) - 1);

	if (lua_status(lt->lThread) != LUA_YIELD)
	{
		if (lua_status(lt->lThread) != 0)		// ������ ������
		{
			const char* err = lua_tostring(lt->lThread, -1);
			Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, "thread 0x%p: %s", lt->lThread, err);
			SCRIPT::LogLuaCallStack(lt->lThread, DEFAULT_SCRIPT_LOG_NAME, logLevelError );
			#ifdef LUA_ERROR_SCRIPT
					SCRIPT::ScriptError();
			#endif

		}
		RemoveThread(it);
	}

	return 0;
}

// ���������� ���������� ���������������� coroutine.
// ���������� ������� coroutines.resume, ������ ��������
// ��������� ������, ��� ��� ������� ���� ����� ��������� ����� ������.
// �� ���������� ���������, ��������� ����� Wait
int scriptApi::Resume(lua_State* L)
{
	lua_State *co = lua_tothread(L, 1);
	luaL_argcheck(L, co, 1, "coroutine expected");
	ThreadIter it = GetThread(co);
	if (it != threads.end())
	{
		return ::Resume(it, L);
	}


	// ����� luaB_coresume (coroutine.resume � lua) � auxresume �� ���� lua 5.1.
	// ��� ����������, ����� ��������� ��������� coroutine.resume. 
	// TODO: ��������, ����� � ���������� ������� coroutine.resume ����� lua_pcall, �� ���
	// ������ ����� ���� ������������ ����� ���� �� ����������, ��� ����������, ��� �� ������������.
	// �� � ������, ��� ���� ������, ������ ���� ���-�� ���������� ������������ ���� Resume ������
	// coroutine.resume ��� ����������� �����, ���������� �� � ������� NewThread.
	int narg = lua_gettop(L) - 1;
	lua_xmove(L, co, narg);
	int status = lua_resume(co, narg);
	int r;
	if (status == 0 || status == LUA_YIELD) {
		int nres = lua_gettop(co);
		if (!lua_checkstack(L, nres + 1))
			luaL_error(L, "too many results to resume");
		lua_xmove(co, L, nres);  /* move yielded values */
		//return nres;
		r = nres;
	}
	else {
		lua_xmove(co, L, 1);  /* move error message */
		//return -1;  /* error flag */
		r = -1;
	}
	if (r < 0) {
		lua_pushboolean(L, 0);
		lua_insert(L, -2);
		return 2;  /* return false + error message */
	}
	else {
		lua_pushboolean(L, 1);
		lua_insert(L, -(r + 1));
		return r + 1;  /* return true + `resume' returns */
	}
}

// ���������� ��� ���������� ������� �������, �������� ����������� ������ �����
void ProcessThread(LuaRegRef r)
{
	LuaThread* lt = NULL;
	ThreadIter it;
	for(it = threads.begin();
		it != threads.end();
		it++)
	{
		lt = *it;
		if (lt->refKey == r)
			break;
	}

	if (lt)
	{
		Resume(it, NULL);
	}
}
