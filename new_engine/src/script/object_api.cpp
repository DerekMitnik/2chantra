#include "StdAfx.h"

#include "../misc.h"

#include "../game/object_mgr.h"
#include "../game/player.h"
#include "../game/objects/object.h"
#include "../game/objects/object_enemy.h"
#include "../game/objects/object_bullet.h"
#include "../game/objects/object_environment.h"
#include "../game/objects/object_sprite.h"
#include "../game/objects/object_effect.h"
#include "../game/objects/object_waypoint.h"
#include "../game/objects/object_spawner.h"
#include "../game/objects/object_item.h"
#include "../game/objects/object_particle_system.h"
#include "../game/objects/object_ray.h"
#include "../game/objects/object_ribbon.h"

#include "../game/editor.h"
#include "../game/phys/phys_collisionsolver.h"

#include "../game/sprite.h"
#include "../render/texture.h"

#include "api.h"
#include "luathread.h"
#include "udata.hpp"
#include "CUData.hpp"

//////////////////////////////////////////////////////////////////////////
extern Player* playerControl;
extern std::vector<Player*> players;
extern InputMgr inpmgr;
extern ResourceMgr<Proto> * protoMgr;
extern lua_State* lua;
#ifdef MAP_EDITOR
extern editor::EditorStates editor_state;
#endif //MAP_EDITOR

extern float CAMERA_DRAW_LEFT;
extern float CAMERA_DRAW_RIGHT;
extern float CAMERA_DRAW_TOP;
extern float CAMERA_DRAW_BOTTOM;
//////////////////////////////////////////////////////////////////////////


void PushSpawner(lua_State* L, ObjSpawner* o);
void PushPhysic(lua_State* L, ObjPhysic* o);
void PushDynamic(lua_State* L, ObjDynamic* o);
void PushCharacter(lua_State* L, ObjCharacter* o);
void PushPlayer(lua_State* L, ObjPlayer* o);
void PushRibbon(lua_State* L, ObjRibbon* o);
//////////////////////////////////////////////////////////////////////////



// �������� � ���� �������, ������������� ������� ������
void PushObject(lua_State* L, GameObject* o)
{
	lua_newtable(L);	// ����: obj

	lua_pushinteger(L, o->id);		lua_setfield(L, -2, "id");
	lua_pushinteger(L, o->type);	lua_setfield(L, -2, "type");
	SCRIPT::PushAABB(L, o->aabb);	lua_setfield(L, -2, "aabb");

	lua_newtable(L);	// ����: obj flags
	{
		lua_pushboolean(L, o->IsPhysic());	lua_setfield(L, -2, "physic");
	}
	lua_pushboolean(L, o->IsDead());	lua_setfield(L, -2, "dead");
	lua_setfield(L, -2, "flags");	// ����: obj

#ifdef MAP_EDITOR
	if ( editor_state != editor::EDITOR_OFF )
	{
		lua_pushstring(L, o->editor.proto_name);	lua_setfield(L, -2, "proto");
		lua_pushboolean(L, o->editor.grouped);		lua_setfield(L, -2, "group");
		SCRIPT::PushVector(L, o->editor.creation_shift);	lua_setfield(L, -2, "creation_shift");
	}
#endif //MAP_EDITOR

	if(o->sprite)
	{
		lua_newtable(L);	// ����: obj sprite
		lua_pushboolean(L, o->sprite->IsMirrored());        lua_setfield(L, -2, "mirrored");
		lua_pushboolean(L, o->sprite->IsFixed());           lua_setfield(L, -2, "fixed");
		lua_pushboolean(L, o->sprite->IsVisible());         lua_setfield(L, -2, "visible");
		lua_pushnumber(L, o->sprite->z);                    lua_setfield(L, -2, "z");
		lua_pushboolean(L, o->sprite->IsAnimDone());        lua_setfield(L, -2, "animDone");
		lua_pushinteger(L, o->sprite->animsCount);          lua_setfield(L, -2, "animsCount");
		lua_pushstring(L, o->sprite->cur_anim.c_str());     lua_setfield(L, -2, "cur_anim");
		Animation* a = o->sprite->GetAnimation(o->sprite->cur_anim_num);
		lua_pushinteger(L, (a ? a->current : -1));          lua_setfield(L, -2, "currentFrame");
		lua_pushinteger(L, o->sprite->getCurrentFrame());   lua_setfield(L, -2, "frame");
		lua_pushinteger(L, o->sprite->frameWidth);          lua_setfield(L, -2, "frameWidth");
		lua_pushinteger(L, o->sprite->frameHeight);	        lua_setfield(L, -2, "frameHeight");
		lua_newtable(L);	//����: obj sprite color
		lua_pushnumber(L, o->sprite->color.r);              lua_rawseti(L, -2, 1);
		lua_pushnumber(L, o->sprite->color.g);              lua_rawseti(L, -2, 2);
		lua_pushnumber(L, o->sprite->color.b);              lua_rawseti(L, -2, 3);
		lua_pushnumber(L, o->sprite->color.a);              lua_rawseti(L, -2, 4);
		lua_setfield(L, -2, "color");  // ����: obj sprite
		
#ifdef MAP_EDITOR
		if ( editor_state != editor::EDITOR_OFF && o->sprite->tex != NULL )
		{
			lua_pushinteger(L, o->sprite->tex->frames->framesCount);	lua_setfield(L, -2, "frames");
		}
#endif //MAP_EDITOR
		lua_setfield(L, -2, "sprite");	// ����: obj
	}

	if(o->IsPhysic())
		PushPhysic(L, (ObjPhysic*)o);

	if (o->type == objPlayer)
		PushPlayer(L, (ObjPlayer*)o);

	if (o->type == objSpawner)
		PushSpawner(L, (ObjSpawner*)o);

	if (o->type == objRibbon)
		PushRibbon(L, (ObjRibbon*)o);

	// ����: obj
}

void PushSpawner(lua_State* L, ObjSpawner* o)
{
	// ����: obj
	lua_pushinteger(L, o->maximumEnemies);	lua_setfield(L, -2, "maximumEnemies");
	lua_pushinteger(L, o->enemySpawnDelay);	lua_setfield(L, -2, "enemySpawnDelay");
	lua_pushinteger(L, o->respawnDelay);	lua_setfield(L, -2, "respawnDelay");
	lua_pushinteger(L, o->size);			lua_setfield(L, -2, "enemySize");
	lua_pushstring(L, o->enemyType);		lua_setfield(L, -2, "enemyType");
	lua_pushinteger(L, o->respawn_dist);	lua_setfield(L, -2, "respawn_dist");
	lua_pushinteger(L, o->direction);		lua_setfield(L, -2, "direction");
}

void PushRibbon(lua_State* L, ObjRibbon* o)
{
	// ����: obj
	lua_pushnumber(L, o->getBL());	lua_setfield(L, -2, "bl");
	lua_pushnumber(L, o->getBR());	lua_setfield(L, -2, "br");
	lua_pushnumber(L, o->getBT());	lua_setfield(L, -2, "bt");
	lua_pushnumber(L, o->getBB());	lua_setfield(L, -2, "bb");
	SCRIPT::PushVector(L, o->k);	lua_setfield(L, -2, "k");
	lua_pushboolean(L, static_cast<int>(o->getUBL()));	lua_setfield(L, -2, "ubl");
	lua_pushboolean(L, static_cast<int>(o->getUBR()));	lua_setfield(L, -2, "ubr");
	lua_pushboolean(L, static_cast<int>(o->getUBT()));	lua_setfield(L, -2, "ubt");
	lua_pushboolean(L, static_cast<int>(o->getUBB()));	lua_setfield(L, -2, "ubb");
	lua_pushboolean(L, o->repeat_x);	lua_setfield(L, -2, "repeat_x");
	lua_pushboolean(L, o->repeat_y);	lua_setfield(L, -2, "repeat_y");
#ifdef MAP_EDITOR
	if ( editor_state != editor::EDITOR_OFF )
	{
		lua_pushboolean(L, o->editor.from_proto);	lua_setfield(L, -2, "from_proto");
	}
#endif //MAP_EDITOR
}

// ��������� � ������� �� ������� ����� ���� ����������� �������� �������
void PushPhysic(lua_State* L, ObjPhysic* o)
{
	// ����: obj
	if ( o->geometry != &o->rectangle )
	{
		SCRIPT::PushPolygon( L, *o->geometry ); lua_setfield(L, -2, "polygon");
	}
	lua_newtable(L);	// ����: obj phFlags
#ifdef MAP_EDITOR
	if ( editor_state != editor::EDITOR_OFF && !o->editor.really_physic )
	{
		lua_pushboolean(L, false);	lua_setfield(L, -2, "oneSided");
		lua_pushboolean(L, false);	lua_setfield(L, -2, "solid");
		lua_pushboolean(L, false);	lua_setfield(L, -2, "bulletCollideable");
		lua_pushboolean(L, false);	lua_setfield(L, -2, "dynamic");
	}
	else
#endif //MAP_EDITOR
	{
		lua_pushboolean(L, o->IsOneSide());	lua_setfield(L, -2, "oneSided");
		lua_pushboolean(L, o->IsSolid());	lua_setfield(L, -2, "solid");
		lua_pushboolean(L, o->IsBulletCollidable());	lua_setfield(L, -2, "bulletCollideable");
		lua_pushboolean(L, o->IsDynamic());	lua_setfield(L, -2, "dynamic");
	}
	lua_setfield(L, -2, "phFlags");	// ����: obj
	lua_pushinteger(L, o->solid_to); lua_setfield(L, -2, "solid_to");
	lua_newtable(L);
	if ( o->material != NULL )
	{
		lua_pushinteger(L, o->material->type);	lua_setfield(L, -2, "type");
		lua_pushnumber(L, o->material->bounce_bonus);	lua_setfield(L, -2, "bounce_bonus");
		lua_pushnumber(L, o->material->friction);	lua_setfield(L, -2, "friction");
	}
	lua_setfield(L, -2, "material");

	if (o->IsDynamic())
		PushDynamic(L, (ObjDynamic*)o);
}

// ��������� � ������� �� ������� ����� ���� ������������� ����������� �������� �������
void PushDynamic(lua_State* L, ObjDynamic* o)
{
	// ����: obj
	SCRIPT::PushVector(L, o->acc);				lua_setfield(L, -2, "acc");
	SCRIPT::PushVector(L, o->vel);				lua_setfield(L, -2, "vel");
	lua_pushnumber(L, o->walk_acc);				lua_setfield(L, -2, "walk_acc");
	SCRIPT::PushVector(L, o->gravity);			lua_setfield(L, -2, "gravity");
	lua_pushboolean(L, o->IsOnPlane()); lua_setfield(L, -2, "onPlane");
	lua_pushnumber(L, o->jump_vel);			lua_setfield(L, -2, "jump_vel");
	lua_pushnumber(L, o->max_x_vel);			lua_setfield(L, -2, "max_x_vel");
	lua_pushnumber(L, o->max_y_vel);			lua_setfield(L, -2, "max_y_vel");
	lua_pushboolean(L, o->movementDirectionX);	lua_setfield(L, -2, "movementDirectionX");

	lua_pushinteger(L, o->movement);			lua_setfield(L, -2, "movement");
	lua_pushinteger(L, o->activity);			lua_setfield(L, -2, "activity");

	if ( o->GetSuspectedPlane() )
		lua_pushinteger(L, o->GetSuspectedPlane()->id);
	else
		lua_pushinteger(L, 0);
	
	lua_setfield(L, -2, "suspected_plane");

	if ( o->type == objEnemy )
		PushCharacter(L, reinterpret_cast<ObjEnemy*>(o));
}

// ��������� � ������� �� ������� ����� ���� �������� ������� ���������
void PushCharacter(lua_State* L, ObjCharacter* o)
{
	// ����: obj
	lua_pushinteger(L, o->health);			lua_setfield(L, -2, "health");
	lua_pushinteger(L, o->gunDirection);	lua_setfield(L, -2, "gunDirection");
	lua_pushboolean(L, o->is_invincible);	lua_setfield(L, -2, "isInvincible");
}

// ��������� � ������� �� ������� ����� ���� �������� ������� ������
void PushPlayer(lua_State* L, ObjPlayer* o)
{
	// ����: obj
	PushCharacter(L, o);
	lua_pushboolean(L, o->controlEnabled);	lua_setfield(L, -2, "controlEnabled");
	lua_pushinteger(L, o->ammo);
	lua_setfield(L, -2, "ammo");
	if ( o->weapon )
	{
		lua_pushboolean(L, o->cur_weapon == o->alt_weapon);
		lua_setfield(L, -2, "altWeaponActive");
		lua_pushboolean(L, playerControl ? playerControl->current == o : false);
		lua_setfield(L, -2, "active");
		if (o->alt_weapon) lua_pushstring(L, o->alt_weapon->bullet_proto->name.c_str());
		else lua_pushnil(L);
		lua_setfield(L, -2, "altWeaponName");
	}
	else
	{
		lua_pushboolean(L, false);
		lua_setfield(L, -2, "altWeaponActive");
		lua_pushstring(L, "none");
		lua_setfield(L, -2, "altWeaponName");
	}
}

//////////////////////////////////////////////////////////////////////////

// ���������� �������, �������������� ����� ������ ������
int scriptApi::GetPlayer(lua_State* L)
{
	// TODO: CHECKGAME ����� �����. ������ ������������ ��������� loader.lua �����.
	// ������� �� ��� ����.
	//CHECKGAME;

	luaL_argcheck(L, lua_isnumber(L, 1) || lua_isnone(L, 1), 1, "player num or none");

	ObjPlayer* plr = NULL;
	if (playerControl)
	{
		if (lua_isnone(L, 1))
		{
			plr = playerControl->current;
		}
		else 
		{
			plr = playerControl->GetByNum(static_cast<int>(lua_tointeger(L, 1)));
		}
	}

	if (plr)
		PushObject(L, plr);
	else
	{
		lua_pushnil(L);
		//Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error no player");
	}

	return 1;
}

int scriptApi::GetPlayerCharacter(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1) || lua_isnone(L, 1), 1, "player num or none");
	if ( lua_isnone(L, 1) && playerControl && playerControl->current )
	{
		playerControl->current->pushUData(L);
		return 1;
	}
	if ( lua_isnumber(L, 1) )
	{
		int num = static_cast<int>(lua_tonumber(L, 1));
		if ( num > 0 && num <= static_cast<int>(players.size()) )	
		{
			Player* plr = players[static_cast<size_t>(num-1)];
			if ( plr && plr->current )
			{
				plr->current->pushUData(L);
				return 1;
			}
		}
	}
	lua_pushnil(L);
	return 1;
}

int scriptApi::GetPlayerNum(lua_State* L)
{
	CHECKGAME;

	if (playerControl)
	{
		lua_pushinteger(L, playerControl->current_num+1);
		return 1;
	}
	else
		lua_pushnil(L);

	return 1;
}

///

// WARNING! This can be dangerous, because there is no checking of passed userdata.
CUDataUser* getObjectHelper(lua_State* L, int idx)
{
	luaL_argcheck(L, lua_isnumber(L, idx) || lua_isuserdata(L, idx), idx, "Object id or userdata expected");

	if (lua_isnumber(L, idx))
	{
		UINT id = static_cast<UINT>(lua_tointeger(L, idx));
		GameObject* obj = GetGameObject(id);
		return obj;
		//if (obj)
		//{
		//	return obj;
		//}
		//else
		//{
		//	Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
		//		"GetObject: obj id=%d is not exist", id);
		//	return NULL;
		//}
	}
	else
	{
		CUData *ud = reinterpret_cast<CUData*>(lua_touserdata(L, idx));
		if (!ud)
			return NULL;
		return ud->getUser();
	}
}

///

// ���������� �������, �������������� ����� ������� ������
int scriptApi::GetObject(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	if (obj)
	{
		PushObject(L, obj);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"GetObject: there is no such object");
		return 0;
	}

	return 1;
}

int scriptApi::GetObjectUserdata(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Object id expected");

	UINT id = static_cast<UINT>(lua_tointeger(L, 1));
	GameObject* obj = GetGameObject(id);

	if (obj)
	{
		return obj->pushUData(L);
	}
	
	Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "GetObjectUserdata: error, no obj id=%d", id);
	return 0;
}

int scriptApi::GetCharHealth(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	if (obj && (obj->type == objPlayer || obj->type == objEnemy))
	{
		lua_pushinteger(L, ((ObjCharacter*)obj)->health);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"GetCharHealth: obj id=%d does not exist", lua_tointeger(L, 1));
		lua_pushnil(L);
	}
	return 1;
}

int scriptApi::SetCharHealth(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "health");
	if (obj && (obj->type == objPlayer || obj->type == objEnemy))
	{
		((ObjCharacter*)obj)->health = static_cast<short>(lua_tointeger(L, 2));
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"GetCharHealth: obj id=%d does not exist", lua_tointeger(L, 1));
	}
	return 0;
}

//////////////////////////////////////////////////////////////////////////

int scriptApi::SetObjDead(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	if (obj)
	{
		obj->SetDead();
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"Error inf SetObjDead. Obj does not exist");
	}

	return 0;
}

/// ������ ���� sleep �������
/// @param   id - id �������
/// @param   [sleep] - ����� ��������� �����. ���� none - ����������� �������
/// @return  none
int scriptApi::SetObjSleep(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isboolean(L, 2) || lua_isnone(L, 2), 2, "Sleep state or none expected");

	if (obj)
	{
		bool sleep;
		if (lua_isboolean(L, 2)) 
			sleep = lua_toboolean(L, 2) != 0;
		else
			sleep = !obj->IsSleep();
		
		
		if (sleep)
			obj->SetSleep();
		else
			obj->ClearSleep();
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"SetObjSleep. Obj does not exist");
	}

	return 0;
}

// �������� ���������� �������� �������
int scriptApi::SetObjPos(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "X coord expected");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Y coord expected");
	luaL_argcheck(L, lua_isnumber(L, 4)||lua_isnil(L, 4)||lua_isnone(L, 4), 4, "Z coord expected");
	luaL_argcheck(L, lua_isboolean(L, 5)||lua_isnil(L, 5)||lua_isnone(L, 5), 5, "editor flag expected");

	if (obj)
	{	
		float x = static_cast<float>(lua_tonumber(L, 2));
		float y = static_cast<float>(lua_tonumber(L, 3));
		if ( lua_isboolean(L, 5) && (lua_toboolean(L, 5) != 0) )
		{
			obj->aabb.p.x = x + obj->aabb.W;
			obj->aabb.p.y = y + obj->aabb.H;
		}
		else
		{
			obj->aabb.p.x = x;
			obj->aabb.p.y = y;
		}
		if ( lua_isnumber(L, 4) && obj->sprite )
			obj->sprite->z = (float)lua_tonumber(L, 4);

		// TODO: ���-�� ����� ��� ������ ����. �� ����� ����� ��� ���������� � ���.
#ifdef MAP_EDITOR
		if ( editor_state == editor::EDITOR_OFF && obj->IsPhysic() )
#else
		if (obj->IsPhysic())
#endif //MAP_EDITOR
		{
			if ( !((ObjPhysic*)obj)->IsDynamic() )
			{
				UpdateSAPObject( ((ObjPhysic*)obj)->sap_handle, obj->aabb );
			}
		}
#ifdef SELECTIVE_RENDERING
		selectForRenederUpdate(obj);
#endif
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"SetObjPos: Obj does not exist");
	}

	return 0;
}

int scriptApi::SetObjPolygon(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_istable(L, 2), 2, "polygon table expected");

	if (obj && obj->IsPhysic())
	{
		ObjPhysic* op = static_cast<ObjPhysic*>(obj);
		CPolygon* poly = SCRIPT::GetPolygonField(L, 2);
		if ( poly ) 
		{
			op->SetPolygon( poly );
			lua_pushboolean( L, true );
		}
		else lua_pushboolean( L, false );
	}
	else lua_pushboolean( L, false );

	return 1;
}

int scriptApi::SetObjRectangle(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "width expected");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "height expected");

	if (obj)
	{
		obj->Resize( static_cast<float>(lua_tonumber(L, 2)), static_cast<float>(lua_tonumber(L, 3)) );
	}
	
	return 0;
}

int scriptApi::SetObjSpriteRenderMethod(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "render_method (constants.rsm*");

	if (obj && obj->sprite)
	{
		obj->sprite->setRenderMethod(static_cast<RenderSpriteMethod>(lua_tointeger(L, 2)));
	}

	return 0;
}

int scriptApi::SetObjSpriteBlendingMode(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "blending mode (constants.bm*");

	if (obj && obj->sprite)
	{
		obj->sprite->blendingMode = static_cast<BlendingMode>(lua_tointeger(L, 2));
	}

	return 0;
}

int scriptApi::SetDynObjGravity(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "X acceleration expected");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Y acceleration expected");

	if (obj && obj->IsPhysic() && ((ObjPhysic*)obj)->IsDynamic())
	{
		((ObjDynamic*)obj)->gravity.x = (float)lua_tonumber(L, 2);
		((ObjDynamic*)obj)->gravity.y = (float)lua_tonumber(L, 3);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"SetDybObjGravity: Obj does not exist or is not dynamic");
	}

	return 0;
}

// �������� ��������� ������������� �������� �������
int scriptApi::SetDynObjAcc(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "X acceleration expected");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Y acceleration expected");

	if (obj && obj->IsPhysic() && ((ObjPhysic*)obj)->IsDynamic())
	{
		((ObjDynamic*)obj)->acc.x = (float)lua_tonumber(L, 2);
		((ObjDynamic*)obj)->acc.y = (float)lua_tonumber(L, 3);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"SetDynObjAcc:  Obj does not exist or is not dynamic");
	}

	return 0;
}

// �������� �������� ������������ �������� �������
int scriptApi::SetDynObjVel(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "X velocity expected");
	luaL_argcheck(L, lua_isnumber(L, 3)||lua_isnil(L, 3)||lua_isnone(L, 3), 3, "Y velocity expected");
	
	if (obj && obj->IsPhysic() && ((ObjPhysic*)obj)->IsDynamic())
	{
		((ObjDynamic*)obj)->vel.x = (float)lua_tonumber(L, 2);
		if (lua_isnumber(L, 3))
		{
			((ObjDynamic*)obj)->vel.y = (float)lua_tonumber(L, 3);
		}
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"SetDynObjVel:  Obj does not exist or is not dynamic");
	}
	
	return 0;
}

// ��������/��������� �������� ������
int scriptApi::EnablePlayerControl(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isboolean(L, 1), 1, "Boolean expected");
	luaL_argcheck(L, lua_isnumber(L, 2)||lua_isnil(L, 2)||lua_isnone(L, 2), 2, "Player num");

	if ( lua_isnumber(L, 2) )
	{
		size_t num = static_cast<size_t>(lua_tointeger(L, 2)) - 1;
		if ( num >= players.size() )
			return 0;
		if ( !players[num]->current )
			return 0;
		players[num]->current->controlEnabled = lua_toboolean(L, 1) != 0;
	}
	else
	{
		bool state = lua_toboolean(L, 1) != 0;
		for ( vector<Player*>::iterator it = players.begin(); it != players.end(); it++ )
		{
			if ((*it)->current)
			{
				(*it)->current->controlEnabled = state;
			}
		}
	}

	return 0;
}

// ��������/��������� �������� ������
int scriptApi::GetPlayerControlState(lua_State* L)
{
	CHECKGAME;

	if (playerControl && playerControl->current)
	{
		lua_pushboolean(L, playerControl->current->controlEnabled);
	}

	return 1;
}

// ������������� ��� �������� ������������� �������� �������
int scriptApi::SetDynObjMovement(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Movement type expected");

	if (obj && obj->IsPhysic() && ((ObjPhysic*)obj)->IsDynamic())
	{
		((ObjDynamic*)obj)->movement = (ObjectMovementType)lua_tointeger(L, 2);
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"SetDynObjMovement: Obj does not exist");
	}

	return 0;
}

// ������������� ����������� �������� ������������� �������� �������
int scriptApi::SetDynObjMoveDirX(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isboolean(L, 2), 2, "Movement type expected");

	if (obj && obj->IsPhysic() && ((ObjPhysic*)obj)->IsDynamic())
	{
		((ObjDynamic*)obj)->movementDirectionX = lua_toboolean(L, 2) != 0;
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"SetDynObjMovement: Obj does not exist or is not dynamic");
	}

	return 0;
}

int scriptApi::SetObjSpriteMirrored(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isboolean(L, 2), 2, "Boolean expected");

	if (obj)
	{
		if (lua_toboolean(L, 2))
			obj->SetFacing( true );
		else
			obj->SetFacing( false );
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"SetObjSpriteMirrored:  Obj does not exist or is not dynamic");
	}

	return 0;
}

int scriptApi::SetObjSpriteAngle(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Angle expected");

	if (obj)
	{
		obj->sprite->setAngle((float)lua_tonumber(L, 2));
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"SetObjSpriteAngle:  Obj does not exist or is not dynamic");
	}

	return 0;
}

int scriptApi::SetObjSpriteColor(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_istable(L, 2)||lua_isnil(L, 2), 2, "Color table or nil expected");
	luaL_argcheck(L, lua_isnumber(L, 3)||lua_isnil(L, 3)||lua_isnone(L, 3), 3, "Overlay number expected");

	RGBAf col; SCRIPT::GetColorFromTable(L, 2, col);

	if (obj && obj->sprite)
	{
		if ( lua_isnumber(L, 3) )
		{
			uint32_t over = (uint32_t)lua_tointeger(L, 3);
			SCRIPT::GetColorFromTable(L, 2, obj->sprite->ocolor[over]);
		}
		else 
		{
			luaL_argcheck(L, lua_istable(L, 2), 2, "Color table expected");
			SCRIPT::GetColorFromTable(L, 2, obj->sprite->color);
		}
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"SetObjSpriteColor:  Obj does not exist or is not dynamic");
	}

	return 0;
}

int scriptApi::SetObjAnim(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isstring(L, 2)||lua_isnumber(L, 2), 2, "String or number expected");
	luaL_argcheck(L, lua_isboolean(L, 3), 3, "Boolean expected");

	if (obj)
	{
		if ( lua_isnumber(L, 2) )
		{
			UINT tilenum = static_cast<UINT>(lua_tointeger(L, 2));
			obj->sprite->SetCurrentFrame(tilenum);
			obj->sprite->animsCount = 0;
			obj->type = objTile;
			obj->aabb.W = ((float)obj->sprite->frameWidth) / 2.0f;
			obj->aabb.H = ((float)obj->sprite->frameHeight) / 2.0f;
			lua_pushboolean( L, true );
		}
		else

		{
			lua_pushboolean( L, obj->SetAnimation(string(lua_tostring(L, 2)), lua_toboolean(L, 3) != 0 ));
		}
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"SetObjAnim:  Obj does not exist or is not dynamic");
		lua_pushboolean( L, false );
	}


	return 1;
}

int scriptApi::SetObjProcessor(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));	
	
	if (obj)
	{
		SCRIPT::RegProc(L, &obj->scriptProcess, 2);		
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"SetObjProcessor:  Obj does not exist or is not dynamic");
	}

	return 0;
}

int scriptApi::GetObjProcessor(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));	
	
	if ( obj )
	{
		SCRIPT::GetFromRegistry(L, obj->scriptProcess );
	}
	else
	{
		lua_pushnil( L );
	}

	return 1;
}

int scriptApi::SetDynObjJumpVel(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "jump vel expected");

	if (obj)
	{
		if (obj->IsPhysic() && ((ObjPhysic*)obj)->IsDynamic())
		{
			((ObjDynamic*)obj)->jump_vel = (float)lua_tonumber(L, 2);
		}
	}
	return 0;
}

int scriptApi::SetDynObjShadow(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2) || lua_isnil(L, 2), 2, "width or nil expected");

	if (obj)
	{
		if (obj->IsPhysic() && ((ObjPhysic*)obj)->IsDynamic())
		{
			if (lua_isnil(L, 2))
				((ObjDynamic*)obj)->drops_shadow = false;
			else
			{
				((ObjDynamic*)obj)->drops_shadow = true;
				((ObjDynamic*)obj)->shadow_width = (float)lua_tonumber(L, 2);
			}
		}
	}
	return 0;
}

int scriptApi::GetDynObjShadow(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	if (obj)
	{
		if (obj->IsPhysic() && ((ObjPhysic*)obj)->IsDynamic())
		{
			if (((ObjDynamic*)obj)->drops_shadow)
			{
				lua_pushnumber(L, ((ObjDynamic*)obj)->shadow_width);
				return 1;
			}


		}
	}
	lua_pushnil(L);
	return 1;
}

int scriptApi::GroupObjects(lua_State* L)
{
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Object id expected");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Second object id expected");

	::GroupObjects( static_cast<UINT>(lua_tointeger(L, 1)), 
					static_cast<UINT>(lua_tointeger(L, 2)) );

	return 0;
}

int scriptApi::SetRayPos(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "x0");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "y0");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "x1");
	luaL_argcheck(L, lua_isnumber(L, 5), 5, "y1");

	if (obj)
	{
		if (obj->type == objRay)
		{
			CRay r(
				(float)lua_tonumber(L, 2), (float)lua_tonumber(L, 3), 
				(float)lua_tonumber(L, 4), (float)lua_tonumber(L, 5) );
			((ObjRay*)obj)->setRay(r);
		}
	}
	return 0;
}

int scriptApi::SetRaySearchDistance(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Distance expected");

	if (obj)
	{
		if (obj->type == objRay)
		{
			((ObjRay*)obj)->searchDistance = (float)lua_tonumber(L, 2);
		}
	}
	return 0;
}

int scriptApi::SpawnerLaunch(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));

	if (obj && obj->type == objSpawner)
	{
		lua_pushboolean(L, static_cast<ObjSpawner*>(obj)->ScriptLaunch());
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning,
			"SpawnerLaunch: Obj does not exist or is not spawner");
		lua_pushnil(L);
	}
	return 1;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

int scriptApi::CreateColorBox(lua_State* L)
{
	CHECKGAME;
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be coordinate X1");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be coordinate Y1");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be coordinate X2");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "Must be coordinate Y2");
	luaL_argcheck(L, lua_isnumber(L, 5), 5, "Must be coordinate Z");
	luaL_argcheck(L, lua_istable(L, 6), 6, "Must be table color");
	
	float x1 = (float)lua_tonumber(L, 1);
	float y1 = (float)lua_tonumber(L, 2);
	float x2 = (float)lua_tonumber(L, 3);
	float y2 = (float)lua_tonumber(L, 4);
	float z = (float)lua_tonumber(L, 5);
	RGBAf col; SCRIPT::GetColorFromTable(L, 6, col);

	lua_pop(L, lua_gettop(L));	// ����:

	GameObject* obj = ::CreateColorBox(CAABB(x1, y1, x2, y2), z, col);

	lua_pushnumber(L, obj->id);
	return 1;
}

int scriptApi::CreateMap(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_istable(L,1), 1, "Map table expected");
	luaL_argcheck(L, lua_isnumber(L,2)||lua_isnil(L,2)||lua_isnone(L,2), 2, "Map version number expected" );

	int version = 0;
	if ( lua_isnumber(L, 2 ) ) version = static_cast<int>( lua_tointeger(L, 2) );
	int type = -1;
	float x1, y1, x2, y2, z;
	bool b1, b2, b3, b4;
	UINT count, delay, dir, ssize, sdist, spawnLimit = (UINT)-1;
	int tilenum;
	RGBAf col;
	//UINT num = 1;
	const char* proto_name;
	UINT respawnDelay = 0;
	bool skip = false;
	
	// ����: table
	const UINT size = static_cast<UINT>(lua_objlen(L, 1));

	GameObject* obj1 = NULL;
	//GameObject* obj2 = NULL;
	ObjPhysic* op = NULL;
	vector<int> secretIDs;

	float difficulty = 1.0f;
	lua_getglobal( L, "difficulty" );
	if ( lua_isnumber( L, - 1 ) )
	{
		difficulty = static_cast<float>(lua_tonumber( L, - 1 ));
	}
	lua_pop( L, 1 );

	for (UINT i = 1; i <= size; i++) 
	{
		skip = false;
		// �������� � ����� i-��� ������� �������
		lua_pushnumber(L, i);	// ����: table i
		lua_gettable(L, 1);	// ����: table table[i]		

		luaL_argcheck(L, lua_istable(L,1), 1, "Something wrong in map table");
		lua_rawgeti(L, -1, 1);		// ����: table table[i]	type
		type = static_cast<int>(lua_tointeger(L, -1));
		lua_pop(L, 1);				// ����: table table[i]
		lua_pushstring( L, "min_difficulty" );
		lua_rawget(L, -2);
		if (lua_isnumber(L, -1))
		{
			if ( difficulty < static_cast<float>(lua_tonumber( L, - 1 )) )
			{
				skip = true;
			}
		}
		lua_pop(L, 1);
		lua_pushstring( L, "max_difficulty" );
		lua_rawget(L, -2);
		if (lua_isnumber(L, -1))
		{
			if ( difficulty > static_cast<float>(lua_tonumber( L, - 1 )) )
			{
				skip = true;
			}
		}
		lua_pop(L, 1);
		if ( !skip )
		{
			switch ( type )
			{
				case otSprite: //������
						lua_rawgeti(L, -1, 2);				// ����: table table[i] proto_name
						proto_name = lua_tostring(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 3);				// ����: table table[i] x1 
						x1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 4);				// ����: table table[i] y1
						y1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);						// ����: table table[i]
						obj1 = ::CreateSprite(proto_name, Vector2(x1, y1), false, NULL, !(version>0));
						lua_rawgeti(L, -1, 5);
						if ( lua_isnumber(L, -1) && obj1 && obj1->sprite )
						{
							z = (float)lua_tonumber(L, -1);
							obj1->sprite->z = z;
						}
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 6);
						if ( lua_isstring( L, -1 ) && obj1 )
						{
							obj1->SetAnimation( lua_tostring( L, -1 ), false );
						}
						lua_pop(L, 1);
						break;
				case otPolygon: //�������������
					{
						lua_rawgeti(L, -1, 2);				
						CPolygon* poly = SCRIPT::GetPolygonField( L, -1 );
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 3);			
						x1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 4);				
						y1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						//obj1 = ::CreateDummySprite( true );	//Crashes the game
						obj1 = ::CreateSprite( "phys-empty", Vector2(x1, y1), false, NULL, false );
						ObjPhysic* op = static_cast<ObjPhysic*>(obj1);
						if ( op )
						{
							lua_pushstring( L, "one_sided" );
							lua_rawget(L, -2);
#ifdef MAP_EDITOR
							if ( editor_state != editor::EDITOR_OFF )
							{
								op->editor.editor_color = RGBAf( 0.8f, 0.2f, 0.2f, 0.5f );
							}
#endif //MAP_EDITOR
							if (lua_isboolean(L, -1))
							{
								if ( lua_toboolean(L, -1) != 0 )
								{
									op->SetOneSide();
#ifdef MAP_EDITOR
									if ( editor_state != editor::EDITOR_OFF )
									{
										op->editor.editor_color = RGBAf( 0.2f, 0.8f, 0.2f, 0.5f );
									}
#endif //MAP_EDITOR
								}
							}
							lua_pop(L, 1);
							op->aabb.p.x = x1;
							op->aabb.p.y = y1;
							op->SetPolygon( poly );
						}
						else DELETESINGLE( poly );
						break;
					}
				case otTile: //������
						lua_rawgeti(L, -1, 2);				// ����: table table[i] proto_name

						proto_name = lua_tostring(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 3);				// ����: table table[i] x1 
						x1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 4);				// ����: table table[i] y1
						y1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 5);				// ����: table table[i] tilenum
						tilenum = static_cast<int>(lua_tointeger(L, -1));
						obj1 = ::CreateSprite(proto_name, Vector2(x1, y1), false, NULL, !(version>0));
						if (obj1 && obj1->sprite)
						{
							obj1->sprite->SetCurrentFrame(tilenum);
							obj1->sprite->animsCount = 0;
							obj1->aabb.W  = (float)obj1->sprite->frameWidth / 2.0f;

							obj1->aabb.H  = (float)obj1->sprite->frameHeight / 2.0f;
						}
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 6);				// ����: table table[i] z
						if ( lua_isnumber(L, -1) && obj1 && obj1->sprite )
						{
							z = (float)lua_tonumber(L, -1);
							obj1->sprite->z = z;
						}
						if (obj1)
						{
							if ( version > 0 ) obj1->aabb.p = Vector2( x1, y1 );
							obj1->type = objTile;
						}
						lua_pop(L, 1);						// ����: table table[i]
						break;
				case otPlayer: //�����
						lua_rawgeti(L, -1, 2);
						proto_name = lua_tostring(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 3);
						x1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 4);
						y1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						if ( players.size() < KEY_SETS_NUMBER )
						{
							playerControl = new Player;
							playerControl->input = new Input(players.size());
							inpmgr.RegisterInput( playerControl->input, players.size() );
							playerControl->Create(proto_name, Vector2(x1, y1), NULL, !(version>0));
							players.push_back(playerControl);
						}
						break;
				case otEnemy: //���������
						lua_rawgeti(L, -1, 2);
						proto_name = lua_tostring(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 3);
						x1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 4);
						y1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						obj1 = ::CreateEnemy(proto_name, Vector2(x1, y1), NULL, !(version>0));
						lua_rawgeti(L, -1, 5);
						if ( obj1 && obj1->sprite && lua_isnumber(L, -1) )
							obj1->sprite->z = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						if ( version > 1 && version < 3 )
						{
							lua_pushstring( L, "mirrored" );
							lua_rawget(L, -2);
							if (lua_isboolean(L, -1))
							{
								if ( lua_toboolean(L, -1) != 0 && obj1)
									obj1->SetFacing(true);
							}
							lua_pop(L, 1);
						}
						break;
				case otSpawner:
						lua_rawgeti(L, -1, 2);
						proto_name = lua_tostring(L, -1);
						lua_pop(L, 1);

						lua_rawgeti(L, -1, 3);
						x1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 4);
						y1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 5);
						count = static_cast<UINT>(lua_tointeger(L, -1));
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 6);
						delay = static_cast<UINT>(lua_tointeger(L, -1));
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 7);
						dir = static_cast<UINT>(lua_tointeger(L, -1));
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 8);
						if ( lua_isnumber(L, -1) )
						{
							ssize = static_cast<UINT>(lua_tointeger(L, -1));
							lua_pop(L, 1);
							lua_rawgeti(L, -1, 9);
							if ( lua_isnumber(L, -1) )
							{
								sdist = static_cast<UINT>(lua_tointeger(L, -1));
								lua_pop(L, 1);
								lua_rawgeti(L, -1, 10);
								if ( lua_isnumber(L, -1) )
								{
									spawnLimit = static_cast<UINT>(lua_tointeger(L, -1));
								}
							}
							else
								sdist = ssize;
						}
						else
						{
							ssize = 0;
							sdist = 0;
						}
						lua_pop(L, 1);
						lua_pushstring( L, "respawn_delay" );
						lua_rawget(L, -2);
						if (lua_isnumber(L, -1))
						{
							respawnDelay = lua_tointeger(L, -1);
						}
						lua_pop(L, 1);
						::CreateSpawner( Vector2(x1, y1), proto_name, count, delay, (directionType)dir, ssize, sdist, respawnDelay, spawnLimit );
						break;
				case otBox: //�������
						lua_rawgeti(L, -1, 2);
						x1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 3);
						y1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 4);
						x2 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 5);
						y2 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 6);
						z = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 7);
						SCRIPT::GetColorFromTable(L, -1, col);
						lua_pop(L, 1);
						::CreateColorBox(CAABB(x1, y1, x2, y2), z, col);
						break;
				case otGroup: //������
						lua_rawgeti(L, -1, 2);
						proto_name = lua_tostring(L, -1);
						lua_pop(L,1);
						lua_rawgeti(L, -1, 3);
						x1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 4);
						y1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 5);
						x2 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 6);
						y2 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						obj1 = ::CreateSprite(proto_name, Vector2(x1, y1), false, NULL, !(version>0));
						if (obj1)
						{
							obj1->Resize( x2-x1, y2-y1 );
							obj1->aabb.p = Vector2( (x1+x2)/2.0f, (y1+y2)/2.0f );
#ifdef MAP_EDITOR
							if ( editor_state != editor::EDITOR_OFF )
							{
								obj1->editor.grouped = true;
								obj1->editor.creation_shift = Vector2(0, 0);
							}
#endif //MAP_EDITOR
						}
						break;
				case otItem: //�������
						lua_rawgeti(L, -1, 2);
						proto_name = lua_tostring(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 3);
						x1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 4);
						y1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						obj1 = ::CreateItem(proto_name, Vector2(x1, y1), "init", !(version>0));
						if ( version > 0 && obj1) obj1->aabb.p = Vector2( x1, y1 );
						break;
				case otSecret: //��������� ����
						lua_rawgeti(L, -1, 2);				// ����: table table[i] proto_name
						proto_name = lua_tostring(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 3);				// ����: table table[i] x1 
						x1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 4);				// ����: table table[i] y1
						y1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);						// ����: table table[i]
						obj1 = ::CreateSprite(proto_name, Vector2(x1, y1), false, NULL, !(version>0));
						if (obj1) 
						{
							secretIDs.push_back(obj1->id);
							if ( version > 0 ) obj1->aabb.p = Vector2( x1, y1 );
						}
						break;
				case otRibbon:
					{
						lua_rawgeti(L, -1, 2);
						proto_name = lua_tostring(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 3);
						x1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 4);
						y1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 5);
						x2 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 6);
						y2 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 7);
						z = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 8);
						b1 = lua_toboolean(L, -1) != 0;
						lua_pop(L, 1);
						ObjRibbon* rib = CreateRibbon( proto_name, Vector2(x1, y1), Vector2(x2, y2), z, b1);
						lua_rawgeti(L, -1, 9);
						x1 = 0;
						if ( (b1 = (lua_isnumber(L, -1) != 0)) == true ) x1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 10);
						y1 = 0;
						if ( (b2 = (lua_isnumber(L, -1) != 0)) == true ) y1 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 11);
						x2 = 0;
						if ( (b3 = (lua_isnumber(L, -1) != 0)) == true ) x2 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 12);
						y2 = 0;

						if ( (b4 = (lua_isnumber(L, -1) != 0)) == true ) y2 = (float)lua_tonumber(L, -1);
						lua_pop(L, 1);
						if (rib)
						{
							rib->setBounds( x1, y1, x2, y2 );
							rib->setBoundsUsage( b1, b2, b3, b4 );
						}
						lua_rawgeti(L, -1, 13);
						b1 = lua_toboolean(L, -1) != 0;
						lua_pop(L, 1);
						lua_rawgeti(L, -1, 14);
						b2 = lua_toboolean(L, -1) != 0;
						lua_pop(L, 1);
						if (rib) 
							rib->setRepitition( b1, b2 );
						break;
					}
				case otNone: //�� ���� ������ ��������� - ������
						break;
				default:
						Log(DEFAULT_LOG_NAME, logLevelWarning, "Wrong object type in map table");
						return 0;
			}
			if ( version >= 3 && obj1 && obj1->sprite )
			{
				lua_pushstring( L, "mirrored" );
				lua_rawget(L, -2);
				if (lua_isboolean(L, -1))
				{
					if ( lua_toboolean(L, -1) != 0 )
						obj1->SetFacing(true);
				}
				lua_pop(L, 1);
			}
			if ( version >= 3 && obj1 && obj1->IsPhysic() )
			{
				lua_pushstring( L, "solid_to" );
				lua_rawget(L, -2);
				if (lua_isnumber(L, -1))
				{
					op = static_cast<ObjPhysic*>(obj1);
					op->solid_to = static_cast<BYTE>(lua_tointeger(L, -1));
				}
				lua_pop(L, 1);
			}
		}
		lua_pop(L,1);		// ����: table

	}
	if ( secretIDs.size() > 0 )
	{
		lua_newtable(L);
		for ( size_t i = 0; i < secretIDs.size(); i ++ )
		{
			lua_pushinteger(L, secretIDs[i]);
			lua_rawseti(L, -2, static_cast<int>(i+1) );
		}
	}
	else lua_pushnil(L);
	lua_setglobal(L, "secrets");
	return 0;
}

int scriptApi::CreateSprite(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be proto name");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be coordinate X");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be coordinate Y");
	luaL_argcheck(L, lua_isstring(L, 4) || lua_isnil(L, 4) || lua_isnone(L, 4), 4, "Must be start_anim");

	int num = lua_gettop ( L );
	bool isFixed = false;
	float z = 0;
	bool override_z = false;
	const char* start_anim = lua_tostring(L, 4);

	if (num == 5)
	{
		if (lua_isboolean(L, 5))
		{
			isFixed = lua_toboolean(L, 5) != 0;
		}
		else if (lua_isnumber(L, 5))
		{
			z = (float)lua_tonumber(L, 5);
			override_z = true;
		}
		else
			luaL_argcheck(L, false, 5, "Boolean or number expected");
	}
	else if (num >= 6)
	{
		luaL_argcheck(L, lua_isnumber(L, 5), 5, "Number expected");
		luaL_argcheck(L, lua_isboolean(L, 6), 6, "Boolean expected");

		z = (float)lua_tonumber(L, 5);
		isFixed = lua_toboolean(L, 6) != 0;
		override_z = true;
	}

	const char* proto_name = lua_tostring(L, 1);
	float x = (float)lua_tonumber(L, 2);
	float y = (float)lua_tonumber(L, 3);

	//lua_pop(L, num);	// ����: Why?!

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Creating sprite %s", proto_name);

	GameObject* obj = NULL;
	obj = ::CreateSprite(proto_name, Vector2(x, y), isFixed, start_anim);
	if (obj && obj->id)
	{
		if (obj->sprite)
		{


			if (override_z)
				obj->sprite->z = z;
		}

		lua_pushnumber(L, obj->id);	// ����: obj->id
	}
	else
	{
		lua_pushnil(L);				// ����: nil
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error creating sprite");
	}

	return 1;
}

int scriptApi::CreateEffect(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be proto name");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be coordinate X");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be coordinate Y");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "Must be parent object");
	luaL_argcheck(L, lua_isnumber(L, 5), 5, "Must be mount point number");

	const char* proto_name = lua_tostring(L, 1);
	float x = (float)lua_tonumber(L, 2);
	float y = (float)lua_tonumber(L, 3);
	GameObject* parent = GetGameObject( (UINT)lua_tointeger(L, 4) );
	int num = static_cast<int>(lua_tointeger(L, 5));

	if (parent)
	{
		if (!(parent->IsPhysicType() && static_cast<ObjPhysic*>(parent)->IsDynamic()))
		{
			Log(DEFAULT_LOG_NAME, logLevelError, "CreateEffect error: parent is not dynamic");
			parent = NULL;
		}
	}

	GameObject* obj = NULL;
	obj = ::CreateEffect(proto_name, false, (ObjDynamic*)parent, static_cast<USHORT>(num), Vector2(x,y));
	if (obj && obj->id)
	{
		lua_pushnumber(L, obj->id);	// ����: obj->id
	}
	else
	{
		lua_pushnil(L);				// ����: nil
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error creating sprite");
	}

	return 1;
}

int scriptApi::CreateGroup(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be proto name");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be x1");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be y1");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "Must be x2");
	luaL_argcheck(L, lua_isnumber(L, 5), 5, "Must be y2");
	luaL_argcheck(L, lua_isnumber(L, 6), 6, "Must be z");
	luaL_argcheck(L, lua_isboolean(L, 7), 7, "Must be activity flag");

	GameObject* obj = NULL;
	float x1 = (float)lua_tonumber(L, 2);
	float y1 = (float)lua_tonumber(L, 3);
	float x2 = (float)lua_tonumber(L, 4);
	float y2 = (float)lua_tonumber(L, 5);
	if ( lua_toboolean(L, 7) == 0 )
		obj = ::CreateSprite(lua_tostring(L, 1), Vector2(0, 0), false, NULL, CAABB(x1, y1, x2, y2));
	else
		obj = ::CreateItem(lua_tostring(L, 1), Vector2(0, 0), NULL, CAABB(x1, y1, x2, y2));
	if (obj)
	{
		if (obj->sprite)
		{
			obj->sprite->z = (float)lua_tonumber(L, 6);
		}

		if (obj->id != 0)
			lua_pushnumber(L, obj->id);	// ����: obj->id
		else
			lua_pushnil(L);
	}
	else
	{
		lua_pushnil(L);				// ����: nil
	}

	return 1;
}

int scriptApi::CreateWaypoint(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be coordinate X");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be coordinate Y");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be size X");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "Must be size Y");
	luaL_argcheck(L, lua_isfunction(L, 5)|lua_isnil(L,5)|lua_isnone(L,5), 5, "Must be reach event handler");
	
	Vector2 coord = Vector2( (float)lua_tonumber(L, 1), (float)lua_tonumber(L, 2) );
	Vector2 size = Vector2( (float)lua_tonumber(L, 3), (float)lua_tonumber(L, 4) );
	
	ObjWaypoint* obj = ::CreateWaypoint( coord, size );
	if (obj)
	{
		if ( lua_isfunction(L, 5) )
			SCRIPT::RegProc(L, &obj->reach_event, 5);
		lua_pushnumber(L, obj->id);
	}
	else
	{
		lua_pushnil(L);	
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error creating waypoint");
	}

	return 1;
}

int scriptApi::CreateSpecialWaypoint(lua_State* L)
{
	CHECKGAME;


	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be coordinate X");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be coordinate Y");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be size X");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "Must be size Y");
	luaL_argcheck(L, lua_isnumber(L, 5) || lua_isnil(L, 5) || lua_isnone(L, 5), 5, "Must be waypoint number");
	luaL_argcheck(L, lua_isfunction(L, 6) || lua_isnil(L, 6) || lua_isnone(L, 6), 6, "Must be reach event handler");
	
	Vector2 coord = Vector2( (float)lua_tonumber(L, 1), (float)lua_tonumber(L, 2) );
	Vector2 size = Vector2( (float)lua_tonumber(L, 3), (float)lua_tonumber(L, 4) );
	
	ObjWaypoint* obj = ::CreateWaypoint( coord, size );
	if ( obj )
	{
		UINT wp_id;
		if ( lua_isnumber(L, 5) )
		{
			wp_id = static_cast<UINT>(lua_tointeger(L, 5));
			::AddWaypoint(obj, wp_id);
		}
		else wp_id = ::AddWaypoint(obj);
		if ( lua_isfunction(L, 6) )
			SCRIPT::RegProc(L, &obj->reach_event, 6);

		lua_pushinteger(L, obj->id);
		lua_pushinteger(L, wp_id);
	}
	else
	{
		lua_pushnil(L);
		lua_pushnil(L);
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error creating waypoint");
	}

	return 2;
}


int scriptApi::CreateItem(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be proto name");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be coordinate X");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be coordinate Y");
	luaL_argcheck(L, lua_isnumber(L, 4) || lua_isnil(L, 4) || lua_isnone(L, 4)  , 4, "Must be start_anim");

	const char* proto_name = lua_tostring(L, 1);
	float x = (float)lua_tonumber(L, 2);
	float y = (float)lua_tonumber(L, 3);
	const char* start_anim = lua_tostring(L, 4);

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Creating item %s", proto_name);

	int result = -1;
	GameObject* obj = NULL;

	obj = ::CreateItem(proto_name, Vector2(x, y), start_anim);

	if (obj)
	{
		result = obj->id;
	}
	else
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error creating item");
	}

	if (result == -1)
	{
		lua_pushnil(L);				// ����: nil
	}
	else
	{
		lua_pushnumber(L, result);	// ����: result
	}
	return 1;
}

int scriptApi::CreateEnvironment(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be proto name");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be coordinate X");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be coordinate Y");

	const char* proto_name = lua_tostring(L, 1);
	float x = (float)lua_tonumber(L, 2);
	float y = (float)lua_tonumber(L, 3);

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Creating environment %s", proto_name);

	int result = -1;
	GameObject* obj = NULL;

	obj = ::CreateEnvironment(proto_name, Vector2(x, y));

	if (obj)
	{
		result = obj->id;
	}
	else
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error creating environment");
	}

	if (result == -1)
	{
		lua_pushnil(L);				// ����: nil
	}
	else
	{
		lua_pushnumber(L, result);	// ����: result
	}
	return 1;
}

int scriptApi::CreatePlayer(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be proto name");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be coordinate X");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be coordinate Y");
	luaL_argcheck(L, lua_isstring(L, 4) || lua_isnil(L, 4) || lua_isnone(L, 4)  , 4, "Must be start_anim");

	const char* proto_name = lua_tostring(L, 1);
	float x = (float)lua_tonumber(L, 2);
	float y = (float)lua_tonumber(L, 3);
	const char* start_anim = lua_tostring(L, 4);

	bool override_z = false;
	float z = 0;

	int num = lua_gettop(L);
	if (num == 5)
	{
		luaL_argcheck(L, lua_isnumber(L, 5), 5, "Must be coordinate Z");

		z = (float)lua_tonumber(L, 5);
		override_z = true;
	}


	lua_pop(L, num);

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Creating player character %s", proto_name);

	int result = -1;
	GameObject* obj = NULL;

	if ( !playerControl )
	{
		playerControl = new Player;
		playerControl->input = new Input(players.size());
		inpmgr.RegisterInput( playerControl->input, players.size() );
		players.push_back(playerControl);
		Log(DEFAULT_LOG_NAME, logLevelError, "Player character %s is created before playerControl!", proto_name);
	}

	obj = playerControl->Create(proto_name, Vector2(x, y), start_anim);

	if (obj)
	{
		if (obj->sprite)
		{


			if (override_z)
				obj->sprite->z = z;
		}

		result = obj->id;
	}
	else
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error creating player character");
	}

	if (result == -1)
	{
		lua_pushnil(L);				// ����: nil
	}
	else
	{
		lua_pushnumber(L, result);	// ����: result
	}
	return 1;
}

int scriptApi::RevivePlayer(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be player number");
	luaL_argcheck(L, lua_isstring(L, 2) || lua_isnil(L, 2) || lua_isnone(L, 2), 2, "Must be start_anim");
	
	int num = static_cast<int>(lua_tointeger(L, 1));
	const char* start_anim = lua_tostring(L, 2);

	lua_pop(L, lua_gettop(L));

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Reviving player %d", num);

	int result = -1;
	ObjPlayer* obj = playerControl ? playerControl->Revive(num, start_anim) : NULL;

	if (obj)
	{
		result = obj->id;
	}
	else
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error reviving player");
	}

	if (result == -1)
	{
		lua_pushnil(L);				// ����: nil
	}
	else
	{
		lua_pushnumber(L, result);	// ����: result
	}
	return 1;
}

int scriptApi::GetChildren(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	int num = 1;

	if ( obj != NULL && obj->childrenConnection != NULL && obj->childrenConnection->children.size() > 0  )
	{
		lua_newtable(L);
		for ( vector<GameObject*>::iterator iter = obj->childrenConnection->children.begin(); iter != obj->childrenConnection->children.end(); ++iter )
		{
				(*iter)->pushUData(L);
				lua_rawseti(L, -2, num);
				num = num + 1;
		}
	}
	else
		lua_pushnil(L);

	return 1;
}

int scriptApi::GetParent(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	if ( obj != NULL && obj->parentConnection != NULL && !obj->parentConnection->orphan )
	{
		obj->parentConnection->parent->pushUData(L);
	}
	else lua_pushnil(L);

	return 1;
}

int scriptApi::CreateEnemy(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be proto name");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be coordinate X");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be coordinate Y");
	luaL_argcheck(L, lua_isstring(L, 4) || lua_isnil(L, 4) || lua_isnone(L, 4), 4, "Must be start_anim");
	luaL_argcheck(L, lua_isnumber(L, 5) || lua_isnil(L, 5) || lua_isnone(L, 5), 5, "Must be z");
	luaL_argcheck(L, lua_isnumber(L, 6) || lua_isnil(L, 6) || lua_isnone(L, 6) || lua_isuserdata(L, 6), 6, "Must be parent");
	
	const char* proto_name = lua_tostring(L, 1);
	float x = (float)lua_tonumber(L, 2);
	float y = (float)lua_tonumber(L, 3);
	const char* start_anim = lua_tostring(L, 4);

	bool override_z = false;
	float z = 0;

	if (lua_isnumber(L, 5))
	{
		z = (float)lua_tonumber(L, 5);
		override_z = true;
	}

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Creating enemy %s", proto_name);

	int result = -1;
	GameObject* obj = NULL;
	GameObject* parent = NULL;

	if ( lua_isnumber(L, 6) || lua_isuserdata(L,6) )
	{
		parent = static_cast<GameObject*>(getObjectHelper(L, 6));
	}

	obj = ::CreateEnemy(proto_name, Vector2(x, y), start_anim, false, parent);

	if (obj)
	{
		if (obj->sprite)
		{
			if (override_z)
				obj->sprite->z = z;
		}

		result = obj->id;
	}
	else
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error creating enemy");
	}

	if (result == -1)
	{
		lua_pushnil(L);				// ����: nil
	}
	else
	{
		lua_pushnumber(L, result);	// ����: result
	}
	return 1;
}

int scriptApi::EditorSetLink(lua_State* L)
{
#ifdef MAP_EDITOR
	if ( editor_state != editor::EDITOR_OFF )
	{
		CHECKGAME;

		luaL_argcheck(L, lua_isnumber(L, 1), 1, "First object id expected");
		luaL_argcheck(L, lua_isnumber(L, 2)||lua_isnil(L,2)||lua_isnone(L,2), 2, "Second object id expected");
		

		GameObject* obj1 = GetGameObject( static_cast<UINT>(lua_tonumber( L, 1 )) );

		if ( lua_isnumber(L, 2) )
		{
			GameObject* obj2 = GetGameObject( static_cast<UINT>(lua_tonumber( L, 2 )) );
			if ( obj1 != NULL && obj2 != NULL )
				obj1->editor.SetLink( obj1, obj2 );
		}
		else
		{
			if ( obj1 != NULL )
				obj1->editor.SetLink( obj1, NULL );
		}
	}
	else
#endif //MAP_EDITOR
	{
		UNUSED_ARG( L );
	}
	return 0;
}

int scriptApi::CreateSpawner(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be enemy proto name");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be coordinate X");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be coordinate Y");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "Must be enemies count");
	luaL_argcheck(L, lua_isnumber(L, 5), 5, "Must be direction");
	luaL_argcheck(L, lua_isnumber(L, 6), 6, "Must be delay");
	luaL_argcheck(L, lua_isnumber(L, 7) || lua_isnil(L, 7) || lua_isnone(L, 7), 7, "Must be size");
	luaL_argcheck(L, lua_isnumber(L, 8) || lua_isnil(L, 8) || lua_isnone(L, 8), 8, "Must be reset distance");
	luaL_argcheck(L, lua_isnumber(L, 9) || lua_isnil(L, 9) || lua_isnone(L, 9), 9, "Must be spawn limit");
	luaL_argcheck(L, lua_isnumber(L, 10) || lua_isnil(L, 10) || lua_isnone(L, 10), 10, "Must be spawn delay");

	const char* proto_name = lua_tostring(L, 1);
	float x = (float)lua_tonumber(L, 2);
	float y = (float)lua_tonumber(L, 3);
	UINT count = static_cast<UINT>(lua_tointeger(L, 4));
	UINT dir = static_cast<UINT>(lua_tointeger(L, 5));
	UINT delay = static_cast<UINT>(lua_tointeger(L, 6));
	UINT size = 0;
	UINT dist = 0;
	UINT spawnLimit = (UINT)-1;
	UINT spawnDelay = 0;
	if ( lua_isnumber(L, 7) ) size = static_cast<UINT>(lua_tointeger(L, 7));
	if ( lua_isnumber(L, 8) ) dist = static_cast<UINT>(lua_tointeger(L, 8));
	if ( lua_isnumber(L, 9) ) spawnLimit = static_cast<UINT>(lua_tointeger(L, 9));
	if ( lua_isnumber(L, 10) ) spawnDelay = static_cast<UINT>(lua_tointeger(L, 10));

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Creating spawner %s", proto_name);

	ObjSpawner* obj = NULL;

	obj = ::CreateSpawner( Vector2(x,y), proto_name, count, delay, (directionType)dir, size, dist, spawnDelay, spawnLimit);

	if (obj)
	{
		lua_pushinteger(L, obj->id);
	}
	else
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Error creating spawner");
		lua_pushnil(L);
	}

	return 1;
}


int scriptApi::CreateParticleSystem(lua_State *L)
{
	CHECKGAME;

	UINT parent_id = 0;
	int override_table = 5;
	float x = 0.0f;
	float y = 0.0f;

	luaL_argcheck(L, lua_isstring(L, 1), 1, "Must be proto name");
	const char* proto_name = lua_tostring(L, 1);
	int mode = 0;

	if ( lua_isuserdata(L, 2) )
	{
		const GameObject* parent = static_cast<GameObject*>(getObjectHelper(L, 2));
		if ( NULL == parent )
		{
			return 0;
		}
		parent_id = parent->id;
		luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be coordinate X");
		luaL_argcheck(L, lua_isnumber(L, 4), 4, "Must be coordinate Y");
		luaL_argcheck(L, lua_isnumber(L, 5), 5, "Must be mode");
		override_table = 6;

		x = static_cast<float>(lua_tonumber(L, 3));
		y = static_cast<float>(lua_tonumber(L, 4));
		mode = static_cast<int>(lua_tointeger(L, 5));
	}
	else
	{
		luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be coordinate X");
		luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be coordinate Y");
		luaL_argcheck(L, lua_isnumber(L, 4)||lua_isnone(L, 4)||lua_isnil(L, 4), 4, "Must be mode");
		x = static_cast<float>(lua_tonumber(L, 2));
		y = static_cast<float>(lua_tonumber(L, 3));
		if ( lua_isnumber( L, 4 ) )
		{
			mode = static_cast<int>(lua_tointeger(L, 4));
		}
	}

	GameObject* obj = ::CreateParticleSystem( proto_name, Vector2(x, y), parent_id, mode );
	if ( !obj )
	{
		lua_pushnil(L);
		return 1;
	}
	else
	{
		lua_pushinteger(L, obj->id);
	}

	if (lua_istable(L, override_table))
	{
		lua_settop(L, override_table );
		SCRIPT::GetFieldByName(L, "start_color", obj->ParticleSystem->sc);
		SCRIPT::GetFieldByName(L, "end_color", obj->ParticleSystem->ec);
		SCRIPT::GetFieldByName(L, "var_color", obj->ParticleSystem->vc);

		SCRIPT::GetFieldByName(L, "start_size", obj->ParticleSystem->startsize );
		SCRIPT::GetFieldByName(L, "end_size", obj->ParticleSystem->endsize );
		SCRIPT::GetFieldByName(L, "size_variability", obj->ParticleSystem->sizevar );

		SCRIPT::GetFieldByName(L, "trajectory_type", obj->ParticleSystem->trajectory );
		SCRIPT::GetFieldByName(L, "trajectory_param1", obj->ParticleSystem->t_param1 );
		SCRIPT::GetFieldByName(L, "trajectory_param2", obj->ParticleSystem->t_param2 );

		SCRIPT::GetFieldByName(L, "max_particles", obj->ParticleSystem->MaxParticles );

		SCRIPT::GetFieldByName(L, "emission", obj->ParticleSystem->emission );
	}

	return 1;
}

int scriptApi::AddParticleArea(lua_State* L)
{
	CHECKGAME;

	GameObject* ps = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_istable(L, 2), 1, "Area table expeced");
	lua_rawgeti(L, 2, 1);
	float x1 = static_cast<float>(lua_tonumber(L, -1));
	lua_rawgeti(L, 2, 2);
	float y1 = static_cast<float>(lua_tonumber(L, -1));
	lua_rawgeti(L, 2, 3);
	float x2 = static_cast<float>(lua_tonumber(L, -1));
	lua_rawgeti(L, 2, 4);
	float y2 = static_cast<float>(lua_tonumber(L, -1));

	if (!ps || !ps->ParticleSystem)
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "AddParticleArea: object is not a ParticleSystem!");
		return 0;
	}
	if ( ps->ParticleSystem->area == NULL )
		ps->ParticleSystem->area = new vector<CAABB>;
	ps->ParticleSystem->area->push_back( CAABB( x1, y1, x2, y2 ) );
	return 0;
}

int scriptApi::SwitchLighting(lua_State* L)
{
	luaL_argcheck(L, lua_isboolean(L, 1), 1, "New state");
	SetRenderAdditive( lua_toboolean(L, 1) != 0 );

	return 0;
}

int scriptApi::SetParticleWind(lua_State* L)
{
	CHECKGAME;

	int wind_table = 1;
	luaL_argcheck(L, lua_istable(L, 1)||lua_isnumber(L, 1)||lua_isuserdata(L, 1), 1, "Wind table or environment expected");
	GameObject* obj = NULL;
	if (lua_isnumber(L, 1)||lua_isuserdata(L, 1))
	{
		obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	}
	if ( obj && obj->type == objEnvironment )
	{
		wind_table = 2;
	}
	lua_rawgeti(L, wind_table, 1);
	float x = (float)lua_tonumber(L, -1);
	lua_rawgeti(L, wind_table, 2);
	float y = (float)lua_tonumber(L, -1);
	
	if ( obj && obj->type == objEnvironment )
	{
		static_cast<ObjEnvironment*>(obj)->wind = Vector2( x, y );
	}
	else
	{
		SetParticlesWind(Vector2(x, y));
	}

	return 0;
}

int scriptApi::GetParticleWind(lua_State* L)
{
	Vector2 wind = GetParticlesWind();
	luaL_argcheck(L, lua_isnone(L, 1)||lua_isnil(L, 1)||lua_isnumber(L, 1)||lua_isuserdata(L, 1), 1, "Environment or nothing");
	GameObject* obj = NULL;
	if (lua_isnumber(L, 1)||lua_isuserdata(L, 1))
	{
		obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	}
	if ( obj && obj->type == objEnvironment )
	{
		wind = static_cast<ObjEnvironment*>(obj)->wind;
	}
	lua_newtable(L);
	lua_pushnumber(L, wind.x);
	lua_rawseti(L, -2, 1);
	lua_pushnumber(L, wind.y);
	lua_rawseti(L, -2, 2);
	return 1;
}

int scriptApi::CreateRay(lua_State* L)
{
	CHECKGAME;
	luaL_argcheck(L, lua_isnumber(L, 1)|lua_isstring(L, 1), 1, "x1 or prototype name expected");
	if ( lua_isnumber(L,1) )
	{
		luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be coordinate Y1");
		luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be coordinate X2");
		luaL_argcheck(L, lua_isnumber(L, 4), 4, "Must be coordinate Y2");
		
		float x1 = (float)lua_tonumber(L, 1);
		float y1 = (float)lua_tonumber(L, 2);
		float x2 = (float)lua_tonumber(L, 3);
		float y2 = (float)lua_tonumber(L, 4);
		
		lua_pop(L, lua_gettop(L));	// ����:
		
		ObjRay* obj = ::CreateRay(x1, y1, x2, y2);
		
		lua_pushnumber(L, obj->id);
		return 1;
	}
	luaL_argcheck(L, lua_isnumber(L, 2)|lua_isnil(L, 2), 2, "shooter id");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "angle");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "x1");
	luaL_argcheck(L, lua_isnumber(L, 5), 5, "y1");
	luaL_argcheck(L, lua_isnumber(L, 6)||lua_isnone(L, 6)||lua_isnil(L, 6), 5, "damage");
	luaL_argcheck(L, lua_isnumber(L, 7)||lua_isnone(L, 7)||lua_isnil(L, 7), 5, "max targets");
	
	float x1 = (float)lua_tonumber(L, 4);
	float y1 = (float)lua_tonumber(L, 5);
	
	GameObject* shooter = NULL;
	
	if ( lua_isnumber(L, 2) ) shooter = GetGameObject( static_cast<UINT>(lua_tointeger(L, 2)) );
	float angle = (float)lua_tonumber(L, 3);
	
	ObjRay* obj = ::CreateRay(lua_tostring(L, 1), Vector2(x1, y1), (ObjCharacter*)shooter, angle);
	if ( obj )
	{
		if ( lua_isnumber( L, 6 ) )
			obj->damage = static_cast<UINT>(lua_tointeger( L, 6 ));
		if ( lua_isnumber( L, 7 ) )
		{
			obj->SetUseCollisionCounter();
			obj->collisionsCount = static_cast<UINT>(lua_tointeger( L, 7 ));
		}
		lua_pushnumber(L, obj->id);
	}
	else lua_pushnil(L);
	return 1;
}

int scriptApi::CreateBullet(lua_State* L)
{
	CHECKGAME;
	luaL_argcheck(L, lua_isstring(L, 1), 1, "prototype name");


	luaL_argcheck(L, lua_isnumber(L, 2), 2, "x1");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "y1");
	luaL_argcheck(L, lua_isnumber(L, 4)|lua_isnil(L, 4), 4, "shooter id");
	luaL_argcheck(L, lua_isboolean(L, 5), 5, "mirror bool");
	luaL_argcheck(L, lua_isnumber(L, 6), 6, "angle");

	luaL_argcheck(L, lua_isnumber(L, 7), 7, "animation_id");

	Vector2 coord = Vector2( (float)lua_tonumber(L, 2), (float)lua_tonumber(L, 3) );
	Vector2 aim = Vector2(coord.x+1, coord.y);
	GameObject* shooter = NULL;
	if ( lua_isnumber(L, 4) ) shooter = GetGameObject( static_cast<UINT>(lua_tointeger(L, 4)) );
	bool mirror = lua_toboolean( L, 5 ) != 0;
	int angle = static_cast<int>(lua_tointeger( L, 6 ));
	//int anim_id = lua_tointeger( L, 7 );
	if ( mirror )
	{
		aim.x -= 2;
	}
	ObjBullet* obj = CreateBullet(lua_tostring(L, 1), coord, (ObjCharacter*)shooter, aim, angle);
	if ( angle > 90 || angle < -90 ) angle = 180 + angle;
	obj->sprite->setAngle((angle * Const::Math::PI)/180.0f);
	
	lua_pushnumber(L, obj->id);
	return 1;
}

//////////////////////////////////////////////////////////////////////////

/// Creates "dummy" sprite object without initialization from prototype
/// @luaparam boolean physic - if true then ObjPhysic will be created
/// @return integer id - ID of object
int scriptApi::CreateDummySprite(lua_State *L)
{
	CHECKGAME;
	luaL_argcheck(L, lua_isboolean(L, 1) || lua_isnoneornil(L, 1), 1, "Is physisc");
	
	bool physic = lua_toboolean(L, 1) != 0;
	GameObject* obj = ::CreateDummySprite(physic);
	if (obj)
		lua_pushnumber(L, obj->id);
	else
		lua_pushnil(L);
	return 1;
}

/// Creates "dummy" ray object without initialization from prototype
/// @return integer id - ID of object
int scriptApi::CreateDummyRay(lua_State *L)
{
	CHECKGAME;
	GameObject* obj = ::CreateDummyRay();
	if (obj)
		lua_pushnumber(L, obj->id);
	else
		lua_pushnil(L);
	return 1;
}

/// Creates "dummy" ribbon object without initialization from prototype
/// @return integer id - ID of object
int scriptApi::CreateDummyRibbon(lua_State *L)
{
	CHECKGAME;
	GameObject* obj = ::CreateDummyRibbon();
	if (obj)
		lua_pushnumber(L, obj->id);
	else
		lua_pushnil(L);
	return 1;
}

/// Creates "dummy" spawner object without initialization from prototype
/// @return integer id - ID of object
int scriptApi::CreateDummySpawner(lua_State *L)
{
	CHECKGAME;
	GameObject* obj = ::CreateDummySpawner();
	if (obj)
		lua_pushnumber(L, obj->id);
	else
		lua_pushnil(L);
	return 1;
}

/// Creates "dummy" waypoint object without initialization from prototype
/// @return integer id - ID of object
int scriptApi::CreateDummyWaypoint(lua_State *L)
{
	CHECKGAME;
	GameObject* obj = ::CreateDummyWaypoint();
	if (obj)
		lua_pushnumber(L, obj->id);
	else
		lua_pushnil(L);
	return 1;
}

/// Creates "dummy" environment object without initialization from prototype
/// @return integer id - ID of object
int scriptApi::CreateDummyEnvironment(lua_State *L)
{
	CHECKGAME;
	GameObject* obj = ::CreateDummyEnvironment();
	if (obj)
		lua_pushnumber(L, obj->id);
	else
		lua_pushnil(L);
	return 1;
}

/// Creates "dummy" bullet object without initialization from prototype
/// @return integer id - ID of object
int scriptApi::CreateDummyBullet(lua_State *L)
{
	CHECKGAME;
	GameObject* obj = ::CreateDummyBullet();
	if (obj)
		lua_pushnumber(L, obj->id);
	else
		lua_pushnil(L);
	return 1;
}

/// Creates "dummy" player object without initialization from prototype
/// @return integer id - ID of object
int scriptApi::CreateDummyPlayer(lua_State *L)
{
	CHECKGAME;
	GameObject* obj = ::CreateDummyPlayer();
	if (obj)
		lua_pushnumber(L, obj->id);
	else
		lua_pushnil(L);
	return 1;
}

/// Creates "dummy" enemy object without initialization from prototype
/// @return integer id - ID of object
int scriptApi::CreateDummyEnemy(lua_State *L)
{
	CHECKGAME;
	GameObject* obj = ::CreateDummyEnemy();
	if (obj)
		lua_pushnumber(L, obj->id);
	else
		lua_pushnil(L);
	return 1;
}

/// Creates "dummy" effect object without initialization from prototype
/// @return integer id - ID of object
int scriptApi::CreateDummyEffect(lua_State *L)
{
	CHECKGAME;
	GameObject* obj = ::CreateDummyEffect();
	if (obj)
		lua_pushnumber(L, obj->id);
	else
		lua_pushnil(L);
	return 1;
}

/// Creates "dummy" item object without initialization from prototype
/// @return integer id - ID of object
int scriptApi::CreateDummyItem(lua_State *L)
{
	CHECKGAME;
	GameObject* obj = ::CreateDummyItem();
	if (obj)
		lua_pushnumber(L, obj->id);
	else
		lua_pushnil(L);
	return 1;
}


//////////////////////////////////////////////////////////////////////////

int scriptApi::GetMP(lua_State* L)
{
	CHECKGAME;
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "MP number");

	int mp = static_cast<int>(lua_tointeger( L, 2 ));
	if ( !obj || !obj->sprite || obj->sprite->mpCount <= mp )
	{
		lua_pushinteger( L, 0 );
		lua_pushinteger( L, 0 );
		return 2;
	}
	lua_pushnumber( L, obj->sprite->mp[mp].x );
	lua_pushnumber( L, obj->sprite->mp[mp].y );
	return 2;
}

//////////////////////////////////////////////////////////////////////////

int scriptApi::SetPlayerHealth(lua_State* L)
{
	CHECKGAME;
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "player num");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "health");

	int num = static_cast<int>(lua_tointeger(L, 1));
	UINT health = static_cast<UINT>(lua_tointeger(L, 2));

	ObjPlayer* plr = playerControl->GetByNum(num);
	if (plr)
	{
		plr->health = static_cast<short>(health);
	}
	return 0;
}

int scriptApi::DamageObject(lua_State* L)
{
	CHECKGAME;
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "damage amount");

	int damage_type = 0;
	if (obj && (obj->type == objPlayer || obj->type == objEnemy))
	{
		if ( lua_isnumber(L, 3) )
		{
			damage_type = static_cast<int>(lua_tointeger(L, 3));
		}
		((ObjCharacter*)obj)->ReceiveDamage(static_cast<UINT>(lua_tointeger(L, 2)), damage_type);
	}
	return 0;
}

int scriptApi::SetPlayerStats(lua_State* L)
{
	CHECKGAME;
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_istable(L, 2), 2, "player stats table");

	if ( !obj || obj->type != objPlayer ) return 0;
	ObjPlayer* plr = (ObjPlayer*)obj;
	lua_getfield(L, 2, "jump_vel");
	plr->jump_vel = static_cast<float>(lua_tonumber(L, -1));
	lua_getfield(L, 2, "walk_acc");
	plr->walk_acc = static_cast<float>(lua_tonumber(L, -1));
	lua_getfield(L, 2, "max_health");
	plr->health_max = static_cast<short>(lua_tointeger(L, -1));
	lua_getfield(L, 2, "max_x_vel");
	plr->max_x_vel = static_cast<float>(lua_tonumber(L, -1));
	if ( plr->health > plr->health_max ) plr->health = plr->health_max;
	return 0;
}

int scriptApi::AddBonusHealth(lua_State* L)
{
	CHECKGAME;
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "amount");

	if (obj)
		((ObjCharacter*)obj)->health += static_cast<short>(lua_tointeger(L, 2));
	return 0;
}

int scriptApi::AddBonusAmmo(lua_State* L)
{
	CHECKGAME;
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "amount");
	((ObjPlayer*)obj)->ammo += static_cast<short>(lua_tointeger(L, 2));
	return 0;
}

int scriptApi::AddBonusSpeed(lua_State* L)
{
	CHECKGAME;
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "amount");

	if (obj) 
	{
		((ObjCharacter*)obj)->max_x_vel += lua_tointeger(L, 2);
		((ObjCharacter*)obj)->walk_acc += lua_tointeger(L, 2);
	}
	return 0;
}

int scriptApi::ReplacePrimaryWeapon(lua_State* L)
{
	CHECKGAME;
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isstring(L, 2), 2, "weapon name");

	if (obj && obj->type == objPlayer)
		((ObjPlayer*)obj)->GiveWeapon(lua_tostring(L, 2), true);

	return 0;
}

int scriptApi::SetEnvironmentStats(lua_State* L)
{
	CHECKGAME;
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_istable(L, 2), 2, "environment stats table");

	if ( !obj || obj->type != objEnvironment ) return 0;
	ObjEnvironment* env = (ObjEnvironment*)obj;
	lua_getfield(L, 2, "gravity_bonus_x");
	if ( !lua_isnil(L, -1) )
		env->gravity_bonus.x = static_cast<float>(lua_tonumber(L, -1));

	lua_getfield(L, 2, "gravity_bonus_y");
	if ( !lua_isnil(L, -1) )
		env->gravity_bonus.y = static_cast<float>(lua_tonumber(L, -1));

	lua_getfield(L, 2, "on_use");
	if ( lua_isstring(L, -1) )
	{	
		env->script_on_use = StrDupl( lua_tostring(L, -1) );
	}
	else if ( lua_isfunction(L, -1) )
	{
		if (env->on_use > 0) SCRIPT::ReleaseProc(static_cast<LuaRegRef*>(&env->on_use));
		SCRIPT::RegProc(L, static_cast<LuaRegRef*>(&env->on_use), -1);
	}

	lua_getfield(L, 2, "on_enter");
	if ( lua_isstring(L, -1) )
	{	
		env->script_on_enter = StrDupl( lua_tostring(L, -1) );
	}
	else if ( lua_isfunction(L, -1) )
	{
		if (env->on_enter > 0) SCRIPT::ReleaseProc(static_cast<LuaRegRef*>(&env->on_enter));
		SCRIPT::RegProc(L, static_cast<LuaRegRef*>(&env->on_enter), -1);
	}

	lua_getfield(L, 2, "on_leave");
	if ( lua_isstring(L, -1) )
	{	
		env->script_on_leave = StrDupl( lua_tostring(L, -1) );
	}
	else if ( lua_isfunction(L, -1) )
	{
		if (env->on_leave > 0) SCRIPT::ReleaseProc(static_cast<LuaRegRef*>(&env->on_leave));
		SCRIPT::RegProc(L, static_cast<LuaRegRef*>(&env->on_leave), -1);
	}

	lua_getfield(L, 2, "on_stay");
	if ( lua_isstring(L, -1) )
	{	
		env->script_on_stay = StrDupl( lua_tostring(L, -1) );
	}
	else if ( lua_isfunction(L, -1) )
	{
		if (env->on_stay > 0) SCRIPT::ReleaseProc(static_cast<LuaRegRef*>(&env->on_stay));
		SCRIPT::RegProc(L, static_cast<LuaRegRef*>(&env->on_stay), -1);
		lua_pop(L, 1);
	}

	lua_getfield(L, 2, "material");
	if ( !lua_isnil(L, -1) )
		env->material_num = static_cast<int>(lua_tointeger(L, -1));

	lua_getfield(L, 2, "stop_global_wind");
	if ( lua_isboolean(L, -1) )
	{
		env->stop_global_wind = lua_toboolean(L, -1) != 0;
	}

	lua_getfield(L, 2, "wind_x");
	if ( !lua_isnil(L, -1) )
		env->wind.x = static_cast<float>(lua_tonumber(L, -1));

	lua_getfield(L, 2, "wind_y");
	if ( !lua_isnil(L, -1) )
		env->wind.y = static_cast<float>(lua_tonumber(L, -1));

	return 0;
}

int scriptApi::SetCustomWeapon(lua_State* L)
{
	CHECKGAME;
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_istable(L, 2), 2, "weapon table");

	if (!obj || obj->type != objPlayer)
		return 0;
	ObjPlayer* pl = static_cast<ObjPlayer*>(obj);
	if ( !pl->alt_weapon )
		return 0;
	Weapon* wp = pl->alt_weapon;

	lua_getfield(L, 2, "reload_time");
	if ( !lua_isnil(L, -1) )
		wp->reload_time = static_cast<UINT>(lua_tointeger(L, -1));
	lua_getfield(L, 2, "clip_reload_time");
	if ( !lua_isnil(L, -1) )
		wp->clip_reload_time = static_cast<UINT>(lua_tointeger(L, -1));
	lua_getfield(L, 2, "shots_per_clip");
	if ( !lua_isnil(L, -1) )
		wp->shots_per_clip = static_cast<USHORT>(lua_tointeger(L, -1));
	lua_getfield(L, 2, "ammo_per_shot");
	if ( !lua_isnil(L, -1) )
		wp->bullets_count = static_cast<USHORT>(lua_tointeger(L, -1));
	lua_getfield(L, 2, "recoil");
	if ( !lua_isnil(L, -1) )
		wp->recoil = static_cast<float>(lua_tonumber(L, -1));
	return 0;
}

int scriptApi::SetCustomBullet(lua_State* L)
{
	CHECKGAME;
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_istable(L, 2), 2, "bullet table");

	if (!obj || obj->type != objBullet)
		return 0;
	ObjBullet* bl = static_cast<ObjBullet*>(obj);

	lua_getfield(L, 2, "damage");
	if ( !lua_isnil(L, -1) )
		bl->damage = static_cast<UINT>(lua_tointeger(L, -1));
	lua_getfield(L, 2, "hurts_same_type");
	if ( !lua_isnil(L, -1) )
		bl->hurts_same_type = lua_toboolean(L, -1) != 0;
	lua_getfield(L, 2, "multiple_targets");
	if ( !lua_isnil(L, -1) )
		bl->multiple_targets = lua_toboolean(L, -1) != 0;
	lua_getfield(L, 2, "push_force");
	if ( !lua_isnil(L, -1) )
		bl->push_force = static_cast<float>(lua_tonumber(L, -1));
	lua_getfield(L, 2, "vel");
	if ( !lua_isnil(L, -1) )
	{
		float nv = static_cast<float>(lua_tonumber(L, -1));
		bl->vel.x *= nv;
		bl->vel.y *= nv;
		bl->acc.x *= nv;
		bl->acc.y *= nv;
		bl->max_x_vel *= nv;
		bl->max_y_vel *= nv;
	}
	return 0;
}

int scriptApi::setDefaultEnvironment(lua_State* L)
{
	CHECKGAME;
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	if ( !obj || obj->type != objEnvironment ) return 0;
	DefaultEnvSet( (ObjEnvironment*)obj );

	return 0;
}

int scriptApi::SetObjectInvincible(lua_State* L)
{
	CHECKGAME;
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "object num");
	luaL_argcheck(L, lua_isboolean(L, 2), 2, "invincibility flag");
	
	// TODO: ����� �� ������������, ������ �������� ����������. ���� ����� ����������� � ���������.
	//int num = lua_tointeger(L, 1);
	bool inv = lua_toboolean(L, 2) != 0;

	for( size_t i = 0; i < playerControl->character_list.size(); i++ )
	{
		playerControl->character_list[i].first->is_invincible = inv;
	}
	return 0;
}

int scriptApi::SetObjectInvisible(lua_State* L)
{
	CHECKGAME;
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isboolean(L, 2), 2, "invisbility flag");
	
	if ( obj == NULL )
		return 0;
	bool inv = lua_toboolean(L, 2) != 0;
	if ( inv )
		obj->sprite->ClearVisible();
	else
		obj->sprite->SetVisible();
	return 0;
}

int scriptApi::SetPlayerRecoveryTime(lua_State* L)
{
	CHECKGAME;
	GameObject* go = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "invincibility time");



	if ( !go || go->type != objPlayer )
		return 0;
	float ammount = (float)lua_tonumber(L, 2);
	ObjPlayer* op = (ObjPlayer*)go;
	op->recovery_time = max( op->recovery_time, ammount );
	if ( ammount == -1 )
	{
		op->recovery_time = 0;
		op->is_invincible = false;
	}
	if ( ammount > 0 ) op->is_invincible = true;
	
	return 0;
}

int scriptApi::SetObjSolidToByte(lua_State* L)
{
	CHECKGAME;
	GameObject* go = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "ghost_to byte");

	BYTE solid_to = (BYTE)lua_tointeger(L, 2);

	if ( !go || !go->IsPhysic() )
		return 0;
	((ObjPhysic*)go)->solid_to = solid_to;
	
	return 0;
}

int scriptApi::SetObjGhostToByte(lua_State* L)
{
	CHECKGAME;
	GameObject* go = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "ghost_to byte");

	if ( !go || !go->IsPhysic() )
		return 0;
	BYTE solid_to = static_cast<BYTE>(lua_tointeger(L, 2));
	((ObjPhysic*)go)->solid_to = ~solid_to;
	
	return 0;
}

int scriptApi::SetObjSolidTo(lua_State* L)
{
	CHECKGAME;
	GameObject* go = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "ghost_to byte");

	if ( !go || !go->IsPhysic() )
		return 0;
	BYTE solid_to = (BYTE)lua_tointeger(L, 2);
	((ObjPhysic*)go)->solid_to |= solid_to;
	
	return 0;
}

int scriptApi::SetObjGhostTo(lua_State* L)
{
	CHECKGAME;
	GameObject* go = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "ghost_to byte");

	BYTE solid_to = (BYTE)lua_tointeger(L, 2);

	if ( !go || !go->IsPhysic() )
		return 0;
	((ObjPhysic*)go)->solid_to &= ~solid_to;
	
	return 0;
}

int scriptApi::SetNextWaypoint(lua_State* L)
{
	CHECKGAME;
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "next id");

	int id = static_cast<int>(lua_tointeger(L, 2));

	if ( obj->type != objWaypoint )
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "SetNextWaypont: object is not a waypoint!");
		return 0;
	}
	((ObjWaypoint*)obj)->next_id = id;
	return 0;
}

int scriptApi::SetEnemyWaypoint(lua_State* L)

{
	CHECKGAME;
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "next id");

	int id = static_cast<int>(lua_tointeger(L, 2));

	if (!obj || obj->type != objEnemy )
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "SetNextWaypont: object does not exists or is not an enemy!");
		return 0;
	}
	
	((ObjEnemy*)obj)->current_waypoint = id;
	return 0;
}

int scriptApi::GetWaypoint(lua_State* L)
{
	CHECKGAME;
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "waypoint id");
	UINT num = static_cast<UINT>(lua_tointeger(L, 1));
	ObjWaypoint* wp = ::GetWaypoint( num );
	if ( wp )
		lua_pushinteger( L, wp->id );
	else
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "GetWaypoint: no special waypoint %i!", num);
		lua_pushnil( L );
	}
	return 1;
}



int scriptApi::SetPlayerAltWeapon(lua_State* L)
{
	CHECKGAME;
	luaL_argcheck(L, lua_isnumber(L, 1)||lua_isnone(L, 1)||lua_isnil(L, 1), 1, "player num");

	luaL_argcheck(L, lua_isstring(L, 2)||lua_isnone(L, 2)||lua_isnil(L, 2), 2, "weapon name or nil");

	ObjPlayer* plr = NULL;

	if ( lua_isnumber(L, 1) )
	{
		int num = static_cast<int>(lua_tointeger(L, 1));
		plr = playerControl->GetByNum(num);
	}
	else plr = playerControl->current;
	const char* wn = NULL;
	if ( lua_isstring(L, 2 ) ) wn = lua_tostring(L, 2);
	
	if (plr)
	{
		plr->GiveWeapon(wn, false);
	}

	return 0;

}

int scriptApi::SetPlayerAmmo(lua_State* L)

{
	CHECKGAME;
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "player num");

	luaL_argcheck(L, lua_isnumber(L, 2), 2, "ammo amount");

	int num = static_cast<int>(lua_tointeger(L, 1));
	UINT ammo = static_cast<UINT>(lua_tointeger(L, 2));

	ObjPlayer* plr = playerControl ? playerControl->GetByNum(num) : NULL;
	if (plr)
	{
		plr->SetAmmo(ammo);
	}

	return 0;
}

int scriptApi::SwitchPlayer(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isboolean(L, 1)||lua_isnil(L, 1)||lua_isnone(L, 1), 1, "Immediate change flag expected.");

	bool immediate = false;
	if ( lua_isboolean(L, 1) )
	{
		immediate = lua_toboolean(L, 1) != 0;
	}

	if ( playerControl ) playerControl->Changer( true, immediate );
	return 0;
}

int scriptApi::SwitchWeapon(lua_State* L)
{
	CHECKGAME;

	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isboolean(L, 2)||lua_isnil(L, 2)||lua_isnone(L, 2), 2, "weapon bool");

	if (!obj)
	{
		return 0;
	}
	
	ObjPlayer* op = static_cast<ObjPlayer*>(obj);

	if (op)
	{
		if ( lua_isboolean(L, 2) ) op->cur_weapon = lua_toboolean(L, 2) ? op->alt_weapon : op->weapon;
		else op->cur_weapon = ( op->alt_weapon && op->cur_weapon == op->alt_weapon ? op->weapon : op->alt_weapon );
	}
	return 0;
}

int scriptApi::SetPlayerRevivePoint(lua_State* L)
{
	CHECKGAME;
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "x coord");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "y coord");

	if ( playerControl ) playerControl->revivePoint = Vector2((float)lua_tonumber(L,1), (float)lua_tonumber(L, 2));

	return 0;
}

int scriptApi::SetOnChangePlayerProcessor(lua_State* L)
{
	CHECKGAME;
	if ( playerControl ) SCRIPT::RegProc(L, &playerControl->onChangePlayerProcessor, 1);
	return 0;
}


int scriptApi::SetOnPlayerDeathProcessor(lua_State* L)
{
	CHECKGAME;

	LuaRegRef ref = LUA_NOREF;
	SCRIPT::RegProc(L, &ref, 1);

	for( size_t i = 0; i < players.size(); i++ )
	{
		SCRIPT::ReleaseProc(&(players[i]->onPlayerDeathProcessor));
		players[i]->onPlayerDeathProcessor = ref;
		SCRIPT::ReserveProc(ref);
	}
	// ���� ��� �����������, ����������� ������ ������� ������ RegProc. ��� ����������� �� ���������� �������.
	SCRIPT::ReleaseProc(&ref);

	return 0;
}

int scriptApi::BlockPlayerChange(lua_State* L)
{
	CHECKGAME;
	luaL_argcheck(L, lua_isnumber(L, 1)||lua_isnil(L, 1)||lua_isnone(L, 1), 1, "player num or none");
	if ( lua_isnumber(L, 1) )
	{
		int num = static_cast<int>(lua_tonumber(L, 1)-1);
		if ( num >= 0 && num < static_cast<int>(players.size()) )
		{
			Player* plr = players[static_cast<size_t>(num)];
			plr->BlockChange();
		}
	}
	else
	{
		for ( vector<Player*>::iterator iter = players.begin(); iter != players.end(); iter++ )
		{
			(*iter)->BlockChange();
		}
	}
	return 0;
}

int scriptApi::UnblockPlayerChange(lua_State* L)
{
	CHECKGAME;
	luaL_argcheck(L, lua_isnumber(L, 1)||lua_isnil(L, 1)||lua_isnone(L, 1), 1, "player num or none");
	if ( lua_isnumber(L, 1) )
	{
		int num = static_cast<int>(lua_tonumber(L, 1)-1);
		if ( num >= 0 && num < static_cast<int>(players.size()) )
		{
			Player* plr = players[static_cast<size_t>(num)];
			plr->UnblockChange();
		}
	}
	else
	{
		for ( vector<Player*>::iterator iter = players.begin(); iter != players.end(); iter++ )
		{
			(*iter)->UnblockChange();
		}
	}
	return 0;
}

int scriptApi::CreateActor(lua_State* L)
{
	CHECKGAME;

	luaL_argcheck(L, lua_isnumber(L, 1)||lua_isnil(L, 1)||lua_isnone(L, 1), 1, "input num");

	int input_num = 0;
	if ( lua_isnumber(L, 1) )
	{
		input_num = static_cast<int>(lua_tointeger(L, 1)) - 1;
	}
	else
	{
		input_num = players.size();
	}

	if ( input_num < KEY_SETS_NUMBER && input_num >= 0 )
	{
		playerControl = new Player;
		playerControl->input = new Input(input_num);
		inpmgr.RegisterInput( playerControl->input, input_num );
		players.push_back(playerControl);
		lua_pushinteger( L, players.size() );
	}
	else
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "CreateActor: Cannot create new Actor.");
		lua_pushnil( L );
	}
	return 1;
}

int scriptApi::DestroyActor(lua_State* L)
{
	CHECKGAME;
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Actor number");

	int n = lua_tointeger(L, 1) - 1;
	if( n < 0 || n >= static_cast<int>(players.size()) )
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "DestroyActor: bad number! n = %i!", n);
		return 0;
	}
	players.erase(players.begin()+n);
	return 0;
}

int scriptApi::GetActorCount(lua_State* L)
{
	CHECKGAME;
	lua_pushinteger( L, players.size() );
	return 1;
}

int scriptApi::GetDefaultActor(lua_State* L)
{
	CHECKGAME;
	for(size_t i = 0; i < players.size(); i++)
	{
		if (players[i] == playerControl)
		{
			lua_pushinteger( L, i + 1 );
			return 1;
		}
	}
	lua_pushnil( L );
	Log(DEFAULT_LOG_NAME, logLevelError, "GetDefaultActor: no \'playerControl\' in \'players\'!");	
	return 1;
}

int scriptApi::SetDefaultActor(lua_State* L)
{
	CHECKGAME;
	luaL_argcheck(L, lua_isnumber(L, 1), 1, "Actor number");

	int n = lua_tointeger(L, 1) - 1;
	if( n < 0 || n >= static_cast<int>(players.size()) )
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "SetDefaultActor: bad number! n = %i!", n);
		return 0;
	}
	playerControl = players[n];
	return 0;
}

int scriptApi::SetPhysObjBorderColor(lua_State* L)
{
#ifdef MAP_EDITOR
	if ( editor_state != editor::EDITOR_OFF )
	{
		GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
		luaL_argcheck(L, lua_istable(L, 2), 2, "Must be table color");
		luaL_argcheck(L, lua_isboolean(L, 3), 3, "Must be bool show");
		
		if (obj)
		{
			SCRIPT::GetColorFromTable(L, 2, obj->editor.border_color);
			obj->editor.show_border = lua_toboolean(L, 3) != 0;
		}
		else
		{
			Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error SetPhysObjBorderColor: obj id=%d does not exist or is not physic", (UINT)lua_tointeger(L, 1));
		}
	}
	else
#endif //MAP_EDITOR
	{
		UNUSED_ARG( L );
	}
	return 0;
}


//////////////////////////////////////////////////////////////////////////

int scriptApi::PushInt(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "value expected");

	if (obj)
	{
		if (obj->sprite)
			obj->sprite->stack->Push((int)lua_tointeger(L, 2));
	}
	return 0;
}

int scriptApi::PopInt(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));

	if (obj && obj->sprite)	lua_pushinteger( L, obj->sprite->stack->PopInt() );
	else lua_pushnil( L );
	return 1;
}

int scriptApi::CreateRibbonObj(lua_State* L)
{
	luaL_argcheck(L, lua_isstring(L, 1), 1, "Texture or prototype name expected");
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "k_x expected");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "k_y expected");
	luaL_argcheck(L, lua_isnumber(L, 4), 4, "x expected");
	luaL_argcheck(L, lua_isnumber(L, 5), 5, "y expected");
	luaL_argcheck(L, lua_isnumber(L, 6), 6, "z expected");
	luaL_argcheck(L, lua_isboolean(L, 7), 7, "Prototype usage flag expected");
	ObjRibbon* rib = CreateRibbon( lua_tostring(L, 1), Vector2((float)lua_tonumber(L, 2), (float)lua_tonumber(L, 3)), Vector2((float)lua_tonumber(L, 4), (float)lua_tonumber(L, 5)), (float)lua_tonumber(L, 6), lua_toboolean(L, 7) != 0);

	lua_pushinteger( L, rib->id );
	return 1;
}

int scriptApi::SetRibbonObjBounds(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2) || lua_isnil(L, 2), 2, "x1 expected");
	luaL_argcheck(L, lua_isnumber(L, 3) || lua_isnil(L, 3), 3, "y1 expected");
	luaL_argcheck(L, lua_isnumber(L, 4) || lua_isnil(L, 4), 4, "x2 expected");
	luaL_argcheck(L, lua_isnumber(L, 5) || lua_isnil(L, 5), 5, "y2 expected");
	
	if ( obj == NULL || obj->type != objRibbon ) return 0;
	ObjRibbon* rib = (ObjRibbon*)obj;
	float x1 = 0.0f;

	float y1 = 0.0f;
	float x2 = 0.0f;
	float y2 = 0.0f;
	bool ux1, ux2, uy1, uy2;
	if ( (ux1 = (lua_isnumber(L, 2) != 0)) == true ) x1 = (float)lua_tonumber( L, 2 );
	if ( (uy1 = (lua_isnumber(L, 3) != 0)) == true ) y1 = (float)lua_tonumber( L, 3 );
	if ( (ux2 = (lua_isnumber(L, 4) != 0)) == true ) x2 = (float)lua_tonumber( L, 4 );
	if ( (uy2 = (lua_isnumber(L, 5) != 0)) == true ) y2 = (float)lua_tonumber( L, 5 );
	rib->setBounds( min(x1, x2), min(y1, y2), max(x1, x2), max(y1, y2) );	//I see what you did there.
	rib->setBoundsUsage( ux1, uy1, ux2, uy2 );
	return 0;
}

int scriptApi::SetRibbonObjK(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2), 2, "kx expected");
	luaL_argcheck(L, lua_isnumber(L, 3), 3, "ky expected");
	
	if ( obj == NULL || obj->type != objRibbon ) return 0;
	ObjRibbon* rib = (ObjRibbon*)obj;
	rib->k.x = static_cast<float>(lua_tonumber(L, 2));
	rib->k.y = static_cast<float>(lua_tonumber(L, 3));
	return 0;
}

int scriptApi::SetRibbonObjSpacing(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2)|lua_isnil(L, 2)|lua_isnone(L, 2), 2, "x spacing expected");
	luaL_argcheck(L, lua_isnumber(L, 3)|lua_isnil(L, 3)|lua_isnone(L, 3), 3, "y spacing expected");
	
	if ( obj == NULL || obj->type != objRibbon ) return 0;
	ObjRibbon* rib = (ObjRibbon*)obj;

	float x = -1.0f;
	float y = -1.0f;

	if ( lua_isnumber(L, 2) )
	{
		x = static_cast<float>(lua_tonumber(L, 2));
	}
	if ( lua_isnumber(L, 3) )
	{
		y = static_cast<float>(lua_tonumber(L, 3));
	}
	
	rib->setSpacing( x, y );

	return 0;
}

int scriptApi::SetRibbonObjRepitition(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isboolean(L, 2), 1, "x flag expected");
	luaL_argcheck(L, lua_isboolean(L, 3), 2, "y flag expected");
	
	if ( obj == NULL || obj->type != objRibbon ) return 0;
	ObjRibbon* rib = (ObjRibbon*)obj;
	rib->setRepitition( lua_toboolean(L, 2) != 0, lua_toboolean(L, 3) != 0 );
	return 0;
}

extern GameObject* Waypoints;

int scriptApi::GetNearestWaypoint(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	if ( obj == NULL ) return 0;
	ObjWaypoint* wp = NULL;
	ObjWaypoint* target = NULL;
	float dist = 0.0f;
	for ( vector<GameObject*>::iterator iter = Waypoints->childrenConnection->children.begin(); 
		iter != Waypoints->childrenConnection->children.end(); ++iter)
	{
			wp = (ObjWaypoint*)(*iter);
			if ( target == NULL || (wp->aabb.p - obj->aabb.p).Length() < dist )
			{
					target = wp;
					dist = (wp->aabb.p - obj->aabb.p).Length();
			}
	}
	PushObject(L, target);
	return 1;
}

//////////////////////////////////////////////////////////////////////////

int scriptApi::ApplyProto(lua_State* L)
{
	CHECKGAME;
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isstring(L, 2), 2, "proto name expected");

	if (!obj)
		return luaL_error(L, "Cannot get object with the id %d");

	const char* proto_name = lua_tostring(L, 2);

	const Proto* proto = protoMgr->GetByName(lua_tostring(L, 2));
	if (!proto)
		return luaL_error(L, "Cannot get proto with the name %s", proto_name);

	

	lua_pushboolean(L, obj->ApplyProto(proto));
	return 1;
}

//////////////////////////////////////////////////////////////////////////
int scriptApi::ObjectOnScreen(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));
	luaL_argcheck(L, lua_isnumber(L, 2)||lua_isnone(L, 2)||lua_isnil(L, 2), 2, "size adjustment");
	
	int size_adj = 0;

	if ( lua_isnumber( L, 2 ) ) size_adj = lua_tointeger(L, 2);
	
	if ( obj )
	{
		if ( !(obj->aabb.Left() - size_adj > CAMERA_DRAW_RIGHT  || obj->aabb.Right()  + size_adj < CAMERA_DRAW_LEFT) &&
		     !(obj->aabb.Top()  - size_adj > CAMERA_DRAW_BOTTOM || obj->aabb.Bottom() + size_adj < CAMERA_DRAW_TOP))
			lua_pushboolean( L, true );
		else lua_pushboolean( L, false );
	}
	else lua_pushnil( L );
	
	return 1;
}

int scriptApi::GetNearestEnemy(lua_State* L)
{
	GameObject* obj = static_cast<GameObject*>(getObjectHelper(L, 1));

	if ( (obj->type != objPlayer) && (obj->type != objEnemy) )
	{
		lua_pushnil( L );
	}
	else
	{
		ObjCharacter* oc = static_cast<ObjCharacter*>(obj);
		obj = oc->GetNearestCharacter( oc->faction_hates );
		if ( NULL != obj )
		{
			obj->pushUData( L );
		}
		else
		{
			lua_pushnil( L );
		}
	}

	return 1;
}

int scriptApi::EditorSetState(lua_State* L)
{
#ifdef MAP_EDITOR
	luaL_argcheck(L, lua_isstring(L, 1), 1, "String expected");
	string mode = string(lua_tostring(L, 1));
	if ( mode == "off" )
	{
		editor::SetEditorState(editor::EDITOR_OFF);
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "EditorSetState(\"off\")");
	}
	else if ( mode == "map_editor" )
	{
		editor::SetEditorState(editor::EDITOR_MAP_EDITOR);
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "EditorSetState(\"map_editor\")");
	}
	else
	{
		Log(DEFAULT_SCRIPT_LOG_NAME, logLevelError, "EditorSetState(): Input Error.");
	}
#else
	UNUSED_ARG( L );
#endif //MAP_EDITOR
	return 0;
}
