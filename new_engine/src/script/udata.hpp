#pragma once
#ifndef __UDATA_HPP
#define __UDATA_HPP

struct lua_State;
class CUData;

void RegisterAllTypeMetatables(lua_State* L);
void RegisterCustomUserdataFunction(lua_State* L);


template <typename T>
CUData* CreateUData(lua_State* L, T& user);

template <typename T>
CUData* check_userdata(lua_State* L, int ud, bool throw_error = true);

int GetBasicMetatables(lua_State* L);

#endif //__UDATA_HPP
