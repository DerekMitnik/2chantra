#pragma once

class CUDataUser;


#include <string>

class CUData
{
	CUDataUser* pUser;

	// Reference to userdata in lua registry. Registry is in standard lua state "lua", created on lua initialization.
	LuaRegRef regRef;

	CUData(CUData& ref);
	CUData& operator=(CUData& ref);
	~CUData();
public:

	CUData(CUDataUser* pUser) :
		pUser(pUser),
		regRef(-1)
	{

	}

	CUDataUser* getUser() const { return pUser; }
	void clearUser();
	void onGC();

	const LuaRegRef& getRegRef() const { return regRef; }
	void setRegRef(const LuaRegRef& ref) { regRef = ref; }
};