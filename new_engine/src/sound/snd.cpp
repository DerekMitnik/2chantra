#include "StdAfx.h"

#include "bass.h"
#include "snd.h"

#include "../config.h"

extern config cfg;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


const char * GetBassErrorText(int code)
{
	switch (code)
	{
	case BASS_OK:				return "all is OK"; break;
	case BASS_ERROR_MEM:		return "memory error"; break;
	case BASS_ERROR_FILEOPEN:	return "can't open the file"; break;
	case BASS_ERROR_DRIVER:		return "can't find a free/valid driver"; break;
	case BASS_ERROR_BUFLOST:	return "the sample buffer was lost"; break;
	case BASS_ERROR_HANDLE:		return "invalid handle"; break;
	case BASS_ERROR_FORMAT:		return "unsupported sample format"; break;
	case BASS_ERROR_POSITION:	return "invalid position"; break;
	case BASS_ERROR_INIT:		return "BASS_Init has not been successfully called"; break;
	case BASS_ERROR_START:		return "BASS_Start has not been successfully called"; break;
	case BASS_ERROR_ALREADY:	return "already initialized/paused/whatever"; break;
	case BASS_ERROR_NOCHAN:		return "can't get a free channel"; break;
	case BASS_ERROR_ILLTYPE:	return "an illegal type was specified"; break;
	case BASS_ERROR_ILLPARAM:	return "an illegal parameter was specified"; break;
	case BASS_ERROR_NO3D:		return "no 3D support"; break;
	case BASS_ERROR_NOEAX:		return "no EAX support"; break;
	case BASS_ERROR_DEVICE:		return "illegal device number"; break;
	case BASS_ERROR_NOPLAY:		return "not playing"; break;
	case BASS_ERROR_FREQ:		return "illegal sample rate"; break;
	case BASS_ERROR_NOTFILE: 	return "the stream is not a file stream"; break;
	case BASS_ERROR_NOHW:		return "no hardware voices available"; break;
	case BASS_ERROR_EMPTY:		return "the MOD music has no sequence data"; break;
	case BASS_ERROR_NONET:		return "no internet connection could be opened"; break;
	case BASS_ERROR_CREATE:		return "couldn't create the file"; break;
	case BASS_ERROR_NOFX:		return "effects are not available"; break;
	case BASS_ERROR_NOTAVAIL:	return "requested data is not available"; break;
	case BASS_ERROR_DECODE:		return "the channel is a \"decoding channel\""; break;
	case BASS_ERROR_DX:			return "a sufficient DirectX version is not installed"; break;
	case BASS_ERROR_TIMEOUT:	return "connection timedout"; break;
	case BASS_ERROR_FILEFORM:	return "unsupported file format"; break;
	case BASS_ERROR_SPEAKER:	return "unavailable speaker"; break;
	case BASS_ERROR_VERSION:	return "invalid BASS version (used by add-ons)"; break;
	case BASS_ERROR_CODEC:		return "codec is not available/supported"; break;
	case BASS_ERROR_ENDED:		return "the channel/file has ended"; break;
	case BASS_ERROR_UNKNOWN:
	default:					return "some other mystery problem"; break;
	}
}

__INLINE void LogBassError(const char* const funcName)
{
	int err = BASS_ErrorGetCode();
	Log(DEFAULT_LOG_NAME, logLevelError, "BASS error %d in function %s: %s", err, funcName, GetBassErrorText(err));
}

#ifdef USE_SDL_MIXER
__INLINE void LogSDLMixerError(const char* const funcName)
{
	Log(DEFAULT_LOG_NAME, logLevelError, "SDL_mixer error in function %s: %s", funcName, Mix_GetError());
}

Sound* current_music = NULL;
map<int, Sound*> active_channels;
bool loading_music = false; //This is not good. This is not good at all. But there doesn't seem to be any easy way to do that with SDL_mixer

void channel_finished( int channel )
{
	active_channels[channel]->ResetChannel();
}

void music_finished()
{
	if ( current_music )
	{
		current_music->SetVolume( cfg.volume * cfg.volume_music );
	}
}

void Sound::ResetChannel()
{
	channel = -1;
}

void Sound::SetMusicVolume( float volume )
{
	music_volume = volume;
}

float Sound::GetMusicVolume()
{
	return music_volume;
}
#endif
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

Sound::~Sound()
{
#ifdef USE_SDL_MIXER
	if ( chunk ) Mix_FreeChunk( chunk );
	if ( music_chunk ) Mix_FreeMusic( music_chunk );
	if ( rwops ) SDL_FreeRW( rwops );
#endif
	if( _buffer ) DELETEARRAY( _buffer );
}

bool Sound::Play(bool restart)
{
#ifdef NOSOUND_BASS
#ifdef USE_SDL_MIXER
	if ( type == atSound )
	{
		if ( channel > -1 )
		{
			if ( !restart ) return true;
			if ( Mix_PlayChannel( channel, chunk, 0 ) == -1 )
			{
				LogSDLMixerError(__FUNCTION__);
				return false;
			}
			active_channels[channel] = this;
			return true;
		}
		else
		{
			if ( (channel = Mix_PlayChannel( -1, chunk, 0 )) == -1 )
			{
				LogSDLMixerError(__FUNCTION__);
				return false;
			}
			active_channels[channel] = this;
			return true;
		}
	}
	else
	{
		if ( current_music == this )
		{
			if ( restart )
			{
				Mix_RewindMusic();
			}
			return true;
		}
		else
		{
			if ( Mix_PlayMusic( music_chunk, 1 ) == -1 )
			{
				LogSDLMixerError(__FUNCTION__);
				return false;
			}
			current_music = this;
		}
		return true;
	}
#else
	UNUSED_ARG(restart);
#endif // USE_SDL_MIXER
#else
	SetLooped( false );
	bool rest = true;
	if ( BASS_ChannelIsActive(bassHandle) )
	{
		const QWORD pos = BASS_ChannelGetPosition( bassHandle, BASS_POS_BYTE );
		const QWORD len = BASS_ChannelGetLength( bassHandle, BASS_POS_BYTE );
		if ( !( pos >= len ) )
		{
			rest = restart;
		}
	}
	if (BASS_ChannelPlay(this->bassHandle, rest))
		return true;
	else
		LogBassError(__FUNCTION__);
#endif // NOSOUND_BASS
	return false;
}

bool Sound::PlayLooped(bool restart)
{
#ifdef NOSOUND_BASS
#ifdef USE_SDL_MIXER
	if ( type == atSound )
	{
		if ( channel > -1 )
		{
			if ( !restart ) return true;
			if ( Mix_PlayChannel( channel, chunk, -1 ) == -1 )
			{
				LogSDLMixerError(__FUNCTION__);
				return false;
			}
			active_channels[channel] = this;
			return true;
		}
		else
		{
			if ( (channel = Mix_PlayChannel( -1, chunk, -1 )) == -1 )
			{
				LogSDLMixerError(__FUNCTION__);
				return false;
			}
			active_channels[channel] = this;
			return true;
		}
	}
	else
	{
		if ( current_music == this )
		{
			if ( restart )
			{
				Mix_RewindMusic();
			}
			return true;
		}
		else
		{
			if ( Mix_PlayMusic( music_chunk, -1 ) == -1 )
			{
				LogSDLMixerError(__FUNCTION__);
				return false;
			}
			current_music = this;
		}
		return true;
	}
#else
	UNUSED_ARG(restart);
#endif // USE_SDL_MIXER
#else
	SetLooped( true );
	bool rest = true;
	if ( BASS_ChannelIsActive(bassHandle) )
	{
		const QWORD pos = BASS_ChannelGetPosition( bassHandle, BASS_POS_BYTE );
		const QWORD len = BASS_ChannelGetLength( bassHandle, BASS_POS_BYTE );
		if ( !( pos >= len ) )
		{
			rest = restart;
		}
	}
	if (BASS_ChannelPlay(this->bassHandle, rest))
		return true;
	else
		LogBassError(__FUNCTION__);
#endif // NOSOUND_BASS
	return false;
}

bool Sound::Pause() const
{
#ifdef USE_SDL_MIXER
	if ( type == atSound )
	{
		if ( channel < 0 ) return true;
		Mix_Pause( channel );
	}
	else
	{
		if ( current_music == this )
		{
			Mix_PauseMusic();
		}
	}
	return true;
#endif // USE_SDL_MIXER
#ifndef NOSOUND_BASS
	if (BASS_ChannelPause(this->bassHandle))
		return true;
	else
		LogBassError(__FUNCTION__);

#endif // NOSOUND_BASS
	return false;
}

bool Sound::Stop() const
{
#ifdef USE_SDL_MIXER
	if ( type == atSound )
	{
		if ( channel < 0 ) return true;
		Mix_HaltChannel( channel );
	}
	else
	{
		Mix_HaltMusic();
	}
	return true;
#endif // USE_SDL_MIXER
#ifndef NOSOUND_BASS
	if (BASS_ChannelStop(this->bassHandle))
		return true;
	else
		LogBassError(__FUNCTION__);
#endif // NOSOUND_BASS
	return false;
}

float Sound::GetVolume() const
{
#ifdef USE_SDL_MIXER
	if ( type == atSound )
	{
		return static_cast<float>(Mix_VolumeChunk(chunk, -1)/128.0);
	}
	else
	{
	}
#endif // USE_SDL_MIXER
#ifndef NOSOUND_BASS
	float bufVolume;
	DWORD attrib = BASS_ATTRIB_VOL;
	if ( type == atMusic ) attrib = BASS_ATTRIB_MUSIC_VOL_CHAN;
	if (BASS_ChannelGetAttribute(this->bassHandle, attrib, &bufVolume))
		return bufVolume;
	else
		LogBassError(__FUNCTION__);
#endif // NOSOUND_BASS
	return 0.0f;
}

bool Sound::SetPan( float pan )
{
#ifdef USE_SDL_MIXER
	if ( type == atMusic ) return false;
	if ( channel < 0 ) return false;
	uint8_t left = floor( 255.0f * ((pan + 1.0f) / 2.0f) );
	uint8_t right = 255 - left;
	Mix_SetPanning( channel, left, right );
	return true;
#endif
#ifndef NOSOUND_BASS
	if (BASS_ChannelSetAttribute( this->bassHandle, BASS_ATTRIB_PAN, pan ))
		return true;
#endif // NOSOUND_BASS
	return false;
}

bool Sound::SetVolume(float vol)
{
	if (vol < 0.0f) vol = 0.0f;
	if (vol > 1.0f) vol = 1.0f;
#ifdef USE_SDL_MIXER
	if ( type == atSound )
	{
		Mix_VolumeChunk( chunk, static_cast<int>( floor(vol*MIX_MAX_VOLUME) ) );
	}
	else
	{
		Mix_VolumeMusic( static_cast<int>( floor(music_volume*vol*MIX_MAX_VOLUME) ) );
	}
	return true;
#endif
#ifndef NOSOUND_BASS
	DWORD attrib = BASS_ATTRIB_VOL;
	//BASS_ATRIB_MUSIC_VOL doesn't work for .it music for some reason, but BASS_ATTRIB_VOL seems to work just fine.
	//Weird.
	//if ( type == atMusic ) attrib = BASS_ATTRIB_MUSIC_VOL_CHAN;
	if (BASS_ChannelSetAttribute(this->bassHandle, attrib, vol))
		return true;
	else
		LogBassError(__FUNCTION__);
#endif // NOSOUND_BASS
	return false;
}

bool Sound::SetLooped(bool loop)
{
#ifdef NOSOUND_BASS
	UNUSED_ARG(loop);
#else
	DWORD flag = loop ? BASS_SAMPLE_LOOP : 0;
	if ( BASS_ChannelFlags(this->bassHandle, flag, BASS_SAMPLE_LOOP) )
		return true;
	else
		LogBassError(__FUNCTION__);
#endif // NOSOUND_BASS
	return false;
}

uint32_t Sound::GetLevel()
{
#ifndef NOSOUND_BASS
	return BASS_ChannelGetLevel( this->bassHandle );
#else
	return 0;
#endif //NOSOUND_BASS
}

double Sound::GetLength()
{
#ifndef NOSOUND_BASS
	uint64_t bytes = BASS_ChannelGetLength( this->bassHandle, BASS_POS_BYTE );
	return BASS_ChannelBytes2Seconds( this->bassHandle, bytes ); 
#else
#ifdef USE_SDL_MIXER
	if ( type == atSound )
	{
		return (chunk->alen / MIX_DEFAULT_FORMAT / 2.0);
	}
#endif //USE_SDL_MIXER
	return 0;
#endif //NOSOUND_BASS
}

float Sound::GetSpeed()
{
#ifndef NOSOUND_BASS
	if ( this->type == atMusic )
	{
		float speed = 0.0f;
		BASS_ChannelGetAttribute( this->bassHandle, BASS_ATTRIB_MUSIC_SPEED, &speed );
		return speed;
	}
	else return 0.0f;
#else
	return 0.0f;
#endif //NOSOUND_BASS
}

void Sound::SetSpeed( float speed )
{
#ifndef NOSOUND_BASS
	if ( this->type == atMusic )
	{
		BASS_ChannelSetAttribute( this->bassHandle, BASS_ATTRIB_MUSIC_SPEED, speed );
	}
	else return;
#else
	return;
#endif //NOSOUND_BASS
}

float* Sound::GetFFT()
{
	float* buffer = new float[256];
	memset( buffer, 0, 256*sizeof(float) );
#ifndef NOSOUND_BASS
	BASS_ChannelGetData( this->bassHandle, buffer, BASS_DATA_FFT512 );
#endif //NOSOUND_BASS
	return buffer;
}

bool Sound::Load()
{
#ifdef USE_SDL_MIXER
	if ( buffer == NULL )
		return false;

	if (_buffer) DELETEARRAY(buffer);
	_buffer = new char[buffer_size];
	memcpy(_buffer, buffer, buffer_size);
	_buffer_size = buffer_size;
	if ( !rwops ) rwops = SDL_RWFromConstMem(_buffer, _buffer_size);

	if ( loading_music )
	{
		music_chunk = Mix_LoadMUS_RW(rwops);
		if ( music_chunk )
		{
			type = atMusic;
			return true;
		}
	}
	else
	{
		chunk = Mix_LoadWAV_RW(rwops, 0);
		if ( chunk )
		{
			type = atSound;
			return true;
		}
	}
	Log(DEFAULT_LOG_NAME, logLevelError, "Error with '%s'", name.c_str());
	LogSDLMixerError(__FUNCTION__);
	return false;
#endif
#ifndef NOSOUND_BASS
	if ( buffer == NULL )
		return false;
	
	bassHandle = BASS_MusicLoad(TRUE, buffer, 0, static_cast<DWORD>(buffer_size), BASS_MUSIC_PRESCAN, 0);

	type = atMusic;
	if (!bassHandle)
	{
		_buffer_size = buffer_size;
		if ( _buffer ) DELETEARRAY( _buffer );
		_buffer = new char[ _buffer_size ];
		memcpy( _buffer, buffer, _buffer_size );
		bassHandle = BASS_StreamCreateFile(TRUE, _buffer, 0, _buffer_size, 0);
		type = atSound;
	}
	if (bassHandle)	
		return true;
	else
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "Error with '%s'", name.c_str());
		LogBassError(__FUNCTION__);
	}
#endif // NOSOUND_BASS

	return false;
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


SoundMgr::SoundMgr(const char* path, const char* extension, vector<string>* archive_list, vector<uint32_t>* crc_list) : ResourceMgr<Sound>(path, extension, archive_list, crc_list)
{
	initialized = false;
	Initialize();
}

//SND* snd = NULL;
SoundMgr::~SoundMgr()
{
	Destroy();
}

bool SoundMgr::Initialize()
{
	if (initialized)
		Destroy();

#ifndef NOSOUND_BASS
	if (BASS_Init(-1, 44100, 0, 0, NULL))
	{
		return initialized = true;
	}
	else
	{
		LogBassError(__FUNCTION__);
		initialized = false;		
	}
#endif // NOSOUND_BASS
#ifdef USE_SDL_MIXER
#ifdef SDL_MIXER_HAS_MIX_INIT
	int flags = MIX_INIT_OGG | MIX_INIT_MOD;
	if ( Mix_Init( flags ) != flags )
	{
		LogSDLMixerError(__FUNCTION__);
		return initialized = false;
	}
	else
#endif //SDL_MIXER_HAS_MIX_INIT
	{
		if ( Mix_OpenAudio( MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024 ) == -1 )
		{
			LogSDLMixerError(__FUNCTION__);
			return initialized = false;
		}
		Mix_AllocateChannels( 256 );
		Mix_ChannelFinished( &channel_finished );
		Mix_HookMusicFinished( &music_finished );
		return initialized = true;
	}
#endif
	return false;
}

void SoundMgr::Destroy()
{
#ifndef NOSOUND_BASS
	if (initialized)
	{
		if (!BASS_Free())
			LogBassError(__FUNCTION__);
		initialized = false;
	}
#endif // NOSOUND_BASS
#ifdef USE_SDL_MIXER
	if (initialized)
	{
#ifdef SDL_MIXER_HAS_MIX_INIT
		while (Mix_Init(0))
			Mix_Quit();
#endif //SDL_MIXER_HAS_MIX_INIT
		initialized = false;
	}
#endif //USE_SDL_MIXER
}

bool SoundMgr::PlaySnd(const string& name, bool restart, float volume )
{
	if (!initialized) return false;
	if ( volume <= 0.0f ) return false;
#ifdef USE_SDL_MIXER
	loading_music = false;
#endif
	Sound* snd = GetByName(name);
	bool result = false;
	if (snd)
	{
		result = snd->Play(restart);
		if (result) 
		{
			snd->SetVolume( cfg.volume_sound * min( volume, 1.0f ) );
		}
	}
	return result;
}

extern float CAMERA_X;
extern float CAMERA_Y;

bool SoundMgr::PlaySnd(const string& name, bool restart, Vector2 origin, float volume )
{
	if (!initialized) return false;
	if ( volume <= 0.0f ) return false;
#ifdef USE_SDL_MIXER
	loading_music = false;
#endif
	Sound* snd = GetByName(name);
	if (snd)
	{
		Vector2 delta = origin - Vector2(CAMERA_X, CAMERA_Y);
		float dist = delta.Length();
		if ( dist > MAXIMUM_SOUND_DISTANCE )
			return true;
		dist = 1 - dist/MAXIMUM_SOUND_DISTANCE;
		dist *= cfg.volume_sound;
		bool result = false;
		result = snd->Play(restart);
		if ( result )
		{
			snd->SetVolume( dist * min( volume, 1.0f ) );
			snd->SetPan( delta.x / MAXIMUM_SOUND_DISTANCE );
		}
		return result;
	}
	return false;
}

bool SoundMgr::StopSnd(const string& name)
{
	if (!initialized) return false;
	if (name.empty())
		return false;
	const Sound* snd = GetByName(name);
	if (snd)
		return snd->Stop();
	return false;
}

bool SoundMgr::PauseSnd(const string& name)
{
	if (!initialized) return false;
	const Sound* snd = GetByName(name);
	if (snd)
		return snd->Pause();
	return false;
}

float SoundMgr::GetVolume()
{
#ifdef USE_SDL_MIXER
	return cfg.volume;
#endif
#ifndef NOSOUND_BASS
	if (initialized)
	{
		
		unsigned long vol = BASS_GetConfig(BASS_CONFIG_GVOL_MUSIC);
		//if (vol != -1)
		if (!BASS_ErrorGetCode())
			return (float)vol / 10000;
		else
			LogBassError(__FUNCTION__);
	}
#endif // NOSOUND_BASS
	return 0.0f;
}

float SoundMgr::GetSoundVolume()
{
	return cfg.volume_sound;
}

float SoundMgr::GetMusicVolume()
{
	return cfg.volume_music;
}

void SoundMgr::SetSoundVolume(float value)
{
	cfg.volume_sound = floor((value*10)+0.5f)/10.0f;
#ifdef USE_SDL_MIXER
	Mix_Volume( -1, static_cast<int>(floor(cfg.volume * cfg.volume_sound * MIX_MAX_VOLUME)) );
#endif
}

void SoundMgr::SetMusicVolume(float value)
{
	cfg.volume_music = floor((value*10)+0.5f)/10.0f;
	SetMusVolume( cfg.volume_music );
}

bool SoundMgr::SetVolume(float vol)
{
	if (!this->initialized)
		return false;
	
	if (vol < 0.0f) vol = 0.0f;
	if (vol > 1.0f) vol = 1.0f;
	cfg.volume = vol;
#ifdef USE_SDL_MIXER
	Mix_Volume( -1, static_cast<int>(floor(cfg.volume * cfg.volume_sound * MIX_MAX_VOLUME)) );
	if ( current_music )
	{
		current_music->SetVolume( cfg.volume * cfg.volume_music );
	}
#endif
#ifndef NOSOUND_BASS
	if (BASS_SetConfig(BASS_CONFIG_GVOL_MUSIC, (DWORD)(vol*10000)) && 
		BASS_SetConfig(BASS_CONFIG_GVOL_SAMPLE, (DWORD)(vol*10000)) &&
		BASS_SetConfig(BASS_CONFIG_GVOL_STREAM, (DWORD)(vol*10000)) )		
		return true;
	else
		LogBassError(__FUNCTION__);	
#endif // NOSOUND_BASS
	return false;
}

float SoundMgr::GetSndVolume(string name)
{
	if (!initialized) return 0.0f;
	const Sound* snd = GetByName(name);
	if (snd)
		return snd->GetVolume();
	return 0.0f;
}

bool SoundMgr::SetSndVolume(string name, float vol)
{
	if (!initialized) return false;
	Sound* snd = GetByName(name);
	if (snd)
		return snd->SetVolume(vol);
	return false;
}

bool SoundMgr::SetMusVolume(float vol)
{
	if (!initialized) return false;
	std::string mus = GetCurrentBackMusic();
	if ( mus == "" ) return true;
	Sound* snd = GetByName(mus);
	if (snd)
		return snd->SetVolume( vol );
	return false;
}

char* SoundMgr::GetBackMusic()
{
	std::string mus = GetCurrentBackMusic();
	if ( mus == "" ) return NULL;
	return StrDupl(mus.c_str());
}

bool SoundMgr::PlayBackMusic(string name, float volume, bool loop)
{
	if (!initialized) return false;
	this->StopBackMusic();
#ifdef USE_SDL_MIXER
	loading_music = true;
#endif
	Sound* snd = GetByName(name);
	
	if (snd && 
		((loop && snd->PlayLooped(true)) |
		(!loop && snd->Play(true))))
	{
#ifdef USE_SDL_MIXER
		snd->SetMusicVolume( volume );
#endif
		snd->SetVolume( cfg.volume_music );
		SetCurrentBackMusic(name);
		return true;
	}
	return false;
}

bool SoundMgr::PauseBackMusic()
{
	return PauseSnd(GetCurrentBackMusic());
}

bool SoundMgr::ResumeBackMusic()
{
	return PlaySnd(GetCurrentBackMusic(), false );
}


bool SoundMgr::StopBackMusic()
{
	bool ret = StopSnd(GetCurrentBackMusic());
	SetCurrentBackMusic(string(""));
	return ret;
}

void SoundMgr::SetCurrentBackMusic(string newMusic)
{
	currBackMusic = newMusic;
}

uint32_t SoundMgr::GetBackMusicLevel()
{
	if (!initialized) return 0;
	std::string mus = GetCurrentBackMusic();
	if ( mus == "" ) return 0;
	Sound* snd = GetByName(mus);
	if (!snd) return 0;
	return snd->GetLevel();
}

double SoundMgr::GetBackMusicLength()
{
	if (!initialized) return 0;
	std::string mus = GetCurrentBackMusic();
	if ( mus == "" ) return 0;
	Sound* snd = GetByName(mus);
	if (!snd) return 0;
	return snd->GetLength();
}

double SoundMgr::GetSndLength(string name)
{
	if (!initialized) return 0.0;
	Sound* snd = GetByName(name);
	if (snd)
		return snd->GetLength();
	return -1.0;
}

float* SoundMgr::GetBackMusicFFT()
{
	if (!initialized) return NULL;
	std::string mus = GetCurrentBackMusic();
	if ( mus == "" ) return NULL;
	Sound* snd = GetByName(mus);
	if (!snd) return NULL;
	return snd->GetFFT();
}

const string& SoundMgr::GetCurrentBackMusic() const
{
	return currBackMusic;
}

void SoundMgr::StopAll()
{
	if (!initialized) return;
	if (resMap.size() == 0)
		return;
	
	for (ResMapIter it = resMap.begin(); it != resMap.end(); it++)
	{
		it->second->Stop();
	}
}

bool SoundMgr::PauseAll()
{
#ifndef NOSOUND_BASS
	if (this->initialized)
	{
		if (BASS_Pause())
			return true;
		else
			LogBassError(__FUNCTION__);
	}
#endif // NOSOUND_BASS
#ifdef USE_SDL_MIXER
	Mix_Pause( -1 );
	if ( GetCurrentBackMusic() != "" )
	{
		Mix_PauseMusic();
	}
#endif
	return false;
}

bool SoundMgr::ResumeAll()
{
#ifndef NOSOUND_BASS
	if (this->initialized)
	{
		if (BASS_Start())
			return true;
		else
			LogBassError(__FUNCTION__);
	}
#endif // NOSOUND_BASS
#ifdef USE_SDL_MIXER
	Mix_Resume( -1 );
	if ( GetCurrentBackMusic() != "" )
	{
		Mix_ResumeMusic();
	}
#endif
	return false;
}

float SoundMgr::GetMusicSpeed()
{
#ifndef NOSOUND_BASS
	if (this->initialized)
	{
		if (!initialized) return 0.0f;
		std::string mus = GetCurrentBackMusic();
		if ( mus == "" ) return 0.0f;
		Sound* snd = GetByName(mus);
		if (!snd) return 0.0f;
		return snd->GetSpeed();
	}
#endif // NOSOUND_BASS
	return 0.0f;
}

void SoundMgr::SetMusicSpeed(float value)
{
#ifndef NOSOUND_BASS
	if (this->initialized)
	{
		if (!initialized) return;
		std::string mus = GetCurrentBackMusic();
		if ( mus == "" ) return;
		Sound* snd = GetByName(mus);
		if (!snd) return;
		snd->SetSpeed( value );
	}
#endif // NOSOUND_BASS
	return;
}

