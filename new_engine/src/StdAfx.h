#if !defined(AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_)
#define AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#if !defined(NDEBUG) && !defined(_DEBUG)
# define _DEBUG 1
#endif

#if defined(DEBUG) && !defined(_DEBUG)
# define _DEBUG 1
#endif

#if defined(_DEBUG) && !defined(DEBUG)
# define DEBUG 1
#endif

#ifdef WIN32
	#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
	#define _CRT_SECURE_NO_WARNINGS
	#define _CRT_SECURE_NO_DEPRECATE			//
	#define _CRT_RAND_S
	#define _DEBUG_

	#ifdef _DEBUG

		//// For iterator check disabling in debug builds
		//#define _HAS_ITERATOR_DEBUGGING 0

		#define FIND_MEM_LEAKS

		#ifdef FIND_MEM_LEAKS
			// ����� ������ ������
			// � VS2005 ����������� _CRTDBG_MAP_ALLOC �� ���� ������� ������ ��-�� ������
			// � ������ �����������. �������������, ��� ���� ����������������.
			// � VS2005 SP1 ��� ���������.
			// � �����, _CRTDBG_MAP_ALLOC �� ��� �� � �����.
			#define _CRTDBG_MAP_ALLOC
			#include <crtdbg.h>
		#endif // FIND_MEM_LEAKS
	#endif // _DEBUG

#include <windows.h>

#endif // WIN32


#define __STDC_LIMIT_MACROS		// ����� ��� g++ ��� SIZE_MAX

#include <ctime>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#define _USE_MATH_DEFINES  // ��� ������ �������� ���� M_PI, M_SQRT2
#include <math.h>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <algorithm>
#include <limits>
#include <limits.h>

#ifdef FIND_MEM_LEAKS
	// ��� ��� ��������� new ��������� �� ���� ���������� ������, � ���� ������
	// ����� ���������� ���������� � ����� � ������, �� ������� ��� ����� new.
	//#define DEBUG_CLIENTBLOCK   new( _NORMAL_BLOCK, __FILE__, __LINE__)
	//#define new DEBUG_CLIENTBLOCK
#endif // FIND_MEM_LEAKS

#if defined(__APPLE__)
#include </usr/include/malloc/malloc.h>
#elif defined(__FreeBSD__)
#else
#include <malloc.h>
#endif

#include <assert.h>

#include "types.h"
#include "defines.h"

using namespace std;

// for compatibility with 2005 studio
#if !defined DISP_CHANGE_BADDUALVIEW
#define DISP_CHANGE_BADDUALVIEW -6
#endif

#endif // !defined(AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_)
