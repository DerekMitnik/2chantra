#include "StdAfx.h"


#include "input_mgr.h"
#include "config.h"

//////////////////////////////////////////////////////////////////////////

extern UINT internal_time;

//////////////////////////////////////////////////////////////////////////



// �������� �� ���� ������ unicode � �������� ������� ������ ��������� �� CP1251.
__INLINE Uint16 GetCyrillicCP1251(Uint16 symb)
{
	if (0x0410 <= symb && symb <= 0x044f)
		return symb - 0x350;	// �������� ������� ������ ���� �������
	else if (symb == 0x0401)
		return 0x00a8;			// �
	else if (symb == 0x0451)
		return 0x00b8;			// �
	else
		return (Uint16)'?';		// ��� ��������� ������� ��������������� �������
}



InputMgr::InputMgr()
{
	memset(pressed, false, keys_count);
	memset(holded, false, keys_count);
	memset(released, false, keys_count);

	mouseX = 0;
	mouseY = 0;
	mouseX_last = 0;
	mouseY_last = 0;
	mouse_moved = false;
	memset(mouse, false, mousebtns_count);

	memset( joystick_axis_pressed, false, sizeof(joystick_axis_pressed) );
	memset( joy_hat, false, sizeof(joy_hat) );

	repeatingState = rs_none;
	repeat_timer = 0;
	repeatTime = 30;
	repeatWaitingTime = 500;
	event_to_repeat = NULL;
}

InputMgr::~InputMgr()
{
	FlushEvents();
	if (event_to_repeat) //����� �� ��������� ������.
		DELETESINGLE(event_to_repeat);
}

void InputMgr::PreProcess()
{
	ArrayFlush();
	
	mouse_moved = false;

	FlushEvents();
}



void InputMgr::Process()
{
	if ((mouseX != mouseX_last) || (mouseY != mouseY_last))
	{
		// ��� ���������� ������ ��� �������. ��������� MOUSEMOVED ���������� ����,
		// ����� �� �������� �����.
		mouseX_last = mouseX;
		mouseY_last = mouseY;
		mouse_moved = true;
	}

	if (repeatingState == rs_waiting && internal_time - repeat_timer >= repeatWaitingTime)
	{
		// ���������, ����� ���������
		repeat_timer = internal_time - repeatTime;
		repeatingState = rs_repeating;
	}
	if (repeatingState == rs_repeating && internal_time - repeat_timer >= repeatTime)
	{
		// ��������� ���������� ����������
		assert(event_to_repeat);
		repeat_timer = internal_time;
		InputEvent* new_ev = new InputEvent(*event_to_repeat);
		repeated[event_to_repeat->kb.key] = true;
		events.push_back(new_ev);
	}
	
	if (registered_inputs.size() == 1)
	{
		for ( int i = 0; i < cakLastKey; i++ )
		{
			registered_inputs[0]->held[i] = IsHolded( (ConfigActionKeys)i );
			registered_inputs[0]->released[i] = IsProlongedReleased( (ConfigActionKeys)i );
		}
	}
	else
	{
		for ( vector<Input*>::iterator iter = registered_inputs.begin(); iter != registered_inputs.end(); ++iter )
		{
			for ( int i = 0; i < cakLastKey; i++ )
			{
				(*iter)->held[i] = IsHolded( (ConfigActionKeys)i, (*iter)->key_set );
				(*iter)->released[i] = IsProlongedReleased( (ConfigActionKeys)i, (*iter)->key_set );
			}
		}
	}
}

void InputMgr::ClearInput()
{
	registered_inputs.clear();
}

void InputMgr::UnregisterInput( Input* inp )
{
	for ( vector<Input*>::iterator iter = registered_inputs.begin(); iter != registered_inputs.end(); ++iter )
	{
		if ( inp == (*iter) )
		{
			registered_inputs.erase( iter );
			break;
		}
	}
}

void InputMgr::RegisterInput( Input* inp, size_t key_set )
{
	assert(inp);
	UnregisterInput( inp );
	registered_inputs.push_back( inp );
	inp->key_set = key_set;
}

void InputMgr::AddEvent(SDL_KeyboardEvent &ev)
{
	InputEvent* ie = new InputEvent;
	ie->type = InputKBoard;

	ie->kb.key = ev.keysym.sym;

	if( ev.keysym.sym == SDLK_UNKNOWN )
	{
		delete ie;
		return;
	}

	if ( ev.keysym.unicode & 0xFF80 )
	{
		// ������, �� �������� � ������ �������� ASCII. ��������� ���������� � ��������� CP1251.
		ie->kb.symbol = GetCyrillicCP1251(ev.keysym.unicode);
	}
	else
	{
		ie->kb.symbol = ev.keysym.unicode;
	}


	if (ev.type == SDL_KEYDOWN)
	{
		ie->state = InputStatePressed;
		pressed[ev.keysym.sym] = true;
		holded[ev.keysym.sym] = true;
		released[ev.keysym.sym] = false;

		//prolonged_pressed[ev.keysym.sym] = true;

		if (!event_to_repeat || (event_to_repeat && event_to_repeat->kb.key != ie->kb.key))
		{
			// ���������� ������� ��� ����������
			DELETESINGLE(event_to_repeat);
			event_to_repeat = new InputEvent(*ie);
			repeatingState = rs_waiting;
			repeat_timer = internal_time;
		}

		events.push_back(ie);
		return;
	}
	else
	if (ev.type == SDL_KEYUP)
	{
		ie->state = InputStateReleased;
		pressed[ev.keysym.sym] = false;
		holded[ev.keysym.sym] = false; 
		released[ev.keysym.sym] = true;

		prolonged_released[ev.keysym.sym] = true;

		if (event_to_repeat && event_to_repeat->kb.key == ie->kb.key)
		{
			// ��������� �������, ������� ���������. ������ ��������� �� ����
			repeatingState = rs_none;
			DELETESINGLE(event_to_repeat);
		}

		events.push_back(ie);
		return;
	}
	delete ie;
	return;
}

void InputMgr::AddEvent(SDL_MouseButtonEvent &ev)
{
	InputEvent* ie = new InputEvent;
	ie->type = InputMouse;

	bool st = (ev.type == SDL_MOUSEBUTTONDOWN ? true : false);
	switch (ev.button)
	{
	case SDL_BUTTON_LEFT:
		mouse[MOUSE_BTN_LEFT] = st;
		ie->mouse.key = MOUSE_BTN_LEFT;
		break;
	case SDL_BUTTON_RIGHT:
		mouse[MOUSE_BTN_RIGHT] = st;
		ie->mouse.key = MOUSE_BTN_RIGHT;
		break;
	case SDL_BUTTON_MIDDLE:
		mouse[MOUSE_BTN_MIDDLE] = st;
		ie->mouse.key = MOUSE_BTN_MIDDLE;
		break;
	}

	ie->state = st ? InputStatePressed : InputStateReleased;
	events.push_back(ie);
}

extern config cfg;

void InputMgr::AddEvent(SDL_JoyAxisEvent &ev)
{
	if ( ev.axis > MAX_JOYSTICK_AXES )
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "Joysticks with more than %i axes are not fully supported, sorry. (Do such joysticks even exist?)", MAX_JOYSTICK_AXES);
		return;
	}
	if (!joystick_axis_pressed[ev.which][ev.axis] )
	{
		if ( static_cast<UINT>(abs(ev.value)) < cfg.joystick_sensivity ) return;
		joystick_axis_pressed[ev.which][ev.axis] = true;
		InputEvent* ie = new InputEvent;
		ie->type = InputKBoard;
		
		int index = SDLK_LAST + ev.which * JOYSTICK_VK +  2 * ev.axis + 1;
		if (ev.value < 0 )
			index += 1;
		ie->kb.key = (SDLKey)index;
		ie->kb.symbol = 0;
				
		ie->state = InputStatePressed;
		pressed[index] = true;
		holded[index] = true;
		released[index] = false;

		//prolonged_pressed[index] = true;

		events.push_back(ie);
	}
	else
	{
		if ( static_cast<UINT>(abs(ev.value)) > cfg.joystick_sensivity ) return;
		joystick_axis_pressed[ev.which][ev.axis] = false;
		InputEvent* ie;
				
		int index = SDLK_LAST + ev.which * JOYSTICK_VK + 2 * ev.axis + 1;

		if ( holded[index] )
		{
			ie = new InputEvent;
			ie->type = InputKBoard;
			ie->kb.key = (SDLKey)index;
			ie->kb.symbol = 0;
			ie->state = InputStateReleased;
			pressed[index] = false;
			holded[index] = false;
			released[index] = true;
			prolonged_released[index] = true;

			events.push_back(ie);
		}

		index++;

		if ( holded[index] )
		{
			ie = new InputEvent;
			ie->type = InputKBoard;
			ie->kb.key = (SDLKey)index;
			ie->kb.symbol = 0;
			ie->state = InputStateReleased;
			pressed[index] = false;
			holded[index] = false;
			released[index] = true;
			prolonged_released[index] = true;

			events.push_back(ie);
		}
		
	}
}

void InputMgr::AddEvent(SDL_JoyHatEvent &ev)
{
	bool joy_hat_new[4];
	for ( int i = 0; i < 4; i++ ) joy_hat_new[i] = false;
	switch( ev.value )
	{
		case SDL_HAT_LEFT: joy_hat_new[2] = true; break;
		case SDL_HAT_UP: joy_hat_new[0] = true; break;
		case SDL_HAT_DOWN: joy_hat_new[1] = true; break;
		case SDL_HAT_RIGHT: joy_hat_new[3] = true; break;
		case SDL_HAT_LEFTUP: joy_hat_new[2] = joy_hat_new[0] = true; break;
		case SDL_HAT_LEFTDOWN: joy_hat_new[2] = joy_hat_new[1] = true; break;
		case SDL_HAT_RIGHTUP: joy_hat_new[3] = joy_hat_new[0] = true; break;
		case SDL_HAT_RIGHTDOWN: joy_hat_new[3] = joy_hat_new[1] = true; break;
	}
	for ( int i = 0; i < 4; i++ )
	{
		if ( joy_hat_new[i] != joy_hat[ev.which][i] )
		{
			InputEvent* ie = new InputEvent;
			ie->type = InputKBoard;
			int index = SDLK_LAST + (ev.which * JOYSTICK_VK) + (MAX_JOYSTICK_AXES * 2) + MAX_JOYSTICK_BUTTONS + 1;
			ie->kb.key = static_cast<SDLKey>(index+i);
			ie->kb.symbol = 0;
			pressed[index+i] = joy_hat_new[i];
			holded[index+i] = joy_hat_new[i];
			released[index+i] = !joy_hat_new[i] & joy_hat[ev.which][i];
			if ( released[index+i] ) 
				ie->state = InputStateReleased;
			else
				ie->state = InputStatePressed;
			events.push_back(ie);
		}
	}
	for ( int i = 0; i < 4; i++ )
	{
		joy_hat[ev.which][i] = joy_hat_new[i];
	}
}

void InputMgr::AddEvent(SDL_JoyButtonEvent &ev)
{
	if ( ev.button > MAX_JOYSTICK_BUTTONS )
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "Joysticks with more than %i buttons are not fully supported, sorry.", MAX_JOYSTICK_BUTTONS);
		return;
	}
	InputEvent* ie = new InputEvent;
	ie->type = InputKBoard;

	int index = SDLK_LAST + ev.which * JOYSTICK_VK + MAX_JOYSTICK_AXES * 2 + 1 + ev.button;
	ie->kb.key = static_cast<SDLKey>(index);
	ie->kb.symbol = 0;
	if (ev.type == SDL_JOYBUTTONDOWN)
	{
		ie->state = InputStatePressed;
		pressed[index] = true;
		holded[index] = true;
		released[index] = false;

		//prolonged_pressed[index] = true;
	}
	else
	{
		ie->state = InputStateReleased;
		pressed[index] = false;
		holded[index] = false;
		released[index] = true;

		prolonged_released[index] = true;
	}

	events.push_back(ie);
}

void InputMgr::AddEvent(SDL_MouseMotionEvent &ev)
{
	extern UINT viewport_x;
	extern UINT viewport_y;

	mouseX = static_cast<int>((((int)ev.x - (int)viewport_x) * scrwinX));
	mouseY = static_cast<int>((((int)ev.y - (int)viewport_y) * scrwinY));
}

void InputMgr::SetScreenToWindowCoeff(float kx, float ky)
{
	mouseX = static_cast<int>((mouseX / scrwinX) * kx);
	mouseY = static_cast<int>((mouseY / scrwinY) * ky);

	scrwinX = kx;
	scrwinY = ky;
}

// ������� ������ ����������� ������� �����
void InputMgr::FlushEvents()
{
	if (events.empty())
		return;
	else
	{
		EventsIterator it;
		for (it = events.begin(); it != events.end(); it++)
		{
			DELETESINGLE((*it));
		}
		events.clear();
	}
}

void InputMgr::ArrayFlush()
{
	//memcpy(holded, pressed, keys_count * sizeof(pressed[0]));
	memset(pressed, false, keys_count);
	memset(released, false, keys_count);
	memset(repeated, false, keys_count);
}

void InputMgr::ProlongedArrayFlush()
{
	//memset(prolonged_pressed, false, keys_count);
	memset(prolonged_released, false, keys_count);
}

bool InputMgr::CheckKeyInArray(const bool* arr, ConfigActionKeys key) const
{
	for (size_t i = 0; i < KEY_SETS_NUMBER; i++)
	{
		if (arr[ cfg.cfg_keys[i][key] ])
			return true;
	}
	return false;
}

bool InputMgr::CheckKeyInArray(const bool* arr, ConfigActionKeys key, size_t key_set) const
{
	return arr[ cfg.cfg_keys[key_set][key] ];
}

bool InputMgr::IsPressed(ConfigActionKeys key) const
{
	return CheckKeyInArray(pressed, key);
}
bool InputMgr::IsReleased(ConfigActionKeys key) const
{
	return CheckKeyInArray(released, key);
}

bool InputMgr::IsHolded(ConfigActionKeys key) const
{
	return CheckKeyInArray(holded, key);
}

bool InputMgr::IsRepeated(ConfigActionKeys key) const
{
	return CheckKeyInArray(repeated, key);
}

bool InputMgr::IsProlongedReleased(ConfigActionKeys key) const
{
	return CheckKeyInArray(prolonged_released, key);
}

bool InputMgr::IsPressed(ConfigActionKeys key, size_t key_set) const
{
	return CheckKeyInArray(pressed, key, key_set);
}
bool InputMgr::IsReleased(ConfigActionKeys key, size_t key_set) const
{
	return CheckKeyInArray(released, key, key_set);
}

bool InputMgr::IsHolded(ConfigActionKeys key, size_t key_set) const
{
	return CheckKeyInArray(holded, key, key_set);
}

bool InputMgr::IsRepeated(ConfigActionKeys key, size_t key_set) const
{
	return CheckKeyInArray(repeated, key, key_set);
}

bool InputMgr::IsProlongedReleased(ConfigActionKeys key, size_t key_set) const
{
	return CheckKeyInArray(prolonged_released, key, key_set);
}

bool InputMgr::IsHolded(UINT key) const
{
	return holded[key];
}
