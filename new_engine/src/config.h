#ifndef __CONFIG_H_
#define __CONFIG_H_

#include "misc.h"

// ������ ��� ������, ������� �������� � �������.
// ��� ���������� ����� ������ � ���� ����������:
// 1. ���� �� ������� � ���� ������ (cakLastKey - ������ ��������� � ������� �� ��������)
// 2. ���� � ������ configKeysNames ��� ����, ������� ����� ��������� � ����� �������.
// 3. � ������ standartKeys ������ ����������� �������� ��� ������.
enum ConfigActionKeys
{
	cakLeft, cakRight, cakDown, cakUp, cakJump, cakSit, cakFire, cakAltFire,
	cakChangeWeapon, cakChangePlayer, 
	cakGuiNavAccept, cakGuiNavDecline,
	cakGuiNavPrev, cakGuiNavNext,
	cakGuiNavMenu,
	cakScreenshot,
	cakPlayerUse,
	cakWeaponSlot1,
	cakWeaponSlot2,
	cakWeaponSlot3,
	cakWeaponSlot4,

	cakLastKey		// ������������ ��� ������������ ������� �������
};

typedef struct tagCONFIG {
	unsigned int scr_width;
	UINT scr_height;
	UINT scr_bpp;
	float aspect_ratio;

	float near_z;
	float far_z;

	UINT window_width;
	UINT window_height;

	UINT full_width;
	UINT full_height;

	UINT joystick_sensivity;

	BOOL fullscreen;
	BOOL vert_sync;

	BOOL debug;
	BOOL show_fps;


	float backcolor_r;
	float backcolor_g;
	float backcolor_b;

	float volume;
	float volume_music;
	float volume_sound;

#ifdef GAMETICK_FROM_CONFIG
	UINT gametick;
#endif // GAMETICK_FROM_CONFIG

	UINT cfg_keys[KEY_SETS_NUMBER][cakLastKey];


	UINT gui_nav_mode;
	UINT gui_nav_cycled;

	UINT shadows;
	
	LogLevel log_level;

	char* language;
	BOOL weather;

	char* nick;
	float color_r;
	float color_g;
	float color_b;

	tagCONFIG()
	{
		memset(this, 0, sizeof(*this));
		joystick_sensivity = 1000;
	}

	~tagCONFIG()
	{
		DELETEARRAY(language);
		DELETEARRAY(nick);
	}
} config;

bool LoadDefaultConfig();
bool LoadConfig();
bool SaveConfig( const char* filename = NULL );
bool RecreateConfig();

void InitVKeysArrays();
void ClearVKeysArrays();
UINT GetNumByVKey(string s);
bool GetVKeyByNum(UINT num, char* vk);

const char* GetConfigKeyName(ConfigActionKeys key);

#endif
