#ifndef __OBJECT_MGR_H_
#define __OBJECT_MGR_H_

#include "objects/object.h"

void AddObject(GameObject* obj, UINT id = 0);
void RemoveObject(GameObject* obj);

void BatchAdd();
void BatchRemove();

void PrepareRemoveAllObjects();
void RemoveAllObjects();

GameObject* GetGameObject(UINT id);
void GroupObjects( int objGroup, int objToAdd );

void ProcessAllActiveObjects();
void ProcessAllPassiveObjects();

void DrawAllObjects();
#ifdef SELECTIVE_RENDERING
void selectForRenederUpdate(GameObject const* obj);
#endif

#endif // __OBJECT_MGR_H_