#ifndef __NET_H
#define __NET_H

#include "phys/2de_Math.h"
#include "phys/2de_Vector2.h"

#include <SDL/SDL.h>

class GameObject;

namespace Net
{
	struct NetPlayer;
	
	enum NetEventType
	{
		objectCreated, 
		objectDestroyed, 
		objectUpdated, 
		
		playerInfo,
		inputUpdated, 
		chatMessage, 
		
		playerUpdated,
		
		eventBroadcast,
		idBatchRequest,
		idBatchConfirmed,
		
		pingPacket,
		pongPacket
	};
	
	enum NetPacketPriority{ nppNormal, nppImportant, nppCritical };
	//Normal - ��������� � ������.
	//Important - ����������, ���� �� ������� �������������
	//Critical - ���������� �� ������ ����������, ���� �� ������� �������������.
	
	struct LocalEvent
	{
		NetEventType type;
		
		GameObject* obj;
		GameObject* obj2;
		char* proto;
		Vector2 coord;
		int int_param;
		UINT uint_param;

		NetPlayer* origin_player; //Used not to repeat to the player, that originally sent it.
		uint8_t*  packet;

		LocalEvent( NetEventType type, GameObject* obj, char* proto ) : type( type ), obj( obj ), obj2(NULL), coord( 0, 0 ), int_param( 0 ), uint_param( 0 ), origin_player( NULL ), packet( NULL )
		{ 
			this->proto = proto;
		}

		LocalEvent( LocalEvent* that ) : type( that->type ), obj( that->obj ), obj2( that->obj2 ), proto( NULL ),
			coord( that->coord.x, that->coord.y ), int_param( that->int_param ), uint_param( that->uint_param ), 
			origin_player( that->origin_player ), packet( NULL )
		{
			if ( that->proto )
			{
				this->proto = new char[ strlen(that->proto) + 1 ];
				memcpy( this->proto, that->proto, strlen( that->proto )+1 );
			}
			if ( that->packet )
			{
				this->packet = new uint8_t[ that->uint_param ];
				memcpy( this->packet, that->packet, that->uint_param );
			}
		}

		~LocalEvent()
		{ 
			if ( proto ) DELETEARRAY( proto );
			if ( packet ) DELETEARRAY( packet );
		}
	};

	bool Init();
	bool Stop();
	bool Close();
	
	bool StartServer( uint16_t port );
	bool Connect( const char* host, uint16_t port );

	char* GetError();
	bool IsServer();
	
	void Update();

	uint32_t GetNextID();
	void RequestNewIDs();

	void ChatMessage( char* message );

	void AddEvent( LocalEvent* event );
}

#endif //__NET_H
