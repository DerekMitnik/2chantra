#ifndef __PROTO_H_
#define __PROTO_H_


#include "../render/render_types.h"
#include "phys/phys_misc.h"

#include "animation_frame.h"
#include "animation.h"

#include "../resource.h"
#include "phys/2de_Vector2.h"
#include "phys/2de_RGBA.h"
#include "phys/2de_Geometry.h"

enum PROTO_BEH { protoNullBeh, protoPlayer, protoEnemy, protoPowerup, protoSprite };
struct lua_State;

class Proto : public Resource
{
public:

	// sprite
	char*                texture;      //  particle
	UINT overlayCount;
	UINT* overlayUsage;
	RGBAf* ocolor;
	float                z;            // particle
	RGBAf color;
	RGBAf outline_color;
	UINT                 blendingMode;    // particle
	UINT renderMethod;
	UINT                 frame_widht;
	UINT                 frame_height;
	Animation*           animations;
	size_t               animationsCount;
	map<string, size_t>  animNames;
	USHORT               mpCount;
	vector<CPolygon*>    polygons;
	float                isometric_depth_x;
	float                isometric_depth_y;

	// physic
	UINT                 ghost_to;
	UINT                 phys_solid;
	UINT                 phys_one_sided;
	UINT                 phys_ghostlike;
	UINT                 phys_bullet_collidable;
	UINT                 physic;                 // particle
	UINT                 slopeType;
	UINT                 touch_detection;
	char*                material_name;
#ifdef MAP_EDITOR 
	RGBAf                editor_color;
#endif //MAP_EDITOR

	// dynamic
	float                shadow_width;
	UINT                 drops_shadow;
	float                mass;
	int                  facing;	///!< ��������� ��������� ������� ����������� � ����������
	float                phys_walk_acc;
	float                phys_jump_vel;
	float                phys_max_x_vel;
	float                phys_max_y_vel;
	float                bounce;                //  particle
	float                gravity_x;	        // particle
	float                gravity_y;         // particle

	// env
	float walk_vel_multiplier;
	float jump_vel_multiplier;
	float bounce_bonus;
	float friction;
	vector<char*> sounds;
	vector<char*> sprites;
	Vector2              gravity_bonus;
	UINT                 env_material;
	char*                env_onenter;
	char*                env_onleave;
	char*                env_onstay;
	char*                env_onuse;
	LuaRegRef            env_func_onenter;
	LuaRegRef            env_func_onleave;
	LuaRegRef            env_func_onstay;
	LuaRegRef            env_func_onuse;
	int                  stop_global_wind;
	Vector2              wind;	

	// bullet
	float                bullet_vel;
	UINT                 hurts_same_type;  // ray
	float                push_force;       // ray
	UINT                 bullet_damage;    // ray
	UINT                 multiple_targets; // ray
	UINT                 damage_type;      // ray

	// ray
	int                  ray_first_frame_shift;
	UINT                 time_to_live;
	char*                end_effect;
	char*                hit_effect;
	int                  next_shift_y;

	// character
	int faction_id;
	vector<int> faction_hates;
	vector<int> faction_follows;
	float                aiming_speed;

	// player
	float                air_control;
	float                walljump_vel;
	UINT                 recovery;
	UINT additional_jumps;
	UINT walljumps;
	UINT health;
	UINT                 max_health;
	UINT                 ammo;
	char* main_weapon;
	char* alt_weapon;
	
	// enemy
	float                offscreen_distance;
	int                  offscreen_behavior;
	
	// effect
	UINT                 effect;

	// weapon
	float                recoil; // !!!!!!!! copy
	char*                charge_effect;
	char*                flash;
	UINT reload_time;
	int bullets_count;
	UINT shots_per_clip;
	UINT clip_reload_time;
	UINT is_ray_weapon;
	UINT charge_hold;
	LuaRegRef fire_script;

	// particle
	RGBAf start_color;
	RGBAf end_color;
	RGBAf var_color;
	int max_particles;
	float particle_life_min;
	float particle_life_max;
	float start_size;
	float size_variability;
	float end_size;
	float particle_life;
	float particle_life_var;
	float system_life;
	float emission;
	float min_speed;
	float max_speed;
	float min_angle;
	float max_angle;
	float min_trace;
	float max_trace;
	float trace_width;
	float                min_param;         // dynamic
	float                max_param;         // dynamic
	int                  trajectory;        // dynamic
	float                trajectory_param1; // dynamic
	float                trajectory_param2; // dynamic
	int affected_by_wind;
	int max_bounce;
	int particle_anim;
	int particle_anim_param;
	float particle_max_abs_speed;
	
	//////////////////////////////////////////////////////////////////////////

	Proto(string name, unsigned char* buffer, size_t buffer_size);

	~Proto();	

	virtual bool Load();

	void LoadTable(lua_State* L);
	
private:

	void LoadSpriteFields(lua_State* L);
	void LoadSpriteOverlay(lua_State* L);
	void LoadSpriteAnims(lua_State* L);
	void LoadSpritePolygons(lua_State* L);
	void LoadPhysicFields(lua_State* L);
	void LoadDynamicFields(lua_State* L);
	void LoadEnvironmentFields(lua_State* L);
	void LoadEnvironmentSounds(lua_State* L);
	void LoadEnvironmentSprites(lua_State* L);
	void LoadBulletFields(lua_State* L);
	void LoadRayFields(lua_State* L);
	void LoadCharacterFields(lua_State* L);
	void LoadPlayerFields(lua_State* L);
	void LoadEnemyFields(lua_State* L);
	void LoadEffectFields(lua_State* L);
	void LoadWeaponFields(lua_State* L);
	void LoadParticleFields(lua_State* L);
	

	void Copy(const Proto& src);
};


#endif // __PROTO_H_


