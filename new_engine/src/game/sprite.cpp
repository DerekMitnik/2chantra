#include "StdAfx.h"

#include "../render/renderer.h"
#include "../config.h"

#include "sprite.h"

#include "../resource_mgr.h"

//////////////////////////////////////////////////////////////////////////

extern UINT internal_time;
extern ResourceMgr<Texture> * textureMgr;
extern ResourceMgr<Proto> * protoMgr;
extern float CAMERA_DRAW_LEFT;
extern float CAMERA_DRAW_TOP;
extern float CAMERA_DRAW_RIGHT;
extern float CAMERA_DRAW_BOTTOM;
extern config cfg;

bool render_additive_sprites = true;

void SetRenderAdditive( bool state )
{
	render_additive_sprites = state;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// ������ � ���� ������
void ParametersStack::Push(const char* str)
{
	if (this->locked) return;
	StackElement* se = new StackElement();
	se->data.stringData = str;
	se->type = stString;
	se->next = this->top;
	this->top = se;
}

// �������� �� ��������
bool ParametersStack::isEmpty()
{
	if ( this->top )
		return false;
	else
		return true;
}

// ������ � ���� �����
void ParametersStack::Push(int num)
{
	if (this->locked) return;
	StackElement* se = new StackElement();
	se->data.intData = num;
	se->type = stInt;
	se->next = this->top;
	this->top = se;
}

// ���������� ������� �����
StackElement* ParametersStack::Pop()
{
	if (!top)
	{
		StackElement* se = new StackElement();
		se->data.intData = 0;
		se->next = NULL;
		se->type = stNone;
		return se;
	}
	else
	{
		StackElement* se = this->top;
		this->top = this->top->next;
		return se;
	}
}
//��������� ���� ��������� ����� �� ������������, ������ ����.
bool ParametersStack::CheckParamTypes(int num, StackElementType first, ...)
{
		va_list mark;
		StackElementType setp = first;
		StackElement* se = top;
		va_start( mark, first );
		for ( int i = 0; i < num; i++ )
		{
			if ( !se && setp && setp != stNone && setp != stIntOrNone )
			{
				Log(DEFAULT_LOG_NAME, logLevelInfo, "Wrong set of parameters in stack for the animation command");
				return false;
			}

			if ( se && se->type != setp && !(se->type == stInt && setp == stIntOrNone) )
			{
				Log(DEFAULT_LOG_NAME, logLevelInfo, "Wrong set of parameters in stack for the animation command");
				return false;
			}
/*
			if ( se )
			{
				if ( se->type != setp )
				{
					if ( !(se->type == stInt && setp == stIntOrNone) )
					{
						Log(DEFAULT_LOG_NAME, logLevelInfo, "Wrong params in stack");
						return false;
					}
				}
			}
*/
			// TODO: warning: �StackElementType� is promoted to �int� when passed through �...�
			// note: (so you should pass �int� not �StackElementType� to �va_arg�)
			// note: if this code is reached, the program will abort
			setp = (StackElementType)va_arg( mark, int );
			if (se) se = se->next;
		}
		va_end( mark );
		return true;
}

//���������� ��� � �������� �����. ������ �� ���������, ����� �� ����������� �������� CheckParamTypes
int ParametersStack::PopInt()
{
	StackElement* sd = this->Pop();
	int ret = sd->data.intData;
	DELETESINGLE(sd);
	return ret;
}
//���������� ��� �� �����. ������ �� ���������, ����� �� ����������� �������� CheckParamTypes � �� ������� �������
int ParametersStack::GetInt(int depth)
{
	if ( !this->top ) return 0;
	StackElement* sd = this->top;
	for ( int i = 0; i < depth; i++ )
	{
		sd = sd->next;
		if ( !sd ) return 0;
	}
	return sd->data.intData;
}

//���������� ������ � �������� �����. ������ �� ���������, ����� �� ����������� �������� CheckParamTypes
const char* ParametersStack::PopString()
{
	StackElement* sd = this->Pop();
	const char * ret = sd->data.stringData;
	DELETESINGLE(sd);
	return ret;
}

//���������� ������ � �������� �����. ������ �� ���������, ����� �� ����������� �������� CheckParamTypes
std::string ParametersStack::PopStdString()
{
	StackElement* sd = this->Pop();
	string ret = string(sd->data.stringData);
	DELETESINGLE(sd);
	return ret;
}

//���������� ������ �� �����. ������ �� ���������, ����� �� ����������� �������� CheckParamTypes � �� ������� �������
const char* ParametersStack::GetString(int depth)
{
	if ( !this->top ) return 0;
	StackElement* sd = this->top;
	for ( int i = 0; i < depth; i++ )
	{
		sd = sd->next;
		if ( !sd ) return 0;
	}
	return sd->data.stringData;
}

void ParametersStack::DumpToLog()
{
	Log(DEFAULT_LOG_NAME, logLevelError, "SpriteStack Dump");
	if (!this->top) return;
	StackElement* sd = this->top;
	int i = 0;
	for (; sd; sd = sd->next, i++)
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "%d: t=%d, i=%d, s=%s", i, (int)sd->type, sd->data.intData, sd->type == stString ? sd->data.stringData : NULL);
	}
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void Sprite::SetCurrentFrame(UINT cf)
{
	if (this->tex)
	{
		if (cf >= this->tex->frames->framesCount)
		{
			// ���� �� �����-�� ������� ��� ����� ���� �� ChangeFrame � � a->frames[a->current] ���� �����-�� ���,
			// ������ ��� ������ �������� �� ����� �������� ������. ������, ���� ������, 
			// ������ ����� �� �������� ���� ��� ���������� ���� ������.
			// ���� ��� ������ ���� �� api, ������ �������� � ��������.
			Log(DEFAULT_LOG_NAME, logLevelError,
				"Unable to use frame %d of texture %s - it does not exist (possibly anim %s).",
				cf, this->tex->name.c_str(), this->cur_anim.c_str());
			assert(cf < this->tex->frames->framesCount);	// ��� �� ���� �� ������ � ������ ������
			return;
		}
		else
		{
			this->frameWidth = (USHORT)this->tex->frames->frame[cf].size.x;
			this->frameHeight = (USHORT)this->tex->frames->frame[cf].size.y;
		}
	}
	
	this->currentFrame = cf;
}

// ������������� ������� ���� �������� �� ���� � ������� index
bool Sprite::JumpFrame( int index )
{
	Animation* a = this->GetAnimation( this->cur_anim_num );
	if ( !a || index < 0 || index > a->frameCount-1 )
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "Impossible jump to frame %d in anim", index);
		return false;
	}
	this->SetCurrentFrame(a->frames[ index ].num);
	a->current = static_cast<USHORT>(index);
	return true;
}

// ����������� ������� ���� �������� �� ���������.
// ���������� true, ���� �������� ��������� ��� ���������� ����� ���������� �����.
bool Sprite::ChangeFrame(Animation* a)
{
	if (IsAnimDone())
		return true;

	UINT now = internal_time;

	if ( a->current == MAX_ANIMFRAMES_COUNT )
	{
		a->current = 0;
		this->SetCurrentFrame(a->frames[a->current].num);
		prevAnimTick = now;
	}
	else if (now - prevAnimTick >= a->frames[a->current].duration - offset)
	{
		offset = 0;
		prevAnimTick = now;

		if ( a->current >= a->frameCount-1 )
		{
			SetAnimDone();
			return true;
		}

		a->current++;
		assert(a->current < a->frameCount);

		// TODO: afcLoop � ����� �� �� ����� � �����
		if (a->frames[a->current].command == afcLoop)
		{
			a->current = 0;
		}

		this->SetCurrentFrame(a->frames[a->current].num);
	}
	else
		return true;

	return false;

}

Animation* Sprite::GetAnimation(string name)
{
	if (this->animsCount > 0)
	{
		assert(anims);
		AnimationMap::const_iterator it = this->animNames->find(name);
		if (it != this->animNames->end())
			return &anims[it->second];
	}

	return NULL;
}

Animation* Sprite::GetAnimation(size_t num)
{
	if (this->animsCount > 0 && num < this->animsCount && num < MAX_ANIMATIONS_COUNT)
	{
		assert(anims);
		return &anims[num];
	}
	return NULL;
}

bool Sprite::SetAnimation(size_t num)
{
	Animation* a = GetAnimation(num);
	if (a)
	{
		this->cur_anim = string(a->name);
		this->cur_anim_num = num;
		a->current = MAX_ANIMFRAMES_COUNT;
		this->ClearAnimDone();
		//this->ProcessSprite();
		return true;
	}
	else
	{
		//Just leave it be
		//this->cur_anim = "";
		//this->cur_anim_num = MAX_ANIMATIONS_COUNT;
	}

	return false;
}


bool Sprite::SetAnimation(const string& anim_name)
{
	if (this->animNames)
	{
		AnimationMap::const_iterator it = this->animNames->find(anim_name);
		if (it != this->animNames->end())
		{
			return SetAnimation(it->second);
		}
	}
	return false;
}

#ifdef SELECTIVE_RENDERING
CAABB Sprite::getRenderAABB(const CAABB& aabb) const
{
	float x = aabb.p.x - aabb.W;
	float y = aabb.p.y - aabb.H;
	float rot_x = aabb.p.x;
	float rot_y = aabb.p.y;

	coord2f_t* frame_coord = NULL;
	FrameInfo* f = NULL;

	if (!render_without_texture && tex)
	{
		f = this->tex->frames->frame + this->currentFrame;

		if (this->IsMirrored())
		{
			frame_coord = f->coordMir;

			if (renderMethod == rsmStretch)
			{
				// Smells like a crutch
				x -= f->size.x - this->realWidth / (aabb.W * 2 / f->size.x) - realX;
				y -= realY;
			}
			else
			{
				x -= f->size.x - this->realWidth - realX;
				y -= realY;
			}
		}
		else
		{
			x -= realX;
			y -= realY;
			frame_coord = f->coord;
		}
	}
	else
	{
		UNUSED_ARG(frame_coord);
	}

	assert(!IsFixed());

	if (render_without_texture || !f)
	{

		coord2f_t frame_size;
		frame_size.x = aabb.W*2;
		frame_size.y = aabb.H*2;

		return CAABB(x, y, x + frame_size.x, y + frame_size.y);
	}
	else
	{
		CAABB res;
		bool repeat_x = false;
		bool repeat_y = false;

		/*
			rot_x += -aabb.W + f->size.x / 2;
			rot_y += -aabb.H + f->size.y / 2;
		*/
		
		switch (renderMethod)
		{
		case rsmStandart:
			res = CAABB(x, y, x + f->size.x, y + f->size.y);
			break;
		case rsmStretch:
			res = CAABB(x, y, x + aabb.W * 2.f, y + aabb.H * 2.f);
			break;
		case rsmCrop:
			res = CAABB(x, y, x + std::min(f->size.x, aabb.W * 2.f), y + std::min(f->size.y, aabb.H * 2.f));
			break;
		case rsmRepeatX:
			repeat_x = true;
			goto repeat;
			break;
		case rsmRepeatY: 
			repeat_y = true;
			goto repeat;
			break;
		case rsmRepeatXY:
			repeat_x = repeat_y = true;
			goto repeat;
			break;
		}

#ifdef DEBUG_SELECTIVE_RENDERING_SPRITES
		RenderBox( res.p.x - res.W, res.p.y - res.H, 1.1f, 2*res.W, 2*res.H, RGBAf(0.0f, 1.0f, 0.0f, 1.0f) );
#endif
		if (!IsNear(angle, 0.f, 0.001f))
			res.Rotate(angle, Vector2(rot_x,rot_y));
#ifdef DEBUG_SELECTIVE_RENDERING_SPRITES
		RenderBox( res.p.x - res.W, res.p.y - res.H, 1.1f, 2*res.W, 2*res.H, RGBAf(1.0f, 1.0f, 0.0f, 1.0f) );
#endif
		return res;

repeat:
		Vector2 coord = Vector2(x, y);

		if ( repeat_x ) coord.x = aabb.Left();
		if ( repeat_y ) coord.y = aabb.Top();
		if ( !repeat_x && coord.x < aabb.Left()) coord.x = aabb.Left();
		Vector2 edge = Vector2( aabb.Right(), aabb.Bottom() );
		return CAABB(coord.x, coord.y, edge.x, edge.y);
	}
}
#endif

void Sprite::Draw(const CAABB &aabb)
{
	if (!this->IsVisible())
		return;

	if ( blendingMode == bmSrcA_One && !render_additive_sprites )
		return;

	if (this->tex && this->currentFrame >= this->tex->frames->framesCount)
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning,
			"Unable to draw frame %d of texture '%s' in animation '%s'. There are only %d.",
			this->currentFrame, this->tex->name.c_str(), this->cur_anim.c_str(), this->tex->frames->framesCount);
		return;
	}

	float x = aabb.p.x - aabb.W;
	float y = aabb.p.y - aabb.H;
	float z = this->z;
#ifdef DEBUG_DRAW_OBJECTS_BORDERS
	float x_ph = x;
	float y_ph = y;
#endif // DEBUG_DRAW_OBJECTS_BORDERS
#ifdef ADJUST_Z_FOR_SCREEN_POS
	z = max( min( cfg.far_z, z - isometric_depth.x * ((aabb.p.x - CAMERA_DRAW_LEFT) / (CAMERA_DRAW_RIGHT - CAMERA_DRAW_LEFT)) - isometric_depth.y * ((aabb.p.y - CAMERA_DRAW_TOP) )/( CAMERA_DRAW_BOTTOM-CAMERA_DRAW_TOP) ), cfg.near_z);
#endif //ADJUST_Z_FOR_SCREEN_POS
#ifdef BLENDING_HACK_Z
	if ( blendingMode == bmSrcA_One )
	{
		z = BLENDING_HACK_Z;
	}
#endif
	
	coord2f_t* frame_coord = NULL;
	coord2f_t* overlay_coord = NULL;
	FrameInfo* f = NULL;
	FrameInfo* o = NULL;
	float over_x = 0.0f, over_y = 0.0f;
	
	if (!render_without_texture)
	{
		#ifndef DEBUG_RENDER_UNLOADED_TEXTURES
			if (!this->tex)
				return;
		#endif // DEBUG_RENDER_UNLOADED_TEXTURES

		f = this->tex->frames->frame + this->currentFrame;
		

		if (this->IsMirrored())
		{
			frame_coord = f->coordMir;

			if (renderMethod == rsmStretch)
			{
				// Smells like a crutch
				x -= f->size.x - this->realWidth / (aabb.W * 2 / f->size.x) - realX;
				y -= realY;
			}
			else
			{
				x -= f->size.x - this->realWidth - realX;
				y -= realY;
			}
		}
		else
		{
			x -= realX;
			y -= realY;
			frame_coord = f->coord;
		}
	}

	if (this->IsFixed())
	{
		extern float CAMERA_OFF_X;
		extern float CAMERA_OFF_Y;
		x -= CAMERA_OFF_X;
		y -= CAMERA_OFF_Y;
	}

	if (render_without_texture)
	{
		// ���� ������ ��������� �� ��������� ������ ������, �� �� ��� �� ������.
		if (x + this->frameWidth < CAMERA_DRAW_LEFT || x > CAMERA_DRAW_RIGHT)
			return;
		if (y + this->frameHeight < CAMERA_DRAW_TOP || y > CAMERA_DRAW_BOTTOM )
			return;

		coord2f_t frame_size;
		frame_size.x = aabb.W*2;
		frame_size.y = aabb.H*2;
		RenderFrameRotated(x, y, z, &frame_size, NULL, NULL, this->color, this->angle, aabb.p, blendingMode);
	}
	else
	{
		if (renderMethod == rsmStandart)
		{
			// ���� ������ ��������� �� ��������� ������ ������, �� �� ��� �� ������.
			if (x + this->frameWidth < CAMERA_DRAW_LEFT || x > CAMERA_DRAW_RIGHT)
				return;
			if (y + this->frameHeight < CAMERA_DRAW_TOP || y > CAMERA_DRAW_BOTTOM )
				return;

			RenderFrameRotated(x, y, z, &f->size, frame_coord, this->tex, this->color, this->angle, aabb.p, blendingMode);
			UINT use = 0;
			for (UINT i = 0; i < this->overlayCount; i++)
			{
				use = this->overlayUse[i];
				if (use >= tex->frames->overlayCount)
				{
					Log(DEFAULT_LOG_NAME, logLevelError, "Proto %s uses overlay %d, but texture describes only %d overlays",
						this->proto_name, use, tex->frames->overlayCount);
					continue;
				}

				o = &tex->frames->overlay[use*tex->frames->framesCount + this->currentFrame];
				float off_x = o->offset.x;
				float off_y = o->offset.y;
				if ( angle != 0.0f )
				{
					float sx = off_x + o->size.x/2.0f - f->size.x/2.0f;
					float sy = off_y + o->size.y/2.0f - f->size.y/2.0f;
					float rsx = sx * cos(angle) - sy * sin(angle);
					float rsy = sx * sin(angle) + sy * cos(angle);
					off_x = rsx - o->size.x/2.0f  + f->size.x/2.0f;
					off_y = rsy - o->size.y/2.0f  + f->size.y/2.0f;
				}
				if (this->IsMirrored())
				{
					over_x = x + f->size.x - off_x - o->size.x;
					over_y = y + off_y;
					overlay_coord = o->coordMir;
				}
				else
				{
					over_x = x + off_x;
					over_y = y + off_y;
					overlay_coord = o->coord;
				}
				RenderFrameRotated(over_x, over_y, z, &o->size, overlay_coord, this->tex, this->ocolor[i], this->angle, aabb.p, blendingMode);
			}
		}
		else
		{
			CAABB c = aabb;
			if (this->IsFixed())
			{
				extern float CAMERA_OFF_X;
				extern float CAMERA_OFF_Y;
				c.p.x -= CAMERA_OFF_X;
				c.p.y -= CAMERA_OFF_Y;
			}

			// ���� ������ ��������� �� ��������� ������ ������, �� �� ��� �� ������.
			if (c.Right() < CAMERA_DRAW_LEFT || c.Left() > CAMERA_DRAW_RIGHT)
				return;
			if (c.Bottom() < CAMERA_DRAW_TOP || c.Top() > CAMERA_DRAW_BOTTOM )
				return;
				
			RenderFrame(x, y, z, c, f, this->tex, this->color, IsMirrored(), renderMethod, angle, blendingMode);

			float kx = (aabb.W / f->size.x);
			float ky = (aabb.H / f->size.y);
			UINT use = 0;
			for (UINT i = 0; i < this->overlayCount; i++)
			{
				use = this->overlayUse[i];
				if (use >= tex->frames->overlayCount)
				{
					Log(DEFAULT_LOG_NAME, logLevelError,  "Proto %s uses overlay %d, but texture describes only %d overlays",
						this->proto_name, use, tex->frames->overlayCount);
					continue;
				}

				o = &tex->frames->overlay[use*tex->frames->framesCount + this->currentFrame];
				float off_x = o->offset.x * kx * 2;
				float off_y = o->offset.y * ky * 2;
				if ( angle != 0.0f )
				{
					// TODO: not tested with scaling and all those fancy shit
					float sx = off_x + o->size.x/2.0f - f->size.x/2.0f;
					float sy = off_y + o->size.y/2.0f - f->size.y/2.0f;
					float rsx = sx * cos(angle) - sy * sin(angle);
					float rsy = sx * sin(angle) + sy * cos(angle);
					off_x = rsx - o->size.x/2.0f  + f->size.x/2.0f;
					off_y = rsy - o->size.y/2.0f  + f->size.y/2.0f;
				}
				//if (this->IsMirrored())
				/*if (false)
				{
					over_x = x + f->size.x - off_x - o->size.x;
					over_y = y + off_y;
					overlay_coord = o->coordMir;
				}
				else*/
				{
					over_x = x + off_x;
					over_y = y + off_y;
					overlay_coord = o->coord;
				}

				
				c.W = kx * o->size.x;
				c.H = ky * o->size.y;

				RenderFrame(over_x, over_y, z, c, o, this->tex, this->ocolor[i], this->IsMirrored(), renderMethod, angle, blendingMode);
			}
		}
	}

#ifdef DEBUG_DRAW_MP0_POINT
	if (this->mpCount > 0)
	{
		Vector2 mpos = this->IsMirrored () ? Vector2(-this->mp[0].x, this->mp[0].y) : this->mp[0];
		Vector2 pos = aabb.p + mpos;

		RenderFrame(pos.x, pos.y, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, NULL, NULL, RGBAf(DEBUG_MP0_POINT_COLOR));
	}
#endif // RENDER_MP0_POINT

}

void Sprite::LoadFromTexture(const char* name)
{
	this->tex = textureMgr->GetByName(string(name));
	if ( this->tex ) this->tex->ReserveUsage();
	this->z = 0.0f;
	this->ocolor = NULL;
	this->overlayUse = NULL;
	if ( tex )
	{
		this->SetVisible();
		this->frameWidth = (USHORT)tex->frames->frame[0].size.x;
		this->frameHeight = (USHORT)tex->frames->frame[0].size.y;

		if (tex->isTransparent)
		{
			this->blendingMode = bmSrcA_OneMinusSrcA;
		}
	}
}

void Sprite::SetMpPointsFromProto(const Proto* proto)
{
	assert(proto);

	mpCount = proto->mpCount;
	mp = new Vector2[ mpCount ];
}

void Sprite::LoadFromProto(const Proto* proto)
{
	if (!proto)
		return;
	
	polygons = &(proto->polygons);

	this->proto = proto;
	proto_name = StrDupl(proto->name.c_str());
	proto->ReserveUsage();
	this->isometric_depth = Vector2(proto->isometric_depth_x, proto->isometric_depth_y);

	this->tex = textureMgr->GetByName(proto->texture);

	this->z = proto->z;

	if ( proto->mpCount )
	{
		SetMpPointsFromProto(proto);
	}

	if (tex)
	{
		tex->ReserveUsage();
		if (proto->overlayCount && tex->frames->overlay)
		{
			this->overlayCount = proto->overlayCount;
			if (proto->overlayCount > tex->frames->overlayCount)
				Log(DEFAULT_LOG_NAME, logLevelWarning, "Proto %s uses more overlays than texture %s describes", proto_name, proto->texture);
		}
		if ( overlayCount > 0 )
		{
			this->ocolor = new RGBAf[ overlayCount ];
			memcpy( this->ocolor, proto->ocolor, overlayCount * sizeof( RGBAf ) );
			this->overlayUse = new UINT[ overlayCount ];
			memcpy( this->overlayUse, proto->overlayUsage, overlayCount * sizeof( UINT ) );
		}
		else
		{
			this->ocolor = NULL;
			this->overlayUse = NULL;
		}

		this->frameWidth = (USHORT)tex->frames->frame[0].size.x;
		this->frameHeight = (USHORT)tex->frames->frame[0].size.y;

		if (tex->isTransparent)
		{
			// ���� �� �������� ���� ��������� �������, ���������� �������������� ����� ���������.
			// ������, � ��������� ��� ����� ����� ������� ������ �����.
			this->blendingMode = bmSrcA_OneMinusSrcA;
		}
	}
	else
	{
		this->overlayCount = 0;
		this->ocolor = NULL;
		this->overlayUse = NULL;

		this->frameWidth = static_cast<USHORT>(proto->frame_widht);
		this->frameHeight = static_cast<USHORT>(proto->frame_height);
	}

	this->color = proto->color;
	this->outline_color = proto->outline_color;
	if (static_cast<BlendingMode>(proto->blendingMode) != bmLast)
		this->blendingMode = static_cast<BlendingMode>(proto->blendingMode);
	this->renderMethod = static_cast<RenderSpriteMethod>(proto->renderMethod);
	
	this->SetVisible();

	if (proto->animations && proto->animationsCount > 0)
	{
		this->animNames = &(proto->animNames);
		this->animsCount = proto->animationsCount;
		// ������ ����� ���������� ��������� � ������� malloc, ����� �� ���� ��� ������������
		// �� �������� �����������.
		this->anims = (Animation*)malloc(sizeof(Animation) * proto->animationsCount);
		memcpy(this->anims, proto->animations, sizeof(Animation) * proto->animationsCount);
	}
}



Sprite::~Sprite()
{
	// ������ ����� ������� � ������� free, ����� �� �������� �����������.
	free(this->anims);

	DELETESINGLE(this->stack);
	DELETEARRAY(this->mp);

	if ( ocolor )
	{
		DELETEARRAY(ocolor);
		DELETEARRAY(overlayUse);
	}

	if (proto_name)
	{
		Resource::Release( proto );
		DELETEARRAY(proto_name);
	}

	if (tex)
	{
		Resource::Release( tex );
	}
}

void Sprite::SaveState()
{
	state_stack.push( SpriteState( this ) );
}

void Sprite::RestoreState()
{
	if ( state_stack.empty() ) return;
	state_stack.top().restore( this );
	state_stack.pop();
}

void Sprite::PopState()
{
	state_stack.pop();
}

SpriteState::SpriteState( Sprite* s ):
	polygon( NULL ),
	realX( s->getRealX() ),
	realY( s->getRealY() ),
	currentFrame( s->getCurrentFrame() ),
	cur_anim_num( s->cur_anim_num ),
	cur_anim( s->cur_anim ),
	prevAnimTick_diff( internal_time - s->prevAnimTick )
{
	Animation* anim = s->GetAnimation(s->cur_anim_num);
	animationFrame = anim->current;
}

void SpriteState::restore( Sprite* s )
{
	s->setRealX(realX);
	s->setRealY(realY);
	s->setCurrentFrame(currentFrame);
	s->cur_anim_num = cur_anim_num;
	s->cur_anim = cur_anim;
	Animation* anim = s->GetAnimation(cur_anim_num);
	anim->current = animationFrame;
	s->prevAnimTick = internal_time - prevAnimTick_diff;
}
