#include "StdAfx.h"

#include "../misc.h"

#include "../script/script.h"

#include "phys/phys_misc.h"

#include "../render/texture.h"
#include "animation.h"
#include "sprite.h"
#include "proto.h"
#include "../config.h"
#include "../sound/snd.h"

#include "object_mgr.h"
#include "objects/object.h"
#include "objects/object_sprite.h"
#include "objects/object_environment.h"
#include "objects/object_character.h"
#include "objects/object_ray.h"
#include "player.h"
#include "particle_system.h"

#include "phys/phys_collisionsolver.h"

#include "camera.h"
#include "ribbon.h"

#include "game.h"
#include "editor.h"

#include "../input_mgr.h"

#include "../resource_mgr.h"
#include "net.h"

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

extern char path_levels[MAX_PATH];
extern InputMgr inpmgr;

extern ResourceMgr<Proto> * protoMgr;
extern SoundMgr* soundMgr;
#ifdef MAP_EDITOR
extern editor::EditorStates editor_state;
#endif // MAP_EDITOR

extern float CAMERA_GAME_LEFT;
extern float CAMERA_GAME_RIGHT;
extern float CAMERA_GAME_TOP;
extern float CAMERA_GAME_BOTTOM;
extern float CAMERA_DRAW_LEFT;
extern float CAMERA_DRAW_RIGHT;
extern float CAMERA_DRAW_TOP;
extern float CAMERA_DRAW_BOTTOM;

extern config cfg;

UINT spare_physics_time;		//�����, ���������� �������������� ������� � �������� �����.

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

game::GameStates game_state = game::GAME_NONE;
Player* playerControl = NULL;
vector<Player*> players;

UINT last_passive_obj_update_tick = 0;		// ����� ����������� ���� ���������� ��������� ��������


#ifdef DEBUG_DRAW_COLLISION_PAIRS
list<Vector2> collPairs;
#endif // DEBUG_DRAW_COLLISION_PAIRS

bool isNewGameCreated = false;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// �� �������� ����
// ��� �������� ����, ������� ������� ������ ���� ������� �� SAP �, �����, �����������.
// �������� ���� ���������� ��� �������� �����, ��� ������ �� ������� ��� ��� 
// ���������� ������ ������.
// 1) ���� game::InitGame game::FreeGame ������� �� �������, ����������� ������� 
// �� ������� ������� ��� � �������� ���������� DEFAULT_INIT_SCRIPT, �� � ����� 
// ������ �������� SAP � ������� �������� �� ����� ������� ���������.
// 2) ���� game::InitGame game::FreeGame ������� �� �������, ����������� ��� 
// ��������� �������(������ ������� ��� ������� ��������, ���������� ��� lua), ��
// ������ ������ ������� SAP � �������, ��� ��� ����� �������� ���� �������.
// �������, ������� � FreeGame ���������������� ��� ��������, � �����, ����� 
// ����� ������� �������� ����� ���� � game::UpdateGame ���������� RemoveAllObjects, 
// � ������� ���������� ������ ��������.

// TODO: ������� ���������� �������� ���� �� �������, �� �������� ����� ����. ��. scriptApi::DestroyGame

void game::FreeGame(bool fullDestroy)
{
	game_state = game::GAME_DESTROYING;
	ClearFactions();
	// ��� ������ �� ������ asap = NULL, � ������� �� �������, ������� �� ����� 
	// �������������� � ������ ������.
	PrepareRemoveAllObjects();
	if (fullDestroy)
	{
		// ����� ������ ������ ��������.
		RemoveAllObjects();
		Net::Stop();
	}

	playerControl = NULL;
	for(size_t i = 0; i < players.size(); i++)
		DELETESINGLE(players[i]);
	players.clear();
	
	DeleteAllRibbons();

	assert(protoMgr);
	protoMgr->UnloadAllUnUsed();
	
	SCRIPT::ReleaseAllInGameRegRef();

	game_state = game::GAME_NONE;
}

void game::InitGame()
{
	FreeGame(false);

	DefaultEnvSet( NULL );

	InitArraySAP();

	game_state = game::GAME_INITIALIZING;
	spare_physics_time = 0;

	CameraMoveToPos(0,0);

	inpmgr.ClearInput();

	game_state = game::GAME_RUNNING;

	SetParticlesWind( Vector2(0, 0) );

	isNewGameCreated = true;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////



void game::UpdateGame(UINT dt)
{
	if (isNewGameCreated)
	{
		// ��. ����, ������ ��� ���������� RemoveAllObjects
		RemoveAllObjects();
		InitParticles();
		isNewGameCreated = false;
	}

	if (game_state == GAME_RUNNING)
	{
		for(size_t i = 0; i < players.size(); i++)
			players[i]->Update();

		dt += spare_physics_time;
#ifdef USE_TIME_DEPENDENT_PHYS
		while (dt >= TARGET_GAMETICK && !isNewGameCreated)
		{
			CameraUpdateFocus();
#ifdef MAP_EDITOR
			if ( editor_state == editor::EDITOR_OFF )
#endif // MAP_EDITOR
			{
				ProcessAllActiveObjects();
			}
			dt -= TARGET_GAMETICK;
		}
#endif // USE_TIME_DEPENDENT_PHYS
		spare_physics_time = dt;


		if (internal_time - last_passive_obj_update_tick >= GAMETICK && !isNewGameCreated)
		{
			ProcessAllPassiveObjects();
#ifdef MAP_EDITOR
			if ( editor_state != editor::EDITOR_OFF )
			{
				ProcessAllActiveObjects();
			}
#endif // MAP_EDITOR
			
			last_passive_obj_update_tick = internal_time;

			Net::Update();
		}
	}
	else if ( game_state == GAME_PAUSED )
	{
		//UpdatePhysTime();
	}

	inpmgr.ProlongedArrayFlush();
}

void game::TogglePause()
{
		if ( game_state == GAME_RUNNING) game_state = GAME_PAUSED;
		else if ( game_state == GAME_PAUSED ) game_state = GAME_RUNNING;
}

void game::TogglePause( bool state )
{
		if ( game_state != GAME_RUNNING && game_state != GAME_PAUSED )
			return;
		if ( state ) game_state = GAME_PAUSED;
		else game_state = GAME_RUNNING;
}

bool game::GetPause()
{
	return ( game_state == GAME_PAUSED );
}

#ifdef MAP_EDITOR
extern Vector2 grid;
extern RGBAf grid_color;
#endif //MAP_EDITOR

void game::DrawGame()
{
	if (game_state == GAME_RUNNING || game_state == GAME_PAUSED)
	{
		CameraUpdatePosition();
		
#ifdef MAP_EDITOR
		if ( editor_state != editor::EDITOR_OFF && grid.x > 0 )
		{
			int cl = (int)floor(CAMERA_DRAW_LEFT);
			int cr = (int)floor(CAMERA_DRAW_RIGHT);
			int ct = (int)floor(CAMERA_DRAW_TOP);
			int cb = (int)floor(CAMERA_DRAW_BOTTOM);
			int gx = (int)grid.x;
			int gy = (int)grid.y;
			for ( int i = cl - (cl % gx); i <= cr - (cr % gx) + gx; i+=gx )
			{
				RenderLine( static_cast<float>(i), CAMERA_DRAW_TOP, static_cast<float>(i), CAMERA_DRAW_BOTTOM, 0.85f, grid_color );
			}
			for ( int i = ct - (ct % gy); i <= cb - (cb % gy) + gy; i+=gy )
			{
				RenderLine( CAMERA_DRAW_LEFT, static_cast<float>(i), CAMERA_DRAW_RIGHT, static_cast<float>(i), 0.85f, grid_color );
			}
		}
#endif //MAP_EDITOR
		
		DrawRibbons();

		DrawAllObjects();


#ifdef DEBUG_DRAW_COLLISION_PAIRS
		for (list<Vector2>::const_iterator it = collPairs.begin(); it != collPairs.end(); it++)
		{
			const Vector2& p1 = *it;
			it++;
			const Vector2& p2 = *it;
			RenderLine(p1.x, p1.y, p2.x, p2.y, 1.0f, RGBAf(DEBUG_COLLISION_PAIRS_COLOR) );
		}
#endif // DEBUG_DRAW_COLLISION_PAIRS
	}
}

