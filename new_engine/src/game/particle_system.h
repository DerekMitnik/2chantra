#ifndef _PARTICLE_SYSTEM_H_
#define _PARTICLE_SYSTEM_H_

#include "../script/script.h"

#include "phys/phys_misc.h"
#include "sprite.h"

#include "proto.h"
#include "../render/renderer.h"

#define PARTICLES_PARAM_TYPE float

class ObjEnvironment;

enum ParticlesParam
{
	particles_param_first,
	intensity,
	relative_speed,
	use_physic,
	include_one_sided,
	include_polygons,
	max_simple_particles,
	max_phys_particles,
	max_weather_particles,
	particles_param_last
};

enum ParticlesAnimation
{
	no_animation,
	lifetime_animation,
	bounce_animation,
	loop_animation,
	random_frame_animation
};

void InitParticles();
void InitParticlesParam();
void SetParticlesParam( ParticlesParam param, PARTICLES_PARAM_TYPE value );
PARTICLES_PARAM_TYPE GetParticlesParam( ParticlesParam param );

enum TrajectoryType { pttLine, pttRipple, pttRandom, pttSine, pttCosine, pttPseudoDepth, pttTwist, pttOrbit, pttGlobalSine, pttToCenter };

void SetParticlesWind( Vector2 new_wind );
Vector2 GetParticlesWind();

struct CParticle
{
	//Vector2			p;			//	������� �������
	Vector2			old_p;		//  ������ ������� �������, ����� ��� �����
	Vector2			v;			//	�������� 
	//RGBAf			c;			//	������� ����
	RGBAf			dc;			//	���������� ����� �� ������ ����
	float			size;		//	������� ������
	float			dsize;		//	���������� �������
	int			life;		//	����� ����� �������; -1 ������ �������������
	int			age;		//	������� �������, �� ���� ������� ��� ��� ���������������
	float			parameter;	//  ��������� ��� ����������� ������� ����������.
	float			trace;		//  ����� "������" �������
	bool			is_on_plane;
	int			bounces;
};

enum ParticleCounterType
{
	pcSimple,
	pcPhysic,
	pcWeather
};

typedef void (*FCreateFunc)(CParticle *);
typedef void (*FUpdateFunc)(CParticle *, float);

/**
*	TODO:
*		1. �������� ������������ �������, ������� ��������
*		2. �������� ��������� ���������� ������� ��� �������
*		3. �������� ��� ���-������
*/


class CParticleSystem
{
public:
	CParticle*				particles;
	
	Vector2*				pp;
	float*					pz;
	RGBAf*					pcolor;
	
	const Texture*			texture;
	coord2f_t**				tex_coords;
	coord2f_t*				fsizes;
	int*					extra;

	UINT					emitter;
	int						mode;
	//CRenderObject*			UserRenderSample;

	vector<CAABB>*				area;
	vector<CAABB>*				drop_area;

	float					system_z;
	bool					dead;

	ParticleCounterType			counter_type;


	CParticleSystem() :
		particles(NULL),
		pp(NULL), pz(NULL), pcolor(NULL),
		texture(NULL), tex_coords(NULL), fsizes(NULL),
		emitter(0), mode(0),
		area(NULL), drop_area(NULL),
		system_z(0), dead(false), counter_type( pcSimple ), gravity(0,0)
	{
		MaxParticles = 0;			//	����������� �������� ����� ������
		ParticlesActive = 0;		//	������� ������ ������� ������
		lifemin = 0;				//	����������� ����� ����� �������
		lifemax = 0;				//	������������ ����� ����� �������

		startsize = 0;				//	��������� ������ ������ �������
		endsize = 0;				//	�������� ������
		sizevar = 0;				//	������ ����������� ��� ������ ������� �������

		plife = 0;					//	���������������
		plifevar = 0;				//	���������������

		life = 0;					//	����� ����� �������; -1 ������ ������������
		age = 0;					//	������� ������� �������

		emission = 0.0f;				//	������, ������������ ������� ������ ������� �� ���
		emission_accumulated = 0.0f;
		notcreated = 0;				//	���������� ������, ������� �� �� ������ ��������� � ���� ����� �� �����-�� ��������
		geom = NULL;				//	������ �����, ��� ��������� ������
		GeomNumPoints = 0;			//	����� ���� �����

		max_speed = 0;		// ������������ ��������� �������� �������
		min_speed = 0;		// ����������� ��������� �������� �������
		max_angle = 0;		// ������������ ���� ������ (��� �������� ������)
		min_angle = 0;		// ����������� ���� ������ (��� �������� ������)
		min_param = 0;		// ����������� ��������� �������� ���������� �������
		max_param = 0;		// ������������ ��������� �������� ��������� �������
		min_trace = 0;		// ���������� ����� "������" �������.
		max_trace = 0;		// ������������ ����� "������" �������.
		trace_width = 1.0f;
		//gravity;		// ����������, ����������� �� �������.
		physical = 0;		// ���� ����� �� ������������
		wind_affected = 0;	// ���������� �� ������� ����������� �����
		trajectory = pttLine;		// ��� ��������� ������
		t_param1 = 0;		// �������� ���������� 1
		t_param2 = 0;		// �������� ���������� 2

		blendingMode = bmLast; // ����� ��������� (�������������� �� ���������)
		physic = false; // Is obstructed by static physic objects on screen
		bounce = 0;
		max_bounce = -1;
		animation = lifetime_animation;
		animation_param = 0;
		max_abs_speed = -1.0f;
	}

	~CParticleSystem();
	void					Init();
	void					LoadFromProto(const Proto* proto);
	bool					Update( ObjEnvironment* env );
	void Render();

	CParticle*				CreateParticle();
	void					SetGeometry(Vector2 * points, int numPoints);

	//bool					SaveToFile();
	//bool					LoadFromFile();
	bool					AreaMovement( Vector2& coord, Vector2 vel, vector<CAABB>* area, bool physic, float bounce, Vector2& real_v, CParticle* p, int max_bounce );

	Vector2			position;						//	������� ������� ������

	RGBAf			sc;						//	��������� ���� ������ �������
	RGBAf			ec;						//	�������� ���� ������ �������
	RGBAf			vc;						//	������ ����������� ��� ������ �����

	size_t			MaxParticles;			//	����������� �������� ����� ������
	size_t			ParticlesActive;		//	������� ������ ������� ������
	int				lifemin;				//	����������� ����� ����� �������
	int				lifemax;				//	������������ ����� ����� �������

	float			startsize;				//	��������� ������ ������ �������
	float			endsize;				//	�������� ������
	float			sizevar;				//	������ ����������� ��� ������ ������� �������

	int				plife;					//	���������������
	int				plifevar;				//	���������������

	int			life;					//	����� ����� �������; -1 ������ ������������
	int			age;					//	������� ������� �������

	float				emission;				//	������, ������������ ������� ������ ������� �� ���
	float				emission_accumulated;
	int				notcreated;				//	���������� ������, ������� �� �� ������ ��������� � ���� ����� �� �����-�� ��������
	Vector2			*geom;					//	������ �����, ��� ��������� ������
	int				GeomNumPoints;			//	����� ���� �����

	float					max_speed;		// ������������ ��������� �������� �������
	float					min_speed;		// ����������� ��������� �������� �������
	float					max_angle;		// ������������ ���� ������ (��� �������� ������)
	float					min_angle;		// ����������� ���� ������ (��� �������� ������)
	float					min_param;		// ����������� ��������� �������� ���������� �������
	float					max_param;		// ������������ ��������� �������� ��������� �������
	float					min_trace;		// ���������� ����� "������" �������.
	float					max_trace;		// ������������ ����� "������" �������.
	float					trace_width;
	Vector2					gravity;		// ����������, ����������� �� �������.
	bool					physical;		// ���� ����� �� ������������
	bool					wind_affected;	// ���������� �� ������� ����������� �����
	TrajectoryType	trajectory;		// ��� ��������� ������
	float					t_param1;		// �������� ���������� 1
	float					t_param2;		// �������� ���������� 2

	BlendingMode            blendingMode;   // ����� ���������
	bool					physic; // Is obstructed by static physic objects on screen
	float					bounce;
	int					max_bounce;

	ParticlesAnimation			animation;
	UINT					animation_param;
	float					max_abs_speed;

#ifdef SELECTIVE_RENDERING
	Vector2  min_lt, max_rb;

	void updateLTRB(Vector2 const& p, float size)
	{
		min_lt.x = std::min(p.x - size, min_lt.x);
		min_lt.y = std::min(p.y - size, min_lt.y);
		max_rb.x = std::max(p.x + size, max_rb.x);
		max_rb.y = std::max(p.y + size, max_rb.y);
	}
#endif
protected:
	void					swapParticles(int i, int j);
};

#endif //_PARTICLE_SYSTEM_H_
