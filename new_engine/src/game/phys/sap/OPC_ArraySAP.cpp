///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *	OPCODE - Optimized Collision Detection
 *	Copyright (C) 2001 Pierre Terdiman
 *	Homepage: http://www.codercorner.com/Opcode.htm
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *	Contains an array-based version of the sweep-and-prune algorithm
 *	\file		OPC_ArraySAP.cpp
 *	\author		Pierre Terdiman
 *	\date		December, 2, 2007
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Precompiled Header
#include "StdAfx.h"

#if defined(USING_GTEST)
# undef assert
# undef ASSERT
# include "gtest/gtest.h"
# define assert EXPECT_TRUE
# define ASSERT EXPECT_TRUE
#endif

#include "OPC_ArraySAP.h"



#if defined(DEBUG_SAP) || defined(DEBUG_RAYSAP)
#include "../../../misc.h"
#define TRACEMSG(msg, x1, x2) {Log(DEFAULT_LOG_NAME, logLevelInfo, "%s %d, %d", msg, x1, x2);}
#define TRACEMSG2(msg, x1, x2, p1, p2) {Log(DEFAULT_LOG_NAME, logLevelInfo, "%s %d, %d - 0x%p, 0x%p", msg, x1, x2, p1, p2);}
#else
#define TRACEMSG(msg, x1, x2)
#define TRACEMSG2(msg, x1, x2, p1, p2)
#endif // DEBUG_SAP


using namespace IceCore;
using namespace Opcode;

//#include "SAP_Utils.h"


struct Opcode::IAABB : public Allocateable
{
	udword mMinX;
	udword mMinY;
#ifndef KERNEL_BUG_ASAP_2D
	udword mMinZ;
#endif
	udword mMaxX;
	udword mMaxY;
#ifndef KERNEL_BUG_ASAP_2D
	udword mMaxZ;
#endif

	inline_ udword	GetMin(udword i)	const	{ assert(i < ASAP_AxesCount); return (&mMinX)[i]; }
	inline_ udword	GetMax(udword i)	const	{ assert(i < ASAP_AxesCount); return (&mMaxX)[i]; }
};



/*
	- already sorted for batch create?
	- better axis selection batch create
*/

//#define USE_WORDS		// Use words or dwords for box indices. Words save memory but seriously limit the max number of objects in the SAP.
#define USE_PREFETCH
//#define USE_INTEGERS

#define USE_OVERLAP_TEST_ON_REMOVES	// "Useless" but faster overall because seriously reduces number of calls (from ~10000 to ~3 sometimes!)
//#define RELEASE_ON_RESET	// Release memory instead of just doing a reset

#include "OPC_ArraySAP.h"


struct Opcode::CreateData
{
	udword	mHandle;
	ASAP_AABB	mBox;
};

ArraySAP::ArraySAP(BoxGroupIntersectionMethod method)
{
	mNbBoxes	= 0;
	mMaxNbBoxes	= 0;
	mNbAddedBoxes = 0;
	mNbUsedBoxes = 0;
	mBoxes		= null;
	mEndPoints[0] = mEndPoints[1] = 
#ifndef KERNEL_BUG_ASAP_2D
		mEndPoints[2] = 
#endif
		null;
	mFirstFree	= INVALID_ID;

	mMaxGUID = 0;

	SetBoxGroupIntersectionMethod(method);
}

ArraySAP::~ArraySAP()
{
	mNbBoxes	= 0;
	mMaxNbBoxes	= 0;
	mNbUsedBoxes = 0;
	DELETEARRAY(mBoxes);
	for(udword i=0;i<ASAP_AxesCount;i++)
	{
		DELETEARRAY(mEndPoints[i]);
	}
}

void ArraySAP::SetBoxGroupIntersectionMethod(BoxGroupIntersectionMethod method)
{
	switch(method)
	{
	case TRIVIAL:
		mBoxGroupIntersecterFunc = IsIntersectByGroupTrivial;
		break;
	case SIMMETRICAL:
		mBoxGroupIntersecterFunc = IsIntersectByGroupSimmetrical;
		break;
	case NONSIMMETRICAL:
		mBoxGroupIntersecterFunc = IsIntersectByGroupNonsimmetrical;
		break;
	default:
		assert(false);
	}
}

void ArraySAP::ResizeBoxArray()
{
	const udword NewMaxBoxes = mMaxNbBoxes ? mMaxNbBoxes*2 : 64;

	ASAP_Box* NewBoxes = ICE_NEW_TMP(ASAP_Box)[NewMaxBoxes];
	const udword NbSentinels=2;
	ASAP_EndPoint* NewEndPointsX = ICE_NEW_TMP(ASAP_EndPoint)[NewMaxBoxes*2+NbSentinels];
	ASAP_EndPoint* NewEndPointsY = ICE_NEW_TMP(ASAP_EndPoint)[NewMaxBoxes*2+NbSentinels];
#ifndef KERNEL_BUG_ASAP_2D
	ASAP_EndPoint* NewEndPointsZ = ICE_NEW_TMP(ASAP_EndPoint)[NewMaxBoxes*2+NbSentinels];
#endif

	if(mNbBoxes)
	{
		// �������� ���� ������ ������ (� ��� ����� � ��� ��������, ���� ��� ����)
		CopyMemory(NewBoxes, mBoxes, sizeof(ASAP_Box)*mNbBoxes);
		// �������� ������� �� �� ����. �� � 2 �. ������, ��� ��������� ���������� ������ + 2 ���������.
		CopyMemory(NewEndPointsX, mEndPoints[0], sizeof(ASAP_EndPoint)*(mNbAddedBoxes*2+NbSentinels));
		CopyMemory(NewEndPointsY, mEndPoints[1], sizeof(ASAP_EndPoint)*(mNbAddedBoxes*2+NbSentinels));
#ifndef KERNEL_BUG_ASAP_2D
		CopyMemory(NewEndPointsZ, mEndPoints[2], sizeof(ASAP_EndPoint)*(mNbAddedBoxes*2+NbSentinels));
#endif
	}
	else
	{
		// Initialize sentinels
#ifdef USE_INTEGERS
		const udword Min = EncodeFloat(MIN_FLOAT);
		const udword Max = EncodeFloat(MAX_FLOAT);
#else
		const float Min = MIN_FLOAT;
		const float Max = MAX_FLOAT;
#endif
		NewEndPointsX[0].SetData(Min, INVALID_INDEX, FALSE);
		NewEndPointsX[1].SetData(Max, INVALID_INDEX, TRUE);  NewEndPointsX[1].mData = 0xffffffff;
		NewEndPointsY[0].SetData(Min, INVALID_INDEX, FALSE);
		NewEndPointsY[1].SetData(Max, INVALID_INDEX, TRUE);  NewEndPointsY[1].mData = 0xffffffff;
#ifndef KERNEL_BUG_ASAP_2D
		NewEndPointsZ[0].SetData(Min, INVALID_INDEX, FALSE);
		NewEndPointsZ[1].SetData(Max, INVALID_INDEX, TRUE);  NewEndPointsZ[1].mData = 0xffffffff;
#endif
	}
	DELETEARRAY(mBoxes);
#ifndef KERNEL_BUG_ASAP_2D
	DELETEARRAY(mEndPoints[2]);
#endif
	DELETEARRAY(mEndPoints[1]);
	DELETEARRAY(mEndPoints[0]);
	mBoxes = NewBoxes;
	mEndPoints[0] = NewEndPointsX;
	mEndPoints[1] = NewEndPointsY;
#ifndef KERNEL_BUG_ASAP_2D
	mEndPoints[2] = NewEndPointsZ;
#endif

	mMaxNbBoxes = NewMaxBoxes;

	checkEndpointsOrder();
}

inline_ BOOL Intersect(const IAABB& a, const IAABB& b, udword axis)
{
	if(b.GetMax(axis) < a.GetMin(axis) || a.GetMax(axis) < b.GetMin(axis))	return FALSE;
	return TRUE;
}

// ### TODO: the sorts here might be useless, as the values have been sorted already
bool ArraySAP::CompleteBoxPruning2(udword nb, const IAABB* array, const Axes& axes, const CreateData* batched)
{
	// nb - ���������� ������, ������� ����������
	// array - ������ �������� �� ������������ ������
	// axes - ���
	// batched - ������ ������  � ����� ������
	// Checkings
	if(!nb || !array)	return false;

	// Catch axes
	const udword Axis0 = axes.mAxis0;
	const udword Axis1 = axes.mAxis1;
#ifndef KERNEL_BUG_ASAP_2D
	const udword Axis2 = axes.mAxis2;
#endif

	// Allocate some temporary data
	// ������ ����������� �� �� ������� ���
	udword* PosList = (udword*)ICE_ALLOC_TMP(sizeof(udword)*(nb+1));

	// 1) Build main list using the primary axis
	for(udword i=0;i<nb;i++)	PosList[i] = array[i].GetMin(Axis0);
//PosList[nb++] = ConvertToSortable(MAX_FLOAT);

	// 2) Sort the list
//	static RadixSort r;
	RadixSort r;
	RadixSort* RS = &r;

//	const udword* Sorted = RS->Sort(PosList, nb, RADIX_SIGNED).GetRanks();
	// ������ �������� ���������������� ����� �� ���� ���
	// ���� �����������, ����� ��� ��� ���� � ��� �������, � ������� ����������� �����
	const udword* Sorted = RS->Sort(PosList, nb, false).GetRanks();

	// 3) Prune the list
	const udword* const LastSorted = &Sorted[nb];	// ������ ����� ������� (������ ������ ���������� �� ������� ��������)
	const udword* RunningAddress = Sorted;
	udword Index0, Index1;
	while(RunningAddress<LastSorted && Sorted<LastSorted)		// ���� �� ����� �� ������� �������
	{
		Index0 = *Sorted++;

		// ���� ��������� ������� �� ��� ����� ������� ����������� ��
		while(RunningAddress<LastSorted && PosList[*RunningAddress++]<PosList[Index0]);
//		while(PosList[*RunningAddress++]<PosList[Index0]);

		if(RunningAddress<LastSorted)
		{
			const udword* RunningAddress2 = RunningAddress;

			// ����, ��������� �� ����� ���. � ����. ������� ����� Index0 ���. ����� ������ ������ Index1
			// ���� ����� ���������, ������ �� ���� ��� ����� Index0 � Index1 ������������
			while(RunningAddress2<LastSorted && PosList[Index1 = *RunningAddress2++]<=array[Index0].GetMax(Axis0))
//			while(PosList[Index1 = *RunningAddress2++]<=(udword)ConvertToSortable(array[Index0].GetMax(Axis0)))
			{
				if(Intersect(array[Index0], array[Index1], Axis1))
				{
#ifndef KERNEL_BUG_ASAP_2D
					if(Intersect(array[Index0], array[Index1], Axis2))
					{
#endif
						// �������� �������� �� ��������� ���� ����, ������� ������������
						const ASAP_Box* Box0 = mBoxes + batched[Index0].mHandle;
						const ASAP_Box* Box1 = mBoxes + batched[Index1].mHandle;

						if (mBoxGroupIntersecterFunc(*Box1, *Box0))
						{
							AddPair(Box0->mObject, Box1->mObject, 
								Box0->mGUID, Box1->mGUID);
						}
#ifndef KERNEL_BUG_ASAP_2D
					}
#endif
				}
			}
		}
	}

	ICE_FREE(PosList);
	return true;
}

bool ArraySAP::BipartiteBoxPruning2(udword nb0, const IAABB* array0, udword nb1, const IAABB* array1, const Axes& axes, const CreateData* batched, const udword* box_indices)
{
	// nb0 - ���������� ����� ������
	// ������ �������� �� ����� ������
	// nb1 - ���-�� ������ ������
	// array1 - ������ �� ������ ������
	// axes - ���
	// batched - ������ ������ � ����� ������
	// box_indices - ����� �������� ������ ������
	// Checkings
	if(!nb0 || !array0 || !nb1 || !array1)	return false;

	// Catch axes
	const udword Axis0 = axes.mAxis0;
	const udword Axis1 = axes.mAxis1;
#ifndef KERNEL_BUG_ASAP_2D
	const udword Axis2 = axes.mAxis2;
#endif

	// Allocate some temporary data
	udword* MinPosList0 = (udword*)ICE_ALLOC_TMP(sizeof(udword)*nb0);
	udword* MinPosList1 = (udword*)ICE_ALLOC_TMP(sizeof(udword)*nb1);

	// 1) Build main lists using the primary axis
	for(udword i=0;i<nb0;i++)	MinPosList0[i] = array0[i].GetMin(Axis0);
	for(udword i=0;i<nb1;i++)	MinPosList1[i] = array1[i].GetMin(Axis0);

	// 2) Sort the lists
/*	static RadixSort r0;
	static RadixSort r1;
	RadixSort* RS0 = &r0;
	RadixSort* RS1 = &r1;*/
	RadixSort r0;
	RadixSort r1;
	RadixSort* RS0 = &r0;
	RadixSort* RS1 = &r1;

	const udword* Sorted0 = RS0->Sort(MinPosList0, nb0, false).GetRanks();
	const udword* Sorted1 = RS1->Sort(MinPosList1, nb1, false).GetRanks();

	// 3) Prune the lists
	udword Index0, Index1;

	const udword* const LastSorted0 = &Sorted0[nb0];
	const udword* const LastSorted1 = &Sorted1[nb1];
	const udword* RunningAddress0 = Sorted0;
	const udword* RunningAddress1 = Sorted1;

	while(RunningAddress1<LastSorted1 && Sorted0<LastSorted0)
	{
		Index0 = *Sorted0++;

		while(RunningAddress1<LastSorted1 && MinPosList1[*RunningAddress1]<MinPosList0[Index0])	RunningAddress1++;

		const udword* RunningAddress2_1 = RunningAddress1;

		while(RunningAddress2_1<LastSorted1 && MinPosList1[Index1 = *RunningAddress2_1++]<=array0[Index0].GetMax(Axis0))
		{
			if(Intersect(array0[Index0], array1[Index1], Axis1))
			{
#ifndef KERNEL_BUG_ASAP_2D
				if(Intersect(array0[Index0], array1[Index1], Axis2))
				{
#endif
					const ASAP_Box* Box0 = mBoxes + batched[Index0].mHandle;
					const ASAP_Box* Box1 = mBoxes + box_indices[Index1];

					if (mBoxGroupIntersecterFunc(*Box1, *Box0))
					{
						// ��� ����� � �������������� �������� �����, ���� ��� ������ � ������ ������ ������
						assert(Box1->mGUID != INVALID_ID);
						AddPair(Box0->mObject, Box1->mObject, 
							Box0->mGUID, Box1->mGUID);
					}
#ifndef KERNEL_BUG_ASAP_2D
				}
#endif
			}
		}
	}

	////

	while(RunningAddress0<LastSorted0 && Sorted1<LastSorted1)
	{
		Index0 = *Sorted1++;

		while(RunningAddress0<LastSorted0 && MinPosList0[*RunningAddress0]<=MinPosList1[Index0])	RunningAddress0++;

		const udword* RunningAddress2_0 = RunningAddress0;

		while(RunningAddress2_0<LastSorted0 && MinPosList0[Index1 = *RunningAddress2_0++]<=array1[Index0].GetMax(Axis0))
		{
			if(Intersect(array0[Index1], array1[Index0], Axis1))
			{
#ifndef KERNEL_BUG_ASAP_2D
				if(Intersect(array0[Index1], array1[Index0], Axis2))
				{
#endif
					const ASAP_Box* Box0 = mBoxes + batched[Index1].mHandle;
					const ASAP_Box* Box1 = mBoxes + box_indices[Index0];

					if (mBoxGroupIntersecterFunc(*Box1, *Box0))
					{
						// ��� ����� � �������������� �������� �����, ���� ��� ������ � ������ ������ ������
						assert(Box1->mGUID != INVALID_ID);
						AddPair(Box0->mObject, Box1->mObject, 
							Box0->mGUID, Box1->mGUID);
					}
#ifndef KERNEL_BUG_ASAP_2D
				}
#endif
			}

		}
	}

	ICE_FREE(MinPosList0);
	ICE_FREE(MinPosList1);
	return true;
}

// � ���� ������� ��������� ������ ���������� ������ ������� � SAP. �������� ����������
// ���������� � BatchCreate. ����� �� ������ ������������� id � ������� � ���������� 
// ���������� ���������� ��� ������ ������ ��� �������������.
udword ArraySAP::AddObject(void* object, udword guid, const ASAP_AABB& box, ASAP_Box::GroupMaskType group, ASAP_Box::GroupMaskType groupMask)
{
	assert(!(size_t(object)&3));	// We will use the 2 LSBs

	TRACEMSG(">>>>>>>>>> nb = %d, bnu = %d", mNbBoxes, mNbUsedBoxes);

	// ����������� ��������� ������� � ������� � ����� ����������������� ������ (��� �������������)
	udword BoxIndex;
	if(mFirstFree!=INVALID_ID)
	{
		// ���������� ������ ������� �� ����� ���������� �����
		BoxIndex = mFirstFree;
		mFirstFree = mBoxes[BoxIndex].mGUID;
		assert(mNbBoxes > mNbAddedBoxes);
		if (mNbBoxes == mNbUsedBoxes) mNbBoxes++;
		mNbAddedBoxes++;
	}
	else
	{
		// ���������� � ����� ������
		if(mNbBoxes==mMaxNbBoxes)
			ResizeBoxArray();
		BoxIndex = mNbBoxes;
		assert(mNbBoxes == mNbAddedBoxes);
		mNbBoxes++;
		mNbAddedBoxes++;
	}

	TRACEMSG("<<<<<<<<<<< nb, bna = ", mNbBoxes, mNbAddedBoxes);

	ASAP_Box* Box = &mBoxes[BoxIndex];
	// Initialize box
	Box->mObject	= object;
	Box->mGUID		= guid;
	Box->mGroup     = group;
	Box->mGroupMask = groupMask;
	for(udword i=0;i<ASAP_AxesCount;i++)
	{
		Box->mMin[i] = INVALID_INDEX;
		Box->mMax[i] = INVALID_INDEX;
	}

	CreateData* CD = (CreateData*)mCreated.Reserve(sizeof(CreateData)/sizeof(udword));
	CD->mHandle = BoxIndex;
	CD->mBox = box;

	if (mMaxGUID < guid)
		mMaxGUID = guid;

	return BoxIndex;
}

void ArraySAP::InsertEndPoints(udword axis, const ASAP_EndPoint* end_points, udword nb_endpoints)
{
	assert(axis < ASAP_AxesCount);
	ASAP_EndPoint* const BaseEP = mEndPoints[axis];

	const udword OldSize = mNbAddedBoxes*2 - nb_endpoints;
	const udword NewSize = mNbAddedBoxes*2;
	//assert(BaseEP[OldSize + 1].mValue == MAX_FLOAT);
	BaseEP[NewSize + 1] = BaseEP[OldSize + 1];

	sdword WriteIdx = NewSize;
	udword CurrInsIdx = 0;

	const ASAP_EndPoint* First = &BaseEP[0];
	const ASAP_EndPoint* Current = &BaseEP[OldSize];
	while(Current>=First)
	{
		const ASAP_EndPoint& Src = *Current;
		const ASAP_EndPoint& Ins = end_points[CurrInsIdx];

		// We need to make sure we insert maxs before mins to handle exactly equal endpoints correctly
		const bool ShouldInsert = Ins.IsMax() ? (Src.mValue <= Ins.mValue) : (Src.mValue < Ins.mValue);

		const ASAP_EndPoint& Moved = ShouldInsert ? Ins : Src;
		BaseEP[WriteIdx] = Moved;
		mBoxes[Moved.GetOwner()].mMin[axis + Moved.IsMax()] = WriteIdx--;

		if(ShouldInsert)
		{
			CurrInsIdx++;
			if(CurrInsIdx >= nb_endpoints)
				break;//we just inserted the last endpoint
		}
		else
		{
			Current--;
		}
	}
	//assert(BaseEP[NewSize + 1].mValue == MAX_FLOAT);
}

// � ���� ������� ���������� �������� ���������� ����� �������� � ������ � �� ����������
// ���������� �� ����.
void ArraySAP::BatchCreate()
{
	udword NbBatched = mCreated.GetNbEntries();
	if(!NbBatched)	return;	// Early-exit if no object has been created
	NbBatched /= sizeof(CreateData)/sizeof(udword);
	const CreateData* Batched = (const CreateData*)mCreated.GetEntries();
	mCreated.Reset();

	{

	mNbUsedBoxes = mNbAddedBoxes;

	const udword NbEndPoints = NbBatched*2;			// ��������� ����������� �������� �����
	ASAP_EndPoint* NewEPSorted = ICE_NEW_TMP(ASAP_EndPoint)[NbEndPoints];	// ������ ����� �������� �����
	ASAP_EndPoint* Buffer = (ASAP_EndPoint*)ICE_ALLOC_TMP(sizeof(ASAP_EndPoint)*NbEndPoints);	// ������ �������������� ����� ��
	RadixSort RS;

	// ���������� ������� ����� �� ����������, ���������� � �������� � ������ ������������ �� �� ������ ���
	for(udword Axis=0;Axis<ASAP_AxesCount;Axis++)
	{
		// ��������� ������� ����� ��
		for(udword i=0;i<NbBatched;i++)
		{
			const udword BoxIndex = (udword)Batched[i].mHandle;
			assert(mBoxes[BoxIndex].mMin[Axis]==INVALID_INDEX);
			assert(mBoxes[BoxIndex].mMax[Axis]==INVALID_INDEX);

			// ����� ������� ASAP_AABB ��� �����
			const float MinValue = Batched[i].mBox.GetMin(Axis);
			const float MaxValue = Batched[i].mBox.GetMax(Axis);

			//NewEPSorted[i*2+0].SetData(EncodeFloat(MinValue), BoxIndex, FALSE);
			//NewEPSorted[i*2+1].SetData(EncodeFloat(MaxValue), BoxIndex, TRUE);

			// ������ ������� � ���� �� � ������
			NewEPSorted[i*2+0].SetData(MinValue, BoxIndex, FALSE);
			NewEPSorted[i*2+1].SetData(MaxValue, BoxIndex, TRUE);
		}

		// Sort endpoints backwards
		{
			//udword* Keys = (udword*)Buffer;
			float* Keys = (float*)Buffer;
			for(udword i=0;i<NbEndPoints;i++)
				Keys[i] = NewEPSorted[i].mValue;

			//const udword* Sorted = RS.Sort(Keys, NbEndPoints, RADIX_UNSIGNED).GetRanks();
			const udword* Sorted = RS.Sort(Keys, NbEndPoints).GetRanks();

			// ������������ � ����� �������������� �� � �������� �������
			for(udword i=0;i<NbEndPoints;i++)
				Buffer[i] = NewEPSorted[Sorted[NbEndPoints-1-i]];
		}

		// ��������� �� � ��� ������������
		InsertEndPoints(Axis, Buffer, NbEndPoints);
	}

	ICE_FREE(Buffer);
	DELETEARRAY(NewEPSorted);
	}

#ifdef _DEBUG
	// ��������, ��� ��� ������� ����������� ����� ���������� ��
	for(udword i=0;i<NbBatched;i++)
	{
		udword BoxIndex = (udword)Batched[i].mHandle;
		ASAP_Box* Box = mBoxes + BoxIndex;
		assert(Box->HasBeenInserted());
	}
#endif // _DEBUG

	checkEndpointsOrder();
	

	//if(1)
	{
		// ������� ������, � ������� �������� ������ ����� �����
		mBitArray.ReservePow2(mMaxNbBoxes);

		// Using box-pruning on array indices....
		// ������ �������� �������� ����� ����� ������
		IAABB* NewBoxes = ICE_NEW_TMP(IAABB)[NbBatched];
		for(udword i=0;i<NbBatched;i++)
		{
			const ASAP_Box* Box = mBoxes + (udword)Batched[i].mHandle;		// ����� ����
			assert((udword)Batched[i].mHandle<mMaxNbBoxes);					// ����� �� ����� �� �������
			mBitArray.SetBit((udword)Batched[i].mHandle);					// ������� � ������� �������
			// ������������ ������� ��� ��
			NewBoxes[i].mMinX = Box->mMin[0];
			NewBoxes[i].mMaxX = Box->mMax[0];
			NewBoxes[i].mMinY = Box->mMin[1];
			NewBoxes[i].mMaxY = Box->mMax[1];
#ifndef KERNEL_BUG_ASAP_2D
			NewBoxes[i].mMinZ = Box->mMin[2];
			NewBoxes[i].mMaxZ = Box->mMax[2];
#endif
		}

		// ����� ����������� ����� ������ � ���������, �� ������������ �� ���
#ifdef KERNEL_BUG_ASAP_2D
		CompleteBoxPruning2(NbBatched, NewBoxes, Axes(AXES_XYZ), Batched);
#else
		CompleteBoxPruning2(NbBatched, NewBoxes, Axes(AXES_XZY), Batched);
#endif

		checkEndpointsOrder();
		// the old boxes are not the first ones in the array

		const udword NbOldBoxes = mNbUsedBoxes - NbBatched;// ��������� ������, ������ ��� ����
		if(NbOldBoxes)
		{
			// ����� ����, ���� ���������, �� ������������ �� ����� �� �������
			IAABB* OldBoxes = ICE_NEW_TMP(IAABB)[NbOldBoxes];		// ������ �������� �������� ����� ������ ������
			udword* OldBoxesIndices = (udword*)ICE_ALLOC_TMP(sizeof(udword)*NbOldBoxes);	// ������ �������� ������ ������
			udword Offset=0;		// �������-�������� �� ������� ���� ������. ������������� ����� �����
			udword i=0;				// ������� �����, ����� ���������� ������ ������ �����
			while(i<NbOldBoxes)
			{
				// �������� �� ��, ��� �� �� �������� ������������ ��������� ����.
				// ��������� ������ ��� �������� ������������� mMin[0] = INVALID_INDEX
				while (mBitArray.IsSet(Offset) || (mBoxes + Offset)->mMin[0] == INVALID_INDEX)
				{
					// ���� ������ �� �������������� ����� ����� � ��������� �����
					Offset++;
					assert(Offset<mNbBoxes);
				}

				const ASAP_Box* Box = mBoxes + Offset;		// ������ ����
				OldBoxesIndices[i] = Offset;		// ������ ������� ����� ����� � ������
				// ������������ ������� ��� ��
				OldBoxes[i].mMinX = Box->mMin[0];
				OldBoxes[i].mMaxX = Box->mMax[0];
				OldBoxes[i].mMinY = Box->mMin[1];
				OldBoxes[i].mMaxY = Box->mMax[1];
#ifndef KERNEL_BUG_ASAP_2D
				OldBoxes[i].mMinZ = Box->mMin[2];
				OldBoxes[i].mMaxZ = Box->mMax[2];
#endif
				Offset++;
				i++;
			}
			assert(i==NbOldBoxes);

#ifdef KERNEL_BUG_ASAP_2D
			BipartiteBoxPruning2(NbBatched, NewBoxes, NbOldBoxes, OldBoxes, Axes(AXES_XYZ), Batched, OldBoxesIndices);
#else
			BipartiteBoxPruning2(NbBatched, NewBoxes, NbOldBoxes, OldBoxes, Axes(AXES_XZY), Batched, OldBoxesIndices);
#endif
			ICE_FREE(OldBoxesIndices);
			DELETEARRAY(OldBoxes);
		}
		DELETEARRAY(NewBoxes);
	}
#ifdef RELEASE_ON_RESET
	mCreated.Empty();
#else
	mCreated.Reset();
#endif

	checkEndpointsOrder();
}

void ArraySAP::BatchRemove()
{
	udword NbRemoved = mRemoved.GetNbEntries();
	if(!NbRemoved)	return;	// Early-exit if no object has been removed
	const udword* Removed = mRemoved.GetEntries();
	mRemoved.Reset();

	checkEndpointsOrder();
	for(udword Axis=0;Axis<ASAP_AxesCount;Axis++)
	{
		ASAP_EndPoint* const BaseEP = mEndPoints[Axis];
		udword MinMinIndex = MAX_UDWORD;
		for(udword i=0;i<NbRemoved;i++)
		{
			assert(Removed[i]<mMaxNbBoxes);
			const ASAP_Box* RemovedObject = mBoxes + Removed[i];
			const udword MinIndex = RemovedObject->mMin[Axis];
			assert(MinIndex<mMaxNbBoxes*2+2);
			const udword MaxIndex = RemovedObject->mMax[Axis];
			assert(MaxIndex<mMaxNbBoxes*2+2);
			assert(BaseEP[MinIndex].GetOwner()==Removed[i]);
			assert(BaseEP[MaxIndex].GetOwner()==Removed[i]);
			assert(MinIndex < MaxIndex);	// ���-�� �������� ����� ���������� �� ���������
			BaseEP[MinIndex].mData = 0xfffffffe;
			BaseEP[MaxIndex].mData = 0xfffffffe;
			if(MinIndex<MinMinIndex)	MinMinIndex = MinIndex;
		}

		udword ReadIndex = MinMinIndex;
		udword DestIndex = MinMinIndex;
		const udword Limit = mNbUsedBoxes*2+2;
		while(ReadIndex!=Limit)
		{
			while(ReadIndex!=Limit && BaseEP[ReadIndex].mData == 0xfffffffe)
			{
				ReadIndex++;
			}
			if(ReadIndex!=Limit)
			{
				if(ReadIndex!=DestIndex)
				{
					BaseEP[DestIndex] = BaseEP[ReadIndex];
					assert(BaseEP[DestIndex].mData != 0xfffffffe);

					if(!BaseEP[DestIndex].IsSentinel())
					{
						udword BoxOwner = BaseEP[DestIndex].GetOwner();
						assert(BoxOwner<mMaxNbBoxes);
						ASAP_Box* Box = mBoxes + BoxOwner;

						Box->mMin[Axis + BaseEP[DestIndex].IsMax()] = DestIndex;
					}
				}
				DestIndex++;
				ReadIndex++;
			}
		}
	}

	mBitArray.ReservePow2(mMaxGUID);
	const udword Saved = NbRemoved;
	TRACEMSG(">>>>>>>>>>>> nb, bnu = ", mNbBoxes, mNbUsedBoxes);
	while(NbRemoved--)
	{
		udword Index = *Removed++;
		assert(Index<mMaxNbBoxes);

		ASAP_Box* Object = mBoxes + Index;
		assert(Object->mGUID <= mMaxGUID);
		assert(BitsToDwords(Object->mGUID) <= mBitArray.GetSize());
		mBitArray.SetBit(Object->mGUID);

		// ������ mMin[0] = INVALID_INDEX, ����� ����� ����� ���� ���� �������������
		Object->mMin[0] = INVALID_INDEX;

		Object->mGUID = mFirstFree;
		mFirstFree = Index;
	}
	mNbUsedBoxes -= Saved;
	mNbAddedBoxes = mNbUsedBoxes;
	TRACEMSG("<<<<<<<<<<< nb, bnu = ", mNbBoxes, mNbUsedBoxes);
	mPairs.RemovePairs(mBitArray);

#ifdef RELEASE_ON_RESET
	mRemoved.Empty();
#else
	mRemoved.Reset();
#endif

	checkEndpointsOrder();
}

bool ArraySAP::RemoveObject(udword handle)
{
#ifdef DEBUG_SAP
	Log(DEFAULT_LOG_NAME, logLevelInfo, "ArraySAP::RemoveObject sap_handle = %d", handle);
#endif // DEBUG_SAP
	mRemoved.Add(handle);
	return true;
}

#ifdef USE_INTEGERS
bool ArraySAP::UpdateObject(udword handle, const ASAP_AABB& box_)
#else
bool ArraySAP::UpdateObject(udword handle, const ASAP_AABB& box)
#endif
{
	const ASAP_Box* Object = mBoxes + handle;
#ifdef _DEBUG
	assert(Object->HasBeenInserted());
#endif
	const void* UserObject = Object->mObject;
	const udword UserGUID = Object->mGUID;

#ifdef USE_INTEGERS
	IAABB box;
	box.mMinX = EncodeFloat(box_.GetMin(0));
	box.mMinY = EncodeFloat(box_.GetMin(1));
	box.mMinZ = EncodeFloat(box_.GetMin(2));
	box.mMaxX = EncodeFloat(box_.GetMax(0));
	box.mMaxY = EncodeFloat(box_.GetMax(1));
	box.mMaxZ = EncodeFloat(box_.GetMax(2));
#endif

#ifdef KERNEL_BUG_ASAP_2D
	for(udword Axis=0; Axis<2;++Axis)
	{
		const udword Axis1 = (1 << Axis) & 1;
#else
	for(udword Axis=0;Axis<3;Axis++)
	{
		const udword Axis1 = (1  << Axis) & 3;
		const udword Axis2 = (1  << Axis1) & 3;
#endif
		ASAP_EndPoint* const BaseEP = mEndPoints[Axis];

		checkSentinels(BaseEP);

		// Update min
		{
			ASAP_EndPoint* CurrentMin = BaseEP + Object->mMin[Axis];
			ASSERT(!CurrentMin->IsMax());

			const ValType Limit = box.GetMin(Axis);
			if(Limit < CurrentMin->mValue)
			{
				CurrentMin->mValue = Limit;

				// Min is moving left:
				ASAP_EndPoint Saved = *CurrentMin;
				udword EPIndex = static_cast<udword>((size_t(CurrentMin) - size_t(BaseEP))/sizeof(ASAP_EndPoint));
				const udword SavedIndex = EPIndex;

				while((--CurrentMin)->mValue > Limit)
				{
#ifdef USE_PREFETCH
					_prefetch(CurrentMin-1);
#endif
					ASAP_Box* id1 = mBoxes + CurrentMin->GetOwner();
					const BOOL IsMax = CurrentMin->IsMax();
					if(IsMax)
					{
						// Our min passed a max => start overlap
						if(Object!=id1
							&& mBoxGroupIntersecterFunc(*Object, *id1)
#ifdef KERNEL_BUG_ASAP_2D
							&& Intersect1D(*Object, *id1, Axis1)
							&& Intersect1D_Min(box, *id1, BaseEP, Axis)
#else
							&& Intersect2D(*Object, *id1, Axis1, Axis2)
							&& Intersect1D_Min(box, *id1, BaseEP, Axis)
#endif
							)
						{
							AddPair(UserObject, id1->mObject, 
								UserGUID, id1->mGUID);
						}
					}

					id1->mMin[Axis + IsMax] = EPIndex--;
					*(CurrentMin+1) = *CurrentMin;
				}

				if(SavedIndex!=EPIndex)
				{
					mBoxes[Saved.GetOwner()].mMin[Axis + Saved.IsMax()] = EPIndex;
					BaseEP[EPIndex] = Saved;
				}
			}
			else if(Limit > CurrentMin->mValue)
			{
				CurrentMin->mValue = Limit;

				// Min is moving right:
				ASAP_EndPoint Saved = *CurrentMin;
				udword EPIndex = static_cast<udword>((size_t(CurrentMin) - size_t(BaseEP))/sizeof(ASAP_EndPoint));
				const udword SavedIndex = EPIndex;

				while((++CurrentMin)->mValue < Limit)
				{
#ifdef USE_PREFETCH
					_prefetch(CurrentMin+1);
#endif
					ASAP_Box* id1 = mBoxes + CurrentMin->GetOwner();
					const BOOL IsMax = CurrentMin->IsMax();
					if(IsMax)
					{
						// Our min passed a max => stop overlap
						if(Object!=id1
#ifdef USE_OVERLAP_TEST_ON_REMOVES
							&& mBoxGroupIntersecterFunc(*Object, *id1)
# ifdef KERNEL_BUG_ASAP_2D
							&& Intersect1D(*Object, *id1, Axis1)
# else
							&& Intersect2D(*Object, *id1, Axis1, Axis2)
# endif
#endif
							)
							RemovePair(UserObject, id1->mObject, 
								UserGUID, id1->mGUID);
					}

					id1->mMin[Axis + IsMax] = EPIndex++;
					*(CurrentMin-1) = *CurrentMin;
				}

				if(SavedIndex!=EPIndex)
				{
					mBoxes[Saved.GetOwner()].mMin[Axis + Saved.IsMax()] = EPIndex;
					BaseEP[EPIndex] = Saved;
				}
			}
		}

		checkSentinels(BaseEP);

		// Update max
		{
			ASAP_EndPoint* CurrentMax = BaseEP + Object->mMax[Axis];
			ASSERT(CurrentMax->IsMax());

			const ValType Limit = box.GetMax(Axis);
			if(Limit > CurrentMax->mValue)
			{
				CurrentMax->mValue = Limit;

				// Max is moving right:
				ASAP_EndPoint Saved = *CurrentMax;
				udword EPIndex = static_cast<udword>((size_t(CurrentMax) - size_t(BaseEP))/sizeof(ASAP_EndPoint));
				const udword SavedIndex = EPIndex;

				while((++CurrentMax)->mValue < Limit)
				{
#ifdef USE_PREFETCH
					_prefetch(CurrentMax+1);
#endif
					ASAP_Box* id1 = mBoxes + CurrentMax->GetOwner();
					const BOOL IsMax = CurrentMax->IsMax();
					if(!IsMax)
					{
						// Our max passed a min => start overlap
						if(Object!=id1
							&& mBoxGroupIntersecterFunc(*Object, *id1)
#ifdef KERNEL_BUG_ASAP_2D
							&& Intersect1D(*Object, *id1, Axis1)
							&& Intersect1D_Min(box, *id1, BaseEP, Axis)
#else
							&& Intersect2D(*Object, *id1, Axis1, Axis2)
							&& Intersect1D_Min(box, *id1, BaseEP, Axis)
#endif
							)
						{
							AddPair(UserObject, id1->mObject, 
								UserGUID, id1->mGUID);
						}
					}

					id1->mMin[Axis + IsMax] = EPIndex++;
					*(CurrentMax-1) = *CurrentMax;
				}

				if(SavedIndex!=EPIndex)
				{
					mBoxes[Saved.GetOwner()].mMin[Axis + Saved.IsMax()] = EPIndex;
					BaseEP[EPIndex] = Saved;
				}
			}
			else if(Limit < CurrentMax->mValue)
			{
				CurrentMax->mValue = Limit;

				// Max is moving left:
				ASAP_EndPoint Saved = *CurrentMax;
				udword EPIndex = static_cast<udword>((size_t(CurrentMax) - size_t(BaseEP))/sizeof(ASAP_EndPoint));
				const udword SavedIndex = EPIndex;

				while((--CurrentMax)->mValue > Limit)
				{
#ifdef USE_PREFETCH
					_prefetch(CurrentMax-1);
#endif
					ASAP_Box* id1 = mBoxes + CurrentMax->GetOwner();
					const BOOL IsMax = CurrentMax->IsMax();
					if(!IsMax)
					{
						// Our max passed a min => stop overlap
						if(Object!=id1
#ifdef USE_OVERLAP_TEST_ON_REMOVES
							&& mBoxGroupIntersecterFunc(*Object, *id1)
# ifdef KERNEL_BUG_ASAP_2D
							&& Intersect1D(*Object, *id1, Axis1)
# else
							&& Intersect2D(*Object, *id1, Axis1, Axis2)
# endif
#endif
							)
							RemovePair(UserObject, id1->mObject, 
								UserGUID, id1->mGUID);
					}

					id1->mMin[Axis + IsMax] = EPIndex--;
					*(CurrentMax+1) = *CurrentMax;
				}

				if(SavedIndex!=EPIndex)
				{
					mBoxes[Saved.GetOwner()].mMin[Axis + Saved.IsMax()] = EPIndex;
					BaseEP[EPIndex] = Saved;
				}
			}
		}

		checkSentinels(BaseEP);
		assert(Object->mMin[Axis] < Object->mMax[Axis]);	// ������������ ����� �������� �����
	}
	return true;
}



udword ArraySAP::DumpPairs(SAP_CreatePair create_cb, SAP_DeletePair delete_cb, void* cb_user_data, ASAP_Pair** pairs)
{
#ifdef DEBUG_SAP
	Log(DEFAULT_LOG_NAME, logLevelInfo, "DumpPairs");
#endif // DEBUG_SAP

	checkEndpointsOrder();

	BatchCreate();

	const udword* Entries = mData.GetEntries();
	const udword* Last = Entries + mData.GetNbEntries();

	// mCreated here is free to use for another purpouse
	// We will use mCreated to save deleted objects ids (only in this method,
	// just to not create another container)
	ASSERT(mCreated.GetNbEntries() == 0);

#if defined(DEBUG)
// Macro to check mCreated is not changed in user callbacks.
// If this is needed to add new objects in callbacks, it's needed to rewrite 
// this method to not use mCreated for temporary deleted ids storage
# define PREP_MCREATED_CHECK const udword __nbcreated = mCreated.GetNbEntries()
# define EXEC_MCREATED_CHECK ASSERT(__nbcreated == mCreated.GetNbEntries())
#else
# define PREP_MCREATED_CHECK
# define EXEC_MCREATED_CHECK
#endif

	// �������� �� ������� �����/��������� ���, ���� ���������
	while(Entries!=Last)
	{
		const udword ID = *Entries++;
		ASAP_Pair* UP = mPairs.mActivePairs + ID;

		{
			ASSERT(UP->IsInArray());
			if(UP->IsRemoved())
			{
				// No need to call "ClearInArray" in this case, since the pair will get removed anyway

				// Remove
				if(delete_cb && !UP->IsNew())
				{
					PREP_MCREATED_CHECK;
#ifdef PAIR_USER_DATA
					(delete_cb)(UP->GetObject0(), UP->GetObject1(), cb_user_data, UP->userData);
#else
					(delete_cb)(UP->GetObject0(), UP->GetObject1(), cb_user_data, null);
#endif
					EXEC_MCREATED_CHECK;
				}

				// ��������� ������� �������� ��������� ����
				mCreated.Add(UP->id0);
				mCreated.Add(UP->id1);
			}
			else
			{
				UP->ClearInArray();
				// Add => already there... Might want to create user data, though
				if(UP->IsNew())
				{
					if(create_cb)
					{
						PREP_MCREATED_CHECK;
#ifdef PAIR_USER_DATA
						UP->userData = (create_cb)(UP->GetObject0(), UP->GetObject1(), cb_user_data);
#else
						(create_cb)(UP->GetObject0(), UP->GetObject1(), cb_user_data);
#endif
						EXEC_MCREATED_CHECK;
					}
					UP->ClearNew();
				}
			}
		}
	}

	// #### try batch removal here
	Entries = mCreated.GetEntries();
	const udword* LastEntry = Entries + mCreated.GetNbEntries();
	while (Entries != LastEntry)
	{
		const udword id0 = *Entries++; 
		ASSERT(Entries != LastEntry);
		const udword id1 = *Entries++;
#ifdef _DEBUG
		bool Status = mPairs.RemovePair(id0, id1);	// ������� �� ����������
		ASSERT(Status);
#else
		mPairs.RemovePair(id0, id1);	// ������� �� ����������
#endif // _DEBUG
	}

#ifdef RELEASE_ON_RESET
	mCreated.Empty();
	mData.Empty();
#else
	mCreated.Reset();
	mData.Reset();
#endif

	// ������������ ������� ������� � ���� �� �����������
	BatchRemove();

	if(pairs)	*pairs = mPairs.mActivePairs;

	checkEndpointsOrder();

	return mPairs.mNbActivePairs;
}


#if defined (USING_GTEST)
# pragma push_macro("inline_")
# undef inline_
# define inline_
#endif

inline_	void ArraySAP::AddPair(const void* object0, const void* object1, udword id0, udword id1)
{
	ASSERT(object0);
	ASAP_Pair* UP = (ASAP_Pair*)mPairs.AddPair(id0, id1, null, null);
	ASSERT(UP);

	if(UP->object0)
	{
		// Persistent pair
	}
	else
	{

		// New pair
		ASSERT(object0);
		ASSERT(object1);
		UP->object0 = object0;
		UP->object1 = object1;
		TRACEMSG2("ArraySAP::AddPair", id0, id1, object0, object1);
		UP->SetInArray();
		mData.Add(mPairs.GetPairIndex(UP));
		UP->SetNew();
	}
	UP->ClearRemoved();
}

#if defined (USING_GTEST)
# pragma pop_macro("inline_")
#endif

/*inline_*/	bool ArraySAP::RemovePair(const void* object0, const void* object1, udword id0, udword id1)
{
	UNUSED_ARG(object0);
	UNUSED_ARG(object1);
	ASAP_Pair* UP = (ASAP_Pair*)mPairs.FindPair(id0, id1);
	if(UP)
	{
		if(!UP->IsInArray())
		{
			UP->SetInArray();
			mData.Add(mPairs.GetPairIndex(UP));
		}
		UP->SetRemoved();
		return true;
	}
	return false;
}

#ifndef NDEBUG
inline_ void ArraySAP::checkEndpointsOrder() const
{
	if (mEndPoints[0] == NULL)
		return;
	checkSentinels();

	// ��������, ��� ��� �� ������������� ���������
	for(udword i=0;i<mNbUsedBoxes*2+1;i++)
	{
		assert(mEndPoints[0][i].mValue <= mEndPoints[0][i+1].mValue);
		assert(mEndPoints[1][i].mValue <= mEndPoints[1][i+1].mValue);
# ifndef KERNEL_BUG_ASAP_2D
		assert(mEndPoints[2][i].mValue <= mEndPoints[2][i+1].mValue);
# endif
	}
}

inline_ void ArraySAP::checkSentinels() const
{
		checkSentinels(mEndPoints[0]);
		checkSentinels(mEndPoints[1]);
#ifndef KERNEL_BUG_ASAP_2D
		checkSentinels(mEndPoints[2]);
#endif
}

inline_ void ArraySAP::checkSentinels(ASAP_EndPoint const* axis) const
{
		// �������� ��������� ����������
		assert(axis[0].mValue == MIN_FLOAT);
		assert(axis[mNbUsedBoxes*2 + 1].mValue == MAX_FLOAT);

		assert(axis[0].mData == 0xfffffffc);
		assert(axis[mNbUsedBoxes*2 + 1].mData == 0xffffffff);
}

#endif // NDEBUG
