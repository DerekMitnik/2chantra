///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *	OPCODE - Optimized Collision Detection
 *	Copyright (C) 2001 Pierre Terdiman
 *	Homepage: http://www.codercorner.com/Opcode.htm
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *	Contains an array-based version of the sweep-and-prune algorithm
 *	\file		OPC_ArraySAP.h
 *	\author		Pierre Terdiman
 *	\date		December, 2, 2007
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Include Guard
#ifndef OPC_ARRAYSAP_H
#define OPC_ARRAYSAP_H

#define KERNEL_BUG_ASAP_2D

#ifdef KERNEL_BUG_ASAP_2D
enum { ASAP_AxesCount = 2 };
#else
enum { ASAP_AxesCount = 3 };
#endif

#include "SweepAndPrune.h"

#include "ASAP_Types.h"

#ifdef USE_WORDS
	typedef uword			IndexType;
	#define	INVALID_INDEX	0xffff
#else
	typedef udword			IndexType;
	#define	INVALID_INDEX	0xffffffff
#endif

#ifdef USE_INTEGERS
	typedef udword	ValType;
	typedef IAABB	SAP_AABB;
#else
	typedef float	ValType;
	typedef ASAP_AABB	SAP_AABB;
#endif


class CRay;

namespace Opcode
{
	#include "ASAP_PairManager.h"


	class ASAP_EndPoint : public IceCore::Allocateable
	{
	public:
		inline_					ASAP_EndPoint()		{}
		inline_					~ASAP_EndPoint()	{}

		ValType			mValue;		// Min or Max value
		udword			mData;		// Parent box | MinMax flag
	public:

		inline_	bool			IsSentinel()	const	{ return (mData&~3)==0xfffffffc;	}

		inline_	void			SetData(ValType v, udword owner_box_id, BOOL is_max)
		{
			mValue = v;
			mData = owner_box_id<<2;
			assert(ASAP_AxesCount <= 3);
			if(is_max)	mData |= ASAP_AxesCount;
		}
		/// IsMax result is used to select array in class ASAP_Box (mMin or mMax),
		/// so it must be the count of axes

		inline_	BOOL			IsMax()		const	{ return mData & 3;		}
		inline_	udword			GetOwner()	const	{ return mData>>2;		}
	};

	class ASAP_Box : public IceCore::Allocateable
	{
	public:
		inline_					ASAP_Box()	{}
		inline_					~ASAP_Box()	{}

		/// DO NOT CHANGE THE ORDER OF THESE ARRAYS!
		/// DO NOT INSERT FIELDS BETWEEN THEM!
		/// PRAY THAT COMPILER WILL NOT INSERT ANYTHING BETWEEN THEM!
		IndexType		mMin[ASAP_AxesCount];		// ������ �������� ����� �� ����
		IndexType		mMax[ASAP_AxesCount];		// ������ �������� ����� �� ����

		typedef uword GroupMaskType;
		GroupMaskType   mGroup;						// Object is belong to groups (butmask)
		GroupMaskType   mGroupMask;					// Object will intersect with groups (bitmask)

		void*			mObject;					// ��������� �� ������� ������
		udword			mGUID;						// ����� �������� ������� (�� �������� ����� ����, �� SAP'a)

		inline_	ValType	GetMaxValue(udword i, const ASAP_EndPoint* base)	const
		{
			assert(i < ASAP_AxesCount);
			return base[mMax[i]].mValue;
		}

		inline_	ValType	GetMinValue(udword i, const ASAP_EndPoint* base)	const
		{
			assert(i < ASAP_AxesCount);
			return base[mMin[i]].mValue;
		}

		GroupMaskType   IsIntersectsWith(GroupMaskType objectGroup) const
		{
			return objectGroup & mGroupMask;
		}

	#ifdef _DEBUG
		bool			HasBeenInserted()	const
		{
			assert(mMin[0]!=INVALID_INDEX);
			assert(mMax[0]!=INVALID_INDEX);
			assert(mMin[1]!=INVALID_INDEX);
			assert(mMax[1]!=INVALID_INDEX);
#ifndef KERNEL_BUG_ASAP_2D
			assert(mMin[2]!=INVALID_INDEX);
			assert(mMax[2]!=INVALID_INDEX);
#endif
			return true;
		}
	#endif
	};



	typedef void* (*SAP_CreatePair)(const void* object0, const void* object1, void* user_data);
	typedef void  (*SAP_DeletePair)(const void* object0, const void* object1, void* user_data, void* pair_user_data);

	// Forward declarations
	class ASAP_EndPoint;
	class ASAP_Box;
	struct IAABB;
	struct CreateData;

	// ������� �����, ������ �����, ������� �� � ���������� �����������
	class OPCODE_API ArraySAP : public IceCore::Allocateable
	{
		public:
			// Method of intersection object's group.
			// SIMMETRICAL = (A.group & B.mask) | (B.group & A.mask)
			// NONSIMMETRICAL = (A.group & B.mask), where A.handle > B.handle on object creation or A is the updated object
			enum BoxGroupIntersectionMethod { TRIVIAL, SIMMETRICAL, NONSIMMETRICAL };

									ArraySAP(BoxGroupIntersectionMethod method = TRIVIAL);
									~ArraySAP();

				udword				AddObject(void* object, udword guid, const ASAP_AABB& box, ASAP_Box::GroupMaskType group = 1, ASAP_Box::GroupMaskType groupMask = 1);
				bool				RemoveObject(udword handle);
				bool				UpdateObject(udword handle, const ASAP_AABB& box);

				udword				DumpPairs(SAP_CreatePair create_cb, SAP_DeletePair delete_cb, void* cb_user_data, ASAP_Pair** pairs=null);

		const	ASAP_EndPoint*		GetEndPoints(udword axis) { return mEndPoints[axis]; }
		const	ASAP_Box*			GetBoxes() { return mBoxes; }
				udword				GetNbUsedBoxes() { return mNbUsedBoxes; }

		/*inline_*/	bool			RemovePair(const void* object0, const void* object1, udword id0, udword id1);

				void				SetBoxGroupIntersectionMethod(BoxGroupIntersectionMethod method);

		private:
				IceCore::Container	mData;	// ��� �������� ������� ��� �����������, ������� ��������� ��� �����������
				ASAP_PairManager	mPairs;	// �������� ��� �����������

#if defined (USING_GTEST)
# pragma push_macro("inline_")
# undef inline_
# define inline_
#endif
				// ��� ��������� � OPC_ArraySAP.cpp
		inline_	void				AddPair(const void* object0, const void* object1, udword id0, udword id1);
		//inline_	void				RemovePair(const void* object0, const void* object1, uword id0, uword id1);
#if defined (USING_GTEST)
# pragma pop_macro("inline_")
#endif

				udword				mNbAddedBoxes;	// ���������� ������ � �������, ������� ���� ��������� � BatchAdd
				udword				mNbUsedBoxes;	// ���������� ��������� ������, ������ ��������� � ������� � ������������ � ������ ������
				udword				mNbBoxes;		// ���������� ������, ������ ��������� � �������
				udword				mMaxNbBoxes;	// ���������� ������, ��� ������� �������� ������
				ASAP_Box*			mBoxes;			// ������ ������
				ASAP_EndPoint*		mEndPoints[ASAP_AxesCount];	// ������ �������� ����� ������ �� ����
				udword				mFirstFree;		// ��������� ��������� ����� ����� - ��������� ��������� ������ � �������

				udword				mMaxGUID;
				IceCore::BitArray	mBitArray;		// bit array for BatchCreate and BatchRemove


				void				ResizeBoxArray();
			// For batch creation
				IceCore::Container	mCreated;
				void				BatchCreate();
				void				InsertEndPoints(udword axis, const ASAP_EndPoint* end_points, udword nb_endpoints);
				bool				CompleteBoxPruning2(udword nb, const IAABB* array, const Axes& axes, const CreateData* batched);
				bool				BipartiteBoxPruning2(udword nb0, const IAABB* array0, udword nb1, const IAABB* array1, const Axes& axes, const CreateData* batched, const udword* box_indices);

			// For batch removal
				IceCore::Container	mRemoved;
				void				BatchRemove();


			// For intersection by box group
				ASAP_Box::GroupMaskType (*mBoxGroupIntersecterFunc)(ASAP_Box const& a, ASAP_Box const& b);

		static	ASAP_Box::GroupMaskType IsIntersectByGroupSimmetrical(ASAP_Box const& a, ASAP_Box const& b)
		{
			return a.IsIntersectsWith(b.mGroup) | b.IsIntersectsWith(a.mGroup);		
		}

		static	ASAP_Box::GroupMaskType IsIntersectByGroupNonsimmetrical(ASAP_Box const& a, ASAP_Box const& b)
		{
			return a.IsIntersectsWith(b.mGroup);		
		}

		static	ASAP_Box::GroupMaskType IsIntersectByGroupTrivial(ASAP_Box const& a, ASAP_Box const& b)
		{
			UNUSED_ARG(a);
			UNUSED_ARG(b);
			return 1;		
		}

#ifdef NDEBUG
				void checkEndpointsOrder() const {}
				void checkSentinels() const {}
				void checkSentinels(ASAP_EndPoint const* axis) const {UNUSED_ARG(axis);}
#else
				void checkEndpointsOrder() const;
				void checkSentinels() const;
				void checkSentinels(ASAP_EndPoint const* axis) const;
#endif // NDEBUG

	#if defined(USING_GTEST)
		friend class SAPTest;
	#endif
	};

	
	inline_ BOOL Intersect2D(const ASAP_Box& c, const ASAP_Box& b, udword axis1, udword axis2)
	{
		if(	b.mMax[axis1] < c.mMin[axis1] || c.mMax[axis1] < b.mMin[axis1]
		||	b.mMax[axis2] < c.mMin[axis2] || c.mMax[axis2] < b.mMin[axis2])	return FALSE;
		return TRUE;
	}

#ifdef KERNEL_BUG_ASAP_2D
	inline_ BOOL Intersect1D(const ASAP_Box& c, const ASAP_Box& b, udword axis1)
	{
		if(	b.mMax[axis1] < c.mMin[axis1] || c.mMax[axis1] < b.mMin[axis1])	return FALSE;
		return TRUE;
	}
#endif

	inline_ BOOL Intersect1D_Min(const SAP_AABB& a, const ASAP_Box& b, const ASAP_EndPoint* const base, udword axis)
	{
		if(b.GetMaxValue(axis, base) < a.GetMin(axis))
			return FALSE;
		return TRUE;
	}

}

#endif // OPC_ARRAYSAP_H
