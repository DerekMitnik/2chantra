#include "StdAfx.h"

#include <map>

#include "2de_Matrix2.h"
#include "phys_collisionsolver.h"

#include "sap/OPC_ArraySAP.h"

#ifdef SELECTIVE_RENDERING
# include "../object_mgr.h"
#endif

#include "../objects/object_dynamic.h"
#include "../objects/object_bullet.h"
#include "../objects/object_item.h"
#include "../objects/object_player.h"
#include "../objects/object_enemy.h"
#include "../objects/object_ray.h"

#include "../editor.h"
#ifdef MAP_EDITOR
extern editor::EditorStates editor_state;
#endif // MAP_EDITOR
//////////////////////////////////////////////////////////////////////////

Opcode::ArraySAP *asap = NULL;
Opcode::ArraySAP *tempAsap = NULL;

#ifdef DEBUG_DRAW_COLLISION_PAIRS
extern list<Vector2> collPairs;
#endif // DEBUG_DRAW_COLLISION_PAIRS

extern bool isNewGameCreated;

//////////////////////////////////////////////////////////////////////////

bool SolveDynamicVSStatic(ObjDynamic* objD, ObjPhysic* objS, Vector2 n, float depth);
bool SolveDynamicVSDynamic(ObjDynamic* objD1, ObjDynamic* objD2, Vector2 n, float depth);
void SolveBullet(ObjBullet* bul, ObjPhysic* obj, Vector2 n, float depth);
void SolveEnvironment(ObjEnvironment* env, ObjDynamic* obj);
void SolveDelayedEnvCollision();
void SolveEnvLeave(ObjEnvironment* env, ObjDynamic* obj);
bool SolidTo( ObjPhysic* obj, ObjectType type );
bool SolveRay(ObjRay* ray, ObjPhysic* obj, Vector2& pos, Vector2& norm);

void ProcessRayCollision();

//////////////////////////////////////////////////////////////////////////

// ������ ��������� � ��������, �������, ��������, ����� ������������ � ����� ����.
// ������ ���� ���������, ������ - ������.
list<GameObject*> delayed_env_coll;

#ifdef DEBUG_PRINT
UINT dbgPairsCount = 0;
#endif // DEBUG_PRINT

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// ��� ��������� callback-� �� �������� � �������� ��� �����������. ���� ��� �������������� ������ ��� �������.
//#if defined(DEBUG_SAP_PAIRS_COORDS)
	#define USE_PAIR_CALLBACK
//#endif // defined(DEBUG_SAP_PAIRS_COORDS)

#ifdef USE_PAIR_CALLBACK
void* OnPairCreate(const void* object0, const void* object1, void* user_data)
{
	UNUSED_ARG(user_data);
#ifdef DEBUG_SAP_PAIRS_COORDS
	const ObjPhysic* o0 = static_cast<const ObjPhysic*>(object0);
	const ObjPhysic* o1 = static_cast<const ObjPhysic*>(object1);

	Log(DEFAULT_LOG_NAME, logLevelInfo, "Added pair %d (%.2f, %.2f - %.2f, %.2f) - %d (%.2f, %.2f - %.2f, %.2f)",
		o0->id, o0->aabb.Left(), o0->aabb.Top(), o0->aabb.Right(), o0->aabb.Bottom(), o1->id, o1->aabb.Left(), o1->aabb.Top(), o1->aabb.Right(), o1->aabb.Bottom());
#else
	UNUSED_ARG(object0);
	UNUSED_ARG(object1);
#endif // DEBUG_SAP_PAIRS_COORDS
	return NULL;
}

void  OnPairRemove(const void* object0, const void* object1, void* user_data, void* pair_user_data)
{
	UNUSED_ARG(pair_user_data);
	UNUSED_ARG(user_data);
	const ObjPhysic* o0 = static_cast<const ObjPhysic*>(object0);
	const ObjPhysic* o1 = static_cast<const ObjPhysic*>(object1);
#ifdef DEBUG_SAP_PAIRS_COORDS
	Log(DEFAULT_LOG_NAME, logLevelInfo, "Removed pair %d (%.2f, %.2f - %.2f, %.2f) - %d (%.2f, %.2f - %.2f, %.2f)",
		o0->id, o0->aabb.Left(), o0->aabb.Top(), o0->aabb.Right(), o0->aabb.Bottom(), o1->id, o1->aabb.Left(), o1->aabb.Top(), o1->aabb.Right(), o1->aabb.Bottom());
#endif // DEBUG_SAP_PAIRS_COORDS

	if (o0->type == objEnvironment && o1->IsDynamic())
		SolveEnvLeave((ObjEnvironment*)o0, (ObjDynamic*)o1);
	else if  (o1->type == objEnvironment && o0->IsDynamic())
		SolveEnvLeave((ObjEnvironment*)o1, (ObjDynamic*)o0);
}
#endif // USE_PAIR_CALLBACK


////////////////////////////////////////////////////////////////////////// 
////////////////////////////////////////////////////////////////////////// 


// ���� ���� ��� �������� �� ��, ����� ����� ������ �� ����� � SAP � �� �����, ����� 
// �������� ������� ProcessCollisions. ���� ���-�� ������� asap->DumpPairs ��� asap->UpdateObject,
// �� ����� ��������� ��������� ����������� ������� asap->mPairs.mActivePairs ��� ���� ��� �����,
// ��� �������� � ����, ��� � ProcessCollisions �������� ��������� �� ���������� ������.
// ����� ����� �� ���������, �� "�����" ��������� SAP � ����������, ��� ��� ������� ������� 
// DumpPairs ��� UpdateObject. �����, � ����� ProcessCollisions �� "��������" SAP � 
// �������� ��� �������.

bool sapStateLocked = false;
bool sapStateUpdatePending = false;
bool sapObjUpdatePending = false;
class UpdateObjInfo
{
public:
	UINT handle;
	ASAP_AABB aabb;
	
	UpdateObjInfo(UINT h, const ASAP_AABB& a) 
		: handle(h), aabb(a)
	{ }


};

list<UpdateObjInfo> updateObjList;

// ��������� �������. ������������ ������ ������ ����� �����.
// � ��� � ��������. ������������ � object_mgr.cpp, ���� SELECTIVE_RENDERER �������.
ASAP_AABB GetASAP_AABB( CAABB& aabb )
{
	ASAP_AABB box;
	box.mMin.x = aabb.Left();
	box.mMax.x = aabb.Right();
	box.mMin.y = aabb.Top();
	box.mMax.y = aabb.Bottom();
#ifndef KERNEL_BUG_ASAP_2D
	box.mMin.z = 0;
	box.mMax.z = 1;
#endif
	return box;
}

// ���������� ��� ������������� ������.
void InitArraySAP()
{
	asap = new Opcode::ArraySAP();
}

// ���������� ����� ������������ ���� ��������.
void PrepareRemoveArraySAP()
{
	tempAsap = asap;
	asap = NULL;
}

void RemoveArraySAP()
{
	DELETESINGLE(tempAsap);
}

UINT AddSAPObject(ObjPhysic* obj, UINT id, CAABB& aabb)
{
	return asap->AddObject(obj, (udword)id, GetASAP_AABB(aabb));
}

void RemoveSAPObject(UINT sap_handle)
{
	asap->RemoveObject(sap_handle);
}

// ���������� ��� ����������� ���� ��������.
void RemoveTempSAPObject(UINT sap_handle)
{
	tempAsap->RemoveObject(sap_handle);
}

// �������� ��� ������� ������ DumpPairs(NULL, NULL, NULL, NULL)
void UpdateSAPState()
{
	if (sapStateLocked)
	{
		sapStateUpdatePending = true;
		return;
	}

#ifdef USE_PAIR_CALLBACK
	asap->DumpPairs(OnPairCreate, OnPairRemove, NULL, NULL);
#else
	asap->DumpPairs(NULL, NULL, NULL, NULL);
#endif // USE_PAIR_CALLBACK

	sapStateUpdatePending = false;
}

void DumpPairs()
{
	asap->DumpPairs(NULL, NULL, NULL);
}

void DumpTempPairs()
{
	tempAsap->DumpPairs(NULL, NULL, NULL);
}

// �������� ��� ������� ������ UpdateObject
void UpdateSAPObject(UINT sap_handle, CAABB& aabb)
{
	if (sapStateLocked)
	{
		sapObjUpdatePending = true;
		updateObjList.push_back(UpdateObjInfo(sap_handle, GetASAP_AABB(aabb)));
	}
	else
	{
		asap->UpdateObject(sap_handle, GetASAP_AABB(aabb));
	}
}

void UpdateObject(UINT sap_handle, CAABB& aabb)
{
	asap->UpdateObject(sap_handle, GetASAP_AABB(aabb));
}

void BatchSAPObjUpdate()
{
	if (!sapObjUpdatePending)
		return;
	assert(!sapStateLocked);
	assert(!updateObjList.empty());
	for (list<UpdateObjInfo>::iterator it = updateObjList.begin(); it != updateObjList.end(); it++)
	{
		UpdateObjInfo& uoi = *it;
		asap->UpdateObject(uoi.handle, uoi.aabb);
	}
	updateObjList.clear();
	sapObjUpdatePending = false;
}

//////////////////////////////////////////////////////////////////////////
//  ������������ �������. ������������� ������� ��� � ����� ����� �������� ��-�� ����������� ������ ������ �� ������������:
//���� �� ������������ "��������� ���������" � ������ ����������� �� depth, �� ��������� ����������� �� ��������� ���� ����� �����.

void PushOut( GameObject* obj, float depth, Vector2 n)
{
	obj->aabb.p += depth * n * 1.001f;
}

//////////////////////////////////////////////////////////////////////////

// ��������� ������������
void ProcessCollisions()
{
	Opcode::ASAP_Pair* pairs;

#ifdef DEBUG_SAP
	Log(DEFAULT_LOG_NAME, logLevelInfo, "Beginning ProcessCollisions");
#endif // DEBUG_SAP

#ifdef DEBUG_DRAW_COLLISION_PAIRS
	collPairs.clear();
#endif // DEBUG_DRAW_COLLISION_PAIRS

#ifdef DEBUG_PRINT
	dbgPairsCount = 0;
#endif // DEBUG_PRINT

	assert(!sapStateLocked);	// ���� ����� ���, �� ������ ���-�� ��������� ��������� ����� ProcessCollisions
	sapStateLocked = true;

#ifdef USE_PAIR_CALLBACK
	UINT pairsCount = asap->DumpPairs(OnPairCreate, OnPairRemove, NULL, &pairs);
#else
	UINT pairsCount = asap->DumpPairs(NULL, NULL, NULL, &pairs);
#endif // USE_PAIR_CALLBACK
	
#ifdef DEBUG_PRINT
	dbgPairsCount = pairsCount;
#endif // DEBUG_PRINT
#ifdef DEBUG_SAP
	Log(DEFAULT_LOG_NAME, logLevelInfo, "pairs count = %d", pairsCount);
#endif // DEBUG_SAP

	if (pairsCount)
	{
		ObjPhysic** odArr = new ObjPhysic*[pairsCount * 2];
		for(UINT i = 0; i < pairsCount; i++)
		{
			odArr[i*2 + 0] = (ObjPhysic*)pairs[i].object0;
			odArr[i*2 + 1] = (ObjPhysic*)pairs[i].object1;
			// ������� ���������� ���� ������������
			SolveCollision(odArr[i*2 + 0], odArr[i*2 + 1]);
			if (isNewGameCreated)
			{
				DELETEARRAY(odArr);
				delayed_env_coll.clear();
				assert(sapStateLocked);
				sapStateLocked = false;
				updateObjList.clear();
				return;
			}
		}
		for(UINT i = 0, j = 0; i < pairsCount * 2; i+=2)
		{
			j = i+1;
			// ���������� ��������� �������� ����� ����, ��� �� ������������ ���� ������
			asap->UpdateObject(odArr[i]->sap_handle, GetASAP_AABB(odArr[i]->aabb) );
			asap->UpdateObject(odArr[j]->sap_handle, GetASAP_AABB(odArr[j]->aabb) );
		}
		DELETEARRAY(odArr);
	}

	SolveDelayedEnvCollision();
	ProcessRayCollision();

	assert(sapStateLocked);
	sapStateLocked = false;
	if (sapStateUpdatePending) UpdateSAPState();
	BatchSAPObjUpdate();

#ifdef DEBUG_SAP
	Log(DEFAULT_LOG_NAME, logLevelInfo, "Ending ProcessCollisions");
#endif // DEBUG_SAP

}

//////////////////////////////////////////////////////////////////////////

// ������ ��� ���������� ��������, ������� ��� �������
ObjPhysic* arRayObjIntersections[maxRayIntersectionsCount];
// ������ ���������� �� ������ ���� �� ����� ����������� ���� � ���������
float arRayObjIntersectionsDistances[maxRayIntersectionsCount];
// ������ ���������, � ������� ��� ������� �������
Vector2 arRayObjIntersectionsPositions[maxRayIntersectionsCount];
// ������ �������� � ��������, ������� ��� �������
Vector2 arRayObjIntersectionsNorms[maxRayIntersectionsCount];

// � ��� ������� ���� ���������� ������� ��������, � �������� ��� ������������,
// � ���������� �� ���.
UINT GetRayIntersections(bool dir, float posFirstCoord, float limit, 
			  AxisIndex fa, AxisIndex sa, ObjRay* ray) 
{
	CRay const& r = ray->getRay();

	const Opcode::ASAP_Box* boxes = asap->GetBoxes();
	const Opcode::ASAP_EndPoint* firstAxis	= asap->GetEndPoints(fa);
	const Opcode::ASAP_EndPoint* secondAxis = asap->GetEndPoints(sa);

	const Opcode::ASAP_EndPoint* current = firstAxis + ( dir ? 0 : (asap->GetNbUsedBoxes()*2 + 1) );
	assert(current->mValue == (dir ? MIN_FLOAT : MAX_FLOAT));

	const Opcode::ASAP_Box* object = NULL;
	const Opcode::ASAP_EndPoint* anotherEP;				// ��� ����� �����, ��������������� ��������� �� firstAxis
	const Opcode::ASAP_EndPoint *minEP_SA, *maxEP_SA;	// ��� ����� �� secondAxis

	// � ��������, ���������� ������, �������, �� �����. ��� ����� �� ���������� ����������� � ������ ������
	// � ����� ������ ����� �������, ������� ������.
	//memset(arRayObjIntersections, 0, sizeof(arRayObjIntersections));
	//memset(arRayObjIntersectionsDistances, 0, sizeof(arRayObjIntersectionsDistances));
	UINT intersCount = 0;
	Vector2 intersPoint;
	Vector2 intersNorm;

	// ����� ���� ������� ����� �� ��� firstAxis, ������� ������� � ����������� �� ����������� � �����
	if (dir)
	{
		// �������� ������/����

		// ����������� ��, ��� �����
		// TODO: ��� �� �������� �����, � �� ��������
		while((++current)->mValue < posFirstCoord);

		--current;

		// ���� �����������
		while( (++current)->mValue < limit && intersCount < maxRayIntersectionsCount )
		{
			object = boxes + current->GetOwner();

			if (current->IsMax())
			{
				anotherEP = firstAxis + object->mMin[fa];
				if (anotherEP->mValue <= posFirstCoord)
				{
					// ������ ����� �������� ������, ��� ����� ���������
					minEP_SA = secondAxis + object->mMin[sa]; 
					maxEP_SA = secondAxis + object->mMax[sa];

					ObjPhysic* obj = (ObjPhysic*)object->mObject;
					if( GetIntersection(r, *obj->geometry, obj->aabb.p, intersPoint, intersNorm ) &&
						ray->CheckIntersection(obj) )
					{
						arRayObjIntersections[intersCount] = obj;
						arRayObjIntersectionsPositions[intersCount] = intersPoint;
						arRayObjIntersectionsNorms[intersCount] = intersNorm;
						intersPoint -= r.p;
						arRayObjIntersectionsDistances[intersCount++] = intersPoint.Length();
					}
				}
			}
			else
			{
				// ����� �����/������� �������, ���������
				anotherEP	= firstAxis + object->mMax[fa];
				minEP_SA	= secondAxis + object->mMin[sa]; 
				maxEP_SA	= secondAxis + object->mMax[sa];

				ObjPhysic* obj = (ObjPhysic*)object->mObject;
				if( GetIntersection(r, *obj->geometry, obj->aabb.p, intersPoint, intersNorm ) && 
					ray->CheckIntersection(obj) )
				{
					arRayObjIntersections[intersCount] = obj;
					arRayObjIntersectionsPositions[intersCount] = intersPoint;
					arRayObjIntersectionsNorms[intersCount] = intersNorm;
					intersPoint -= r.p;
					arRayObjIntersectionsDistances[intersCount++] = intersPoint.Length();
				}
			}
		}
	}
	else
	{
		// �������� �����/����

		// ����������� ��, ��� ������
		// TODO: ��� �� �������� �����, � �� ��������
		while((--current)->mValue > posFirstCoord);

		++current;

		// ���� �����������
		while( (--current)->mValue > limit && intersCount < maxRayIntersectionsCount)
		{
			object = boxes + current->GetOwner();

			if (current->IsMax())
			{
				// ����� ������ �������, ���������
				anotherEP	= firstAxis + object->mMin[fa];
				minEP_SA	= secondAxis + object->mMin[sa]; 
				maxEP_SA	= secondAxis + object->mMax[sa];

				ObjPhysic* obj = (ObjPhysic*)object->mObject;
				if( GetIntersection(r, *obj->geometry, obj->aabb.p, intersPoint, intersNorm ) && 
					ray->CheckIntersection(obj) )
				{
					arRayObjIntersections[intersCount] = obj;
					arRayObjIntersectionsPositions[intersCount] = intersPoint;
					arRayObjIntersectionsNorms[intersCount] = intersNorm;
					intersPoint -= r.p;
					arRayObjIntersectionsDistances[intersCount++] = intersPoint.Length();
				}
			}
			else
			{
				anotherEP = firstAxis + object->mMax[fa];
				if (anotherEP->mValue >= posFirstCoord)
				{
					// ������ ����� �������� ������, ��� ����� ���������
					minEP_SA = secondAxis + object->mMin[sa]; 
					maxEP_SA = secondAxis + object->mMax[sa];

					ObjPhysic* obj = (ObjPhysic*)object->mObject;
					if( GetIntersection(r, *obj->geometry, obj->aabb.p, intersPoint, intersNorm ) && 
						ray->CheckIntersection(obj) )
					{
						arRayObjIntersections[intersCount] = obj;
						arRayObjIntersectionsPositions[intersCount] = intersPoint;
						arRayObjIntersectionsNorms[intersCount] = intersNorm;
						intersPoint -= r.p;
						arRayObjIntersectionsDistances[intersCount++] = intersPoint.Length();
					}
				}
			}
		}
	}

	return intersCount;
}

// � ���� ������� ���� ����� ����������� ������� ���� �� ����� ������� � SAP
void FindRayAABBintersection(ObjRay* ray)
{
	// TODO: ������������ ��� ������� ����������� �����, ������
	ray->debug_color = RGBAf(1.0f, 1.0f, 1.0f, 1.0f);

	// ��������, �� ����� ����� ��� ����� ����������.
	if (ray->activity == oatDying)
		return;

	if(asap->GetNbUsedBoxes() == 0)
		return;

	CRay const& r = ray->getRay();
	bool isVert = r.IsVertical();
	bool dir;
	if (isVert)
		dir = r.r.y > 0;
	else
		dir = r.r.x > 0;
	//bool isHor = r.IsHorizontal();

	assert(asap);	






	// ���������� ������ ��� (��, �� ������� ����� ���������) � ������
	// ��� ������������� ���� ����� ��������� �� ��� Y
	AxisIndex fa = isVert ? Y_ : X_;
	AxisIndex sa = isVert ? X_ : Y_;

	float posFirstCoord = isVert ? r.p.y : r.p.x;		// ������ ���� �� ������ ���

	float limit;		// ��������� ����� �� ��� firstAxis, �� ������� ����� ������� �����

	if (ray->searchDistance == 0)
	{
		// ��� ����������� �� ��������� - ����� �� ����� SAP
		limit = dir ? (MAX_FLOAT - 100) : (MIN_FLOAT + 100);
	}
	else
	{
		// ���� ����������� �� ���������
		if (isVert)
		{
			// ��� ������ ����������, ������� �����
			if (dir)
				limit = posFirstCoord + ray->searchDistance;
			else
				limit = posFirstCoord - ray->searchDistance;


		}
		else
		{
			// TODO: ��������� ray->searchDistance / sqrt(r.k*r.k + 1) - �������� �� ���
			// ����� ����� ������ ��� ��������� k - ���� ������� ��� ���������� ������
			if (dir)
				limit = posFirstCoord + ray->searchDistance / sqrt(r.r.y/r.r.x * r.r.y/r.r.x + 1);
			else
				limit = posFirstCoord - ray->searchDistance / sqrt(r.r.y/r.r.x * r.r.y/r.r.x + 1);
		}
	}
	
	// TODO: �������� ����� ��� �������������� � ������������ ����� ������������ 
	// ���������� ����� ��� ���������� (������ ��� � rev. 1198). 

	UINT intersCount = GetRayIntersections(dir, posFirstCoord, limit, fa, sa, ray);

	if (intersCount > 0)
	{
		// ������� ���������, ������ �� ��������� ������ ����, �������� ��������������� ������� � 
		// ���������� ��, ����� � ���������� ������� ������ ������ � �������
		IceCore::RadixSort r;
		IceCore::RadixSort* RS = &r;
		const UINT* sorted = RS->Sort(arRayObjIntersectionsDistances, intersCount).GetRanks();
		
		const UINT* const lastSorted = &sorted[intersCount];
		UINT index;
		// ������� �������
		while(sorted < lastSorted)
		{
			index =  *sorted++;
			if (SolveRay(ray, arRayObjIntersections[index], arRayObjIntersectionsPositions[index], arRayObjIntersectionsNorms[index]))
				break;
		}
	}
}

// ��� ������������ ��� ���� ��� ������ �� �����������
void ProcessRayCollision()
{
    extern std::map<int,ObjRay*>* RayTree;
    if (RayTree != NULL)
    for(std::map<int,ObjRay*>::iterator rayIterator = RayTree->begin(); rayIterator != RayTree->end(); rayIterator++ )
    {
        ObjRay* ray = rayIterator->second;
        assert(ray);
        FindRayAABBintersection(ray);
    }
}

// ���������� true, ���� ������ ��� �� � ��� �� ������������.
bool SolveRay(ObjRay* ray, ObjPhysic* obj, Vector2& pos, Vector2& norm)
{
	if ( !SolidTo(obj, ray->type) )
		return false;
	
	if ( (ray->parentConnection && ray->parentConnection->getParent() == obj)
		|| (obj->parentConnection && obj->parentConnection->getParent() == ray) )
		return false;
	
	// TODO: ��������� �������� ��� ������� ����������� �����
	ray->debug_color = RGBAf(1.0f, 0.2f, 0.2f, 1.0f);
	
	if (ray->Hit(obj, pos, norm))
	{
		ray->debug_color = RGBAf(0.2f, 1.0f, 0.2f, 1.0f);
	}
	
	return ray->activity == oatDying;
}

//////////////////////////////////////////////////////////////////////////

bool TouchCondition( ObjDynamic* toucher, GameObject* obj )
{
	if ( obj->IsSleep() || toucher->IsSleep() || !toucher->sprite->IsVisible() ) return false;
	switch (obj->touch_detection)
	{
		case tdtFromEverywhere:
			{
				if (toucher->aabb.p.x - toucher->vel.x > obj->aabb.Right() || toucher->aabb.p.x - toucher->vel.x < obj->aabb.Left())
					return true;
				if (toucher->aabb.p.y - toucher->vel.y > obj->aabb.Bottom() || toucher->aabb.p.y - toucher->vel.y < obj->aabb.Top())
					return true;
				return false;
			}
		case tdtFromTop:
			{
				if (toucher->vel.y > 0 && toucher->aabb.p.y - toucher->vel.y < obj->aabb.Top() )
					return true;
				return false;
			}
		case tdtFromBottom:
			{
				if (toucher->vel.y < 0 && toucher->aabb.p.y - toucher->vel.y > obj->aabb.Bottom() )
					return true;
				return false;
			}
		case tdtFromSides:
			{
				if (toucher->aabb.p.x - toucher->vel.x > obj->aabb.Right() || toucher->aabb.p.x - toucher->vel.x < obj->aabb.Left())
					return true;
				return false;
			}
		case tdtTopAndSides:
			{
				if (toucher->aabb.p.y - toucher->vel.y > obj->aabb.Bottom())
					return false;
				return true;
			}
		default:
			return true;
	}
}

void SolveCollision(ObjPhysic* obj1, ObjPhysic* obj2)
{
	assert(obj1);
	assert(obj2);
#ifdef SELECTIVE_RENDERING
	/// Object of this class is used to memorize the aabbs of passed objects and
	/// update them in render, if they was moved.
	class ObjMovedChecker 
	{
		ObjPhysic const *o1;
		ObjPhysic const *o2;
		const CAABB b1, b2;
		ObjMovedChecker& operator=(ObjMovedChecker const&) { return *this; }
	public:
		ObjMovedChecker(ObjPhysic const* o1, ObjPhysic const* o2) : o1(o1), o2(o2), b1(o1->aabb), b2(o2->aabb) {}
		~ObjMovedChecker()
		{
			if (b1 != o1->aabb) selectForRenederUpdate(o1);
			if (b2 != o2->aabb) selectForRenederUpdate(o2);
		}
	};

	// Destructor will be called on function end, making all the work.
	ObjMovedChecker(obj1, obj2);

#endif

#ifdef MAP_EDITOR
if ( editor_state == editor::EDITOR_OFF )
#endif // MAP_EDITOR
{
	float depth = 0.0f;
	Vector2 n = Vector2();
	Vector2 v1 = Vector2();
	Vector2 v2 = Vector2();
	if ( obj1->IsDynamic() )
		v1 = ((ObjDynamic*)obj1)->vel + ((ObjDynamic*)obj1)->own_vel;
	if ( obj2->IsDynamic() )
		v2 = ((ObjDynamic*)obj2)->vel + ((ObjDynamic*)obj2)->own_vel;

	if ( obj1->geometry == &obj1->rectangle && obj2->geometry == &obj2->rectangle )
	{
		if ( !CAABBCollide( obj1->aabb, obj2->aabb, n, depth ) )
		{
			return;
		}
	}
	else
	{
		if ( !CPolygon::Collide( *obj1->geometry, obj1->aabb.p, v1, Matrix2(0.0f), *obj2->geometry, obj2->aabb.p, v2, Matrix2(0.0f), n, depth ) )
		{
			return;
		}
	}
	if ( fabs(depth) <= 0.01f )
		return;	//Probably just touching anyway
	if ( !SolidTo(obj1, obj2->type) || !SolidTo(obj2, obj1->type) )
	{
		asap->RemovePair(obj1, obj2, 
			obj1->id, obj2->id);
		return;
	}

#ifdef DEBUG_DRAW_COLLISION_PAIRS
	collPairs.push_back(obj1->aabb.p);
	collPairs.push_back(obj2->aabb.p);
#endif // DEBUG_DRAW_COLLISION_PAIRS

	if ( (obj1->parentConnection && obj1->parentConnection->getParent() == obj2)
		|| (obj2->parentConnection && obj2->parentConnection->getParent() == obj1) )
	{
		asap->RemovePair(obj1, obj2,
			obj1->id, obj2->id);
	}
	else if (obj1->IsDynamic() && obj2->IsDynamic())
	{
		if (((ObjDynamic*)obj1)->IsBullet() )
		{
			SolveBullet((ObjBullet*)obj1, obj2, n, depth);
			return;
		}

		if (((ObjDynamic*)obj2)->IsBullet() )
		{
			SolveBullet((ObjBullet*)obj2, obj1, n, depth);
			return;
		}

		if ( obj1->type == objPlayer && obj2->IsSolid() )
			SolveDynamicVSDynamic((ObjDynamic*)obj1, (ObjDynamic*)obj2, n, depth);
		else if ( obj1->IsSolid() && obj2->type == objPlayer )
			SolveDynamicVSDynamic((ObjDynamic*)obj1, (ObjDynamic*)obj2, n, depth);
		else if ( obj1->IsSolid() || obj2->IsSolid() )
			SolveDynamicVSDynamic((ObjDynamic*)obj1, (ObjDynamic*)obj2, n, depth);

		if ( obj1->type == objPlayer && obj2->IsTouchable() && TouchCondition((ObjDynamic*)obj1, obj2) ) //��� ����� �� �������� ��������. ����.
		{
			switch ( obj2->type )
			{
				case objItem:
					((ObjItem*)obj2)->Touch( (ObjDynamic*)obj1 );
					return;
				case objEnemy:
					((ObjEnemy*)obj2)->Touch( (ObjDynamic*)obj1 );
					return;
				default:
					obj2->Touch( (ObjDynamic*)obj1 );
					return;
			}
		}
		if ( obj2->type == objPlayer && obj1->IsTouchable() && TouchCondition((ObjDynamic*)obj2, obj1) ) //��� ����� �� �������� ��������. ����.
		{
			switch ( obj1->type )
			{
				case objItem:
					((ObjItem*)obj1)->Touch( (ObjDynamic*)obj2 );
					return;
				case objEnemy:
					((ObjEnemy*)obj1)->Touch( (ObjDynamic*)obj2 );
					return;
				default:
					obj1->Touch( (ObjDynamic*)obj2 );
					return;
			}
		}
	}
	else if (obj1->IsDynamic() && !obj2->IsDynamic())
	{
		depth = -depth;	// n must be reversed
		CAABB old_aabb = obj1->aabb;
		if (((ObjDynamic*)obj1)->IsBullet() )
			SolveBullet((ObjBullet*)obj1, obj2, n, depth);
		else 
			SolveDynamicVSStatic((ObjDynamic*)obj1, obj2, n, depth);
		
		if (obj1->childrenConnection && obj1->aabb != old_aabb)
			obj1->childrenConnection->Event( ObjectEventInfo( eventCollisionPushed, obj1->aabb.p-old_aabb.p));
	}
	else if (!obj1->IsDynamic() && obj2->IsDynamic())
	{
		CAABB old_aabb = obj2->aabb;
		if (((ObjDynamic*)obj2)->IsBullet() )
			SolveBullet((ObjBullet*)obj2, obj1, n, depth);
		else 
			SolveDynamicVSStatic((ObjDynamic*)obj2, obj1, n, depth);
		
		if (obj2->childrenConnection && obj2->aabb != old_aabb)
			obj2->childrenConnection->Event( ObjectEventInfo( eventCollisionPushed, obj2->aabb.p-old_aabb.p));
	}
	else
	{
		asap->RemovePair(obj1, obj2, 
			obj1->id, obj2->id);
	}
}
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

//Check if one polygon is above the other. Needed for "one-sided" platforms.
bool PolygonOver( CPolygon* p1, Vector2 pos1, CPolygon* p2, Vector2 pos2 )
{
	UINT n1 = p1->GetVertexCount();
	UINT n2 = p2->GetVertexCount();
	bool* answer1 = new bool[ n1 ]; //Vertex is above at least one line containg "upper" side.
	bool* answer2 = new bool[ n1 ];	//Vertex is above at least one line containg "lower" side.
	// Yes, it's n1 both times. No, that's not an error.
	bool ret = true;
	memset( answer1, 0, n1 * sizeof( bool ) );
	memset( answer2, 0, n1 * sizeof( bool ) );
	double t;
	for ( UINT i = 0; i < n2; i++ )
	{
		for ( UINT j = 0; j < n1; j++ )
		{
			t = ( (pos1.x + (*p1)[j].x) - (pos2.x + (*p2)[i].x) ) / ((*p2)[(i+1) % n2].x - (*p2)[i].x);
			if ( ((*p2)[(i+1) % n2].x - (*p2)[i].x) > 0 )
				answer1[j] |= ( pos2.y + (*p2)[i].y ) + ((*p2)[(i+1) % n2].y - (*p2)[i].y) * t >= ( pos1.y + (*p1)[j].y );
			else
				answer2[j] |= ( pos2.y + (*p2)[i].y ) + ((*p2)[(i+1) % n2].y - (*p2)[i].y) * t >= ( pos1.y + (*p1)[j].y );
			//Notice we use j both times for answer1 and answer2 indeces.
			//It ranges from 0 to n1-1, not n2-1.
		}
	}
	for ( size_t i = 0; i < n1; i++ ) ret &= answer1[i] & answer2[i]; //This time we use i, but it only goes to n1-1
	DELETEARRAY( answer1 );
	DELETEARRAY( answer2 );
	//Everything is fine with array sizes. Wea are aware that n1 is used two times. Thanks, but no need to report it again.
	return ret;
}

//��������� ������������ ������ � ������ ������������� �������.
bool SolveDynamicVSStatic(ObjDynamic* objD, ObjPhysic* objS, Vector2 n, float depth)
{
	//�������� ������� �� ������������ � �������������� ����������� � �������� ������ ����� ���.
	if ( objS->IsOneSide() && (((objD->gravity.x == 0.0f || objD->IsBullet() ) && objD->gravity.y == 0.0f)) )
		return false;

	if ( objS->IsOneSide() )
	{
		//Players only want to drop one platform at a time. Bullets and enemies, hovewer, do not.
		//Also, if object is not a player than it's okay to drop down "forced" platforms if dropping bit is set.
		bool controlled_drop = !objD->IsDropping() || (objD->type == objPlayer  && ( objS != objD->drop_from || objS->IsForced() ));
		//We should be only bothered by one-side platform is we are moving down.
		bool going_down = (objD->vel.y >= 0.0f) && (!objD->IsDropping() || controlled_drop);
		//A broad fase of the "past collision detection". We roll back the speed to see if the object
		//reall comes from above. But before bothering with polygons we check AABBs - it's simpler
		//and rectangular platforms are far more plenty anyway.
		bool broad = objD->aabb.p.y - objD->vel.y + objD->aabb.H - 1.5f <= objS->aabb.p.y - objS->aabb.H;
		if ( n.y < 0 && going_down && ( broad || PolygonOver( objD->geometry, objD->aabb.p - objD->vel, objS->geometry, objS->aabb.p ) ) )
		{
			objS->PutOn( objD );
			objD->suspected_plane = objS->foundation_for;
			objD->Bounce( n, objS );
		}
		return true;
	}

	if ( objS->IsOneSide() && objD->IsDropping() && objS->IsForced() && objD->type == objPlayer)
	{
		//��� ����� ���, ����� �� ���������� ����������� �����.
		((ObjCharacter*)objD)->movement = omtDropSitting;
	}

	//���� ����� ��� ���������
	if ( objS->type == objEnvironment )
	{
		SolveEnvironment((ObjEnvironment*)objS, objD);
		return true;
	}

	if (!objS->IsSolid())
		return false;

	if (objD->ghostlike)
		return false;

	if ( n.y > 0 && objD->gravity.y > 0 && objD->IsOnPlane() && ( (objD->suspected_plane == NULL) || objS != objD->GetSuspectedPlane()) )
	{
		objD->SetPushedUp(); 
		objD->SetPushedDown(); //������� ����� �������� ���� � ����������� ���������, ��������.
		//������ ��� ��� ����� ���������� "����������" � ��������� �������, ���� �� ���� ����������.
		n.x = n.x < 0.0f ? -1.0f : 1.0f;
		n.y = 0.0f;
		PushOut( objD, depth, n );
		return true;
	}

	PushOut( objD, depth, n );
	if ( depth < 0 )
		objD->Bounce( -n, objS );
	else
		objD->Bounce( n, objS );

	if ( n.y*depth < 0.0f )
	{
		objD->SetPushedUp();
	}
	if ( n.y*depth > 0.0f )
	{
		objD->SetPushedDown();
	}
	if ( n.x*depth > 0.0f )
	{
		objD->SetPushedRight();
	}
	if ( n.x*depth < 0.0f )
	{
		objD->SetPushedLeft();
	}
	return true;
}

//��������� ������������ ������ � ������ ������������� �������.
//Returns true if any actual movement occured
bool SolveDynamicVSDynamic(ObjDynamic* objD1, ObjDynamic* objD2, Vector2 n, float depth)
{
//	if (!objD1->IsSolid() || !objD2->IsSolid())
//		return;

	if (objD1->ghostlike || objD2->ghostlike)
		return false;

	if (objD1->mass == 0.0f && objD2->mass == 0.0f)
		return false;

	if (objD1->mass == 0.0f && objD2->mass != 0.0f)
		return SolveDynamicVSStatic( objD1, objD2, n, -depth);

	if (objD1->mass != 0.0f && objD2->mass == 0.0f)
		return SolveDynamicVSStatic( objD2, objD1, n, depth);

	if (objD1->mass > 0.0f && objD2->mass < 0.0f)
		return SolveDynamicVSStatic( objD1, objD2, n, -depth);

	if (objD1->mass < 0.0f && objD2->mass > 0.0f)
		return SolveDynamicVSStatic( objD2, objD1, n, depth);

	Vector2 gravity;

	gravity = objD2->GetGravity();
	if ( objD1->IsOnPlane() && ( n * gravity * depth < 0.0f ) )	//If we are going against gravity
	{
		objD2->PutOn( objD1 );
		objD2->suspected_plane = objD1->foundation_for;
		PushOut( objD2, depth, n );
		objD2->SetOnPlane();
		if ( depth < 0 )
			objD2->Bounce( Vector2( -n.x, -n.y ), objD1 );
		else
			objD2->Bounce( n, objD1 );
		//if (n.x * gravity.x < 0.0f) objD->vel.x = 0.0f;
		//if (n.y * depth * gravity.y <= 0.0f) objD2->vel.y = objD1->vel.y;
		return true;
	}

	gravity = objD1->GetGravity();
	if ( objD2->IsOnPlane() && ( -n * gravity * depth < 0.0f ) )	//If we are going against gravity
	{
		objD2->PutOn( objD1 );
		objD1->suspected_plane = objD2->foundation_for;
		PushOut( objD1, -depth, n );
		objD1->SetOnPlane();
		if ( depth < 0 )
			objD1->Bounce( n, objD2 );
		else
			objD1->Bounce( Vector2( -n.x, -n.y ), objD2 );
		//if (n.x * gravity.x < 0.0f) objD->vel.x = 0.0f;
		//if (-n.y * depth * gravity.y <= 0.0f) objD1->vel.y = objD2->vel.y;
		return true;
	}

	float m1 = std::abs(objD1->mass);
	float m2 = std::abs(objD2->mass);
	assert(m1 > 0);
	assert(m2 > 0);
	float depth1 = depth * m2 / (m1 + m2);
	float depth2 = depth * m1 / (m1 + m2);
	Vector2 np = n.GetPerpendicular();

	float v1 = objD1->vel * n;
	float v2 = objD2->vel * n;

	float vp1 = objD1->vel * np;
	float vp2 = objD2->vel * np;

	float vmid = (v1 * m1 + v2 * m2) / (m1 + m2);
	float dv1 = v1 - vmid;
	float dv2 = v2 - vmid;

	float v1res = vmid - dv1 * objD1->bounce;
	float v2res = vmid - dv2 * objD2->bounce;

	objD1->vel = v1res * n + vp1 * np;
	objD2->vel = v2res * n + vp2 * np;

	PushOut( objD1, -depth1, n );
	PushOut( objD2, depth2, n );

	if ( n.y*depth < 0.0f )
	{
		objD2->SetPushedUp();
		objD1->SetPushedDown();
	}
	if ( n.y*depth > 0.0f )
	{
		objD2->SetPushedDown();
		objD1->SetPushedUp();
	}
	if ( n.x*depth > 0.0f )
	{
		objD2->SetPushedRight();
		objD1->SetPushedLeft();
	}
	if ( n.x*depth < 0.0f )
	{
		objD2->SetPushedLeft();
		objD1->SetPushedRight();
	}

/*
	if ( depth < 0 )
		objD2->Bounce( Vector2( -n.x, -n.y ), objD1 );
	else
		objD2->Bounce( n, objD1 );

	if ( depth < 0 )
		objD1->Bounce( n, objD2 );
	else
		objD1->Bounce( Vector2( -n.x, -n.y ), objD2 );


	float mbounce = -bounce;
	if ( obj->material ) mbounce -= obj->material->bounce_bonus;

	if ( gravity * n < -gravity.Length() * 0.5f )
	{
		SetOnPlane();
		obj->PutOn( this );
		suspected_plane = obj->foundation_for;
	
		float frict = 0.75f;
		if ( obj->material )
		{
			if ( obj->material->friction <= 1.0f )
				frict = 0.75f + (1.0f - obj->material->friction) * 0.25f;
			else
				frict *= 0.75f / obj->material->friction;
		}
		vel_p *= frict;
		own_vel_p *= frict;
	}

	if ( gravity * n < -gravity.Length() * 0.5f )
	{
		SetOnPlane();
		obj->PutOn( this );
		suspected_plane = obj->foundation_for;
	
		float frict = 0.75f;
		if ( obj->material )
		{
			if ( obj->material->friction <= 1.0f )
				frict = 0.75f + (1.0f - obj->material->friction) * 0.25f;
			else
				frict *= 0.75f / obj->material->friction;
		}
		vel_p *= frict;
		own_vel_p *= frict;
	}


	gravity = objD2->GetGravity();
	if ( objD1->IsSolid() && objD1->GetGravity().Length() == 0 && ( n.x * depth * gravity.x < 0.0f || n.y * depth * gravity.y < 0.0f ) )	//If we are going against gravity
	{
		objD1->PutOn( objD2 );
		objD2->suspected_plane = objD1->foundation_for;
		gravity = gravity.Normalized();
		float gn = gravity.x * n.x + gravity.y * n.y;
		PushOut( objD1, -depth1 * gn, gravity );
		PushOut( objD2, depth2 * gn, gravity );
		//objD1->vel += -depth1 * gn * gravity;
		//objD2->vel +=  depth2 * gn * gravity;
		objD2->SetOnPlane();
		if ( depth < 0 )
			objD2->Bounce( Vector2( -n.x, -n.y ), objD1 );
		else
			objD2->Bounce( n, objD1 );
		return true;
	}


	gravity = objD1->GetGravity();
	if ( objD2->IsSolid() && objD2->GetGravity().Length() == 0 && ( -n.x * depth * gravity.x < 0.0f || -n.y * depth * gravity.y < 0.0f ) )	//If we are going against gravity
	{
		objD2->PutOn( objD1 );
		objD1->suspected_plane = objD2->foundation_for;
		gravity = gravity.Normalized();
		float gn = gravity.x * n.x + gravity.y * n.y;
		PushOut( objD1, -depth1 * gn, gravity );
		PushOut( objD2, depth2 * gn, gravity );
		//objD1->vel += -depth1 * gn * gravity;
		//objD2->vel +=  depth2 * gn * gravity;
		objD1->SetOnPlane();
		if ( depth < 0 )
			objD1->Bounce( n, objD2 );
		else
			objD1->Bounce( Vector2( -n.x, -n.y ), objD2 );
		return true;
	}

	objD1->vel += -depth1 * n;
	objD2->vel +=  depth2 * n;
	PushOut( objD1, -depth1, n );
	PushOut( objD2, depth2, n );
*/
	return true;
}

void SolveBullet(ObjBullet* bul, ObjPhysic* obj, Vector2 n, float depth)
{
	bool ignore_collision = !bul->Hit(obj);
	//���� ���� ������������ ��� ���������, �� ��� ��� ������ ���� �������. �� �� ������, ���� ��� - ����� ������ ������������.
	if ( ignore_collision || (bul->activity != oatDying && SolidTo( obj, objBullet ) && obj->IsSolid()) ) 
	{
		bul->Bounce();
		if( obj->IsDynamic() )
		{
			if ( SolveDynamicVSStatic( bul, obj, n, depth ) )
				bul->Miss();
		}
		else
			SolveDynamicVSStatic( bul, obj, n, depth );
	}
}

void SolveEnvironment(ObjEnvironment* env, ObjDynamic* obj)
{
	if(env == obj->env && env != default_environment)
	{
		//obj->env = env;
		env->OnStay(obj);
	}
	else
	{
		// ����������� ��������� ����������� � ����� ����������
		delayed_env_coll.push_back(env);
		delayed_env_coll.push_back(obj);
	}
}

// ���������� ��������� ����������� � �����������. ������������ ����� ����, ��� 
// � ������� ����������� ��� ��������� ���������, ��������� ������� �������������.
void SolveDelayedEnvCollision()
{
	if (delayed_env_coll.empty())
		return;
	
	ObjDynamic* obj = NULL;
	ObjEnvironment* env = NULL;
	list<GameObject*>::iterator it = delayed_env_coll.begin();
	
	while(it != delayed_env_coll.end())
	{
		env = (ObjEnvironment*)*it;
		it++;
		obj = (ObjDynamic*)*it;
		it++;
		
		if (CollideAABB(env->aabb, obj->aabb))
		{
			// ������ ������������� ����� � ����� ���������.
			if ( obj->env->id < env->id )
			{
				obj->SetEnv(env);
			}
		}
	}
	
	delayed_env_coll.clear();
}


// ��������� ������, ��� SolveEnvironment � SolveDelayedEnvCollision
void SolveEnvLeave(ObjEnvironment* env, ObjDynamic* obj)
{
	if (env == obj->env)
	{
		obj->ResetEnv();
	}
}

//////////////////////////////////////////////////////////////////////////

// ������ ���������� �� MTD MTD = Minimal Translation Distance
// ��� �� ������� void PhysDynamicBody::SatanaResolveCollisionMTD(const ASAP_AABB &bb)
void ResolveCollisionMTD(ObjDynamic* dyn, const CAABB &bb)
{
	float deltaX, deltaY;
	SegIntersect(	Vector2(dyn->aabb.p.x - dyn->aabb.W, dyn->aabb.p.x + dyn->aabb.W),
		Vector2(bb.p.x - bb.W, bb.p.x + bb.W), deltaX	);
	SegIntersect(	Vector2(dyn->aabb.p.y - dyn->aabb.H, dyn->aabb.p.y + dyn->aabb.H),
		Vector2(bb.p.y - bb.H, bb.p.y + bb.H), deltaY	);
	if (fabs(deltaX) < fabs(deltaY))
	{
		if ( deltaX > 0 )
			dyn->SetPushedRight();
		else
			dyn->SetPushedLeft();
		dyn->aabb.p.x += deltaX;
		dyn->vel.x = 0.0f;
	}
	else
	{
		if ( deltaY > 0 )
			dyn->SetPushedDown();
		else
			dyn->SetPushedUp();
		dyn->aabb.p.y += deltaY;
		dyn->vel.y = 0.0f;
	}
}

// ��� ����� �� ��������
//�������: �� ����� ���� ���� ��� ��� �� �����������
bool ResolveCollision( int sX, int sY, ObjDynamic* dyn, const CAABB &bb )
{
	bool mark_as_suspected = false;
	if (sX == SEG_PROJ_STATE_INTERSECT)
	{
		if (sY == SEG_PROJ_STATE_BEFORE)
		{
			dyn->aabb.p.y = bb.p.y - bb.H - dyn->aabb.H - 1;
			dyn->SetOnPlane();
			dyn->drop_from = NULL;
			mark_as_suspected = true;
		}
		if (sY == SEG_PROJ_STATE_AFTER)
		{
			dyn->aabb.p.y = bb.p.y + bb.H + dyn->aabb.H + 1;
			dyn->ClearOnPlane();
		}
	}
	
	if (sY == SEG_PROJ_STATE_INTERSECT)
	{
		if (sX == SEG_PROJ_STATE_BEFORE)
		{
			dyn->aabb.p.x = bb.p.x - bb.W - dyn->aabb.W - 1;
			dyn->SetPushedLeft();
		}
		if (sX == SEG_PROJ_STATE_AFTER)
		{
			dyn->aabb.p.x = bb.p.x + bb.W + dyn->aabb.W + 1;
			dyn->SetPushedRight();
		}
	}
	
	if (sY == SEG_PROJ_STATE_BEFORE)
	{
		if (sX == SEG_PROJ_STATE_BEFORE )
		{
			dyn->aabb.p.x = bb.p.x - bb.W - dyn->aabb.W - 1;
			dyn->aabb.p.y = bb.p.y - bb.H - dyn->aabb.H - 1;
			dyn->SetPushedLeft();
			dyn->SetPushedUp();
		}
		if (sX == SEG_PROJ_STATE_AFTER )
		{
			dyn->aabb.p.x = bb.p.x + bb.W + dyn->aabb.W + 1;
			dyn->aabb.p.y = bb.p.y - bb.H - dyn->aabb.H - 1;
			dyn->SetPushedRight();
			dyn->SetPushedUp();
		}
	}
	
	if (sY == SEG_PROJ_STATE_AFTER)
	{
		if (sX == SEG_PROJ_STATE_BEFORE )
		{
			dyn->aabb.p.y = bb.p.y + bb.H + dyn->aabb.H + 1;
			dyn->SetPushedDown();
		}
		if (sX == SEG_PROJ_STATE_AFTER )
		{
			dyn->aabb.p.y = bb.p.y + bb.H + dyn->aabb.H + 1;
			dyn->SetPushedDown();
		}
	}
	return mark_as_suspected;
}

bool RestsOn( ObjDynamic* objD, ObjPhysic* objS )
{
	Vector2 n;
	float depth = 0.0f;
	
	if ( objD == NULL || objS == NULL )
		return false;
	
	if ( objD->IsDropping() && objS->IsOneSide() && objD->drop_from == objS )
		return false;
	
	if ( !SolidTo( objD, objS->type ) || !SolidTo( objS, objD->type ) )
		return false;
	
	if ( objD->geometry == &objD->rectangle && objS->geometry == &objS->rectangle )
	{
		if ( CAABBCollide( objD->aabb, objS->aabb, n, depth ) )
		{
			//If we are already colliding then we should leave it to collision solver.
			if ( fabs(depth) > 0.01f ) return false; //Touching actually registers as a collision. Ouch.
		}
	}
	else
	{
		if ( CPolygon::Collide( *objD->geometry, objD->aabb.p, Vector2(), Matrix2(0.0f), 
					*objS->geometry, objS->aabb.p, Vector2(), Matrix2(0.0f), 
					n, depth ) )
		{
			//If we are already colliding then we should leave it to collision solver.
			if ( fabs(depth) > 0.01f ) return false; //Touching actually registers as a collision. Ouch.
		}
	}
	
	Vector2 gravity = objD->GetGravity();
	
	if ( gravity.Length() > 0.0f ) gravity.Normalize();
	
	depth = 0.0f;
	if ( objD->geometry == &objD->rectangle && objS->geometry == &objS->rectangle )
	{
		CAABB aabb_gravity(objD->aabb);
		aabb_gravity.p += gravity;
		if ( CAABBCollide( aabb_gravity, objS->aabb, n, depth ) )
		{
			return true;
		}
	}
	else
	{
		if ( CPolygon::Collide( *objD->geometry, objD->aabb.p+gravity, Vector2(), Matrix2(0.0f), 
					*objS->geometry,         objS->aabb.p, Vector2(), Matrix2(0.0f), 
					n, depth ) )
		{
			return true;
		}
	}
	
	return false;
}


// �������� ������, �� ������� ������������� ����.
using namespace Opcode;

ObjPhysic* GetShadowPlane(ObjDynamic* od)
{
	assert(asap);

	const ASAP_Box* boxes = asap->GetBoxes();
	const ASAP_EndPoint* epY = asap->GetEndPoints(Y_);
	assert(boxes);
	assert(epY);

	const ASAP_Box* object = boxes + od->sap_handle;
	assert(object->HasBeenInserted());

	const Opcode::ASAP_EndPoint* current = epY + (boxes + od->sap_handle)->mMin[Y_];
	const ValType limit = current->mValue + SHADOW_DISTANCE;

	const ASAP_AABB& asab_aabb = GetASAP_AABB(od->aabb);

	ObjPhysic* obj = NULL;

	while ((++current)->mValue <= limit)
	{
		assert(!current->IsSentinel());
		obj = (ObjPhysic*)(boxes[current->GetOwner()].mObject);
		if( obj->aabb.Bottom() > od->aabb.Bottom() )
		{
			if( obj->IsSolid() && obj->slopeType == 0 )
			{
				const ASAP_Box* id1 = boxes + current->GetOwner();
				// Our max passed a min => start overlap
				if ((object != id1 &&
#ifdef KERNEL_BUG_ASAP_2D
					Intersect1D(*object, *id1, X_) &&
					Intersect1D_Min(asab_aabb, *id1, epY, Y_))
#else
					Intersect2D(*object, *id1, X_, Z_) &&
					Intersect1D_Min(asab_aabb, *id1, epY, Y_))
#endif
					)
				{
					if( PolygonOver(od->geometry, od->aabb.p, obj->geometry, obj->aabb.p) )
						return obj;
				}
			}
		}
	}

	return NULL;
}

bool GetIntersection(CRay const& ray, CPolygon& poly, Vector2& polypos, Vector2& res, Vector2& norm)
{
	CPolygon box;
	CPolygon::MakeBoxFrom(box);
	Vector2 n;
	float d = 0.0f;
	if( CPolygon::Collide( poly, polypos, Vector2(0.0f, 0.0f), Matrix2(0.0f), box, ray.p, Vector2(0.0f, 0.0f), Matrix2(0.0f), n, d ) )
	{
		res = ray.p;
		norm = ray.r;
		return true;
	}

	Vector2 restmp = ray.p;
	Vector2 normtmp = ray.r;
	float lmin = -1;
	for (unsigned j = poly.GetVertexCount() - 1, i = 0; i < poly.GetVertexCount(); j = i, i++)
	{
		Vector2 v1 = poly[j] + polypos;
		Vector2 v2 = poly[i] + polypos;
		
		if( ray.GetIntersectionInterval(v1,v2,restmp,normtmp) )
		{
			float l = (restmp - ray.p) * ray.r;
			if ( l >= 0 && ( lmin == -1 || l < lmin ) )
			{
				lmin = l;
				res = restmp;
				norm = normtmp;
			}
		}
	}
	if( lmin != -1 )
		return true;
	else
		return false;
}
