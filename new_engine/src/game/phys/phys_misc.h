#ifndef __PHYSMISC_H_
#define __PHYSMISC_H_

//
//
//		��������� ����� ��� ��� ������
//		������ ��
//
//

#include "2de_Vector2.h"

class CAABB
{
public:
	Vector2 p;
	float H;
	float W;

	float Left() const { return p.x - W; }
	float Right() const { return p.x + W; }
	float Top() const { return p.y - H; }
	float Bottom() const { return p.y + H; }

	float GetMin(UINT axis) const;
	float GetMax(UINT axis) const;

	__INLINE CAABB(void) : H(0.0f), W(0.0f) { }
	CAABB(const CAABB& a);
	CAABB(float x1, float y1, float x2, float y2);
	__INLINE bool IsEmpty ();
	void Intersect(const CAABB &a, const CAABB &b);
	bool PointInCAABB(float x, float y);
	bool operator == (const CAABB &A) const;
	bool operator != (const CAABB &A) const;

	/// Calculates bounding box of the current box rotated around specific point
	/// @param angle in radians
	/// @param center point of ratation
	void Rotate(float angle, Vector2 center);
	
	/// Calculates bounding box of the current box rotated around its center
	/// @param angle in radians
	void Rotate(float angle);
};

// ����� ����, ���������� �����������
class CRay
{
public:
	Vector2 p;	// ����� ������ ����

	Vector2 r;	// ������ ����������� ����
	//float k;	// ������� ���� ����� ����� � ���� ��. � ������, ����� ��� ����������,
				// ��������� �������� inf (+O�, ����) � -inf (-��, �����).

	//bool dir;	// ����������� ����. true - ������, false - �����. ��� �������������
				// ���� true - ����, false - �����.

	__INLINE CRay() : r(1.0f, 0.0f) {}
	CRay(const CRay& r);
	CRay(float x0, float y0, float x1, float y1);
	bool IsVertical() const;
	bool IsHorizontal() const;
	// ���� ����������� ���� � ��������������. 
	bool IsIntersecting(float left, float right, float top, float bottom) const;
	// ���� ����������� ���� � ��������������
	bool IsIntersecting(const CAABB& aabb) const;
	bool GetIntersectionPoint(CAABB const& aabb, Vector2& p) const;
	bool GetIntersectionPoint(float left, float top, float right, float bottom, Vector2& c) const;
	// ���� ����������� ���� � �������, ������ ���������� ����� ����������� ����� x � y
	bool GetIntersectionPoint(float x1, float y1, float x2, float y2, float& x, float& y) const;
	bool GetIntersectionInterval(Vector2& v1, Vector2& v2, Vector2& res, Vector2& norm) const;

	bool operator==(CRay const& r) const;
	bool operator!=(CRay const& r) const;
//private:
	// ���� ������������ ���� � ��������������� �������
	bool IsHorLineInters(float x1, float x2, float y) const;
	// ���� ����������� ���� � ������������� �������
	bool IsVertLineInters(float y1, float y2, float x) const;
	bool GetHorLineInters(float x1, float x2, float y, Vector2& c) const;
	// ���� ����������� ���� � ������������� �������
	bool GetVertLineInters(float y1, float y2, float x, Vector2& c) const;
};

/**
*	Collide - ������������ �� 2 �����.
*	TODO: ������� ����� ��������\��������� � ����� ������� �-��.
*	������ ��� ����� ���� ��� ����� ��������. Vector2 min, Vector2 max
*	� �� ����� �������.
*/

__INLINE bool CollideAABB(const CAABB &a, const CAABB &b)
{
	if (a.p.x + a.W < b.p.x - b.W || a.p.x - a.W > b.p.x + b.W)
		return false;
	if (a.p.y + a.H < b.p.y - b.H || a.p.y - a.H > b.p.y + b.H)
		return false;
	return true;
}

/**
*	������� ��� ����� ����������� �������� �������� ������.
*	������: ��� ����������� - ��� ���� ����� ������ ������ ����� ������ ��� �����, ���� ���� �����������, �� ���� � ��� ����.
*/

#define SEG_PROJ_STATE_BEFORE		0x01
#define SEG_PROJ_STATE_INTERSECT	0x02
#define SEG_PROJ_STATE_AFTER		0x03

/**
*	���������� ���������� ��� ����������� �������� �������� ������
*	������ �� �������� ������ ���������, � ������ ��� ����������� �������. �� �� �������� � �� ������ �������� ������.
*	a.x, a.y - 1� � 2� ���������� ������� �������. ������, �� ��� ��� � ������� � ������� x y ����� ����� � �����. ����� �� �������.
*/

__INLINE int SegProjIntersect(const Vector2 &a, const Vector2 &b)
{
	if (a.y <= b.x)
		return SEG_PROJ_STATE_BEFORE;
	if (a.x >= b.y)
		return SEG_PROJ_STATE_AFTER;
	return SEG_PROJ_STATE_INTERSECT;
}

/**
*	��� ������� �����. C - Corner.
*	TL - Top Left
*	BR - Bottom right
*	�� � �� ��������
*/

#define C_TL 0x00
#define C_TR 0x01
#define C_BL 0x02
#define C_BR 0x03

// ��������.

__INLINE void Sort2f(float &f1, float &f2)
{
	if (f1 > f2)
		std::swap(f1, f2);
}

/**
*	���������� ��� ����, �� ����� ������������ �������� ������.
*	sX - ��� �������� �� ��� �, ��� � ����������.
*/

__INLINE int DetectCorner(const int sX, const int sY)
{
	if (sY == SEG_PROJ_STATE_BEFORE)
	{
		if (sX == SEG_PROJ_STATE_BEFORE )
			return C_TL;
		if (sX == SEG_PROJ_STATE_AFTER )
			return C_TR;
	}

	if (sY == SEG_PROJ_STATE_AFTER)
	{
		if (sX == SEG_PROJ_STATE_BEFORE )
			return C_BL;
		if (sX == SEG_PROJ_STATE_AFTER )
			return C_BR;
	}
	assert(false);
	return SEG_PROJ_STATE_BEFORE;	// ����� ��������� ���-�� ����������, ���� ������ ��������.
}

/*
*	�-� CornerProblemSolve(...) ������ ������ �� �������� � ������. ��� ���������� ������� � �� �����.
*	������ ������ ������ ��������������, ������ ��� � ��� � ��� ��� ����������. ����� �� �������.
*/

__INLINE bool CornerProblemSolve(int &Sx, int &Sy, const CAABB &dyn, const CAABB &stat, const Vector2 &vel)
{
	float hy0 = 0.0f, hy1 = 0.0f;
	float vx0 = 0.0f, vx1 = 0.0f;
	Vector2 v0;
	Vector2 v1;
	Vector2 x0, x1, p;
	int corner = DetectCorner(Sx, Sy);

	switch(corner)
	{
	case C_TL:
		{
			v0.x = dyn.p.x + dyn.W;
			v0.y = dyn.p.y + dyn.H;
			hy0 = stat.p.y - stat.H;
			vx0 = stat.p.x - stat.W;
			hy1 = stat.p.y + stat.H;
			vx1 = stat.p.x + stat.W;
		}
		break;
	case C_TR:
		{
			v0.x = dyn.p.x - dyn.W;
			v0.y = dyn.p.y + dyn.H;
			hy0 = stat.p.y - stat.H;
			vx0 = stat.p.x + stat.W;
			hy1 = stat.p.y + stat.H;
			vx1 = stat.p.x - stat.W;
		}
		break;
	case C_BL:
		{
			v0.x = dyn.p.x + dyn.W;
			v0.y = dyn.p.y - dyn.H;
			hy0 = stat.p.y + stat.H;
			vx0 = stat.p.x - stat.W;
			hy1 = stat.p.y - stat.H;
			vx1 = stat.p.x + stat.W;
		}
		break;
	case C_BR:
		{
			v0.x = dyn.p.x - dyn.W;
			v0.y = dyn.p.y - dyn.H;
			hy0 = stat.p.y + stat.H;
			vx0 = stat.p.x + stat.W;
			hy1 = stat.p.y - stat.H;
			vx1 = stat.p.x - stat.W;
		}
		break;
	}
	p = Vector2(vx0, hy0);
	v0 -= p;
	v1 = v0 + vel;
	float k = (v1.y - v0.y)/(v1.x - v0.x);
	float b = v1.x*k - v1.y;
	x0.x = -b/k;
	x0.y = 0.0f;
	x1.x = 0.0f;
	x1.y = b;
	x0 += p;
	x1 += p;

  	if (abs(x0.x - vx0) < 0.001f && abs(x1.y - hy0) < 0.001f)
	{
		return false;
	}
	Sort2f(hy0, hy1);
	if (hy0 < x1.y && x1.y < hy1)
	{
		Sy = SEG_PROJ_STATE_INTERSECT;
		if (corner == C_TL || corner == C_BL)
			Sx = SEG_PROJ_STATE_BEFORE;
		else
			Sx = SEG_PROJ_STATE_AFTER;
		return true;
	}
	Sort2f(vx0, vx1);
	if (vx0 < x0.x && x0.x < vx1)
	{
		Sx = SEG_PROJ_STATE_INTERSECT;
		if (corner == C_TL || corner == C_TR)
			Sy = SEG_PROJ_STATE_BEFORE;
		else
			Sy = SEG_PROJ_STATE_AFTER;
		return true;
	}
	return false;
}

__INLINE float Minf(const float a, const float b)
{
	return (a < b)?a:b;
}

__INLINE float Maxf(const float a, const float b)
{
	return (a > b)?a:b;
}


#define SEG_INTERS_B	0x00	// no intersection, first segment before second
#define SEG_INTERS_L	0x01	// left intersection
#define SEG_INTERS_AI	0x02	// a inside b
#define SEG_INTERS_BI	0x03	// b inside a
#define SEG_INTERS_R	0x04	// right intersection
#define SEG_INTERS_A	0x05	// no intersection, first segment after second
#define	SEG_INTERS_ERR	0x06	// Error!

__INLINE float Minfex(const float a, const float b)
{
	return (a < b)?a:-b;
}

__INLINE float Maxfex(const float a, const float b)
{
	return (a > b)?a:-b;
}

/**
*	��� ������� (SegIntersect(...)) ���������� ��� ����������� 2� ��������
*	a.x < a.y - ������ �������
*	���������� ��� ������� (b)
*	� delta �� ���������� ����������� ���������, �� ������� ���� ���������� �������, ����� ��� ����...
*	�-� ���������� ��� �����������: ���� ��� ���, ���� ����, �� �����, ������ ��� ������, ���� ���, �� ����� � ��� �����.
*	��������� ��������������� ��������� 1�� ������� ������������ �������. ���-��.
*/

__INLINE int SegIntersect(const Vector2 &a, const Vector2 &b, float &delta)
{
#ifdef _DEBUG_
	if (a.x > a.y || b.x > b.y)
		return SEG_INTERS_ERR;
#endif
	delta = 0.0f;
	if (a.y < b.x)
		return SEG_INTERS_B;
	if (a.x > b.y)
		return SEG_INTERS_A;
	if (a.x > b.x && a.y < b.y)
	{
		delta = Minfex(b.y-a.x, a.y-b.x);
		return SEG_INTERS_AI;
	}
	if (b.x > a.x && b.y < a.y)
	{
		delta = Minfex(a.y-b.x, b.y-a.x);
		return SEG_INTERS_BI;
	}
	if (a.x > b.x)
	{
		delta = (b.y - a.x);
		return SEG_INTERS_R;
	}
	else
	{
		delta = -(a.y - b.x);
		return SEG_INTERS_L;
	}
	return SEG_INTERS_ERR;
}

__INLINE bool CAABBCollide( const CAABB &a, const CAABB &b, Vector2& n, float& depth )
{
	float dlx = a.p.x - b.p.x - a.W - b.W;
	float drx = b.p.x - a.p.x - a.W - b.W;
	float dty = a.p.y - b.p.y - a.H - b.H;
	float dby = b.p.y - a.p.y - a.H - b.H;
	if (dlx > drx)
	{
		if (dty > dby)
		{
			if (dlx > dty)
			{
				if( dlx >= 0 )
					return false;
				n = Vector2(1,0);
				depth = dlx;
			}
			else
			{
				if( dty >= 0 )
					return false;
				n = Vector2(0,1);
				depth = dty;
			}
		}
		else
		{
			if (dlx > dby)
			{
				if( dlx >= 0 )
					return false;
				n = Vector2(1,0);
				depth = dlx;
			}
			else
			{
				if( dby >= 0 )
					return false;
				n = Vector2(0,-1);
				depth = dby;
			}
		}
	}
	else
	{
		if (dty > dby)
		{
			if (drx > dty)
			{
				if( drx >= 0 )
					return false;
				n = Vector2(-1,0);
				depth = drx;
			}
			else
			{
				if( dty >= 0 )
					return false;
				n = Vector2(0,1);
				depth = dty;
			}
		}
		else
		{
			if (drx > dby)
			{
				if( drx >= 0 )
					return false;
				n = Vector2(-1,0);
				depth = drx;
			}
			else
			{
				if( dby >= 0 )
					return false;
				n = Vector2(0,-1);
				depth = dby;
			}
		}
	}
	return true;
}

#endif
