#include "StdAfx.h"
#include "phys_misc.h"

float CAABB::GetMin( UINT axis ) const
{
	return axis == 0 ? this->Left() : this->Top();
}

float CAABB::GetMax( UINT axis ) const
{
	return axis == 0 ? Right() : Bottom();
}

CAABB::CAABB( const CAABB& a )
{
	p = a.p;
	H = a.H;
	W = a.W;
}

CAABB::CAABB( float x1, float y1, float x2, float y2 )
{
	W = (max(x1,x2) - min(x1, x2)) * 0.5f;
	H = (max(y1,y2) - min(y1, y2)) * 0.5f;
	p = Vector2(min(x1, x2) + W, min(y1, y2) + H);
}

bool CAABB::IsEmpty()
{
	return (H == 0 && W == 0);
}

void CAABB::Intersect( const CAABB &a, const CAABB &b )
{
	if (fabs(a.p.x - b.p.x) <= a.W + b.W &&
		fabs(a.p.y - b.p.y) <= a.H + b.H)
	{
		float al, bl, ar, br, at, bt, ab, bb;
		float cl, cr, cb, ct;

		al = a.p.x - a.W;
		at = a.p.y - a.H;
		bl = b.p.x - b.W;
		bt = b.p.y - b.W;

		ar = a.p.x + a.W;
		ab = a.p.y + a.H;
		br = b.p.x + b.W;
		bb = b.p.y + b.W;

		cl = al > bl ? al : bl;
		cr = ar < br ? ar : br;
		ct = at > bt ? at : bt;
		cb = ab < bb ? ab : bb;

		W = (cl - cr) / 2;
		H = (cb - ct) / 2;
		p.x = cl + W;
		p.y = ct + H;
	}
	else
	{
		H = 0;
		W = 0;
		p = Vector2(0, 0);
	}
}

bool CAABB::PointInCAABB( float x, float y )
{
	return (x >= this->Left() && x <= this->Right() && y >= this->Top() && y <= this->Bottom() );
}

bool CAABB::operator ==( const CAABB &A ) const
{
	return p == A.p && W == A.W && H == A.H;
}

bool CAABB::operator !=( const CAABB &A ) const
{
	return !(*this == A);
}

void CAABB::Rotate(float angle, Vector2 center)
{
	Vector2 lt = Vector2(Left(), Top()) - center;
	Vector2 rt = Vector2(Right(), Top()) - center;
	Vector2 lb = Vector2(Left(), Bottom()) - center;
	Vector2 rb = Vector2(Right(), Bottom()) - center;
	float s = sin(angle);
	float c = cos(angle);
	lt.Rotate(s, c);
	rt.Rotate(s, c);
	lb.Rotate(s, c);
	rb.Rotate(s, c);

	float l = std::min(std::min(lt.x, rt.x), std::min(lb.x, rb.x));
	float t = std::min(std::min(lt.y, rt.y), std::min(lb.y, rb.y));
	float r = std::max(std::max(lt.x, rt.x), std::max(lb.x, rb.x));
	float b = std::max(std::max(lt.y, rt.y), std::max(lb.y, rb.y));

	W = (r - l) * 0.5f;
	H = (b - t) * 0.5f;
	p = Vector2(l + W, t + H) + center;
}

void CAABB::Rotate(float angle)
{
	Rotate(angle, p);
}

//////////////////////////////////////////////////////////////////////////
// CRay

CRay::CRay( const CRay& r )
{
	this->p = r.p;
	this->r = r.r;
}

CRay::CRay( float x0, float y0, float x1, float y1 )
{
	assert( x0 != x1 || y0 != y1 );
	p = Vector2(x0, y0);
	r.x = x1 - x0;
	r.y = y1 - y0;
}

bool CRay::IsVertical() const
{
	return r.x == 0;
}

bool CRay::IsHorizontal() const
{
	// TODO: ���������� �� IsNear
	return r.y == 0;
	//if (r.x == 0)
	//	return false;
	//return fabs(r.y/r.x) < 0.00001;
}

bool CRay::IsIntersecting( float left, float right, float top, float bottom ) const
{
	assert(top < bottom);
	assert(left < right);

	// � ����� ������ ������. ��������� ���� ����� ����� � �������
	if (p.x < left)
	{
		// ������ ����� �������������
		if (r.x <= 0)
			return false;

		if ( IsHorizontal() )
			return top <= p.y && p.y <= bottom;

		if (r.y > 0 )
			// ����� � �������
			return IsHorLineInters(left, right, top) || IsVertLineInters(top, bottom, left);
		else
			// ����� � ������
			return IsHorLineInters(left, right, bottom) || IsVertLineInters(top, bottom, left);
	}
	else if (p.x > right)
	{
		// ������ ������ ��������������
		if (r.x >= 0)
			return false;

		if ( IsHorizontal() )
			return top <= p.y && p.y <= bottom;

		if (r.y > 0)
			// ������ � �������
			return IsHorLineInters(left, right, top) || IsVertLineInters(top, bottom, right);
		else
			// ������ � ������
			return IsHorLineInters(left, right, bottom) || IsVertLineInters(top, bottom, right);
	}
	else
	{
		// ������ �������������� (����������� �� ��� �)
		if (p.y < top)
		{
			// ���� ��������������
			if (r.y <= 0)
				return false;
			else
				return IsHorLineInters(left, right, top);
		}
		else if (p.y > bottom)
		{
			// ���� ��������������
			if (r.y >= 0)
				return false;
			else
				return IsHorLineInters(left, right, bottom);
		}
		else
		{
			// ����� ������, ��� �� ����� ����������
			return true;
		}
	}

	//return false;	//unreachable code
}

bool CRay::IsIntersecting( const CAABB& aabb ) const
{
	//float left = aabb.Left();
	//float right = aabb.Right();
	//float top = aabb.Top();
	//float bottom = aabb.Bottom();

	return IsIntersecting(aabb.Left(), aabb.Right(), aabb.Top(), aabb.Bottom());
}

bool CRay::GetIntersectionPoint( CAABB const& aabb, Vector2& p ) const
{
	return GetIntersectionPoint(aabb.Left(), aabb.Top(), aabb.Right(), aabb.Bottom(), p);
}

bool CRay::GetIntersectionPoint( float left, float top, float right, float bottom, Vector2& c ) const
{
	assert(top <= bottom);
	assert(left <= right);

	if (p.x < left)
	{
		// ������ ����� �������������
		if (r.x <= 0)
			return false;

		if ( IsHorizontal() )
			return GetVertLineInters(top, bottom, left, c);

		if (r.y > 0 )
			// ����� � �������
			return GetHorLineInters(left, right, top, c) || GetVertLineInters(top, bottom, left, c);
		else
			// ����� � ������
			return GetHorLineInters(left, right, bottom, c) || GetVertLineInters(top, bottom, left, c);
	}
	else if (p.x > right)
	{
		// ������ ������ ��������������
		if (r.x >= 0)
			return false;

		if ( IsHorizontal() )
			return GetVertLineInters(top, bottom, right, c);

		if (r.y < 0)
			// ������ � ������
			return GetHorLineInters(left, right, bottom, c) || GetVertLineInters(top, bottom, right, c);
		else
			// ������ � �������
			return GetHorLineInters(left, right, top, c) || GetVertLineInters(top, bottom, right, c);
	}
	else
	{
		// ������ �������������� (����������� �� ��� �)
		if (p.y < top)
		{
			// ���� ��������������
			if (r.y <= 0)
				return false;
			else
				return GetHorLineInters(left, right, top, c);
		}
		else if (p.y > bottom)
		{
			// ���� ��������������
			if (r.y >= 0)
				return false;
			else
				return GetHorLineInters(left, right, bottom, c);
		}
		else
		{
			if (IsVertical())
			{
				if (r.y < 0)
					return GetHorLineInters(left, right, top, c);
				else
					return GetHorLineInters(left, right, bottom, c);
			}

			if (r.x > 0)
			{
				if ( IsHorizontal() )
					return GetVertLineInters(top, bottom, right, c);
				
				if (r.y > 0)
					// ������ � ������
					return GetHorLineInters(left, right, bottom, c) || GetVertLineInters(top, bottom, right, c);
				else
					// ������ � �������
					return GetHorLineInters(left, right, top, c) || GetVertLineInters(top, bottom, right, c);
			}
			else
			{
				if ( IsHorizontal() )
					return GetVertLineInters(top, bottom, left, c);
				
				if (r.y > 0)
					// ����� � ������
					return GetHorLineInters(left, right, bottom, c) || GetVertLineInters(top, bottom, left, c);
				else
					// ����� � �������
					return GetHorLineInters(left, right, top, c) || GetVertLineInters(top, bottom, left, c);
			}
		}
	}

	//return false; //unreachable code
}

bool CRay::GetIntersectionPoint( float x1, float y1, float x2, float y2, float& x, float& y ) const
{
	Vector2 c;
	bool b = GetIntersectionPoint(x1,y1,x2,y2,c);
	x = c.x;
	y = c.y;
	return b;
}

bool CRay::GetIntersectionInterval(Vector2& v1, Vector2& v2, Vector2& res, Vector2& norm) const
{
	Vector2 rt = r;
	if( rt.Length() == 0 )
		return false;
	rt.Normalize();

	Vector2 n = (v2 - v1).GetPerpendicular();
	if( n.Length() == 0 )
		return false;
	n.Normalize();

	Vector2 rp = rt.GetPerpendicular();

	float l1 = (v1 - p) * rt;
	float l2 = (v2 - p) * rt;
	float r1 = (v1 - p) * rp;
	float r2 = (v2 - p) * rp;

	if( r1 * r2 > 0)
		return false;

	r1 = abs(r1);
	r2 = abs(r2);

	if( r1 + r2 < 0.0001f )
	{
		if( l1 * l2 <= 0)
		{
			res = p;
			norm = n;
			return true;
		}
		else if( l1 > 0 )
		{
			res = p + min(l1,l2) * rt;
			norm = n;
			return true;
		}
		else
			return false;
	}

	float l = (l2 * r1 + l1 * r2) / (r1 + r2);
	
	if( l >= 0 )
	{
		res = p + l * rt;
		norm = n;
		return true;
	}
	else
		return false;
}

bool CRay::operator==(CRay const& ray) const
{
	return (p == ray.p) && (r == ray.r);
}

bool CRay::operator!=(CRay const& r) const 
{
	return !((*this) == r);
}

bool CRay::IsHorLineInters( float x1, float x2, float y ) const
{
	assert(x1 <= x2);
	assert(r.y != 0);
	float xn = (y - p.y) * r.x / r.y + p.x;
	return x1 <= xn && xn <= x2;
}

bool CRay::IsVertLineInters( float y1, float y2, float x ) const
{
	assert(y1 <= y2);
	assert(r.x != 0);
	float yn = (x - p.x) * r.y / r.x + p.y;
	return y1 <= yn && yn <= y2;
}

bool CRay::GetHorLineInters( float x1, float x2, float y, Vector2& c ) const
{
	assert(x1 <= x2);
	assert(r.y != 0);
	c.x = (y - p.y) *r.x / r.y + p.x;
	c.y = y;
	return x1 <= c.x && c.x <= x2;
}

bool CRay::GetVertLineInters( float y1, float y2, float x, Vector2& c ) const
{
	assert(y1 <= y2);
	assert(r.x != 0);
	c.y = (x - p.x) * r.y / r.x + p.y;
	c.x = x;
	return y1 <= c.y && c.y <= y2;
}
