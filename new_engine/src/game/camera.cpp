#include "StdAfx.h"

#include "../config.h"

#include "camera.h"
#include "objects/object_player.h"

//////////////////////////////////////////////////////////////////////////

extern config cfg;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// ��������� ������
float CAMERA_X = 0;
float CAMERA_Y = 0;
Vector2 camera_pos = Vector2(0,0);

// �����������, ���������� �� �������� ������� ������ �� ��������� ��������� �������
float CAMERA_LAG = 1.0f;

// ��������� ������ ������
float CAMERA_GAME_LEFT = 0;
float CAMERA_GAME_RIGHT = 0;
float CAMERA_GAME_TOP = 0;
float CAMERA_GAME_BOTTOM = 0;

float CAMERA_DRAW_LEFT = 0;
float CAMERA_DRAW_RIGHT = 0;
float CAMERA_DRAW_TOP = 0;
float CAMERA_DRAW_BOTTOM = 0;

// ��� ��������� ������� ��������
float CAMERA_OFF_X = 0;
float CAMERA_OFF_Y = 0;

float CAMERA_OLD_OFF_X = 0;
float CAMERA_OLD_OFF_Y = 0;

// �������� ������ ������������ �������
float CAMERA_OBJ_OFF_X = 0;
float CAMERA_OBJ_OFF_Y = 0;

// Scale factors
float CAMERA_SCALE_X = 1.0f;
float CAMERA_SCALE_Y = 1.0f;

// Camera rotation angle
float CAMERA_ANGLE = 0.0f;

// ����� �������, �� ������� �������� ������
CameraFocusObjectPoint CAMERA_FOCUS_ON_OBJ_POS = CamFocusCenter;

// ������, �� ������� �������� ������
GameObject* attached_object = NULL;

bool camera_attach_x = true;
bool camera_attach_y = true;

bool camera_use_bounds = false;
float CAMERA_LEFT_BOUND = 0;
float CAMERA_RIGHT_BOUND = 0;
float CAMERA_TOP_BOUND = 0;
float CAMERA_BOTTOM_BOUND = 0;
//////////////////////////////////////////////////////////////////////////
// �������

__INLINE void CalcCamOffX()
{
	CAMERA_OFF_X = -(CAMERA_X - cfg.scr_width * 0.5f);
	CAMERA_DRAW_LEFT = CAMERA_GAME_LEFT = -CAMERA_OFF_X;
	CAMERA_DRAW_RIGHT = CAMERA_GAME_RIGHT = CAMERA_GAME_LEFT + cfg.scr_width;
}

__INLINE void CalcCamOffY()
{
	CAMERA_OFF_Y = -(CAMERA_Y - cfg.scr_height * 0.5f);
	CAMERA_DRAW_TOP = CAMERA_GAME_TOP = -CAMERA_OFF_Y;
	CAMERA_DRAW_BOTTOM = CAMERA_GAME_BOTTOM = CAMERA_GAME_TOP + cfg.scr_height;
}

__INLINE void ApplyBounds()
{
	if (CAMERA_GAME_LEFT < CAMERA_LEFT_BOUND)
	{
		CAMERA_X += CAMERA_LEFT_BOUND - CAMERA_GAME_LEFT;
		CalcCamOffX();
	}

	if (CAMERA_GAME_RIGHT >  CAMERA_RIGHT_BOUND)
	{
		CAMERA_X -= CAMERA_GAME_RIGHT - CAMERA_RIGHT_BOUND;
		CalcCamOffX();
	}

	if (CAMERA_GAME_TOP < CAMERA_TOP_BOUND)
	{
		CAMERA_Y += CAMERA_TOP_BOUND - CAMERA_GAME_TOP;
		CalcCamOffY();
	}

	if (CAMERA_GAME_BOTTOM >  CAMERA_BOTTOM_BOUND)
	{
		CAMERA_Y -= CAMERA_GAME_BOTTOM - CAMERA_BOTTOM_BOUND;
		CalcCamOffY();
	}
}
//////////////////////////////////////////////////////////////////////////
void CameraAttachToAxis(bool x, bool y)
{
	camera_attach_x = x;
	camera_attach_y = y;
}

// ���������� ������ � �������
// ���� �������� NULL, �� �������� ��
void CameraAttachToObject(GameObject* obj)
{
	attached_object = obj;
}

void SetCameraAttachedObjectOffset(float x, float y)
{
	CAMERA_OBJ_OFF_X = x;
	CAMERA_OBJ_OFF_Y = y;

	if ( CAMERA_LAG > 0 )
		return;

	if ( attached_object )
	{
		CAMERA_X = attached_object->aabb.p.x + x;
		CAMERA_Y = attached_object->aabb.p.y + y;
	}
	else
	{
		CAMERA_X += x;
		CAMERA_Y += y;
	}
}

// ���������� ������
GameObject* GetCameraAttachedObject()
{
	return attached_object;
}

// ������� ������
// x, y - ���������� �����, ������� ����� � ������ ������
void CameraMoveToPos(float x, float y, bool use_bounds)
{
	attached_object = NULL;

	camera_pos.x = x;
	camera_pos.y = y;
	CAMERA_X = floor( camera_pos.x + 0.5f);
	CAMERA_Y = floor( camera_pos.y + 0.5f);

	CalcCamOffX();
	CalcCamOffY();
	
	if (use_bounds && camera_use_bounds)
		ApplyBounds();
}

// �������������, ���������� �� ������������ ������� ��� ������
void CameraUseBounds(bool b)
{
	camera_use_bounds = b;
}

// ������������� �������� ������
// ���� ������� ������, ��� ������ �� ������ � �����, �� ������� � ������ ����� ��������
void CameraSetBounds(float left, float right, float top, float bottom)
{
	CAMERA_LEFT_BOUND = left;
	CAMERA_RIGHT_BOUND = right;
	if (right - left < cfg.scr_width) 
		right = left + cfg.scr_width;
	CAMERA_TOP_BOUND = top;
	CAMERA_BOTTOM_BOUND = bottom;
	if (bottom - top < cfg.scr_height) 
		bottom = top + cfg.scr_height;
}

// ���������� � x, y ����� ������������ �������, � �������� ���������� ������
void GetCameraAttachedFocusShift(float& x, float& y)
{
	if (attached_object)
	{
		switch (CAMERA_FOCUS_ON_OBJ_POS)
		{
		case CamFocusLeftCenter:
			x = -attached_object->aabb.W;
			break;
		case CamFocusRightCenter:
			x = attached_object->aabb.W;
			break;
		case CamFocusBottomCenter:
			y = attached_object->aabb.H;
			break;
		case CamFocusTopCenter:
			y = -attached_object->aabb.H;
			break;
		case CamFocusLeftTopCorner:
			x = -attached_object->aabb.W;
			y = -attached_object->aabb.H;
			break;
		case CamFocusLeftBottomCorner:
			x = -attached_object->aabb.W;
			y = attached_object->aabb.H;
			break;
		case CamFocusRightTopCorner:
			x = attached_object->aabb.W;
			y = -attached_object->aabb.H;
			break;
		case CamFocusRightBottomCorner:
			x = attached_object->aabb.W;
			y = attached_object->aabb.H;
			break;
		default: break;
		}
	}
}

// ��������� ��������� ������
void CameraUpdatePosition()
{
	// �� �������� ������� ��������� OpenGL - � ����� �������� ��, ���
	// �� ������ �������.

	glTranslated( floor(CAMERA_OFF_X+0.5f)+0.375, floor(CAMERA_OFF_Y+0.5f)+0.375, 0);
}


void CameraUpdateFocus()
{
	CAMERA_OLD_OFF_X = CAMERA_OFF_X;
	CAMERA_OLD_OFF_Y = CAMERA_OFF_Y;

	if (attached_object)
	{
		float foc_x = 0;
		float foc_y = 0;
		GetCameraAttachedFocusShift(foc_x, foc_y);

		if (camera_attach_x)
		{
			camera_pos.x = camera_pos.x + (attached_object->aabb.p.x - CAMERA_OBJ_OFF_X + foc_x - camera_pos.x)*CAMERA_LAG;
			CAMERA_X = floor( camera_pos.x + 0.5f);
			CalcCamOffX();
		}
		if (camera_attach_y)
		{
			float target_y = attached_object->aabb.p.y - CAMERA_OBJ_OFF_Y + foc_y;
			if (attached_object->type == objPlayer) 
				target_y +=((ObjPlayer*)attached_object)->camera_offset;
			camera_pos.y = camera_pos.y + ( target_y - camera_pos.y)*CAMERA_LAG;
			CAMERA_Y = floor( camera_pos.y + 0.5f);
			CalcCamOffY();
		}
	}

	if (camera_use_bounds)
		ApplyBounds();
}

void CameraSetLag( float lag )
{
	CAMERA_LAG = 1-lag;
}

// Scale

// ���������� ����������� ����������
void CameraSetScale(float x, float y)
{
	CAMERA_SCALE_X = x;
	CAMERA_SCALE_Y = y;
}

void CameraSetAngle(float angle)
{
	CAMERA_ANGLE = angle;
}

// ��������, ������������ �� ����������/����������
bool _CameraUsesTransformations()
{
	return (CAMERA_SCALE_X != 1.0f || CAMERA_SCALE_Y != 1.0f || CAMERA_ANGLE != 0.0f);
}

void StoreCamera()
{
	glPushMatrix();
}

void RestoreCameraEdges()
{
	CAMERA_DRAW_LEFT   = CAMERA_GAME_LEFT;
	CAMERA_DRAW_RIGHT  = CAMERA_GAME_RIGHT;
	CAMERA_DRAW_TOP    = CAMERA_GAME_TOP;
	CAMERA_DRAW_BOTTOM = CAMERA_GAME_BOTTOM;
}

void RestoreCamera()
{
	glPopMatrix();
	RestoreCameraEdges();
}

void _CameraCalcTransformedBounds()
{
	const float w = (cfg.scr_width / (2.0f * CAMERA_SCALE_X) );
	const float h = (cfg.scr_height / (2.0f * CAMERA_SCALE_Y) );
	const float c = cos( (CAMERA_ANGLE * Const::Math::PI) / 180.0f );
	const float s = sin( (CAMERA_ANGLE * Const::Math::PI) / 180.0f );

	const float w_x = w * c /*- 0.0f * s*/;
	const float w_y = w * s /*+ 0.0f * c*/;

	const float h_x = /*0.0f * c*/ - h * s;
	const float h_y = /*0.0f * s +*/ h * c;

	CAMERA_DRAW_LEFT   = CAMERA_X - abs(w_x) - abs(h_x);
	CAMERA_DRAW_TOP    = CAMERA_Y - abs(w_y) - abs(h_y);
	CAMERA_DRAW_RIGHT  = CAMERA_X + abs(w_x) + abs(h_x);
	CAMERA_DRAW_BOTTOM = CAMERA_Y + abs(w_y) + abs(h_y);

	/*
	RenderBox( CAMERA_X - cfg.scr_width / 20.f, CAMERA_Y - cfg.scr_height / 20.f, 1.1f, cfg.scr_width / 10.f, cfg.scr_height / 10.0f, RGBAf( 1.0f, 1.0f, 1.0f ) );
	RenderBox( CAMERA_X - (CAMERA_DRAW_RIGHT - CAMERA_DRAW_LEFT) / 20.f, CAMERA_Y - (CAMERA_DRAW_BOTTOM - CAMERA_DRAW_TOP) / 20.f, 1.1f, (CAMERA_DRAW_RIGHT - CAMERA_DRAW_LEFT) / 10.f, (CAMERA_DRAW_BOTTOM - CAMERA_DRAW_TOP) / 10.f, RGBAf( 1.0f, 0.0f, 0.0f ) );

	RenderLine( CAMERA_X, CAMERA_Y, CAMERA_X + (- w_x - h_x)/10.0f, CAMERA_Y + (- w_y - h_y)/10.0f, 1.1f, RGBAf( 1.0f, 0.0f, 0.0f ) );
	RenderLine( CAMERA_X, CAMERA_Y, CAMERA_X + (  w_x - h_x)/10.0f, CAMERA_Y + (  w_y - h_y)/10.0f, 1.1f, RGBAf( 0.0f, 1.0f, 0.0f ) );
	RenderLine( CAMERA_X, CAMERA_Y, CAMERA_X + (  w_x + h_x)/10.0f, CAMERA_Y + (  w_y + h_y)/10.0f, 1.1f, RGBAf( 0.0f, 0.0f, 1.0f ) );
	RenderLine( CAMERA_X, CAMERA_Y, CAMERA_X + (- w_x + h_x)/10.0f, CAMERA_Y + (- w_y + h_y)/10.0f, 1.1f, RGBAf( 0.0f, 1.0f, 1.0f ) );

	RenderLine( CAMERA_X + (- w_x + h_x)/10.0f, CAMERA_Y + (- w_y + h_y)/10.0f, CAMERA_X + (- w_x - h_x)/10.0f, CAMERA_Y + (- w_y - h_y)/10.0f, 1.1f, RGBAf( 1.0f, 1.0f, 0.0f ) );
	RenderLine( CAMERA_X + (- w_x - h_x)/10.0f, CAMERA_Y + (- w_y - h_y)/10.0f, CAMERA_X + (  w_x - h_x)/10.0f, CAMERA_Y + (  w_y - h_y)/10.0f, 1.1f, RGBAf( 1.0f, 1.0f, 0.0f ) );
	RenderLine( CAMERA_X + (  w_x - h_x)/10.0f, CAMERA_Y + (  w_y - h_y)/10.0f, CAMERA_X + (  w_x + h_x)/10.0f, CAMERA_Y + (  w_y + h_y)/10.0f, 1.1f, RGBAf( 1.0f, 1.0f, 0.0f ) );
	RenderLine( CAMERA_X + (  w_x + h_x)/10.0f, CAMERA_Y + (  w_y + h_y)/10.0f, CAMERA_X + (- w_x + h_x)/10.0f, CAMERA_Y + (- w_y + h_y)/10.0f, 1.1f, RGBAf( 1.0f, 1.0f, 0.0f ) );
	*/
}

// ��������� ������������ ����������
void _CameraApplyScale()
{
	glMatrixMode(GL_MODELVIEW_MATRIX);


	float scale_off_x = -(CAMERA_SCALE_X-1.0f)*CAMERA_X;
	float scale_off_y = -(CAMERA_SCALE_Y-1.0f)*CAMERA_Y;

	glTranslatef(scale_off_x, scale_off_y, 0 );
	
	glScalef(CAMERA_SCALE_X, CAMERA_SCALE_Y, 1.0f);
}

void _CameraApplyAngle()
{
	glTranslatef( CAMERA_X, CAMERA_Y, 0.0f );
	glRotatef( CAMERA_ANGLE, 0.0f, 0.0f, -1.0f );
	glTranslatef( -CAMERA_X, -CAMERA_Y, 0.0f );
}
