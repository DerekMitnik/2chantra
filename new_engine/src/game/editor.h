#ifndef __EDITOR_H_
#define __EDITOR_H_

#ifdef MAP_EDITOR

class GameObject;
struct ObjectConnection;

namespace editor
{
	enum EditorStates
	{
		EDITOR_OFF,
		EDITOR_MAP_EDITOR,
		EDITOR_PROTO_EDITOR,
		EDITOR_TEXTURE_EDITOR,
		EDITOR_SCRIPT_EDITOR,
		EDITOR_CONTROL_EDITOR,
	};
	
	void SetEditorState(EditorStates state);
}

class EditorObject
{
public:
	char* proto_name;
	bool grouped;
	bool really_physic;
	Vector2 creation_shift;
	RGBAf border_color;
	RGBAf editor_color;
	bool show_border;
	
	bool removed;
	
	// ObjectRibbon
	bool from_proto;
	
	// ObjectSpawner
	float min_difficulty;
	float max_difficulty;
	
	ObjectConnection* link;
	ObjectConnection* incoming_links;

public:
	EditorObject();
	~EditorObject();
	void SetLink( GameObject* obj, GameObject* target );
	void SetProtoName( const char* proto_name );
	void SetCreationShift( Vector2 shift );
	void SetGrouped( bool b = true );
	void SetFromProto( bool b = true );
};

class EditorWidget
{
	
};

void ProcessEditor();
void EditorGetObjects(CAABB area);
void AddObjectInArea(UINT id);
void EditorRegAreaSelectProc(lua_State* L);
int EditorCopyObject(lua_State* L);
int EditorDumpMap(lua_State* L);
int EditorResize(lua_State* L);

#endif // MAP_EDITOR

#endif //__EDITOR_H_