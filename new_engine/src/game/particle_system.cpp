#include "StdAfx.h"
#include "particle_system.h"
#include "object_mgr.h"
#include "../misc.h"
#include "phys/phys_misc.h"
#include "objects/object.h"
#include "objects/object_physic.h"
#include "objects/object_player.h"
#include "objects/object_dynamic.h"
#include "objects/object_environment.h"
#include "player.h"
#include "camera.h"

extern GameObject* attached_object;
extern vector<GameObject*>* oldOnScreenPhys;
extern config cfg;

uint32_t total_particles = 0;
uint32_t phys_particles = 0;
uint32_t weather_particles = 0;

PARTICLES_PARAM_TYPE particles_cfg[ static_cast<size_t>(particles_param_last) ];

bool ParticleCounterCheck( ParticleCounterType type )
{
	switch ( type )
	{
		case pcPhysic:
		{
			return ( particles_cfg[ max_phys_particles ] >= 0.0f && phys_particles >= particles_cfg[ max_phys_particles ] );
		}
		case pcWeather:
		{
			return ( particles_cfg[ max_weather_particles ] >= 0.0f && weather_particles >= particles_cfg[ max_weather_particles ] );
		}
		default:
		{
			return ( particles_cfg[ max_simple_particles ] >= 0.0f && total_particles >= particles_cfg[ max_simple_particles ] );
		}
	}
}

void ParticleCounterChange( ParticleCounterType type, int amount )
{
	switch ( type )
	{
		case pcPhysic:
		{
			assert( amount >= 0 || phys_particles > 0 );
			phys_particles += amount;
			break;
		}
		case pcWeather:
		{
			assert( amount >= 0 || weather_particles > 0 );
			weather_particles += amount;
			break;
		}
		default:
		{
			assert( amount >= 0 || total_particles > 0 );
			total_particles += amount;
			break;
		}
	}
}


Vector2 triangle1;
Vector2 triangle2;
Vector2 triengle3;

void InitParticles()
{
	total_particles = 0;
	phys_particles = 0;
	weather_particles = 0;
}

void InitParticlesParam()
{
	memset( particles_cfg, 0, sizeof( particles_cfg ) );
	particles_cfg[intensity] = particles_cfg[include_one_sided] = particles_cfg[use_physic] = 1.0f;
	particles_cfg[relative_speed] = 0.2f;
	particles_cfg[max_simple_particles] = 4096.0f;
	particles_cfg[max_phys_particles] = 3072.0f;
	particles_cfg[max_weather_particles] = 1024.0f;
}

void SetParticlesParam( ParticlesParam param, PARTICLES_PARAM_TYPE value )
{
	assert( param > 0 && param <= particles_param_last );
	particles_cfg[ param ] = value;
}

PARTICLES_PARAM_TYPE GetParticlesParam( ParticlesParam param )
{
	assert( param > 0 && param <= particles_param_last );
	return particles_cfg[ param ];
}

/*
������:
1 - �������� �� ����� aabb �������.
2 - ��������� ����������.
3 - "����", �������� �� ������� ������ ��������� ������� ������� ����.
*/

Vector2 wind;

void SetParticlesWind( Vector2 new_wind )
{
    wind = new_wind;
}

Vector2 GetParticlesWind()
{
    return wind;
}

template<typename T>
__INLINE void _swap(int i, int j, T* arr)
{
    T t = arr[i];
    arr[i] = arr[j];
    arr[j] = t;
}

__INLINE void CParticleSystem::swapParticles(int i, int j)
{
	_swap(i, j, particles);
	_swap(i, j, pp);
	_swap(i, j, pcolor);
	_swap(i, j, pz);
	_swap(i ,j, tex_coords);
	_swap(i ,j, fsizes);
	_swap(i ,j, extra);
}


void CParticleSystem::Init()
{
	this->particles = new CParticle[MaxParticles];
	this->pp = new Vector2[MaxParticles];
	this->pcolor = new RGBAf[MaxParticles];
	this->pz = new float[MaxParticles];
	this->tex_coords = new coord2f_t*[MaxParticles];
	this->fsizes = new coord2f_t[MaxParticles];
	this->extra = new int[MaxParticles];
	if ( mode & 32 || mode & 64 )
	{
		counter_type = pcWeather;
	}
	else
	{
		if ( physic )
		{
			counter_type = pcPhysic;
		}
	}
}

bool CParticleSystem::AreaMovement( Vector2& coord, Vector2 vel, vector<CAABB>* area, bool physic, float bounce, Vector2& real_v, CParticle* p, int max_bounce )
{
	if ( (!physic) || (particles_cfg[use_physic] == 0.0f) )
	{
		coord += vel;
		return true;
	}
	
	Vector2 pnt = coord;
	pnt.y = pnt.y + vel.y;
	bool blocked = false;
	bool bounced = false;
	
	if ( area != NULL && area->size() > 0 && !(mode & 32)  )
	{
		assert(area->size() == 1);
		bool in_area = false;
		for ( vector<CAABB>::iterator it = area->begin(); it != area->end(); it++ )
		{
			if ( it->PointInCAABB( coord.x, coord.y ) )
			{
				in_area = true;
				break;
			}
		}
		if (!in_area)
			blocked = true;
		
		//if ( !blocked )
		//	coord += vel;
		//return !blocked;
	}

	bool check;
	ObjPhysic* op = NULL;
	for ( vector<GameObject*>::iterator it = oldOnScreenPhys->begin(); it != oldOnScreenPhys->end(); it++ )
	{
		op = static_cast<ObjPhysic*>(*it);
		if ( SolidTo(op, objParticleSystem) )
		{
			check = op->aabb.PointInCAABB(pnt.x, pnt.y);
			if ( check && particles_cfg[include_polygons] == 1.0f && op->geometry != &op->rectangle )
				check = op->PointIn( pnt );
			if ( particles_cfg[include_one_sided] == 1.0f && op->IsOneSide() && coord.y > (*it)->aabb.Top() )
				check = false;
			if ( !op->IsSolid() )
				check = false;
			
			if ( check )
			{
				p->bounces++;
				if ( bounce > 0 ) real_v.y = -real_v.y * bounce;
				{
					blocked = true;
					bounced = true;
				}
				break;
			}
		}
	}
	if ( !blocked )
	{
		pnt.x = pnt.x + vel.x;
		for ( vector<GameObject*>::iterator it = oldOnScreenPhys->begin(); it != oldOnScreenPhys->end(); it++ )
		{
			assert( op );
			if ( SolidTo(op, objParticleSystem) )
			{
				if ( particles_cfg[include_polygons] == 1.0f )
					check = op->PointIn( pnt );
				else
					check = (*it)->aabb.PointInCAABB(pnt.x, pnt.y);
				if ( particles_cfg[include_one_sided] == 1.0f && op->IsOneSide() && coord.y > (*it)->aabb.Top() )
					check = false;
				if ( !op->IsSolid() )
					check = false;

				if ( check )
				{
					if ( !bounced )
					{
						p->bounces++;
					}
					if ( bounce > 0 )
					{
						real_v.x = -real_v.x * bounce;
					}
					blocked = true;
					break;
				}
			}
		}
	}
	if ( max_bounce > 0 && p->bounces > max_bounce )
	{
		p->age = p->life;
	}
	if ( !blocked )
		coord += vel;
	return !blocked;
}

//Updates particle positions and parameters
bool CParticleSystem::Update( ObjEnvironment* env )
{
#ifdef _DEBUG_DISABLE_PARTICLES_UPDATE
	return false;
#endif
	for(size_t i=0; i < ParticlesActive; i++)
	{
		CParticle& pi = particles[i];

		pi.age++;
		if (pi.age >= pi.life)
		{
			//swapParticles(static_cast<int>(i), static_cast<int>(ParticlesActive-1));
			ParticlesActive--;

			ParticleCounterChange( counter_type, -1 );

			particles[i] = particles[ParticlesActive];
			pp[i] = pp[ParticlesActive];
			pcolor[i] = pcolor[ParticlesActive];
			pz[i] = pz[ParticlesActive];
			tex_coords[i] = tex_coords[ParticlesActive];
			fsizes[i] = fsizes[ParticlesActive];
			extra[i] = extra[ParticlesActive];

			i--;
			continue;
		}
		Vector2& p = pp[i];
		RGBAf& color = pcolor[i];

		pi.v += gravity;
		if ( max_abs_speed > 0.0f )
		{
			if ( pi.v.y > max_abs_speed )
			{
				pi.v.y = max_abs_speed;
			}
			if ( pi.v.y < -max_abs_speed )
			{
				pi.v.y = -max_abs_speed;
			}
			if ( pi.v.x > max_abs_speed )
			{
				pi.v.x = max_abs_speed;
			}
			if ( pi.v.x < -max_abs_speed )
			{
				pi.v.x = -max_abs_speed;
			}
		}
		pi.old_p = p;

#ifdef PARTICLES_EXTRA_PHYS_CHECKS
		if ( physic )
		{
			for ( vector<GameObject*>::iterator it = oldOnScreenPhys->begin(); it != oldOnScreenPhys->end(); it++ )
			{
				if ( (*it)->aabb.PointInCAABB(p.x, p.y) )
				{
					//���������� �������, ����������� ������ ������� ���������� ����.
					pi.life = 0;	//��� ���� ���� ������� ��� (������� ������ �����), ���� ��������� ��� �� ����� ������.	
					break;
				}
			}
		}
#endif //PARTICLES_EXTRA_PHYS_CHECKS

#ifndef _DEBUG
		if ( drop_area != NULL && !pi.is_on_plane && drop_area->size() > 0 )
#else
		if ( false )
#endif
		{
			for ( vector<CAABB>::iterator it = drop_area->begin(); it != drop_area->end(); it++ )
			{
				if ( it->PointInCAABB(p.x, p.y) )
				{
					if ( rand() % 100 > 90 )
					{
						pi.is_on_plane = true;
						pz[i] = SHADOW_Z;
					}
					break;
				}
			}
		}

		if ( pi.is_on_plane )
		{
			color += (pi.dc*particles_cfg[relative_speed]);
			pi.size += pi.dsize;
			continue;
		}

	}

	switch ( trajectory )
	{
		case pttRipple:
		{
			for(size_t i=0; i < ParticlesActive; i++)
			{
				CParticle& pi = particles[i];
				Vector2& p = pp[i];
				RGBAf& color = pcolor[i];
				
				pi.parameter += particles_cfg[relative_speed];

				// TODO: �� ����, ��� � ������ ������ �����: sin ��� FastSin? ��������� ����������, ���
				// sin ������� �����. ��������, �� ������ ����������� ��������� ����� ������.
				// �� ���� InvLengthQ ������� ��������.
				//float tr = t_param1 * FastSin<false>(pi.parameter*t_param2) * pi.v.InvLengthQ();
				float tr = t_param1 * FastSimpleSin(pi.parameter*t_param2) * InvSqrt( pi.v.x*pi.v.x + pi.v.y*pi.v.y );
				//float tr = t_param1 * sin(pi.parameter*t_param2) * pi.v.InvLengthQ();

				float tr_x = -pi.v.x * tr;
				float tr_y = pi.v.y * tr;
				AreaMovement( p, (pi.v + Vector2( tr_x, tr_y ))*particles_cfg[relative_speed], area, physic, bounce, pi.v, &pi, max_bounce );
				color += (pi.dc*particles_cfg[relative_speed]);
				pi.size += pi.dsize;
#ifdef SELECTIVE_RENDERING
				updateLTRB(p, pi.size);
#endif
			}
			break;
		}
		case pttTwist:
		{
			GameObject* go = GetGameObject( emitter );
			Vector2 center_point;
			for(size_t i=0; i < ParticlesActive; i++)
			{
				CParticle& pi = particles[i];
				Vector2& p = pp[i];
				
				center_point = p;
				//Vector2 old_center_point = center_point;
				if ( !dead )
					center_point = position;
					//center_point = old_center_point = position;
				if ( mode & 2 )
				{
					if ( go )
					{
						center_point = go->aabb.p;
						//if ( go->IsPhysic() && ((ObjPhysic*)go)->IsDynamic() )
						//old_center_point = center_point - ((ObjDynamic*)go)->vel;
					}
				}
				pi.parameter += particles_cfg[relative_speed]*t_param1;

				//float ps = sin(pi.parameter);
				float ps = FastSimpleSin(pi.parameter);
				// TODO: ���� FastCos, �������� ���.
				//float pc = cos(pi.parameter);
				float pc = FastSimpleSin(3.1415926f/2 - pi.parameter);

				AreaMovement( p, center_point + (particles_cfg[relative_speed]*pi.v.Length() + (p - center_point).Length()) * Vector2(ps, pc) - p, area, physic, bounce, pi.v, &pi, max_bounce);
				pcolor[i] += (pi.dc*particles_cfg[relative_speed]);
				pi.size += pi.dsize;
#ifdef SELECTIVE_RENDERING
				updateLTRB(p, pi.size);
#endif
			}
			
			break;
		}
		case pttToCenter:
		{
			Vector2 center = position;
			if ( emitter )
			{
				center = GetGameObject( emitter )->aabb.p;
			}
			for(size_t i=0; i < ParticlesActive; i++)
			{
				CParticle& pi = particles[i];
				Vector2& p = pp[i];
				RGBAf& color = pcolor[i];
			
				Vector2 to_center = center - p;
				Vector2 dp;
				const float l = pi.v.Length();
				if ( l < to_center.Length() )
				{
					dp = to_center.Normalized() * pi.v.Length();
				}
				else
				{
					dp = to_center;
				}
				AreaMovement( p, dp, area, physic, bounce, pi.v, &pi, max_bounce );
				color += (pi.dc*particles_cfg[relative_speed]);
				pi.size += pi.dsize;
#ifdef SELECTIVE_RENDERING
				updateLTRB(p, pi.size);
#endif
			}
			break;
		}
		case pttSine:
		{
			for(size_t i=0; i < ParticlesActive; i++)
			{
				CParticle& pi = particles[i];
				Vector2& p = pp[i];
				RGBAf& color = pcolor[i];

				pi.parameter += particles_cfg[relative_speed];
				Vector2 perp = Vector2();
				if ( pi.v.Length() > 0 ) perp = pi.v.GetPerpendicular().Normalized();
				Vector2 tr = perp * t_param1 * FastSimpleSin(pi.parameter*t_param2);
				//Vector2 tr(pi.v.GetPerpendicular());
				//tr.Normalize();
				//tr *= t_param1 * FastSin<false>(pi.parameter*t_param2);

				AreaMovement( p, (pi.v + tr)*particles_cfg[relative_speed], area, physic, bounce, pi.v, &pi, max_bounce );
				color += (pi.dc*particles_cfg[relative_speed]);
				pi.size += pi.dsize;
#ifdef SELECTIVE_RENDERING
				updateLTRB(p, pi.size);
#endif
			}
			break;
		}
		case pttCosine:
		{
			for(size_t i=0; i < ParticlesActive; i++)
			{
				CParticle& pi = particles[i];
				Vector2& p = pp[i];
				RGBAf& color = pcolor[i];

				pi.parameter += particles_cfg[relative_speed];
				Vector2 perp = Vector2();
				if ( pi.v.Length() > 0 ) perp = pi.v.GetPerpendicular().Normalized();
				Vector2 tr = perp * t_param1 * FastSimpleSin(pi.parameter*t_param2);
				//Vector2 tr(pi.v.GetPerpendicular());
				//tr.Normalize();
				//tr *= t_param1 * FastSin<false>(pi.parameter*t_param2);

				AreaMovement( p, (pi.v + tr)*particles_cfg[relative_speed], area, physic, bounce, pi.v, &pi, max_bounce );
				color += (pi.dc*particles_cfg[relative_speed]);
				pi.size += pi.dsize;
#ifdef SELECTIVE_RENDERING
				updateLTRB(p, pi.size);
#endif
			}
			break;
		}
		case pttPseudoDepth:
		{
			for(size_t i=0; i < ParticlesActive; i++)
			{
				CParticle& pi = particles[i];
				Vector2& p = pp[i];
				RGBAf& color = pcolor[i];

				pi.size += pi.parameter*particles_cfg[relative_speed];
				if (pi.size > t_param2 || pi.size < -t_param2) pi.parameter = 0;
				AreaMovement( p, pi.v*particles_cfg[relative_speed], area, physic, bounce, pi.v, &pi, max_bounce);
				color += (pi.dc*particles_cfg[relative_speed]);
#ifdef SELECTIVE_RENDERING
				updateLTRB(p, pi.size);
#endif
			}
			break;
		}
		case pttRandom:
		{
			for(size_t i=0; i < ParticlesActive; i++)
			{
				CParticle& pi = particles[i];
				Vector2& p = pp[i];
				RGBAf& color = pcolor[i];

				//Vector2 tr = pi.v.GetPerpendicular().Normalized() * Random_Float(-pi.parameter, pi.parameter);
				Vector2 perp = Vector2();
				//Vector2 norm = Vector2();
				if ( pi.v.Length() > 0 )
				{
					perp = pi.v.GetPerpendicular().Normalized();
					//norm = pi.v.Normalized();
				}
				Vector2 tr = perp * Random_Float(-t_param1, t_param1);

				//Vector2 tr2 = norm * Random_Float(-t_param2, t_param2);
				pi.v += tr;
				AreaMovement( p,  pi.v*particles_cfg[relative_speed], area, physic, bounce, pi.v, &pi, max_bounce );
				color += (pi.dc*particles_cfg[relative_speed]);
				pi.size += pi.dsize;
#ifdef SELECTIVE_RENDERING
				updateLTRB(p, pi.size);
#endif
			}
			break;
		}
		default:
		{
			for(size_t i=0; i < ParticlesActive; i++)
			{
				CParticle& pi = particles[i];
				Vector2& p = pp[i];
				RGBAf& color = pcolor[i];

				AreaMovement( p,  pi.v*particles_cfg[relative_speed], area, physic, bounce, pi.v, &pi, max_bounce );
				color += (pi.dc*particles_cfg[relative_speed]);
				pi.size += pi.dsize;

#ifdef SELECTIVE_RENDERING
				updateLTRB(p, pi.size);
#endif
			}
			break;
		}
	}


	if (wind_affected)
	{
		if ( mode == 32 || !env->stop_global_wind )
		{
			for(size_t i=0; i < ParticlesActive; i++)
			{
				AreaMovement( pp[i], wind*particles_cfg[relative_speed], area, physic, bounce, particles[i].v, particles+i, max_bounce );
			}
		}
		if ( env->wind.x != 0.0f || env->wind.y != 0.0f )
		{
			for(size_t i=0; i < ParticlesActive; i++)
			{
				AreaMovement( pp[i], env->wind*particles_cfg[relative_speed], area, physic, bounce, particles[i].v, particles+i, max_bounce );
			}
		}
	}

	if (life != -1)
	{
		age++;
		if (age >= life)
		{
			this->dead = true;
			return true;
		}
	}

	emission_accumulated += emission * particles_cfg[relative_speed] * particles_cfg[intensity];

	while (emission_accumulated >= 1.0f)
	{
		CreateParticle();
		emission_accumulated -= 1.0f;
	}
	return true;
}

void CParticleSystem::Render()
{
#ifdef _DEBUG_DISABLE_PARTICLES_DRAW
    return;
#endif
    if (ParticlesActive == 0)
        return;

    Vector2 trace;
    Vector2 trace_n;
    float k = 1.0f;
    for(size_t i=0;i<ParticlesActive;i++)
    {
	trace = pp[i];
	if ( particles[i].trace > 0.0f )
	{
		Vector2 norm = particles[i].old_p - pp[i];
		if ( norm.Length() > 0 )
		    norm = norm.Normalized();
		else
		    norm = Vector2();
		trace = pp[i] + ( norm * particles[i].trace);
	}
	else if ( particles[i].trace < 0.0f )
	{
		trace = particles[i].old_p;
	}
		if ( trace != pp[i] )
		{
			//RenderLine( pp[i].x, pp[i].y, trace.x, trace.y, pz[i]-0.0001f, pcolor[i], blendingMode);
			trace_n = (trace-pp[i]).GetPerpendicular().Normalized();
			k = particles[i].size * trace_width;
			RenderTriangle( trace, pp[i] + (k * trace_n), pp[i] - (k * trace_n),  pz[i]-0.0001f, pcolor[i], blendingMode );
		}
	}
	if ( texture )
	{
		FrameInfo* fi = this->texture->frames->frame;
		assert(fi);
		size_t frameNum = 0;
		
		//if (this->texture->frames->framesCount > 1)
		//{
		switch ( animation )
		{
			case lifetime_animation:
				for(size_t i=0;i<ParticlesActive;i++)
				{
					const CParticle& par = particles[i];
					fsizes[i].x = fsizes[i].y = par.size * 0.5f;
					if ( par.life > 0 )
					    frameNum = (size_t)((par.age * this->texture->frames->framesCount ) / par.life );
					else
					    frameNum = 0;
					if (frameNum >= this->texture->frames->framesCount) frameNum = this->texture->frames->framesCount-1;
					tex_coords[i] = fi[frameNum].coord;
				}
				break;
			case bounce_animation:
				for(size_t i=0;i<ParticlesActive;i++)
				{
					const CParticle& par = particles[i];
			        	fsizes[i].x = fsizes[i].y = par.size * 0.5f;
					frameNum = min( static_cast<int>(this->texture->frames->framesCount-1), par.bounces );
					tex_coords[i] = fi[frameNum].coord;
				}
				break;
			case loop_animation:
				{
					int param = 0 == animation_param ? 1 : animation_param;
					for(size_t i=0;i<ParticlesActive;i++)
					{
						const CParticle& par = particles[i];
						fsizes[i].x = fsizes[i].y = par.size * 0.5f;
						frameNum = static_cast<int>(floor( static_cast<float>(par.age / param) )) % this->texture->frames->framesCount;
						tex_coords[i] = fi[frameNum].coord;
					}
				}
				break;
			case random_frame_animation:
				{
					for(size_t i=0;i<ParticlesActive;i++)
					{
						if ( extra[i] == -1 ) extra[i] = Random_Int( (animation_param & 65280 ) >> 8, animation_param & 255 );
						const CParticle& par = particles[i];
						fsizes[i].x = fsizes[i].y = par.size * 0.5f;
						frameNum = extra[i];
						tex_coords[i] = fi[frameNum].coord;
					}
				}
				break;
			case no_animation:
			default:
				break;
		}
	//}

        RenderFrameArray(pp, pz, fsizes, tex_coords, texture, pcolor, ParticlesActive, blendingMode);

    }


    return;
}

void CParticleSystem::SetGeometry( Vector2 * points, int numPoints )
{
    geom = points;
    GeomNumPoints = numPoints;
}

extern float CAMERA_GAME_TOP;
extern float CAMERA_GAME_BOTTOM;
extern float CAMERA_GAME_LEFT;
extern float CAMERA_GAME_RIGHT;
extern float CAMERA_DRAW_TOP;
extern float CAMERA_DRAW_BOTTOM;
extern float CAMERA_DRAW_LEFT;
extern float CAMERA_DRAW_RIGHT;

CParticle   *CParticleSystem::CreateParticle()
{
    if ( dead ) return NULL;

    if ( ParticleCounterCheck( counter_type ) )
	return NULL;

    //printf( "%6u %6u %6u\n", total_particles, phys_particles, weather_particles );
    
    size_t t = ParticlesActive;
    size_t i = t;
    CParticle& pi = particles[i];
    Vector2& p = pp[i];
    RGBAf& color = pcolor[i];
    {
        if (i >= MaxParticles)
            return NULL;
        if (ParticlesActive >= MaxParticles)
            return NULL;
        ParticlesActive++;

	ParticleCounterChange( counter_type, 1 );

        Vector2 emit_point = position;
        if ( emitter )
        {
            GameObject * emit_obj = GetGameObject( emitter );
            if ( emit_obj )
            {
                if ( mode & 8 )
                {
                    emit_point = emit_obj->aabb.p + this->position;
                }
                else if ( mode & 64 )
		{
			float dx = Random_Float( -3.0f * emit_obj->aabb.W, 3.0f * emit_obj->aabb.W );
			float dy = Random_Float( -3.0f * emit_obj->aabb.H, 3.0f * emit_obj->aabb.H );
	                emit_point = Vector2(emit_obj->aabb.p.x+dx, emit_obj->aabb.p.y+dy);
		}
		else
                {
                    float dx = Random_Float( -emit_obj->aabb.W, emit_obj->aabb.W );
                    float dy = Random_Float( -emit_obj->aabb.H, emit_obj->aabb.H );
                    emit_point = Vector2(emit_obj->aabb.p.x+dx, emit_obj->aabb.p.y+dy);
                }
            }
            else
            {
                this->dead = true;
		ParticlesActive--;
		ParticleCounterChange( counter_type, -1 );
                return NULL;
            }
        }
        if ( mode & 16 )
        {
            ObjDynamic* op = (ObjDynamic*)attached_object;
            float vx = 0;
            float vy = 0;
            if ( op )
            {
                vx += op->vel.x;
                vy += op->vel.y;
            }
            emit_point = Vector2( Random_Float( CAMERA_GAME_LEFT+vx-320, CAMERA_GAME_RIGHT+vx+320 ), CAMERA_GAME_TOP-startsize+sizevar+vy-240 );
            if ( emit_point.x > CAMERA_GAME_RIGHT || emit_point.x < CAMERA_GAME_LEFT )
                emit_point.y += Random_Float( -100.0f, (CAMERA_GAME_BOTTOM - CAMERA_GAME_TOP)+100.0f );
            else
                emit_point.y -= Random_Float( 0.0f, 100.0f );
        }
	if ( mode & 32 )
	{
		float left = CAMERA_DRAW_LEFT - cfg.scr_width / 2.0f;
		float right = CAMERA_DRAW_RIGHT + cfg.scr_width / 2.0f;
		if ( area != NULL && area->size() > 0 )
		{
			bool on_screen = false;
			for ( vector<CAABB>::iterator it = area->begin(); it != area->end(); it++ )
			{
				if ( !(it->Right() < CAMERA_DRAW_LEFT || it->Left() > CAMERA_DRAW_RIGHT) &&
				     !(it->Top() > CAMERA_DRAW_BOTTOM || it->Bottom() < CAMERA_DRAW_TOP) )
				{
					on_screen = true;
					break;
				}
			}
			if ( !on_screen )
			{
				//emit_point = area->begin()->p;
				ParticlesActive--;
				ParticleCounterChange( counter_type, -1 );
				return NULL;
			}
			else
			{
				size_t area_num = static_cast<size_t>(Random_Int(0, area->size()-1));
				const CAABB a = area->at(area_num);
				left = max(left, a.Left());
				right = min(right, a.Right());
				emit_point = Vector2( Random_Float( left, right ), CAMERA_GAME_TOP );
			}
		}
		else
		{
			emit_point = Vector2( Random_Float( left, right ), CAMERA_GAME_TOP - Random_Float( cfg.scr_height/4.0f, cfg.scr_height/2.0f ) );
			if ( emit_point.x > CAMERA_GAME_RIGHT || emit_point.x < CAMERA_GAME_LEFT )
				emit_point.y += Random_Float( -100.0f, (CAMERA_DRAW_BOTTOM - CAMERA_DRAW_TOP)+100.0f );
		}
	}
	if ( mode & 64 && !emitter )
	{
		float dx = Random_Float( -64.0f, 64.0f );
		float dy = Random_Float( -64.0f, 64.0f );
                emit_point = Vector2(position.x+dx, position.y+dy);
	}
        pi.life  = Random_Int(plife ,plife + plifevar);
        if (pi.life < 0 )
            pi.life = plife;
        pi.age = 0;
        pi.parameter = Random_Float( min_param, max_param );

 /*       if ( area != NULL && area->size() > 0 )
        {
            bool in_area = false;
            for ( vector<CAABB>::iterator it = area->begin(); it != area->end(); it++ )
            {
                if ( it->PointInCAABB( emit_point.x, emit_point.y ) )
                {
                    in_area = true;
                    break;
                }
            }
            if (!in_area)
                return NULL;
        }
*/
        if (geom != NULL && GeomNumPoints > 1)
        {
            int sr = Random_Int(0, GeomNumPoints-2);
            p = geom[sr] + (geom[sr+1]-geom[sr])*Random_Float(0.0f, 1.0f);
            float angle = Const::Math::PI * Random_Float(min_angle, max_angle) / 180;
            float speed = Random_Float(max_speed, max_speed);
            Vector2 norm = (geom[sr]-geom[sr+1]).GetPerpendicular();
            if ( norm.Length() > 0 )
                norm = norm.Normalized();
            else
                norm = Vector2();
            pi.v = norm * speed;
            pi.v.x *= cos(angle);
            pi.v.y *= sin(angle);
        }
        else
        {
            p = emit_point;
            float angle = Const::Math::PI * Random_Float(min_angle, max_angle) / 180;
            float speed = Random_Float(min_speed, max_speed);
            if ( trajectory == pttPseudoDepth )
            {
                float angle2 = Const::Math::PI * Random_Float( -t_param1, t_param1 ) / 180;
                pi.parameter = speed * sin(angle2);
                pi.v = Vector2( speed * cos(angle) * cos(angle2), speed * sin(angle) * cos(angle2) );
            }
            else
            {
                pi.v = Vector2( speed * cos(angle), speed*sin(angle) );
            }
        }
        pi.old_p = p;

        pi.size = Random_Float(startsize, startsize + sizevar);
        pi.dsize = 5*(endsize - startsize) / pi.life;

        color.a = Random_Float(sc.a ,sc.a + vc.a);
        color.r = Random_Float(sc.r ,sc.r + vc.r);
        color.g = Random_Float(sc.g ,sc.g + vc.g);
        color.b = Random_Float(sc.b ,sc.b + vc.b);

        pi.dc = (ec - color)/(pi.life/5.0f);
        pi.trace = Random_Float(min_trace, max_trace);
        pi.is_on_plane = false;
		pi.bounces = 0;

        pz[i] = system_z;
		extra[i] = -1;

#ifdef SELECTIVE_RENDERING
		updateLTRB(p, pi.size);
#endif
	}
    return &pi;
}



CParticleSystem::~CParticleSystem()
{
	ParticleCounterChange( counter_type, -static_cast<int>(ParticlesActive) );

	if ( area ) DELETESINGLE(area);
	if ( drop_area ) DELETESINGLE(drop_area);
	if ( geom ) DELETEARRAY(geom);

	if ( particles ) DELETEARRAY(particles);
	if ( pp ) DELETEARRAY(pp);
	if ( pz ) DELETEARRAY(pz);
	if ( pcolor ) DELETEARRAY(pcolor);

	if ( tex_coords ) DELETEARRAY(tex_coords);
	if ( fsizes ) DELETEARRAY(fsizes);
	if ( extra ) DELETEARRAY(extra);
}

void CParticleSystem::LoadFromProto( const Proto* proto )
{
    extern ResourceMgr<Texture> * textureMgr;

    sc = proto->start_color;
    ec = proto->end_color;
    vc = proto->var_color;

    texture = textureMgr->GetByName(proto->texture);
	blendingMode = static_cast<BlendingMode>(proto->blendingMode);

    emission = proto->emission;
    MaxParticles = (size_t)proto->max_particles;
    lifemin = (int)proto->particle_life_min;
    lifemax = (int)proto->particle_life_max;

    min_speed = proto->min_speed;
    max_speed = proto->max_speed;
    min_angle = proto->min_angle;
    max_angle = proto->max_angle;
    min_param = proto->min_param;
    max_param = proto->max_param;
    min_trace = proto->min_trace;
    max_trace = proto->max_trace;
    trace_width = proto->trace_width * 0.5f;
    if ( trace_width <= 0.0f )
    {
	trace_width = 0.5f;
	min_trace = 0.0f;
	max_trace = 0.0f;
    }
    trajectory = (TrajectoryType)proto->trajectory;
    t_param1 = proto->trajectory_param1;
    t_param2 = proto->trajectory_param2;
    wind_affected = proto->affected_by_wind == 1;
    gravity = Vector2( proto->gravity_x, proto->gravity_y );

    startsize = proto->start_size;
    sizevar = proto->size_variability;
    endsize = proto->end_size;

    plife = (int)proto->particle_life * 5;
    plifevar = (int)proto->particle_life_var * 5;

    life = (int)proto->system_life;
    life *= life == -1 ? 1 : 5;
}
