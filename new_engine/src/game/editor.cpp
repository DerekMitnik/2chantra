#include "StdAfx.h"

#ifdef MAP_EDITOR

#include "object_mgr.h"
#include "objects/object_physic.h"
#include "objects/object_sprite.h"
#include "objects/object_enemy.h"
#include "objects/object_item.h"
#include "objects/object_character.h"
#include "objects/object_spawner.h"
#include "objects/object_ribbon.h"
#include "../script/api.h"

#include "editor.h"
editor::EditorStates editor_state = editor::EDITOR_MAP_EDITOR;

Vector2 grid = Vector2(0, 0);
RGBAf grid_color = RGBAf( 0.0f, 0.0f, 0.0f, 1.0f );

void editor::SetEditorState(editor::EditorStates state)
{
	editor_state = state;
}

#include <set>
#include "../script/script.h"

#include "phys/sap/OPC_ArraySAP.h"
#include "phys/phys_collisionsolver.h"

#include <map>

bool editor_ShowBorders = false;

extern lua_State* lua;
extern Opcode::ArraySAP *asap;

extern std::map<int,GameObject*>* ObjTree;
extern std::map<int,GameObject*>* AddTree;

UINT editorAreaSelectorObject = 0;
bool editorAreaSelectNeeded = false;
bool editorPhysicProcessed = false;
set<UINT> editorObjectsInArea;
LuaRegRef editorAreaSelectProc = LUA_NOREF;


//////////////////////////////////////////////////////////////////////////

EditorObject::EditorObject()
{
	really_physic = false;
	proto_name = NULL;
	border_color = RGBAf(1.0f, 0.5f, 0.0f, 1.0f);
	show_border = false;
	grouped = false;
	removed = false;
	from_proto = false;
	min_difficulty = 0;
	max_difficulty = 2;
	creation_shift = Vector2(0, 0);
	link = incoming_links = NULL;
}

EditorObject::~EditorObject()
{
	if ( proto_name )
		DELETEARRAY(proto_name);
	if ( link )
		DELETESINGLE(link);
	if ( incoming_links )
		DELETESINGLE(incoming_links);
}

void EditorObject::SetLink( GameObject* obj, GameObject* target )
{
	if ( target == NULL )
	{
		if ( link )
			link->childDead( obj->id );
		link = NULL;
		return;
	}
	if ( link )
		link->childDead( obj->id );
	if ( !target->editor.incoming_links ) target->editor.incoming_links = new ObjectConnection( target, obj );
	link = target->editor.incoming_links;
	link->children.push_back( obj ); 
}

void EditorObject::SetProtoName(const char* name)
{
	if ( proto_name )
		DELETEARRAY(proto_name);
	proto_name = new char[ strlen(name) + 1 ];
	strcpy( proto_name, name );
}
 
void EditorObject::SetCreationShift(Vector2 shift)
{
	creation_shift = shift;
}

void EditorObject::SetGrouped(bool b /*= true*/)
{
	grouped = b;
}

void EditorObject::SetFromProto(bool b /*= true*/)
{
	from_proto = b;
}

//////////////////////////////////////////////////////////////////////////

void EditorGetObjects(CAABB area)
{
	if ( editor_state != editor::EDITOR_OFF )
	{
		if (editorAreaSelectNeeded)
			return;
		
		editorObjectsInArea.clear();
		
		if ( AddTree != NULL )
		{
			for(std::map<int,GameObject*>::iterator ObjIter = AddTree->begin(); ObjIter != AddTree->end(); ObjIter++)
			{
				if ( CollideAABB(ObjIter->second->aabb, area) )
				{
					AddObjectInArea(ObjIter->second->id);
				}
			}
		}
		
		if (ObjTree != NULL)
		{
			for(std::map<int,GameObject*>::iterator ObjIter = ObjTree->begin(); ObjIter != ObjTree->end(); ObjIter++)
			{
				if ( CollideAABB(ObjIter->second->aabb, area) )
				{
					AddObjectInArea(ObjIter->second->id);
				}
			}
		}
		
		GameObject* objselect = new ObjPhysic;
		objselect->aabb = area;
		objselect->SetPhysic();
		AddObject(objselect);
		editorAreaSelectorObject = objselect->id;
		editorAreaSelectNeeded = true;
		editorPhysicProcessed = true;
	}
	else
	{
		UNUSED_ARG(area);
	}
}


void ProcessEditor()
{
	if ( editor_state != editor::EDITOR_OFF && editorAreaSelectNeeded && editorPhysicProcessed)
	{
		lua_createtable(lua, 0, 2);	// ����: t
		int i = 1;
		for (set<UINT>::iterator it = editorObjectsInArea.begin(); it != editorObjectsInArea.end(); it++, i++)
		{
			lua_pushinteger(lua, *it);
			lua_rawseti(lua, -2, i);
		}
		
		if (SCRIPT::ExecChunkFromReg(editorAreaSelectProc, 1) < 0)
		{
			SCRIPT::ReleaseProc(&editorAreaSelectProc);
		}
		
		GameObject* obj = GetGameObject(editorAreaSelectorObject);
		if (obj) obj->SetDead();
		editorAreaSelectorObject = 0;
		editorAreaSelectNeeded = false;
		editorObjectsInArea.clear();
		editorPhysicProcessed = false;
	}
}

void AddObjectInArea(UINT id)
{
	if ( editor_state != editor::EDITOR_OFF )
	{
		editorObjectsInArea.insert(id);
	}
	else
	{
		UNUSED_ARG(id);
	}
}

void EditorRegAreaSelectProc(lua_State* L)
{
	if ( editor_state != editor::EDITOR_OFF )
	{
		SCRIPT::RegProc(L, &editorAreaSelectProc, 1);
	}
	else
	{
		UNUSED_ARG(L);
	}
}

int EditorCopyObject(lua_State* L)
{
	if ( editor_state != editor::EDITOR_OFF )
	{
		luaL_argcheck(L, lua_isnumber(L, 1), 1, "Must be object id");
		luaL_argcheck(L, lua_isnumber(L, 2), 2, "Must be coordinate x of object copy");
		luaL_argcheck(L, lua_isnumber(L, 3), 3, "Must be coordinate y of object copy");
		GameObject* obj = GetGameObject( static_cast<UINT>(lua_tointeger(L, 1)) );
		float x = static_cast<float>(lua_tonumber(L, 2));
		float y = static_cast<float>(lua_tonumber(L, 3));
		if ( !obj ) return 0;
		switch( obj->type )
		{
		case objSprite:
			{
				if ( !obj->editor.proto_name )
				{
					CAABB new_box = CAABB( x - obj->aabb.W, y - obj->aabb.H, x + obj->aabb.W, y + obj->aabb.H );
					lua_pushinteger(L, CreateColorBox( new_box, obj->sprite->z, obj->sprite->color )->id );
				}
				else
				{
					if ( obj->editor.grouped )
					{
						GameObject* group = CreateSprite( obj->editor.proto_name, Vector2(x, y), false, NULL );
						GameObject* add = CreateSprite( obj->editor.proto_name, Vector2(x+2*(obj->aabb.W-group->aabb.W), y+2*(obj->aabb.H-group->aabb.H)), false, NULL );
						GroupObjects( group->id, add->id );
						lua_pushinteger(L, group->id );
					}
					else
					{
						GameObject* go = CreateSprite( obj->editor.proto_name, Vector2(x, y), false, NULL );
						if ( obj->sprite && go->sprite ) go->SetAnimation( obj->sprite->cur_anim, false );
						lua_pushinteger(L, go->id );
					}
				}
				return 1;
			}
		case objTile:
			{
				GameObject* go = CreateSprite( obj->editor.proto_name, Vector2(x, y), false, NULL );
				/*
				go->sprite->animsCount = 0;
				go->sprite->currentFrame = obj->sprite->currentFrame;
				go->aabb.W = obj->aabb.W;
				go->aabb.H = obj->aabb.H;
				go->type = objTile;
				if ( go->sprite ) go->sprite->z = obj->sprite->z;*/
				go->sprite->SetCurrentFrame(obj->sprite->currentFrame);
				go->sprite->animsCount = 0;
				go->aabb.W  = go->sprite->frameWidth / 2.0f;
				go->aabb.H  = go->sprite->frameHeight / 2.0f;
				go->sprite->z = obj->sprite->z;
				go->type = objTile;
				lua_pushinteger(L, go->id );
				return 1;
			}
		case objEnemy:
			{
				lua_pushinteger(L, CreateEnemy( obj->editor.proto_name, Vector2(x, y), NULL )->id );
				return 1;
			}
		case objItem:
			{
				if ( obj->editor.grouped )
				{
					GameObject* group = CreateItem( obj->editor.proto_name, Vector2(x, y), NULL );
					GameObject* add = CreateItem( obj->editor.proto_name, Vector2(x+2*(obj->aabb.W-group->aabb.W), y+2*(obj->aabb.H-group->aabb.H)), NULL );
					GroupObjects( group->id, add->id );
					lua_pushinteger(L, group->id );
				}
				else
					lua_pushinteger(L, CreateItem( obj->editor.proto_name, Vector2(x, y), NULL )->id );
				return 1;
			}
		case objSpawner:
			{
				ObjSpawner* os = (ObjSpawner*)obj;
				lua_pushinteger(L, CreateSpawner(Vector2(x, y), os->editor.proto_name, os->maximumEnemies, os->enemySpawnDelay, os->direction, os->size, os->respawn_dist, os->respawnDelay, os->spawnLimit )->id );
				return 1;
			}
		case objRibbon:
			{
				ObjRibbon* orb = (ObjRibbon*)obj;
				ObjRibbon* nor = CreateRibbon( orb->editor.proto_name, orb->k, Vector2(x, y), orb->sprite->z, orb->editor.from_proto );
				nor->setBounds( orb->bl, orb->bt, orb->br, orb->bb );
				nor->setBoundsUsage( orb->ubl, orb->ubt, orb->ubr, orb->ubb );
				nor->setRepitition( orb->repeat_x, orb->repeat_y );
				lua_pushinteger(L, nor->id);
				return 1;
			}
		default:
			break;
		}
		return 0;
	}
	else
	{
		UNUSED_ARG(L);
		return 0;
	}
}

int EditorDumpMap(lua_State* L)
{
	if ( editor_state != editor::EDITOR_OFF )
	{
		int size = 0;
		if( ObjTree != NULL )
			size += static_cast<int>(ObjTree->size());
		if( AddTree != NULL )
			size += static_cast<int>(AddTree->size());

		lua_createtable(L, size, 0);	//table

		GameObject* obj;
		int obj_num = 1;

		if ( AddTree != NULL )
		{
			for(std::map<int,GameObject*>::iterator ObjIter = AddTree->begin(); ObjIter != AddTree->end(); ObjIter++)
			{
				obj = ObjIter->second;
				PushObject( L, obj );	//table obj
				lua_rawseti( L, -2, obj_num ); //table
				obj_num++;
			}
		}

		if (ObjTree != NULL)
		{
			for(std::map<int,GameObject*>::iterator ObjIter = ObjTree->begin(); ObjIter != ObjTree->end(); ObjIter++)
			{
				obj = ObjIter->second;
				PushObject( L, obj );	//table obj
				lua_rawseti( L, -2, obj_num ); //table
				obj_num++;
			}
		}

		return 1;
	}
	else
	{
		UNUSED_ARG(L);
		return 0;
	}
}

int EditorResize(lua_State* L)
{
	if ( editor_state != editor::EDITOR_OFF )
	{
		GameObject* obj = GetGameObject( static_cast<UINT>(lua_tointeger(L, 1)) );
		int axis = static_cast<int>(lua_tointeger(L, 2));
		int align = static_cast<int>(lua_tointeger(L, 3));
		int ammount = static_cast<int>(lua_tointeger(L, 4));
		if ( axis == 0 )	//�����, ����� ���������, � �� ���� �������. �� ��� ����� �����.
		{
			if ( obj->aabb.W + ammount/2.0 < 0.5f ) return 0;
			if ( align == -1 )
				obj->aabb.p.x += ammount/2.0f;
			if ( align == 1 )
			{
				ammount = -ammount;
				obj->aabb.p.x -= ammount/2.0f;
			}
			obj->aabb.W += ammount/2.0f;
		}
		else
		{
			if ( obj->aabb.H + ammount/2.0 < 0.5f ) return 0;
			if ( align == -1 )
				obj->aabb.p.y += ammount/2.0f;
			if ( align == 1 )
			{
				ammount = -ammount;
				obj->aabb.p.y -= ammount/2.0f;
			}
			obj->aabb.H += ammount/2.0f;
		}
		obj->editor.SetGrouped();
		if ( obj->sprite != NULL )
		{
			obj->sprite->renderMethod = rsmRepeatXY;
		}
		return 0;
		}
	else
	{
		UNUSED_ARG(L);
		return 0;
	}
}

#endif // MAP_EDITOR
