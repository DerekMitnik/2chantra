#include "StdAfx.h"

#include "object_physic.h"

bool SolidTo( ObjPhysic* obj, ObjectType type )
{
	switch (type)
	{
		case objPlayer:
			return (obj->solid_to & physPlayer) != 0;
		case objRay:
		case objBullet:
			return (obj->solid_to & physBullet) != 0;
		case objEffect:
			return (obj->solid_to & physEffect) != 0;
		case objSprite:
			return (obj->solid_to & physSprite) != 0;
		case objItem:
			return (obj->solid_to & physItem) != 0;
		case objEnemy:
			return (obj->solid_to & physEnemy) != 0;
		case objEnvironment:
			return (obj->solid_to & physEnvironment) != 0;
		case objParticleSystem:
			return (obj->solid_to & physParticles) != 0;
		default:
			return true;
	}
}