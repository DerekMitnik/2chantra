#include "StdAfx.h"

#include "object_item.h"
#include "object.h"

#include "../object_mgr.h"

#include "../../misc.h"
#include "../../resource_mgr.h"

//////////////////////////////////////////////////////////////////////////

extern ResourceMgr<Proto> * protoMgr;

//////////////////////////////////////////////////////////////////////////

void ObjItem::Touch( ObjDynamic* obj )
{
	//������� ��������� �������� ������ �� ���, ��� ���� ���� ���������, ������ ��������� �� �����.
	if ( obj->type == objItem || obj->IsBullet() || obj->activity == oatDying || obj->IsDead() ) return;
	this->sprite->stack->Push( obj->id );
	this->SetAnimation( "touch", false );
}

void ObjItem::Process()
{
	this->aabb.p += this->vel;
}

ObjItem* CreateItem(const char* proto_name, Vector2 coord, const char* start_anim, bool corner_adjust)
{
	const Proto* proto = protoMgr->GetByName( proto_name, "items/" );
	if (!proto)
		return NULL;

	ObjItem* item = new ObjItem();

	if (!item->ApplyProto(proto))
	{
		DELETESINGLE(item);
		return NULL;
	}

	item->aabb.p = coord;


	// TODO: ������ ���. ��� �������� � ���������� � ���������. �� ����� ������ �� ��������.
	// � ���� ������ ������� � SAP ��� �� ��������, ������� � SetAnimation ������ ��� ���������
	// ��� ��������� � SAP.
	item->ClearPhysic();
	item->SetAnimation(start_anim ? start_anim : "init", true);
	item->SetPhysic();

	ObjPhysic* op = (ObjPhysic*)item;
	op->rectangle[0] = Vector2(-op->aabb.W, -op->aabb.H);
	op->rectangle[1] = Vector2( op->aabb.W, -op->aabb.H);
	op->rectangle[2] = Vector2( op->aabb.W,  op->aabb.H);
	op->rectangle[3] = Vector2(-op->aabb.W,  op->aabb.H);
	op->rectangle.CalcBox();

#ifdef COORD_LEFT_UP_CORNER
	if (corner_adjust)
	{
		item->aabb.p.x += item->sprite->frameWidth * 0.5f;
		item->aabb.p.y += item->sprite->frameHeight * 0.5f;
	}
#else
	USUSED_ARG( corner_adjust );
#endif // COORD_LEFT_UP_CORNER

	AddObject(item);

#ifdef MAP_EDITOR
	item->editor.SetProtoName(proto_name);
	item->editor.SetCreationShift(item->aabb.p - coord);
#endif //MAP_EDITOR

	return item;
}

ObjItem* CreateItem(const char* proto_name, Vector2 coord, const char* start_anim, CAABB size, bool corner_adjust)
{
	ObjItem* item = CreateItem(proto_name, coord, start_anim, corner_adjust);
	if ( item )
	{
		item->old_aabb = item->aabb;
		item->aabb = size;
		if ( item->sprite )
		{
			item->sprite->setRenderMethod(rsmRepeatXY);
		}
#ifdef MAP_EDITOR
		item->editor.SetGrouped();
#endif //MAP_EDITOR
	}
	return item;
}

ObjItem* CreateDummyItem()
{
	ObjItem* obj = new ObjItem;
	AddObject(obj);
	return obj;
}