#include "StdAfx.h"
#include "object_ribbon.h"
#include "../camera.h"
#include "../object_mgr.h"
#include "../sprite.h"
#include "../../render/texture.h"
#include "../../resource_mgr.h"
#include "../../config.h"
#include "../editor.h"

extern float CAMERA_DRAW_BOTTOM;
extern float CAMERA_DRAW_TOP;
extern float CAMERA_DRAW_LEFT;
extern float CAMERA_DRAW_RIGHT;
extern float CAMERA_X;
extern float CAMERA_Y;
extern ResourceMgr<Proto> * protoMgr;
extern ResourceMgr<Texture> * textureMgr;
#ifdef MAP_EDITOR
extern editor::EditorStates editor_state;
#endif // MAP_EDITOR

float ObjRibbon::getBound( ORBoundSide side )
{
	switch (side)
	{
	case orBoundLeft:
		return ubl ? max( bl + k.x*CAMERA_X, CAMERA_DRAW_LEFT ) : CAMERA_DRAW_LEFT;
	case orBoundRight:
		return ubr ? min( br + k.x*CAMERA_X, CAMERA_DRAW_RIGHT ) : CAMERA_DRAW_RIGHT;
	case orBoundTop:
		return ubt ? max( bt + k.y*CAMERA_Y, CAMERA_DRAW_TOP ) : CAMERA_DRAW_TOP;
	case orBoundBottom:
		return ubb ? min( bb + k.y*CAMERA_Y, CAMERA_DRAW_BOTTOM ) : CAMERA_DRAW_BOTTOM;
	default:
		assert(false);
		return 0;
	}
}

extern config cfg;

#ifdef SELECTIVE_RENDERING
bool ObjRibbon::GetDrawAABB(CAABB& res) const
{
	const float minf = -10000000;
	const float maxf =  10000000;

	if (!sprite)
		return false;

	float l = ubl ? bl : minf;
	float r = ubr ? br : maxf;
	float t = ubt ? bt : minf;
	float b = ubb ? bb : maxf;

	res = CAABB(l, t, r, b);
	return true;
}
#endif

void ObjRibbon::setSpacing( float x, float y )
{
	if ( x >= 0.0f ) spacing.x = x;
	if ( y >= 0.0f ) spacing.y = y;
}

void ObjRibbon::Draw()
{
	//������ ���� �� ������, �� ���� �����...
	if ( this->sprite == NULL )
		return;

#ifdef MAP_EDITOR
	if ( editor_state != editor::EDITOR_OFF )
	{
		coord2f_t crd = coord2f_t(2*this->aabb.W, 2*this->aabb.H);
		RenderFrameRotated(this->aabb.Left(), this->aabb.Top(), 0.8f, &crd, NULL, NULL, this->editor.editor_color, 0.0f, aabb.p, bmSrcA_OneMinusSrcA);
		if ( this->editor.show_border )
		{
			RenderLine( CAMERA_DRAW_LEFT, getBound(orBoundTop), CAMERA_DRAW_RIGHT, getBound(orBoundTop), 1, RGBAf(1, 0, 0, 1));
			RenderLine( CAMERA_DRAW_LEFT, getBound(orBoundBottom), CAMERA_DRAW_RIGHT, getBound(orBoundBottom), 1, RGBAf(0, 1, 0, 1));
			RenderLine( getBound(orBoundLeft), CAMERA_DRAW_TOP, getBound(orBoundLeft), CAMERA_DRAW_BOTTOM, 1, RGBAf(0, 0, 1, 1));
			RenderLine( getBound(orBoundRight), CAMERA_DRAW_TOP, getBound(orBoundRight), CAMERA_DRAW_BOTTOM, 1, RGBAf(1, 1, 0, 1));
		}
	}
#endif //MAP_EDITOR

	UINT framenum = this->sprite->getCurrentFrame();
	FrameInfo* f = (this->sprite->tex->frames->frame + framenum);
	this->sprite->frameWidth = (USHORT)f->size.x;
	this->sprite->frameHeight = (USHORT)f->size.y;

	//���������, ������ �� ������� �� ����� � �������.
	if ( ubr && getBound(orBoundRight) < CAMERA_DRAW_LEFT )
		return;
	if ( ubl && getBound(orBoundLeft) > CAMERA_DRAW_RIGHT )
		return;
	if ( ubb && getBound(orBoundBottom) < CAMERA_DRAW_TOP )
		return;
	if ( ubt && getBound(orBoundTop) > CAMERA_DRAW_BOTTOM )
		return;

	Vector2 coord = aabb.p + Vector2( k.x*CAMERA_X, k.y*CAMERA_Y ) - Vector2( aabb.W, aabb.H );
	coord -= Vector2( this->sprite->getRealX(), this->sprite->getRealY() );
	if ( !repeat_x && (coord.x + sprite->frameWidth < getBound(orBoundLeft) || coord.x > getBound(orBoundRight)) )
		return;
	if ( !repeat_y && (coord.y + sprite->frameHeight < getBound(orBoundTop) || coord.y > getBound(orBoundBottom)) )
		return;
	//���� ���� ������ �� ������� - ����� ��������� �� ������.
	if ( !repeat_x && (coord.x + this->sprite->frameWidth < CAMERA_DRAW_LEFT || coord.x - this->sprite->frameWidth > CAMERA_DRAW_RIGHT) )
		return;
	if ( !repeat_y && (coord.y + this->sprite->frameHeight < CAMERA_DRAW_TOP || coord.y - this->sprite->frameHeight > CAMERA_DRAW_BOTTOM) )
		return;

	//"����������" �� ��������� ���������� �� �����������, �� ������� ���������.
	if ( repeat_x )
	{
		while ( coord.x + this->sprite->frameWidth < getBound(orBoundRight) )
			coord.x += this->sprite->frameWidth + spacing.x;
		while ( coord.x + this->sprite->frameWidth > getBound(orBoundLeft) )
			coord.x -= this->sprite->frameWidth + spacing.x;
	}
	if ( repeat_y  )
	{
		while ( coord.y + this->sprite->frameHeight < getBound(orBoundBottom) )
			coord.y += this->sprite->frameHeight + spacing.y;
		while ( coord.y + this->sprite->frameHeight > getBound(orBoundTop) )
			coord.y -= this->sprite->frameHeight + spacing.y;
	}
	Vector2 bs = Vector2( max(getBound(orBoundLeft) - coord.x - this->sprite->frameWidth, 0.0f), max(getBound(orBoundTop) - coord.y - this->sprite->frameHeight, 0.0f) );
	if ( repeat_x ) coord.x = getBound(orBoundLeft);
	if ( repeat_y ) coord.y = getBound(orBoundTop);
	if ( !repeat_x && coord.x < getBound(orBoundLeft) )
	{
		bs.x += getBound(orBoundLeft) - coord.x;
		coord.x = getBound(orBoundLeft);
	}

	Vector2 edge = Vector2( getBound(orBoundRight), getBound(orBoundBottom) );

	if (coord.x != edge.x && coord.y != edge.y)
	{
		assert(coord.x < edge.x);
		assert(coord.y < edge.y);

		coord.x = floor( coord.x + 0.5f );
		coord.y = floor( coord.y + 0.5f );
	
		RenderFrameCyclic(coord, this->sprite->z, edge, bs, f, this->sprite->tex, this->sprite->color, this->sprite->IsMirrored(), repeat_x, repeat_y, spacing.x, spacing.y);
	}
}

void ObjRibbon::setBounds( float x1, float y1, float x2, float y2 )
{
	bl = x1;
	bt = y1;
	br = x2;
	bb = y2;

#ifdef SELECTIVE_RENDERING
	selectForRenederUpdate(this);
#endif
}

void ObjRibbon::setBoundsUsage( bool x1, bool y1, bool x2, bool y2 )
{
	ubl = x1;
	ubr = x2;
	ubt = y1;
	ubb = y2;

#ifdef SELECTIVE_RENDERING
	selectForRenederUpdate(this);
#endif
}

void ObjRibbon::setRepitition(bool x, bool y)
{
	repeat_x = x;
	repeat_y = y;
}

ObjRibbon* CreateRibbon( const char* texture, Vector2 k, Vector2 coord, float z, bool from_proto )
{
	ObjRibbon* ribbon = new ObjRibbon();
	ribbon->k = k;
	ribbon->aabb.p = coord;
	ribbon->aabb.W = 16;
	ribbon->aabb.H = 16;
	if ( from_proto )
	{
		if (ribbon->ApplyProto(protoMgr->GetByName(texture, "sprites/")))
		{
			ribbon->SetAnimation( "idle", false );
		}
		else
		{
			Log(DEFAULT_LOG_NAME, logLevelError, "Error creating ribbon %s, proto cannot be applied", texture);
			DELETESINGLE(ribbon);
			return NULL;
		}
	}
	else
	{
		ribbon->sprite = new Sprite( texture );
	}
	ribbon->sprite->z = z;

#ifdef MAP_EDITOR
	ribbon->editor.SetFromProto(from_proto);
	ribbon->editor.SetProtoName(texture);
	ribbon->editor.SetCreationShift(ribbon->aabb.p - coord);
#endif //MAP_EDITOR

	AddObject( ribbon );

	return ribbon;
}

ObjRibbon* CreateDummyRibbon()
{
	ObjRibbon* obj = new ObjRibbon();
	AddObject(obj);
	return obj;
}
