#include "StdAfx.h"
#include "object_character.h"
#include "object_player.h"
#include "../../misc.h"

map<int, GameObject*> factions;

void ObjCharacter::Process()
{
	if ( IsOnPlane() )
	{
		if ( on_plane_reserve < 0 )
		{
			on_plane_reserve = 0;
		}
		else
		{
			on_plane_reserve = 10;
		}
	}
	else if ( on_plane_reserve > 0 )
	{
		on_plane_reserve--;
		SetOnPlane();
	}
}

void ObjCharacter::ProcessShooting()
{
	if (this->IsSleep())
		return;

	if ( abs(target_weapon_angle - weapon_angle) > 0.1f )
	{
		weapon_angle += (target_weapon_angle - weapon_angle) * aiming_speed;
	}
	else
	{
		weapon_angle = target_weapon_angle;
	}

	if (!this->cur_weapon)
		return;

	if ( this->sprite->mpCount < 1 && this->activity == oatShooting )
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "Attempt to shoot without a mount point");
		return;
	}

	switch (this->shootingBeh)
	{
	case csbFreeShooting:
		if (this->activity == oatShooting && (this->type != objPlayer || ((ObjPlayer*)this)->HasAmmo() ))
		{
			if (this->type == objPlayer)
			{
				ObjPlayer* op = static_cast<ObjPlayer*>(this);
				if ( op->alt_fire )
				{
					if (!op->alt_weapon) break;
					op->cur_weapon = op->alt_weapon;
				}
				op->SpendAmmo();
				if ( op->alt_fire )
				{
					if (this->sprite->IsMirrored()) this->cur_weapon->Fire(this, this->aabb.p+Vector2( -(this->sprite->mp[0].x), this->sprite->mp[0].y ));
					else this->cur_weapon->Fire(this, this->aabb.p+this->sprite->mp[0]);
					op->cur_weapon = op->weapon;
					break;
				}
			}
			if (this->sprite->IsMirrored()) this->cur_weapon->Fire(this, this->aabb.p+Vector2( -(this->sprite->mp[0].x), this->sprite->mp[0].y ));
			else this->cur_weapon->Fire(this, this->aabb.p+this->sprite->mp[0]);
		}
		break;
	case csbOnAnimCommand:
		if (this->type != objPlayer || ((ObjPlayer*)this)->HasAmmo() )
		{
			if ( type == objPlayer )
			{
				ObjPlayer* op = static_cast<ObjPlayer*>(this);
				if ( op->alt_fire )
				{
					if (!op->alt_weapon) break;
					op->cur_weapon = op->alt_weapon;
				}
				op->SpendAmmo();
				if ( op->alt_fire )
				{
					if (this->sprite->IsMirrored()) this->cur_weapon->Fire(this, this->aabb.p+Vector2( -(this->sprite->mp[0].x), this->sprite->mp[0].y ));
					else this->cur_weapon->Fire(this, this->aabb.p+this->sprite->mp[0]);
					op->cur_weapon = op->weapon;
					this->shootingBeh = csbNoShooting;
					break;
				}
			}
			if (this->sprite->IsMirrored()) this->cur_weapon->Fire(this, this->aabb.p+Vector2( -(this->sprite->mp[0].x), this->sprite->mp[0].y ));
			else this->cur_weapon->Fire(this, this->aabb.p+this->sprite->mp[0]);
		}
		this->shootingBeh = csbNoShooting;
		break;
	default: break;
	}
}

void ObjCharacter::InitFaction()
{
	map<int, GameObject*>::iterator it = factions.find( this->faction_id );
	if ( it != factions.end() )
	{
		it->second->childrenConnection->children.push_back(this);
		this->faction = it->second->childrenConnection;
	}
	else
	{
		GameObject* fact = new GameObject();
		//AddObject(fact);
		factions[this->faction_id] = fact;
		fact->childrenConnection = new ObjectConnection( fact );
		fact->childrenConnection->children.push_back( this );
		this->faction = fact->childrenConnection;
	}
}

GameObject* ObjCharacter::GetNearestCharacter( vector<int> factions_list )
{
	bool first = true;
	Vector2 dist;
	float mn = 0;
	GameObject* mobj = NULL;
	GameObject* obj;
	bool dying;
	map<int, GameObject*>::iterator foit;
	for (vector<int>::iterator fit = factions_list.begin(); fit != factions_list.end(); fit++)
	{
		foit = factions.find( (*fit) );
		if ( foit != factions.end() )
		{
			for (vector<GameObject*>::iterator oit = foit->second->childrenConnection->children.begin(); oit != foit->second->childrenConnection->children.end(); oit++)
			{
				obj = (*oit);
				dist = obj->aabb.p - this->aabb.p;
				dying = false;
				if ( obj->type == objPlayer || obj->type == objEnemy )
					dying = ((ObjCharacter*)obj)->activity == oatDying;
				if ( !dying && obj->sprite->IsVisible() && (first || dist.Length() < mn) )
				{
					first = false;
					mn = dist.Length();
					mobj = obj;
				}
			}
		}
	}
	return mobj;
}

bool ObjCharacter::IsEnemy( GameObject* obj )
{
	if ( obj->type != objEnemy && obj->type != objPlayer )
		return true; //��������� ���� ���� ����� ������.
	if ( this->type == objPlayer ) //������ ����� ���, � ������� ������� ������ 0 � �� ��� �� �������.
	{
		ObjCharacter* oen = (ObjCharacter*)obj;
		return ( oen->faction_id != faction_id && oen->faction_id > 0 );
	}
	vector<int>::iterator fit = find(  faction_hates.begin(), faction_hates.end(), ((ObjCharacter*)obj)->faction_id );
	if ( fit == faction_hates.end() )
		return false;
	return true;
}

void ClearFactions()
{
	for (map<int, GameObject*>::iterator it = factions.begin(); it != factions.end(); it++)
	{
		it->second->childrenConnection->parentDead();
		DELETESINGLE( it->second );
	}
	factions.clear();
}
