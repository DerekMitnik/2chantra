#ifndef __OBJECT_ENEMY_H_
#define __OBJECT_ENEMY_H_

#include "object_character.h"

#define FACTION_NEUTRAL 0
#define FACTION_PLAYER -1
#define FACTION_ENEMY 1

enum CharacterOffscreenBeh { cobNone, cobDie, cobAnim, cobSleep };

class ObjEnemy : public ObjCharacter
{
public:
	ObjectConnection* target;
	Vector2 aim;
	bool attached;
	UINT current_waypoint;
	int waypoint_mode;
	float waypoint_speed;
	float waypoint_speed_limit;
	Vector2 waypoint_start;
	UINT waypoint_global;

	CharacterOffscreenBeh offscreen_behavior;
	float offscreen_distance;

	virtual bool ApplyProto(const Proto* proto); 
	virtual void Process();
	virtual bool ReduceHealth( UINT damage );
	void Touch(ObjPhysic *obj);
	void ParentEvent( ObjectEventInfo info );
	void UpdateTarget( ObjCharacter* target );
	ObjCharacter* GetTarget();

	ObjEnemy( GameObject* parent = NULL ) :
		target(NULL),
		aim(0.0f, 0.0f),
		attached(false),
		current_waypoint(0),
		waypoint_mode(0),
		waypoint_speed(0.0f),
		waypoint_speed_limit(0.0f),
		waypoint_global(0),
		offscreen_behavior(cobNone),
		offscreen_distance(0.0f)
	{
		type = objEnemy;
		if ( parent ) parent->AddChild( this );
	}

	~ObjEnemy()
	{
		if ( target )
			target->childDead( this->id );
	}

protected:
	virtual CUData* createUData();
};

ObjEnemy* CreateEnemy(const char* proto_name, Vector2 coord, const char* start_anim, bool corner_adjust = true, GameObject* parent = NULL);
ObjEnemy* CreateDummyEnemy();
#endif // __OBJECT_ENEMY_H_
