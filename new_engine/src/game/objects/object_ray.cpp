#include "StdAfx.h"

#include "object_ray.h"

#include "object.h"
#include "object_character.h"
#include "object_bullet.h"
#include "object_sprite.h"

#include "../object_mgr.h"
#include "../phys/phys_collisionsolver.h"

//////////////////////////////////////////////////////////////////////////

extern ResourceMgr<Proto> * protoMgr;

ObjRay::ObjRay():
	first_frame_shift(0),
	shooter_type(objNone),
	player_num(0)
{
	Init();
	parentConnection = new ObjectConnection( NULL );
}

ObjRay::ObjRay( GameObject* shooter ):
	first_frame_shift(0),
	player_num(0)
{
	Init();
	if (shooter)
	{
		ObjectConnection::addChild( shooter, this );
		this->shooter_type = shooter->type;
	}
	else
	{
		parentConnection = new ObjectConnection( NULL );
		this->shooter_type = objNone;
	}
}

void ObjRay::Init()
{
	type = objRay;
	debug_color = RGBAf(1.0f, 1.0f, 1.0f, 1.0f);
	searchDistance = 0.0f;
	collisionsCount = 1;
	rayFlags = 0;
	next_shift_y = 0;
	damage = 0;
	charge = 0;
	damage_type = 0;
	multiple_targets = false;
	hurts_same_type = false;
	push_force = 0.0f;
	//SetUseCollisionCounter();
	timeToLive = 0;
	//SetUseTTLCounter();
	end_effect = NULL;
	hit_effect = NULL;
}

ObjRay::~ObjRay()
{
	DELETEARRAY(this->end_effect);
	DELETEARRAY(this->hit_effect);
}

bool ObjRay::ApplyProto(const Proto* proto)
{
	if (!GameObject::ApplyProto(proto))
		return false;

	if (proto->multiple_targets > 0)
	{
		SetUseCollisionCounter();
		collisionsCount = proto->multiple_targets;
	}

	first_frame_shift = proto->ray_first_frame_shift;

	next_shift_y = proto->next_shift_y;

	damage = proto->bullet_damage;
	damage_type = proto->damage_type;
	
	hurts_same_type = proto->hurts_same_type != 0;
	push_force = proto->push_force;
	
	if (proto->time_to_live > 0)
	{
		SetUseTTLCounter();
		timeToLive = proto->time_to_live;
	}
	
	end_effect = StrDupl(proto->end_effect);
	hit_effect = StrDupl(proto->hit_effect);

	return true;
}


ObjRay* CreateRay(float x1, float y1, float x2, float y2)
{
	ObjRay* ray = new ObjRay;
	
	CRay r(x1, y1, x2, y2);
	ray->setRay(r);

	AddObject(ray);

	return ray;
}

ObjRay* CreateRay(const Proto* proto, Vector2 coord, ObjCharacter* shooter, float angle)
{
	if (!proto)
		return NULL;
	
	float rang = -angle;	

	float x1 = coord.x + cos(rang);
	float y1 = coord.y + sin(rang);

	ObjRay* ray = new ObjRay( shooter );

	if (!ray->ApplyProto(proto))
	{
		DELETESINGLE(ray);
		return NULL;
	}
	
	{
		CRay r(coord.x, coord.y, x1, y1);
		ray->setRay(r); 
	}
	ray->aabb.p = coord;
	


	if (ray->sprite)
	{
		if ( (angle > Const::Math::PI/2.0f && angle < 3*Const::Math::PI/2.0f ) || (angle < -Const::Math::PI/2.0f && angle > -3*Const::Math::PI/2.0f ) )
			ray->sprite->SetMirrored();
	
		ray->SetAnimation("straight", true);
	}

	AddObject(ray);
	
	return ray;
}

ObjRay* CreateRay(const Proto* proto, Vector2 coord, ObjCharacter* shooter, WeaponDirection wd)
{
	if (!proto)
		return NULL;
	
	float x1 = coord.x;
	float y1 = coord.y;

	switch (wd)
	{
	case wdLeft: 
		x1 -= 1.0f;
		break;
	case wdRight:
		x1 += 1.0f;
		break;
	case wdUpLeft:
		x1 -= 1.0f;
		y1 -= 1.0f;
		break;
	case wdUpRight:
		x1 += 1.0f;
		y1 -= 1.0f;
		break;
	case wdDownLeft:
		x1 -= 1.0f;
		y1 += 1.0f;
		break;
	case wdDownRight:
		x1 += 1.0f;
		y1 += 1.0f;
		break;
	}

	ObjRay* ray = new ObjRay( shooter );

	if (!ray->ApplyProto(proto))
	{
		DELETESINGLE(ray);
		return NULL;
	}

	{
		CRay r(coord.x, coord.y, x1, y1);
		ray->setRay(r);
	}

	ray->aabb.p = coord;
	
	if (ray->sprite)
	{
		switch (wd)
		{
		case wdLeft:
			ray->sprite->SetMirrored();
		case wdRight:
			ray->SetAnimation("straight", true);
			break;
		
		
		case wdUpLeft:
			ray->sprite->SetMirrored();
		case wdUpRight:
			ray->SetAnimation("diagup", true);
			break;

		
		case wdDownLeft:
			ray->sprite->SetMirrored();
		case wdDownRight:
			ray->SetAnimation("diagdown", true);
			break;
		}
	}

	AddObject(ray);
	
	return ray;
}

ObjRay* CreateRay(const char* proto_name, Vector2 coord, ObjCharacter* shooter, WeaponDirection wd)
{
	if (!proto_name)
		return NULL;

	return CreateRay(protoMgr->GetByName(proto_name, "projectiles/"), coord, shooter, wd);
}

ObjRay* CreateRay(const char* proto_name, Vector2 coord, ObjCharacter* shooter, float angle)
{
	if (!proto_name)
		return NULL;
	
	return CreateRay(protoMgr->GetByName(proto_name, "projectiles/"), coord, shooter, (angle * Const::Math::PI)/180.0f);
}

ObjRay* CreateDummyRay()
{
	ObjRay* ray = new ObjRay;
	AddObject(ray);
	return ray;
}

//////////////////////////////////////////////////////////////////////////

void ObjRay::setRay(CRay const& r)
{ 
	if (ray != r)
	{
		ray = r;
#ifdef SELECTIVE_RENDERING
		selectForRenederUpdate(this);
#endif
	}
}


void ObjRay::Process()
{
	if (IsUseCollisionCounter() && collisionsCount == 0)
	{
		//this->SetDead();
		this->activity = oatDying;
	}

	if (IsUseTTLCounter())
	{
		if (timeToLive > 0)
			timeToLive--;
		else
			//this->SetDead();
			this->activity = oatDying;
	}

	if (this->activity == oatDying)
	{
		this->damage = 0;
		if (!this->sprite || this->sprite->IsAnimDone())
		{
			this->SetDead();
		}
	}
}

// ������ ��� ���������� ��������, � �������� ��� ������������� ������������, 
// �� �������� ������� ������ ��� �� ������������
bool ObjRay::CheckIntersection(ObjPhysic* obj)
{
	//���� ������� �� ������������ � ����������-�����������. ������� � ������ - �����������, �� ��� � ������� ������������.
	if (obj->IsOneSide())
		return false;

	if (!obj->IsBulletCollidable())
		return false;

	GameObject* shooter = this->parentConnection->getParent();
	if ( shooter && shooter == obj )
		return false;

	if (this->shooter_type == obj->type && !this->hurts_same_type)
		return false;

	return true;
}


bool ObjRay::Hit(ObjPhysic* obj, Vector2& pos, Vector2& norm)
{
	UNUSED_ARG( norm );
	if (this->IsDead() || (activity == oatDying/* && !this->IsUseCollisionCounter()*/) )
		return false;

	// ��� �������� ��� �� � �� �����, ��� ����� ��� ��� ����������
	//if (!this->CheckIntersection(obj))
	//	return false;
	assert(this->CheckIntersection(obj));

	if (obj->IsDynamic())
	{
		if (  obj->sprite->GetAnimation("pain") )
		{
			// TODO: ��������� ����
			//long double ang = atan( fabs(this->aabb.p.y - obj->aabb.p.y)/fabs(this->aabb.p.x - obj->aabb.p.x) );
			//Vector2 push = Vector2( push_force*cos(ang), push_force*sin(ang) );
			//if ( (push.x < 0 && this->aabb.p.x < obj->aabb.p.x) || (push.x > 0 && this->aabb.p.x > obj->aabb.p.x) ) push.x *= -1;
			//if ( (push.y < 0 && this->aabb.p.y < obj->aabb.p.y) || (push.y > 0 && this->aabb.p.y > obj->aabb.p.y) ) push.y *= -1;
			//((ObjDynamic*)obj)->vel += push;
		}
		switch (obj->type)
		{
		case objItem:
			{
				//����� �������� ��������, �� ���� - ����� ����������
				if ( obj->sprite->cur_anim != "die" )
					obj->SetAnimation("die", true);
			}
			break;
		case objPlayer:
		case objEnemy:
			{
				ObjCharacter* oc = static_cast<ObjCharacter*>(obj);
				oc->last_hit_from = Vector2( pos );
				oc->ReceiveDamage(this->damage, this->damage_type, this->player_num);
				break;
			}
		case objBullet:
			{
				if (obj->parentConnection == this->parentConnection) return false;
				ObjBullet* ob = static_cast<ObjBullet*>(obj);
				if (!ob->multiple_targets) ob->activity = oatDying;
				else ob->activity = oatUndead;
				break;
			}
		default: break;
		}

		//if (!multiple_targets) this->activity = oatDying;
		//else this->activity = oatUndead;
	}
	else
	{
		this->collisionsCount = 0;
		/*
		Vector2 c;
		Vector2 n;
		GetIntersection(ray, *obj->geometry, obj->aabb.p, c, n );

		CreateRay(protoMgr->GetByName("weapons/pear15ray_primary"),c-n,(ObjCharacter*)parentConnection->getParent(),-2*atan2(n.y,n.x) - 3.1415926 + atan2(ray.r.y,ray.r.x));
		*/
		//this->activity = oatDying;
	}

	const bool the_end = (!IsUseCollisionCounter()) || ( this->collisionsCount <= 1 );
	if ( ((!the_end && hit_effect) || (the_end && end_effect) ) && this->activity != oatDying)
	{
		
		GameObject* spr = CreateSprite( the_end ? end_effect : hit_effect, pos, false, "idle");
		if ( this->parentConnection != NULL )
		{
			ObjectConnection::addChild( this->parentConnection->getParent(), spr );
		}
		spr->sprite->setAngle(atan2(ray.r.y, ray.r.x));
		spr->aabb.p = pos;
	}

	if (this->IsUseCollisionCounter() && this->collisionsCount > 0)
	{
		this->collisionsCount--;
	}

	if (this->collisionsCount == 0)
	{
		this->activity = oatDying;

		this->ray_end_point = pos;
		if ( obj->PointIn( ray.p ) )
			this->ray_end_point = ray.p;
		this->SetDrawRayEndPoint();
	}

	return true;
}



// ������� ������ (x0,y0)-(x,y) ������������ ����� ������ ����� ������� (x0,y0)
// kx = cos(a), ky = sin(a)
template<typename T>
__INLINE void RotatePoint(T& x, T& y, T x0, T y0, T kx, T ky)
{
	T xx = x - x0;
	T yy = y - y0;
	x = xx*kx - yy*ky + x0;
	y = xx*ky + yy*kx + y0;
}

#ifdef SELECTIVE_RENDERING
bool ObjRay::GetDrawAABB(CAABB& res) const
{
	/// @todo Use min/max distances used in SAP
	const float minf = -10000000;
	const float maxf =  10000000;

	float l = ray.p.x;
	float t = ray.p.y;
	float r = 0.0f;
	float b = 0.0f;
	float sprite_w = 0.0f;
	float sprite_h = 0.0f;

	if ( sprite )
	{
		if ( sprite->getRenderMethod() == rsmStretch )
		{
			sprite_w = res.W;
			sprite_h = res.H;
		}
		else
		{
			if ( !sprite->render_without_texture )
			{
				sprite_w = sprite->frameWidth / 2.0f;
				sprite_h = sprite->frameHeight / 2.0f;
			}
		}
	
		l += ray.r.x > 0 ? -sprite_w : sprite_w;
		t += ray.r.y > 0 ? -sprite_h : sprite_h;
	}
	
	if (IsDrawRayEndPoint())
	{
		r = ray_end_point.x;
		b = ray_end_point.y;
		r += ray.r.x < 0 ? -sprite_w : sprite_w;
		b += ray.r.y < 0 ? -sprite_h : sprite_h;
	}
	else
	{
		r = ray.r.x > 0 ? maxf : minf;
		b = ray.r.y > 0 ? maxf : minf;
	}

	res = CAABB(l, t, r, b);
	res.W = max( 1.0f, res.W );
	res.H = max( 1.0f, res.H );
	return true;
}
#endif

void ObjRay::Draw()
{
	CRay const& r = this->ray;

	extern float CAMERA_DRAW_LEFT;
	extern float CAMERA_DRAW_RIGHT;
	extern float CAMERA_DRAW_TOP;
	extern float CAMERA_DRAW_BOTTOM;

	Sprite* s = this->sprite;
	RGBAf color = RGBAf(1.0f, 1.0f, 1.0f, 1.0f);
	float z = 1.0f;
	if (s)
	{
		if (!s->IsVisible())
			return;

		color = s->color;
		z = s->z;		
	}

	if (r.IsIntersecting(CAMERA_DRAW_LEFT, CAMERA_DRAW_RIGHT, CAMERA_DRAW_TOP, CAMERA_DRAW_BOTTOM))
	{
		float x1 = r.p.x;
		float y1 = r.p.y;
		float x2;
		float y2;

		if (this->IsDrawRayEndPoint())
		{
			x2 = ray_end_point.x;
			y2 = ray_end_point.y;
		}
		else
		{
			// TODO: ��������� x1/y1, ���� ������ ���� � ���� �� ��������.

			// This code can not be replaced with one call to GetIntersectionPoint easyly.
			if (r.IsVertical())
			{
				x2 = r.p.x;
				if (r.r.y > 0)
					y2 = CAMERA_DRAW_BOTTOM;
				else
					y2 = CAMERA_DRAW_TOP;
			}
			else if(r.IsHorizontal())
			{
				y2 = r.p.y;
				if (r.r.x > 0)
					x2 = CAMERA_DRAW_RIGHT;
				else
					x2 = CAMERA_DRAW_LEFT;
			}
			else
			{
				Vector2 v;

				// Here we probing to intersect with different far sides of cam box
				if (r.r.x > 0)
				{
					if (r.r.y > 0)
					{
						if (!r.GetHorLineInters(CAMERA_DRAW_LEFT, CAMERA_DRAW_RIGHT, CAMERA_DRAW_BOTTOM, v))
							if (!r.GetVertLineInters(CAMERA_DRAW_TOP, CAMERA_DRAW_BOTTOM, CAMERA_DRAW_RIGHT, v))
								return;
					}
					else
					{
						if (!r.GetHorLineInters(CAMERA_DRAW_LEFT, CAMERA_DRAW_RIGHT, CAMERA_DRAW_TOP, v))
							if (!r.GetVertLineInters(CAMERA_DRAW_TOP, CAMERA_DRAW_BOTTOM, CAMERA_DRAW_RIGHT, v))
								return;
					}
				}
				else
				{
					if (r.r.y > 0)
					{
						if (!r.GetHorLineInters(CAMERA_DRAW_LEFT, CAMERA_DRAW_RIGHT, CAMERA_DRAW_BOTTOM, v))
							if (!r.GetVertLineInters(CAMERA_DRAW_TOP, CAMERA_DRAW_BOTTOM, CAMERA_DRAW_LEFT, v))
								return;
					}
					else
					{
						if (!r.GetHorLineInters(CAMERA_DRAW_LEFT, CAMERA_DRAW_RIGHT, CAMERA_DRAW_TOP, v))
							if (!r.GetVertLineInters(CAMERA_DRAW_TOP, CAMERA_DRAW_BOTTOM, CAMERA_DRAW_LEFT, v))
								return;
					}
				}

				x2 = v.x;
				y2 = v.y;
			}	
		}

		//RenderLine(x1, y1, 
		//	x2, y2, 
		//	1, 
		//	RGBAf(1,0,0,1));
		
		if (s && !s->render_without_texture && s->tex)
		{	
			if ( !s->IsVisible() )
				return;
			FrameInfo* f = s->tex->frames->frame + s->getCurrentFrame();
			const int first_frame = s->getCurrentFrame() + first_frame_shift;
			FrameInfo* f2 = f;
			if ( first_frame >= 0 && first_frame < static_cast<int>(s->tex->frames->framesCount) )
			{
				f2 = s->tex->frames->frame + first_frame;
			}
			
			// atan2 ��������� ���������� ������ 2 � 3 ��������
			float angle = atan2(y2 - y1, x2 - x1);

			float kx = cos(angle);
			float ky = sin(angle);

			// �������� �������� ����� � �������� ������� ���, ����� ��������� ��� �� �������������� ���. 
			// ��� ����� ��������� ���������� �� ��������.
			float x2_ = x2, y2_ = y2;
			RotatePoint(x2_, y2_, x1, y1, kx, -ky);

			assert(x2_ >= x1);
			assert(IsNear(y2_, y1, 0.001f));
			//RenderLine(x1, y1, 
			//	x2_, y2_, 
			//	1, 
			//	RGBAf(1,0,0,1));

			// ����� ������� ����� �������������� �������
			// ���������� �� ��� ��������������� ���� � ������������ 
			float zx = x1 + s->getRealX();
			float zy = y1 + s->getRealY() - s->getRealHeight()/2;
			RotatePoint(zx, zy, x1, y1, kx, ky);

			float w = static_cast<float>(s->getRealWidth());
			// ������ ������ ����� ���������. ���������� ��� ��������������� ���� � ������������.
			float sx = w, sy = 0;
			RotatePoint(sx, sy, 0.0f, 0.0f, kx, ky);
			coord2f_t aabb_size = coord2f_t( 2*aabb.W, 2*aabb.H );

			// ������ ����� ����� ����
			FrameInfo* frame = f2;
			for ( ; x1 + w <= x2_; x1 += w, zx += sx, zy += sy)
			{
				if ( sprite->getRenderMethod() == rsmStretch )
				     RenderFramePartLT(zx, zy, z, &aabb_size,     &aabb_size,     frame->coord, false, s->tex, color, angle);
				else RenderFramePartLT(zx, zy, z, &frame->size,   &frame->size,   frame->coord, false, s->tex, color, angle);
				frame = f;
			}

			// ������ ��������� ���������� ����� ����
			if (x2_ - x1 > 0)
			{
				if ( sprite->getRenderMethod() == rsmStretch )
				{
					coord2f_t c = aabb_size;
					c.x = (x2_ - x1);
					RenderFramePartLT(zx, zy, z, &aabb_size, &aabb_size, frame->coord, false, s->tex, color, angle);
				}
				else 
				{
					coord2f_t c = f->size;
					c.x = (x2_ - x1);
					RenderFramePartLT(zx, zy, z, &frame->size, &c, frame->coord, false, s->tex, color, angle);
				}
			}
		}
		else
		{
			RenderLine(x1, y1, 
				x2, y2, 
				z, 
				color);
		}
	}
}


