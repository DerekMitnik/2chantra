#ifndef __OBJECT_RAY_H_
#define __OBJECT_RAY_H_

#include "object.h"
#include "object_physic.h"
#include "../phys/phys_misc.h"
#include "object_character.h"

// ������ ������, ����������� ��� ���������� ��������, � ������� ������������ ���
// ������� ���������� �������� ��� �� ��� �������� �� ������
const UINT maxRayIntersectionsCount = 512;

class ObjRay : public GameObject
{

	CRay ray;	
public:

	CRay const& getRay() const { return ray; }
	void setRay(CRay const& r);

	RGBAf debug_color;

	float searchDistance;		// ����������, �� ������ ������� ����� �����������.
								// ���� ����� 0, �� ����� ����� �� ���������.
	int first_frame_shift;		// ���, ������ ������, ��� � ���� ������ ���� ������ (�� ����� ���� ��� �������� �����.
					// ��, ��������, �� ����� �����, ����� ��������. ��������� �� ��������, ����� � ������ ���� ������
					// ���������� ��� ������ ������� ������ �� ��������� � ����� �����?

	// TODO: 
	// 1. ��������� ����� ����������� ������:
	//		�) �� i-���� �����������/������������/����� � �.�.
	//		�) ��������� �� ����� ��������� ������

	UINT collisionsCount;

	BYTE rayFlags;

	int next_shift_y;

	// bullet code
	UINT damage;
	UINT charge;
	ObjectType shooter_type;
	UINT damage_type;
	bool multiple_targets;
	bool hurts_same_type;
	float push_force;

	UINT timeToLive;

	char* end_effect;
	char* hit_effect;

	bool Hit(ObjPhysic* obj, Vector2& pos, Vector2& norm);
	bool CheckIntersection(ObjPhysic* obj);
	// /bullet code

	Vector2 ray_end_point;
	uint8_t player_num;


	__INLINE BYTE IsUseCollisionCounter() const { return rayFlags & 1; }
	__INLINE void SetUseCollisionCounter()      { rayFlags |= 1; }
	__INLINE void ClearUseCollisionCounter()    { rayFlags &= ~1; }

	__INLINE BYTE IsUseTTLCounter()	  const { return rayFlags & 2; }
	__INLINE void SetUseTTLCounter()        { rayFlags |= 2; }
	__INLINE void ClearUseTTLCounter()      { rayFlags &= ~2; }

	__INLINE BYTE IsDrawRayEndPoint() const { return rayFlags & 4; }
	__INLINE void SetDrawRayEndPoint()      { rayFlags |= 4; }
	__INLINE void ClearDrawRayEndPoint()    { rayFlags &= ~4; }

	ObjRay();
	ObjRay( GameObject* shooter );
	~ObjRay();

	virtual bool ApplyProto(const Proto* proto);

	virtual void Process();
	void PhysProcess();

#ifdef SELECTIVE_RENDERING
	virtual bool GetDrawAABB(CAABB& res) const;
#endif
	virtual void Draw();
	
private:
	void Init();
protected:
	virtual CUData* createUData();
};

ObjRay* CreateRay(float x1, float y1, float x2, float y2);
ObjRay* CreateRay(const char* proto_name, Vector2 coord, ObjCharacter* shooter, WeaponDirection wd);
ObjRay* CreateRay(const Proto* proto, Vector2 coord, ObjCharacter* shooter, float angle);
ObjRay* CreateRay(const char* proto_name, Vector2 coord, ObjCharacter* shooter, float angle);
ObjRay* CreateRay(const Proto* proto, Vector2 coord, ObjCharacter* shooter, WeaponDirection wd);
ObjRay* CreateDummyRay();
#endif // __OBJECT_RAY_H_
