/*		
 		��������� �������, ��������, �� ����, ��������������� �����������.
 	����� ����� �������� �� client-side.
 */

#ifndef __OBJECT_EFFECT_H_
#define __OBJECT_EFFECT_H_
#include "object_dynamic.h"


class ObjEffect: public ObjDynamic
{
public:
	USHORT origin_point;
	Vector2 disp;
	bool old_orig_mirror;

	int health;
	RGBAf flash_target;
	int flash_speed;
	int flash_stage;

	virtual void Process();

	void ReceiveDamage( UINT ammount );
	void ParentEvent( ObjectEventInfo event_info );

	ObjEffect() :
		origin_point(0),
		disp(0.0f, 0.0f)
	{
		Init();
		parentConnection = new ObjectConnection( NULL );
	}

	ObjEffect( ObjDynamic* origin, USHORT origin_point, const Vector2& disp ) :
		origin_point(origin_point),
		disp(disp)
	{
		Init();
		ObjectConnection::addChild( origin, this );
	}

	virtual bool ApplyProto(const Proto* proto);

private:

	void Init()
	{
		old_orig_mirror = false;
		flash_target = RGBAf(1.0, 1.0, 1.0, 1.0);
		flash_speed = 0;
		health = 1;
		flash_stage = 0;
	}
protected:
	virtual CUData* createUData();
};

ObjEffect* CreateEffect(const char* proto_name, bool fixed, ObjDynamic* origin, USHORT origin_point, const Vector2& disp, UINT id = 0);
ObjEffect* CreateEffect(const Proto* proto, bool fixed, ObjDynamic* origin, USHORT origin_point, const Vector2& disp, UINT id = 0);
ObjEffect* CreateDummyEffect();

#endif // __OBJECT_EFFECT_H_
