#ifndef __OBJECT_ITEM_H_
#define __OBJECT_ITEM_H_

#include "object_dynamic.h"

class ObjItem : public ObjDynamic
{
public:

	ObjItem()
	{
		type = objItem;
	}

	void Process();
	void Touch( ObjDynamic* obj );
};

ObjItem* CreateItem(const char* proto_name, Vector2 coord, const char* start_anim, bool corner_adjust = true);
ObjItem* CreateItem(const char* proto_name, Vector2 coord, const char* start_anim, CAABB size, bool corner_adjust = true);
ObjItem* CreateDummyItem();

#endif //__OBJECT_ITEM_H_

