#include "StdAfx.h"

#include "object_dynamic.h"
#include "object_particle_system.h"

#include "../object_mgr.h"
#include "../phys/phys_misc.h"

#include "../../misc.h"
#include "../../resource_mgr.h"

//////////////////////////////////////////////////////////////////////////

extern ResourceMgr<Proto> * protoMgr;
extern ResourceMgr<Texture> * textureMgr;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

GameObject* CreateParticleSystem( const char * proto_name, Vector2 coord, UINT emitter, int mode )
{
	const Proto* proto = protoMgr->GetByName( proto_name, "particles/" );
	if (proto == NULL)
	{
		return NULL;
	}
	
	ObjDynamic* obj = new ObjDynamic();
	obj->aabb.p = coord;
	obj->aabb.W = 0.5f;
	obj->aabb.H = 0.5f;
	obj->rectangle[0] = Vector2(-obj->aabb.W, -obj->aabb.H);
	obj->rectangle[1] = Vector2( obj->aabb.W, -obj->aabb.H);
	obj->rectangle[2] = Vector2( obj->aabb.W,  obj->aabb.H);
	obj->rectangle[3] = Vector2(-obj->aabb.W,  obj->aabb.H);
	obj->rectangle.CalcBox();
	obj->SetPhysic();
	obj->SetDynamic();
	obj->update_needed = true;

	obj->solid_to = physEnvironment;
	
	//if (LoadObjectFromProto(proto, obj))
	//{
	//	DELETESINGLE(obj);
	//	return NULL;
	//}
	obj->type = objParticleSystem;
	
	obj->ParticleSystem = new CParticleSystem();
	
	obj->ParticleSystem->LoadFromProto(proto);
	obj->ParticleSystem->position = coord;
	obj->ParticleSystem->physic = (proto->physic == 1);
	obj->ParticleSystem->bounce = proto->bounce;
	obj->ParticleSystem->system_z = proto->z;
	obj->ParticleSystem->max_bounce = proto->max_bounce;
	obj->ParticleSystem->max_abs_speed = proto->particle_max_abs_speed;
	
	if ( proto->particle_anim > 0 )
	{
		obj->ParticleSystem->animation = static_cast<ParticlesAnimation>( proto->particle_anim );
		obj->ParticleSystem->animation_param = proto->particle_anim_param;
	}

	obj->ParticleSystem->emitter = emitter;
	obj->ParticleSystem->mode = mode;
	if ( emitter && (mode & 4) )
	{
		GameObject* emit_obj = GetGameObject( emitter );
		if ( emit_obj )
		{
			CAABB* emit_aabb = &emit_obj->aabb;
			Vector2 * pts = new Vector2[2];
			pts[1] = Vector2(emit_aabb->p.x-emit_aabb->W/*/2*/,emit_aabb->p.y);
			pts[0] = Vector2(emit_aabb->p.x+emit_aabb->W/*/2*/,emit_aabb->p.y);
			obj->ParticleSystem->SetGeometry(pts, 2);
		}
	}
	
	obj->ParticleSystem->Init();
	
	AddObject(obj);
	return obj;
}
