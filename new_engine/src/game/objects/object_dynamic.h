#ifndef __OBJECT_DYNAMIC_H_
#define __OBJECT_DYNAMIC_H_

#include "object_physic.h"
#include "object_environment.h"

enum ObjectMovementType { omtIdling, omtWalking, omtSitting, omtJumping, omtLanding/*, omtAimingUp, omtAimingDown, omtWalkingAimingUp*/, omtDropSitting, omtMovingToWaypoint, omtDodging, omtLying, omtRolling };
enum ObjectFacingType { ofNormal, ofMoonwalking, ofFixed };


extern ObjEnvironment* default_environment;

class ObjDynamic : public ObjPhysic
{
public:
	CAABB old_aabb;
	Vector2 vel;
	Vector2 acc;
	Vector2 gravity;
	Vector2 own_vel; //Only once each update, does not change speed
	TrajectoryType trajectory;
	float t_value;
	float t_param1;
	float t_param2;
	float max_x_vel;
	float max_y_vel;
	ObjectConnection* suspected_plane;
	ObjPhysic* drop_from;			// ����������� ��� ����, ����� ������, ��� ���������� � ���������.
	ObjEnvironment* env;
	ObjEnvironment* old_env;
	UINT last_env_on_stay;
	float shadow_width;
	bool drops_shadow;
	bool facing_left;
	float lifetime;
	float mass;

	float walk_acc;
	float jump_vel;

	bool ghostlike;			// ������ "����������", ��� ���� �� ��������
							// ������������ � �� �������� ������ solid-�������

	bool movementDirectionX;		// ���������� �������� �������


	ObjectMovementType movement;		// ��� �������� �������
	ObjectFacingType facing;

	BYTE physPushes;
	


	// �����
	// on_plane		16		������ ��������� �� �����������
	// bullet		32		������ - ����
	// dropping		128		������ ���������� � ���������
	__INLINE BYTE IsOnPlane()		{ return physFlags & 16; }
	__INLINE BYTE IsBullet()		{ return physFlags & 32; }
	__INLINE BYTE IsDropping()		{ return physFlags & 128; }

	__INLINE void SetOnPlane()		{ physFlags |= 16; }
	__INLINE void SetBullet()		{ physFlags |= 32; }
	__INLINE void SetDropping()		{ physFlags |= 128; drop_from = GetSuspectedPlane(); }

	__INLINE void ClearOnPlane()	{ physFlags &= ~16; }
	__INLINE void ClearBullet()		{ physFlags &= ~32; }
	__INLINE void ClearDropping()	{ physFlags &= ~128; }

	__INLINE BYTE IsPushedLeft()		{ return physPushes & 1; }
	__INLINE BYTE IsPushedRight()		{ return physPushes & 2; }
	__INLINE BYTE IsPushedUp()		{ return physPushes & 4; }
	__INLINE BYTE IsPushedDown()		{ return physPushes & 8; }

	__INLINE void SetPushedLeft()		{ physPushes |= 1; }
	__INLINE void SetPushedRight()		{ physPushes |= 2; }
	__INLINE void SetPushedUp()		{ physPushes |= 4; }
	__INLINE void SetPushedDown()		{ physPushes |= 8; }

	__INLINE void ClearPushes()	{ physPushes = 0; }

	ObjDynamic() :
		trajectory(pttLine),
		t_value(0.0f),
		t_param1(0.0f),
		t_param2(0.0f),
		max_x_vel(0.0f),
		max_y_vel(0.0f),
		suspected_plane(NULL),
		drop_from(NULL),
		env(default_environment),
		old_env(default_environment),
		last_env_on_stay(0),
		shadow_width(0.25f),
		drops_shadow(false),
		facing_left(false),
		lifetime(0.0f),
		mass(1.0f),
		walk_acc(0.0f),
		jump_vel(0.0f),
		ghostlike(false),
		movementDirectionX(true),
		movement(omtIdling),
		facing(ofNormal),
		physPushes(0)
	{
		SetDynamic();
	}

	~ObjDynamic()
	{
		if (suspected_plane) suspected_plane->childDead(id);
	}

	virtual void Bounce( Vector2 n, ObjPhysic* obj );

	virtual bool ApplyProto(const Proto* proto);

	void SetFacing( bool mirrored )
	{
		if ( childrenConnection && facing_left ^ mirrored )
			childrenConnection->Event( ObjectEventInfo( eventFacingChange ) );
		facing_left = mirrored;
		if ( !sprite ) return;
		if ( facing == ofFixed ) return;
		if ( mirrored )
			ofMoonwalking == facing ? sprite->ClearMirrored() : sprite->SetMirrored();
		else
			ofMoonwalking == facing ? sprite->SetMirrored() : sprite->ClearMirrored();
	}

	bool GetFacing()
	{
		return facing_left;
	}

	ObjPhysic* GetSuspectedPlane()
	{
		if ( !suspected_plane || suspected_plane->orphan )
			return NULL;
		return static_cast<ObjPhysic*>(suspected_plane->parent);
	}

	void PhysProcess();

	bool CheckSuspectedPlane();
	void CheckUnderlyngPlane();
	void DropShadow();

	virtual const char* GetSprite( size_t index );
	virtual const char* GetSound( size_t index );

	void SetEnv(ObjEnvironment* e);
	void ResetEnv();
	Vector2 GetGravity();

protected:
	virtual CUData* createUData();
};


#endif // __OBJECT_DYNAMIC_H_
