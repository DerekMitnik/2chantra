#ifndef __OBJECT_SPRITE_H_
#define __OBJECT_SPRITE_H_

#include "object.h"
#include "object_physic.h"

GameObject* CreateSprite(const char* proto_name, Vector2 coord, bool fixed, const char* start_anim, CAABB size, bool corner_adjust = true, UINT id = 0);
GameObject* CreateSprite(const char* proto_name, Vector2 coord, bool fixed, const char* start_anim, bool corner_adjust = true, UINT id = 0);
GameObject* CreateColorBox(const CAABB& aabb, float z, const RGBAf& color, UINT id = 0);
GameObject* CreateDummySprite(bool physic = false);
#endif // __OBJECT_SPRITE_H_
