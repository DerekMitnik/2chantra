#ifndef __OBJECT_CHARACTER_H_
#define __OBJECT_CHARACTER_H_

#include "object_dynamic.h"
#include "weapon.h"
#include <vector>

#include "../../render/font.h"

#include "../../resource_mgr.h"

enum CharacterGunDirection { cgdNone, cgdUp, cgdDown };
enum CharacterShootingBeh { csbNoShooting, csbFreeShooting, csbOnAnimCommand };

extern UINT internal_time;
extern UINT current_time;


// ���� ������������ �� ����� ������, �� ��������� �������� �������������� �������� �
// GameObject::ProcessSprite(), ��� ��� ������������ ObjCharacter* ch.
class ObjCharacter : public ObjDynamic
{
public:
	bool is_invincible;
	bool alt_fire;
	short health;
	short health_max;
	char* message;
	bool message_new;
	UINT message_time;
	int on_plane_reserve;	//Number of cycles to pass before actually losing footing. Needed for downward slopes and the like.

	//��� �������� ��� ����� ����������� �����, �� ������ �� ����� � ������ ����.
	float damage_mult;
	float weapon_angle;	//direction we are currently aiming at.
	float target_weapon_angle;
	float aiming_speed;

	ObjectConnection* faction;
	ObjectConnection* targeted_by;
	int faction_id;
	vector<int> faction_hates;
	vector<int> faction_follows;

	CharacterGunDirection gunDirection;

	Weapon* weapon;
	Weapon* cur_weapon;

	CharacterShootingBeh shootingBeh;

	uint8_t last_player_damage;
	UINT last_player_damage_type;
	Vector2 last_hit_from;
	
	virtual void Process();

	ObjCharacter():
		is_invincible(false),
		alt_fire(false),
		health(1),
		health_max(1),
		message(NULL),
		message_new(false),
		message_time(0),
		on_plane_reserve(0),
		damage_mult(1.0f),
		weapon_angle(0.0f),
		target_weapon_angle(0.0f),
		aiming_speed(0.05f),
		faction(NULL),
		faction_id(0),
		faction_hates(vector<int>()),
		faction_follows(vector<int>()),
		gunDirection(cgdNone),
		weapon(NULL),
		cur_weapon(NULL),
		shootingBeh(csbNoShooting),
		last_player_damage(0),
		last_player_damage_type(0),
		last_hit_from(0, 0)
	{
		targeted_by = new ObjectConnection(this);
	}

	virtual ~ObjCharacter()
	{
		DELETESINGLE(weapon);
		DELETEARRAY(mem_anim);
		if (faction)
			faction->childDead(this->id);
		//if ( activity != oatDying )  //���� ���� ����������, �� ��� �� ������� � �� ��� ���������� � ������ ������.
			targeted_by->parentDead();
		if ( message ) DELETEARRAY(message);
	}
	
	void ProcessShooting();

	void LoadFactionInfo( const Proto* proto )
	{
		faction_id = proto->faction_id;
		vector<int>::const_iterator it;
		for (it = proto->faction_hates.begin(); it != proto->faction_hates.end(); it++)
			faction_hates.push_back( *it );
		for (it = proto->faction_follows.begin(); it != proto->faction_follows.end(); it++)
			faction_follows.push_back( *it );
	}

	virtual void ReceiveDamage(UINT damage, UINT damage_type, uint8_t player_num = 0)
	{
		if ( this->IsSleep() ) return;
		if ( is_invincible || activity == oatDying ) return;

		assert( sprite );
		last_player_damage = player_num;
		last_player_damage_type = damage_type;

		//���� �������� ��������� ����� �� �������������, �� ��������� ��� ��. ����� �� ��������� ������ ��������.
		//TODO: �������� ���� ������ ��������
		//��������� ��������� ��������� ����� ���������� �� ���� �������� ��� �� �������� ��� �� ����������.
		if ( sprite->GetAnimation("pain") == NULL || this->activity == oatMorphing )
		{
			ReduceHealth((UINT)(damage*damage_mult));
			return;
		}

		if ( sprite->cur_anim != "pain" &&  sprite->cur_anim != "die" && damage > 0 && health > 0 )
		{
			sprite->SaveState();
			sprite->stack->Push( (int)(damage*damage_mult) );
			sprite->stack->Push( damage_type );
			SetAnimation("pain", true );
		}

		if (health <= 0 && activity != oatDying)
		{
			SetAnimation("die", false);
			activity = oatDying;
		}
	}

	virtual bool ReduceHealth(UINT damage)
	{
		health -= (short)damage;
		if (health <= 0 && activity != oatDying)
		{
			SetAnimation("die", false);
			//targeted_by->parentDead();
			activity = oatDying;
			return true;
		}
		return false;
	}

	void ReceiveHealing(UINT amount)
	{
		health += (short)amount;
		if ( health > health_max ) health = health_max;
	}

	void Draw()
	{
		if (IsSleep())
			return;

		if ( message )
		{
			IFont* fnt = FontByName( "dialogue" );
			if (fnt)
			{
				float w = (float)fnt->GetStringWidth( message );
				fnt->p = Vector2( aabb.p.x-(w/2), aabb.Top()-15 );
				fnt->z = 1.0f;
				fnt->tClr = RGBAf( 1.0f, 1.0f, 1.0f, 1.0f*(1.0f-(current_time - message_time)/5000.0f) );
				fnt->Print( message );
			}
		}

		GameObject::Draw();
	}

	virtual void SetFacing( bool mirror )
	{
		if ( mirror != GetFacing() )
		{
			target_weapon_angle = -target_weapon_angle;
			weapon_angle = -weapon_angle;
		}
		ObjDynamic::SetFacing( mirror );
	}

	void InitFaction();
	GameObject* GetNearestCharacter( vector<int> factions_list );
	bool IsEnemy( GameObject* obj );
};

void ClearFactions();

#endif // __OBJECT_CHARACTER_H_
