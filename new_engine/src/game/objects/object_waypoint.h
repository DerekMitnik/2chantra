#ifndef __OBJECT_WAYPOINT_H_
#define __OBJECT_WAYPINT_H_

#include "object.h"

class ObjWaypoint : public GameObject
{
public:

	UINT next_id;
	UINT unique_map_id;
	LuaRegRef reach_event;

	ObjWaypoint( GameObject* parent ) :
		next_id(0),
		unique_map_id(0),
		reach_event(LUA_NOREF)
	{
		type = objWaypoint;

		//<HALLOWEEN>
		if ( parent )
			ObjectConnection::addChild( parent, this );
		//</HALLOWEEN>
	}

	~ObjWaypoint();
	
	UINT pointReached( GameObject* obj );
	void setNextID( UINT id );
protected:
	virtual CUData* createUData();
};

ObjWaypoint* CreateWaypoint( const Vector2& coord, const Vector2& size );
ObjWaypoint* CreateDummyWaypoint();
UINT AddWaypoint( ObjWaypoint* waypoint );
void AddWaypoint( ObjWaypoint* waypoint, UINT unique_map_id );
ObjWaypoint* GetWaypoint( UINT wp_id );
void EmptyWPStack();

#endif // __OBJECT_WAYPOINT_H_
