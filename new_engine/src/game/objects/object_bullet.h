#ifndef __OBJECT_BULLET_H_
#define __OBJECT_BULLET_H_

#include "object_dynamic.h"
#include "weapon.h"
class ObjCharacter;

class ObjBullet : public ObjDynamic
{
public:
	UINT damage;
	//ObjCharacter* shooter; <- ��� ������, ������ ��� �� ����� ���� ����� � ���������� �� �������.
	ObjectType shooter_type;

	//Shielding bit - ��������� ������ �������� ��� �������. ������ shielding �������� ������������.
	__INLINE void SetShielding()		{ physFlags |= 128; }
	__INLINE void ClearShielding()		{ physFlags &= ~128; }
	__INLINE bool IsShielding()			{ return (physFlags & 128) != 0; }

	UINT damage_type;
	UINT charge;
	float push_force;
	bool multiple_targets;
	bool hurts_same_type;
	bool hurts_same_faction;
	uint8_t player_num;

	ObjBullet( GameObject* shooter ) :
		damage(0),
		shooter_type(objNone),
		damage_type(0),
		charge(0),
		push_force(0.0f),
		multiple_targets(false),
		hurts_same_type(false),
		hurts_same_faction(false),
		player_num(0)
	{
		this->type = objBullet;

		this->SetBullet();
		this->SetPhysic();

		if (shooter)
		{
			ObjectConnection::addChild( shooter, this );
			this->shooter_type = shooter->type;
		}
		else
		{
			this->SetBulletCollidable();
			this->parentConnection = new ObjectConnection( NULL );
		}
	}

	virtual bool ApplyProto(const Proto* proto);

	virtual void Process();
	virtual void Bounce();
	void PhysProcess();

	bool Hit(ObjPhysic* obj);
	bool Miss();

protected:
	virtual CUData* createUData();
};


ObjBullet* CreateBullet(const char* proto, Vector2 coord, ObjCharacter* shooter, Vector2 aim, int angle, UINT id = 0);
ObjBullet* CreateBullet(const char* proto_name, Vector2 coord, ObjCharacter* shooter, WeaponDirection wd, UINT id = 0);
ObjBullet* CreateBullet(const Proto* proto, Vector2 coord, ObjCharacter* shooter, WeaponDirection wd, UINT id = 0);
ObjBullet* CreateAngledBullet(const Proto* proto, Vector2 coord, GameObject* shooter, bool mirror, float angle, int dir, UINT id = 0);
ObjBullet* CreateAngledBullet(const char* proto_name, Vector2 coord, GameObject* shooter, bool mirror, int angle, int dir, UINT id = 0);
ObjBullet* CreateDummyBullet();

#endif // __OBJECT_BULLET_H_
