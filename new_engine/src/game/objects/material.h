#ifndef __MATERIAL_H
#define __MATERIAL_H

#include "../../resource.h"
#include "../proto.h"

class Material
{
public:
	int type;

	float bounce_bonus;
	float friction;

	char** sounds;
	char** sprites;
	
	Material() :
		type(0),
		bounce_bonus(0),
		friction(1),
		usageCounter(0)
	{
		sounds = new char*[ARRAYS_SIZE];
		std::fill(sounds, sounds + ARRAYS_SIZE, (char*)NULL);

		sprites = new char*[ARRAYS_SIZE];
		std::fill(sprites, sprites + ARRAYS_SIZE, (char*)NULL);
	}

	~Material()
	{
		for ( size_t i = 0; i < ARRAYS_SIZE; i++ )
		{
			DELETEARRAY(sounds[i]);
			DELETEARRAY(sprites[i]);
		}
		DELETEARRAY(sounds);
		DELETEARRAY(sprites);
	}

	bool Load( const Proto* proto );

	const char* GetSprite( size_t index );
	const char* GetSound( size_t index );


	void ReserveUsage()	const	{ usageCounter++; }			// ��� const - ��� ����. �� ����� ���� ��� �����-�������. ������ ��� �����.
	void ReleaseUsage()	const	{ assert(usageCounter > 0); usageCounter--;}
	bool IsUnUsed()		const	{ return usageCounter == 0; }

	//static void Release( const Material* m ) 
	//{
	//	m->ReleaseUsage(); 
	//	if ( m->IsUnUsed() )
	//		DELETESINGLE( m );
	//}

protected:
	mutable int usageCounter;

	enum { ARRAYS_SIZE = 32 };
};

Material* GetMaterial(const char* name );
Material* GetDefaultMaterial();
void ClearMaterials();
void ClearUnusedMaterials();

#endif //__MATERIAL_H

