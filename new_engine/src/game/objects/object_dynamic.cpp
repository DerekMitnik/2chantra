#include "StdAfx.h"
#include "object_dynamic.h"

#include "../phys/phys_collisionsolver.h"
#include "../../config.h"
#include "../phys/2de_Matrix2.h"
#include "../editor.h"

#include "../../script/udata.hpp"

extern float CAMERA_DRAW_RIGHT;
extern float CAMERA_DRAW_LEFT;
extern config cfg;

#ifdef MAP_EDITOR
extern editor::EditorStates editor_state;
#endif // MAP_EDITOR

bool ObjDynamic::ApplyProto(const Proto* proto)
{
	if (!ObjPhysic::ApplyProto(proto))
		return false;

	gravity        = Vector2( proto->gravity_x, proto->gravity_y );
	trajectory     = (TrajectoryType)proto->trajectory;
	t_value        = Random_Float( proto->min_param, proto->max_param );
	t_param1       = proto->trajectory_param1;
	t_param2       = proto->trajectory_param2;
	max_x_vel      = proto->phys_max_x_vel;
	max_y_vel      = proto->phys_max_y_vel;
	shadow_width   = abs(proto->shadow_width);
	drops_shadow   = (1 == proto->drops_shadow);
	mass           = proto->mass;
	walk_acc       = proto->phys_walk_acc;
	jump_vel       = proto->phys_jump_vel;
	ghostlike      = (1 == proto->phys_ghostlike);
	facing         = (ObjectFacingType)proto->facing;

	return true;
}

bool ObjDynamic::CheckSuspectedPlane()
{
	return RestsOn( this, GetSuspectedPlane() );
}

void ObjDynamic::PhysProcess()
{
#ifdef MAP_EDITOR

	if ( false && editor_state != editor::EDITOR_OFF )
	{
		bool movement_occurred = false;
		if ( old_aabb != aabb )
		{
			old_aabb = aabb;
			movement_occurred = true;
		}
		
		// ����� ��� ��������� �������������� �������
		assert(aabb.H != 0);
		assert(aabb.W != 0);
		
		
		
		if (aabb != old_aabb || movement_occurred)
		{
			if ( this->childrenConnection )
				this->childrenConnection->Event( ObjectEventInfo( eventMoved, aabb.p - old_aabb.p ) );
			UpdateObject(sap_handle, aabb);
		}
	}

	if ( editor_state == editor::EDITOR_OFF )
#endif // MAP_EDITOR
	{
	if (IsSleep())
		return;
	bool movement_occurred = false;

	if ( old_env && old_env != env && old_env != default_environment )
	{
		old_env->OnLeave( this );
		old_env = env;
	}

	aabb.p += own_vel;

	switch ( trajectory )
		{
			case pttRipple:
				{
					t_value++;
					float l = vel.Length();
					if ( l == 0 ) break;
					float tr_x = t_param1*( -vel.x/l )*sin(t_value*t_param2 );
					float tr_y = t_param1*( vel.y/l )*sin(t_value*t_param2);
					aabb.p += Vector2( tr_x, tr_y );
					movement_occurred = true;
					break;
				}
			case pttCosine:
				{
					t_value++;
					Vector2 pnorm = Vector2();
					if ( vel.Length() > 0 ) pnorm = vel.GetPerpendicular().Normalized();
					Vector2 tr = pnorm * (t_param1 * cos(t_value*t_param2));
					aabb.p += tr;
					movement_occurred = true;
					break;
				}
			case pttGlobalSine:
				{
					t_value++;
					Vector2 tr = Vector2(0, 1) * (t_param1 * cos(t_value*t_param2));
					aabb.p += tr;
					movement_occurred = true;
					break;
				}
			case pttSine:
				{
					t_value++;
					Vector2 pnorm = Vector2();
					if ( vel.Length() > 0 ) pnorm = vel.GetPerpendicular().Normalized();
					Vector2 tr = pnorm * (t_param1 * cos(t_value*t_param2));
					aabb.p += tr;
					movement_occurred = true;
					break;
				}
			case pttRandom:
				{
					Vector2 perp = Vector2();
					Vector2 norm = Vector2();
					if ( vel.Length() > 0 )
					{
						perp = vel.GetPerpendicular().Normalized();
						norm = vel.Normalized();
					}
					Vector2 tr = perp * Random_Float(-t_param1, t_param1);
					Vector2 tr2 = norm * Random_Float(-t_param2, t_param2);
					movement_occurred = true;
					aabb.p += tr;
					aabb.p += tr2;
				}
			default:
				{
					break;
				}
		}

	if ( lifetime > 0 )
	{
		if ( lifetime <= 1 )
		{
			lifetime = 0;
			if ( sprite && sprite->GetAnimation("die") )
			{
				activity = oatDying;
				SetAnimation("die", false);
			}
			else
				SetDead();
			return;
		}
		else
		{
			lifetime -= 1;
		}
	}

	if ( old_aabb != aabb || old_aabb.p.x != aabb.p.x || old_aabb.p.y != aabb.p.y )
	{
		old_aabb = aabb;
		movement_occurred = true;
	}
	Vector2 old_v = vel;

	bool was_on_plane = IsOnPlane() != 0;
	ClearOnPlane();
	if ( CheckSuspectedPlane() )
	{
		// �������� � �������� �� ������� �����.
		ObjPhysic* plane = GetSuspectedPlane();
		// TODO �������� ����������� ����
		while( plane && plane->IsDynamic() )
		{
			aabb.p += ((ObjDynamic*)plane)->vel + 0.5f * ((ObjDynamic*)plane)->acc;
			aabb.p += ((ObjDynamic*)plane)->env->gravity_bonus;
			//>aabb.p.y = plane->aabb.Top() - aabb.H;	//Could be worse.

			plane = ((ObjDynamic*)plane)->IsOnPlane() ? ((ObjDynamic*)plane)->GetSuspectedPlane() : NULL;
		}
		// TODO �������� ����������� ����
		SetOnPlane();
	}
	else 
	{
		ObjPhysic* susp_plane = GetSuspectedPlane();
		if ( was_on_plane && susp_plane && susp_plane->IsOneSide() )
		{
			float depth;
			Vector2 n;
			if ( CPolygon::Collide( *             geometry,            aabb.p, Vector2(), Matrix2(0.0f), 
						*susp_plane->geometry, susp_plane->aabb.p, Vector2(), Matrix2(0.0f), 
						n, depth ) )
			{
				if ( fabs(depth) > 0.01f ) 
				{
					aabb.p -= depth * n * 1.001f;
					SetOnPlane();
				}
			}
		}
		if ( !IsOnPlane() && suspected_plane )
		{
			suspected_plane->childDead( id );
			suspected_plane = NULL;
		}
	}

	// ����������� ������ ���� ������ �� �� ���������
	if (!IsOnPlane())
		vel += acc + GetGravity();
	else
		vel += acc;

	if (vel.x > max_x_vel)
		vel.x = max_x_vel;
	else if (vel.x < -max_x_vel)
		vel.x = -max_x_vel;

	if (vel.y > max_y_vel)
		vel.y = max_y_vel;
	else if (vel.y < -max_y_vel)
		vel.y = -max_y_vel;

#ifdef USE_TIME_DEPENDENT_PHYS
	aabb.p += old_v + acc * 0.5f;
#else
	aabb.p += vel;
#endif // USE_TIME_DEPENDENT_PHYS
	
	if (IsOnPlane())
	{
		ObjPhysic* susp_plane = GetSuspectedPlane();
		if ( susp_plane && susp_plane->material )
		{
			if ( susp_plane->material->friction <= 1.0f )
				vel.x *= 0.75f + (1.0f - susp_plane->material->friction) * 0.25f;
			else
				vel.x *= 0.75f / susp_plane->material->friction;
		}
		else
			vel.x *= 0.75f;
	}
	else
		vel.x *= 0.9999f;

	if ( fabs(vel.x) < 0.25f ) vel.x = 0.0f;

	//if (old_env != default_environment && env == default_environment)
	//	old_env->OnLeave(this);
	//old_env = env;
	//env = default_environment;

	// ����� ��� ��������� �������������� �������
	assert(aabb.H != 0);
	assert(aabb.W != 0);

	if (aabb != old_aabb || movement_occurred || update_needed)
	{
		update_needed = false;
		if ( childrenConnection )
			childrenConnection->Event( ObjectEventInfo( eventMoved, aabb.p - old_aabb.p ) );
		CAABB extended_aabb(min(aabb.Left(),old_aabb.Left()), min(aabb.Top(),old_aabb.Top()), max(aabb.Right(),old_aabb.Right()), max(aabb.Bottom(),old_aabb.Bottom()));

		UpdateObject(sap_handle, extended_aabb);
	}

	// ���������, ���� �� ��������� ��� ��������.
	//CheckUnderlyngPlane();
	}
}

// �������� ������� ��������� "���"
void ObjDynamic::CheckUnderlyngPlane()
{
	//if (!IsOnPlane())
	//	return;
/*
	assert(asap);

	const ASAP_Box* boxes = asap->GetBoxes();
	const ASAP_EndPoint* epY = asap->GetEndPoints(Y_);
	assert(boxes);
	assert(epY);

	const ASAP_Box* object = boxes + sap_handle;

	const Opcode::ASAP_EndPoint* current = epY + (boxes + sap_handle)->mMax[Y_];
	const ValType limit = current->mValue + 1;

	const ASAP_AABB& asab_aabb = GetASAP_AABB(aabb);

	while ((++current)->mValue <= limit)
	{
		if (!current->IsMax())
		{
			if ( ((ObjPhysic*)(boxes[current->GetOwner()].mObject))->IsSolid() )
			{
				const ASAP_Box* id1 = boxes + current->GetOwner();
				// Our max passed a min => start overlap
				float depth;
				Vector2 n;
				bool collide = CPolygon::Collide( *geometry, aabb.p + Vector2( 0, 1 ), Vector2(), Matrix2(0.0f), *((ObjPhysic*)boxes[current->GetOwner()].mObject)->geometry, ((ObjPhysic*)boxes[current->GetOwner()].mObject)->aabb.p,  Vector2(), Matrix2(0.0f), n, depth );
				if (object != id1 &&
					Intersect2D(*object, *id1, X_, Z_) &&
					Intersect1D_Min(asab_aabb, *id1, epY, Y_) && collide &&
					!( IsDropping() && ((ObjPhysic*)boxes[current->GetOwner()].mObject)->IsOneSide() ) )
				{
					SetOnPlane();
					if ( depth > 1.0f )
						aabb.p += depth * (n + 0.01f);
					drop_from = NULL;
					suspected_plane = (ObjPhysic*)(boxes[current->GetOwner()].mObject);
					return;
				}
			}

		}
	}
	ClearOnPlane();
*/
}

void ObjDynamic::DropShadow()
{
	//���� ���� ��������� - ������ �� ������.
	if ( !cfg.shadows )
		return;

	//������ ������� �� ����������� ����.
	if ( IsSleep() )
		return;

	//�������, �� ������������� ���� ��� �� ����������.
	if ( !drops_shadow )
		return;





	//��� �� ���������� ����, ������� �� �� ����� �������.
	if ( aabb.Left() > CAMERA_DRAW_RIGHT || aabb.Right() < CAMERA_DRAW_LEFT ) 
		return;

	//���� �� ����� �� ����������� - ���� ����� ��� ������.
	ObjPhysic* susp_plane = GetSuspectedPlane();
	Vector2 gravity = GetGravity();
	if ( gravity.y > 0 && IsOnPlane() && susp_plane )
	{
		float y = aabb.Bottom();
		float left = max( aabb.Left(), susp_plane->aabb.Left() );
		float right = min( aabb.Right(), susp_plane->aabb.Right() );
		float w = 0.5f * (right - left);
		RenderEllipse( left + w, y, w, shadow_width * aabb.W, SHADOW_Z, RGBAf(SHADOW_COLOR), id, 90, bmSrcA_OneMinusSrcA);
		return;
	}
	
	ObjPhysic* shadow_plane = GetShadowPlane(this);
	if (shadow_plane)
	{
		float w = aabb.W;
		Vector2 point = aabb.p;

		CRay ray;
		ray.r = Vector2(0,1);
		ray.p = point;
		if( ray.p.x >= shadow_plane->aabb.Right() )
			ray.p.x = shadow_plane->aabb.Right() - 0.001f;
		if( ray.p.x <= shadow_plane->aabb.Left() )
			ray.p.x = shadow_plane->aabb.Left() + 0.001f;

		Vector2 pos;
		Vector2 norm;
		GetIntersection( ray, *shadow_plane->geometry, shadow_plane->aabb.p, pos, norm );

		float y = max( pos.y - abs(norm.x) / (0.01f + abs(norm.y)) * w, shadow_plane->aabb.Top() );
		float dist = y - aabb.Bottom();
		w *= 1.0f - dist / (SHADOW_DISTANCE + 1.0f);
		float left = max( aabb.p.x - w, shadow_plane->aabb.Left() );
		float right = min( aabb.p.x + w, shadow_plane->aabb.Right() );
		w = 0.5f * (right - left);

		RenderEllipse( left + w, y, w, shadow_width*w, SHADOW_Z, RGBAf(SHADOW_COLOR), id, 90, bmSrcA_OneMinusSrcA);
		return;
	}
}

Vector2 ObjDynamic::GetGravity()
{
	return gravity + env->gravity_bonus;
}

void ObjDynamic::SetEnv(ObjEnvironment* e)
{
	if (e != env)
	{
		if (e != default_environment)
			e->OnEnter(this);

		env = e;
		old_env = e;
	}
}

void ObjDynamic::ResetEnv()
{
	old_env = env;
	env = default_environment;
}

const char* ObjDynamic::GetSprite( size_t index )
{
	ObjPhysic* susp_plane = GetSuspectedPlane();
	if ( !susp_plane || !susp_plane->material ) return NULL;
	return susp_plane->material->GetSprite( index );
}

const char* ObjDynamic::GetSound( size_t index )
{
	ObjPhysic* susp_plane = GetSuspectedPlane();
	if ( !susp_plane || !susp_plane->material ) return NULL;
	return susp_plane->material->GetSound( index );
}

void ObjDynamic::Bounce( Vector2 n, ObjPhysic* obj )
{
	Vector2 gravity = GetGravity();
	
	if ( (vel + own_vel) * n >= 0.0f )
		return;

	float mbounce = -bounce;
	if ( obj->material ) mbounce -= obj->material->bounce_bonus;
	
	Vector2 p = n.GetPerpendicular();

	float vel_n = vel * n;
	float vel_p = vel * p;
	float own_vel_n = own_vel * n;
	float own_vel_p = own_vel * p;
	float acc_n = acc * n;
	float acc_p = acc * p;

	if ( gravity * n < -gravity.Length() * 0.5f )
	{
		SetOnPlane();
		obj->PutOn( this );
		suspected_plane = obj->foundation_for;
	
		float frict = 0.75f;
		if ( obj->material )
		{
			if ( obj->material->friction <= 1.0f )
				frict = 0.75f + (1.0f - obj->material->friction) * 0.25f;
			else
				frict *= 0.75f / obj->material->friction;
		}
		vel_p *= frict;
		own_vel_p *= frict;
	}

	vel_n *= mbounce;
	if ( abs(vel_n) < 1.5f ) vel_n = 0;
	own_vel_n *= mbounce;
	if ( abs(own_vel_n) < 1.5f ) own_vel_n = 0;
	acc_n *= mbounce;
	if ( abs(acc_n) < 1.5f ) acc_n = 0;

	vel = vel_n * n + vel_p * p;
	own_vel = own_vel_n * n + own_vel_p * p;
	acc = acc_n * n + acc_p * p;
}
