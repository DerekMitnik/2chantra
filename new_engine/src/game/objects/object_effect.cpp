#include "StdAfx.h"
#include "object_effect.h"
#include "object_sprite.h"

#include "../net.h"
#include "../object_mgr.h"
#include "../proto.h"

#include "../../resource_mgr.h"

//////////////////////////////////////////////////////////////////////////

extern ResourceMgr<Proto> * protoMgr;
extern bool netgame;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

bool ObjEffect::ApplyProto(const Proto* proto)
{
	if (!ObjDynamic::ApplyProto(proto))
		return false;

	ghostlike = true;

	return true;
}

ObjEffect* CreateEffect(const char* proto_name, bool fixed, ObjDynamic* origin, USHORT origin_point, const Vector2& disp, UINT id)
{
	const Proto* proto = protoMgr->GetByName(proto_name, "effects/");
	return CreateEffect( proto, fixed, origin, origin_point, disp, id );
}

ObjEffect* CreateEffect(const Proto* proto, bool fixed, ObjDynamic* origin, USHORT origin_point, const Vector2& disp, UINT id)
{
	if (!proto)
		return NULL;

	if ( netgame )
	{
		//Do nothing if we are not the server and the origin is not managed locally
		if ( !Net::IsServer() && id == 0 && (!origin || !origin->local) )
			return NULL;

		//If we are the server, leave locally managed objects to create effects client-side and report it
		if ( Net::IsServer() && id == 0 && origin && origin->local )
			return NULL;
	}

	if (!(origin && origin->sprite && origin_point < origin->sprite->mpCount))
	{
		Log(DEFAULT_LOG_NAME, logLevelError, "Trying to create effect attached to the the non-existant mount point %d of object %s (id = %d)", 
			origin_point, (origin && origin->sprite) ? origin->sprite->proto_name : "", origin ? origin->id : -1);
		return NULL;
	}

	ObjEffect* object = new ObjEffect(origin, origin_point, disp);
	object->type = objEffect;

	if (!object->ApplyProto(proto))
	{
		DELETESINGLE(object);
		return NULL;
	}
	
	if (fixed) object->sprite->SetFixed();

	object->ClearPhysic();
	object->SetAnimation("idle", true);
	object->flash_target = RGBAf(1.0f, 1.0f, 1.0f, 1.0f);
	object->SetPhysic();


	Vector2 disp2(disp);
	assert(origin->sprite->mpCount > origin_point);
	Vector2 mp(origin->sprite->mp[ origin_point ]);
	if ( origin->sprite->IsMirrored() )
	{
		disp2.x *= -1;
		mp.x *= -1;
		object->SetFacing( true );
	}

	object->aabb.p = origin->aabb.p + disp2 + mp;
	object->flash_stage = 0;

	AddObject(object, id);
	if ( object->id == 0 )
	{
		if ( object->parentConnection )
			object->parentConnection->childDead( 0 );
		DELETESINGLE( object );
		return NULL;
	}

	return object;
}

ObjEffect* CreateDummyEffect()
{
	ObjEffect* obj = new ObjEffect;
	AddObject(obj);
	return obj;
}

void ObjEffect::ReceiveDamage( UINT ammount )
{
	health -= ammount;
	if ( health <= 0 )
		SetAnimation("die", false);
	else if ( flash_stage <= 0 )
	{
		flash_target = RGBAf(1.0f, 0.5f, 0.0f, 1.0f);
		flash_stage = 1;
		flash_speed = 2;
	}
}

void ObjEffect::Process()
{
	if (IsSleep())
		return;

	if ( sprite && sprite->overlayCount > 0 )
	{
		if ( flash_stage != 0 )
		{
			if (!sprite->ocolor)
				Log(DEFAULT_LOG_NAME, logLevelError, "ObjEffect::Process() ocolor == NULL, proto %s", sprite->proto_name);
			else
				sprite->ocolor[0] = RGBAf(1.0, 1.0, 1.0, 0.0) + (flash_target - RGBAf(1.0, 1.0, 1.0, 0.0))*(abs((float)flash_stage)/flash_speed);
			if ( flash_stage < 0 && flash_stage > -1 ) flash_stage = 0;
			else flash_stage++;
			if ( flash_stage > flash_speed ) flash_stage = -flash_speed; 
		}
		else
		{
			if (sprite->ocolor[0].a != 0)
				sprite->ocolor[0].a = 0;
		}
	}
}

void ObjEffect::ParentEvent( ObjectEventInfo info )
{
	switch ( info.type )
	{
		case eventDead:
			{
				SetDead();
				break;
			}
		case eventMPChanged:
		case eventMoved:
		case eventCollisionPushed:
		case eventFacingChange:
			{
				Vector2 disp2( disp );
				Sprite* sprite2( parentConnection->getParent()->sprite );
				Vector2 mp( sprite2->mp[ origin_point ] );
				if ( sprite2->IsMirrored() )
				{
					disp2.x *= -1;
					mp.x *= -1;
					SetFacing( true );
				}
				else
				{
					SetFacing( false );
				}
				aabb.p = parentConnection->getParent()->aabb.p + disp2 + mp;

#ifdef SELECTIVE_RENDERING
				selectForRenederUpdate(this);
#endif
				break;
			}
		default: break;
	}
}

