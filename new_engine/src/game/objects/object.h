#ifndef __OBJECT_H_
#define __OBJECT_H_

enum ObjectType { objNone, objPlayer, objSprite, objBullet, objEffect, objEnemy, objItem, objEnvironment, objRay, objParticleSystem, objWaypoint, objSpawner, objTile, objRibbon };
enum TouchDetectionType { tdtAlways, tdtFromEverywhere, tdtFromTop, tdtFromBottom, tdtFromSides, tdtTopAndSides };
enum ObjectActivityType { oatIdling, oatShooting, oatDying, oatInjuring, oatUndead, oatMorphing, oatUsing };
enum ObjectEventType { eventError, eventDead, eventCollisionPushed, eventMoved, eventFacingChange, eventMPChanged };
extern bool netgame;

#include "../../script/script.h"
#include "../../script/CUDataUser.hpp"

#include "../phys/phys_misc.h"
#include "../sprite.h"

#include "../proto.h"

#include "../particle_system.h"
#include "../editor.h"

class CUData;

class ObjectEventInfo
{
public:
	ObjectEventType type;
	Vector2 coord;
	UINT num;

	ObjectEventInfo() { this->type = eventError; }
	ObjectEventInfo( ObjectEventType type )	{ this->type = type; }
	ObjectEventInfo( ObjectEventType type, Vector2 coord ) { this->type = type; this->coord = coord; }
	ObjectEventInfo( ObjectEventType type, Vector2 coord, UINT num ) { this->type = type; this->coord = coord; this->num = num; }
};

class GameObject;

class ObjectConnection
{
protected:
	void _addChild( GameObject* child );
	
public:
	GameObject* parent;
	vector<GameObject*> children;
	bool orphan;
	static void addChild( GameObject* parent, GameObject* child );
	void parentDead();
	void childDead( UINT id );
	void Event( ObjectEventInfo event_info );
	
	GameObject* getParent();
	
	ObjectConnection( GameObject* parent )
	{
		this->parent = parent;
		this->orphan = NULL == parent;
	}
	
	ObjectConnection( GameObject* parent, GameObject* child )
	{
		this->parent = parent;
		this->orphan = NULL == parent;
		this->children.push_back( child );
	}
	
	~ObjectConnection()
	{
	}
	
	static void removeConnection( ObjectConnection* instance )
	{
		delete instance;
	}
};

class GameObject : public CUDataUser
{
#ifdef MAP_EDITOR
public:
	EditorObject editor;
#endif //MAP_EDITOR

protected:
	virtual CUData* createUData();

public:
	UINT id;

	ObjectType type;
	bool local;	//The object is managed locally in multiplayer

	Sprite* sprite;			// ������
	CParticleSystem *ParticleSystem; // facepalm.jpg

	CAABB aabb;				// �������������, ����������� ���������� ������; ���������� ����
	CAABB old_aabb;

	LuaRegRef scriptProcess;

	ObjectActivityType activity;		// ��� �������� �������

	// �����
	// dead		1		������ �������� �������.
	// physic	2		����������, �������� � ASAP.
	// sleep	4
	BYTE objFlags;
	__INLINE bool IsDead()      { return (objFlags & 1) != 0; }
	__INLINE void SetDead()     { objFlags |= 1; }
	__INLINE void ClearDead()   { objFlags &= ~1; }

	__INLINE bool IsPhysic()    { return (objFlags & 2) != 0; }
	__INLINE void SetPhysic()   { objFlags |= 2; }
	__INLINE void ClearPhysic() { objFlags &= ~2; }

	__INLINE bool IsSleep()     { return (objFlags & 4) != 0; }
	__INLINE void SetSleep()    { objFlags |= 4; }
	__INLINE void ClearSleep()  { objFlags &= ~4; }

	UINT mem_frame;
	UINT mem_tick;
	char * mem_anim;

	TouchDetectionType touch_detection;

	ObjectConnection* parentConnection;
	ObjectConnection* childrenConnection;

	Vector2 compensation_p;	//For smooth net movement prediction
	bool net_update;

	GameObject( GameObject* parent = NULL )
	{
		local = false;
		type = objNone;
		id = 0;
		sprite = NULL;
		ParticleSystem = NULL;

		parentConnection = childrenConnection = NULL;

		mem_anim = NULL;

		compensation_p = Vector2();
		net_update = false;
		
		objFlags = 0;
		scriptProcess = LUA_NOREF;

		touch_detection = tdtAlways;

		activity = oatIdling;

		if ( parent ) parent->AddChild( this );

#ifdef SELECTIVE_RENDERING
		graph_handle = INVALID_GRAPH_HANDLE;
#endif
	}

	virtual ~GameObject();

	virtual bool ApplyProto(const Proto* proto);

	virtual void Process()
	{ }

	virtual void ParentEvent( ObjectEventInfo info )
	{
		UNUSED_ARG(info);
	}

	virtual void Resize( float w, float h );

	virtual bool GetFacing()
	{
		if ( sprite ) return sprite->IsMirrored() != 0;
		return false;
	}

	virtual void SetFacing( bool mirrored )
	{
		if ( sprite ) 
		{
			if ( mirrored )
				sprite->SetMirrored();
			else
				sprite->ClearMirrored();
		}
	}

	virtual const char* GetSprite( size_t index )
	{
		UNUSED_ARG( index );
		return NULL;
	}
	virtual const char* GetSound( size_t index )
	{
		UNUSED_ARG( index );
		return NULL;
	}

	void ProcessSprite();
	bool SetAnimation(string anim_name, bool restart);
	bool SetAnimationIfPossible(string anim_name, bool restart);
	virtual void AddChild( GameObject* child )
	{
		ObjectConnection::addChild( this, child );
	}
	virtual bool IsPhysicType()
	{
		return false;
	}

	virtual void Draw();

#ifdef SELECTIVE_RENDERING
	UINT graph_handle;
	static const UINT INVALID_GRAPH_HANDLE;

	virtual bool GetDrawAABB(CAABB& res) const;
#endif
};	



#endif // __OBJECT_H_
