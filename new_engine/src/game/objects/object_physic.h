#ifndef __OBJECT_PHYSIC_H_
#define __OBJECT_PHYSIC_H_

#include "object.h"
#include "material.h"
#include "../phys/2de_Math.h"
#include "../phys/2de_Matrix2.h"

//Used in collision solver
enum PhysicObjects
{
	physPlayer = 1,
	physBullet = 2,
	physEffect = 4,
	physSprite = 8,
	physItem = 16,
	physEnemy = 32,
	physEnvironment = 64,
	physParticles = 128,
	physEverything = 255
};

class ObjPhysic : public GameObject
{
public:

	UINT sap_handle;

	BYTE physFlags;
	float bounce;
	BYTE solid_to;
	BYTE slopeType; // 0 - ���, 1 - "/", 2 - "\"

	CPolygon rectangle; //Own polygon for compatibility
	CPolygon* custom_polygon;
	CPolygon* geometry; //Currently used polygon
	bool update_needed; //For passive objects to update on next check
	bool particles_collidable;

	Material* material;
	ObjectConnection* foundation_for;  //Suspected_plane for these objects

	// �����
	// solid					1		������ ��������� �� �����������
	// bullet_collidable		2		������ �������� ����������� ��� ����
	// dynamic					4		������������, ����������
	// touchable				8		����������� �� �������
	// forced					16		�� ��������� ����������
	// one-side					64		��������� �������� ������ ����. 64 - ����� �� ������������ � OnPlane, ����� ��� ����.

	__INLINE BYTE IsSolid()				const	{ return physFlags & 1; }
	__INLINE BYTE IsBulletCollidable()	const	{ return physFlags & 2; }
	__INLINE BYTE IsDynamic()			const	{ return physFlags & 4; }
	__INLINE BYTE IsTouchable()			const	{ return physFlags & 8; }
	__INLINE BYTE IsForced()			const	{ return physFlags & 16; }
	__INLINE BYTE IsOneSide()			const	{ return physFlags & 64; }

	__INLINE void SetSolid()				{ physFlags |= 1; }
	__INLINE void SetBulletCollidable()		{ physFlags |= 2; }
	__INLINE void SetDynamic()				{ physFlags |= 4; }
	__INLINE void SetTouchable()			{ physFlags |= 8; }
	__INLINE void SetForced()				{ physFlags |= 16; }
	__INLINE void SetOneSide()				{ physFlags |= 64; }

	__INLINE void ClearSolid()				{ physFlags &= ~1; }
	__INLINE void ClearBulletCollidable() 	{ physFlags &= ~2; }
	__INLINE void ClearDynamic()			{ physFlags &= ~4; }
	__INLINE void ClearTouchable()			{ physFlags &= ~8; }
	__INLINE void ClearForced()				{ physFlags &= ~16; }
	__INLINE void ClearOneSide()			{ physFlags &= ~64; }

	virtual void Resize( float w, float h )
	{
		GameObject::Resize( w, h );
		rectangle[0] = Vector2( -aabb.W, -aabb.H );
		rectangle[1] = Vector2(  aabb.W, -aabb.H );
		rectangle[2] = Vector2(  aabb.W,  aabb.H );
		rectangle[3] = Vector2( -aabb.W,  aabb.H );
		geometry = &rectangle;
		update_needed = true;
	}

	virtual void Bounce( Vector2 n )
	{ UNUSED_ARG(n); }

	virtual bool PointIn( Vector2 point )
	{
		CPolygon box;
		CPolygon::MakeBoxFrom(box);
		Vector2 n;
		float d;
		return CPolygon::Collide( *geometry, aabb.p, Vector2(0.0f, 0.0f), Matrix2(0.0f), box, point, Vector2(0.0f, 0.0f), Matrix2(0.0f), n, d );
	}

	ObjPhysic() : 
		sap_handle(0),
		physFlags(0),
		bounce(0.0f),
		solid_to(0xff),
		slopeType(0),
		rectangle(4),
		custom_polygon(NULL),
		geometry(&rectangle),
		update_needed(false),
		particles_collidable(false),
		material( NULL ),
		foundation_for( NULL )
	{
		this->SetPhysic();

		//assert(aabb.W == 0.5f && aabb.H == 0.5f); //Why do we care again? This object is just being created we can set it just now
		aabb.W = 0.5f;
		aabb.H = 0.5f;

		rectangle[0] = Vector2( -0.5, -0.5 );
		rectangle[1] = Vector2(  0.5, -0.5 );
		rectangle[2] = Vector2(  0.5,  0.5 );
		rectangle[3] = Vector2( -0.5,  0.5 );
		rectangle.CalcBox();
	}

	~ObjPhysic()
	{
		if ( custom_polygon ) DELETESINGLE( custom_polygon );
		if (material) material->ReleaseUsage();
		if (foundation_for) foundation_for->parentDead();
	}

	virtual bool ApplyProto(const Proto* proto)
	{
		if (!GameObject::ApplyProto(proto))
			return false;
		
		bounce         = proto->bounce;		
		solid_to       = ~((BYTE)proto->ghost_to);
		slopeType      = static_cast<BYTE>(proto->slopeType);

		if (proto->phys_solid) SetSolid();
		if (proto->phys_one_sided) SetOneSide();
		if (proto->phys_one_sided & 2) SetForced();
		if (proto->phys_bullet_collidable) SetBulletCollidable();
		if (proto->material_name) material = GetMaterial( proto->material_name );
		else material = GetDefaultMaterial();

		if (material)
			material->ReserveUsage();

#ifdef MAP_EDITOR
		editor.editor_color = proto->editor_color;
		editor.really_physic = proto->physic != 0;
#endif // MAP_EDITOR

		return true;
	}

	virtual void Touch( ObjPhysic* obj )
	{ UNUSED_ARG(obj); }
	
	virtual void SetPolygon( CPolygon* poly )
	{
		if ( custom_polygon ) DELETESINGLE( custom_polygon );
		geometry = custom_polygon = poly;
		CBox box = poly->GetBox();
		aabb.W = box.Width()/2.0f;
		aabb.H = box.Height()/2.0f;
		update_needed = true;
	}
	
	// Mark that object rests on this
	virtual void PutOn( GameObject* obj )
	{
		if ( !foundation_for )
			foundation_for = new ObjectConnection( this );
		foundation_for->childDead( obj->id );
		foundation_for->children.push_back( obj );
	}
	
	virtual bool IsPhysicType()
	{
		return true;
	}
	
	virtual void PhysProcess() {}
	
protected:
	virtual CUData* createUData();
};


bool SolidTo( ObjPhysic* obj, ObjectType type );

#endif // __OBJECT_PHYSIC_H_
