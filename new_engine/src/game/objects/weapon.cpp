#include "StdAfx.h"

#include "weapon.h"
#include "../player.h"
#include "object_bullet.h"
#include "object_effect.h"
#include "object_ray.h"
#include "object_player.h"

#include "../proto.h"
#include "../../script/script.h"

#include "../../resource_mgr.h"

//////////////////////////////////////////////////////////////////////////

extern ResourceMgr<Proto> * protoMgr;

extern UINT internal_time;
extern vector<Player*> players;

extern lua_State* lua;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

Weapon::Weapon() :
		reload_time( 0 ),
		clip_reload_time( 0 ),
		last_reload_tick( 0 ),

		bullet_proto( NULL ),
		flash_proto( NULL ),
		charge_proto( NULL ),
		bullets_count( 0 ),
		shots_per_clip( 0 ),
		clip( 0 ),
		is_infinite( false ),

		is_ray( false ),
		recoil( 0.0f ),
		charge_hold( 0 ),
		charge( 0 ),
		last_charge_time( 0 ),

		fire_script( LUA_NOREF )
{ }

Weapon::~Weapon()
{
	SCRIPT::ReleaseProc(&fire_script);
}

// �������� ������ �� ���������
bool LoadWeaponFromProto(const Proto* proto, Weapon* weapon)
{
	if (!proto || !weapon)
		return true;

	weapon->recoil = proto->recoil;
	weapon->bullets_count = (USHORT)proto->bullets_count;
	if (proto->bullets_count <= 0)
	{
		// ����������� ��������
		weapon->is_infinite = true;
	}

	weapon->reload_time = proto->reload_time;
	weapon->clip_reload_time = proto->clip_reload_time;
	weapon->shots_per_clip = static_cast<USHORT>(proto->shots_per_clip);
	weapon->clip = weapon->shots_per_clip;
	weapon->bullet_proto = proto;
	weapon->flash_proto = protoMgr->GetByName( proto->flash, "effects/" );
	weapon->charge_proto = protoMgr->GetByName( proto->charge_effect, "effects/" );
	weapon->charge_hold = proto->charge_hold;

	weapon->is_ray = proto->is_ray_weapon != 0;
	weapon->fire_script = SCRIPT::ReserveAndReturnProc(proto->fire_script);

	return false;
}

// �������� ������
Weapon* CreateWeapon(const Proto* proto)
{
	if (!proto)
		return NULL;

	Weapon* weapon = new Weapon();

	if (LoadWeaponFromProto(proto, weapon))
	{
		DELETESINGLE(weapon);
		return NULL;
	}

	return weapon;
}

Weapon* CreateWeapon(const char* proto_name)
{
	if (!proto_name)
		return NULL;

	return CreateWeapon(protoMgr->GetByName(proto_name, "weapons/"));
}

//////////////////////////////////////////////////////////////////////////

// �������� ������� ������ ������
void Weapon::Charge(ObjCharacter* shooter)
{
	if ( charge_proto == NULL ) return;
	ObjEffect* charge_sprite = CreateEffect( charge_proto, false, shooter, 0, Vector2(0, 0) );
	if ( shooter->type == objPlayer ) static_cast<ObjPlayer*>(shooter)->SetChargeEffect( charge_sprite );
}

// ������� �� ������
void Weapon::Fire(ObjCharacter* shooter, Vector2 coord)
{
	if (!IsReady())
		return;

	if ( (!is_infinite) && bullets_count > static_cast<ObjPlayer*>(shooter)->ammo )
		return;
	
	if ( shots_per_clip > 0 ) clip--;

	if ( shooter->type == objPlayer ) static_cast<ObjPlayer*>(shooter)->SetChargeEffect( NULL );
	// ����������� ����������� ��������

	if (is_ray)
	{
		ObjRay* obr = NULL;
		if ( shooter->GetFacing() )
			obr = CreateRay(this->bullet_proto, coord, shooter, Const::Math::PI-shooter->weapon_angle);
		else
			obr = CreateRay(this->bullet_proto, coord, shooter, -shooter->weapon_angle);
		if ( obr )
		{
			uint8_t num = 0;
			for ( vector<Player*>::iterator iter = players.begin(); iter != players.end(); iter++ )
			{
				num++;
				if ( (*iter)->current && (*iter)->current == shooter )
				{
					obr->player_num = num;
					break;
				}
			}
			if ( shooter->type == objPlayer )
				obr->charge = static_cast<ObjPlayer*>(shooter)->weapon_charge_time;
			else
				obr->charge = 0;
		}
	}
	else
	{
		ObjBullet* ob = CreateAngledBullet(this->bullet_proto, coord, shooter, shooter->sprite->IsMirrored(), shooter->weapon_angle, 0);
		if ( ob )
		{
			uint8_t num = 0;
			for ( vector<Player*>::iterator iter = players.begin(); iter != players.end(); iter++ )
			{
				num++;
				if ( (*iter)->current && (*iter)->current == shooter )
				{
					ob->player_num = num;
					break;
				}
			}
			ob->sprite->setAngle(shooter->weapon_angle);
			ob->vel.x += shooter->vel.x;
			ob->acc += shooter->acc;
			if ( shooter->type == objPlayer )
				ob->charge = static_cast<ObjPlayer*>(shooter)->weapon_charge_time;
			else
				ob->charge = 0;
		}
	}
	ObjEffect* oe = CreateEffect( flash_proto, false, shooter, 0, Vector2(0, 0) );
	if ( oe )
	{
		//oe->sprite->angle = shooter->GetFacing() ? -shooter->weapon_angle : shooter->weapon_angle;
		oe->sprite->setAngle(shooter->weapon_angle);
	}
	if ( shooter->GetFacing() )
		shooter->vel -= Vector2( -cos(shooter->weapon_angle)*recoil, -sin(shooter->weapon_angle)*recoil );
	else
		shooter->vel -= Vector2( cos(shooter->weapon_angle)*recoil, sin(shooter->weapon_angle)*recoil );
	last_reload_tick = internal_time;
	if ( shooter->type == objPlayer )
	{
		ObjPlayer* op = static_cast<ObjPlayer*>(shooter);
		op->weapon_charge_time = 0;
		op->last_weapon_charge = 0;
	}
	if ( LUA_NOREF != fire_script )
	{
		shooter->pushUData( lua );
		lua_pushnumber( lua, coord.x );
		lua_pushnumber( lua, coord.y );
		int results = SCRIPT::ExecChunkFromReg( fire_script, 3 );
		if ( results > 0 )
		{
			if ( lua_istable( lua, -1 ) )
			{
				/*SCRIPT::GetFieldByName( lua, "reload_time", reload_time );
				SCRIPT::GetFieldByName( lua, "clip_reload_time", clip_reload_time );*/
				lua_pushstring( lua, "reload_time" );
				lua_rawget( lua, -2 );
				if ( lua_isnumber( lua, -1 ) )
				{
					reload_time = static_cast<UINT>(lua_tointeger(lua, -1));
				}
				lua_pop( lua, 1 );
				lua_pushstring( lua, "clip_reload_time" );
				lua_rawget( lua, -2 );
				if ( lua_isnumber( lua, -1 ) )
				{
					clip_reload_time = static_cast<UINT>(lua_tointeger(lua, -1));
				}
				lua_pop( lua, 1 );
			}
		}
	}
}

// �������� �� ����������������.
bool Weapon::IsReloaded()
{
	//if ( clip == 0 && shots_per_clip != 0 ) return false;
	return internal_time - last_reload_tick >= reload_time;
}

bool Weapon::ClipReloaded(bool hold)
{
	if ( hold ) return internal_time - last_reload_tick >= clip_reload_time*2;
	return internal_time - last_reload_tick >= clip_reload_time;
}

// �������� �� ���������� ������ � ��������
bool Weapon::IsReady()
{
	return IsReloaded() && ClipReloaded( false );
}


