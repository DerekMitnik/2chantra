#include "StdAfx.h"

#include "object_environment.h"
#include "object_dynamic.h"

#include "../object_mgr.h"

#include "../../script/script.h"
#include "../../resource_mgr.h"

//////////////////////////////////////////////////////////////////////////

extern ResourceMgr<Proto> * protoMgr;
extern lua_State* lua;
extern UINT internal_time;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

ObjEnvironment* default_environment = NULL;

bool ObjEnvironment::ApplyProto(const Proto* proto)
{
	if (!ObjPhysic::ApplyProto(proto))
		return false;

	// TODO: �� ������ ���-��. ������� ������ � LoadObjectFromProto, ����� ����� ������� ���.
	// �� �� �� �������, ��������� ��� ����� ���������.
	DELETESINGLE(sprite);

	if ( proto->env_func_onenter > 0 ) 
		on_enter = SCRIPT::ReserveAndReturnProc(proto->env_func_onenter);
	else 
		script_on_enter = StrDupl(proto->env_onenter);

	if ( proto->env_func_onleave > 0 )
		on_leave = SCRIPT::ReserveAndReturnProc(proto->env_func_onleave);
	else 
		script_on_leave = StrDupl(proto->env_onleave);

	if ( proto->env_func_onstay > 0 )
		on_stay = SCRIPT::ReserveAndReturnProc(proto->env_func_onstay);
	else 
		script_on_stay = StrDupl(proto->env_onstay);

	if ( proto->env_func_onuse > 0 )
		on_use = SCRIPT::ReserveAndReturnProc(proto->env_func_onuse);
	else 
		script_on_use = StrDupl(proto->env_onuse);

	walk_vel_multiplier = proto->walk_vel_multiplier;
	jump_vel_multiplier = proto->jump_vel_multiplier;
	material_num        = proto->env_material;
	gravity_bonus       = proto->gravity_bonus;
	stop_global_wind    = proto->stop_global_wind != 0;
	wind                = proto->wind;

	int i = 0;
	for(vector<char*>::const_iterator it = proto->sprites.begin();
		it != proto->sprites.end();
		it++)
	{
		sprites[i] = StrDupl(*it);
		if (++i == ObjEnvironment::OBJ_ENV_ARRAYS_SIZE) break;
	}

	i = 0;
	for(vector<char*>::const_iterator it = proto->sounds.begin();
		it != proto->sounds.end();
		it++)
	{
		sounds[i] = StrDupl(*it);
		if (++i == ObjEnvironment::OBJ_ENV_ARRAYS_SIZE) break; 
	}

	return true;
}

ObjEnvironment* CreateEnvironment(const char* proto_name, Vector2 coord)
{
	const Proto* proto = protoMgr->GetByName( proto_name, "environments/" );
	if (!proto)
		return NULL;

	ObjEnvironment* env = new ObjEnvironment();

	if (!env->ApplyProto(proto))
	{
		DELETESINGLE(env);
		return NULL;
	}
	
	env->aabb.p = coord;

	AddObject(env);

#ifdef MAP_EDITOR
	env->editor.proto_name = new char[ strlen(proto_name) + 1 ];
	strcpy( env->editor.proto_name, proto_name );
#endif //MAP_EDITOR

	return env;
}

ObjEnvironment* CreateDummyEnvironment()
{
	ObjEnvironment* obj = new ObjEnvironment;
	AddObject(obj);
	return obj;
}


char* ObjEnvironment::GetSound( int index )
{
	assert(0 <= index && index < OBJ_ENV_ARRAYS_SIZE);
	return *(sounds + index);
}

char* ObjEnvironment::GetSprite( int index )
{
	assert(0 <= index && index < OBJ_ENV_ARRAYS_SIZE);
	return *(sprites + index);
}

Vector2 TraceVelBack( ObjDynamic* eob, ObjEnvironment* area, bool leaving )
{
	const Vector2 vel = leaving ? eob->vel + eob->own_vel : -(eob->vel + eob->own_vel);
	if ( vel.Length() == 0 )
	{
		return eob->aabb.p;
	}
	Vector2 pnt = Vector2(0, 0);
	const float x = vel.x < 0.0f ? area->aabb.Left() : area->aabb.Right();
	const float y = vel.y < 0.0f ? area->aabb.Top() : area->aabb.Bottom();
	float sx = eob->aabb.p.x;
	float sy = eob->aabb.p.y;

	sy = vel.x == 0.0f ? sy : eob->aabb.p.y + ( vel.y * ( x - eob->aabb.p.x ) ) / vel.x;
	sx = vel.y == 0.0f ? sx : eob->aabb.p.x + ( vel.x * ( y - eob->aabb.p.y ) ) / vel.y;

	if ( sx <= area->aabb.Right() && sx >= area->aabb.Left() )
	{
		pnt.x = sx;
		pnt.y = y;
	}
	else
	{
		pnt.x = x;
		pnt.y = sy;
	}
	return pnt;
}

void ObjEnvironment::OnEnter( GameObject* obj )
{
	if ( obj->type == objParticleSystem ) return;
	if ( !this->script_on_enter && this->on_enter <= 0 ) return;
	ObjDynamic* eob = (ObjDynamic*)obj;

	//���� ����� ��� �������� ����������
	Vector2 pnt = TraceVelBack(eob, this, false);
	if ( script_on_enter )
	{
		lua_getglobal(lua, this->script_on_enter);
		if (!lua_isfunction(lua, -1))
		{
			DELETEARRAY( script_on_enter );
			return;
		}
	}
	eob->pushUData(lua);
	lua_pushnumber(lua, pnt.x);
	lua_pushnumber(lua, pnt.y);
	//lua_pushinteger(lua, eob->old_env->material_num);
	lua_pushinteger(lua, this->material_num);
	this->pushUData(lua);
	if ( script_on_enter )
		SCRIPT::ExecChunk(5);
	else
		SCRIPT::ExecChunkFromReg( on_enter, 5 );
}

void ObjEnvironment::OnLeave( GameObject* obj )
{
	if ( obj->type == objParticleSystem ) return;
	if ( !this->script_on_leave && this->on_leave <= 0 ) return;
	ObjDynamic* eob = (ObjDynamic*)obj;
	//���� ����� ��� �������� ����������, ����������� ������ ����� ���.
	Vector2 pnt = TraceVelBack(eob, this, true);

	if ( script_on_leave )
	{
		lua_getglobal(lua, this->script_on_leave);
		if (!lua_isfunction(lua, -1))
		{
			DELETEARRAY( script_on_leave );
			return;
		}
	}
	eob->pushUData(lua);
	lua_pushnumber(lua, pnt.x);
	lua_pushnumber(lua, pnt.y);
	lua_pushinteger(lua, this->material_num);
	lua_pushinteger(lua, eob->env->material_num);
	this->pushUData(lua);
	if ( script_on_leave )
		SCRIPT::ExecChunk(6);
	else
		SCRIPT::ExecChunkFromReg( on_leave, 6 );
}

void ObjEnvironment::OnStay( GameObject* obj )
{
	if ( obj->type == objParticleSystem ) return;
	if ( !this->script_on_stay && this->on_stay <= 0 ) return;
	ObjDynamic* eob = (ObjDynamic*)obj;

	if ( internal_time - eob->last_env_on_stay < 500 )
		return;
	
	eob->last_env_on_stay = internal_time;
	
	if ( this->script_on_stay )
	{
		lua_getglobal(lua, this->script_on_stay);
		if (!lua_isfunction(lua, -1))
		{
			DELETEARRAY( script_on_stay );
			return;
		}
	}
	eob->pushUData(lua);
	lua_pushnumber(lua, eob->aabb.p.x);
	lua_pushnumber(lua, eob->aabb.p.y);
	this->pushUData(lua);
	if ( this->script_on_stay )
		SCRIPT::ExecChunk(4);
	else
	{
		SCRIPT::ExecChunkFromReg( on_stay, 4 );
	}
}

void ObjEnvironment::OnUse( GameObject* obj )
{
	if ( !this->script_on_use && this->on_use <= 0 ) return;
	ObjDynamic* eob = (ObjDynamic*)obj;

	if ( script_on_use )
	{
		lua_getglobal(lua, this->script_on_use);
		if (!lua_isfunction(lua, -1))
		{
			DELETEARRAY( script_on_use );
			return;
		}

	}
	eob->pushUData(lua);
	lua_pushnumber(lua, eob->aabb.p.x);
	lua_pushnumber(lua, eob->aabb.p.y);
	this->pushUData(lua);
	if ( script_on_use )
		SCRIPT::ExecChunk(4);
	else
		SCRIPT::ExecChunkFromReg( on_use, 4 );
}

void DefaultEnvDelete()
{
	if (default_environment) DELETESINGLE(default_environment);
}

void DefaultEnvSet( ObjEnvironment* env )
{
	if ( env )
	{
		default_environment->walk_vel_multiplier = env->walk_vel_multiplier;
		default_environment->jump_vel_multiplier = env->jump_vel_multiplier;
		default_environment->gravity_bonus = env->gravity_bonus;
		default_environment->material_num = env->material_num;
		for ( int i = 0; i < ObjEnvironment::OBJ_ENV_ARRAYS_SIZE; i++ )
		{
			if (default_environment->sprites[i]) DELETESINGLE(default_environment->sprites[i]);
			if (default_environment->sounds[i]) DELETESINGLE(default_environment->sounds[i]);
			if (env->sprites[i]) 
			{
				default_environment->sprites[i] = new char[strlen(env->sprites[i]) + 1];
				strcpy(default_environment->sprites[i], env->sprites[i]);
			}
			if (env->sounds[i]) 
			{
				default_environment->sounds[i] = new char[strlen(env->sounds[i]) + 1];
				strcpy(default_environment->sounds[i], env->sounds[i]);
			}
		}
		//memcpy(default_environment, env, sizeof(ObjEnvironment)); <= �� ���������, ������ ��� ������, �� ������� ��������� ������ ����� ���� ������� �� ������ ����������.
	}
	else 
	{
		DefaultEnvDelete();
		default_environment = new ObjEnvironment();
	}
}

ObjEnvironment::ObjEnvironment() :
	script_on_enter(NULL),
	on_enter(LUA_NOREF),
	script_on_leave(NULL),
	on_leave(LUA_NOREF),
	script_on_stay(NULL),
	on_stay(LUA_NOREF),
	script_on_use(NULL),
	on_use(LUA_NOREF),
	walk_vel_multiplier(1.0f),
	jump_vel_multiplier(1.0f),
	material_num(0),
	gravity_bonus(0.0f, 0.0f)
{
	this->type = objEnvironment;

	sounds = new char*[OBJ_ENV_ARRAYS_SIZE];
	std::fill(sounds, sounds + OBJ_ENV_ARRAYS_SIZE, (char*)NULL);

	sprites = new char*[OBJ_ENV_ARRAYS_SIZE];
	std::fill(sprites, sprites + OBJ_ENV_ARRAYS_SIZE, (char*)NULL);
}


ObjEnvironment::~ObjEnvironment()
{
	DELETEARRAY( script_on_enter );
	DELETEARRAY( script_on_leave );
	DELETEARRAY( script_on_stay );
	DELETEARRAY(script_on_use);
	for ( int i = 0; i < OBJ_ENV_ARRAYS_SIZE; i++ )
	{
		DELETEARRAY(sounds[i]);
		DELETEARRAY(sprites[i]);
	}
	DELETEARRAY(sounds);
	DELETEARRAY(sprites);
	SCRIPT::ReleaseProc(&on_use);
	SCRIPT::ReleaseProc(&on_enter);
	SCRIPT::ReleaseProc(&on_leave);
	SCRIPT::ReleaseProc(&on_stay);
}
