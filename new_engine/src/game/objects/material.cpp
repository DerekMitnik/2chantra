#include "StdAfx.h"
#include "material.h"
#include "../../resource_mgr.h"

extern ResourceMgr<Proto> * protoMgr;

typedef map<std::string, Material*> MaterialMap;
static MaterialMap material_map;

bool Material::Load( const Proto* proto )
{
	if ( proto == NULL )
		return false;

	type = proto->env_material;

	size_t i = 0;
	for(vector<char*>::const_iterator it = proto->sprites.begin();
		it != proto->sprites.end();
		it++)
	{
		sprites[i] = StrDupl(*it);
		if (++i == ARRAYS_SIZE) break;
	}

	i = 0;
	for(vector<char*>::const_iterator it = proto->sounds.begin();
		it != proto->sounds.end();
		it++)
	{
		sounds[i] = StrDupl(*it);
		if (++i == ARRAYS_SIZE) break; 
	}

	bounce_bonus = proto->bounce_bonus;
	friction = proto->friction;

	return true;
}

const char* Material::GetSound( size_t index )
{
	assert(0 <= index && index < ARRAYS_SIZE);
	return *(sounds + index);
}

const char* Material::GetSprite( size_t index )
{
	assert(0 <= index && index < ARRAYS_SIZE);
	return *(sprites + index);
}

Material* GetMaterial(const char* name )
{	
	if (!name)
		return NULL;

	Material* ret = NULL;
	std::string n(name);
	MaterialMap::iterator it = material_map.find(n);
	if ( it == material_map.end() )
	{
		ret = new Material();
		ret->Load( protoMgr->GetByName( n, string("materials/") ) );
		material_map[n] = ret;
	}
	else
		ret = it->second;
	return ret;
}

Material* GetDefaultMaterial()
{
	SuppressLogMessages( logLevelAll );
	Material* ret = GetMaterial( DEFAULT_MATERIAL_NAME );
	SuppressLogMessages( logLevelNone );
	return ret;
}

void ClearMaterials()
{
	for ( MaterialMap::iterator iter = material_map.begin(); iter != material_map.end(); iter++ )
		DELETESINGLE( iter->second );
	material_map.clear();
}

void ClearUnusedMaterials()
{
	MaterialMap::iterator it = material_map.begin();
	while(it != material_map.end())
	{
		if (it->second->IsUnUsed())
		{
			DELETESINGLE(it->second);
			material_map.erase(it++);
		}
		else
		{
			Log(DEFAULT_LOG_NAME, logLevelInfo, "Cannot unload Material name %s, it's still used", it->first.c_str());
			++it;
		}
	}
}