#ifndef __OBJECT_WEAPON_H_
#define __OBJECT_WEAPON_H_

// ��������������� ����������� ������ �������.
// ��� ��� �� ����� ��������, �� � ������ ������� ��� �����
// �� ���� ������� ���������� �� ����.
class ObjCharacter;
class ObjEffect;
class Proto;
class Vector2;

// ����������� ��������. ������������, ����� ���������� ����.
enum WeaponDirection { wdRight, wdLeft, wdUpLeft, wdUpRight, wdDownLeft, wdDownRight/*, wdUp, wdDown*/ };



//////////////////////////////////////////////////////////////////////////

class Weapon
{
public:
	UINT reload_time;			// �����, ������� ������� �� ����������� ����� ��������
	UINT clip_reload_time;
	UINT last_reload_tick;		// ����� ������ �����������

	const Proto* bullet_proto;		// ��������, �� �������� ��������� ����
	const Proto* flash_proto;	// Muzzle flash, if any
	const Proto* charge_proto;  // Charge indicator, if any
	USHORT bullets_count;		// ���������� ���� �� �������.
	USHORT shots_per_clip;
	USHORT clip;
	bool is_infinite;			// ���������� �� ����

	bool is_ray;
	float recoil;
	UINT charge_hold;
	UINT charge;
	UINT last_charge_time;

	LuaRegRef fire_script;

	Weapon();
	~Weapon();

	enum KeyState { none, pressed, released };

	void Charge(ObjCharacter* shooter);
	void Fire(ObjCharacter* shooter, Vector2 coord);
	bool IsReloaded();
	bool ClipReloaded(bool hold);
	bool IsReady();
};

Weapon* CreateWeapon(const char* proto_name);

#endif // __OBJECT_BULLET_H_
