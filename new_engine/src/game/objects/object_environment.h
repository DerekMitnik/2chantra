#ifndef __OBJECT_ENVIRONMENT_H_
#define __OBJECT_ENVIRONMENT_H_

#include "object_physic.h"

class ObjEnvironment : public ObjPhysic
{
	public:
		enum { OBJ_ENV_ARRAYS_SIZE = 32 };

		char* script_on_enter;
		LuaRegRef on_enter;
		char* script_on_leave;
		LuaRegRef on_leave;
		char* script_on_stay;
		LuaRegRef on_stay;
		char* script_on_use;
		LuaRegRef on_use;
		float walk_vel_multiplier;
		float jump_vel_multiplier;
		char** sounds;
		char** sprites;
		int material_num;
		Vector2 gravity_bonus;
		bool stop_global_wind;
		Vector2 wind;

		ObjEnvironment();
		~ObjEnvironment();

		virtual bool ApplyProto(const Proto* proto);

		char* GetSound( int index );
		char* GetSprite( int index );
		void OnEnter( GameObject* obj );
		void OnLeave( GameObject* obj );
		void OnStay( GameObject* obj );
		void OnUse( GameObject* obj );
protected:
	virtual CUData* createUData();
};

ObjEnvironment* CreateEnvironment(const char* proto_name, Vector2 coord);
ObjEnvironment* CreateDummyEnvironment();
void DefaultEnvDelete();
void DefaultEnvSet( ObjEnvironment* env );

#endif
