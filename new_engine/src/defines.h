#ifndef __DEFINES_H_
#define __DEFINES_H_

// ��������, �������� ����� ��� �����-������ ����-������ ����������.
#define _IICHANTRA_PRESENT_

#define GAMENAME "iiChantra"
#define ENGINE "iiChantra kernel_engine"
#define VERSION "v. 1.0.4"

#ifndef __DATE__
#define __DATE__ "unspecified build date"
#endif

#ifndef __TIME__
#define __TIME__ "unspecified build time"
#endif

//////////////////////////////////////////////////////////////////////////

#ifndef __INLINE
#if defined(_MSC_VER)
	#define __INLINE __forceinline
#elif __GNUC__ >= 4
	#define __INLINE __inline __attribute__ ((always_inline))
#else
	#define __INLINE inline
#endif
#endif

//#define USE_SAP_ASM

#ifdef USE_SAP_ASM
	#if defined(WIN32) && !defined(__MINGW32__)
		#define SAP_VC_ASM
	#else
		//#define SAP_GCC_ASM  // !!!! DO NOT USE !!!!
	#endif // WIN32
#endif

#ifndef MAX_PATH
	#define MAX_PATH PATH_MAX
#endif

#define UNUSED_ARG(x)	(void)x

//////////////////////////////////////////////////////////////////////////

// � ��� ���� ����������� �� ������� ���� � ����� (��� ������, �� ����� ������� RadixSort).
// ����������� ��������� (� ��� ����� Intel-Mac) ���������� little endian.
#ifndef II_LITTLE_ENDIAN
	#define II_LITTLE_ENDIAN
#endif

//////////////////////////////////////////////////////////////////////////
template<typename T>
__INLINE void DELETESINGLE(T*& a)
{
	delete a, a = NULL;
}

template<typename T>
__INLINE void DELETEARRAY(T*& a)
{
	delete [] a, a = NULL;
}
//////////////////////////////////////////////////////////////////////////
// ���������

//  ����������� ������ ����
#define DEFAULT_WINDOW_WIDTH		640
#define DEFAULT_WINDOW_HEIGHT		480

// ����������� ���� ����� ��������� � Linux-�������
#if defined(LINUX)
#define DEFAULT_LINUX_APPLICATION_PATH		"/usr/games/"
#define DEFAULT_LINUX_APPLICATION_DATA_PATH	"/usr/share/games/iichantra/bin/"
#define DEFAULT_LINUX_IICHANTRA_PATH		".iichantra/"
#endif //defined(LINUX)

// ����������� ���� � �����
#define DEFAULT_CONFIG_PATH				"config/"
#ifdef MAP_EDITOR
#define DEFAULT_CONFIG_NAME				"editor.lua"
#else
#define DEFAULT_CONFIG_NAME				"default.lua"
#endif //MAP_EDITOR

#define DEFAULT_TEXTURES_PATH			"textures/"
#define DEFAULT_FONTS_PATH				"fonts/"
#define DEFAULT_DATA_PATH				"data/"
#define DEFAULT_PROTO_PATH				"proto/"
#define DEFAULT_SCRIPTS_PATH			"scripts/"
#define DEFAULT_LOG_PATH				"log/"
#define DEFAULT_LEVELS_PATH				"levels/"
#define DEFAULT_SOUNDS_PATH				"sounds/"
#define DEFAULT_SCREENSHOTS_PATH		"screenshots/"
#define DEFAULT_SAVES_PATH				"saves/"

#define DEFAULT_MAIN_ARCHIVE_NAME		"data/iiChantra.zip"
#define DEFAULT_LOG_NAME				"iichantra.log"
#define DEFAULT_GUI_LOG_NAME			"gui.log"
#define DEFAULT_SCRIPT_LOG_NAME			"script.log"
#define DEFAULT_NET_LOG_NAME			"network.log"
//#define DEFAULT_CONS_LOG_NAME			"console.log"

#ifdef MAP_EDITOR
	#define DEFAULT_INIT_SCRIPT			"editor/editor_init.lua"
#else
	#define DEFAULT_INIT_SCRIPT			"init.lua"
#endif //MAP_EDITOR

#define DEFAULT_FONT					"default"
#define TEXT_FONT						"dialogue"
//#define NO_BINARY_FONTS

#define LUA_ERROR_SCRIPT			"_on_error"
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#define MAXIMUM_SOUND_DISTANCE			1024.0f

//////////////////////////////////////////////////////////////////////////

#define MAX_JOYSTICKS					4
#define MAX_JOYSTICK_AXES				4
#define MAX_JOYSTICK_BUTTONS			20
#define JOYSTICK_VK						(MAX_JOYSTICK_AXES * 2 + MAX_JOYSTICK_BUTTONS + 4)

//////////////////////////////////////////////////////////////////////////
enum LogLevel
{
	logLevelNone, logLevelInfo, logLevelWarning, logLevelError, logLevelScript, logLevelAll
};

//////////////////////////////////////////////////////////////////////////
enum ProgrammStates {
	PROGRAMM_LOADING,
	PROGRAMM_RUNNING,
	PROGRAMM_INACTIVE,
	//GAME_RUNNING,
	//GAME_GAMEMENU,
	//GAME_PAUSED,
	PROGRAMM_EXITING
};

//////////////////////////////////////////////////////////////////////////

//#define KEYDOWN(vk_code)((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)
#define ADJUST_Z_FOR_SCREEN_POS	// ������� ����� � ���� �� ������ �������� ���� ����

/////////////////////////////////////////////////////////////////////////


#define COORD_LEFT_UP_CORNER	// ��� �������� �������� ���������� ���������
								// �� ����� ������� ���� �������, � �� �� �����
								// ���. �������. ������� ��� ������������� � ����������.


//#define STOP_RENDER_ON_INACTIVE_NETGAME		// ������������� ������ ��� ���������� ����
											// ���������� ��� ������� ����

#define SELECTIVE_RENDERING     // �������� ���������, ��� ������� �������������� ������ ��, ��� �� ������
#ifdef SELECTIVE_RENDERING
//#define DEBUG_SELECTIVE_RENDERING_SPRITES
#undef ENGINE
#define ENGINE "iiChantra kernel_engine (SELECTIVE_RENDERING on)"
#endif

#define GAMETICK_FROM_CONFIG	// ��������� GAMETICK �� ����� CONFIG
#define USE_TIME_DEPENDENT_PHYS	// ������������ ������, ���������� �� �������


#define DEBUG_LOG_INFO

#ifdef DEBUG_LOG_INFO
	//#define DEBUG_SAP
	//#define DEBUG_SAP_PAIRS_COORDS
#endif // DEBUG_LOG_INFO

#ifdef MAP_EDITOR
	#define DEBUG_RENDER	
#endif //MAP_EDITOR

//#define DEBUG_INSTANT_RENDERING		// �������� ������ ���������, �� ������������ ������ ���������.

#define DEBUG_INGAME_INFO		// ����� ������������� ���������� ���������� � �������

#ifdef DEBUG_INGAME_INFO
	//#define DEBUG_PRINT				// ����� ���������� �������
	#define DEBUG_RENDER			// ���������� �������

	#ifdef DEBUG_PRINT
		#define DEBUG_PRINT_BM_RENDERED_COUNT
	#endif // DEBUG_PRINT

	#ifdef DEBUG_RENDER
		//#define DEBUG_RENDER_UNLOADED_TEXTURES		// ��������� ������������ ������� � ���� ������� ���������������
		#define DEBUG_UNLOADED_TEXTURES_COLOR 1.0f, 0.5f, 0.25f		// ���� �������������� ������ ������������� ��������

		//#define DEBUG_DRAW_SPRITES_BORDERS		// ��������� ������ ��������
		#define DEBUG_SPRITES_BORDERS_COLOR	0.0f, 0.5f, 0.25f, 1.0f

		//#define DEBUG_DRAW_SPRITES_BORDERS_SEL_RD		// ��������� ������ ����������� � graphSap ��������
		#define DEBUG_SPRITES_BORDERS_SEL_RD_COLOR	1.0f, 0.8f, 0.06f, 1.0f

		//#define DEBUG_DRAW_OBJECTS_BORDERS		// ��������� ������ ��������	(�� ��������)
		//#define DEBUG_OBJECTS_BORDERS_COLOR 0.0f, 0.5f, 0.5f, 1.0f

		#define DEBUG_DRAW_WAYPOINTS							// ��������� ����������
		#define DEBUG_WAYPOINT_COLOR 0.25f, 0.25f, 1.0f, 1.0f	// 

		#define DEBUG_DRAW_PHYSOBJ_BORDERS_ONLY	// ��������� ������� ������ ���������� ��������
		#ifdef DEBUG_DRAW_PHYSOBJ_BORDERS_ONLY
			#define DEBUG_PHYSOBJ_BORDER_COLOR 1.0f, 0.5f, 0.0f, 1.0f
			#undef DEBUG_DRAW_OBJECTS_BORDERS
		#endif // DEBUG_DRAW_PHYSOBJ_BORDERS_ONLY

		#define DEBUG_POLYGON_COLOR 0.0f, 1.0f, 0.5f, 1.0f

		//#define DEBUG_DRAW_MP0_POINT
		#ifdef DEBUG_DRAW_MP0_POINT
			#define DEBUG_MP0_POINT_COLOR 1.0f, 0.0f, 0.0f, 1.0f
		#endif // DEBUG_DRAW_PHYSOBJ_BORDERS_ONLY

		//#define DEBUG_DRAW_COLLISION_PAIRS					// ��������� ��� �����������
		#ifdef DEBUG_DRAW_COLLISION_PAIRS
			#define DEBUG_COLLISION_PAIRS_COLOR 1.0f, 1.0f, 0.0f, 1.0f
		#endif // DEBUG_DRAW_PHYSOBJ_BORDERS_ONLY

		#define DEBUG_DRAW_SUSPECTED_PLAIN					// ��������� ����� � suspected_plane
		#ifdef DEBUG_DRAW_SUSPECTED_PLAIN
			#define DEBUG_DRAW_SUSPECTED_PLAIN_COLOR 0.7f, 0.7f, 0.7f, 1.0f
		#endif // DEBUG_DRAW_PHYSOBJ_BORDERS_ONLY

		#define DEBUG_DRAW_SPEED_VECTORS				// ��������� ��� �����������
		#ifdef DEBUG_DRAW_SPEED_VECTORS
			#define DEBUG_DRAW_SPEED_VEC_COLOR 1.0f, 1.0f, 1.0f, 1.0f
			#define DEBUG_DRAW_GRAV_BONUS_COLOR 1.0f, 1.0f, 0.0f, 1.0f
		#endif // DEBUG_DRAW_PHYSOBJ_BORDERS_ONLY

		#define DEBUG_DRAW_PHYSOBJ_ID							// ������ �� ���������� �������� �� id �������
		#ifdef DEBUG_DRAW_PHYSOBJ_ID
			#define DEBUG_PHYSOBJ_ID_FONT_COLOR 1.0f, 1.0f, 1.0f, 1.0f
			#define DEBUG_PHYSOBJ_ID_FONT_OUTLINE_COLOR 0.0f, 0.0f, 0.0f, 1.0f
		#endif // DEBUG_DRAW_PHYSOBJ_BORDERS_ONLY

	#endif // DEBUG_RENDER
#endif // DEBUG_INGAME_INFO

//#define BMP_SCREENSHOTS
#define DEBUG_LUA_CONSTANTS

#define MIN_X_BOUNCE_SPEED 0.0f
#define MIN_Y_BOUNCE_SPEED 4.0f


#define GUI_SETFOCUS_ON_MOUSEMOVE		// ������������� ����� �� �������, ����� ��� ��� �������� �����
//#define GUI_SETFOCUS_ON_MOUSECLICK		// ������������� ����� �� �������, ����� �� ��� ������� <- �� �����? ���� ��� ��� ���...
//#define GUI_UNSETFOCUS_ON_MOUSELEAVE	// ������� ����� � �������, ����� � ���� ������� ������ �����
#define GUI_UNSETFOCUS_ON_MOUSECLICK	// ������� ����� � �������, ���� �� ��� ������� ������ �����.

#define  CLOSE_ON_ALT_F4				// ���������� ����� ������� �� ������� alt+F4

// ��� win32 SDL_win32_main.cpp
#define NO_STDIO_REDIRECT		// �� �������������� ����������� ������ � ����
#define USE_MESSAGEBOX			// ���������� MessageBox ��� ��������� ������ �������������

#define SHADOW_DISTANCE			1480
#define SHADOW_Z				-0.3f
#define SHADOW_COLOR			0, 0, 0, 0.4f

#define BLENDING_HACK_Z			0.8f

// ���������� �������� ������ � ������������
#define KEY_SETS_NUMBER 4
// ������������ ����� �������� ������. ������������ ��� ��������� ������-������, 
// ����� �������� ���������, ����� ����� ����� ������� ���������.
#define MAX_VKEY_NAME_LEN 30


//#define MAP_EDITOR
#define NO_WRITE_SCREEN_CFG_ON_RESIZE_EDITOR

//#define NET_DEBUG
#define NET_TIMEOUT 1000
#define NET_MAX_PLAYERS 15 //+1 server
#define NET_DEFAULT_PORT 2800
#define NET_PACKET_RETRY_TIME 750
#define NET_ID_BATCH_SIZE 256

#define NET_MSG_HELLO "iiC?"
#define NET_MSG_ACK "iiC!"

#define DEFAULT_MATERIAL_NAME "default"

//#define PREFER_PACKED //Prefer archived packed to unpacked ones.

//#define PARTICLES_EXTRA_PHYS_CHECKS	//�������������� �������� ���������� ������ �� ������ ����. ��������.

//#define USE_SDL_MIXER
#ifdef USE_SDL_MIXER
#define NOSOUND_BASS
#endif

#endif // __DEFINES_H_
