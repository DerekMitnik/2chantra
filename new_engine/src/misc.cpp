#include "StdAfx.h"
#include "misc.h"

#ifdef WIN32
	#include <direct.h>
	#include <io.h>
	#include "dirent/dirent.h"
#else
	#ifdef __APPLE__
		#include <mach-o/dyld.h>	// ��� _NSGetExecutablePath
	#elif defined(__FreeBSD__)
		#include <sys/sysctl.h>
	#endif
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <dirent.h>
	#include <unistd.h>
#endif // WIN32

#include "config.h"
#include "script/api.h"

extern config cfg;

char path_log[MAX_PATH];
char path_config[MAX_PATH];
char path_app[MAX_PATH];
char path_home[MAX_PATH];
char path_textures[MAX_PATH];
char path_fonts[MAX_PATH];
char path_protos[MAX_PATH];
char path_scripts[MAX_PATH];
char path_levels[MAX_PATH];
char path_sounds[MAX_PATH];
char path_screenshots[MAX_PATH];
char path_saves[MAX_PATH];
char path_data[MAX_PATH];

int log_suppression_level = -1;

bool IsFileExists(const char* path)
{
#ifdef WIN32
	return ( _access( path, 0 ) != -1 );
#else
	return ( access( path, F_OK ) != -1 );
#endif // WIN32

	//return (GetFileAttributes(path) != INVALID_FILE_ATTRIBUTES);
}

// ���������� ��� ����� ��� ����������,
// �.�. ��� ������ "textures/sky.bmp" ������ "sky".
// �������� � ������ ����� ������, �� �������� �������.
char* GetNameFromPath(const char* path)
{
	if(!path)
		return NULL;
	
	char* p = (char*)path + strlen(path);
	
	UINT l = 0;
	
	bool f = false;
	
	for(;;)
	{
		p--;
		
		if(f)
			l++;
		
		if(*p == '.')
			f = true;
		
		if(*p == '\\' || *p == '/' || p == path)
		{
			if(f)
			{
				char* name = new char[l+1];
				memset(name, '\0', l+1);
				memcpy(name, p + 1, l - 1);
				return name;
			}
			else
				return NULL;
		}
	}
	return NULL;
}

// ��������� ���������� ����� (���� ������ ����� �� �����).
// �� ��������� ����� ������, � ���������� ��������� �� ��������� � ��������.
char* ExtFromPath(const char* path)
{
	if(!path)
		return NULL;
	
	char* p = (char*)path + strlen(path);
	
	for(;;)
	{
		p--;
		
		if(*p == '.')
			return ++p;
		
		if(p == path)
			return NULL;
	}
	return NULL;
}


bool FileExists(const char* file_name)
{
#ifdef WIN32
	return ( _access( file_name, 0 ) != -1 );
#else
	return ( access( file_name, F_OK ) != -1 );
#endif // WIN32
}

int ChangeDir(const char* path)
{
#ifdef WIN32
	return _chdir(path);
#else
	return chdir(path);
#endif // WIN32
}

// ���� ���� � ����� � ���� ���������
// file_name - ��� �����
// path - ���� � �����, � ������� ������������ �����
// buffer - �������� ������, � ��� ����� ��������� ������ ���� � ��������� �����
// ������� ���������� true, ���� ���� ������. 
bool FindFile(const char * const file_name, const char * path, char * const buffer)
{
	strcpy(buffer, path);
	strcat(buffer, "/");
	strcat(buffer, file_name);
	
	if (IsFileExists(buffer))
	{
		return true;
	}
	else
	{
		buffer[0] = 0;
		
		DIR *dp;
		struct dirent *ep;
		char new_path[MAX_PATH];
		
		dp = opendir(path);
		if (dp != NULL)
		{
			while ( (ep = readdir(dp)) != NULL )
			{
				if (ep->d_type == DT_DIR && ep->d_name[0] != '.')
				{
					new_path[0] = 0;
					strcpy(new_path, path);
					strcat(new_path, "/");
					strcat(new_path, ep->d_name);
					
					if ( FindFile(file_name, new_path, buffer) )
						return true;
				}
			}
			closedir(dp);
		}
	}
	return false;
}

bool FindAllFiles(const char * path, char * const buffer, void (*callback)(const char* const) )
{
	buffer[0] = 0;
	
	DIR *dp;
	struct dirent *ep;
	char new_path[MAX_PATH];
	
	dp = opendir(path);
	if (dp != NULL)
	{
		while ( (ep = readdir(dp)) )
		{
			if (ep->d_type == DT_DIR && ep->d_name[0] != '.')
			{
				new_path[0] = 0;
				strcpy(new_path, path);
				strcat(new_path, "/");
				strcat(new_path, ep->d_name);
				
				FindAllFiles(new_path, buffer, callback);
			}
			else if (ep->d_name[0] != '.')
			{
				new_path[0] = 0;
				strcpy(new_path, path);
				strcat(new_path, "/");
				strcat(new_path, ep->d_name);
				
				callback(new_path);
			}
		}
		closedir(dp);
	}
	
	return false;
}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// ��������� ������� ���������

// ������� ����
void CleanupLogs()
{
	CreateLogFile(DEFAULT_LOG_NAME);
	CreateLogFile(DEFAULT_SCRIPT_LOG_NAME);
	CreateLogFile(DEFAULT_GUI_LOG_NAME);
	CreateLogFile(DEFAULT_NET_LOG_NAME);
}

// ��������� � ���� ������, ����������� �� ���������� �����������
void EndupLogs()
{
	static const char* endup_msg = "Finished logging";
	
	//_LogToFile(DEFAULT_LOG_NAME, endup_msg);
	//_LogToFile(DEFAULT_GUI_LOG_NAME, endup_msg);
	//_LogToFile(DEFAULT_SCRIPT_LOG_NAME, endup_msg);
	cfg.log_level = logLevelInfo;
	Log(DEFAULT_LOG_NAME, logLevelInfo, "%s: %s", DEFAULT_LOG_NAME, endup_msg);
	Log(DEFAULT_SCRIPT_LOG_NAME, logLevelInfo, "%s: %s", DEFAULT_SCRIPT_LOG_NAME, endup_msg);
	Log(DEFAULT_NET_LOG_NAME, logLevelInfo, "%s: %s", DEFAULT_NET_LOG_NAME, endup_msg);
}

// ������� ��� ����� �� ������, ���������� ������ ��� �����
void DeleteFileNameFromEndOfPathToFile(char *src)
{
	UINT i = static_cast<UINT>(strlen(src)-1);
	while(src[i] != '/' && src[i] != '\\' && src[i] != ':')
		i--;
	src[i+1] = 0;
}

// ������ ����
void InitPaths(void)
{
#ifdef WIN32
	// application
	GetModuleFileName(GetModuleHandle(0), path_app, MAX_PATH);
	DeleteFileNameFromEndOfPathToFile(path_app);
	ChangeDir(path_app);
	
	GetCurrentDirectory(MAX_PATH, path_app);
	strcat( path_app, "/" );
	strcpy( path_home, path_app );
#elif defined(__APPLE__)
	uint32_t size = sizeof(path_app);
	if (_NSGetExecutablePath(path_app, &size) == 0)
	{
		printf("\nexecutable path is %s\n", path_app);
		DeleteFileNameFromEndOfPathToFile(path_app);
		path_app[strlen(path_app)-1] = 0;
		if (realpath(path_app, path_home) != path_home)
		{
			perror("realpath");
			exit(1);
		}
		strcpy(path_app, path_home);
		strcat(path_app, "/");
		printf("working path will be %s\n", path_app);
		ChangeDir(path_app);
	}
	else
	{
		printf("buffer too small; need size %u\n", size);
	}
	strcpy(path_home, path_app);
	
#elif defined(__linux__)
#if defined(IICHANTRA_INSTALL)
	strcpy(path_app, DEFAULT_LINUX_APPLICATION_DATA_PATH);
	strcpy(path_home, getenv("HOME"));
	strcat(path_home, "/");
	strcat(path_home, DEFAULT_LINUX_IICHANTRA_PATH);
#else
	// TODO: �����  ������ ��� linux. �������� �� ������ *nix-������.
	ssize_t count = readlink("/proc/self/exe", path_app, MAX_PATH );
	if (count > 0)
	{
		path_app[count] = 0;
		DeleteFileNameFromEndOfPathToFile(path_app);
		strcpy(path_home, path_app);
		ChangeDir(path_app);
		printf("path_app: %s\n", path_app);
	}
	else
	{
		printf("Error getting application path");
		exit(EXIT_FAILURE);
	}
#endif // defined(IICHANTRA_INSTALL)
#elif defined(__FreeBSD__)
	memset(path_app, 0, MAX_PATH);
	bool res = readlink("/proc/curproc/file", path_app, MAX_PATH) > 0;

	if (!res)
	{
		puts("readlink failed. Trying to use sysctl");
		int mib[4];
		mib[0] = CTL_KERN;
		mib[1] = KERN_PROC;
		mib[2] = KERN_PROC_PATHNAME;
		mib[3] = -1;
		size_t cb = MAX_PATH;
		res = sysctl(mib, 4, path_app, &cb, NULL, 0) == 0;
	}

	if (res)
	{
		printf("executable path: %s\n", path_app);
		DeleteFileNameFromEndOfPathToFile(path_app);
		strcpy(path_home, path_app);
		ChangeDir(path_app);
		printf("path_app: %s\n", path_app);
	}
	else
	{
		printf("Error getting application path");
		exit(EXIT_FAILURE);
	}
#else
	#error Unsupported platform.
#endif // WIN32
	
	// log
	strcpy(path_log, path_home);
	strcat(path_log, DEFAULT_LOG_PATH);
	
	// config
	strcpy(path_config, path_home);
	strcat(path_config, DEFAULT_CONFIG_PATH);
	
	// textures
	strcpy(path_textures, path_app);
	strcat(path_textures, DEFAULT_TEXTURES_PATH);
	
	// fonts
	strcpy(path_fonts, path_app);
	strcat(path_fonts, DEFAULT_FONTS_PATH);
	
	// protos
	strcpy(path_protos, path_app);
	strcat(path_protos, DEFAULT_PROTO_PATH);
	
	// scripts
	strcpy(path_scripts, path_app);
	strcat(path_scripts, DEFAULT_SCRIPTS_PATH);
	
	// levels
	strcpy(path_levels, path_app);
	strcat(path_levels, DEFAULT_LEVELS_PATH);
	
	// sounds
	strcpy(path_sounds, path_app);
	strcat(path_sounds, DEFAULT_SOUNDS_PATH);
	
	// screenshots
	strcpy(path_screenshots, path_home);
	strcat(path_screenshots, DEFAULT_SCREENSHOTS_PATH);
	
	// saves
	strcpy(path_saves, path_home);
	strcat(path_saves, DEFAULT_SAVES_PATH);
	
	//data
	strcpy(path_data, path_app);
	strcat(path_data, DEFAULT_DATA_PATH);
}


#ifndef WIN32
#define _mkdir(x) mkdir(x, S_IRWXU)
#endif // WIN32

// ��������� ��������� ���������� �� ��������� ����.
// ���� �� ���, ������� ��.
void CheckPaths()
{
	if(!IsFileExists(path_home))
	{
		_mkdir(path_home);
	}
	if(!IsFileExists(path_log))
	{
		_mkdir(path_log);
	}
	if(!IsFileExists(path_config))
	{
		_mkdir(path_config);
	}
	if(!IsFileExists(path_screenshots))
	
	{
		_mkdir(path_screenshots);
	}
	if(!IsFileExists(path_saves))
	{
		_mkdir(path_saves);
	}
	if(!IsFileExists(path_data))
	{
		_mkdir(path_data);
	}
}


// ��������� � ��� �����
void LogPaths(void)
{
#ifdef DISABLE_LOG
	return;
#endif
	
	Log(DEFAULT_LOG_NAME, logLevelInfo, "Game path:\t\t%s", path_app);
	Log(DEFAULT_LOG_NAME, logLevelInfo, "Log path:\t\t%s", path_log);
	Log(DEFAULT_LOG_NAME, logLevelInfo, "Config path:\t\t%s", path_config);
	Log(DEFAULT_LOG_NAME, logLevelInfo, "Textures path:\t%s", path_textures);
	Log(DEFAULT_LOG_NAME, logLevelInfo, "Fonts path:\t\t%s", path_fonts);
	Log(DEFAULT_LOG_NAME, logLevelInfo, "Prototypes path:\t%s", path_protos);
	Log(DEFAULT_LOG_NAME, logLevelInfo, "Scripts path:\t\t%s", path_scripts);
	Log(DEFAULT_LOG_NAME, logLevelInfo, "Levels path:\t\t%s", path_levels);
	Log(DEFAULT_LOG_NAME, logLevelInfo, "Sounds path:\t\t%s", path_sounds);
	Log(DEFAULT_LOG_NAME, logLevelInfo, "Screenshots path:\t\t%s", path_screenshots);
	Log(DEFAULT_LOG_NAME, logLevelInfo, "Saves path:\t\t%s", path_saves);
}


// ��������� ��������� ���������� �� ������ DEFAULT_CONFIG_NAME � ���������� path_config.
// ���� ������� ���, ������� ���, �������� ����������� ������.
void CheckDefaultConfig()
{
	char path_to_cfg[MAX_PATH];
	
	sprintf(path_to_cfg, "%s%s", path_config, DEFAULT_CONFIG_NAME);
	if(!FileExists(path_to_cfg))
	{
		RecreateConfig();
	}
}

//////////////////////////////////////////////////////////////////////////
// ������� ����� ������. �������� �������� ������� strdup, �� ���������� �� malloc,
// � new[]. ����� ������ ����� ����� ������� � ������� DELETEARRAY.
char* StrDupl (const char *str)
{
	if (!str)
		return NULL;
	
	char *newstr = NULL;
	size_t size = strlen(str) + 1;
	
	newstr = new char[size];
	if (newstr)
		memcpy(newstr, str, size);
	return newstr;
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

const char* LogLevelGetName( LogLevel ll )
{
	switch (ll)
	{
		case logLevelAll:
			return "logLevelAll";
		case logLevelNone:
			return "logLevelNone";
		case logLevelError:
			return "logLevelError";
		case logLevelWarning:
			return "logLevelWarning";
		case logLevelInfo:
			return "logLevelInfo";
		default:
			return "logLevelError";
	}
}

// Satanlog
bool LogEnabled = true;
char LogFile[2048];				// ����� ��� ������� ����� ����� ����
CDummy InitLog = CDummy();

CDummy::CDummy(void)
{
	// DEBUG_STOP_ON_ALLOC_NUM ������������ � misc.h
	// ��� �� ������ ����� :)
#ifdef DEBUG_STOP_ON_ALLOC_NUM
	_CrtSetBreakAlloc(DEBUG_STOP_ON_ALLOC_NUM);
#endif // DEBUG_STOP_ON_ALLOC_NUM
}

CDummy::~CDummy(void)
{
}

// ������� ����� ���� ���� (��� ������� ������)
// fname - ��� ����� ���� "\\file_name.log"
void CreateLogFile(const char *fname)
{
#ifdef DISABLE_LOG
	return;
#endif
	
	if (!LogEnabled)
		return;
	
	FILE *hf = NULL;
	
	memset(LogFile, '\0', strlen(path_log) + strlen(fname) + 1);
	sprintf(LogFile, "%s%s", path_log, fname);
	
	hf = fopen(LogFile, "w");
	if (!hf)
	{
		char buf[2048];
		sprintf(buf, "Failed to open log-file: %s", LogFile);
		#ifdef WIN32
		MessageBox(NULL, buf, NULL, NULL);
		#else
		perror(buf);
		#endif // WIN32
		return;
	}
	
	time_t current_time;
	time( &current_time );
	struct tm *newtime;
	newtime = localtime(&current_time);
	char buff[256];
	memset(buff,0,256);
	strcpy(buff, asctime(newtime));
	for (UINT i=0; i<strlen(buff); i++)
	{
		if (buff[i]<=13)
			buff[i] = 0;
	}
	
	fprintf(hf, "[%s] [%s] Log file \"%s\" created\n", buff, LogLevelGetName(logLevelInfo), fname);
	
	fclose(hf);
}


//bool LogLevelLowerThan( const char* Event )
//{
//	/*
//	if ( strcmp(Event, logLevelInfo) == 0 && cfg.log_level > logLevelInfo ) return true;
//	if ( strcmp(Event, LOG_WARNING_EV) == 0 && cfg.log_level > logLevelWarning ) return true;
//	if ( strcmp(Event, logLevelError) == 0 && cfg.log_level > logLevelError ) return true;*/
//	return false;
//}

const char* GetLogLevelName( LogLevel ll )
{
	switch (ll)
	{
		case logLevelError: return "ERROR";
		case logLevelInfo: return "INFO";
		case logLevelWarning: return "WARNING";
		case logLevelScript: return "SCRIPT";
		default: return "???";
	}
}

void SuppressLogMessages(LogLevel level)
{
	log_suppression_level = level;
}

// ������ � ���
// fname - ��� ����� ���� "\\file_name.log"
// Event - �������� ������ � ����
// Format - ������ ������� ��������� (������� printf)
void Log(const char* fname, LogLevel Event, const char* Format,...)
{
#ifdef DISABLE_LOG
	return;
#endif

	if (!LogEnabled)
		return;
	
	if ( Event < cfg.log_level || Event <= log_suppression_level )
		return;
	
	memset(LogFile, '\0', strlen(path_log) + strlen(fname) + 1);
	sprintf(LogFile, "%s%s", path_log, fname);
	
	
	FILE *hf = NULL;
	hf = fopen(LogFile, "a");
	if (!hf)
	{
		char buf[2048];
		sprintf(buf, "Failed to open log-file: %s", LogFile);
		#ifdef WIN32
		MessageBox(NULL, buf, NULL, NULL);
		#else
		perror(buf);
		#endif // WIN32
		return;
	}
	
	
	time_t current_time;
	time( &current_time );
	struct tm *newtime;
	newtime = localtime(&current_time);
	char buff[256];
	memset(buff,0,256);
	strcpy(buff,asctime(newtime));
	for (UINT i=0; i<strlen(buff); i++)
	{
		if (buff[i]<=13)
			buff[i] = 0;
	}
	fprintf(hf,"[%s] [%s] ", buff, GetLogLevelName(Event));
	//printf("[%s] [%s] ", buff, Event);
	va_list ap;
	va_start(ap, Format);
	vfprintf(hf, Format, ap);
	//vprintf(Format, ap);
	va_end(ap);
	fprintf(hf,"\n");
	//printf("\n");
	fclose(hf);
	if ( cfg.debug || Event >= logLevelError )
	{
		//TO �������� DO: ��������� �����.
		printf( "[%s] [%s] ", buff, GetLogLevelName(Event) );
		va_list ap;
		va_start(ap, Format);
		vprintf(Format, ap);
		va_end(ap);
		printf("\n");
	}
}

// ������ ������ � ��� �� ���������
// Format - ������ ������� ��������� (������� printf)
void LogError(const char* Format,...)
{
	const char* fname = DEFAULT_LOG_NAME;
	LogLevel Event = logLevelError;
	#ifdef DISABLE_LOG
	return;
	#endif
	
	if (!LogEnabled)
		return;
	
	if ( Event < cfg.log_level || Event <= log_suppression_level )
		return;
	
	memset(LogFile, '\0', strlen(path_log) + strlen(fname) + 1);
	sprintf(LogFile, "%s%s", path_log, fname);
	
	
	FILE *hf = NULL;
	hf = fopen(LogFile, "a");
	if (!hf)
	{
		char buf[2048];
		sprintf(buf, "Failed to open log-file: %s", LogFile);
		#ifdef WIN32
		MessageBox(NULL, buf, NULL, NULL);
		#else
		perror(buf);
		#endif // WIN32
		return;
	}
	
	
	time_t current_time;
	time( &current_time );
	struct tm *newtime;
	newtime = localtime(&current_time);
	char buff[256];
	memset(buff,0,256);
	strcpy(buff,asctime(newtime));
	for (UINT i=0; i<strlen(buff); i++)
	{
		if (buff[i]<=13)
			buff[i] = 0;
	}
	fprintf(hf,"[%s] [%s] ", buff, GetLogLevelName(Event));
	//printf("[%s] [%s] ", buff, Event);
	va_list ap;
	va_start(ap, Format);
	vfprintf(hf, Format, ap);
	//vprintf(Format, ap);
	va_end(ap);
	fprintf(hf,"\n");
	//printf("\n");
	fclose(hf);
	if ( cfg.debug || Event >= logLevelError )
	{
		//TO �������� DO: ��������� �����.
		printf( "[%s] [%s] ", buff, GetLogLevelName(Event) );
		va_list ap;
		va_start(ap, Format);
		vprintf(Format, ap);
		va_end(ap);
		printf("\n");
	}
}

// ��������, ������������ �� ������ �� ���������
// ������ ������������ endswith
// ������ � http://stackoverflow.com/questions/874134/find-if-string-endswith-another-string-c
bool endswith(std::string const &fullString, std::string const &ending)
{
	if (fullString.length() > ending.length()) {
		return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
	} else {
		return false;
	}
}

bool startswith(std::string const &fullString, std::string const &beginning)
{
	if (fullString.length() < beginning.length()) {
		return false;
	} else {
		return (0 == fullString.compare(0, beginning.length(), beginning));
	}
}

//////////////////////////////////////////////////////////////////////////

