#include "StdAfx.h"

#include "gui_widget.h"
#include "../config.h"

#include "../resource_mgr.h"
#include "../game/editor.h"

//////////////////////////////////////////////////////////////////////////
extern ResourceMgr<Proto> * protoMgr;

extern config cfg;
extern lua_State* lua;
#ifdef MAP_EDITOR
extern editor::EditorStates editor_state;
#endif // MAP_EDITOR
//////////////////////////////////////////////////////////////////////////

GuiWidget::~GuiWidget()
{
	DELETEARRAY(caption);

	DELETESINGLE(typer);

	DELETESINGLE(this->sprite);

	SCRIPT::ReleaseProc(&this->onMouseEnterProc);
	SCRIPT::ReleaseProc(&this->onMouseLeaveProc);
	SCRIPT::ReleaseProc(&this->onMouseRClickProc);
	SCRIPT::ReleaseProc(&this->onMouseLClickProc);
	SCRIPT::ReleaseProc(&this->onKeyDown);
	SCRIPT::ReleaseProc(&this->onKeyInput);
	SCRIPT::ReleaseProc(&this->onKeyPress);
	SCRIPT::ReleaseProc(&this->onFocus);
	SCRIPT::ReleaseProc(&this->onUnFocus);
	SCRIPT::ReleaseProc(&this->onResize);

	Log(DEFAULT_GUI_LOG_NAME, logLevelInfo, "Widget %s is destroyed, id = %d, addr %p", this->name, this->id, this);
	DELETEARRAY(name);
}


//////////////////////////////////////////////////////////////////////////

bool GuiWidget::SetSprite(const char* tex, UINT frame)
{
	DELETESINGLE(this->sprite);

	this->sprite = new Sprite(tex);
	if ( !this->sprite->tex )
	{
		DELETESINGLE(this->sprite);
		return true;
	}
	this->z = this->sprite->z;
	this->sprite->SetFixed();
	if ( frame < this->sprite->tex->frames->framesCount )
	{
		this->sprite->setCurrentFrame(frame);
		this->sprite->frameWidth = (USHORT)sprite->tex->frames->frame[frame].size.x;
		this->sprite->frameHeight = (USHORT)sprite->tex->frames->frame[frame].size.y;
	}

#ifdef MAP_EDITOR
	if ( editor_state != editor::EDITOR_OFF )
	{
		float ratio = 1.0;
		if (this->sprite->frameHeight != 0 && this->sprite->frameHeight > this->sprite->frameWidth)
		{
			ratio = ((float)this->sprite->frameWidth) / this->sprite->frameHeight;
			caption_shift.x = ((2*this->aabb.H) * ratio)/2 - this->aabb.W;
			caption_shift.y = 0;
		}
		else if ( this->sprite->frameWidth != 0  )
		{
			ratio = ((float)this->sprite->frameHeight) / this->sprite->frameWidth;
			caption_shift.x = 0;
			caption_shift.y = ((2*this->aabb.W) * ratio)/2 - this->aabb.H;
		}
		this->sprite->renderMethod = rsmStretch;
	}
#endif // MAP_EDITOR
	
	this->sprite->blendingMode = bmPostEffect;

	return true;
}

bool GuiWidget::SetSprite(const Proto* proto, const char* start_anim)
{
	if (!proto)
		return false;

	DELETESINGLE(this->sprite);

	this->sprite = new Sprite(proto);
	// ��������� ������� ������� ��� z, ������ ��� � �������� �� z �� ���������.
	this->z = this->sprite->z;
	if (start_anim)
		this->sprite->SetAnimation(string(start_anim));
	else
		this->sprite->SetAnimation(string("idle"));

	this->sprite->SetFixed();

	return true;
}

bool GuiWidget::SetSprite(const char* proto_name, const char* start_anim)
{
	if (!proto_name)
		return false;

	return SetSprite(protoMgr->GetByName(proto_name, "widgets/"), start_anim);
}

bool GuiWidget::SetSpriteRenderMethod(RenderSpriteMethod rsm)
{
	if (!sprite)
		return false;

	sprite->setRenderMethod(rsm);

	return true;
}

bool GuiWidget::SetSpriteBlendingMode(BlendingMode bm)
{
	if (!sprite)
		return false;

	sprite->blendingMode = bm;

	return true;
}

bool GuiWidget::SetColorBox(const RGBAf& color)
{
	bool is_visible = (this->sprite && this->sprite->IsVisible());
	if ( this->sprite == NULL || this->sprite->tex != NULL )
	{
		DELETESINGLE(this->sprite);
		this->sprite = new Sprite();
	}
	this->sprite->color = color;
	this->sprite->render_without_texture = true;
	this->sprite->z = z;
	this->sprite->frameWidth = 2*((USHORT)this->aabb.W);
	this->sprite->frameHeight = 2*((USHORT)this->aabb.H);
	this->sprite->blendingMode = bmPostEffect;
	if (is_visible) this->sprite->SetVisible();
	this->sprite->SetFixed();
	return true;
}

void GuiWidget::SetCaption(const char *cap, bool multiline)
{
	DELETEARRAY(this->caption);
	this->caption = StrDupl(cap);
	this->caption_multiline = multiline;
	if (typer)
	{
		TextTyper* t = new TextTyper(caption, *typer);
		DELETESINGLE(typer);
		typer = t;
	}
		
}

const char* GuiWidget::GetCaption() const
{
	return this->caption;
}

void GuiWidget::SetZ(float z)
{
	this->z = z;
	if (this->sprite)
	{
		this->sprite->z = z;
	}
}

void GuiWidget::SetSpriteColor( RGBAf color )
{
	if ( !sprite ) return;
	sprite->color = color;
}

void GuiWidget::SetFixedPosition(bool fixed)
{
	this->fixed_position = fixed;
	if (sprite)
	{
		if (fixed)
			this->sprite->SetFixed();
		else
			this->sprite->ClearFixed();
	}
}

void GuiWidget::Move(float x, float y)
{
	float x_shift = aabb.Left() - x;
	float y_shift = aabb.Top()  - y;
	aabb.p.x = x + aabb.W;
	aabb.p.y = y + aabb.H;
	for ( vector<GuiWidget*>::iterator iter = children.begin(); iter != children.end(); iter++ )
	{
		GuiWidget* child = (*iter);
		child->Move( child->aabb.Left()-x_shift, child->aabb.Top()-y_shift );
	}
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void GuiWidget::ProcessSprite()
{
	if (!sprite)
		return;

	if (sprite->anims && sprite->animsCount > 0/* && sprite->animNames*/)
	{
		Animation* a = this->sprite->GetAnimation(this->sprite->cur_anim_num);

		if (a)
		{
			while (!this->sprite->ChangeFrame(a))
			{
				switch (a->frames[a->current].command)
				{
				case afcSetAnim:
					this->sprite->SetAnimation(string(a->frames[a->current].txt_param));
					this->ProcessSprite();
					return;
					break;
				case afcMirror:
					this->sprite->SetMirrored();
					this->sprite->setRealX(this->sprite->frameWidth);
					break;
				case afcJumpRandom:
					{
						StackElement* sd = this->sprite->stack->Pop();
						if (  (rand() % 256) > sd->data.intData && !this->sprite->JumpFrame( a->frames[a->current].param ) )
							Log(DEFAULT_LOG_NAME, logLevelError, "Impossible jump in anim %s.", this->sprite->cur_anim.c_str());
						DELETESINGLE(sd);
					}
					break;
				case afcLoop:
					this->sprite->SetCurrentFrame(0);
				default:
					break;
				}

			}
		}

	}
}


void GuiWidget::Process()
{
}

void GuiWidget::AddChild( GuiWidget* child )
{
	children.push_back( child );
	child->parent = this;
}

void GuiWidget::SetParent( GuiWidget* new_parent )
{
	if ( parent )
	{
		for ( vector<GuiWidget*>::iterator iter = parent->children.begin(); iter != parent->children.end(); iter++ )
		{
			if ( (*iter)->id == id )
			{
				parent->children.erase( iter );
				break;
			}
		}
	}
	new_parent->AddChild( this );
	SetVisibility( parent->IsVisible(), true );
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

extern float CAMERA_OFF_X;
extern float CAMERA_OFF_Y;

void GuiWidget::DrawCaption()
{
	if (this->caption && this->caption_font)
	{
		caption_font->tClr = this->active ? this->caption_act_color :
			this->caption_inact_color;
		caption_font->outline = this->caption_outline;

		caption_font->p = Vector2( floor(this->aabb.Left()+0.5f), floor(this->aabb.Top()+0.5f));
		if (this->fixed_position)
		{
			caption_font->p.x -= CAMERA_OFF_X;
			caption_font->p.y -= CAMERA_OFF_Y;
		}
		if ( this->sprite )
		{
			if (this->sprite->getRenderMethod() == rsmStretch || 
				this->sprite->getRenderMethod() == rsmRepeatY || 
				this->sprite->getRenderMethod() == rsmRepeatXY )
			{
				caption_font->p.y += 2*this->aabb.H;
				caption_font->p.x += this->aabb.W;
			}
			else
			{
				caption_font->p.y += this->sprite->frameHeight;
				caption_font->p.x += this->sprite->frameWidth/2.0f;
			}
			caption_font->p.x -= caption_font->GetStringWidth(this->caption)/2.0f;
			caption_font->p.y += caption_font->GetStringHeight(this->caption)/2.0f;
		}

		caption_font->z = min( cfg.far_z, this->z + 0.000003f );

		const char* cap = typer ? typer->GetBuffer() : this->caption;
		caption_font->scale = caption_scale;

		if ( this->caption_multiline )
		{
			caption_font->PrintMultiline(cap, this->aabb, -1, caption_multiline_scroll);
		}
		else
		{
			caption_font->Print(cap);
		}

		caption_font->scale = 1.0f;
	}
}

void GuiWidget::DrawSprite()
{
	if (this->sprite)
	{
		if (this->IsVisible()) 
			this->sprite->SetVisible(); 
		else 
			this->sprite->ClearVisible();

		this->sprite->z = this->z;
#ifdef MAP_EDITOR
		if ( editor_state != editor::EDITOR_OFF )
		{
			this->aabb.W += this->caption_shift.x;
			this->aabb.H += this->caption_shift.y;
		}
#endif //MAP_EDITOR
		this->sprite->Draw(this->aabb);
#ifdef MAP_EDITOR
		if ( editor_state != editor::EDITOR_OFF )
		{
			this->aabb.W -= this->caption_shift.x;
			this->aabb.H -= this->caption_shift.y;
		}
#endif //MAP_EDITOR
	}
}

void GuiWidget::DrawBorder()
{
	if (this->border)
	{

		float x = this->aabb.Left();
		float y = this->aabb.Top();
		float w = this->aabb.W * 2;
		float h = this->aabb.H * 2;
		if ( parent )
		{
			if ( x < parent->aabb.Left() )
				x = parent->aabb.Left();
			if ( y < parent->aabb.Top() )
				y = parent->aabb.Top();
			w = min( aabb.Right() , parent->aabb.Right()  ) - x;
			h = min( aabb.Bottom(), parent->aabb.Bottom() ) - y;
			if ( w <= 0 || h <= 0 ) return;
		}
		if (this->fixed_position)
		{
			x -= CAMERA_OFF_X;
			y -= CAMERA_OFF_Y;
		}
		RenderBox(x, y, this->z, w, h,
			this->active ? this->border_active_color : this->border_inactive_color, bmPostEffect);
	}
}

void GuiWidget::Draw()
{
	if (!this->IsVisible())
		return;

	this->DrawBorder();
	this->DrawSprite();
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void GuiWidget::OnMouseClick(BYTE button)
{
	switch(button)
	{
	case InputMgr::MOUSE_BTN_LEFT: OnLMouseClick(); break;
	case InputMgr::MOUSE_BTN_RIGHT: OnRMouseClick(); break;
	case InputMgr::MOUSE_BTN_MIDDLE: break;
	default: break;
	}
}

void GuiWidget::OnKeyDown( USHORT vkey )
{
	if (this->onKeyDown >= 0)
	{
		lua_pushinteger(lua, this->id);
		lua_pushinteger(lua, vkey);
		if (SCRIPT::ExecChunkFromReg(this->onKeyDown, 2) < 0)
		{
			// � ������� ��������� �����-�� ������.
			Log(DEFAULT_GUI_LOG_NAME, logLevelWarning, "Error in button %s handler OnKeyDown()", this->name);
		}
	}
}

void GuiWidget::OnKeyInput( Uint16 symbol )
{
	if (this->onKeyInput >= 0)
	{
		lua_pushinteger(lua, this->id);
		lua_pushinteger(lua, (int)symbol);
		if (SCRIPT::ExecChunkFromReg(this->onKeyInput, 2) < 0)
		{
			// � ������� ��������� �����-�� ������.
			Log(DEFAULT_GUI_LOG_NAME, logLevelWarning, "Error in button %s handler OnKeyInput()", this->name);
		}
	}
}

void GuiWidget::OnKeyPress( USHORT vkey )
{
	if (this->onKeyPress >= 0)
	{
		lua_pushinteger(lua, this->id);
		lua_pushinteger(lua, vkey);
		if (SCRIPT::ExecChunkFromReg(this->onKeyPress, 2) < 0)
		{
			// � ������� ��������� �����-�� ������.
			Log(DEFAULT_GUI_LOG_NAME, logLevelWarning, "Error in button %s handler OnKeyPress()", this->name);
		}
	}
}

void GuiWidget::OnFocus()
{
	if (this->onFocus >= 0)
	{
		lua_pushinteger(lua, this->id);
		if (SCRIPT::ExecChunkFromReg(this->onFocus, 1) < 0)
		{
			// � ������� ��������� �����-�� ������.
			Log(DEFAULT_GUI_LOG_NAME, logLevelWarning, "Error in button %s handler OnFocus()", this->name);
		}
	}
}

void GuiWidget::OnUnFocus()
{
	if (this->onUnFocus >= 0)
	{
		lua_pushinteger(lua, this->id);
		if (SCRIPT::ExecChunkFromReg(this->onUnFocus, 1) < 0)
		{
			// � ������� ��������� �����-�� ������.
			Log(DEFAULT_GUI_LOG_NAME, logLevelWarning, "Error in button %s handler OnUnFocus()", this->name);
		}
	}
}

void GuiWidget::OnResize()
{
	if (this->onResize >= 0)
	{
		lua_pushinteger(lua, this->id);
		if (SCRIPT::ExecChunkFromReg(this->onResize, 1) < 0)
		{
			// � ������� ��������� �����-�� ������.
			Log(DEFAULT_GUI_LOG_NAME, logLevelWarning, "Error in button %s handler OnResize()", this->name);
		}
	}
}

void GuiWidget::OnRepeatedFocus()
{
}

void GuiWidget::OnNavAccept()
{
	OnMouseClick(InputMgr::MOUSE_BTN_LEFT);
}

void GuiWidget::OnNavDecline()
{
	OnMouseClick(InputMgr::MOUSE_BTN_RIGHT);
}

void GuiWidget::UseTyper( UINT period, bool pausable, const char* sound )
{
	if (typer)
	{
		typer->pausable = pausable;
		typer->SetPeriod(period);
		if (!typer->IsStarted())
			typer->Start();
		typer->SetSound( sound );
	}
	else
	{
		typer = new TextTyper(this->caption, period, pausable);
		typer->SetSound( sound );
		//typer->Start();
	}
}

void GuiWidget::UnUseTyper()
{
	DELETESINGLE(typer);
}

bool GuiWidget::StartTyper()
{
	if (typer)
	{
		return typer->Start();
	}
	else
	{
		Log(DEFAULT_GUI_LOG_NAME, logLevelError, "Attempt to start TextTyper on a widget %d %s that does not have a TextTyper", id, name);
		return false;
	}
}

void GuiWidget::SetVisibility( bool vis, bool from_parent )
{
	if ( from_parent ) parent_visible = vis;
	else visible = vis;

	for ( vector<GuiWidget*>::iterator iter = children.begin(); iter != children.end(); iter++ )
		(*iter)->SetVisibility( IsVisible(), true );
}

bool GuiWidget::IsVisible()
{
	return visible && parent_visible;
}

void GuiWidget::SetActivity( bool act, bool from_parent )
{
	if ( from_parent ) parent_can_be_active = act;
	else can_be_active = act;

	for ( vector<GuiWidget*>::iterator iter = children.begin(); iter != children.end(); iter++ )
		(*iter)->SetActivity( IsActive(), true );
}

bool GuiWidget::IsActive()
{
	return can_be_active && parent_can_be_active;
}

bool GuiWidget::CanGainFocus()
{
	return focusable && IsActive();
}

void GuiWidget::StopTyper()
{
	if (typer)
		typer->Stop();
	else
		Log(DEFAULT_GUI_LOG_NAME, logLevelWarning, "Attempt to stop TextTyper on a widget %d %s that does not have a TextTyper", id, name);
}
