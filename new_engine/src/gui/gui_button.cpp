#include "StdAfx.h"

#include "gui_button.h"

#include "../misc.h"

//////////////////////////////////////////////////////////////////////////

extern lua_State* lua;

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void GuiButton::Process()
{
	if (!this->IsVisible())
		return;

	//this->ProcessMouseMoves();
	//this->ProcessMouseClicks();
	this->ProcessSprite();
	//this->ProcessChildren();
}

void GuiButton::Draw()
{
	if (!this->IsVisible())
		return;

	this->DrawCaption();
	this->DrawSprite();
	this->DrawBorder();
}


void GuiButton::OnMouseEnter()
{
	//this->active = true;

	if (this->onMouseEnterProc >= 0)
	{
		lua_pushinteger(lua, this->id);
		if (SCRIPT::ExecChunkFromReg(this->onMouseEnterProc, 1) < 0)
		{
			// � ������� ��������� �����-�� ������.
			Log(DEFAULT_GUI_LOG_NAME, logLevelWarning, "Error in button %s handler OnMouseEnter()", this->name);
		}
	}
}


void GuiButton::OnMouseLeave()
{
	//this->active = false;

	if (this->onMouseLeaveProc >= 0)
	{
		lua_pushinteger(lua, this->id);
		if (SCRIPT::ExecChunkFromReg(this->onMouseLeaveProc, 1) < 0)
		{
			// � ������� ��������� �����-�� ������.
			Log(DEFAULT_GUI_LOG_NAME, logLevelWarning, "Error in button %s handler OnMouseLeave()", this->name);
		}
	}
}




void GuiButton::OnLMouseClick()
{
	//Log(DEFAULT_GUI_LOG_NAME, LOG_INFO_EV, "l-click");

	if (this->onMouseLClickProc >= 0)
	{
		lua_pushinteger(lua, this->id);
		if (SCRIPT::ExecChunkFromReg(this->onMouseLClickProc, 1) < 0)
		{
			// � ������� ��������� �����-�� ������.
			Log(DEFAULT_GUI_LOG_NAME, logLevelWarning, "Error in button %s handler OnLMouseClick()", this->name);
		}
	}
}

void GuiButton::OnRMouseClick()
{
	//Log(DEFAULT_GUI_LOG_NAME, LOG_INFO_EV, "r-click");

	if (this->onMouseRClickProc >= 0)
	{
		lua_pushinteger(lua, this->id);
		if (SCRIPT::ExecChunkFromReg(this->onMouseRClickProc, 1) < 0)
		{
			// � ������� ��������� �����-�� ������.
			Log(DEFAULT_GUI_LOG_NAME, logLevelWarning, "Error in button %s handler OnRMouseClick()", this->name);
		}
	}
}
