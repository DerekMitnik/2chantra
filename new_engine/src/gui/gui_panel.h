#pragma once
#ifndef __GUI_PANEL_H
#define __GUI_PANEL_H

#include "gui_widget.h"
#include "gui_button.h"
#include "../game/sprite.h"

class GuiPanel: public GuiButton
{
public:
	enum PanelPart
	{
		FIRST_PART = 0,
		TOP_LEFT = 0,
		TOP = 1,
		TOP_RIGHT = 2,
		LEFT = 3,
		MIDDLE = 4,
		RIGHT = 5,
		BOTTOM_LEFT = 6,
		BOTTOM = 7,
		BOTTOM_RIGHT = 8,
		LAST_PART = 8
	};

	vector<Sprite*> sprites;

	virtual bool SetSprite(const char* tex, UINT frame = 0 );
	virtual bool SetSprite(const Proto* proto, const char* start_anim);
	virtual bool SetColorBox(const RGBAf& color);
	virtual bool SetSpriteRenderMethod(RenderSpriteMethod rsm);
	virtual bool SetSpriteBlendingMode(BlendingMode bm);
	virtual void SetSpriteColor( RGBAf color );

	virtual void Draw();
	virtual void Process();

	void RemoveSprites();
	void SetSpritesFixed( bool state );

	GuiPanel();
	~GuiPanel();
};


#endif //__GUI_PANEL_H