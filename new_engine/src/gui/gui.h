#ifndef __GUI_H_
#define  __GUI_H_


#include "gui_widget.h"
#include "gui_button.h"
#include "gui_picture.h"
#include "gui_label.h"
#include "gui_textfield.h"
#include "gui_panel.h"

// ���� ��������
enum WidgetTypes { wt_Widget, wt_Button, wt_Picture, wt_Label, wt_Textfield, wt_Panel };
// ������ ������������ ������ ����� ���������
enum GuiNavigationMode { gnm_None, gnm_Normal };


class Gui
{
public:

	Gui();
	~Gui();

	void Process();
	void Draw();

	UINT CreateWidget(WidgetTypes wt, const char* name, float x, float y, float w, float h, GuiWidget* parent );

	void DestroyWidget(UINT id);
	void DestroyAllWidgets();

	void SetFocus(UINT id);
	void LockFocus();
	void UnlockFocus();
	bool GetFocusLock();

	bool SetWidgetVisibility(UINT id, bool val);

	GuiWidget* GetWidget(UINT id);

	GuiNavigationMode nav_mode;			// ����� ������������ ������ ����� ���������
	bool nav_cycled;					// ����������� ������������ ����� ���������

	LuaRegRef onKeyDownGlobal;				    // ���������� ���������� ������� ������
	LuaRegRef onKeyReleaseGlobal;				// ���������� ���������� ������� ������

	LuaRegRef onMouseKeyDownGlobal;				// ���������� ���������� ������� ������ �����
	LuaRegRef onMouseKeyReleaseGlobal;			// ���������� ���������� ������� ������ �����
private:

	void SetFocus(GuiWidget* wi);

	void BatchCreate();
	void BatchDestroy();

	map<UINT, GuiWidget*> widgets;
	map<UINT, GuiWidget*> createdWidgets;
	list<GuiWidget*> destroyedWidgets;

	typedef map<UINT, GuiWidget*>::iterator WidgetIter;
	typedef map<UINT, GuiWidget*>::const_reverse_iterator WidgetConstRevIter;

	bool focus_locked;
	GuiWidget* focusedWidget;			// ������, �� ������� � ������ ������ ���������� �����
	UINT nextId;						// id, ������� ����� �������� ���������� ���������� �������
};





//#define GUITICK 10

#endif // __GUI_H_
