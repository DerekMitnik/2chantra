#ifndef __GUI_VIGET_H_
#define __GUI_VIGET_H_

#include "../game/phys/phys_misc.h"
#include "../game/sprite.h"
#include "../render/renderer.h"
#include "../render/font.h"
#include "../script/script.h"
#include "../input_mgr.h"

#include "text_typer.h"

enum MousePosition { MouseIsOut, MouseIsIn }; 
enum MouseButtonState { MouseButtonUnpresed, MouseButtonPressed };


// ����������� ������
class GuiWidget
{
public:
	UINT id;

	char* name;

	bool fully_added;			// �������� �� ������ � �������� ������ (������ �� BatchCreate)

	bool active;				// ����������, �����
	bool visible;
	bool parent_visible;
	bool can_be_active;
	bool parent_can_be_active;
	bool dead;
	bool staticWidget;
	bool focusable;
	int group;					// ������
	
	vector<GuiWidget*> children;
	GuiWidget* parent;

#ifdef MAP_EDITOR
	Vector2 caption_shift;
#endif //MAP_EDITOR

	CAABB aabb;
	float z;
	Sprite* sprite;
	bool fixed_position;
	//RGBAf active_color;
	//RGBAf inactive_color;

	float caption_scale;

	bool border;				// �������� �� �����
	RGBAf border_active_color;
	RGBAf border_inactive_color;

	char* caption;				// ������� �� �������
	RGBAf caption_act_color;
	RGBAf caption_inact_color;
	RGBAf caption_outline;
	IFont* caption_font;
	bool caption_multiline;
	int caption_multiline_scroll;

	TextTyper* typer;

	MousePosition lastMousePos;					// ���������� ��������� ����� (�� ������� ��� ��� ���)
	MouseButtonState lastmouseButtonState[InputMgr::mousebtns_count];	// ���������� ��������� ������ �����

	GuiWidget()
	{
		Init();
	}

	virtual ~GuiWidget();


	virtual bool SetSprite(const char* tex, UINT frame = 0 );
	virtual bool SetSprite(const Proto* proto, const char* start_anim);
	virtual bool SetSprite(const char* proto_name, const char* start_anim);
	virtual bool SetColorBox(const RGBAf& color);
	virtual bool SetSpriteRenderMethod(RenderSpriteMethod rsm);
	virtual bool SetSpriteBlendingMode(BlendingMode bm);

	virtual void SetZ(float z);
	virtual void SetSpriteColor( RGBAf color );
	virtual void SetFixedPosition(bool fixed);
	virtual void Move(float x, float y);

	void AddChild( GuiWidget* child );
	void SetParent( GuiWidget* parent );

	virtual void Process();
	virtual void Draw();


	void BringToFront();

	virtual void OnMouseEnter() {};
	virtual void OnMouseLeave() {};
	virtual void OnLMouseClick() {};
	virtual void OnRMouseClick() {};
	virtual void OnMouseClick(BYTE button);
	virtual void OnKeyDown(USHORT symbol);
	virtual void OnKeyInput(Uint16 vkey);
	virtual void OnKeyPress(USHORT vkey);
	virtual void OnFocus();
	virtual void OnUnFocus();
	virtual void OnResize();
	virtual void OnRepeatedFocus();
	virtual void OnShow() {};
	virtual void OnHide() {};
	virtual void OnNavAccept();
	virtual void OnNavDecline();

	LuaRegRef onMouseEnterProc;
	LuaRegRef onMouseLeaveProc;
	LuaRegRef onMouseLClickProc;
	LuaRegRef onMouseRClickProc;
	LuaRegRef onKeyDown;
	LuaRegRef onKeyInput;
	LuaRegRef onKeyPress;
	LuaRegRef onFocus;
	LuaRegRef onUnFocus;
	LuaRegRef onResize;


	virtual void SetCaption(const char* cap, bool multiline);
	const char* GetCaption() const;
	void UseTyper( UINT period, bool pausable, const char* sound = NULL );
	void UnUseTyper();
	bool StartTyper();
	void StopTyper();

	void SetVisibility( bool vis, bool from_parent = false );
	void SetActivity( bool act, bool from_parent = false );
	bool IsVisible();
	bool IsActive();

	bool CanGainFocus();

protected:

	

	//void ProcessMouseMoves();
	//void ProcessMouseClicks();
	void ProcessSprite();

	void DrawCaption();
	void DrawSprite();
	void DrawBorder();

	void Init()
	{
		id = 0;
		caption_scale = 1.0f;
		name = NULL;
		sprite = NULL;
		caption = NULL;
		caption_font = FontByName(DEFAULT_FONT);
		caption_multiline = false;
		caption_multiline_scroll = -1;
		active = dead = false;
		visible = true;
		parent_visible = true;
		can_be_active = parent_can_be_active = true;
		border = false;
		focusable = true;
		group = 0;
		caption_act_color = RGBAf(1.0f, 0.48f, 0.09f, 1.0f);
		caption_inact_color = RGBAf(0.5f, 0.5f, 0.5f, 1.0f);
		caption_outline = RGBAf(0.0f, 0.0f, 0.0f, 0.0f);
		border_inactive_color = RGBAf(0.5f, 0.5f, 0.5f, 1.0f);
		border_active_color = RGBAf(1.0f, 1.0f, 1.0f, 1.0f);
		staticWidget = false;
		
		parent = NULL;

		fully_added = false;

		z = 1.05f;
		fixed_position = true;
		lastMousePos = MouseIsOut;
		for (BYTE i = 0; i < InputMgr::mousebtns_count; i++) 
			lastmouseButtonState[i] = MouseButtonUnpresed;

		onMouseEnterProc = onMouseLeaveProc = onMouseLClickProc = 
			onMouseRClickProc = onKeyDown = onKeyPress = 
			onFocus = onUnFocus = onKeyInput = onResize = LUA_NOREF;

		typer = NULL;
	}

};



#endif // __GUI_VIGET_H_
