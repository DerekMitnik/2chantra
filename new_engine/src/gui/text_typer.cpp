#include "StdAfx.h"


#include "text_typer.h"
#include "../sound/snd.h"

extern SoundMgr* soundMgr;

extern lua_State* lua;

void TextTyper::SetSound( const char* sound )
{
	DELETEARRAY(this->sound);
	if ( sound == NULL )
	{
		this->sound = NULL;
	}
	else
	{
		this->sound = StrDupl( sound );
	}
}

bool TextTyper::Start()
{
	if (!buffer)
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Attempt to start TextTyper with no text");
		return false;
	}

	if (timerEventId)
		DeleteTimerEventById(timerEventId);

	if (ended)
		return true;

	timerEventId = AddInternalTimerEvent(this, 0, period, static_cast<UINT>(max_pos - cur_pos), true, pausable);
	return true;
}

void TextTyper::Stop()
{
	if (timerEventId)
	{
		DeleteTimerEventById(timerEventId);
	}
	else
	{
		Log(DEFAULT_LOG_NAME, logLevelWarning, "Attempt to stop TextTyper that was never started to begin with");
	}
}

void TextTyper::OnTimer(InternalTimerEvent& ev)
{
	assert(buffer);
	size_t new_pos = cur_pos+1;
	size_t shift = 0;
	
	// ���������� ��� ������ ������ ����������� ������������������ (����� ������������ ������� '/')
	while(ControlSeqParser::CheckControlSeq(&buffer[new_pos], shift) != ControlSeqParser::ENone)
		new_pos += shift;
	
	for (size_t i = cur_pos; i <= max_pos; i++)
	{
		if (i < new_pos)
		{
			buffer[i] = buffer[i+1];
		}
		else if (i == new_pos)
		{
			buffer[i] = 0;
		}
		else
			break;
	}
	
	if ( sound && cur_pos != new_pos && buffer[cur_pos] != 32 )
		soundMgr->PlaySnd( sound, true, 1.0f );
	cur_pos = new_pos;

	if (cur_pos == max_pos)
	{
		ev.ClearPeriodic();
		ended = true;

		if (this->onTyperEnded >= 0)
		{
			if (SCRIPT::ExecChunkFromReg(this->onTyperEnded, 0) == -1)
			{
				// � ������� ��������� �����-�� ������.
				Log(DEFAULT_SCRIPT_LOG_NAME, logLevelWarning, "Error in handler onTyperEnded()");
			}
		}
	}
}

void TextTyper::OnTimetEventDestroy(const InternalTimerEvent& ev)
{	
	UNUSED_ARG(ev);
	assert(timerEventId);
	assert(timerEventId == ev.id);
	timerEventId = 0;
}
