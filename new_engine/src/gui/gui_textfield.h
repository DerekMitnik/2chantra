#ifndef __GUI_TEXTFIELD_H_
#define __GUI_TEXTFIELD_H_

#include "gui_widget.h"

// ��������� ����
class GuiTextfield : public GuiWidget
{
public:

	size_t reserved_size;			// ����� ������, ���������� ��� caption
	size_t max_size;				// ����������� �� ������ caption. 0 - ��� �����������.
	size_t used_size;				// ����� ������, ������� �������������� ��� ������� (��� ������� '/0' � �����).
	size_t cursor_position;			// ��������� �������. ������ ��������� ����� �������� ����� cursor_position
	bool input_active;
	bool last_active;

	GuiTextfield()
	{		
		max_size = 256;
		used_size = 0;
		reserved_size = 0;
		caption = NULL;
		input_active = false;
		last_active = false;

		cursor_position = 0;	//������ _�����_ �������� � ���� �������.
	}

	virtual ~GuiTextfield() { }



	virtual void Process();
	virtual void Draw();
	virtual void DrawCaption();

	virtual void OnKeyInput(Uint16 vkey);
	virtual void OnKeyDown(USHORT vkey);
	virtual void OnKeyPress(USHORT vkey);
	virtual void OnMouseClick(BYTE button);
	virtual void OnUnFocus();
	virtual void OnRepeatedFocus();
	virtual void OnHide();
	virtual void OnNavAccept();
	virtual void OnNavDecline();

	virtual void SetCaption(const char *cap, bool multiline);

	void SetMaxSize(size_t size);

	void RecalcTextfieldSize();
};

#endif // __GUI_TEXTFIELD_H_