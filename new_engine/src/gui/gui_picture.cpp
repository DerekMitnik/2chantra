#include "StdAfx.h"

#include "gui_picture.h"

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void GuiPicture::Process()
{
	this->ProcessSprite();
}

void GuiPicture::Draw()
{
	if (!this->IsVisible())
		return;

	this->DrawSprite();
	this->DrawBorder();
}
