#ifndef __KEYB_MGR_H_
#define __KEYB_MGR_H_

#include "config.h"
#include <SDL/SDL.h>

enum InputEventType { InputKBoard, InputMouse };
enum InputEventState { InputStatePressed, InputStateReleased };


struct Input
{
	Input(size_t key_set_number)
	{
		key_set = key_set_number;
	};

	void clear()
	{
		memset( held, 0, sizeof(held) );
		memset( released, 0, sizeof(held) );
	}

	size_t key_set;

	bool held[(size_t)(cakLastKey)];
	bool released[(size_t)(cakLastKey)];
};

struct InputKBEvent
{
	SDLKey key;
	Uint16 symbol;
};

struct InputMouseEvent
{
	int key;
};

// ����������� ������� �����
struct InputEvent
{
	InputEventType type;
	InputEventState state;
	union 
	{
		InputKBEvent kb;
		InputMouseEvent mouse;
	};
};

class InputMgr
{
public:
	enum MosueButtons {MOUSE_BTN_LEFT, MOUSE_BTN_RIGHT, MOUSE_BTN_MIDDLE, MOUSE_BTN_LAST};

	SDL_Joystick* joystick[MAX_JOYSTICKS];
	int joysticks_num;
	bool joystick_axis_pressed[MAX_JOYSTICKS][MAX_JOYSTICK_AXES];

	static const UINT keys_count = SDLK_LAST + MAX_JOYSTICKS * JOYSTICK_VK + 1;
	static const UINT mousebtns_count = MOUSE_BTN_LAST;


	bool joy_hat[MAX_JOYSTICKS][4];

	vector< Input* > registered_inputs;

	typedef list<InputEvent*> EventsContainer;
	typedef EventsContainer::iterator EventsIterator;

	EventsContainer events;			// ������ ����������� ������� �����

	// ���������� �������
	int mouseX;
	int mouseY;
	int mouseX_last;
	int mouseY_last;
	bool mouse_moved;					// ���������� ������� ����������
	bool mouse[mousebtns_count];		// ��������� ������ ����
	
	// ����������� �������� ������ � ���� (��������� ����������) ��� ��������� ��������� ������� �����
	float scrwinX;
	float scrwinY;

	UINT repeatWaitingTime;				// ����� ��������, ������ ��� �������� ����������
	UINT repeatTime;					// ����� ����� ������������


	InputMgr();
	~InputMgr();

	void PreProcess();
	void Process();

	void AddEvent(SDL_JoyAxisEvent &ev);
	void AddEvent(SDL_JoyHatEvent &ev);
	void AddEvent(SDL_JoyButtonEvent &ev);
	void AddEvent(SDL_KeyboardEvent &ev);
	void AddEvent(SDL_MouseButtonEvent &ev);
	void AddEvent(SDL_MouseMotionEvent &ev);

	void RegisterInput( Input* inp, size_t key_set );
	void UnregisterInput( Input* inp );
	void ClearInput();

	void SetScreenToWindowCoeff(float kx, float ky);
	
	void FlushEvents();
	void ArrayFlush();
	void ProlongedArrayFlush();


	bool IsPressed(ConfigActionKeys key) const;
	bool IsReleased(ConfigActionKeys key) const;
	bool IsHolded(ConfigActionKeys key) const;
	bool IsRepeated(ConfigActionKeys key) const;
	bool IsProlongedReleased(ConfigActionKeys key) const;
	bool IsPressed(ConfigActionKeys key, size_t key_set) const;
	bool IsReleased(ConfigActionKeys key, size_t key_set) const;
	bool IsHolded(ConfigActionKeys key, size_t key_set) const;
	bool IsRepeated(ConfigActionKeys key, size_t key_set) const;
	bool IsProlongedReleased(ConfigActionKeys key, size_t key_set) const;


	bool IsHolded(UINT key) const;
private:
	bool pressed[keys_count];
	bool holded[keys_count];
	bool released[keys_count];
	bool repeated[keys_count];

	// ��� ������� ��������� ����, � ����� ������ UpdateGame
	//bool prolonged_pressed[keys_count];
	//bool long_holded[keys_count];
	bool prolonged_released[keys_count];

	bool CheckKeyInArray(const bool* arr, ConfigActionKeys key) const;
	bool CheckKeyInArray(const bool* arr, ConfigActionKeys key, size_t ket_set) const;

	enum ERepeatingState { rs_none, rs_waiting, rs_repeating };
	ERepeatingState repeatingState;			// ��������� ����������
	UINT repeat_timer;						// ������ ��� ���������� �������
	InputEvent* event_to_repeat;			// ������� ������� ��� ����������
};

#endif // __KEYB_MGR_H_
