typedef unsigned int                UINT;
typedef unsigned short              USHORT;
typedef unsigned char               BYTE;
typedef unsigned char               UCHAR;
typedef unsigned char               byte;
typedef byte*                       pbyte;
typedef int                         BOOL;

typedef int                         LuaRegRef;


#ifdef _MSC_VER
typedef __int32 int32_t;
typedef unsigned __int32 uint32_t;
#else
#include <stdint.h>
#endif


//#endif // __TYPES_H_
