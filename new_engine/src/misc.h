#ifndef __MISC_H_
#define __MISC_H_

//////////////////////////////////////////////////////////////////////////
// ��� ��������� �� ��������� ������ ����������������� � ������� ����� ���������
#ifdef FIND_MEM_LEAKS
//#define DEBUG_STOP_ON_ALLOC_NUM 152893
#endif // FIND_MEM_LEAKS
//////////////////////////////////////////////////////////////////////////

char* StrDupl (const char *str);

void EndupLogs();

void CheckDefaultConfig();
char* ExtFromPath(const char* path);
char* GetNameFromPath(const char* path);
void DeleteFileNameFromEndOfPathToFile(char *src);
int ChangeDir(const char* path);
bool FindFile(const char * const file_name, const char * path, char * const buffer);
bool FindAllFiles(const char * path, char * const buffer, void (*callback)(const char* const) );

void InitPaths(void);
void CheckPaths();
void CleanupLogs();
void LogPaths(void);

const char* LogLevelGetName( LogLevel ll );
//bool LogLevelLowerThan( const char* Event );

void CreateLogFile(const char *fname);
#if !defined(NO_LOGGING)
void Log(const char* fname, LogLevel Event, const char* Format,...);
void LogError(const char* Format,...);
#else
#define Log(...)
#define LogError(...)
#endif
void SuppressLogMessages(LogLevel level = logLevelNone);
bool FileExists(const char* file_name);
/**
*	������ ���� ��������� ���. ��� �� ��������. TODO!
*	�� �� � ��� ����� ������� ��� � �-� Log � �� ������ ������ ��������� ��������.
*/

class CDummy
{
public:
	CDummy();
	~CDummy();
};


//////////////////////////////////////////////////////////////////////////

// �������� �� ��, ��� ��� ����� � ��������� ����� ������ ���� � ����� � ����������� ���������.
// float � �������, ����� ���� 1e-35, ��� ����� 0, �� �� 0. ��� �� ����� �������� ������ ����� �� �����.
__INLINE bool IsNear(float a, float b, float accuracy)
{
	return fabs(a-b) < accuracy;
}

template <typename T>
T nextPowerOfTwo(T n)
{
	std::size_t k=1;
	--n;
	do {
		n |= n >> k;
		k <<= 1;
	} while (n & (n + 1));
	return n + 1;
}

template <typename T>
T isPowerOfTwo(T n)
{
	return ( (n & (n-1) ) == 0);
}

#ifndef INVPI
#define INVPI 0.31830988618379067154f	// 1/pi
#endif

template <typename T>
T FastSimpleSin(T x)
{
	// simplify the range reduction
	// reduce to a -0.5..0.5 range, then apply polynomial directly
	//const T P = (T)0.225; // const float Q = 0.775;

	x = x * (T)INVPI;
	int k = (int)(x);
	x = x - k;

	T y = (4 - 4 * abs(x)) * x;

	return (k&1) ? -y : y;
}

template <typename T>
T FastPreciseSin(T x)
{
	int k = (int)(x);

	//  const float Q = 0.775;
	const T P = (T)0.225;

	T y = FastSimpleSin<T>(x);
	if ( k&1 )
		y = P * (y * abs(y) - y) + y;   // Q * y + P * y * abs(y)
	else
		y = -(P * (-y * abs(y) + y) - y);   // Q * y + P * y * abs(y)

	return y;
}

__INLINE float InvSqrt (float x)
{
	float xhalf = 0.5f*x;
	union int_float
	{
		int i;
		float f;
	} ifx;
	ifx.f = x;
	//long i = *(long*)&x;
	ifx.i = 0x5f3759df - (ifx.i >> 1); // This line hides a LOT of math!
	//x = *(float*)&i;
	x = ifx.f;
	x = x*(1.5f - xhalf*x*x); // repeat this statement for a better approximation
	return x;
}

// ��������, ������������ �� ������ �� ���������
// ������ ������������ endswith
// ������ � http://stackoverflow.com/questions/874134/find-if-string-endswith-another-string-c
bool endswith(std::string const &fullString, std::string const &ending);

bool startswith(std::string const &fullString, std::string const &beginning);
#endif // __MISC_H_
