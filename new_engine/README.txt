����, �� ������� ��������. ������ ��������� ������ ������������ ������ - ��� � ������� � ���������? ������� ���������.

��� ������. ���������� lua ������ 5.1. � ������ ������� �������� ������������� ����������� ���, �� ������ �� �� ���� ����������. Lua for windows: http://code.google.com/p/luaforwindows/downloads/list, lua: http://www.lua.org/versions.html#5.1

��� ������. ����� ���������, ����� �� �������� ����. ������� ����� ������� �� ��� ����� �������� ����� ������������ ������ ������ ��� windows. ���� �� �� ���������� ���������� �����, �, ������, ������ ���������� � ����������, �� ����� ����� ��������� ������ ����������. ����� ���� 3 � 4 ����� ����������.

��� ������. ��������� ����������. ��� ������ �������� ���������:

1. lua 5.1 - ��. ���� "��� ������"
2. DevIL - IL, http://openil.sourceforge.net/download.php
3. zziplib - zzip, http://zziplib.sourceforge.net/download.html
����������� .lib ��� windows ����� � lib ��������
4. SDL - SDL, http://www.libsdl.org/download-1.2.php
5. SDL_net - SDL_net, http://www.libsdl.org/projects/SDL_net/
6. Bass - bass, ��� � ���������, ������������, ��. �����
7. SDL_mixer - SDL_mixer, http://www.libsdl.org/projects/SDL_mixer/, ������������. ������������ ����������� ������������ ��� ������ � ���, ������, ���� ��� ����� ������� ��� Bass (����. FreeBSD), ��������� �� �� ��������� ������������ ������ ������ ���� � �������� ��� ������ �������.

�������� �������� �� ��, ��� ����������� ���� ��������� ���� � ������������ ������� �������������. ��������, ��� ubuntu ��������� ���� ����������� ��������� ����������� ���:
apt-get install liblua5.1-dev libdevil-dev libsdl2.1-dev libsdl-net1.2-dev libzzip-dev

��� ��������. ����������. ������ �� ��� ������� ������� ��� Microsoft Visual Studio 2008 � 2010, ������� makefile � CMakeLists.txt ��� ������������� cmake.

1. ������ � ������� Visual Studio 2008/2010
� ������� ���� ��������� ������������. ����� �������� DebugStatic, ReleaseStatic, Editor.
���������� �������� ���� � ������������� ����������� ��� � ��������� ������ ��� � ��������� �������. ���� �� �� ������, ��� ��� ������, �� ������������ ���������� ������. � VS2010 ����� ����� ������ �������� �� � $(SolutionDir)..\..\libs, ��� $(SolutionDir) - ����� new_engine.

2. ������ � ������� CMake
�������������� ������ ������ ���� �� ��������, �������� �� Windows. ������ �������������:
cmake CMakeLists.txt && make

3. ������ � ������� Makefile
����������� �������� ����� ����������� �������������� ��������� (��������������� ����������� �������� ��������� � ����� ������������), ������� �� ������������� ������������ ���, �� ����������� �������, ����� ������������ cmake �� ��� ��� ���� ������� ����������.
�������������� ��������� �����: release (�� ���������), release_sdl_mixer, release_nosound, debug, debug_nosound, editor, valgrind.
������� *_nosound ���������� ��� ���������� Bass (��� ���, � ���� �� �����-���� �������� ��� ���������� �� ��������).
����� ������� ������ ����� ���������� ��������� make clean.
������������������ ���� � ����������� �������� � ����� ��� �������������� ��������� � ����� iichantra.Makefile � ���������� INCLUDES, LDLIBS, LDFLAGS � �.�.
�������������� ������ �� ���������� Linux � Darwin (MacOs). ��������� ������������ � ������� uname -s.

��� �����. ������. ���� �� ��������� ������, �� �������� ����� ������ ���� ��������������� � ���������� bin. ����� ������ � ������� Visual Studio ��� iichantra.Release.exe, � cmake - iichantra � ����������� ���������� - iichantra.release




So you have downloaded iiChantra. How do you build and un it? In four relatively simple steps.

Step 1. Install lua5.1. Old iiChantra releases used its own modified lua library, but not anymore. You will first need to install it. Lua for windows: http://code.google.com/p/luaforwindows/downloads/list, lua: http://www.lua.org/versions.html#5.1

Step 2. Decide if you even need to build iiChantra. Most of the time you can find recent windows binaries in the bin directory of the svn repository. If you don't need to change the code and just want to, say, use the map editor, it could be enough. If so, skip steps 3 and 4.

Step 3. Install other libraries. To build iiChantra you will need:

1. lua5.1 - if you didn't install this one then shame on you, why did you skip step 1?
2. DevIl - IL, http://openil.sourceforge.net/download.php
3. zziplib - zzip, http://zziplib.sourceforge.net/download.html
You can also find a static .lib for windows inside the lib directory of iiChantra, so you don't need to build it yourself
4. SDL 1.2 - SDL, http://www.libsdl.org/download-1.2.php
5. SDL_net - SDL_net, http://www.libsdl.org/projects/SDL_net/
6. Bass - bass, comes with iiChantra and is optional (more on that in step 4)
7. SDL_mixer - SDL_mixer, http://www.libsdl.org/projects/SDL_mixer/, optional. You should only use this library for sound if there is no Bass for your system (i.e. FreeBSD), since it often mis-renders game music format and causes various other problems.

Note that most of thies libraries can be found in major linux distributions repositories. For example. installing all necessary libraries on ubuntu would like like this:
apt-get install liblua5.1-dev libdevil-dev libsdl1.2-dev libsdl-net1.2-dev libzzip-dev

Step 4. Building iiChantra. In our code repository you can usually find Microsoft Visual Studio 2008 and 2010 solution files, a makefile and CMakeLists.txt for using cmake.

1. Building with Visual Studio
You can use solution targets to build normal game (ReleaseStatic, Release Dynamic), debug version (DebugStatic, DebugDynamic) and the map editor (Editor).
You will need to add the paths to the libraries you installed to either solution properties or studio properties. If you don't need how to do that you should read how to install and use libraries elsewhere. With Visual Studio 2010 solution you can also place all of them to $(SolutionDir)..\..\libs where $(SolutionDir) is a directory containg the solution file.

2. Building with CMake
This method is preferred on all systems, other than Windows. Usage example:
cmake CMakeLists.txt && make

3. Building with makefile
Default makefile, provided with the game may require addiotional changes (such as changing the library names to match those in your distribution), so you should only use it if cmake is not avaiable for some reason.
Using makefile you can build several targets: release (default), release_sdl_mixer, release_nosound, debug, debug_nosound, valgrind
Targets with _nosound do not use the Bass library, use it if for some reason it doesn't work.
Before changing build target it is recomended to clean up by running make clean.
Platform-dependent library paths are stored in iichantra.Makefile variables INCLUDES, LDLIBS, LDFLAGS and others. Supported platforms are Linux and Darwin (MacOS), the one to use is determined by using uname -s.

Step 5. Running iiChantra. If everything went well, binary files can now be found insinde bin directory. They are called: iichantra.Release.exe (if built with VS), iichantra (if built with cmake) or iichantra.relase (if built with included makefile).

