II_SRC :=$(ROOT_DIR)/src/

all:

OBJ_DIR := obj
BIN_DIR := bin
NAME := CAABBTest

TARGET_NAME := $(NAME).$(TARGET)
TARGET_FULLNAME := $(BIN_DIR)/$(TARGET_NAME)

CFLAGS   :=$(GTEST_CFLAGS)
CXXFLAGS :=
LDFLAGS  :=-L$(ROOT_DIR)lib $(GTEST_LDFLAGS)
LDLIBS   :=
INCLUDES :=-I$(II_SRC)
SRC :=$(wildcard *.cpp)
SRC +=$(II_SRC)game/phys/phys_misc.cpp


#######################################
# Magic
#######################################

.PHONY: test run_test clean_test

WORK_INCLUDES :=$(INCLUDES) #$(PLATFORM_INCLUDES_$(PLATFORM)) $(TARGET_INCLUDES_$(TARGET))
WORK_LDLIBS   :=$(LDLIBS) #$(PLATFORM_LDLIBS_$(PLATFORM)) $(TARGET_LDLIBS_$(TARGET))
WORK_CFLAGS   :=$(CFLAGS) $(TARGET_CFLAGS_$(TARGET)) #$(PLATFORM_CFLAGS_$(PLATFORM)) 
WORK_CXXFLAGS :=$(CXXFLAGS) #$(PLATFORM_CXXFLAGS_$(PLATFORM)) $(TARGET_CXXFLAGS_$(TARGET))
WORK_LDFLAGS  :=$(LDFLAGS) #$(PLATFORM_LDFLAGS_$(PLATFORM)) $(TARGET_LDFLAGS_$(TARGET))
WORK_SRC      :=$(SRC) #$(PLATFORM_SRC_$(PLATFORM)) $(TARGET_SRC_$(TARGET))

WORK_OBJ_DIR :=$(OBJ_DIR)/$(PLATFORM)__$(TARGET)

WORK_OBJ := $(patsubst %,%.o,$(notdir $(WORK_SRC)))
WORK_OBJ := $(patsubst %,$(WORK_OBJ_DIR)/%,$(WORK_OBJ))

WORK_DEP := $(WORK_OBJ:.o=.d)

WORK_OBJ_DIRS := $(sort $(dir $(WORK_OBJ)))

#######################################
# Rules 
#######################################


#$(info $(SRC))
#$(info $(WORK_OBJ))
#$(info $(WORK_OBJ_DIRS))

$(OBJ_DIR) :
	mkdir -p $@
$(WORK_OBJ_DIRS) : 
	mkdir -p $@
$(BIN_DIR) :
	mkdir -p $@

define O_template =
$(WORK_OBJ_DIR)/$$(patsubst %,%.o,$$(notdir $(1))): $(1) 
	$(CXX) $$< -c -MMD $(WORK_CFLAGS) $(WORK_CXXFLAGS) $(WORK_INCLUDES) -o $$@
endef
 
 
$(foreach src,$(WORK_SRC),$(eval $(call O_template,$(src))))

$(TARGET_FULLNAME): $(WORK_OBJ) | $(BIN_DIR) 
	$(CXX) $^ $(WORK_LDFLAGS) $(WORK_LDLIBS) -o $@

-include $(WORK_DEP)

test: $(WORK_OBJ_DIRS)
test: $(TARGET_FULLNAME)

run_test:
	$(TARGET_FULLNAME)
	
clean_test:
	-rm -rf $(WORK_OBJ_DIR)
	-rm -rf $(BIN_DIR)