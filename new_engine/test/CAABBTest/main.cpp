#include <gtest/gtest.h>

#define M_PI_2     1.57079632679489661923
#define M_PI_4     0.785398163397448309616

#include "types.h"

#include "game/phys/phys_misc.h"

class CAABBTest : public testing::Test 
{
protected:

	virtual void SetUp()
	{
	}

	virtual void TearDown()
	{
	}

	static __INLINE bool IsNear(float a, float b, float accuracy)
	{
		return fabs(a-b) < accuracy;
	}

};



#define EXPECT_EQF(expe, act) EXPECT_PRED3(IsNear, expe, act, 1e-6f)

TEST_F(CAABBTest, RotSq90)
{
	CAABB a(1, 1, 3, 3);
	EXPECT_EQF(2.f, a.p.x);
	EXPECT_EQF(2.f, a.p.y);
	EXPECT_EQF(1.f, a.W);
	EXPECT_EQF(1.f, a.H);

	EXPECT_EQF(1.f, a.Left());
	EXPECT_EQF(3.f, a.Right());
	EXPECT_EQF(1.f, a.Top());
	EXPECT_EQF(3.f, a.Bottom());

	a.Rotate(M_PI_2);

	EXPECT_EQF(2.f, a.p.x);
	EXPECT_EQF(2.f, a.p.y);
	EXPECT_EQF(1.f, a.W);
	EXPECT_EQF(1.f, a.H);

	EXPECT_EQF(1.f, a.Left());
	EXPECT_EQF(3.f, a.Right());
	EXPECT_EQF(1.f, a.Top());
	EXPECT_EQF(3.f, a.Bottom());
}

TEST_F(CAABBTest, RotSq45)
{
	CAABB a(1, 1, 3, 3);
	EXPECT_EQF(2.f, a.p.x);
	EXPECT_EQF(2.f, a.p.y);
	EXPECT_EQF(1.f, a.W);
	EXPECT_EQF(1.f, a.H);

	EXPECT_EQF(1.f, a.Left());
	EXPECT_EQF(3.f, a.Right());
	EXPECT_EQF(1.f, a.Top());
	EXPECT_EQF(3.f, a.Bottom());

	a.Rotate(M_PI_4);

	EXPECT_EQF(2.f, a.p.x);
	EXPECT_EQF(2.f, a.p.y);
	EXPECT_EQF(1.414213562f, a.W);
	EXPECT_EQF(1.414213562f, a.H);

	EXPECT_EQF(0.585786437f, a.Left());
	EXPECT_EQF(3.414213562f, a.Right());
	EXPECT_EQF(0.585786437f, a.Top());
	EXPECT_EQF(3.414213562f, a.Bottom());
}

TEST_F(CAABBTest, RotSq90AroundLT)
{
	CAABB a(1, 1, 3, 3);
	EXPECT_EQF(2.f, a.p.x);
	EXPECT_EQF(2.f, a.p.y);
	EXPECT_EQF(1.f, a.W);
	EXPECT_EQF(1.f, a.H);

	EXPECT_EQF(1.f, a.Left());
	EXPECT_EQF(3.f, a.Right());
	EXPECT_EQF(1.f, a.Top());
	EXPECT_EQF(3.f, a.Bottom());


	a.Rotate(M_PI_2, Vector2(a.Left(), a.Top()));

	EXPECT_EQF(0.f, a.p.x);
	EXPECT_EQF(2.f, a.p.y);
	EXPECT_EQF(1.f, a.W);
	EXPECT_EQF(1.f, a.H);

	EXPECT_EQF(-1.f, a.Left());
	EXPECT_EQF(1.f, a.Right());
	EXPECT_EQF(1.f, a.Top());
	EXPECT_EQF(3.f, a.Bottom());
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
