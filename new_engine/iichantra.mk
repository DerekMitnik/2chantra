export LD_RUN_PATH=.
uname_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')

TARGET := release
export PLATFORM :=$(uname_S)

BIN_DIR := bin
OBJ_DIR := obj
SRC_DIR := src
NAME := iichantra

# First rule
all: work

#######################################
# Common settings
#######################################

TARGET_NAME := $(NAME).$(TARGET)
TARGET_FULLNAME := $(BIN_DIR)/$(TARGET_NAME)

CFLAGS   :=
CXXFLAGS :=
LDFLAGS  :=-Llib
LDLIBS   :=
INCLUDES :=-Isrc
SRC := StdAfx.cpp \
		config.cpp \
		input_mgr.cpp \
		main.cpp \
		misc.cpp \
		game/crc32.cpp \
		game/md5.cpp \
		resource_mgr.cpp \
		scene.cpp \
		game/animation.cpp \
		game/camera.cpp \
		game/game.cpp \
		game/highscores.cpp \
		game/http_client.cpp \
		game/editor.cpp \
		game/net.cpp \
		game/object_mgr.cpp \
		game/particle_system.cpp \
		game/player.cpp \
		game/proto.cpp \
		game/ribbon.cpp \
		game/sprite.cpp \
		game/objects/material.cpp \
		game/objects/object.cpp \
		game/objects/object_bullet.cpp \
		game/objects/object_character.cpp \
		game/objects/object_dynamic.cpp \
		game/objects/object_effect.cpp \
		game/objects/object_enemy.cpp \
		game/objects/object_environment.cpp \
		game/objects/object_item.cpp \
		game/objects/object_particle_system.cpp \
		game/objects/object_physic.cpp \
		game/objects/object_player.cpp \
		game/objects/object_ray.cpp \
		game/objects/object_ribbon.cpp \
		game/objects/object_spawner.cpp \
		game/objects/object_sprite.cpp \
		game/objects/object_waypoint.cpp \
		game/objects/weapon.cpp \
		game/phys/phys_collisionsolver.cpp \
		game/phys/phys_misc.cpp \
		game/phys/2de_Math.cpp \
		game/phys/2de_Box.cpp \
		game/phys/2de_Geometry.cpp \
		game/phys/2de_Matrix2.cpp \
		game/phys/sap/ASAP_PairManager.cpp \
		game/phys/sap/IceAllocator.cpp \
		game/phys/sap/IceBitArray.cpp \
		game/phys/sap/IceContainer.cpp \
		game/phys/sap/IceRevisitedRadix.cpp \
		game/phys/sap/OPC_ArraySAP.cpp \
		gui/gui.cpp \
		gui/gui_button.cpp \
		gui/gui_label.cpp \
		gui/gui_picture.cpp \
		gui/gui_textfield.cpp \
		gui/gui_widget.cpp \
		gui/gui_panel.cpp \
		gui/text_typer.cpp \
		render/font.cpp \
		render/image.cpp \
		render/renderer.cpp \
		render/texture.cpp \
		script/api.cpp \
		script/CUData.cpp \
		script/CUDataUser.cpp \
		script/gui_api.cpp \
		script/lua_pusher.cpp \
		script/luathread.cpp \
		script/object_api.cpp \
		script/script.cpp \
		script/timerevent.cpp \
		script/udata.cpp \
		sound/snd.cpp

#######################################
# Platform-specific settings
#######################################

ifeq (FreeBSD,$(PLATFORM))
$(info using fallback settings for lua)
LUA_CFLAGS :=-I/usr/local/include/lua51
LUA_LDLIBS :=-llua-5.1
else
#ifeq (Linux,$(PLATFORM))

ifeq ($(strip $(shell pkg-config --exists lua && echo ok)),ok)
$(info have lua)
LUA_CFLAGS :=$(shell pkg-config --cflags --silence-errors lua)
LUA_LDLIBS :=$(shell pkg-config --libs --silence-errors lua)
else ifeq ($(strip $(shell pkg-config --exists lua5.1 && echo ok)),ok)
$(info have lua5.1)
LUA_CFLAGS :=$(shell pkg-config --cflags --silence-errors lua5.1)
LUA_LDLIBS :=$(shell pkg-config --libs --silence-errors lua5.1)
else
$(info have no lua, fallback settings)
LUA_CFLAGS :=-I/usr/include/lua5.1
LUA_LDLIBS :=-llua5.1
endif
endif # ifeq * platform

ifeq ($(strip $(shell pkg-config --exists sdl && echo ok)),ok)
$(info have sdl)
SDL_CFLAGS :=$(shell pkg-config --cflags --silence-errors sdl)
SDL_LDLIBS :=$(shell pkg-config --libs --silence-errors sdl)
else
$(info have no sdl, fallback settings)
SDL_CFLAGS :=
SDL_LDLIBS :=-lSDL -lSDL_net
endif

export PLATFORM_INCLUDES_Linux :=
export PLATFORM_LDLIBS_Linux   :=-lGL -lIL -lSDL -lSDL_net -lzzip $(LUA_LDLIBS)
export PLATFORM_CFLAGS_Linux   :=$(LUA_CFLAGS)
export PLATFORM_CXXFLAGS_Linux :=
export PLATFORM_LDFLAGS_Linux  :=
export PLATFORM_SRC_Linux      :=

export PLATFORM_INCLUDES_FreeBSD :=-I/usr/local/include
export PLATFORM_LDLIBS_FreeBSD   :=-lGL -lIL -lSDL -lSDL_net -lzzip $(LUA_LDLIBS)
export PLATFORM_CFLAGS_FreeBSD   :=$(LUA_CFLAGS) -DNOSOUND_BASS 
export PLATFORM_CXXFLAGS_FreeBSD :=
export PLATFORM_LDFLAGS_FreeBSD  :=-L/usr/local/lib
export PLATFORM_SRC_FreeBSD      :=

export PLATFORM_INCLUDES_Darwin :=
export PLATFORM_LDLIBS_Darwin   :=-framework OpenGL -lIL -lSDL -lSDL_net -lzzip $(LUA_LDLIBS) $(SDL_LDLIBS)
export PLATFORM_CFLAGS_Darwin   :=$(LUA_CFLAGS) $(SDL_CFLAGS)
export PLATFORM_CXXFLAGS_Darwin :=
export PLATFORM_LDFLAGS_Darwin  :=
export PLATFORM_SRC_Darwin      :=SDLMain.m

PLATFORM_BASS_Linux_32  :=-lbass
PLATFORM_BASS_Linux_64  :=-lbass_x64
PLATFORM_BASS_Darwin_32 :=-lbass
PLATFORM_BASS_Darwin_64 :=-lbass
PLATFORM_BASS_FreeBSD_32 :=
PLATFORM_BASS_FreeBSD_64 :=

LBITS := $(shell getconf LONG_BIT)

BASS_LIB := $(PLATFORM_BASS_$(PLATFORM)_$(LBITS))

#######################################
# Target-specific settings
#######################################

export TARGET_INCLUDES_release :=
export TARGET_LDLIBS_release   :=$(BASS_LIB)
export TARGET_CFLAGS_release   :=-O2 -Wall -DNDEBUG
export TARGET_CXXFLAGS_release :=
export TARGET_LDFLAGS_release  :=
export TARGET_SRC_release      :=

export TARGET_INCLUDES_release_nosound :=
export TARGET_LDLIBS_release_nosound   := 
export TARGET_CFLAGS_release_nosound   :=$(TARGET_CFLAGS_release) -DNOSOUND_BASS 
export TARGET_CXXFLAGS_release_nosound :=
export TARGET_LDFLAGS_release_nosound  :=
export TARGET_SRC_release_nosound      :=

export TARGET_INCLUDES_debug :=
export TARGET_LDLIBS_debug   :=$(BASS_LIB)
export TARGET_CFLAGS_debug   :=-Wall -g -O0 -DDEBUG
export TARGET_CXXFLAGS_debug :=
export TARGET_LDFLAGS_debug  :=
export TARGET_SRC_debug      :=

export TARGET_INCLUDES_debug_nosound :=
export TARGET_LDLIBS_debug_nosound   := 
export TARGET_CFLAGS_debug_nosound   :=$(TARGET_CFLAGS_debug) -DNOSOUND_BASS 
export TARGET_CXXFLAGS_debug_nosound :=
export TARGET_LDFLAGS_debug_nosound  :=
export TARGET_SRC_debug_nosound      :=

export TARGET_INCLUDES_editor :=
export TARGET_LDLIBS_editor   :=$(TARGET_LDLIBS_release)
export TARGET_CFLAGS_editor   :=$(TARGET_CFLAGS_release) -DMAP_EDITOR
export TARGET_CXXFLAGS_editor :=
export TARGET_LDFLAGS_editor  :=
export TARGET_SRC_editor      :=

export TARGET_INCLUDES_editor_nosound :=
export TARGET_LDLIBS_editor_nosound   := 
export TARGET_CFLAGS_editor_nosound   := $(TARGET_CFLAGS_editor) -DNOSOUND_BASS 
export TARGET_CXXFLAGS_editor_nosound :=
export TARGET_LDFLAGS_editor_nosound  :=
export TARGET_SRC_editor_nosound      :=

export TARGET_INCLUDES_valgrind :=
export TARGET_LDLIBS_valgrind   :=
export TARGET_CFLAGS_valgrind   :=-W -Wall -g -O -DNDEBUG -DNOSOUND_BASS
export TARGET_CXXFLAGS_valgrind :=
export TARGET_LDFLAGS_valgrind  :=
export TARGET_SRC_valgrind      :=

#######################################
# Magic
#######################################

.PHONY: release release_nosound debug debug_nosound editor editor_nosound test clean_test work clean distclean

SUPPORTED_TARGETS := release release_nosound debug debug_nosound editor editor_nosound valgrind
SUPPORTED_PLATFORMS := Linux Darwin FreeBSD

ifeq (,$(filter $(TARGET),$(SUPPORTED_TARGETS)))
$(error Unsopported TARGET=$(TARGET))
endif
ifeq (,$(findstring $(PLATFORM),$(SUPPORTED_PLATFORMS)))
$(warning Unknown PLATFORM=$(PLATFORM). Platform-specific settings are undefined - build will most likely fail.)
endif

WORK_INCLUDES :=$(INCLUDES) $(PLATFORM_INCLUDES_$(PLATFORM)) $(TARGET_INCLUDES_$(TARGET))
WORK_LDLIBS   :=$(LDLIBS) $(PLATFORM_LDLIBS_$(PLATFORM)) $(TARGET_LDLIBS_$(TARGET))
WORK_CFLAGS   :=$(CFLAGS) $(PLATFORM_CFLAGS_$(PLATFORM)) $(TARGET_CFLAGS_$(TARGET))
WORK_CXXFLAGS :=$(CXXFLAGS) $(PLATFORM_CXXFLAGS_$(PLATFORM)) $(TARGET_CXXFLAGS_$(TARGET))
WORK_LDFLAGS  :=$(LDFLAGS) $(PLATFORM_LDFLAGS_$(PLATFORM)) $(TARGET_LDFLAGS_$(TARGET))
WORK_SRC      :=$(SRC) $(PLATFORM_SRC_$(PLATFORM)) $(TARGET_SRC_$(TARGET))

WORK_OBJ_DIR :=$(OBJ_DIR)/$(PLATFORM)__$(TARGET)

WORK_OBJ := $(patsubst %,%.o,$(WORK_SRC))
WORK_OBJ := $(patsubst %,$(WORK_OBJ_DIR)/%,$(WORK_OBJ))

WORK_DEP := $(WORK_OBJ:.o=.d)

WORK_OBJ_DIRS := $(sort $(dir $(WORK_OBJ)))

export ROOT_DIR := $(shell pwd)

#######################################
# Rules 
#######################################

$(OBJ_DIR) :
	mkdir -p $@
$(WORK_OBJ_DIRS) : 
	mkdir -p $@
$(BIN_DIR) :
	mkdir -p $@

$(WORK_OBJ_DIR)/%.c.o: $(SRC_DIR)/%.c 
	$(CC) $< -c -MMD $(WORK_CFLAGS) $(WORK_INCLUDES) -o $@

$(WORK_OBJ_DIR)/%.cpp.o: $(SRC_DIR)/%.cpp 
	$(CXX) $< -c -MMD $(WORK_CFLAGS) $(WORK_CXXFLAGS) $(WORK_INCLUDES) -o $@

$(WORK_OBJ_DIR)/%.m.o: $(SRC_DIR)/%.m
	$(CXX) $< -c -MMD $(WORK_CFLAGS) $(WORK_CXXFLAGS) $(WORK_INCLUDES) -o $@

$(TARGET_FULLNAME): $(WORK_OBJ) | $(BIN_DIR) 
	$(CXX) $^ $(WORK_LDFLAGS) $(WORK_LDLIBS) -o $@

-include $(WORK_DEP)

work: $(WORK_OBJ_DIRS)
work: $(TARGET_FULLNAME)

test:
	$(MAKE) -C test -f test.mk test
	
run_test:
	$(MAKE) -C test -f test.mk run_test

clean_test:
	$(MAKE) -C test -f test.mk clean_test

clean:
	-rm -rf $(WORK_OBJ_DIR)
	-rm -f $(TARGET_FULLNAME)

distclean: clean
	-rm -rf #(OBJ_DIR)
