LoadTexture("rooms.png");
LoadPrototype("room.lua");
LoadPrototype("phys-empty-slope1.lua");
LoadTexture("weapons.png")
LoadPrototype("bonus-flamer.lua")
LoadPrototype("testenv.lua")
LoadSound("foot-left.ogg")
LoadSound("foot-right.ogg")
LoadSound("foot-stop.ogg")
LoadSound("foot-stop.ogg")
LoadSound("ammo-pickup.ogg")
LoadSound("boss-hit.ogg")
LoadSound("flame.ogg")
LoadPrototype("dust-run2.lua")
LoadPrototype("pteleport.lua")
LoadTexture("pteleport.png")
LoadPrototype("pblood.lua")
LoadTexture("pblood.png")


wsw = CreateWidget(constants.wt_Label, "health", nil, 90, 75, 40, 10)
WidgetSetCaption(wsw, "none + none = none")
WidgetSetCaptionFont(wsw, "default")
WidgetSetCaptionColor(wsw, {0.0, 1.0, 0.0, 1.0}, false)
ssw = CreateWidget(constants.wt_Label, "health", nil, 90, 85, 40, 10)
WidgetSetCaption(ssw, "SOH  sync: 0")
WidgetSetCaptionFont(ssw, "default")
WidgetSetCaptionColor(ssw, {0.0, 1.0, 0.0, 1.0}, false)
usw = CreateWidget(constants.wt_Label, "health", nil, 90, 95, 40, 10)
WidgetSetCaption(usw, "Unyl sync: 0")
WidgetSetCaptionFont(usw, "default")
WidgetSetCaptionColor(usw, {0.0, 1.0, 0.0, 1.0}, false)

bonuses = {}
sync = {}

function testenv(id, x, y, old_m, new_m)
	CreateParticleSystem("pblood", x, y)
end

stime = {}

function testenvstay(id, x, y)
	if not stime[id] then stime[id] = 0 end
	if GetCurTime() - stime[id] > 1000 then
		stime[id] = GetCurTime()
		testenv( id, x, y )
	end
end


function WeaponSystem( id, weapon )
	if not bonuses[id] then bonuses[id] = {} end
	local bt = bonuses[id]
	if #bt == 2 then
		table.remove(bt, 1)
	end
	table.insert( bt, weapon)
	local str = ( bt[1] or "none" ) .. " + " .. ( bt[2] or "none" ) .. " = "
	if #bt == 2 then
		if bt[1] == "flamer" and bt[2] == "spread" then
			WidgetSetCaption(wsw, str.."spread")
			return "spread"
		end
	end
	WidgetSetCaption(wsw, str.."none")
	return nil
end


function Heartbeat()
	local newstat
	if math.random(1,3) == 2 then
		newstat = { gravity_bonus_x = -0.5 }
	else
		newstat = { gravity_bonus_x = 0.5 }
	end
	SetEnvironmentStats(env, newstat)
	chars = { soh, unyl }
	for _, char in pairs(chars) do
		if active == char then
			ds = -1.0
		else
			ds = 1.0
		end
		if (ds*sync[char] < 0) or (sync[char] == 0 and ds<0) then
			ds = ds * difficulty
		else
			ds = ds / difficulty
		end
		sync[char] = sync[char] + ds
		if sync[char] > 30 then sync[char] = 30 end
		if sync[char] < -30 then sync[char] = -30 end
		if sync[char] < -10 and active == char then
			DamageObject(char, 5*math.floor(sync[char]/(-10))*difficulty)
		end
	end

	WidgetSetCaption(ssw, "SOH  sync: "..sync[soh])
	WidgetSetCaption(usw, "Unyl sync: "..sync[unyl])
end

function CharacterSystem( old_id, new_id )

	if active == soh then
		--changing to Unyl-chan	
		k = ( 30 + sync[unyl] ) / 30	
		stats = { jump_vel = 18+k, walk_acc=3+k, max_health=160/difficulty }
	else
		k = ( 30 + sync[soh] ) / 30	
		stats = { jump_vel = 18, walk_acc=3, max_health=(160+80*k)/difficulty }
	end

	SetPlayerStats(new_id, stats)
	local str = ( bonuses[new_id][1] or "none" ) .. " + " .. ( bonuses[new_id][2] or "none" ) .. " = ".. player.altWeaponName
	WidgetSetCaption(wsw, str)
	active = new_id
end

local group
--UserScriptEnd
CreateSprite("room", -25, 277);
soh = CreatePlayer("sohchan", 41, 473);
unyl = CreatePlayer("unylchan", 41, 473);
group = CreateSprite("phys-empty", 11, 559);
GroupObjects(group, CreateSprite("phys-empty-end", 596, 579));
group = CreateSprite("phys-empty-slope1", 422, 404);
GroupObjects(group, CreateSprite("phys-empty-slope1", 317, 544));
group = CreateSprite("phys-empty", 6, 396);
GroupObjects(group, CreateSprite("phys-empty-end", 67, 416));
group = CreateSprite("phys-empty", 125, 394);
GroupObjects(group, CreateSprite("phys-empty-end", 259, 416));
group = CreateSprite("phys-empty-cl", 409, 404);
GroupObjects(group, CreateSprite("phys-empty-cl-end", 454, 420));
group = CreateSprite("phys-empty", 454, 404);
GroupObjects(group, CreateSprite("phys-empty-end", 612, 420));
group = CreateSprite("phys-empty", -59, 235);
GroupObjects(group, CreateSprite("phys-empty-end", 17, 600));
group = CreateSprite("phys-empty", 578, 229);
GroupObjects(group, CreateSprite("phys-empty-end", 634, 613));
group = CreateSprite("phys-empty", -45, 219);
GroupObjects(group, CreateSprite("phys-empty-end", 612, 277));
CreateItem("bonus-flamer", 490, 509);
CreateItem("bonus-spread", 489, 361);
CreateItem("bonus-flamer", 28, 367);
CreateItem("bonus-spread", 544, 506);
group = CreateEnvironment("testenv", -20, 260)
GroupObjects(group, CreateEnvironment("testenv", 610, 410));
--UserScriptBegin
env = group
sync[soh] = 0
sync[unyl] = 0
bonuses[soh] = {}
bonuses[unyl] = {}
active = soh





