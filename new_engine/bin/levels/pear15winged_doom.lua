CreateMap({
	{constants.ObjBox, -1000, 300, 1000, 1000, -0.950000, { 0.000000, 0.000000, 0.000000, 1.000000 }},
	{constants.ObjGroup, "phys-empty", -425, 173, 401, 249, solid_to=255},
	{constants.ObjTile, "hw_tiles", 378, 178, 34, -0.900000},
	{constants.ObjTile, "hw_tiles", -389, 178, 34, -0.900000},
	{constants.ObjTile, "hw_floor", -375, 208, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -273, 208, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -171, 208, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -69, 208, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 33, 208, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 135, 208, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 237, 208, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 339, 208, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 441, 208, 3, -0.918229},
	{constants.ObjTile, "hw_floor", 441, 280, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 339, 280, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 237, 280, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 135, 280, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 33, 280, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -69, 280, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -171, 280, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -273, 280, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -375, 280, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -375, 352, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -273, 352, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -171, 352, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -69, 352, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 33, 352, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 237, 352, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 339, 352, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 441, 352, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -69, 424, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 33, 424, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 135, 424, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 237, 424, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 339, 424, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 441, 424, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -325, 262, 12, -0.900000},
	{constants.ObjTile, "hw_floor", -156, 226, 12, -0.900000},
	{constants.ObjTile, "hw_floor", -257, 262, 12, -0.900000},
	{constants.ObjTile, "hw_floor", -3, 208, 12, -0.902604},
	{constants.ObjTile, "hw_floor", 66, 217, 10, -0.900000},
	{constants.ObjTile, "hw_floor", -309, 217, 10, -0.900000},
	{constants.ObjTile, "hw_floor", 166, 226, 11, -0.900000},
	{constants.ObjTile, "hw_floor", 151, 379, 10, -0.866146},
	{constants.ObjTile, "hw_floor", -275, 298, 11, -0.900000},
	{constants.ObjTile, "hw_floor", 236, 280, 12, -0.900000},
	{constants.ObjTile, "hw_floor", 338, 217, 10, -0.900000},
	{constants.ObjTile, "hw_tiles", -448, 178, 34, -0.900000},
	{constants.ObjTile, "hw_floor", -104, 352, 12, -0.900000},
	{constants.ObjTile, "hw_floor", 15, 307, 10, -0.900000},
	{constants.ObjTile, "hw_floor", -223, 352, 11, -0.900000},
	{constants.ObjTile, "hw_tiles", 115, 352, 49, -0.900000},
	{constants.ObjTile, "hw_tiles", 214, 352, 48, -0.900000, mirrored=true},
	{constants.ObjTile, "hw_floor", 185, 370, 12, -0.900000},
	{constants.ObjTile, "hw_floor", 185, 334, 12, -0.900000},
	{constants.ObjTile, "hw_tiles", 220, 343, 38, -0.900000},
	{constants.ObjTile, "hw_tiles", -358, 199, 38, -0.905208},
	{constants.ObjTile, "hw_tiles", -188, 199, 38, -0.905208},
	{constants.ObjTile, "hw_tiles", 339, 325, 38, -0.900000},
	{constants.ObjTile, "hw_tiles", 157, 370, 9, -0.910417},
	{constants.ObjTile, "hw_tiles", -153, 178, 19, -0.900000},
	{constants.ObjTile, "hw_tiles", -212, 178, 19, -0.900000},
	{constants.ObjTile, "hw_tiles", -271, 178, 19, -0.900000},
	{constants.ObjTile, "hw_tiles", 319, 178, 19, -0.900000},
	{constants.ObjTile, "hw_tiles", 260, 178, 19, -0.900000},
	{constants.ObjTile, "hw_tiles", 201, 178, 19, -0.900000},
	{constants.ObjTile, "hw_tiles", 142, 178, 19, -0.900000},
	{constants.ObjTile, "hw_tiles", 83, 178, 19, -0.900000},
	{constants.ObjTile, "hw_tiles", 24, 178, 19, -0.900000},
	{constants.ObjTile, "hw_tiles", -330, 178, 19, -0.900000},
	{constants.ObjTile, "hw_tiles", -35, 178, 19, -0.900000},
	{constants.ObjTile, "hw_tiles", -94, 178, 19, -0.900000},
	{constants.ObjTile, "hw_floor", -375, 424, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -69, 496, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 33, 496, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 135, 496, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 237, 496, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 339, 496, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 441, 496, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -257, 451, 10, -0.866146},
	{constants.ObjTile, "hw_tiles", -293, 424, 49, -0.900000},
	{constants.ObjTile, "hw_floor", -223, 442, 12, -0.900000},
	{constants.ObjTile, "hw_floor", -223, 406, 12, -0.900000},
	{constants.ObjTile, "hw_tiles", -188, 415, 38, -0.900000},
	{constants.ObjTile, "hw_floor", -273, 496, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -171, 496, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -171, 424, 3, -0.905208},
	{constants.ObjTile, "hw_tiles", -251, 442, 9, -0.910417},
	{constants.ObjTile, "hw_tiles", -205, 424, 48, -0.900000, mirrored=true},
	{constants.ObjTile, "hw_floor", -375, 496, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -69, 568, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 33, 568, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 135, 568, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 237, 568, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 339, 568, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 441, 568, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -36, 514, 11, -0.900000},
	{constants.ObjTile, "hw_floor", 32, 406, 11, -0.900000},
	{constants.ObjTile, "hw_floor", -2, 433, 10, -0.900000},
	{constants.ObjTile, "hw_tiles", -103, 469, 38, -0.900000},
	{constants.ObjTile, "hw_tiles", -86, 451, 38, -0.900000},
	{constants.ObjTile, "hw_floor", -342, 424, 12, -0.900000},
	{constants.ObjTile, "hw_tiles", -392, 343, 38, -0.900000},
	{constants.ObjTile, "hw_floor", 134, 460, 12, -0.900000},
	{constants.ObjTile, "hw_floor", 286, 451, 10, -0.866146},
	{constants.ObjTile, "hw_tiles", 271, 469, 38, -0.900000},
	{constants.ObjTile, "hw_floor", 321, 370, 12, -0.900000},
	{constants.ObjTile, "hw_floor", -171, 568, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -273, 568, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -375, 568, 3, -0.905208},
	{constants.ObjGroup, "phys-empty", -511, -616, -425, 249, solid_to=255},
	{constants.ObjGroup, "phys-empty", 401, -616, 487, 249, solid_to=255},
	{constants.ObjTile, "hw_floor", 543, 568, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 543, 496, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 543, 424, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 543, 352, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 543, 280, 3, -0.905208},
	{constants.ObjTile, "hw_floor", 543, 208, 3, -0.918229},
	{constants.ObjTile, "hw_floor", -477, 568, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -477, 496, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -477, 424, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -477, 352, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -477, 280, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -477, 208, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -579, 208, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -579, 280, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -579, 352, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -579, 424, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -579, 496, 3, -0.905208},
	{constants.ObjTile, "hw_floor", -579, 568, 3, -0.905208},
	{constants.ObjTile, "hw_tiles", -507, 178, 34, -0.900000},
	{constants.ObjTile, "hw_tiles", -566, 178, 34, -0.900000},
	{constants.ObjTile, "hw_tiles", -625, 178, 34, -0.900000},
	{constants.ObjTile, "hw_tiles", 437, 178, 34, -0.902604},
	{constants.ObjTile, "hw_tiles", 496, 178, 34, -0.910417},
	{constants.ObjTile, "hw_tiles", 555, 178, 34, -0.913021},
	{constants.ObjGroup, "phys-empty-special", -560, -835, 560, -616, solid_to=125},
}, 4)
