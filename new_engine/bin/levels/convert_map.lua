if not arg[1] or not arg[2] then
	print("usage: "..arg[0].." <old_map_name> <new_map_name>")
	return
end

map_file = io.open(arg[1], "r")
new_map = io.open(arg[2], "w")

line = map_file:read()

new_map:write("CreateMap({\n")

while line do
	if string.match(line, "group%s*=%s*") then
		proto, x1, y1 = string.match(line, "CreateSprite%(%s*(\"[^\"]+\"),%s*([-%d]+),%s*([-%d]+)%)")
		line = "NONE"
	elseif string.match(line, "GroupObjects") then
		x2, y2 = string.match(line, "CreateSprite%(%s*\"[^\"]+\",%s*([-%d]+),%s*([-%d]+)%)")
		line = string.format("\t{constants.ObjGroup, %s, %i, %i, %i, %i},", proto, x1, y1, x2, y2)
	end
	if string.match(line, "phys%-empty") then
		if string.match( line, "%-end" ) then
			x2, y2 = string.match( line, "\", (%S+), (%S+)" )
			y1 = string.gsub(y1, ",.*", "")
			y2 = string.gsub(y2, ",.*", "")
			line = string.format("\t{constants.ObjGroup, %s, %s, %s, %s, %s},", proto, x1, y1, x2, y2)
		else
			--print(line)
			proto, x1, y1 = string.match( line, "(\"[^\"]+\"), (%S+), (%S+)" )
			--print(proto, x1, y1)
			line = ""
		end
	end
	z = string.match(line, "nil, ([^)]+)")
	if z then
		z = string.gsub(z, "%,", "%.")
	else
		z = ""
	end
	line = line.gsub(line, ", nil, [^\)]+", "")
	line = string.gsub(line, "CreateEnemy%(\"warhead%-launcher\",%s*(%S+),%s*(%S+)%);?", "\t{constants.ObjSpawner, \"technopoke-spawned\", %1, %2, 1, 1, constants.dirAny, 64},")
	line = string.gsub(line, "CreateEnemy%(\"penguin_omsk%-spawner%-right\",%s*(%S+),%s*(%S+)%);?", "\t{constants.ObjSpawner, \"slowpoke-spawned\", %1, %2, 1, 1, constants.dirAny, 64},")
	line = string.gsub(line, "CreateEnemy%(\"el bitardo\",%s*(%S+),%s*(%S+)%);?", "\t{constants.ObjSpawner, \"btard-spawned\", %1, %2, 1, 1, constants.dirAny, 64},")
	line = string.gsub(line, "CreateEnemy%(\"robot%-spawner%-right\",%s*(%S+),%s*(%S+)%);?", "\t{constants.ObjSpawner, \"slowpoke-moonwalking\", %1, %2, 1, 1, constants.dirAny, 64},")
	line = string.gsub(line, "CreateEnemy%(\"spawner%-([^\"]+)\",%s*(%S+),%s*(%S+)%);?", "\t{constants.ObjSpawner, \"%1\", %2, %3, 1, 1, constants.dirAny, 64},")
	for i=22,43 do
		line = string.gsub(line, "CreateSprite%(%s*\"er%-"..i.."f\",%s*(%S+),%s*(%S+)%);?", "")
	end
	line = string.gsub(line, "CreateSprite%(%s*\"er%-(%d+)f\",%s*(%S+),%s*(%S+)%);?", "\t{constants.ObjTile, \"rock-tiles\", %2, %3, %1, "..z.."},")
	line = string.gsub(line, "CreateSprite%(%s*(\"[^\"]+\"),%s*(%S+),%s*(%S+)%);?", "\t{constants.ObjSprite, %1, %2, %3},")
	line = string.gsub(line, "CreatePlayer%(%s*(\"[^\"]+\"),%s*(%S+),%s*(%S+)%);?", "\t{constants.ObjPlayer, %1, %2, %3},")
	line = string.gsub(line, "CreateItem%(%s*(\"[^\"]+\"),%s*(%S+),%s*(%S+)%);?", "\t{constants.ObjItem, %1, %2, %3},")
	if line~="NONE" and line ~= "" then new_map:write(line.."\n") end
	line = map_file:read()
end

new_map:write("})\n")

new_map:close()
map_file:close()