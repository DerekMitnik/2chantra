parent = "enemies/omich_flyer"

drops_shadow = 1;

animations = 
{
	{
		-- �������� �����
		name = "die";
		frames =
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pdust" },
			{ com = constants.AnimComCallFunctionWithStackParameter; txt = "map_trigger" },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0; },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{ 
		-- ����� ����
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{ 
		-- ������ �����
		name = "move";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealH; param = 58 },
			{ num = 14; dur = 100 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 21 },
			{ num = 15; dur = 100 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 21 },
			{ num = 16; dur = 100 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 21 },
			{ num = 17; dur = 100 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 21 },
			{ num = 18; dur = 100 },
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	}
}



