physic = 1;
phys_solid = 1;
phys_bullet_collidable = 1;
phys_max_x_vel = 0;
phys_max_y_vel = 0;
phys_one_sided = 0;


mass = -1;

mp_count = 1;
offscreen_behavior = constants.facingFixed

drops_shadow = 0;

gravity_x = 0;
gravity_y = 0;

faction_id = 1;
faction_hates = { -1, -2 };

-- �������� �������

texture = "sewermine";

z = 0.5;

animations = 
{
	{ 
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 5 },
			{ com = constants.AnimComRealY; param = 10 },
			{ com = constants.AnimComRealH; param = 42 },
			{ com = constants.AnimComRealW; param = 66 },
			{ com = constants.AnimComSetHealth; param = 1 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComStop; },

			{ dur = 100; num = 3 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 1 },

			{ dur = 150; num = 0 },

			{ dur = 100; num = 1 },
			{ dur = 100; num = 2 },
			{ dur = 50 ; num = 3 },

			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{ 
		name = "die";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion_big" },
		}
	},
	{ 
		name = "touch";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = "clockwork.ogg" },
			{ param = function( obj )
				Resume( NewMapThread( function()
					Wait( 1000 )
					if obj:object_present() then
						local pos = obj:aabb().p
						Game.CreateScoreItems( 1, pos.x, pos.y )
						DamageObject( obj, 28 )
					end
				end ))
			end },
			{ com = constants.AnimComRecover }
		}
	}
}



