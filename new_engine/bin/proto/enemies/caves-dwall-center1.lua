parent = "enemies/caves-dwall-bottom1"

animations =
{
	{
		name = "idle";
		frames =
		{
			{ dur = 1; num = 42 }
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 47 }                                      
		}
	},
	{
		name = "25%";
		frames =
		{
			{ dur = 1; num = 45 }                                      
		}
	},
}
