parent = "enemies/btard2"

phys_jump_vel = 0;

animations = 
{
	{ 
		-- Создание
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComSetHealth; param = 10*difficulty },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComControlledOverlayColor },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ param = function( obj )
				local enemy = GetNearestEnemy( obj )
				if not enemy then return end
				if obj:aabb().p.x > enemy:aabb().p.x then
					SetObjSpriteMirrored( obj, true )
				end
			end },
			{ com = constants.AnimComSetProcessor; param = RegisterFunction( function( obj )
				if not mapvar.tmp[obj] then 
					mapvar.tmp[obj] = { t = 0, time = Loader.time, rc=100+math.random(1000),gc=100+math.random(1000),bc=100+math.random(1000), smokefreq=500+math.random(1000), last_time = Loader.time, landed = false } 
				end
				local p = mapvar.tmp[obj]
				p.t = p.t + 3 + Loader.time - p.time
				p.time = Loader.time
				if obj:health() > 0 then
					if Loader.time - p.last_time > p.smokefreq then
						p.last_time = Loader.time
						local a = -1;
						if not obj:sprite_mirrored() then a = 1 end
						CreateParticleSystem( "pdust", obj:aabb().p.x + 20*a, obj:aabb().p.y-28, 0, { start_color = { math.random(), math.random(), math.random(), 1 }, end_color = { math.random(), math.random(), math.random(), 0 }, var_color = { 0.5, 0.5, 0.5, 0 }, start_size = 5, end_size = 1 } )
						obj:touchable( true )
					end
				end
				if Loader.time - p.last_time > 500 then
					obj:touchable( false )
				end
				SetObjSpriteColor( obj, { 	0.5+0.5*math.sin((p.t)   / p.rc), 
											0.5+0.5*math.sin((p.t/2) / p.gc), 
											0.5+0.5*math.sin((p.t/3) / p.bc),
											1 }, 0)
			end ) },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{
		name = "touch";
		frames =
		{
		 { com = constants.AnimComRecover; param = function ( obj )
			Game.characterHurt( GetPlayerCharacter(), Game.damage_types.substances )
		 end },
		}
	},
	{
		name = "final_count";
		frames = 
		{
			{ com = constants.AnimComInitW; param = 36 },
			{ com = constants.AnimComInitH; param = 75 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComControlledOverlayColor },
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ dur = 1, com = constants.AnimComSetProcessor; param = RegisterFunction( function( obj )
				if not mapvar.tmp[obj] then 
					mapvar.tmp[obj] = { t = 0, time = Loader.time, rc=100+math.random(1000),gc=100+math.random(1000),bc=100+math.random(1000), smokefreq=500+math.random(1000), last_time = Loader.time, landed = false } 
				end
				local p = mapvar.tmp[obj]
				p.t = p.t + 3 + Loader.time - p.time
				p.time = Loader.time
				SetObjSpriteColor( obj, { 	0.5+0.5*math.sin((p.t)   / p.rc), 
					0.5+0.5*math.sin((p.t/2) / p.gc), 
					0.5+0.5*math.sin((p.t/3) / p.bc),
					1 }, 0)
			end ) },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetShadow; param = 0 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetTrajectory; param = constants.pttLine },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComRealX; param = 11 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ dur = 80; num = 5 },
			{ com = constants.AnimComRealX; param = 4 },
			{ dur = 80; num = 6 },
			{ com = constants.AnimComRealX; param = 6 },
			{ dur = 80; num = 7 },
			{ com = constants.AnimComRealX; param = 18 },
			{ dur = 80; num = 8 },
			{ com = constants.AnimComRealX; param = 12 },
			{ dur = 80; num = 9 },
			{ com = constants.AnimComRealX; param = 4 },
			{ dur = 80; num = 10 },
			{ com = constants.AnimComJump, param = 16},
		}
	},
	{ 
		-- Создание
		name = "die";
		frames = 
		{
			{ com = constants.AnimComStop; },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 78 },
			{  },
			{  },
			{  },
			{  },
			{  },
			{  },
			{  },
			{  },
			{  },
			{  },
			{  },
			{  },
			{  },
			{ dur = 100; num = 18 },
			{ com = constants.AnimComRealH; param = 77 },	
			{ dur = 100; num = 19 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ dur = 0; num = 20; com = constants.AnimComCreateParticles; txt = "pblood_gravity"; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ dur = 0; num = 20; com = constants.AnimComCreateParticles; txt = "pblood_gravity2" },
			{ param = function( obj )
				local pos = obj:aabb().p
				Game.AddCombo( 1 )
				Info.RevealEnemy( "BTARD" )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "triptard", { 18, 35 } ) 
				Game.CreateScoreItems( 1, pos.x, pos.y )
			end },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ dur = 100; num = 20; com = constants.AnimComCreateEnemy; txt = "btard_corpse"; param = 8 },
			{ dur = 100; num = 21 },
			{ com = constants.AnimComRealH; param = 75 },
			{ dur = 100; num = 22 },
			{ com = constants.AnimComRealH; param = 68 },
			{ dur = 100; num = 23 },
			{ com = constants.AnimComRealH; param = 55 },
			{ dur = 100; num = 24 },
			{ com = constants.AnimComPushInt; param = -128 },
			{ com = constants.AnimComPushInt; param = -77 },
			{ com = constants.AnimComEnvSound; },
			{ com = constants.AnimComEnvSound; param = 1 },
			{ com = constants.AnimComRealH; param = 30 },
			{ dur = 100; num = 25, com = constants.AnimComCreateObject; txt = "dust-land" },
			{ com = constants.AnimComRealH; param = 25 },
			{ dur = 100; num = 26 },
			{ com = constants.AnimComPushInt; param = -128 },
			{ com = constants.AnimComPushInt; param = -64 },
			{ com = constants.AnimComRealH; param = 21 },
			{ dur = 5000; num = 27 },
			{ com = constants.AnimComPushInt; param = 400; num = 27 },
			{ com = constants.AnimComPushInt; param = 300; num = 27 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 51; num = 27 }
		}
	},
	{ 
		-- Создание
		name = "jump";
		frames = 
		{
			{ com = constants.AnimComJump; param = function( obj )
				if mapvar.tmp[obj] and mapvar.tmp[obj].landed then
					return 0
				else
					return 6
				end
			end },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComSetVelY; param = -1000 },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComJump; param = 6 }	
		}
	},
}
