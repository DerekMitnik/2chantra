offscreen_distance = 640
offscreen_behavior = constants.offscreenDestroy

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 3;
phys_max_y_vel = 6;
phys_jump_vel = 20;
phys_walk_acc = 3;
mp_count = 1;

-- �������� �������

texture = "penguin_omsk_runner";

z = -0.001;

faction_id = 1;
faction_hates = { -1, -2 };

drops_shadow = 1;

gravity_x = 0;
gravity_y = 0.15;

local acc = 500

animations = WithLabels
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComRealW; param = 37 },
			{ com = constants.AnimComRealH; param = 66 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ param = function( obj )
				local enemy = GetNearestEnemy( obj )
				if not enemy then return end
				if obj:aabb().p.x > enemy:aabb().p.x then
					SetObjSpriteMirrored( obj, true )
				end
			end },
			{ com = constants.AnimComSetRelativeAccX; param = acc },
			{ com = constants.AnimComSetAnim; txt = "move" }	
		}
	},
	{ 
		-- �����
		name = "pain";
		frames = 
		{
			{ param = function(this) Game.FlashObject( this, {1,0,0,0} ) end },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity_small"; param = 2 },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		-- Ƹ����
		name = "move";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealH; param = 58 },
			{ com = constants.AnimComRealX; param = 3 },
			{ com = constants.AnimComRealY; param = 0 },
			{ num = 0; dur = 100 },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 1 },
			{ num = 1; dur = 100 },
			{ com = constants.AnimComRealY; param = 2 },
			{ num = 2; dur = 100 },
			{ com = constants.AnimComRealY; param = 1 },
			{ num = 3; dur = 100 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealX; param = 3 },
			{ num = 4; dur = 100 },
			{ com = constants.AnimComRealY; param = -1 },
			{ num = 5; dur = 100 },
			{ com = constants.AnimComRealY; param = -2 },
			{ com = constants.AnimComRealX; param = 5 },
			{ num = 6; dur = 100 },
			{ com = constants.AnimComRealY; param = -1 },
			{ com = constants.AnimComRealX; param = 3 },
			{ num = 7; dur = 100 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "edge";
		frames =
		{
			{ com = constants.AnimComSetVelY; param = -3000 },
			{ com = constants.AnimComRemoveRecoveryState },
			{ com = constants.AnimComSetAnim; txt = "fly" },
		}
	},
	{ 
		-- Ƹ����
		name = "fly";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealH; param = 57 },
			{ num = 11; dur = 100 },
			{ num = 12; dur = 100 },
			{ num = 13; dur = 100 },
			{ num = 14; dur = 100 },
			{ num = 15; dur = 100 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "land";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		-- �����
		name = "die";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = "ouch2.ogg" },
			{ param = function( obj )
				local pos = obj:aabb().p
				Game.AddCombo( 1 )
				Info.RevealEnemy( "OMICH_RUNNER" )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "omich_flyer", { 19, 33 } ) 
				Game.CreateScoreItems( 2, pos.x, pos.y )
				obj:solid_to( constants.physSprite + constants.physEnemy )
				obj:sprite_z( -0.4 )
				obj:drops_shadow( false )
			end },
			{ com = constants.AnimComStop },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-omsk-safe" },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0; },
			{ com = constants.AnimComSetAnim; txt = "die_noscore" }
		}
	},	
	{ 
		-- ������
		name = "die_noscore";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 20 },
			{ com = constants.AnimComRealX; param = 20 },
			{ com = constants.AnimComRealH; param = 36 },
			{ com = constants.AnimComRealY; param = -6 },
			{ dur = 50; num = 20 },
			{ com = constants.AnimComRealH; param = 31 },
			{ dur = 50; num = 21 },
			{ com = constants.AnimComRealH; param = 26 },
			{ dur = 50; num = 22 },
			{ com = constants.AnimComRealH; param = 20 },
			{ dur = 50; num = 23 },
			{ com = constants.AnimComRealH; param = 16 },
			{ dur = 50; num = 24 },
			{ com = constants.AnimComRealH; param = 17 },
			{ label = "KEEP_CORPSE" },
			{ dur = 5000; num = 25 },
			{ com = constants.AnimComPushInt; param = 400; num = 25 },
			{ com = constants.AnimComPushInt; param = 300; num = 25 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = "KEEP_CORPSE"; num = 25 },
			{}
		}
	},
	{
		name = "touch";
		frames =
		{
			{ param = function( obj )
				local toucher = GetObjectUserdata( ObjectPopInt( obj ) )
				Game.touch_push( obj, toucher )
				DamageObject( toucher, 20 )
			end },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{
		name = "stuck";
		frames =
		{
			{ com = constants.AnimComMirror },
			{ com = constants.AnimComSetRelativeAccX; param = acc },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{
		name = "final_count";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComInitW; param = 37 },
			{ com = constants.AnimComInitH; param = 66 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ dur = 1; com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetShadow; param = 0 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetTrajectory; param = constants.pttLine },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComClearTarget },
			{ num = 14; dur = 100 },
			{ num = 15; dur = 100 },
			{ num = 16; dur = 100 },
			{ num = 17; dur = 100 },
			{ num = 18; dur = 100 },
			{ com = constants.AnimComJump; param = 9 }
		}
	}
}
