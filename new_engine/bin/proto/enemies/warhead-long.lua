--forbidden
name = "warhead-long";

physic = 1;

reload_time = 500;
bullets_per_shot = 0;
push_force = 5;

-- �������� ����
bullet_damage = 40;
bullet_vel = 0;

-- �������� ������� ����
texture = "warhead3";

z = -0.002;

--bounce = 0.75;

animations = 
{
	{
		name = "fly";
		frames =
		{
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -400 },
			{ com = constants.AnimComSetGravity; num = 2 }, 
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 100 },
			{ com = constants.AnimComRealH; param = 508 },
			{ com = constants.AnimComSetMaxVelX; param = 0 },
			{ com = constants.AnimComSetMaxVelY; param = 10000 },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{
		-- ����, ������� �� ��������� ����
		name = "diagdown";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "straight"; num = 2 }
		}
	},
	{
		-- ����, ������� �� ��������� �����
		name = "diagup";
		frames = 
		{

			{ com = constants.AnimComSetAnim; txt = "straight"; num = 2 }
		}
	}
}
