physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 2;
phys_max_y_vel = 8;
phys_jump_vel = 20;
phys_walk_acc = 3;
mp_count = 1;

gravity_x = 0;
gravity_y = 0.4;

texture = "hw_tiles";

z = -0.001;

trajectory_type = constants.pttGlobalSine;
trajectory_param1 = 2;
trajectory_param2 = 0.05;

faction_id = 1;
faction_hates = { -1, -2, 3 };

mass = -1;

drops_shadow = 1;

animations = 
{
	{ 
		name = "init";
		frames = 
		{
			{ com = constants.AnimComSetHealth; param = 250 },
			{ com = constants.AnimComRealW; param = 35 },
			{ com = constants.AnimComRealH; param = 138 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetShadow; param = 1 },
			{ com = constants.AnimComSetZ; param = -1 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetRelativeAccX; param = 0 },
			{ com = constants.AnimComSetTrajectory; param = constants.pttLine },
			{ com = constants.AnimComSetBulletCollidable; param = 1 },
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = 400},
			{ com = constants.AnimComSetGravity; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "target" }
		}
	},
	{ 
		name = "target";
		frames = 
		{
			{ dur = 150; com = constants.AnimComWaitForTarget; param = 30000; txt = "idle"; num = 54 },
			{ com = constants.AnimComLoop; }
		}
	},
	{ 
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComJumpIfIntEquals; param = Game.damage_types.super_laser; txt = "laser" },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		name = "laser";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComPushInt; param = 9000; },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetTrajectory; param = constants.pttLine },
			{ com = constants.AnimComRealW; param = 35 },
			{ com = constants.AnimComRealH; param = 138 },
			{ num = 54; dur = 150; },
			{ num = 55; dur = 150; },
			{ num = 56; dur = 150; },
			{ num = 55; dur = 150; },
			{ com = constants.AnimComJump; param = function()
				if difficulty > 1.5 then
					return 12
				else
					return 8
				end
			end
			},
			{ num = 54; dur = 150; },
			{ num = 55; dur = 150; },
			{ num = 56; dur = 150; },
			{ num = 55; dur = 150; },
			{ com = constants.AnimComFaceTarget; },
			{ com = constants.AnimComSetRelativeAccX; param = 300 },
			{ com = constants.AnimComSetVelY; param = -18000 },
			{ com = constants.AnimComSetTrajectory; param = constants.pttGlobalSine },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		name = "land";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = "weapons/grenade-explosion.ogg" },
			{ com = constants.AnimComPushInt; param = -128; },
			{ com = constants.AnimComPushInt; param = -26; },
			{ com = constants.AnimComCreateObject; txt = "dust-land"; },
			{ com = constants.AnimComStop },
			{ com = constants.AnimComSetAnim; txt = "target" }
		}
	},
	{ 
		name = "move";
		frames = 
		{
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComRealW; param = 35 },
			{ com = constants.AnimComRealH; param = 138 },
			{ num = 54; dur = 150; },
			{ num = 55; dur = 150; },
			{ num = 56; dur = 150; },
			{ num = 55; dur = 150; },
			{ com = constants.AnimComLoop }
		}
	},
	{ 
		name = "die";
		frames =
		{
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetShadow; param = 0 },
			{ com = constants.AnimComSetZ; param = -400 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetRelativeAccX; param = 0 },
			{ com = constants.AnimComSetTrajectory; param = constants.pttLine },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComCreateParticles; txt = "pdust" },
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = 30},
			{ com = constants.AnimComCreateParticles; txt = "pdust" },
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = -30},
			{ com = constants.AnimComCreateParticles; txt = "pdust" },
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = 60},
			{ com = constants.AnimComCreateParticles; txt = "pdust" },
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = -60},
			{ com = constants.AnimComCreateParticles; txt = "pdust" },
			{ param = function(obj)
				local pos = obj:aabb().p
				Game.AddCombo(1)
				Info.RevealEnemy( "DEVIL_COLUMN" )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "hw_devil_column", { 18, 70 } ) 
				Game.CreateScoreItems( 6, pos.x, pos.y )
			end },
			{ com = constants.AnimComSetAnim; txt = "die_noscore" }
		}
	},
	--
	{ 
		name = "die_noscore";
		frames = 
		{
			{ dur = 100; num = 53 },
			{ com = constants.AnimComLoop; }
		}
	},
	{ 
		name = "wait";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 35 },
			{ com = constants.AnimComRealH; param = 138 },
			{ com = constants.AnimComSetHealth; param = 0 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetShadow; param = 0 },
			{ com = constants.AnimComSetZ; param = -900 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetRelativeAccX; param = 0 },
			{ com = constants.AnimComSetTrajectory; param = constants.pttLine },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComSetGravity; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "die_noscore" }
		}
	},
	--
	{
		name = "touch";
		frames =
		{
			{ com = constants.AnimComJumpIfPlayerId; param = 3 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComRecover },
			{ dur = 0 },
			{ com = constants.AnimComDealDamage; param = 30 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComRecover }
		}
	},
	{
		name = "stuck";
		frames =
		{
			{ com = constants.AnimComMirror },
			{ com = constants.AnimComSetRelativeAccX; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "target" }
		}
	},
	{
		name = "target_dead";
		frames = 
		{
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetRelativeAccX; param = 0 },
			{ com = constants.AnimComSetTrajectory; param = constants.pttLine },
			{ com = constants.AnimComSetAnim; txt = "target" }
		}
	},
	{
		name = "final_count";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComInitW; param = 35 },
			{ com = constants.AnimComInitH; param = 138 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ dur = 1; com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetShadow; param = 0 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetTrajectory; param = constants.pttLine },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComClearTarget },
			{ num = 54; dur = 150; },
			{ num = 55; dur = 150; },
			{ num = 56; dur = 150; },
			{ num = 55; dur = 150; },
			{ com = constants.AnimComJump; param = 9 }
		}
	}
}
