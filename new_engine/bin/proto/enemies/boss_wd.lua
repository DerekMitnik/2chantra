--forbidden
physic = 1;
phys_solid = 0;
phys_ghostlike = 1;
phys_bullet_collidable = 1;
phys_max_x_vel = 15;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone
facing = constants.facingMoonwalking

mass = -1;
faction_id = 1;

drops_shadow = 0;

gravity_x = 0;
gravity_y = 0;

--texture = 0;

z = -1.05;

faction_hates = { -1, -2, 3 }

mp_count = 12;

local phase = 1

--local wd_health = 50;
local wd_angle = -120;
local wd_shoot_left = false;
local wd_1 = 0;
local wd_1_5 = 0;
local wd_2 = 0;
local wd_3 = 0;
local wd_4 = 0;
local wd_5 = 0;
local wd_6 = 0;
local wd_7 = 0;
local wdface = 0;
local wdrhand = 0;
local wdlhand = 0;
local wdrplatform = 0;
local wdlplatform = 0;
local wdrarea = 0;
local wdlarea = 0;
local wdy = 0;
local wd_state = 1;

local HIDE = 1
local ACTION_1 = 2
local MOVEMENT = 3
local ACTION_2 = 4
local MAX_HEALTH = 3000

local local_threads = {}

local wings_active = true

local function wd_move(wd, x, y)
	SetObjPos( wd, x, y )
	if wings_active then
		SetObjPos(wdrplatform,wd:aabb().p.x - 290, wd:aabb().p.y + 230+60)
		SetObjPos(wdlplatform,wd:aabb().p.x + 280, wd:aabb().p.y + 230+60)
		SetObjPos(wdrarea,wd:aabb().p.x - 290, wd:aabb().p.y + 230+120)
		SetObjPos(wdlarea,wd:aabb().p.x + 280, wd:aabb().p.y + 230+120)
	end
end

local function wd_set(wd, dont_change_anim)
	SetObjAnim(wd_1,"loop",true)
	SetObjAnim(wd_1_5,"loop",true)
	SetObjAnim(wd_2,"loop",true)
	SetObjAnim(wd_3,"loop",true)
	SetObjAnim(wd_4,"loop",true)
	SetObjAnim(wd_5,"loop",true)
	SetObjAnim(wd_6,"loop",true)
	SetObjAnim(wd_7,"loop",true)
	SetObjAnim(wdface,"loop",false)
	SetObjInvisible(wdrhand,false)
	SetObjInvisible(wdlhand,false)
	SetEnvironmentStats( wdrarea, {
		on_enter = function( ud, x, y, material, this )
		end,
		on_leave = function( ud, x, y, material, new_material, this )
		end,
		on_stay = function( ud, x, y, material, this )
			if ud:type() == 1 then
				if not wd:sprite_mirrored() then
					SetObjAnim(wdrhand, "down", false)
				else
					SetObjAnim(wdlhand, "down", false)
				end
			end
		end,
	} )
	SetEnvironmentStats( wdlarea, {
		on_enter = function( ud, x, y, material, this )
		end,
		on_leave = function( ud, x, y, material, new_material, this )
		end,
		on_stay = function( ud, x, y, material, this )
			if ud:type() == 1 then
				if not wd:sprite_mirrored() then
					SetObjAnim(wdlhand, "down", false)
				else
					SetObjAnim(wdrhand, "down", false)
				end
			end
		end,
	} )
	SetObjPos(wdrplatform,wd:aabb().p.x - 290, wd:aabb().p.y + 230+60)
	SetObjPos(wdlplatform,wd:aabb().p.x + 280, wd:aabb().p.y + 230+60)
	SetObjPos(wdrarea,wd:aabb().p.x - 290, wd:aabb().p.y + 230+100)
	SetObjPos(wdlarea,wd:aabb().p.x + 280, wd:aabb().p.y + 230+100)
end

local function wd_unset(wd)
	SetObjAnim(wd_1,"loop2",true)
	SetObjAnim(wd_1_5,"loop2",true)
	SetObjAnim(wd_2,"loop2",true)
	SetObjAnim(wd_3,"loop2",true)
	SetObjAnim(wd_4,"loop2",true)
	SetObjAnim(wd_5,"loop2",true)
	SetObjAnim(wd_6,"loop2",true)
	SetObjAnim(wd_7,"loop2",true)
	SetObjAnim(wdface,"loop2",false)
	SetObjInvisible(wdrhand,true)
	SetObjInvisible(wdlhand,true)
	SetEnvironmentStats( wdlarea, {
		on_enter = function( ud, x, y, material, this )
		end,
		on_leave = function( ud, x, y, material, new_material, this )
		end,
		on_stay = function( ud, x, y, material, this )
		end,
	} )
	SetEnvironmentStats( wdrarea, {
		on_enter = function( ud, x, y, material, this )
		end,
		on_leave = function( ud, x, y, material, new_material, this )
		end,
		on_stay = function( ud, x, y, material, this )
		end,
	} )
	SetObjPos(wdrplatform,wd:aabb().p.x - 290, wd:aabb().p.y + 1230+60)
	SetObjPos(wdlplatform,wd:aabb().p.x + 280, wd:aabb().p.y + 1230+60)
	SetObjPos(wdrarea,wd:aabb().p.x - 290, wd:aabb().p.y + 1230+100)
	SetObjPos(wdlarea,wd:aabb().p.x + 280, wd:aabb().p.y + 1230+100)
end

local function wd_move_thread(wd, direction)
	local step_size = 50
	local step_delay_dur = 400
	local step_y = 10
	local step_dur = 800

	if phase == 2 then
		step_dur = 600
		local enemy
		if direction == 1 then
			enemy = CreateEnemy("omich_bomber",500,-300);
			enemy = GetObjectUserdata(enemy)
			enemy:ghostlike(true)
		else
			enemy = CreateEnemy("omich_bomber",-500,-300);
			enemy = GetObjectUserdata(enemy)
			enemy:ghostlike(true)
		end
	end

	local dir = direction
	local done = false
	local pos = wd:aabb().p
	local start_y = pos.y
	local last_step = pos.x
	local next_step = pos.x + direction * step_size
	local step_delay = 0
	local step_progress = 0
	local dt = 0
	local last_time = Loader.time
	SetObjAnim(wdface,"loop2",false)
	if dir < 1 then
		SetObjSpriteMirrored( wd, true )
	else
		SetObjSpriteMirrored( wd, false )
	end
	local thread = NewMapThread( function()
		while not done do
			dt = Loader.time - last_time
			last_time = Loader.time
			if step_delay > 0 then
				step_delay = step_delay - dt
				pos.y = math.floor( start_y + step_y/2 * math.sin(math.pi * step_delay / 2*step_delay_dur) + 0.5 )
				wd_move( wd, pos.x, pos.y )
			else
				step_progress = step_progress + dt
				if step_progress >= step_dur then
					last_step = next_step
					pos.x = next_step
					pos.y = start_y
					wd_move( wd, pos.x, pos.y )
					if pos.x == -5 then
						done = true
						SetObjAnim(wdface,"loop",false)
						SetObjAnim( wd, "idle", false )
					end
					step_delay = step_delay_dur
					step_progress = 0
					next_step = pos.x + dir * step_size
					if math.abs( next_step ) > 300 then
						dir = -dir
						next_step = pos.x + dir * step_size
						if dir < 1 then
							SetObjSpriteMirrored( wd, true )
						else
							SetObjSpriteMirrored( wd, false )
						end
					end
				else
					pos.x = math.floor( last_step + dir*step_size*math.sin( math.pi/2*step_progress/step_dur ) + 0.5 )
					pos.y = math.floor( start_y + step_y*math.sin( math.pi*step_progress/step_dur ) + 0.5 )
					wd_move( wd, pos.x, pos.y )
				end
			end
	
			Wait( 1 )	
		end
	end )
	table.insert( local_threads, thread ) 
	Resume( thread )
end

function boss_hurt( obj )
	local damage_type = ObjectPopInt( obj )
	local damage = ObjectPopInt( obj )
	if health_lock then
		return
	end
	local health = obj:health( obj:health() - damage )
	if health > 0 then
		Game.FlashObject( wdface, {0.1,0.1,0.1,1}, nil, true )
	end
	WidgetSetSize( boss_bar, math.max( 1, 243 * health / MAX_HEALTH ), 11 )
	if health <= 1500 and phase == 1 then
		phase = 2
		WidgetSetSpriteColor( boss_bar, { 0.8, 0.2, 0.2, 0.75 } )
	elseif health <= 0 then
		mapvar.tmp.cutscene = true
		phase = 4
	end
end

animations = 
{
	{
		name = "start";
		frames =
		{
			{ param = function(wd)
				boss_bar_bkg = CreateWidget( constants.wt_Picture, "", nil, CONFIG.scr_width / 2 - 137, CONFIG.scr_height - 3 * 21, 1, 1 )
				WidgetSetSprite( boss_bar_bkg, "pear15boss_bar", "background" )
				WidgetSetSpriteColor( boss_bar_bkg, { 0.8, 0.4, 0.4, 0.5 } )
				boss_bar = CreateWidget( constants.wt_Picture, "", nil, CONFIG.scr_width / 2 - 137 + 24, CONFIG.scr_height - 3 * 21 + 5, 243, 11 )
				WidgetSetSprite( boss_bar, "pear15boss_bar", "bar" )
				WidgetSetSpriteRenderMethod( boss_bar, constants.rsmCrop )
				WidgetSetSpriteColor( boss_bar, { 0.8, 0.4, 0.4, 0.5 } )
				cleanup = function() DestroyWidget(boss_bar) DestroyWidget(boss_bar_bkg) end
				if not Game.cleanupFunctions then
					Game.cleanupFunctions = {}
				end
				Game.cleanupFunctions[ cleanup ] = true
				
				wd_state = 1;
				phase = 1;
				wd_unset(wd)
				SetObjAnim(wd_1,"loop2",true)
				SetObjAnim(wd_1_5,"loop2",true)
				SetObjAnim(wd_2,"loop",true)
				SetObjAnim(wd_3,"loop",true)
				SetObjAnim(wd_4,"loop",true)
				SetObjAnim(wd_5,"loop",true)
				SetObjAnim(wd_6,"loop",true)
				SetObjAnim(wd_7,"loop",true)
				SetObjAnim(wdface,"loop",false)

				local thread = NewMapThread( function()
					local pos = wd:aabb().p
					local change
					while math.abs( pos.y - -250 ) > 5 do
						change = math.max( -5, math.min( -3, ( -250 - pos.y ) * 0.01) )
						pos.y = math.floor(pos.y + change)
						SetObjPos( wd, pos.x, pos.y )
						Wait ( 1 )
					end
					SetObjAnim( wd, "idle", false )
				end )
				table.insert( local_threads, thread )
				Resume( thread )

				thread = NewMapThread( function()
					--MAIN BACKGROUND BOSS THREAD
					if wd:health() <= 0 then
						return
					end
					boss_ended = false
					old_state = 0
					local pos
					local pl_c1
					local pl_c2
					local t = 0

					while not boss_ended do
						t = t + 0.05
						if t > 2 * math.pi then
							t = t - 2 * math.pi
						end

						if wd_state == ACTION_1 or wd_state == ACTION_2 then
							if old_state ~= wd_state then
								t = 0
							end
							pos = wd:aabb().p
							pl_c1 = GetPlayerCharacter( 1 )
							pl_c2 = GetPlayerCharacter( 2 )
							pos.y = math.floor(-230 + 10 * math.cos( t ) + 0.5)
							wd_move( wd, pos.x, pos.y )
						end

						old_state = wd_state	
						Wait(1)
						boss_ended = wd:health() <= 0;
					end
				end )
				table.insert( local_threads, thread )
				Resume( thread )
			end },
			{}
		}
	},  
	{
		name = "moveleft";
		frames =
		{
			{ dur = 1 },
			{ param = function(wd)
				--wd_unset(wd)
				wd_move_thread( wd, -1 )
			end },
			{}
		}
	},  
	{
		name = "moveright";
		frames =
		{
			{ dur = 1 },
			{ param = function(wd)
				--wd_unset(wd)
				wd_move_thread( wd, 1 )
			end },
			{}
		}
	},  
	{
		name = "movedown";
		frames =
		{
			{ param = function(wd)
				wd_unset(wd)
				SetObjAnim(wd_1,"loop2",true)
				SetObjAnim(wd_1_5,"loop2",true)
				SetObjAnim(wd_2,"loop",true)
				SetObjAnim(wd_3,"loop",true)
				SetObjAnim(wd_4,"loop",true)
				SetObjAnim(wd_5,"loop",true)
				SetObjAnim(wd_6,"loop",true)
				SetObjAnim(wd_7,"loop",true)
				SetObjAnim(wdface,"loop",false)
				local enemy = nil
				enemy = CreateEnemy("omich_bomber",-500,-300);
				enemy = GetObjectUserdata(enemy)
				enemy:ghostlike(true)
				enemy = CreateEnemy("omich_bomber",500,-300);
				enemy = GetObjectUserdata(enemy)
				enemy:ghostlike(true)
				enemy = CreateEnemy("omich_bomber",-550,-250);
				enemy = GetObjectUserdata(enemy)
				enemy:ghostlike(true)
				enemy = CreateEnemy("omich_bomber",550,-250);
				enemy = GetObjectUserdata(enemy)
				enemy:ghostlike(true)
				enemy = CreateEnemy("omich_bomber",-600,-200);
				enemy = GetObjectUserdata(enemy)
				enemy:ghostlike(true)
				enemy = CreateEnemy("omich_bomber",600,-200);
				enemy = GetObjectUserdata(enemy)
				enemy:ghostlike(true)

				local thread = NewMapThread( function()
					local pos = wd:aabb().p
					local change
					while math.abs( pos.y - 450 ) > 5 do
						change = math.min( 5, math.max( 3, ( 450 - pos.y ) * 0.01) )
						pos.y = math.floor(pos.y + change)
						SetObjPos( wd, pos.x, pos.y )
						Wait ( 1 )
					end
					Wait( 3000 )
					while math.abs( pos.y - -250 ) > 5 do
						change = math.max( -5, math.min( -3, ( -250 - pos.y ) * 0.01) )
						pos.y = math.floor(pos.y + change)
						SetObjPos( wd, pos.x, pos.y )
						Wait ( 1 )
					end
					SetObjAnim( wd, "idle", false )
				end )
				table.insert( local_threads, thread )
				Resume( thread )
			end },
			{ com = constants.AnimComSetAnim; txt = "none" },
		}
	},  
	{ 
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 150 },
			{ com = constants.AnimComRealH; param = 200 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetHealth; param = MAX_HEALTH },
			--body
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComPushInt; param = 380 },
			{ com = constants.AnimComMPSet; param = 1 },
			
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComPushInt; param = 280 },
			{ com = constants.AnimComMPSet; param = 2 },
			
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComPushInt; param = 280 },
			{ com = constants.AnimComMPSet; param = 11 },
			
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComPushInt; param = 180 },
			{ com = constants.AnimComMPSet; param = 3 },
			
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComPushInt; param = 80 },
			{ com = constants.AnimComMPSet; param = 4 },
			
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComMPSet; param = 5 },
			
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComPushInt; param = -120 },
			{ com = constants.AnimComMPSet; param = 6 },
			
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComPushInt; param = -220 },
			{ com = constants.AnimComMPSet; param = 7 },
			--face
			{ com = constants.AnimComPushInt; param = -220+50 },
			{ com = constants.AnimComPushInt; param = -70+50 },
			{ com = constants.AnimComMPSet; param = 8 },
			--rhand
			{ com = constants.AnimComPushInt; param = -270 },
			{ com = constants.AnimComPushInt; param = 230 },
			{ com = constants.AnimComMPSet; param = 9 },
			--lhand
			{ com = constants.AnimComPushInt; param = 180 },
			{ com = constants.AnimComPushInt; param = 230 },
			{ com = constants.AnimComMPSet; param = 10 },
			{ dur = 1; num = 0 },
			{ param = function(wd)
				wd_shoot_left = false
				wd_angle = -165
				wd_1 = CreateEffect("boss_wd_1",0,0,wd:id(),1)
				wd_1_5 = CreateEffect("boss_wd_1_5",0,0,wd:id(),11)
				wd_2 = CreateEffect("boss_wd_2",0,0,wd:id(),2)
				wd_3 = CreateEffect("boss_wd_3",0,0,wd:id(),3)
				wd_4 = CreateEffect("boss_wd_4",0,0,wd:id(),4)
				wd_5 = CreateEffect("boss_wd_5",0,0,wd:id(),5)
				wd_6 = CreateEffect("boss_wd_6",0,0,wd:id(),6)
				wd_7 = CreateEffect("boss_wd_7",0,0,wd:id(),7)
				wdface = GetObjectUserdata( CreateEffect("boss_wdface",0,0,wd:id(),8) )
				wdrhand = GetObjectUserdata( CreateEffect("boss_wdrhand",0,0,wd:id(),9) )
				wdlhand = GetObjectUserdata( CreateEffect("boss_wdlhand",0,0,wd:id(),10) )
				wdlplatform = GetObjectUserdata( CreateSprite( 'phys-empty-cl', wd:aabb().p.x + 180-0, wd:aabb().p.y + 230+20 ) )
				local obj3 = CreateSprite( 'phys-empty-cl', wd:aabb().p.x + 180+200, wd:aabb().p.y + 230+100 )
				local wdlplatformgroup = GroupObjects( wdlplatform:id(), obj3 )
				wdlarea = CreateEnvironment( 'default_environment', wd:aabb().p.x + 180-0, wd:aabb().p.y + 230+100 )
				local obj31 = CreateEnvironment( 'default_environment', wd:aabb().p.x + 180+200, wd:aabb().p.y + 230+300 )
				local wdlplatform_area = GroupObjects( wdlarea, obj31 )
				wdrplatform = GetObjectUserdata( CreateSprite( 'phys-empty-cl', wd:aabb().p.x - 290+100, wd:aabb().p.y + 230+20 ) )
				local obj5 = CreateSprite( 'phys-empty-cl', wd:aabb().p.x - 290-100, wd:aabb().p.y + 230+100 )
				local wdrplatformgroup = GroupObjects( wdrplatform:id(), obj5 )
				wdrarea = CreateEnvironment( 'default_environment', wd:aabb().p.x - 290+100, wd:aabb().p.y + 230+100 )
				mapvar.tmp[wdrhand] = wdrplatform
				mapvar.tmp[wdlhand] = wdlplatform
				local obj51 = CreateEnvironment( 'default_environment', wd:aabb().p.x - 290-100, wd:aabb().p.y + 230+300 )
				local wdrplatform_area = GroupObjects( wdrarea, obj51 )
				wdy = wd:aabb().p.y
				wd_set(wd)
				column1 = CreateEnemy("hw_devil_column",-410+3, 93+74+5, 1, -0.900000);
				column2 = CreateEnemy("hw_devil_column",386-17+14, 93+74+5, 1, -0.900000);
				SetObjAnim(column1,"wait",true);
				SetObjAnim(column2,"wait",true);
				SetObjPos(wd,wd:aabb().p.x,wd:aabb().p.y + 800)
				wd_unset(wd)
			  end },
			{ com = constants.AnimComSetAnim; txt = "none" },
		}
	},
	{ 
		name = "none";
		frames = 
		{
			{ dur = 50 },
			{ com = constants.AnimComLoop; },
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ dur = 50; },
			{ param = function(wd)
				SetObjSpriteMirrored( wd, true )
				if wd_state == 1 then
				      wd_state = ACTION_1
				      SetObjAnim(wd, "action",true)
				elseif wd_state == 2 then
				      wd_state = MOVEMENT
				      SetObjAnim(wd, "move",true)
				elseif wd_state == 3 then
				      wd_state = ACTION_2
				      SetObjAnim(wd, "action",true)
				elseif wd_state == 4 then
				      wd_state = HIDE
				      SetObjAnim(wd, "movedown",true)
				else
				      Log("ERROR!")
				      SetObjAnim(wd, "die",true)
				end
			  end },
			--{ com = constants.AnimComStop },
			{ com = constants.AnimComLoop; },
		}
	},
	{ 
		name = "move";
		frames = 
		{
			{ com = constants.AnimComSetAnim; param = 128; txt = "moveleft" },
			{ com = constants.AnimComSetAnim; txt = "moveright" },
		}
	},
	{ 
		name = "lazor_shoot";
		frames = 
		{
			{ dur = 50; },
			{ param = function(wd)
			  local atack = false
			  if phase == 2 then
				if wd_shoot_left == true then
					if wd_angle < -70 then
						wd_angle = -150
						wd_shoot_left = false
						atack = true
					end
					if atack == false then
						CreateRay("wd-lazor", wd:id(), 180-wd_angle-3, wd:aabb().p.x + 35-5, wd:aabb().p.y + 7 )
						CreateRay("wd-lazor", wd:id(), wd_angle+3, wd:aabb().p.x - 35-6, wd:aabb().p.y + 7 )
						SetObjAnim(wd,"lazor_shoot", true)
					end
					wd_angle = wd_angle - 1;
				  else
					if wd_angle > -100 then
						wd_angle = -30
						wd_shoot_left = true
						atack = true
					end
					if atack == false then
						CreateRay("wd-lazor", wd:id(), 180-wd_angle-3, wd:aabb().p.x + 35-5, wd:aabb().p.y + 7 )
						CreateRay("wd-lazor", wd:id(), wd_angle+3, wd:aabb().p.x - 35-6, wd:aabb().p.y + 7 )
						SetObjAnim(wd,"lazor_shoot", true)
					end
					wd_angle = wd_angle + 1;
				  end
			  else
				  if wd_shoot_left == true then
					if wd_angle < -120 then
						wd_angle = -150
						wd_shoot_left = false
						atack = true
					end
					if atack == false then
						CreateRay("wd-lazor", wd:id(), wd_angle-3, wd:aabb().p.x + 35-5, wd:aabb().p.y + 7 )
						CreateRay("wd-lazor", wd:id(), wd_angle+3, wd:aabb().p.x - 35-6, wd:aabb().p.y + 7 )
						SetObjAnim(wd,"lazor_shoot", true)
					end
					wd_angle = wd_angle - 1;
				  else
					if wd_angle > -60 then
						wd_angle = -30
						wd_shoot_left = true
						atack = true
					end
					if atack == false then
						CreateRay("wd-lazor", wd:id(), wd_angle-3, wd:aabb().p.x + 35-5, wd:aabb().p.y + 7 )
						CreateRay("wd-lazor", wd:id(), wd_angle+3, wd:aabb().p.x - 35-6, wd:aabb().p.y + 7 )
						SetObjAnim(wd,"lazor_shoot", true)
					end
					wd_angle = wd_angle + 1;
				  end
			  end
			  end },
			{ com = constants.AnimComSetAnim; txt = "idle" },
		}
	},
	{ 
		name = "pain";
		frames = 
		{
			{ param = boss_hurt },
			{ param = function( obj )
				local pos = obj:last_hit_from()
				CreateParticleSystem( "pblood_gravity_small", pos.x, pos.y )
			end },
			{ com = constants.AnimComRecover }
		}
	},
	{
		name = "action";
		frames = 
		{
			{ param = function(wd)
				wd_set(wd)
				SetObjAnim(wdrhand, "down", true)
				SetObjAnim(wdlhand, "down", true)
			end },
			{ dur = 250; },
			{ param = function(wd)
				SetObjAnim(column1, "init", true)
				SetDynObjVel(column1, 0, -7)
				SetObjAnim(column2, "init", true)
				SetDynObjVel(column2, 0, -7)
			end },
			{ com = constants.AnimComSetAnim; txt = "lazor_shoot" },
		}
	},
	{
		name = "move2";
		frames = 
		{
			--{ dur = 100; num = 0; com = constants.AnimComSetAccX; param = -1000 },
			{ dur = 100; num = 0; com = constants.AnimComRealH; param = 87 },
			{ dur = 100; num = 1; com = constants.AnimComRealH; param = 88 },
			--{ dur = 100; num = 2; com = constants.AnimComRealH; param = 86 },
			--{ dur = 100; num = 3; com = constants.AnimComRealH; param = 88 },
			--{ dur = 100; num = 4; com = constants.AnimComRealH; param = 88 },
			{ com = constants.AnimComLoop; }
		}
	},
	{
		name = "touch";
		frames = 
		{
			{ param = function( obj )
				local toucher = GetObjectUserdata( ObjectPopInt( obj ) )
				DamageObject( toucher, 30 )
			end },
			{ com = constants.AnimComSetTouchable; param = 1; },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		name = "die";
		frames = 
		{
			{ param = function(wd)
				for k, v in pairs( local_threads ) do
					StopThread( v )
				end
				---wd_unset(wd)
				SetEnvironmentStats( wdlarea, {
					on_enter = function( ud, x, y, material, this )
					end,
					on_leave = function( ud, x, y, material, new_material, this )
					end,
					on_stay = function( ud, x, y, material, this )
					end,
				} )
				SetEnvironmentStats( wdrarea, {
					on_enter = function( ud, x, y, material, this )
					end,
					on_leave = function( ud, x, y, material, new_material, this )
					end,
					on_stay = function( ud, x, y, material, this )
					end,
				} )
				SetObjSolidToByte( wdrplatform, 0 )
				SetObjSolidToByte( wdlplatform, 0 )
				SetObjPos( wdrplatform, -9000, -9000 )
				SetObjPos( wdlplatform, -9000, -9000 )
				SetObjInvisible(wdrhand,true)
				SetObjInvisible(wdlhand,true)
				SetObjAnim(wd_1,"loop2",true)
				SetObjAnim(wd_1_5,"loop2",true)
				SetObjAnim(wd_2,"loop",true)
				SetObjAnim(wd_3,"loop",true)
				SetObjAnim(wd_4,"loop",true)
				SetObjAnim(wd_5,"loop",true)
				SetObjAnim(wd_6,"loop",true)
				SetObjAnim(wd_7,"loop",true)
				SetObjAnim(wdface,"death",true)
				DamageObject(column1,9000)
				DamageObject(column2,9000)
				StopBackMusic()
			end },
			{ com = constants.AnimComSetTouchable; param = 0; },
			{ com = constants.AnimComSetBulletCollidable; param = 0; },
			{ dur = 100; com = constants.AnimComSetVelY; param = 3000 },
			{ dur = 100; com = constants.AnimComSetVelX; param = 0 },
			{ com = constants.AnimComPushInt; param = -250+1000 },
			{ com = constants.AnimComJumpIfYGreater; param = 7 },
			{ com = constants.AnimComJump; param = 2 },

			{ param = function(wd)
				if Loader.level.boss_done then
					Loader.level.boss_done()
				end
			end },
		}
	},
}



