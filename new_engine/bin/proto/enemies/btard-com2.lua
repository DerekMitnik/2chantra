parent = "enemies/btard-com"

texture = "btard-com"

local function turn_away( this )
	local plane = this:suspected_plane()
	if not plane then return end
	local aabb = this:aabb()
	plane = plane:aabb()
	if (not this:sprite_mirrored() and (aabb.p.x + aabb.W >= plane.p.x + plane.W)) or
	   (    this:sprite_mirrored() and (aabb.p.x - aabb.W <= plane.p.x - plane.W)) then
		SetObjAnim( this, "stuck", false )
	end
end

animations = WithLabels
{
	{ 
		name = "move";
		frames = 
		{
			{ com = constants.AnimComSetRelativeAccX; param = 500 },
			{ com = constants.AnimComRealX; param = 11 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = "ATTACK" },
			{ com = constants.AnimComEnvSound; param = 1},
			{ param = turn_away },
			{ dur = 80; num = 5 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = "ATTACK" },
			{ com = constants.AnimComRealX; param = 4 },
			{ param = turn_away },
			{ dur = 80; num = 6 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = "ATTACK" },
			{ com = constants.AnimComRealX; param = 6 },
			{ param = turn_away },
			{ dur = 80; num = 7 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = "ATTACK" },
			{ com = constants.AnimComRealX; param = 18 },
			{ com = constants.AnimComEnvSound; },
			{ param = turn_away },
			{ dur = 80; num = 8 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = "ATTACK" },
			{ com = constants.AnimComRealX; param = 12 },
			{ param = turn_away },
			{ dur = 80; num = 9 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = "ATTACK" },
			{ com = constants.AnimComRealX; param = 4 },
			{ param = turn_away },
			{ dur = 80; num = 10 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = "ATTACK" },
			{ com = constants.AnimComLoop },
			{ dur = 0; label = "ATTACK" },
			{ com = constants.AnimComSetAnim; txt = "attack" }	
		}
	},

}

