parent = "enemies/btard2"

texture = "btard-com"

animations = WithLabels
{
	{ 
		name = "die";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 78 },
			{ com = constants.AnimComStop },

			{ com = constants.AnimComSetBulletCollidable; param = 0 },

			{ param = function( obj )
				local pos = obj:aabb().p
				Game.AddCombo( 1 )
				Info.RevealEnemy( "BTARD_COM" )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "btard-com", { 37, 40 } ) 
				Game.CreateScoreItems( 2, pos.x, pos.y )
			end },
			{ dur = 100; num = 18 },
			{ com = constants.AnimComRealH; param = 77 },	
			{ dur = 100; num = 19 },
			
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ dur = 0; num = 20; com = constants.AnimComCreateParticles; txt = "pblood_gravity"; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ dur = 0; num = 20; com = constants.AnimComCreateParticles; txt = "pblood_gravity2" },
			{ dur = 100; num = 21 },
			{ com = constants.AnimComRealH; param = 75 },
			{ dur = 100; num = 22 },

			{ com = constants.AnimComRealH; param = 68 },
			{ dur = 100; num = 23 },
			{ com = constants.AnimComRealH; param = 55 },
			{ dur = 100; num = 24 },
			{ com = constants.AnimComPushInt; param = -128 },

			{ com = constants.AnimComPushInt; param = -77 },
			{ com = constants.AnimComEnvSound; },
			{ com = constants.AnimComEnvSound; param = 1 },
			{ com = constants.AnimComRealH; param = 30 },
			{ dur = 100; num = 25, com = constants.AnimComCreateObject; txt = "dust-land" },
			{ com = constants.AnimComRealH; param = 25 },

			{ dur = 100; num = 26 },
			{ com = constants.AnimComPushInt; param = -128 },
			{ com = constants.AnimComPushInt; param = -64 },
			{ com = constants.AnimComRealH; param = 21, label = "KEEP-CORPSE" },
			{ dur = 5000; num = 27 },

			{ com = constants.AnimComPushInt; param = 320 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ dur = 1; num = 27; com = constants.AnimComJumpIfCloseToCamera; param = "KEEP-CORPSE" }
	
		}

	},
	{
		name = "final_count";
		frames = 
		{
			{ com = constants.AnimComInitW; param = 36 },
			{ com = constants.AnimComInitH; param = 75 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComControlledOverlayColor },
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetShadow; param = 0 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetTrajectory; param = constants.pttLine },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComRealX; param = 11 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ dur = 80; num = 5 },
			{ com = constants.AnimComRealX; param = 4 },
			{ dur = 80; num = 6 },
			{ com = constants.AnimComRealX; param = 6 },
			{ dur = 80; num = 7 },
			{ com = constants.AnimComRealX; param = 18 },
			{ dur = 80; num = 8 },
			{ com = constants.AnimComRealX; param = 12 },
			{ dur = 80; num = 9 },
			{ com = constants.AnimComRealX; param = 4 },
			{ dur = 80; num = 10 },
			{ com = constants.AnimComJump, param = 15},
		}
	},
	{ 
		-- Создание
		name = "jump";
		frames = 
		{
			{ com = constants.AnimComJump; param = function( obj )
				if mapvar.tmp[obj] and mapvar.tmp[obj].landed then
					return 0
				else
					return 6
				end
			end },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 51 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComSetVelY; param = -2000 },
			{ dur = 100; num = 28 },
			{ com = constants.AnimComJump; param = 6 }
		}
	},
}

