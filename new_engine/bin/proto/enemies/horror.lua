bounce = 0.5;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 300;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
mp_count = 1;
offscreen_distance = 940
offscreen_behavior = constants.offscreenDestroy

texture = "horror";

z = -0.001;

local max_speed = 300;
local speed = 30;
health = 60;

mass = 0.3;

faction_id = 1;
faction_hates = { -1, -2 };


trajectory_type = constants.pttGlobalSine;
trajectory_param1 = 0.5;
trajectory_param2 = 0.05;


animations = 
{
	{ 
		-- Создание
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 110 },
			{ com = constants.AnimComRealH; param = 73 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetHealth; param = health },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ dur = 100; num = 0; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 100; num = 1; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 100; num = 2; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 100; num = 3; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 100; num = 4; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 100; num = 3; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 100; num = 2; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 100; num = 1; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ param = function(this) Game.FlashObject( this ) end },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity_small" },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		name = "move";
		frames = 
		{
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComPushInt; param = max_speed },
			{ dur = 100; num = 0; com = constants.AnimComAdjustVelToTarget; param = speed},
			{ com = constants.AnimComPushInt; param = max_speed },
			{ dur = 100; num = 1; com = constants.AnimComAdjustVelToTarget; param = speed},
			{ com = constants.AnimComPushInt; param = max_speed },
			{ dur = 100; num = 2; com = constants.AnimComAdjustVelToTarget; param = speed},
			{ com = constants.AnimComPushInt; param = max_speed },
			{ dur = 100; num = 3; com = constants.AnimComAdjustVelToTarget; param = speed},
			{ com = constants.AnimComPushInt; param = max_speed },
			{ dur = 100; num = 4; com = constants.AnimComAdjustVelToTarget; param = speed},
			{ com = constants.AnimComPushInt; param = max_speed },
			{ dur = 100; num = 3; com = constants.AnimComAdjustVelToTarget; param = speed},
			{ com = constants.AnimComPushInt; param = max_speed },
			{ dur = 100; num = 2; com = constants.AnimComAdjustVelToTarget; param = speed},
			{ com = constants.AnimComPushInt; param = max_speed },
			{ dur = 100; num = 1; com = constants.AnimComAdjustVelToTarget; param = speed},
			{ com = constants.AnimComLoop }
		}
	},
	{ 
		name = "die";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = "slowpoke-death.ogg" },
			{ param = function( obj )
				local pos = obj:aabb().p
				Game.AddCombo( 1 )
				Info.RevealEnemy( "HORROR" )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "horror", { 40, 38 } ) 
				Game.CreateScoreItems( 2, pos.x, pos.y )
				CreateParticleSystem( "pblood_gravity", pos.x, pos.y )
				CreateParticleSystem( "pblood_gravity2", pos.x, pos.y )
				CreateParticleSystem( "pmeat", pos.x, pos.y )
			end },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComStop },
			{ com = constants.AnimComRealW; param = 147 },
			{ com = constants.AnimComInitH; param = 132 },
			{ num = 5; dur = 100 },
			{ num = 6; dur = 100 },
			{ num = 7; dur = 100 },
			{ num = 8; dur = 100 },
			{ num = 9; dur = 100 },
			{ num = 10; dur = 100 },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		name = "touch";
		frames =
		{
			{ param = function( obj )
				local toucher = GetObjectUserdata( ObjectPopInt( obj ) )
				Game.touch_push( obj, toucher )
				DamageObject( toucher, 10 )
			end },
			{ com = constants.AnimComRecover }
		}
	},
	{
		name = "target_dead";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{
		name = "final_count";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComInitW; param = 110 },
			{ com = constants.AnimComInitH; param = 73 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ dur = 1; com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetShadow; param = 0 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetTrajectory; param = constants.pttLine },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComClearTarget },
			{ num = 0; dur = 100 },
			{ num = 1; dur = 100 },
			{ num = 2; dur = 100 },
			{ num = 3; dur = 100 },
			{ num = 4; dur = 100 },
			{ num = 3; dur = 100 },
			{ num = 2; dur = 100 },
			{ num = 1; dur = 100 },
			{ com = constants.AnimComJump; param = 9 }
		}
	}
}



