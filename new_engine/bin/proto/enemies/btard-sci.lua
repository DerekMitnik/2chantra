--forbidden

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 5;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 1;

FunctionName = "CreateEnemy";

-- �������� �������

texture = "btard-sci";

drops_shadow = 1;

z = -0.002;

gravity_x = 0;
gravity_y = .8;

overlay = {0};
ocolor = {{1, 0.8, 1, 1}}
local diff = (difficulty-1)/5+1;
local difcom = constants.AnimComNone;
if (difficulty >=1) then difcom = constants.AnimComJump; end

faction_id = 1
faction_hates = {-1, -2}

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 30 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComSetHealth; param = 60*difficulty },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		-- ��������
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ dur = 200; num = 0; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 200; num = 1; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 200; num = 2; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 200; num = 3; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 200; num = 4; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 200; num = 5; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		-- ��������
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood-wound"; param = 2 },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		-- ��������
		name = "move";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 30 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ dur = 100; num = 6; com = constants.AnimComMoveToTargetX; param = 250/diff },
			{ dur = 100; num = 7; com = constants.AnimComMoveToTargetX; param = 250/diff },
			{ dur = 100; num = 8; com = constants.AnimComMoveToTargetX; param = 250/diff },
			{ dur = 100; num = 9; com = constants.AnimComMoveToTargetX; param = 500/diff },
			{ dur = 100; num = 10; com = constants.AnimComMoveToTargetX; param = 500/diff },
			{ dur = 100; num = 11; com = constants.AnimComMoveToTargetX; param = 500/diff },
			{ com = constants.AnimComLoop },
		}
	},
	{ 
		-- ��������
		name = "jump";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 30 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		-- ��������
		name = "die";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 30 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 78 },
			{ com = constants.AnimComMapVarAdd; param = difficulty*40; txt = "score" },
			{ com = constants.AnimComMapVarAdd; param = 1; txt = "kills" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateItem; txt = "ammo" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ dur = 1; num = 0; com = constants.AnimComCreateParticles; txt = "pblood"; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ dur = 1; num = 0; com = constants.AnimComCreateParticles; txt = "pmeat"; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 1; num = 0; com = constants.AnimComCreateParticles; txt = "pmeat"; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -10 },
			{ dur = 1; num = 0; com = constants.AnimComCreateParticles; txt = "pmeat"; param = 2 },
			{ com = constants.AnimComPushInt; param = -128 },
			{ com = constants.AnimComPushInt; param = -77 },
			{ com = constants.AnimComEnvSound; },
			{ com = constants.AnimComEnvSound; param = 1 },
		}
	},
	{ 
		-- ��������
		name = "land";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 30 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComEnvSound; },
			--{ com = constants.AnimComEnvSound; param = 1 },
			--{ com = constants.AnimComPushInt; param = -128 },
			--{ com = constants.AnimComPushInt; param = -56 },
			--{ com = constants.AnimComCreateObject; txt = "dust-land" },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
}



