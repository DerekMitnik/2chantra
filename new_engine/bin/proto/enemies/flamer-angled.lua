--$(DESCRIPTION).�����
--$(DESCRIPTION).
--$(DESCRIPTION).������ � ��� ���� ������ �����: ���� � ��������, ����� �������, �������� ����� ���������� � ����������������� ��������


physic = 1;
ghost_to = 255;

phys_max_x_vel = 0;
phys_max_y_vel = 0;

faction_id = 4
faction_hates = { -1, -2 }

if Editor then
	texture = "fireshot"
end

mass = -1;

animations =
{
	{
		name = "init";
		frames =
		{
			{ com = constants.AnimComInitWH; param = 16 },
			{ dur = 1 },
			{ param = function( obj )
				local t = {}
				SetObjProcessor( obj, function( this )
					if not ObjectOnScreen( this, 320 ) then
						return
					end
					local t = mapvar.tmp[ this ]
					if not t then
						t = {}
						t.flames = ObjectPopInt(this)
						t.delay = ObjectPopInt(this)
						t.length = ObjectPopInt(this)
						t.angle = ObjectPopInt(this)
						t.last_time = 0
						t.pos = this:aabb().p
						mapvar.tmp[this] = t
					end
					if Loader.time - t.last_time > t.delay then
						t.last_time = Loader.time + t.flames * 50
						Resume( NewMapThread( function()
							local o
							for i=1, t.flames do
								o = GetObjectUserdata( CreateBullet("flame-angled", t.pos.x, t.pos.y, this:id(), false, t.angle, 0 )  )
								mapvar.tmp[o] = { t = Loader.time, lifetime = t.length*10 }
								--[[SetObjProcessor( o, function( inception )
									if Loader.time - mapvar.tmp[inception].t >= mapvar.tmp[inception].lifetime then
										SetObjAnim( inception, "miss", false )
									end
								end )]]
								Wait(50)
							end
						end ))
					end
				end )
			end },
		}
	}
}
