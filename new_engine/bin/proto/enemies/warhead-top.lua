name = "warhead-top";

physic = 1;
phys_solid = 1;
phys_bullet_collidable = 1;
phys_max_x_vel = 20;
phys_max_y_vel = 10;
phys_jump_vel = 20;
phys_walk_acc = 10;
phys_one_sided = 0;
mp_count = 1;

FunctionName = "CreateEnemy";

-- �������� �������

texture = "warhead";

z = -0.1;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 63 },
			{ com = constants.AnimComRealH; param = 50 },
			{ com = constants.AnimComSetHealth; param = 90000 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -4000 },
			{ com = constants.AnimComSetGravity },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 420 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 6 },
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ com = constants.AnimComPushInt; param = 160 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComSetGravity },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 420 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 10 },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{ 
		-- ��������
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComRecover }
		}
	},
	{
		name = "jump";
		frames = 
		{
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 420 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 6 },
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ com = constants.AnimComPushInt; param = 160 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComSetGravity },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 420 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 10 },
			{ com = constants.AnimComDestroyObject }
		}
	}

}



