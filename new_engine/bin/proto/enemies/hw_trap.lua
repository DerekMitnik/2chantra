texture = "hw_tiles";
facing = constants.facingFixed
touch_detection = constants.tdtTopAndSides

z = -0.008;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_ghostlike = 1;
phys_bullet_collidable = 0;

mass = -1;

ghost_to = 16;

phys_max_x_vel = 5;
phys_max_y_vel = 5;

ghost_to = 18;


animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComInitH; param = 14 },
			{ com = constants.AnimComRealH; param = 13 },
			{ com = constants.AnimComRealW; param = 65 },
			{ com = constants.AnimComInitW; param = 48 },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComInitW; param = 54 },
			{ com = constants.AnimComRealX; param = 17 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "loop" }
		}
	},
	{
		name = "loop";
		frames =
		{
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ dur = 50, num = 24 },
		}
	},
	{
		name = "touch";
		frames =
		{
			{ com = constants.AnimComDealDamage; param = 10 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -8 },
			{ com = constants.AnimComPushObject },
			{ com = constants.AnimComPop },
			{ dur = 25; num = 26 },
			{ com = constants.AnimComRealY; param = 17 },
			{ dur = 1000; num = 25 },
			{ com = constants.AnimComRealY; param = 0 },
			{ dur = 25; num = 26 },
			{ com = constants.AnimComSetAnim; txt = "loop" }
		}
	}
}
