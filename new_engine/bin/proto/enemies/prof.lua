--forbidden
physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 5;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 1;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

drops_shadow = 1;

gravity_x = 0;
gravity_y = 0.8;


faction_id = -2;
faction_hates = { 1, 2, 3 };

-- �������� �������

texture = "prof";

z = -0.002;

animations = 
{
	{ 
		-- ���������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 28 },
			{ com = constants.AnimComRealH; param = 71 },
			{ com = constants.AnimComSetHealth; param = 100*difficulty },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		-- ����������
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ dur = 7500; num = 0 },
			{ dur = 100, num = 12 },
			{ dur = 100, num = 13 },
			{ dur = 100, num = 14 },
			{ dur = 100, num = 15 },
			{ dur = 100, num = 16 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "catch_breath";
		frames =
		{
			{ com = constants.AnimComRealY; param = -4 },
			{ dur = 100, num = 1, com = constants.AnimComRealX; param = -2 },
			{ dur = 100, num = 2, com = constants.AnimComRealX; param = -3 },
			{ dur = 100, num = 3, com = constants.AnimComRealX; param = -3 },
			{ dur = 100, num = 2, com = constants.AnimComRealX; param = -3 },
			{ dur = 100, num = 1, com = constants.AnimComRealX; param = -2 },
			{ dur = 100, num = 2, com = constants.AnimComRealX; param = -3 },
			{ dur = 100, num = 3, com = constants.AnimComRealX; param = -3 },
			{ dur = 100, num = 2, com = constants.AnimComRealX; param = -3 },
			{ dur = 100, num = 1, com = constants.AnimComRealX; param = -2 },
			{ dur = 100, num = 2, com = constants.AnimComRealX; param = -3 },
			{ dur = 100, num = 3, com = constants.AnimComRealX; param = -3 },
			{ dur = 100, num = 2, com = constants.AnimComRealX; param = -3 },
			{ com = constants.AnimComSetAnim; txt = "init" },
		}
	},
	{
		name = "talk";
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ dur = 100, num = 17 },
			{ dur = 100, num = 18 },
			{ dur = 100, num = 19 },
			{ dur = 100, num = 17 },
			{ dur = 100, num = 18 },
			{ dur = 100, num = 17 },
			{ dur = 100, num = 18 },
			{ dur = 100, num = 19 },
			{ dur = 100, num = 17 },
			{ dur = 100, num = 18 },
			{ dur = 100, num = 17 },
			{ dur = 100, num = 18 },
			{ dur = 100, num = 19 },
			{ dur = 100, num = 17 },
			{ dur = 100, num = 18 },
			{ com = constants.AnimComSetAnim; txt = "init" },
		}
	},
	{
		name = "talk_fgsfds";
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ dur = 100, num = 20 },
			{ dur = 100, num = 21 },
			{ dur = 100, num = 22 },
			{ dur = 100, num = 23 },
			{ dur = 100, num = 24 },
			{ dur = 100, num = 23 },
			{ dur = 100, num = 22 },
			{ dur = 100, num = 23 },
			{ dur = 100, num = 24 },
			{ dur = 100, num = 23 },
			{ dur = 100, num = 22 },
			{ dur = 100, num = 23 },
			{ dur = 100, num = 24 },
			{ dur = 100, num = 22 },
			{ dur = 100, num = 21 },
			{ dur = 100, num = 20 },
			{ com = constants.AnimComSetAnim; txt = "init" },
		}
	},
	{
		name = "turn_left";
		frames =
		{
			{ dur = 1; num = 0; com = constants.AnimComSetVelX; param = 1 },
			{ dur = 0; com = constants.AnimComStop },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		-- ��������
		name = "land";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	}
}



