parent = "enemies/btard-com"

animations =
{
	{ 
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ param = function( obj )
				local enemy = GetNearestEnemy( obj )
				if not enemy then return end
				if obj:aabb().p.x > enemy:aabb().p.x then
					SetObjSpriteMirrored( obj, true )
				end
				mapvar.tmp[obj] = { landed = false }
			end },
			{ com = constants.AnimComSetAnim; txt = "move" }	
		}
	},
	{
		name = "move";
		frames =
		{
			{ com = constants.AnimComSetRelativeAccX; param = 500 },
			{},
			{ dur = 100, param = function(obj)
				if not mapvar.tmp[obj] then mapvar.tmp[obj] = { landed = true } end
				local frame = mapvar.tmp[obj].frame or 4
				frame = frame + 1
				if frame > 10 then
					frame = 5
				end
				mapvar.tmp[obj].frame = frame
				return 0, nil, frame
			end },
			{ com = constants.AnimComJump; param = function( obj )
				if ObjectOnScreen( obj, 64 ) then
					return 3
				else
					return 1
				end
			end },
			{ com = constants.AnimComCallAnim; txt = "grenade" },
			{ com = constants.AnimComCallAnim; txt = "grenade" },
			{ com = constants.AnimComCallAnim; txt = "grenade" },
			{ com = constants.AnimComMirror },
			{ com = constants.AnimComSetRelativeAccX; param = 500 },
			{},
			{ dur = 100, param = function(obj)
				local frame = mapvar.tmp[obj].frame or 4
				frame = frame + 1
				if frame > 10 then
					frame = 5
				end
				mapvar.tmp[obj].frame = frame
				return 0, nil, frame
			end },
			{ com = constants.AnimComJump; param = function( obj )
				if not ObjectOnScreen( obj, -64 ) then
					return 11
				else
					return 9
				end
			end },
			{ com = constants.AnimComDestroyObject }
		}	
	},
	{
		name = "grenade";
		frames =
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 34 },
			{ com = constants.AnimComRealH; param = 81 },
			{ dur = 100; num = 11 },
			{ com = constants.AnimComRealW; param = 68 },
			{ com = constants.AnimComRealH; param = 78 },
			{ dur = 100; num = 12 },
			{ com = constants.AnimComRealH; param = 77 },
			{ dur = 100; num = 13 },
			{ dur = 100; num = 14; param = function(obj)
				--50
				local target = GetNearestEnemy( obj )
				if target then
					target = target:aabb().p
					local pos = obj:aabb().p
					local dx = 50
					if obj:sprite_mirrored() then
						dx = -dx
					end
					local granada = GetObjectUserdata( CreateBullet( "btard-grenade-small", pos.x + dx, pos.y, obj:id(), obj:sprite_mirrored(), 0, 0 ) )
					mapvar.tmp[granada] = { target = target, start = { x = pos.x + dx, y = pos.y }, progress = 0 }
				end
			end },
			{ com = constants.AnimComRealH; param = 68 },
			{ dur = 100; num = 15 },
			{ com = constants.AnimComRealH; param = 78 },
			{ dur = 100; num = 16 }, 
			{ com = constants.AnimComRecover }
		}
	},
}
