offscreen_behavior = constants.offscreenNone

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 0;
phys_max_x_vel = 5;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 1;

drops_shadow = 0;

faction_id = 1;
faction_hates = { -1, -2 };

gravity_x = 0;
gravity_y = 0;

-- �������� �������

texture = "fireshot";

z = 0.5;

local speed = 200;

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComInitWH; param = 22; },
			{ dur = 1 },
			{ param = function( obj )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
				local pos = obj:aabb().p
				local flare = GetObjectUserdata( CreateSprite( "circle", pos.x, pos.y ) )
				SetObjRectangle( flare, 64, 64 )
				SetObjSpriteRenderMethod( flare, constants.rsmStretch )
				SetObjSpriteColor( flare, { 1, 160/255, 90/255, 0.5 } )
				SetObjPos( flare, pos.x, pos.y, 1.0 )
				mapvar.tmp[obj] = flare
				CreateParticleSystem( "ppermaspark", obj, 0, 0, 10 )
			end },
			{ com = constants.AnimComSetHealth; param = 1; },
			{ com = constants.AnimComSetAnim; txt = "fire"; }
		}
	},
	{
		name = "fire";
		frames =
		{
			{ dur = 1000 },
			{ param = function(obj)
				local target = GetNearestEnemy( obj )
				local pos = obj:aabb().p
				if target then
					target = target:aabb().p
					CreateBullet( "fireshot", pos.x, pos.y, obj:id(), false, math.atan2( target.y - pos.y, target.x - pos.x ) * 180/math.pi, 0 )
				end
			end },
			{ dur = 1000 },
			{ param = function(obj)
				local target = GetNearestEnemy( obj )
				local pos = obj:aabb().p
				if target then
					target = target:aabb().p
					CreateBullet( "fireshot", pos.x, pos.y, obj:id(), false, math.atan2( target.y - pos.y, target.x - pos.x ) * 180/math.pi, 0 )
				end
			end },
			{ dur = 1000 },
			{ param = function(obj)
				local target = GetNearestEnemy( obj )
				local pos = obj:aabb().p
				if target then
					target = target:aabb().p
					CreateBullet( "fireshot", pos.x, pos.y, obj:id(), false, math.atan2( target.y - pos.y, target.x - pos.x ) * 180/math.pi, 0 )
				end
			end },
			{ param = function( obj ) SetObjDead( mapvar.tmp[obj] ) end },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		name = "permament";
		frames =
		{
			{ dur = 1000 },
			{ param = function(obj)
				local target = GetNearestEnemy( obj )
				local pos = obj:aabb().p
				if target then
					target = target:aabb().p
					CreateBullet( "fireshot", pos.x, pos.y, obj:id(), false, math.atan2( target.y - pos.y, target.x - pos.x ) * 180/math.pi, 0 )
				end
			end },
			{ com = constants.AnimComLoop }
		}
	}
}

