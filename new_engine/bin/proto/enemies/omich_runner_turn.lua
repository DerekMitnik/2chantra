parent = "enemies/omich_runner_jump";

animations =
{
	{
		name = "edge";
		frames =
		{
			{ com = constants.AnimComMirror },
			{ com = constants.AnimComSetRelativeAccX; param = 500 },
			{ com = constants.AnimComRecover }
		}
	},
}
