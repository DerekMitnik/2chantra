physic = 1;
phys_bullet_collidable = 1;
phys_max_x_vel = 2;
phys_max_y_vel = 50;
mp_count = 1;

gravity_x = 0;
gravity_y = 0.8;
mass = -1;

texture = "robot";

z = -0.001;

faction_id = 1;
faction_hates = { -1, -2 };

drops_shadow = 1;

animations = 
{
	{ 
		-- Создание
		name = "init";
		frames = 
		{
			--{ com = constants.AnimComRealX; param = 0 },
			--{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 30 },
			{ com = constants.AnimComRealH; param = 33 },
			{ com = constants.AnimComSetHealth; param = 60*difficulty },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ dur = 100; num = 4 },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 2 },
			{ com = constants.AnimComSetBulletCollidable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ dur = 100; num = 0; com = constants.AnimComSetAnim; txt = "move" },
		}
	},
	{ 
		-- Создание
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pexplosion_sparks" },
			{ dur = 100; num = 2; com = constants.AnimComRealH; param = 33 },
			{ dur = 100; num = 3 },
			{ dur = 5000; num = 4 },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 2 },
			{ com = constants.AnimComSetBulletCollidable; param = 1 },
			{ com = constants.AnimComRecover; txt = "idle" }
		}
	},
	{ 
		-- Создание
		name = "move";
		frames = 
		{
			{ com = constants.AnimComSetBulletCollidable; param = 1 },
			{ com = constants.AnimComRealH; param = 33 },
			{ dur = 100; num = 0; com = constants.AnimComSetRelativeAccX; param = 800  },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComJumpIfTargetClose; param = 10 },		
			{ com = constants.AnimComRealH; param = 34 },
			{ dur = 100; num = 1; },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComJumpIfTargetClose; param = 10 },
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ com = constants.AnimComSetAnim; txt = "attack" }
		}
	},
	{
		name = "attack";
		frames =
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComRealH; param = 34 },
			{ dur = 100; num = 5},
			{ dur = 100; num = 6},
			{ dur = 100; num = 8},
			{ dur = 100; num = 9},
			{ com = constants.AnimComRealH; param = 37 },
			{ dur = 100; num = 10},

			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealH; param = 42 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ dur = 100; num = 11},
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ dur = 100; num = 12},
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ dur = 100; num = 13},
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ dur = 100; num = 14},
			
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetRelativeAccX; param = 800  },
			{ com = constants.AnimComSetAnim; txt = "move" }			
		}
	},
	{
		name = "touch";
		frames = 
		{
			{ com = constants.AnimComJumpIfPlayerId; param = 2 },
			{ com = constants.AnimComRecover },
			{ dur = 0 },
			{ com = constants.AnimComDealDamage; param = 15 },
			{ com = constants.AnimComRecover },
		}
	},
	{ 
		name = "die";
		frames = 
		{
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion_big" },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComRealW; param = 30 },
			{ com = constants.AnimComRealH; param = 33 },
			{ com = constants.AnimComMapVarAdd; param = 400; txt = "score" },
			{ com = constants.AnimComMapVarAdd; param = 1; txt = "kills" },
			{ dur = 5000; num = 15 },
			{ com = constants.AnimComPushInt; param = 400 },
			{ com = constants.AnimComPushInt; param = 300 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 6 }	
		}
	},
	{ 
		name = "stuck";
		frames = 
		{
			{ com = constants.AnimComMirror },
			{ com = constants.AnimComSetRelativeAccX; param = 800  },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	}
}



