texture = "2011lab4";

z = -0.1;

physic = 1;
phys_solid = 1;
phys_one_sided = 1;

mass = -1
facing = constants.facingFixed;

animations = 
{
	{ 
		name = "init";
		frames = 
		{
			{ com = constants.AnimComInitW; param = 33 },
			{ com = constants.AnimComInitH; param = 23 },
			{ com = constants.AnimComRealX; param = 13 },
			{ com = constants.AnimComRealY; param = 13 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ num = 21; dur = 1 },
			{ param = function( obj ) 
				local y = ObjectPopInt( obj ) / 1000
				local x = ObjectPopInt( obj ) / 1000
				mapvar.tmp[obj] = { x = x, y = y, obj = {} }
				local t = Loader.time
				local dt
				local ov
				local xd, yd = false, false
				SetObjProcessor( obj, function( this )
					dt = Loader.time - t
					t = Loader.time
					for k, v in pairs( mapvar.tmp[obj].obj ) do
						mapvar.tmp[obj].obj[k] = v - dt
						if v <= 0 and k:object_present() then
							ov = k:own_vel()
							ov.x = ov.x * 0.75
							ov.y = ov.y * 0.75
							xd, yd = math.abs(ov.x) < 1, math.abs(ov.y) < 1
							if xd and yd then
								k:own_vel( { x = 0, y = 0 } )
								mapvar.tmp[obj].obj[k] = nil
							else
								k:own_vel( ov )
							end
						end
					end
				end )
			end },
			{ num = 21; dur = 1 },
		}
	},
	{
		name = "touch";
		frames =
		{
			{ param = function( obj )
				local toucher = GetObjectUserdata( ObjectPopInt( obj ) )
				if not toucher or toucher:aabb().p.y - toucher:vel().y > obj:aabb().p.y then return end
				local x = mapvar.tmp[obj].x
				local y = mapvar.tmp[obj].y
				mapvar.tmp[obj].obj[toucher] = 1000
				toucher:vel( { x = x, y = y } )
				toucher:own_vel( { x = x, y = y } )
			end },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComRecover },
		}
	},
	{
		name = "pain";
		frames =
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComRecover },
		}
	}
}



