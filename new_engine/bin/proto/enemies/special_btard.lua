--forbidden

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 2;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 1;

FunctionName = "CreateEnemy";

-- �������� �������

texture = "btard-sci";

drops_shadow = 1;

ghost_to = constants.physEnemy;

z = -0.002;

gravity_x = 0;
gravity_y = .8;

overlay = {0};
ocolor = {{1, 0.8, 1, 1}}
local diff = (difficulty-1)/5+1;
local difcom = constants.AnimComNone;
if (difficulty >=1) then difcom = constants.AnimComJump; end

faction_id = 1
faction_hates = {-1, -2}

animations = 
{
	{ 
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 30 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComSetHealth; param = 60*difficulty },
			{ com = constants.AnimComSetTouchable; param = 0 }
		}
	},
	{ 
		name = "move_right";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 30 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComSetAccX; param = 5000 },
			{ dur = 100; num = 6 },
			{ dur = 100; num = 7 },
			{ dur = 100; num = 8 },
			{ dur = 100; num = 9 },
			{ dur = 100; num = 10 },
			{ dur = 100; num = 11 },
			{ com = constants.AnimComLoop },
		}
	},
	{ 
		name = "move_left";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 30 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ param = function(obj)
				SetObjSpriteMirrored( obj, true )
			end },
			{ dur = 100; num = 6 },
			{ com = constants.AnimComJump; param = function( obj )
				if not ObjectOnScreen( obj ) then
					return 5
				else
					return 6
				end
			end },
			{ dur = 300; num = 6 },
			{ com = constants.AnimComSetAccX; param = -5000 },
			{ dur = 100; num = 7 },
			{ dur = 100; num = 8 },
			{ dur = 100; num = 9 },
			{ dur = 100; num = 10 },
			{ dur = 100; num = 11 },
			{ dur = 100; num = 6 },
			{ com = constants.AnimComJump; param = function( obj )
				if not ObjectOnScreen( obj ) then
					return 15
				else
					return 8
				end
			end },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		name = "walk";
		frames =
		{
			{ com = constants.AnimComRealX; param = 30 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ dur = 100; num = 7 },
			{ dur = 100; num = 8 },
			{ dur = 100; num = 9 },
			{ dur = 100; num = 10 },
			{ dur = 100; num = 11 },
			{ dur = 100; num = 6 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "walk_with_bottle";
		frames =
		{
			{ com = constants.AnimComRealX; param = 30 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ dur = 100; num = 14 },
			{ dur = 100; num = 15 },
			{ dur = 100; num = 16 },
			{ dur = 100; num = 17 },
			{ dur = 100; num = 18 },
			{ dur = 100; num = 19 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "think";
		frames =
		{
			{ com = constants.AnimComRealX; param = 30 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 4 },
			{ dur = 100; num = 5 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "pain";
		frames =
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComRecover },
		}
	}
}



