offscreen_distance = 640
offscreen_behavior = constants.offscreenDestroy

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 1.5;
phys_max_y_vel = 6;
phys_jump_vel = 20;
phys_walk_acc = 3;
mp_count = 1;

-- �������� �������

texture = "penguin_omsk";

z = -0.001;

trajectory_type = constants.pttGlobalSine;
trajectory_param1 = 6;
trajectory_param2 = 0.05;

faction_id = 1;
faction_hates = { -1, -2 };

drops_shadow = 1;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComRealW; param = 37 },
			{ com = constants.AnimComRealH; param = 66 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ param = function( obj )
				local enemy = GetNearestEnemy( obj )
				if not enemy then return end
				if obj:aabb().p.x > enemy:aabb().p.x then
					SetObjSpriteMirrored( obj, true )
				end
			end },
			{ com = constants.AnimComSetRelativeAccX; param = 300 },
			{ com = constants.AnimComSetAnim; txt = "move" }	
		}
	},
	{ 
		-- �����
		name = "pain";
		frames = 
		{
			{ param = function(this) Game.FlashObject( this, {1,0,0,0} ) end },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity_small"; param = 2 },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		-- Ƹ����
		name = "move";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealH; param = 58 },
			{ num = 14; dur = 100 },
			{ num = 15; dur = 100 },
			{ num = 16; dur = 100 },
			{ num = 17; dur = 100 },
			{ num = 18; dur = 100 },
			{ com = constants.AnimComLoop }
		}
	},
	{ 
		-- �����
		name = "die";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = "ouch2.ogg" },
			{ param = function( obj )
				local pos = obj:aabb().p
				Game.AddCombo( 1 )
				Info.RevealEnemy( "OMICH_FLYER" )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "omich_flyer", { 19, 33 } ) 
				Game.CreateScoreItems( 2, pos.x, pos.y )
				obj:solid_to( constants.physSprite + constants.physEnemy )
				obj:sprite_z( -0.4 )
			end },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0; },
			{ com = constants.AnimComSetAnim; txt = "die_noscore" }
		}
	},	
	{ 
		-- ������
		name = "die_noscore";
		frames = 
		{
			{ com = constants.AnimComSetTrajectory; param = constants.pttLine },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = explosion },
			{ com = constants.AnimComStop },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ dur = 0; num = 3; com = constants.AnimComCreateParticles; txt = "pblood_gravity"; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ dur = 0; num = 3; com = constants.AnimComCreateParticles; txt = "pblood_gravity2" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 8000 },
			{ com = constants.AnimComSetGravity },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 29 },
			{ num = 3; dur = 1; com = constants.AnimComRealH; param = 62 },
			{ num = 3, dur = 1 },
			{ com = constants.AnimComJumpIfOnPlane; param = 23 },
			{ com = constants.AnimComJump; param = 20 },
			{ dur = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity"; param = 2 },
			{ com = constants.AnimComSetShadow; param = 0 },
			{ num = 3; dur = 50; com = constants.AnimComRealH; param = 67 },
			{ num = 4; dur = 50; com = constants.AnimComRealH; param = 67 },
			{ com = constants.AnimComRealX; param = 6 },
			{ com = constants.AnimComRealY; param = -7 },
			{ num = 5; dur = 50; com = constants.AnimComRealH; param = 67 },
			{ com = constants.AnimComRealX; param = 7 },
			{ com = constants.AnimComRealY; param = -9 },
			{ num = 6; dur = 50; com = constants.AnimComRealH; param = 57 },
			{ com = constants.AnimComRealX; param = 9 },
			{ com = constants.AnimComRealY; param = -11 },
			{ num = 7; dur = 50; com = constants.AnimComRealH; param = 57 },
			{ com = constants.AnimComRealX; param = 13 },
			{ com = constants.AnimComRealY; param = -12 },
			{ num = 8; dur = 50; com = constants.AnimComRealH; param = 52 },
			{ com = constants.AnimComRealX; param = 14 },
			{ com = constants.AnimComRealY; param = -20 },
			{ num = 9; dur = 50; com = constants.AnimComRealH; param = 52 },
			{ com = constants.AnimComRealX; param = 16 },
			{ com = constants.AnimComRealY; param = -19 },
			{ num = 10; dur = 50; com = constants.AnimComRealH; param = 52 },
			{ com = constants.AnimComRealX; param = 17 },
			{ com = constants.AnimComRealY; param = -16 },
			{ num = 11; dur = 50; com = constants.AnimComRealH; param = 36 },
			{ com = constants.AnimComRealY; param = -15 },
			{ num = 12; dur = 5000; com = constants.AnimComRealH; param = 32 },
			{ com = constants.AnimComPushInt; param = 400; num = 12 },
			{ com = constants.AnimComPushInt; param = 300; num = 12 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 52; num = 12 }
		}
	},
	{
		name = "touch";
		frames =
		{
			{ com = constants.AnimComJumpIfPlayerId; param = 3 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComRecover },
			{ dur = 0 },
			{ com = constants.AnimComDealDamage; param = 30 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{
		name = "stuck";
		frames =
		{
			{ param = function( obj )
				obj:solid_to( constants.physSprite + constants.physEnemy )
				obj:sprite_z( -0.4 )
			end },
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die_noscore" }
		}
	},
	{
		name = "final_count";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComInitW; param = 37 },
			{ com = constants.AnimComInitH; param = 66 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ dur = 1; com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetShadow; param = 0 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetTrajectory; param = constants.pttLine },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComClearTarget },
			{ num = 14; dur = 100 },
			{ num = 15; dur = 100 },
			{ num = 16; dur = 100 },
			{ num = 17; dur = 100 },
			{ num = 18; dur = 100 },
			{ com = constants.AnimComJump; param = 9 }
		}
	}
}
