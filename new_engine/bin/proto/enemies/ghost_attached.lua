--forbidden
bounce = 0;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 200;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
mp_count = 1;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

FunctionName = "CreateEnemy";

-- �������� �������

drops_shadow = 1;
ghost_to = 252;

z = -0.001;

local speed = 300*((difficulty-1)/3+1);
local health = 60;
if difficulty > 1 then
	health = 80;
elseif difficulty < 1 then
	health = 40;
end

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComSetHealth; param = health },
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 1 },
			{ com = constants.AnimComCreateParticles; param = 3; txt = "pghostsmoke2" },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ dur = 25; num = 0; },
			{ dur = 25; num = 1; },
			{ dur = 25; num = 2; },
			{ dur = 25; num = 3; },
			{ dur = 25; num = 4; },
			{ dur = 25; num = 5; },
			{ dur = 25; num = 6; },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		name = "die";
		frames = 
		{
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		name = "touch";
		frames =
		{
			{ com = constants.AnimComJumpIfPlayerId; param = 3 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" },
			{ dur = 0 },
			{ com = constants.AnimComDealDamage; param = 10*difficulty },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{ 
		-- ��������
		name = "land";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	}
	
}



