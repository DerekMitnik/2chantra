texture = "hw_tiles"

facing = constants.facingFixed
touch_detection = constants.tdtTopAndSides

z = -0.002;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_ghostlike = 1;
phys_bullet_collidable = 0;

mass = -1;

ghost_to = 16;

phys_max_x_vel = 5;
phys_max_y_vel = 5;


animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComInitH; param = 14 },
			{ com = constants.AnimComRealH; param = 13 },
			{ com = constants.AnimComRealW; param = 65 },
			{ com = constants.AnimComInitW; param = 48 },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComInitW; param = 54 },
			{ com = constants.AnimComRealX; param = 17 },
			{ com = constants.AnimComRealY; param = 0 },
			{ dur = 1, num = 19 },
			{ param = function(obj)
				if not mapvar then mapvar = {} end
				if not mapvar.tmp then mapvar.tmp = {} end
				if not mapvar.tmp.spikes then mapvar.tmp.spikes = {} end
				if not mapvar.tmp.spikes.x then mapvar.tmp.spikes.x = {} end
				if not mapvar.tmp.spikes.y then mapvar.tmp.spikes.y = {} end
				if not mapvar.tmp.spikes.obj then mapvar.tmp.spikes.obj = {} end
				if not mapvar.tmp.spikes.n then mapvar.tmp.spikes.n = 0 end
				local n = mapvar.tmp.spikes.n
				n = n + 1
				mapvar.tmp.spikes.x[n] = obj:aabb().p.x
				mapvar.tmp.spikes.y[n] = obj:aabb().p.y
				mapvar.tmp.spikes.obj[n] = obj
				mapvar.tmp.spikes.n = n;
			end },
			{ com = constants.AnimComSetAnim; txt = "loop" }
		}
	},
	{
		name = "loop";
		frames =
		{
			{ dur = 500, num = 19 },
			{ com = constants.AnimComLoop; }
		}
	},
	{
		name = "spike";
		frames =
		{
			--{ com = constants.AnimComDealDamage; param = 10 },
			--{ com = constants.AnimComPushInt; param = 0 },
			--{ com = constants.AnimComPushInt; param = -8 },
			--{ com = constants.AnimComPushObject },
			--{ com = constants.AnimComPop },
			{ dur = 50; num = 26 },
			{ com = constants.AnimComRealY; param = 17 },
			{ dur = 400; num = 25 },
			{ com = constants.AnimComRealY; param = 0 },
			{ dur = 50; num = 26 },
			{ com = constants.AnimComSetAnim; txt = "loop" }
		}
	}
}
