texture = "shelf";
facing = constants.facingFixed

z = -0.8;

physic = 1;
phys_solid = 1;

mass = -1;

phys_one_sided = 1;

phys_max_x_vel = 5;
phys_max_y_vel = 5;

gravity_x = 0;
gravity_y = 0;

faction_id = -28;
ghost_to = constants.physEverything - constants.physSprite - constants.physPlayer

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 79 },
			{ com = constants.AnimComRealH; param = 26 },
			{ param = function( obj )
				if not mapvar.tmp.portraits then
					mapvar.tmp.portraits = {}
				end
				mapvar.tmp.portraits[obj] = true
			end },
			{ dur = 1; num = 0 },
		}
	},
	{
		name = "fall";
		frames =
		{
			{ param = function( obj )
				obj:gravity( { x = 0, y = 0.8 } )
				obj:solid_to( constants.physSprite )
			end },
			{ com = constants.AnimComPlaySound; txt = "wood.ogg" },
			{ com = constants.AnimComRealW; param = 101 },
			{ com = constants.AnimComRealH; param = 44 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComJump; param = function( obj )
				return iff( obj:suspected_plane(), 5, 4 )
			end },
			{ com = constants.AnimComRealW; param = 118 },
			{ com = constants.AnimComRealH; param = 10 },
			{ param = function( obj )
				obj:gravity( { x = 0, y = 0 } )
				obj:solid( false )
			end },
			{ dur = 100; num = 2 }
		}
	}
}
