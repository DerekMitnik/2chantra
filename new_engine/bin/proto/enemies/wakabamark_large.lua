parent = "enemies/wakabamark_small"

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComInitW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ param = function( obj )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
				Resume( NewMapThread( function()
					Wait( 5000 )
					local vis = true
					if not obj:object_present() then return end
					for i=1,50 do
						if not obj:object_present() then return end
						vis = not vis
						SetObjInvisible( obj, vis )
						Wait( 100 )
					end
					if not obj:object_present() then return end
					SetObjDead( obj )
				end ))
			end },
			{ com = constants.AnimComPushInt; param = 4000 },
			{ com = constants.AnimComPushInt; param = 6000 },
			{ com = constants.AnimComPushInt; param = -130 },
			{ com = constants.AnimComPushInt; param = -80 },
			{ com = constants.AnimComRandomAngledSpeed; param = 1 },
			{ dur = 1 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "fly"}
		}
	},
	{ 
		name = "touch";
		frames = 
		{

			{ com = constants.AnimComPlaySound; txt = "item-score.ogg" },
			{ param = function( obj )
				local pos = obj:aabb().p
				Game.GainScore( GetObjectUserdata( ObjectPopInt(obj) ), 1000, pos.x, pos.y )
			end },
			{ com = constants.AnimComDestroyObject }

		}
	}
}
