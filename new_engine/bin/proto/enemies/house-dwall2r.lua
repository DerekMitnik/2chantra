parent = "enemies/house-dwall1r"

animations =
{
	{
		name = "idle";
		frames =
		{
			{ dur = 1; num = 12 }
		}
	},
	{
		name = "75%";
		frames =
		{
			{ dur = 1; num = 12 } 
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 12 }                                      
		}
	},
	{
		name = "25%";
		frames =
		{
			{ dur = 1; num = 399 }                                      
		}
	}
}
