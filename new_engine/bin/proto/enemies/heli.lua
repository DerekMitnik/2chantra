name = "heli";

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 20;
phys_max_y_vel = 30;
phys_jump_vel = 20;
phys_walk_acc = 10;
phys_one_sided = 0;
mp_count = 1;


local diff = (difficulty-1)/5+1;

FunctionName = "CreateEnemy";

-- �������� �������

texture = "heli";

z = -0.1;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 99 },
			{ com = constants.AnimComRealH; param = 81 },
			{ com = constants.AnimComSetHealth; param = 1100*diff },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComSetGravity },
			{ com = constants.AnimComSetAnim; txt = "ready" }	
		}
	},
	{ 
		name = "ready";
		frames = 
		{
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 320 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 6},
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ com = constants.AnimComSetAccX; param = 200},
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetAccX; param = -200 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetAccX; param = 0 },
			{ com = constants.AnimComSetVelX; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "right" }	
		}
	},
	{
		name = "right";
		frames =
		{
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 192 },
			{ com = constants.AnimComJumpRandom; param = 16 },
			{ com = constants.AnimComPushInt; param = 192 },
			{ com = constants.AnimComJumpRandom; param = 17 },
			{ com = constants.AnimComPushInt; param = 192 },
			{ com = constants.AnimComJumpRandom; param = 18 },
			{ com = constants.AnimComPushInt; param = 192 },
			{ com = constants.AnimComJumpRandom; param = 19 },
			{ com = constants.AnimComPushInt; param = 192 },
			{ com = constants.AnimComJumpRandom; param = 20 },
			{ com = constants.AnimComSetAnim; txt = "right_btards" },
			{ com = constants.AnimComSetAnim; txt = "right_bombingrun" },
			{ com = constants.AnimComSetAnim; txt = "right_toleft" },
			{ com = constants.AnimComSetAnim; txt = "right_gun" },
			{ com = constants.AnimComSetAnim; txt = "right_toleft_gun" },
			{ com = constants.AnimComSetAnim; txt = "right_wave" }
		}
	},
	{
		name = "right_wave";
		frames = 
		{
			{ com = constants.AnimComSetDamageMult; param = math.floor(difficulty*(-2/3)+4/3*1000) },
			{ com = constants.AnimComSetAccX; param = -200},
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetDamageMult; param = 1000 },
			{ dur = 100; num = 12 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComSetAccX; param = 200 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb-waves"; param = 0 },
			{ dur = 100; num = 14 },
			{ dur = 100; num = 12 },
			{ com = constants.AnimComSetDamageMult; param = math.floor(difficulty*(-2/3)+4/3*1000) },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetAccX; param = 0 },
			{ com = constants.AnimComSetVelX; param = 0 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetAnim; txt = "left" }
		}
		
	},
	{
		name = "right_gun";
		frames =
		{

			{ com = constants.AnimComSetDamageMult; param = math.floor(difficulty*(-2/3)+4/3*1000) },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetAnim; txt = "right" }
		}
	},
	{
		name = "right_toleft_gun";
		frames =
		{

			{ com = constants.AnimComSetDamageMult; param = math.floor(difficulty*(-2/3)+4/3*1000) },
			{ com = constants.AnimComSetAccX; param = -200},
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetAccX; param = 200 },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ com = constants.AnimComSetAccX; param = 0 },
			{ com = constants.AnimComSetVelX; param = 0 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetAnim; txt = "left" }
		}
	},
	{
		name = "right_btards";
		frames =
		{

			{ com = constants.AnimComSetDamageMult; param = math.floor(difficulty*(-2/3)+4/3*1000) },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemy; txt = "btard-heli" },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemy; txt = "btard-heli" },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemy; txt = "btard-heli" },
			{ com = constants.AnimComSetAnim; txt = "right_toleft" }
		}
	},
	{ 
		name = "right_toleft";
		frames = 
		{

			{ com = constants.AnimComSetDamageMult; param = math.floor(difficulty*(-2/3)+4/3*1000) },
			{ com = constants.AnimComSetAccX; param = -200},
			{ dur = 100; num = 12 },
			{ dur = 100; num = 13 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ dur = 100; num = 14 },
			{ dur = 100; num = 13 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComSetAccX; param = 200 },
			{ dur = 100; num = 14 },
			{ dur = 100; num = 13 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComSetAccX; param = 0 },
			{ com = constants.AnimComSetVelX; param = 0 },
			{ dur = 100; num = 14 },
			{ dur = 100; num = 12 },
			{ com = constants.AnimComSetAnim; txt = "left" }
		}
	},
	{ 
		name = "right_bombingrun";
		frames = 
		{

			{ com = constants.AnimComSetDamageMult; param = 1000 },
			{ com = constants.AnimComSetAccX; param = -200},
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 12 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ com = constants.AnimComSetAccX; param = 200 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ com = constants.AnimComSetAccX; param = 0 },
			{ com = constants.AnimComSetVelX; param = 0 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 12 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ com = constants.AnimComSetDamageMult; param = math.floor(difficulty*(-2/3)+4/3*1000) },
			{ com = constants.AnimComSetAnim; txt = "left" }
		}
	},
	{
		name = "left";
		frames =
		{
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 192 },
			{ com = constants.AnimComJumpRandom; param = 16 },
			{ com = constants.AnimComPushInt; param = 192 },
			{ com = constants.AnimComJumpRandom; param = 17 },
			{ com = constants.AnimComPushInt; param = 192 },
			{ com = constants.AnimComJumpRandom; param = 18 },
			{ com = constants.AnimComPushInt; param = 192 },
			{ com = constants.AnimComJumpRandom; param = 19 },
			{ com = constants.AnimComPushInt; param = 192 },
			{ com = constants.AnimComJumpRandom; param = 20 },
			{ com = constants.AnimComSetAnim; txt = "left_btards" },
			{ com = constants.AnimComSetAnim; txt = "left_bombingrun" },
			{ com = constants.AnimComSetAnim; txt = "left_toright" },
			{ com = constants.AnimComSetAnim; txt = "left_gun" },
			{ com = constants.AnimComSetAnim; txt = "left_toright_gun" },
			{ com = constants.AnimComSetAnim; txt = "left_wave" }
		}
	},
	{ 
		name = "left_toright";
		frames = 
		{

			{ com = constants.AnimComSetDamageMult; param = math.floor(difficulty*(-2/3)+4/3*1000) },
			{ com = constants.AnimComSetAccX; param = 200},
			{ dur = 100; num = 12 },
			{ com = constants.AnimComSetDamageMult; param = 1000 },
			{ dur = 100; num = 13 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ dur = 100; num = 14 },
			{ dur = 100; num = 13 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComSetAccX; param = -200 },
			{ dur = 100; num = 14 },
			{ dur = 100; num = 13 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComSetAccX; param = 0 },
			{ com = constants.AnimComSetVelX; param = 0 },
			{ dur = 100; num = 14 },
			{ dur = 100; num = 12 },
			{ com = constants.AnimComSetDamageMult; param = math.floor(difficulty*(-2/3)+4/3*1000) },
			{ com = constants.AnimComSetAnim; txt = "right" }
		}
	},
{ 
		name = "left_bombingrun";
		frames = 
		{

			{ com = constants.AnimComSetDamageMult; param = 1000 },
			{ com = constants.AnimComSetAccX; param = 200},
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 12 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ com = constants.AnimComSetAccX; param = -200 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ com = constants.AnimComSetAccX; param = 0 },
			{ com = constants.AnimComSetVelX; param = 0 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ dur = 100; num = 12 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ com = constants.AnimComSetDamageMult; param = math.floor(difficulty*(-2/3)+4/3*1000) },
			{ com = constants.AnimComSetAnim; txt = "right" }
		}
	},
	{
		name = "left_btards";
		frames =
		{

			{ com = constants.AnimComSetDamageMult; param = math.floor(difficulty*(-2/3)+4/3*1000) },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemy; txt = "btard-heli" },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemy; txt = "btard-heli" },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemy; txt = "btard-heli" },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComSetAnim; txt = "left_toright" }
		}
	},
	{
		name = "left_gun";
		frames =
		{

			{ com = constants.AnimComSetDamageMult; param = math.floor(difficulty*(-2/3)+4/3*1000) },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetAnim; txt = "left" }
		}
	},
	{
		name = "left_toright_gun";
		frames =
		{

			{ com = constants.AnimComSetDamageMult; param = math.floor(difficulty*(-2/3)+4/3*1000) },
			{ com = constants.AnimComSetAccX; param = 200},
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetAccX; param = -200 },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComAimedShot; txt = "fireshot" },
			{ com = constants.AnimComSetAccX; param = 0 },
			{ com = constants.AnimComSetVelX; param = 0 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetAnim; txt = "right" }
		}
	},
	{
		name = "jump";
		frames = 
		{
			{ com = constants.AnimComSetAccY; param = 5},
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetAccY; param = 0 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComJump; param = 4 }
		}
	},
	{
		name = "left_wave";
		frames = 
		{

			{ com = constants.AnimComSetDamageMult; param = math.floor(difficulty*(-2/3)+4/3*1000) },
			{ com = constants.AnimComSetAccX; param = 200},
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetDamageMult; param = 1000 },
			{ dur = 100; num = 12 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComSetAccX; param = -200 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb-waves"; param = 0 },
			{ dur = 100; num = 14 },
			{ dur = 100; num = 12 },
			{ com = constants.AnimComSetDamageMult; param = math.floor(difficulty*(-2/3)+4/3*1000) },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetAccX; param = 0 },
			{ com = constants.AnimComSetVelX; param = 0 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetAnim; txt = "right" }
		}
		
	},
	{
		name = "die";
		frames = 
		{
			{ com = constants.AnimComMapVarAdd; param = 5000; txt = "score" },
			{ com = constants.AnimComStop },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 8000 },
			{ com = constants.AnimComSetGravity },
			{ num = 0; dur = 1 },
			{ com = constants.AnimComJumpIfOnPlane; param; param = 8 },
			{ com = constants.AnimComJump; param; param = 5 },
			{ dur = 0 },
			{ com = constants.AnimComSetInvisible; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = -10 },
			{ com = constants.AnimComPushInt; param = 10 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = 10 },
			{ com = constants.AnimComPushInt; param = -10 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ dur = 10; num = 3 },
			{ com = constants.AnimComPushInt; param = 10 },
			{ com = constants.AnimComPushInt; param = 10 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = -10 },
			{ com = constants.AnimComPushInt; param = -10 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = 20 },
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComPushInt; param = 20 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ dur = 10; num = 3 },
			{ com = constants.AnimComPushInt; param = 20 },
			{ com = constants.AnimComPushInt; param = 20 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ dur = 200; num = 3 },
			{ com = constants.AnimComPushInt; param = 10 },
			{ com = constants.AnimComPushInt; param = -10 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = -10 },
			{ com = constants.AnimComPushInt; param = -10 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -10 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ dur = 200 },
			{ com = constants.AnimComPushInt; param = 20 },
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ dur = 200; num = 3 },
			{ com = constants.AnimComPushInt; param = 30 },
			{ com = constants.AnimComPushInt; param = -30 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = -30 },
			{ com = constants.AnimComPushInt; param = -30 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -30 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = 10 },
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComPushInt; param = -10 },
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComMapVarAdd; param = 1; txt = "helidead" },
			{ com = constants.AnimComDestroyObject }
		}
	}

}



