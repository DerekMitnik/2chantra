parent = "enemies/caves-dwall-bottom1"

animations =
{
	{
		name = "idle";
		frames =
		{
			{ dur = 1; num = 48 }
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 43 }                                      
		}
	},
	{
		name = "25%";
		frames =
		{
			{ dur = 1; num = 38 }                                      
		}
	},
}
