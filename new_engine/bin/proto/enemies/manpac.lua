--forbidden
bounce = 0;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 200;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
mp_count = 1;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

FunctionName = "CreateEnemy";

-- �������� �������

texture = "manpac";

z = -0.001;

local speed = 300*((difficulty-1)/3+1);
local health = 20;
if difficulty > 1 then
	health = 30;
elseif difficulty < 1 then
	health = 15;
end

faction_id = 1;
faction_hates = { -1, -2 };

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComSetHealth; param = health },
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ dur = 25; num = 0; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 25; num = 1; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 25; num = 2; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 25; num = 3; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 25; num = 4; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 25; num = 5; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ dur = 25; num = 6; com = constants.AnimComWaitForTarget; param = 3000; txt = "move" },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity_small" },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "retreat" }
		}
	},
	{ 
		name = "move";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = "pacman.ogg" },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComPushInt; param = 128 },
			{ com = constants.AnimComJumpRandom; param = 4 },
			{ com = constants.AnimComSetAnim; txt = "move_y" },
			{ dur = 0 },
			{ com = constants.AnimComSetAnim; txt = "move_x" }
		}
	},
	{ 
		name = "move_x";
		frames = 
		{
			{ com = constants.AnimComSetVelY; param = 5 },
			{ com = constants.AnimComPushInt; param = 16 },
			{ com = constants.AnimComJumpIfTargetCloseByX; param = 18 },
			{ dur = 25; num = 0; com = constants.AnimComMoveToTargetX; param = speed },
			{ dur = 25; num = 1; com = constants.AnimComMoveToTargetX; param = speed },
			{ dur = 25; num = 2; com = constants.AnimComMoveToTargetX; param = speed },
			{ dur = 25; num = 3; com = constants.AnimComMoveToTargetX; param = speed },
			{ dur = 25; num = 4; com = constants.AnimComMoveToTargetX; param = speed },
			{ dur = 25; num = 5; com = constants.AnimComMoveToTargetX; param = speed },
			{ dur = 25; num = 6; com = constants.AnimComMoveToTargetX; param = speed },
			{ dur = 25; num = 0; com = constants.AnimComMoveToTargetX; param = speed },
			{ dur = 25; num = 1; com = constants.AnimComMoveToTargetX; param = speed },
			{ dur = 25; num = 2; com = constants.AnimComMoveToTargetX; param = speed },
			{ dur = 25; num = 3; com = constants.AnimComMoveToTargetX; param = speed },
			{ dur = 25; num = 4; com = constants.AnimComMoveToTargetX; param = speed },
			{ dur = 25; num = 5; com = constants.AnimComMoveToTargetX; param = speed },
			{ dur = 25; num = 6; com = constants.AnimComMoveToTargetX; param = speed },
			{ com = constants.AnimComSetAnim; txt = "move" },
			{ dur = 1; num = 0; },
			{ com = constants.AnimComSetAnim; txt = "move_y"}
		}
	},
	{ 
		name = "move_y";
		frames = 
		{
			{ com = constants.AnimComSetVelX; param = 5 },
			{ com = constants.AnimComPushInt; param = 16 },
			{ com = constants.AnimComJumpIfTargetCloseByY; param = 18 },
			{ dur = 25; num = 0; com = constants.AnimComMoveToTargetY; param = speed },
			{ dur = 25; num = 1; com = constants.AnimComMoveToTargetY; param = speed },
			{ dur = 25; num = 2; com = constants.AnimComMoveToTargetY; param = speed },
			{ dur = 25; num = 3; com = constants.AnimComMoveToTargetY; param = speed },
			{ dur = 25; num = 4; com = constants.AnimComMoveToTargetY; param = speed },
			{ dur = 25; num = 5; com = constants.AnimComMoveToTargetY; param = speed },
			{ dur = 25; num = 6; com = constants.AnimComMoveToTargetY; param = speed },
			{ dur = 25; num = 0; com = constants.AnimComMoveToTargetY; param = speed },
			{ dur = 25; num = 1; com = constants.AnimComMoveToTargetY; param = speed },
			{ dur = 25; num = 2; com = constants.AnimComMoveToTargetY; param = speed },
			{ dur = 25; num = 3; com = constants.AnimComMoveToTargetY; param = speed },
			{ dur = 25; num = 4; com = constants.AnimComMoveToTargetY; param = speed },
			{ dur = 25; num = 5; com = constants.AnimComMoveToTargetY; param = speed },
			{ dur = 25; num = 6; com = constants.AnimComMoveToTargetY; param = speed },
			{ com = constants.AnimComSetAnim; txt = "move" },
			{ dur = 1; num = 0; },
			{ com = constants.AnimComSetAnim; txt = "move_x"}
		}
	},
	{ 
		name = "retreat";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 128 },
			{ com = constants.AnimComJumpRandom; param = 3 },
			{ com = constants.AnimComSetAnim; txt = "retreat_y" },
			{ dur = 0 },
			{ com = constants.AnimComSetAnim; txt = "retreat_x" }
		}
	},
	{ 
		name = "retreat_x";
		frames = 
		{
			{ com = constants.AnimComSetVelY; param = 0 },
			{ dur = 25; num = 0; com = constants.AnimComMoveToTargetX; param = -speed },
			{ dur = 25; num = 1; com = constants.AnimComMoveToTargetX; param = -speed },
			{ dur = 25; num = 2; com = constants.AnimComMoveToTargetX; param = -speed },
			{ dur = 25; num = 3; com = constants.AnimComMoveToTargetX; param = -speed },
			{ dur = 25; num = 4; com = constants.AnimComMoveToTargetX; param = -speed },
			{ dur = 25; num = 5; com = constants.AnimComMoveToTargetX; param = -speed },
			{ dur = 25; num = 6; com = constants.AnimComMoveToTargetX; param = -speed },
			{ dur = 25; num = 0; com = constants.AnimComMoveToTargetX; param = -speed },
			{ dur = 25; num = 1; com = constants.AnimComMoveToTargetX; param = -speed },
			{ dur = 25; num = 2; com = constants.AnimComMoveToTargetX; param = -speed },
			{ dur = 25; num = 3; com = constants.AnimComMoveToTargetX; param = -speed },
			{ dur = 25; num = 4; com = constants.AnimComMoveToTargetX; param = -speed },
			{ dur = 25; num = 5; com = constants.AnimComMoveToTargetX; param = -speed },
			{ dur = 25; num = 6; com = constants.AnimComMoveToTargetX; param = -speed },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		name = "retreat_y";
		frames = 
		{
			{ com = constants.AnimComSetVelX; param = 0 },
			{ dur = 25; num = 0; com = constants.AnimComMoveToTargetY; param = -speed },
			{ dur = 25; num = 1; com = constants.AnimComMoveToTargetY; param = -speed },
			{ dur = 25; num = 2; com = constants.AnimComMoveToTargetY; param = -speed },
			{ dur = 25; num = 3; com = constants.AnimComMoveToTargetY; param = -speed },
			{ dur = 25; num = 4; com = constants.AnimComMoveToTargetY; param = -speed },
			{ dur = 25; num = 5; com = constants.AnimComMoveToTargetY; param = -speed },
			{ dur = 25; num = 6; com = constants.AnimComMoveToTargetY; param = -speed },
			{ dur = 25; num = 0; com = constants.AnimComMoveToTargetY; param = -speed },
			{ dur = 25; num = 1; com = constants.AnimComMoveToTargetY; param = -speed },
			{ dur = 25; num = 2; com = constants.AnimComMoveToTargetY; param = -speed },
			{ dur = 25; num = 3; com = constants.AnimComMoveToTargetY; param = -speed },
			{ dur = 25; num = 4; com = constants.AnimComMoveToTargetY; param = -speed },
			{ dur = 25; num = 5; com = constants.AnimComMoveToTargetY; param = -speed },
			{ dur = 25; num = 6; com = constants.AnimComMoveToTargetY; param = -speed },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		name = "die";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateItem; txt = "wakabamark_small" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateItem; txt = "wakabamark_small" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity2" },
			{ com = constants.AnimComPlaySound; txt = "pacman-die.ogg" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 800 },
			{ com = constants.AnimComSetGravity },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0; },
			{ com = constants.AnimComMapVarAdd; param = 15; txt = "score" },
			{ com = constants.AnimComMapVarAdd; param = 1; txt = "kills" },
			{ num = 7; dur = 25 },
			{ num = 8; dur = 25 },
			{ dur = 0 },
			{ num = 9; dur = 1 },
			{ com = constants.AnimComJumpIfOnPlane; param = 26 },
			{ com = constants.AnimComJump; param = 20 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity2" },
			{ dur = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ num = 10; dur = 25; },
			{ num = 11; dur = 25; },
			{ num = 12; dur = 25; },
			{ num = 13; dur = 500; },
			{ com = constants.AnimComPushInt; param = 320 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ dur = 1; num = 0; com = constants.AnimComJumpIfCloseToCamera; param = 37 },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		name = "touch";
		frames =
		{
			{ com = constants.AnimComJumpIfPlayerId; param = 3 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "move" },
			{ dur = 0 },
			{ com = constants.AnimComDealDamage; param = 10*difficulty },
			{ com = constants.AnimComSetAnim; txt = "retreat" }
		}
	},
	{ 
		-- ��������
		name = "land";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	}
	
}



