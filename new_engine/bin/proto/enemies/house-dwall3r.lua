parent = "enemies/house-dwall1r"

animations =
{
	{
		name = "idle";
		frames =
		{
			{ dur = 1; num = 269 }
		}
	},
	{
		name = "75%";
		frames =
		{
			{ dur = 1; num = 269 } 
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 269 }                                      
		}
	},
	{
		name = "25%";
		frames =
		{
			{ dur = 1; num = 371 }                                      
		}
	}
}
