physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 1.8;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 2;
offscreen_distance = 1280
offscreen_behavior = constants.offscreenDestroy

drops_shadow = 1;

gravity_x = 0;
gravity_y = 0.4;

faction_id = 1;
faction_hates = { -1, -2 };

texture = "slime";

z = -0.1;

color = {61/255, 114/255, 59/255, 1}
overlay = {0};
ocolor = {{ 0.7, 1, 0.7, 1}}

animations = 
{
	{ 
		-- Создание
		name = "init";
		frames = 
		{
			{ param = function( obj )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
			end },
			
			{ com = constants.AnimComPushInt; param = -26},
			{ com = constants.AnimComPushInt; param = 20},
			{ com = constants.AnimComMPSet; param = 1 },
			
			{ com = constants.AnimComPushInt; param = 1},
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComCreateEffect; txt = "slime_trail", param = 4 },
			
			{ param = function( obj )
				local children = GetChildren( obj )
				if not children then return end
				for k, v in pairs( children ) do
					mapvar.tmp[v] = 
					{
						start_color = { 61/255, 114/255, 59/255, 1 },
						end_color = { 61/255, 114/255, 59/255, 0 }
					}
				end
			end },
			
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 64 * 39/25 },
			{ com = constants.AnimComRealH; param = 64 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{
		name = "final_count";
		frames =
		{
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
			end },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 50 * 39/25 },
			{ com = constants.AnimComRealH; param = 50 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComJump; param = 6 }
		}
	},
	{ 
		-- Создание
		name = "idle";
		frames = 
		{
			{ dur = 100; num = 0; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ dur = 100; num = 1; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ dur = 100; num = 2; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ dur = 100; num = 3; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ dur = 100; num = 2; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ dur = 100; num = 1; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ com = constants.AnimComLoop }	
		}
	},
	{
		name = "chase";
		frames =
		{
			{ constants.AnimComFaceTarget }, --Does not work for some reason.
			{ param = function( obj )
				if obj:aabb().p.x > obj:target():aabb().p.x then
					SetObjSpriteMirrored( obj, true );
				end
			end },
			{ constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		-- Создание
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = "slime.ogg" },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ param = function(this) Game.FlashObject( this ) end },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "psewerslime2" },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		-- Создание
		name = "move";
		frames = 
		{
			{ com = constants.AnimComSetRelativeAccX; param = 450 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "stuck";
		frames =
		{
			{ com = constants.AnimComMirror },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		-- Создание
		name = "die";
		frames = 
		{
			{ com = constants.AnimComStop; },
			{ com = constants.AnimComDestroyObject; param = 3 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "psewerslime2" },
			{ com = constants.AnimComRealW; param = 64 * 67/25 },
			{ com = constants.AnimComRealH; param = 64 * 35/25 },
			{ num = 4; dur = 100 },
			{ num = 5; dur = 100 },
			{ num = 6; dur = 100 },
			{ param = function( obj )
				Info.RevealEnemy( "GREEN_SLIME" )
				local pos = obj:aabb().p
				Game.AddCombo( 1 )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "dark_green_slime", { 39, 25 } ) 
				Game.CreateScoreItems( 1, pos.x, pos.y )
			end },
			{ num = 7; dur = 100 },
			{ num = 8; dur = 100 },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		name = "land";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{
		name = "target_dead";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{
		name = "touch";
		frames =
		{
			{ param = function( obj )
				local toucher = GetObjectUserdata( ObjectPopInt( obj ) )
				Game.touch_push( obj, toucher )
				DamageObject( toucher, 10 )
			end },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComRecover; txt = "move" }
		}
	}
}



