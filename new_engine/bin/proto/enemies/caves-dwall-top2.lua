parent = "enemies/caves-dwall-bottom1"

animations =
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ dur = 1; num = 30 }
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 35 }                                      
		}
	},
	{
		name = "25%";
		frames =
		{
			{ dur = 1; num = 40 }                                      
		}
	},
}
