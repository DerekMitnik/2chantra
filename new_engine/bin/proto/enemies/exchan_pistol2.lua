--forbidden
name = "exchan_pistol";



offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 5;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 1;

drops_shadow = 1;

faction_id = 1;
faction_hates = { 3, -1, -2 };

gravity_x = 0;
gravity_y = 0.8;

FunctionName = "CreateEnemy";

-- �������� �������

--LoadTexture("soh-chan1024.png")
texture = "ex_pistol";
z = -0.0015;

local speed = 150;
if difficulty>=2.0 then speed = 180; end
local close = 150;
local far = 700;
local mc = 64;
local fc = 74;
local prob_shot = 20;
if difficulty>=2.0 then prob_shot = 40; end
local health = 125

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 50 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 80 },
			{ dur = 100; num = 21 },
			{ dur = 100; num = 22 },
			{ dur = 100; num = 23 },
			{ dur = 100; num = 21 },
			{ dur = 100; num = 22 },
			{ dur = 100; num = 23 },
			{ com = constants.AnimComSetHealth; param = health; },
			{ com = constants.AnimComSetAnim; txt = "move"; }
		}
	},
	{
		name = "idle";
		frames = 
		{
			{ dur = 100; com = constants.AnimComWaitForTarget; param = 30000; txt = "move"; },
			{ com = constants.AnimComLoop; }
		}
	},
	{
		name = "shoot";
		frames = 
		{
			{ com = constants.AnimComSetWeaponAngle; param =  0 },
			{ com = constants.AnimComShootDir; },
			--{ com = constants.AnimComSetAnimIfWeaponNotReady; txt = "aim"; },
			{ com = constants.AnimComRealX; param = 50 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 80 },
			{ com = constants.AnimComShootDir; },
			{ dur = 40; num = 0 },
			{ dur = 40; num = 1 },
			{ dur = 40; num = 2 },
			{ dur = 40; num = 3 },
			{ dur = 40; num = 4 },
			{ dur = 40; num = 5 },
			{ dur = 40; num = 6 },
			{ dur = 40; num = 7 },
			{ dur = 40; num = 8 },
			{ dur = 40; num = 9 },
			{ com = constants.AnimComPushInt; param = 31 },
			{ com = constants.AnimComAdjustAim; param = -24 },
			{ com = constants.AnimComPushInt; param = 30; },
			{ com = constants.AnimComPushInt; param = -24; },			
			{ dur = 40; num = 10; com = constants.AnimComAimedShot; txt = "fireshot"; },
			{ dur = 40; num = 11 },
			{ com = constants.AnimComPushInt; param = 30; },
			{ com = constants.AnimComPushInt; param = -24; },			
			{ dur = 40; num = 12; com = constants.AnimComAimedShot; txt = "fireshot"; },
			{ dur = 40; num = 13 },
			{ com = constants.AnimComPushInt; param = 30; },
			{ com = constants.AnimComPushInt; param = -24; },			
			{ dur = 40; num = 14; com = constants.AnimComAimedShot; txt = "fireshot"; },
			{ dur = 40; num = 15 },
			{ dur = 40; num = 16 },
			{ dur = 40; num = 17 },
			{ dur = 40; num = 18 },
			{ dur = 40; num = 19 },
			{ dur = 40; num = 20 },
			{ dur = 40; num = 21 },
			{ dur = 40; num = 22 },
			{ dur = 40; num = 23 },
			{ dur = 40; num = 24 },
			{ dur = 40; num = 25 },
			{ dur = 40; num = 26 },
			{ com = constants.AnimComPushInt; param = 12; },
			{ com = constants.AnimComPushInt; param = -24; },			
			{ dur = 40; num = 27; com = constants.AnimComAimedShot; txt = "fireshot"; },
			{ dur = 40; num = 28 },
			{ com = constants.AnimComPushInt; param = 12; },
			{ com = constants.AnimComPushInt; param = -24; },			
			{ dur = 40; num = 29; com = constants.AnimComAimedShot; txt = "fireshot"; },
			{ dur = 40; num = 30 },
			{ com = constants.AnimComPushInt; param = 12; },
			{ com = constants.AnimComPushInt; param = -24; },			
			{ dur = 40; num = 31; com = constants.AnimComAimedShot; txt = "fireshot"; },
			{ dur = 40; num = 32 },
			{ dur = 40; num = 33 },
			{ dur = 40; num = 34 },
			{ com = constants.AnimComSetAnim; txt = "move"; }
		}
	},
	{
		name = "move";
		frames = 
		{
			{ dur = 100; num = 12; com = constants.AnimComMoveToTargetX; param = speed; },
			{ dur = 100; num = 13; com = constants.AnimComMoveToTargetX; param = speed; },
			{ dur = 100; num = 14; com = constants.AnimComMoveToTargetX; param = speed; },
			{ com = constants.AnimComSetAnim; param = 240; txt = "move" },
			{ com = constants.AnimComSetAnim; txt = "shoot" },
		}
	},
	{
		name = "die";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 300 },
			{ com = constants.AnimComSetGravity }, 
			{ com = constants.AnimComDrop; param = 1 },
			{ com = constants.AnimComSetTouchable; param = 0; },
			{ com = constants.AnimComSetShadow; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity2" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity" },
			{ com = constants.AnimComSetBulletCollidable; param = 0; },
			{ com = constants.AnimComMapVarAdd; param = 100*difficulty; txt = "score"; },
		}
	}	
}
