parent = "enemies/lab-dwall1"

animations =
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealH; param = 20 },
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealX; param = 12 },
			{ com = constants.AnimComRealY; param = 13 },
			{ dur = 1; num = 66 }
		}
	},
	{
		name = "80%";
		frames =
		{
			{ dur = 1; num = 66 } 
		}
	},
	{
		name = "60%";
		frames =
		{
			{ dur = 1; num = 66 }                                      
		}
	},
	{
		name = "40%";
		frames =
		{
			{ dur = 1; num = 66 }                                      
		}
	},
	{
		name = "20%";
		frames =
		{
			{ dur = 1; num = 81 }                                      
		}
	}
}
