name = "thief";

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 30;
phys_max_y_vel = 0;
phys_jump_vel = 20;
phys_walk_acc = 10;
phys_one_sided = 0;
phys_bullet_collidable = 0;
mp_count = 1;

FunctionName = "CreateEnemy";

-- �������� �������

texture = "btard-ny";

z = -0.001;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 54 },
			{ com = constants.AnimComRealH; param = 72 },
			{ com = constants.AnimComSetHealth; param = 90000 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComSetGravity },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ dur = 100; num = 28 },
			{ com = constants.AnimComPushInt; param = 220 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 5 },
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ com = constants.AnimComPushInt; param = 200 },
			{ com = constants.AnimComPushInt; param = -1 },
			{ com = constants.AnimComSetGravity },
			{ dur = 100; num = 28 },
			{ dur = 100; num = 29 },
			{ dur = 100; num = 30 },
			{ dur = 100; num = 31 },
			{ dur = 100; num = 32 },
			{ dur = 100; num = 33 },
			{ com = constants.AnimComPushInt; param = 1200 },
			{ com = constants.AnimComPushInt; param = 840 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 10 },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{ 
		-- ��������
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComRecover }
		}
	},
	{
		name = "jump";
		frames = 
		{
			{ dur = 100; num = 28 },
			{ com = constants.AnimComPushInt; param = 220 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 5 },
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ com = constants.AnimComPushInt; param = 200 },
			{ com = constants.AnimComPushInt; param = -1 },
			{ com = constants.AnimComSetGravity },
			{ dur = 100; num = 28 },
			{ dur = 100; num = 29 },
			{ dur = 100; num = 30 },
			{ dur = 100; num = 31 },
			{ dur = 100; num = 32 },
			{ dur = 100; num = 33 },
			{ com = constants.AnimComPushInt; param = 1200 },
			{ com = constants.AnimComPushInt; param = 840 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 10 },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		name = "land";
		frames = 
		{
			{ dur = 100; num = 28 },
			{ com = constants.AnimComPushInt; param = 220 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 5 },
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ com = constants.AnimComPushInt; param = 200 },
			{ com = constants.AnimComPushInt; param = -1 },
			{ com = constants.AnimComSetGravity },
			{ dur = 100; num = 28 },
			{ dur = 100; num = 29 },
			{ dur = 100; num = 30 },
			{ dur = 100; num = 31 },
			{ dur = 100; num = 32 },
			{ dur = 100; num = 33 },
			{ com = constants.AnimComPushInt; param = 1200 },
			{ com = constants.AnimComPushInt; param = 840 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 10 },
			{ com = constants.AnimComDestroyObject }
		}
	}

}



