physic = 1;
phys_bullet_collidable = 1;
phys_max_x_vel = 2;
phys_max_y_vel = 50;
mp_count = 1;

gravity_x = 0;
gravity_y = 0.8;
mass = -1;

texture = "robot";

z = -0.1;

faction_id = 1;
faction_hates = { -1, -2 };

drops_shadow = 1;

animations = 
{
	{ 
		-- Создание
		name = "init";
		frames = 
		{
			--{ com = constants.AnimComRealX; param = 0 },
			--{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 30 },
			{ com = constants.AnimComRealH; param = 33 },
			{ com = constants.AnimComSetHealth; param = 30 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ param = function( obj )
				local enemy = GetNearestEnemy( obj )
				if not enemy then return end
				if obj:aabb().p.x > enemy:aabb().p.x then
					SetObjSpriteMirrored( obj, true )
				end
			end },
			{ dur = 100; num = 4 },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 2 },
			{ com = constants.AnimComSetBulletCollidable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ dur = 100; num = 0; com = constants.AnimComSetAnim; txt = "move" },
		}
	},
	{ 
		-- Создание
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ param = function(this) Game.FlashObject( this ) end },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pexplosion_sparks" },
			{ dur = 100; num = 2; com = constants.AnimComRealH; param = 33 },
			{ dur = 100; num = 3 },
			--{ dur = 5000; num = 4 },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 2 },
			{ com = constants.AnimComSetBulletCollidable; param = 1 },
			{ com = constants.AnimComRecover; txt = "idle" }
		}
	},
	{ 
		-- Создание
		name = "move";
		frames = 
		{
			{ com = constants.AnimComSetBulletCollidable; param = 1 },
			{ com = constants.AnimComRealH; param = 33 },
			{ dur = 100; num = 0; com = constants.AnimComSetRelativeAccX; param = 400  },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComJumpIfTargetClose; param = 10 },
			{ com = constants.AnimComRealH; param = 34 },
			{ dur = 100; num = 1; },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComJumpIfTargetClose; param = 10 },
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ com = constants.AnimComSetAnim; txt = "attack" }
		}
	},
	{
		name = "attack";
		frames =
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComRealH; param = 34 },
			{ dur = 100; num = 5},
			{ dur = 100; num = 6},
			{ dur = 100; num = 8},
			{ dur = 100; num = 9},
			{ com = constants.AnimComRealH; param = 37 },
			{ dur = 100; num = 10},
			
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealH; param = 42 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ dur = 100; num = 11},
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ dur = 100; num = 12},
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ dur = 100; num = 13},
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ dur = 100; num = 14},
			
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComRealH; param = 37 },
			{ dur = 100; num = 10},
			{ com = constants.AnimComRealH; param = 34 },
			{ dur = 100; num = 9},
			{ dur = 100; num = 8},
			{ dur = 100; num = 7},
			{ dur = 100; num = 6},
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetRelativeAccX; param = 400  },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{
		name = "touch";
		frames = 
		{
			{ com = constants.AnimComJumpIfPlayerId; param = 2 },
			{ com = constants.AnimComRecover },
			{ dur = 0 },
			{ com = constants.AnimComDealDamage; param = 10 },
			{ com = constants.AnimComRecover },
		}
	},
	{ 
		name = "die";
		frames = 
		{
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pexplosion_sparks" },
			--{ com = constants.AnimComCreateEnemyBullet; txt = "explosion_big" },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComRealW; param = 30 },
			{ com = constants.AnimComRealH; param = 33 },
			{ param = function( obj )
				local pos = obj:aabb().p
				Game.AddCombo( 1 )
				--Info.RevealEnemy( info_handle )
				--Info.RevealEnemy( "BLUE_SLIME" )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "pear15robot", { 39, 25 } )
				Game.CreateScoreItems( 2, pos.x, pos.y )
			end },
			{ dur = 5000; num = 15 },
			{ com = constants.AnimComPushInt; param = 400 },
			{ com = constants.AnimComPushInt; param = 300 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 8 }
		}
	},
	{ 
		name = "stuck";
		frames = 
		{
			{ dur = 100; num = 2 },
			{ com = constants.AnimComRealX; param = -2 },
			{ dur = 100; num = 3 },
			{ com = constants.AnimComRealX; param = -4 },
			{ dur = 100; num = 4 },
			{ com = constants.AnimComMirror },
			{ com = constants.AnimComRealX; param = -2 },
			{ dur = 100; num = 3 },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 100; num = 2 },
			{ com = constants.AnimComSetRelativeAccX; param = 400  },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{
		name = "final_count";
		frames = 
		{
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ com = constants.AnimComRealW; param = 30 },
			{ com = constants.AnimComRealH; param = 33 },
			{ com = constants.AnimComRealX; },
			{ com = constants.AnimComRealY; },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComRealH; param = 33 },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComRealH; param = 34 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComRealH; param = 33 },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComRealH; param = 34 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComRealH; param = 33 },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComRealH; param = 34 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComRealH; param = 34 },
			{ dur = 100; num = 5},
			{ dur = 100; num = 6},
			{ dur = 100; num = 8},
			{ dur = 100; num = 9},
			{ com = constants.AnimComRealH; param = 37 },
			{ dur = 100; num = 10},
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealH; param = 42 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ dur = 100; num = 11},
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ dur = 100; num = 12},
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ dur = 100; num = 13},
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ dur = 100; num = 14},
			{ com = constants.AnimComRealH; param = 37 },
			{ dur = 100; num = 10},
			{ com = constants.AnimComRealH; param = 34 },
			{ dur = 100; num = 9},
			{ dur = 100; num = 8},
			{ dur = 100; num = 7},
			{ dur = 100; num = 6},
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComJump; param = 6 }
		}
	},
	{
		name = "edge";
		frames =
		{
			{ com = constants.AnimComJump; param = function( obj )
				if not mapvar.tmp[obj] then
					mapvar.tmp[obj] = {}
				end
				local plane = obj:suspected_plane()
				local o1 = obj:aabb()
				if plane then
					local o2 = plane:aabb()
					if not mapvar.tmp[obj].time then mapvar.tmp[obj].time = Loader.time - 200 end
					if mapvar.tmp[obj].time + 150 < Loader.time then
					--if o2.W > o1.W + 20 then
						mapvar.tmp[obj].time = Loader.time
						return 0
					end
				end
				return 12
			end },
			{ com = constants.AnimComSetRelativeAccX; param = 0 },
			{ dur = 100; num = 2 },
			{ com = constants.AnimComRealX; param = -2 },
			{ dur = 100; num = 3 },
			{ com = constants.AnimComRealX; param = -4 },
			{ dur = 100; num = 4 },
			{ com = constants.AnimComMirror },
			{ com = constants.AnimComRealX; param = -2 },
			{ dur = 100; num = 3 },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 100; num = 2 },
			{ com = constants.AnimComSetRelativeAccX; param = 400 },
			{ com = constants.AnimComRecover }
		}
	},
}



