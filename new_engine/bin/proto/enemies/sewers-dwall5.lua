parent = "enemies/sewers-dwall1"

animations =
{
	{
		name = "idle";
		frames =
		{
			{ dur = 1; num = 16 }
		}
	},
	{
		name = "75%";
		frames =
		{
			{ dur = 1; num = 17 } 
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 18 }                                      
		}
	},
	{
		name = "25%";
		frames =
		{
			{ dur = 1; num = 19 }                                      
		}
	}
}
