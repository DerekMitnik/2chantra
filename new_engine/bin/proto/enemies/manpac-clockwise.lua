physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 2.25;
phys_max_y_vel = 3;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 1;
offscreen_distance = 640
offscreen_behavior = constants.offscreenDestroy

drops_shadow = 1;

gravity_x = 0;
gravity_y = 0;

faction_id = 1;
faction_hates = { -1, -2 };

texture = "manpac";

z = -0.1;

animations = WithLabels
{
	{ 
		-- Создание
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{
		name = "final_count";
		frames =
		{
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ dur = 75; num = 1; },
			{ dur = 75; num = 2; },
			{ dur = 75; num = 3; },
			{ dur = 75; num = 4; },
			{ dur = 75; num = 5; },
			{ dur = 75; num = 6; },
			{ dur = 75; num = 0; },
			{ com = constants.AnimComJump; param = 6 }
		}
	},
	{ 
		-- Создание
		name = "idle";
		frames = 
		{
			{ dur = 100; com = constants.AnimComWaitForTarget; param = 30000; txt = "start" },
			{ com = constants.AnimComLoop }	
		}
	},
	{
		name = "start";
		frames =
		{
			{ com = constants.AnimComFaceTarget },
			{ param = function( obj )
				local target = obj:target():aabb().p
				local pos = obj:aabb().p
				if math.abs( pos.x - target.x ) < 200 and math.abs( pos.y - target.y ) > 100 then
					if pos.y > target.y then
						mapvar.tmp[obj] = 8
						obj:acc( { x = 0, y = -0.4 } )
					else
						mapvar.tmp[obj] = 2
						obj:acc( { x = 0, y = 0.4 } )
					end
				else
					if pos.x > target.x then
						mapvar.tmp[obj] = 4
						obj:acc( { x = -0.4, y = 0 } )
						obj:sprite_mirrored( true )
					else
						mapvar.tmp[obj] = 6
						obj:acc( { x = 0.4, y = 0 } )
						obj:sprite_mirrored( false )
					end
				end
			end },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{ 
		-- Создание
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ param = function(this) Game.FlashObject( this ) end },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity_small" },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		-- Создание
		name = "fly";
		frames = 
		{
			{ dur = 25; num = 0; },
			{ com = constants.AnimComJump; param = function( obj )
				if ObjectOnScreen( obj, 256 ) then
					return 1
				else
					return 16
				end
			end },
			{ com = constants.AnimComPushInt; param = 128 },
			{ com = constants.AnimComPlaySound; txt = "pacman.ogg"; param = 2 },
			{ dur = 25; num = 1; },
			{ dur = 25; num = 2; },
			{ dur = 25; num = 3; },
			{ dur = 25; num = 4; },
			{ dur = 25; num = 5; },
			{ dur = 25; num = 6; },
			{ dur = 25; num = 0; },
			{ dur = 25; num = 1; },
			{ dur = 25; num = 2; },
			{ dur = 25; num = 3; },
			{ dur = 25; num = 4; },
			{ dur = 25; num = 5; },
			{ dur = 25; num = 6; },
			{},
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "stuck";
		frames =
		{
			{ param = function( obj )
				obj:vel( { x = 0, y = 0 } )
				local acc = {}
				if mapvar.tmp[obj] == 4 then
					acc.x = 0
					acc.y = -0.4
					mapvar.tmp[obj] = 8
				elseif mapvar.tmp[obj] == 6 then
					acc.x = 0
					acc.y = 0.4
					mapvar.tmp[obj] = 2
				elseif mapvar.tmp[obj] == 8 then
					acc.x = 0.4
					acc.y = 0
					obj:sprite_mirrored( false )
					mapvar.tmp[obj] = 6
				else
					acc.x = -0.4
					acc.y = 0
					obj:sprite_mirrored( true )
					mapvar.tmp[obj] = 4
				end
				obj:acc( acc )
			end },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{
		name = "stuck_vertical";
		frames =
		{
			{ param = function( obj )
				obj:vel( { x = 0, y = 0 } )
				local acc = {}
				if mapvar.tmp[obj] == 4 then
					acc.x = 0
					acc.y = -0.4
					mapvar.tmp[obj] = 8
				elseif mapvar.tmp[obj] == 6 then
					acc.x = 0
					acc.y = 0.4
					mapvar.tmp[obj] = 2
				elseif mapvar.tmp[obj] == 8 then
					acc.x = 0.4
					acc.y = 0
					obj:sprite_mirrored( false )
					mapvar.tmp[obj] = 6
				else
					acc.x = -0.4
					acc.y = 0
					obj:sprite_mirrored( true )
					mapvar.tmp[obj] = 4
				end
				obj:acc( acc )
			end },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{ 
		name = "die";
		frames = 
		{
			{ com = constants.AnimComSetMaxVelY; param = 90000 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity2" },
			{ com = constants.AnimComPushInt; param = 128 },
			{ com = constants.AnimComPlaySound; txt = "pacman-die.ogg"; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 800 },
			{ com = constants.AnimComSetGravity },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0; },
			{ param = function(obj)
				Game.AddCombo( 1 )
				Info.RevealEnemy( "MANPAC" )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "manpac-clockwise", { 39, 16 } ) 
				local pos = obj:aabb().p
				Game.CreateScoreItems( 2, pos.x, pos.y )
			end },
			{ num = 7; dur = 25 },
			{ num = 8; dur = 25 },
			{ label = "FREEFALL" },
			{ num = 9; dur = 1 },
			{ com = constants.AnimComJumpIfOnPlane; param = "LAND" },
			{ com = constants.AnimComJump; param = "FREEFALL" },
			{ label = "LAND" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity2" },
			{ dur = 0; param = function( obj )
				obj:acc( { x = 0, y = 0 } )
			end },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ num = 10; dur = 25; },
			{ num = 11; dur = 25; },
			{ num = 12; dur = 25; },
			{ label = "KEEP_CORPSE" },
			{ num = 13; dur = 1500; },
			{ com = constants.AnimComPushInt; param = 320 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ dur = 1; num = 0; com = constants.AnimComJumpIfCloseToCamera; param = "KEEP_CORPSE" },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		name = "land";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{
		name = "target_dead";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{
		name = "touch";
		frames =
		{
			{ param = function( obj )
				local toucher = GetObjectUserdata( ObjectPopInt( obj ) )
				Game.touch_push( obj, toucher )
				DamageObject( toucher, 10 )
			end },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComRecover }
		}
	}
}



