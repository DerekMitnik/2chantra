--forbidden
health = 100;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 5;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 1;
offscreen_distance = 640
offscreen_behavior = constants.offscreenDestroy

drops_shadow = 1;

gravity_x = 0;
gravity_y = 0.3;

FunctionName = "CreateEnemy";

faction_id = 1;
faction_hates = { -1, -2 };

-- �������� �������

texture = "btard";

z = -0.1;

image_width = 1024;
image_height = 2048;
frame_width = 256;
frame_height = 128;
frames_count = 28;

overlay = {0};
ocolor = {{1, 0.8, 1, 1}}
local diff = (difficulty-1)/5+1;
local difcom = constants.AnimComNone;
if (difficulty >=1) then difcom = constants.AnimComJump; end

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComControlledOverlayColor },
			{ param = function( obj )
				local enemy = GetNearestEnemy( obj )
				if not enemy then return end
				if obj:aabb().p.x > enemy:aabb().p.x then
					SetObjSpriteMirrored( obj, true )
				end
				mapvar.tmp[obj] = { landed = false }
			end },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		-- ��������
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComStop; },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComRealX; param = 1 },
			{ dur = 100; num = 0; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ com = constants.AnimComRealX; param = 1 },
			{ dur = 100; num = 1; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 100; num = 2; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ dur = 100; num = 3; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ dur = 100; num = 4; com = constants.AnimComWaitForTarget; param = 30000; txt = "chase" },
			{ com = constants.AnimComLoop }	
		}
	},
	{
		name = "chase";
		frames =
		{
			{ com = constants.AnimComFaceTarget },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		-- ��������
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = "ouch2.ogg" },
			--{ com = constants.AnimComJumpIfIntEquals; param = 2; txt = "sfg9000" },
			{ param = function( obj )
				local damage_type = ObjectPopInt( obj )
				if damage_type == Game.damage_types.fire then
					CreateEffect( "flame_effect", -5, -20, obj:id(), 0 )
					Resume( NewMapThread( function()
						while obj:health() > 0 do
							DamageObject( obj, 1 )
							Wait( 1000 )
						end
					end ))
				elseif damage_type == Game.damage_types.deadly_fire then
					CreateEffect( "flame_effect", -5, -20, obj:id(), 0 )
					Resume( NewMapThread( function()
						while obj:health() > 0 do
							DamageObject( obj, 1 )
							Wait( 200 )
						end
					end ))
				end
			end },
			{ com = constants.AnimComReduceHealth },
			{ param = function(this) Game.FlashObject( this ) end },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity_small"; param = 2 },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		-- ��������
		name = "move";
		frames = 
		{
			{ com = constants.AnimComSetRelativeAccX; param = 500 },
			{ com = constants.AnimComRealX; param = 11 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = 35 },
			{ com = constants.AnimComEnvSound; param = 1},
			{ dur = 80; num = 5 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = 35 },
			{ com = constants.AnimComRealX; param = 4 },
			{ dur = 80; num = 6 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = 35 },
			{ com = constants.AnimComRealX; param = 6 },
			{ dur = 80; num = 7 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = 35 },
			{ com = constants.AnimComRealX; param = 18 },
			{ com = constants.AnimComEnvSound; },
			{ dur = 80; num = 8 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = 35 },
			{ com = constants.AnimComRealX; param = 12 },
			{ dur = 80; num = 9 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = 35 },
			{ com = constants.AnimComRealX; param = 4 },
			{ dur = 80; num = 10 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = 35 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComWaitForTarget; param = 30000; txt = "move" },
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ com = constants.AnimComSetAnim; txt = "attack" }	
		}
	},
	{
		name = "stuck";
		frames =
		{
			{ com = constants.AnimComMirror },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{
		name = "think";
		frames = 
		{
			{ dur = 0 },
			--{ com = constants.AnimComEnemyClean; param = 20 },
			{ dur = 0 }, 
			{ com = constants.AnimComRealX; param = 0; com = constants.AnimComStop; },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComRealX; param = 1 },
			{ dur = 200; num = 0; com = constants.AnimComWaitForTarget; param = 100; txt = "move" },
			{ com = constants.AnimComRealX; param = 1 },
			{ dur = 200; num = 1; com = constants.AnimComWaitForTarget; param = 100; txt = "move" },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 200; num = 2; com = constants.AnimComWaitForTarget; param = 100; txt = "move" },
			{ dur = 200; num = 3; com = constants.AnimComWaitForTarget; param = 100; txt = "move" },
			{ dur = 200; num = 4; com = constants.AnimComWaitForTarget; param = 100; txt = "move" },
			--������� ����� ������� ����� � ������������ 0.5
			{ com = constants.AnimComPushInt; param = 128 },
			{ com = constants.AnimComJumpRandom; param = 18 },
			{ com = difcom; param = 19 },
			{ com = constants.AnimComSetAnim; txt = "move" },
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ com = constants.AnimComPushInt; param = 640 },
			{ com = constants.AnimComJumpIfTargetClose; param = 21 },
			{ com = constants.AnimComSetAnim; txt = "move" },
			{ dur = 0 },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		-- ��������
		name = "jump";
		frames = 
		{
			{ com = constants.AnimComJump; param = function( obj )
				if mapvar.tmp[obj] and mapvar.tmp[obj].landed then
					return 0
				else
					return 6
				end
			end },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 59 },
			{ com = constants.AnimComRealH; param = 67 },
			{ com = constants.AnimComSetVelY; param = -3000 },
			{ dur = 100; num = 35 },
			{ com = constants.AnimComJump; param = 6 }	
		}
	},
	{ 
		-- ��������
		name = "die";
		frames = 
		{
			{ com = constants.AnimComStop; },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 78 },
			{  },
			{  },
			{ com = constants.AnimComPushInt; param = 50 },
			{ com = constants.AnimComJumpRandom; param = 12 },
			{  },
			{  },
			{  },
			{  },
			{  },
			{  },
			{ com = constants.AnimComPushInt; param = 128 },
			{ com = constants.AnimComJumpRandom; param = 21 },
			{  },
			{  },
			{  },
			{ dur = 100; num = 18 },
			{ com = constants.AnimComRealH; param = 77 },	
			{ dur = 100; num = 19 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ dur = 0; num = 20; com = constants.AnimComCreateParticles; txt = "pblood_gravity"; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ dur = 0; num = 20; com = constants.AnimComCreateParticles; txt = "pblood_gravity2" },
			{ param = function( obj )
				local pos = obj:aabb().p
				Game.AddCombo( 1 )
				Info.RevealEnemy( "BTARD" )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "btard2", { 37, 40 } ) 
				Game.CreateScoreItems( 1, pos.x, pos.y )
			end },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ dur = 100; num = 20; com = constants.AnimComCreateEnemy; txt = "btard_corpse"; param = 8 },
			{ dur = 100; num = 21 },
			{ com = constants.AnimComRealH; param = 75 },
			{ dur = 100; num = 22 },
			{ com = constants.AnimComRealH; param = 68 },
			{ dur = 100; num = 23 },
			{ com = constants.AnimComRealH; param = 55 },
			{ dur = 100; num = 24 },
			{ com = constants.AnimComPushInt; param = -128 },
			{ com = constants.AnimComPushInt; param = -77 },
			{ com = constants.AnimComEnvSound; },
			{ com = constants.AnimComEnvSound; param = 1 },
			{ com = constants.AnimComRealH; param = 30 },
			{ dur = 100; num = 25, com = constants.AnimComCreateObject; txt = "dust-land" },
			{ com = constants.AnimComRealH; param = 25 },
			{ dur = 100; num = 26 },
			{ com = constants.AnimComPushInt; param = -128 },
			{ com = constants.AnimComPushInt; param = -64 },
			{ com = constants.AnimComRealH; param = 21 },
			{ dur = 5000; num = 27 },
			{ com = constants.AnimComPushInt; param = 400; num = 27 },
			{ com = constants.AnimComPushInt; param = 300; num = 27 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 53; num = 27 }
		}
	},
	{
		name = "attack";
		frames =
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 34 },
			{ com = constants.AnimComRealH; param = 81 },
			{ com = constants.AnimComFaceTarget },
			{ dur = 50; num = 11 },
			{ com = constants.AnimComRealW; param = 68 },
			{ com = constants.AnimComRealH; param = 78 },
			{ dur = 50; num = 12 },
			{ com = constants.AnimComRealH; param = 77 },
			{ dur = 50; num = 13 },
			{ com = constants.AnimComRealH; param = 69 },
			{ com = constants.AnimComPushInt; param = 50 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPlaySound; txt = "btard-swish.ogg"; param = 0 },
			{ dur = 150; num = 14; com = constants.AnimComCreateEnemyBullet; txt = "btard-punch" },
			{ com = constants.AnimComRealH; param = 68 },
			{ dur = 150; num = 15 },
			{ com = constants.AnimComRealH; param = 78 },
			{ dur = 400; num = 16 }, --����� �������
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{ 
		-- ��������
		name = "land";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComEnvSound; },
			{ com = constants.AnimComPushInt; param = -128 },
			{ com = constants.AnimComPushInt; param = -56 },
			{ com = constants.AnimComCreateObject; txt = "dust-land" },
			{ param = function(obj)
				if not mapvar.tmp[obj] then
					mapvar.tmp[obj] = { landed = false }
				end
				mapvar.tmp[obj].landed = true
				if not obj:target() then
					SetObjAnim( obj, "idle", false )
				end
			end },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComWaitForTarget; param = 30000; txt = "move" },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{
		name = "target_dead";
		frames =
		{
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{ 
		-- ��������
		name = "follow";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 11 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = 38 },
			{ com = constants.AnimComEnvSound; param = 1},
			{ dur = 100; num = 5; com = constants.AnimComMoveToTargetX; param = 250 },
			{ dur = 0; com = constants.AnimComWaitForEnemy; param = 3000; txt = "move" },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = 38 },
			{ com = constants.AnimComRealX; param = 4 },
			{ dur = 100; num = 6; com = constants.AnimComMoveToTargetX; param = 250 },
			{ dur = 0; com = constants.AnimComWaitForEnemy; param = 3000; txt = "move" },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = 38 },
			{ com = constants.AnimComRealX; param = 6 },
			{ dur = 100; num = 7; com = constants.AnimComMoveToTargetX; param = 250 },
			{ dur = 0; com = constants.AnimComWaitForEnemy; param = 3000; txt = "move" },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = 38 },
			{ com = constants.AnimComRealX; param = 18 },
			{ com = constants.AnimComEnvSound; },
			{ dur = 100; num = 8; com = constants.AnimComMoveToTargetX; param = 250 },
			{ dur = 0; com = constants.AnimComWaitForEnemy; param = 3000; txt = "move" },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = 38 },
			{ com = constants.AnimComRealX; param = 12 },
			{ dur = 100; num = 9; com = constants.AnimComMoveToTargetX; param = 250 },
			{ dur = 0; com = constants.AnimComWaitForEnemy; param = 3000; txt = "move" },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = 38 },
			{ com = constants.AnimComRealX; param = 4 },
			{ dur = 100; num = 10; com = constants.AnimComMoveToTargetX; param = 250 },
			{ dur = 0; com = constants.AnimComWaitForEnemy; param = 3000; txt = "move" },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComJumpIfTargetClose; param = 38 },
			{ com = constants.AnimComLoop },
			{ com = constants.AnimComRealX; param = 1 },
			{ dur = 100; num = 0; com = constants.AnimComWaitForEnemy; param = 3000; txt = "move" },
			{ com = constants.AnimComRealX; param = 1 },
			{ dur = 100; num = 1; com = constants.AnimComWaitForEnemy; param = 3000; txt = "move"; },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 100; num = 2; com = constants.AnimComWaitForEnemy; param = 3000; txt = "move" },
			{ dur = 100; num = 3; com = constants.AnimComWaitForEnemy; param = 3000; txt = "move"},
			{ dur = 100; num = 4; com = constants.AnimComWaitForEnemy; param = 3000; txt = "move"},
			{ com = constants.AnimComLoop },
		}
	},
	{
		name = "final_count";
		frames = 
		{
			{ com = constants.AnimComInitW; param = 36 },
			{ com = constants.AnimComInitH; param = 75 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComControlledOverlayColor },
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComSetShadow; param = 0 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetTrajectory; param = constants.pttLine },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComRealX; param = 11 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 75 },
			{ dur = 80; num = 5 },
			{ com = constants.AnimComRealX; param = 4 },
			{ dur = 80; num = 6 },
			{ com = constants.AnimComRealX; param = 6 },
			{ dur = 80; num = 7 },
			{ com = constants.AnimComRealX; param = 18 },
			{ dur = 80; num = 8 },
			{ com = constants.AnimComRealX; param = 12 },
			{ dur = 80; num = 9 },
			{ com = constants.AnimComRealX; param = 4 },
			{ dur = 80; num = 10 },
			{ com = constants.AnimComJump, param = 16},
		}
	},
}
