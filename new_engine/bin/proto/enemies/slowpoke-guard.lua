offscreen_distance = 640
offscreen_behavior = constants.offscreenDestroy

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 5;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 1;

drops_shadow = 1;

faction_id = 1;
faction_hates = { -1, -2 };

gravity_x = 0;
gravity_y = 0.8;

mass = -1;

-- �������� �������

texture = "slowpoke";

z = -0.02;

local speed = 200;
local labels

animations, labels = WithLabels
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 86; },
			{ com = constants.AnimComRealH; param = 46; },
			{ com = constants.AnimComRealX; },
			{ com = constants.AnimComRealY; },
			{ com = constants.AnimComSetHealth; param = 60; },
			{ com = constants.AnimComSetTouchable; param = 1; },
			{ com = constants.AnimComSetAnim; txt = "idle"; }
		}
	},
	{
		name = "final_count";
		frames = 
		{

			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ com = constants.AnimComRealW; param = 86; },
			{ com = constants.AnimComRealH; param = 71; },
			{ com = constants.AnimComRealX; },
			{ com = constants.AnimComRealY; },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ dur = 100; num = 11 }
		}
	},
	{
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 86; },
			{ com = constants.AnimComRealH; param = 46; },
			{ dur = 100; com = constants.AnimComWaitForTarget; param = 30000; txt = "shoot"; num = 8 },
			{ com = constants.AnimComLoop; }
		}
	},
	{
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPop; },
			{ com = constants.AnimComReduceHealth; },
			{ param = function(this) Game.FlashObject( this ) end },
			{ com = constants.AnimComPushInt; },
			{ com = constants.AnimComPushInt; },
			{ com = constants.AnimComPushInt; },
			{ com = constants.AnimComCreateParticles; param = 2; txt = "pblood_gravity_small"; },
			{ com = constants.AnimComRecover }
		}
	},
	{
		name = "shoot";
		frames = 
		{
			{ dur = 100; num = 8; com = constants.AnimComRealH; param = 46; },	
			{ com = constants.AnimComRealH; param = 51; },				
			{ dur = 100; num = 9; com = constants.AnimComFaceTarget; },		
			{ com = constants.AnimComFaceTarget; },					
			{ dur = 1500; num = 10; com = constants.AnimComRealH; param = 72; },	
			{ dur = 100; num = 11; com = constants.AnimComRealH; param = 71; },	
			{ dur = 100; num = 12; com = constants.AnimComRealH; param = 70; },	
			{ com = constants.AnimComRealH; param = 70; },				
			{ com = constants.AnimComPushInt; param = 60 },				
			{ com = constants.AnimComAdjustAim; param = 5 },			
			{ com = constants.AnimComPushInt; },					
			{ com = constants.AnimComPushInt; param = -10; },			
			{ com = constants.AnimComRealX; param = -3 },				
			{ com = constants.AnimComFaceTarget },
			{ com = constants.AnimComJump; param = function(obj)			
				local cx, cy = GetCamPos()
				local pos = obj:aabb()
				if not ObjectOnScreen( obj ) then
					return labels.FIRE1
				end
				return labels.SKIP_FIRE1
			end },
			{ label = "FIRE1" },
			{ dur = 100; num = 13; com = constants.AnimComAimedShot; txt = "pear15-slowpoke-projectile"; }, --14
			{ label = "SKIP_FIRE1" },
			{ com = constants.AnimComSetTouchable; param = 1 },			
			{ com = constants.AnimComRealX; param = 0 },				
			{ dur = 100; num = 11; com = constants.AnimComRealH; param = 71; },	
			{ dur = 100; num = 12; com = constants.AnimComRealH; param = 70; },	
			{ com = constants.AnimComRealH; param = 70; },				
			{ com = constants.AnimComPushInt; param = 60 },				
			{ com = constants.AnimComAdjustAim; param = 5 },			
			{ com = constants.AnimComPushInt; },					
			{ com = constants.AnimComPushInt; param = -10; },			
			{ com = constants.AnimComRealX; param = -3 },				
			{ com = constants.AnimComFaceTarget },
			{ com = constants.AnimComJump; param = function(obj)			
				local cx, cy = GetCamPos()
				local pos = obj:aabb()
				if not ObjectOnScreen( obj ) then
					return labels.SKIP_FIRE2
				end
				return labels.FIRE2
			end },
			{ label = "FIRE2" },
			{ dur = 100; num = 13; com = constants.AnimComAimedShot; txt = "pear15-slowpoke-projectile"; }, --26
			{ label = "SKIP_FIRE2" },
			{ com = constants.AnimComSetTouchable; param = 1 },			
			{ com = constants.AnimComRealX; param = 0 },				
			{ dur = 100; num = 11; com = constants.AnimComRealH; param = 71; },	
			{ dur = 100; num = 12; com = constants.AnimComRealH; param = 70; },	
			{ com = constants.AnimComRealH; param = 70; },				
			{ com = constants.AnimComPushInt; param = 60 },				
			{ com = constants.AnimComAdjustAim; param = 5 },			
			{ com = constants.AnimComPushInt; },					
			{ com = constants.AnimComPushInt; param = -10; },			
			{ com = constants.AnimComRealX; param = -3 },				
			{ com = constants.AnimComFaceTarget },
			{ com = constants.AnimComJump; param = function(obj)			
				local cx, cy = GetCamPos()
				local pos = obj:aabb()
				if not ObjectOnScreen( obj ) then
					return labels.SKIP_FIRE3
				end
				return labels.FIRE3
			end },
			{ label = "FIRE3" },
			{ dur = 100; num = 13; com = constants.AnimComAimedShot; txt = "pear15-slowpoke-projectile"; }, --38
			{ label = "SKIP_FIRE3" },
			{ com = constants.AnimComSetTouchable; param = 1 },			
			{ com = constants.AnimComRealX; param = 0 },				
			{},									
			{ com = constants.AnimComSetTouchable; param = 1 },			
			{ dur = 100; num = 11; com = constants.AnimComRealH; param = 70; },	
			{ com = constants.AnimComJump; param = 3 },				
		}
	},
	{
		name = "jump";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "reinit"; }
		}
	},
	{
		name = "touch";
		frames = 
		{
			{ param = function( obj )
				local toucher = GetObjectUserdata( ObjectPopInt( obj ) )
				DamageObject( toucher, 10 )
				Game.touch_push( obj, toucher )
			end },
			{ com = constants.AnimComSetTouchable; param = 0; num = 8 },
			{ com = constants.AnimComRecover }
		}
	},
	{
		name = "die";
		frames = 
		{
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 300 },
			{ com = constants.AnimComSetGravity }, 
			{ com = constants.AnimComStop },
			{ com = constants.AnimComSetTouchable; param = 0; },
			{ com = constants.AnimComSetShadow; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity2", param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pblood_gravity", param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pmeat", param = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0; },
			{ com = constants.AnimComPlaySound; txt = "slowpoke-death.ogg" },
			{ param = function(obj)
				local pos = obj:aabb().p
				Game.AddCombo(1)
				Info.RevealEnemy( "SLOWPOKE_GUARD" )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "slowpoke-guard", { 42, 35 } ) 
				Game.CreateScoreItems( 6, pos.x, pos.y )
			end },
			{ com = constants.AnimComRealX; param = 11; },
			{ dur = 100; num = 17; com = constants.AnimComRealH; param = 45; },
			{ com = constants.AnimComRealX; param = 15; },
			{ dur = 100; num = 18; com = constants.AnimComRealH; param = 46; },
			{ com = constants.AnimComRealX; param = 20; },
			{ dur = 100; num = 19; com = constants.AnimComRealH; param = 58; },
			{ com = constants.AnimComRealX; param = 28; },
			{ dur = 100; num = 20; com = constants.AnimComRealH; param = 48; },
			{ dur = 100; num = 21; com = constants.AnimComRealH; param = 30; },
			{ dur = 100; num = 22; com = constants.AnimComRealH; param = 22; },
			{ com = constants.AnimComSetZ; param = -450 },
			{ label = "KEEP_CORPSE" },
			{ dur = 5000; num = 23; com = constants.AnimComRealH; param = 10; },
			{ com = constants.AnimComPushInt; param = 320 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ dur = 1; num = 23; com = constants.AnimComJumpIfCloseToCamera; param = "KEEP_CORPSE" },
			{ num = 23 }
		}
	},
	{
		name = "land";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "reinit"; }
		}
	},
	{
		name = "reinit";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 86; },
			{ com = constants.AnimComRealH; param = 46; },
			{ com = constants.AnimComRealX; },
			{ com = constants.AnimComRealY; },
			{ com = constants.AnimComSetAnim; txt = "shoot"; }
		}
	},
	{
		name = "target_dead";
		frames = 
		{
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
}

