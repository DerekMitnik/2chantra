--forbidden
name = "exracer";
facing = constants.facingFixed

--main_weapon = "sfg9500";

air_control = 1;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 6;
phys_max_y_vel = 40;
phys_jump_vel = 20;
phys_walk_acc = 2;
phys_one_sided = 0;
mp_count = 1;
drops_shadow = 1;
--gravity_x = 20;
gravity_x = 5;
--gravity_y = 0;

health = 100;

FunctionName = "CreateEnemy";

trajectory_type = constants.pttCosine;
trajectory_param1 = 0.05;
trajectory_param2 = 0.05;

-- �������� �������

texture = "exracer";

z = -0.5;

--image_width = 1024;
--image_height = 256;
--frame_width = 1;
--frame_height = 1;
--frames_count = 28;
animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComSetHealth; param = 40 },
			{ com = constants.AnimComSetHealth; param = 100 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 29 },
			{ com = constants.AnimComRealY; param = 95 },
			{ com = constants.AnimComRealW; param = 80 },
			{ com = constants.AnimComRealH; param = 18 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComMPSet; param = 0; },
			{ dur = 100; num = 0},
			{ dur = 100; num = 1},
			{ dur = 100; num = 2},
			{ com = constants.AnimComLoop }
		}
	},
}



