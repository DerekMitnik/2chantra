texture = "2011lab3";
facing = constants.facingFixed

z = -0.8;

physic = 1;
phys_solid = 1;
phys_one_sided = 0;
phys_bullet_collidable = 1;
phys_ghostlike = 0;
ghost_to = constants.physSprite;

mass = -1;

phys_one_sided = 0;

phys_max_x_vel = 0;
phys_max_y_vel = 0;

gravity_x = 0;
gravity_y = 0;

faction_id = 3;

isometric_depth_x = 0.001;
isometric_depth_y = 0.001;

local group_size = 60

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealH; param = 40 },
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealX; param = 12 },
			{ com = constants.AnimComRealY; param = 0 },
			{ param = function( obj, num )
				local num = 1
				local pos = obj:aabb().p
				if mapvar.tmp.destr_walls then
					local found_group = false
					for k, v in pairs( mapvar.tmp.destr_walls ) do
						if (pos.x + group_size >= v[1] and pos.x - group_size <= v[3]) and 
                           			   (pos.y + group_size >= v[2] and pos.y - group_size <= v[4]) then
							found_group = true
							mapvar.tmp.destr_walls[k] = { math.min( v[1], pos.x - group_size ), math.min( v[2], pos.y - group_size ), 
							                              math.max( v[3], pos.x + group_size ), math.max( v[4], pos.y + group_size ) }
							num = k
						end
					end
					if not found_group then
						num = #mapvar.tmp.destr_walls + 1
						mapvar.tmp.destr_walls[num] = { pos.x - group_size, pos.y - group_size, pos.x + group_size, pos.y + group_size }
					end
				else
					mapvar.tmp.destr_walls = { { pos.x - group_size, pos.y - group_size, pos.x + group_size, pos.y + group_size } }
				end
				mapvar.tmp[obj] = num
				if not mapvar.tmp.destruction then
					mapvar.tmp.destruction = {}
				end
				if not mapvar.tmp.destruction[num] then
					mapvar.tmp.destruction[num] = {}
				end
				table.insert( mapvar.tmp.destruction[num], obj )
			end },
			{ com = constants.AnimComSetHealth; param = 400 },
			{ com = constants.AnimComSetAnim; txt = "idle" },
		}
	},
	{
		name = "idle";
		frames =
		{
			{ dur = 1; num = 49 }
		}
	},
	{
		name = "80%";
		frames =
		{
			{ dur = 1; num = 77 } 
		}
	},
	{
		name = "60%";
		frames =
		{
			{ dur = 1; num = 78 } 
		}
	},
	{
		name = "40%";
		frames =
		{
			{ dur = 1; num = 79 }                                      
		}
	},
	{
		name = "pre20%";
		frames =
		{
			{ param = function( obj ) 
				obj:solid_to(0) obj:solid(false) 
				obj:sprite_z( -0.8 )
				--obj:sprite_isometric_depth( { x = 0, y = 0 } )
			end; num = 80 },
--			{ com = constants.AnimComCreateParticles; txt = "psewerstone" },
			{ com = constants.AnimComCreateParticles; txt = "pdust" },
			{ com = constants.AnimComSetAnim; txt = "20%" },
		}
	},
	{
		name = "20%";
		frames =
		{
			{ dur = 1; num = 80 }                                      
		}
	},
	{
		name = "beam";
		frames =
		{
			{ param = function ( obj )
				obj:health( 1 )
				local num = mapvar.tmp[ obj ]
				for k, v in pairs( mapvar.tmp.destruction[num] ) do
					v:health( 1 )
					SetObjAnim( v, "pre20%", false )
				end
			end }
		}
	},
	{
		name = "pain";
		frames =
		{
			{ com = constants.AnimComJumpIfIntEquals; param = Game.damage_types.super_laser; txt = "beam" },
			{ com = constants.AnimComPop },   
			{ com = constants.AnimComReduceHealth },
			{ param = function ( obj )
				local health = obj:health()
				local pos = obj:aabb().p
				local num = mapvar.tmp[ obj ]
				if health < 0 then
					return
				elseif health < 100 then
					for k, v in pairs( mapvar.tmp.destruction[num] ) do
						v:health( health )
						SetObjAnim( v, "pre20%", false )
					end
				elseif health < 200 then
					for k, v in pairs( mapvar.tmp.destruction[num] ) do
						v:health( health )
						SetObjAnim( v, "40%", false )
					end
				elseif health < 300 then
					for k, v in pairs( mapvar.tmp.destruction[num] ) do
						v:health( health )
						SetObjAnim( v, "60%", false )
					end
				elseif health < 400 then
					for k, v in pairs( mapvar.tmp.destruction[num] ) do
						v:health( health )
						SetObjAnim( v, "80%", false )
					end
				else
					for k, v in pairs( mapvar.tmp.destruction[num] ) do
						v:health( health )
						SetObjAnim( v, "idle", false )
					end
				end
			end }
		}
	},
}
