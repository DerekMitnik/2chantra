--forbidden
bounce = 0;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 5;
phys_max_y_vel = 5;
phys_jump_vel = 20;
phys_walk_acc = 3;
mp_count = 1;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

mass = -1; --���������� ������� �����.

drops_shadow = 1;
shadow_width = 0.1;

-- �������� �������

texture = "pear15bigmanpac";

z = -0.001;

local speed = 300;
local max_health = 1600;

--1 2
--3 4

local STATE =
{
	IDLE = 0,
	MOVE = 1,
	SWEEP_LOWER_RETREAT = 2,
	SWEEP_LOWER = 3,
	SWEEP_UPPER_RETREAT = 4,
	SWEEP_UPPER = 5,
}
local waypoints
local attack_type
local state = STATE.MOVE

local dir = 0
local last_dir_change = 0
local last_shot = 0
local shooting_thread = 0

local last_update
local movement_t
local destination
local starting_point

local last_movement
local final_destination
local shifts

faction_id = 1;
faction_hates = { -1, -2 };

screen = { -256, 320+16-480, -256 + 640, 320+16 }

local boss_bar_bkg
local boss_bar

local retreat_thread
local orbitpacs_count

local function orbitpack_processor( obj, manpac, r, angle, angle_speed )
	mapvar.tmp.orbitpacs[obj] = true
	local ang = angle
	local last_pos = { x = 0, y = 0 }
	SetObjProcessor( obj, function( this )
		if this:health() <= 0 then
			orbitpacs_count = orbitpacs_count - 1
			mapvar.tmp.orbitpacs[obj] = nil
			SetObjProcessor( this, function() end )
			return
		end
		local pos = manpac:aabb().p
		local sin = math.sin(ang)
		pos.x = pos.x + r * math.cos(ang)
		pos.y = pos.y + r * sin
		ang = ang + angle_speed
		if sin < 0 then
			this:sprite_mirrored( angle_speed < 0 )
		else
			this:sprite_mirrored( angle_speed > 0 )
		end
		last_pos = pos
		SetObjPos( this, pos.x, pos.y )
	end )
end

local create_orbitpacs
local destroy_orbitpacs

local function manpac_processor( this )
	local pos = this:aabb()
	local vel = this:vel()
	if state == STATE.MOVE then
		movement_t = movement_t + (Loader.time - last_update) / 1000
		last_update = Loader.time
		if movement_t >= math.pi / 2 then 
			movement_t = math.pi / 2 
			state = STATE.IDLE
			last_movement = Loader.time
			if type( destination[3] ) == 'boolean' then
				this:sprite_mirrored( destination[3] )
				local shoot_x = iff( destination[1] < starting_point[1], screen[3] - 96, screen[1] + 96 )
				local shoot_y = waypoints[3][2]
				local angle = math.atan2( shoot_y - destination[2], shoot_x - destination[1] )
				if this:sprite_mirrored() then
					angle = math.pi + angle
				end
				CreateBullet("manpac_projectile", pos.p.x, pos.p.y, this:id(), this:sprite_mirrored(), angle * 180 / math.pi, 0 )
			end
		end
		SetObjPos( this, 
			   starting_point[1] + ( destination[1] - starting_point[1] ) * ( math.sin( movement_t ) + 1 ) / 2,
			   starting_point[2] + ( destination[2] - starting_point[2] ) * ( math.sin( movement_t ) + 1 ) / 2 )
--		this:vel( vel )
	elseif state == STATE.IDLE then
		--if Loader.time - last_movement > 2000 then
		if Loader.time - last_movement > 500 then
			shifts = shifts + 1
			if shifts < 5 then
				starting_point = destination
				final_destination = final_destination + 1
				if final_destination > 7 then final_destination = 5 end
				destination = waypoints[ final_destination ]
				movement_t = -math.pi/2
				last_update = Loader.time
				state = STATE.MOVE
				if destination[1] < starting_point[1] then
					SetObjSpriteMirrored( this, true )
				else
					SetObjSpriteMirrored( this, false )
				end
			else
				shifts = 0
				state = iff( math.random(1, 10) > 5, STATE.SWEEP_UPPER_RETREAT, STATE.SWEEP_LOWER_RETREAT )
			end
		end
		--[[if Loader.time - last_shot > 5000 then
			last_shot = Loader.time
			if shooting_thread then
				StopThread( shooting_thread )
			end
			shooting_thread = NewMapThread( function()
				for i = 1, 3 do
					local target = GetNearestEnemy( this )
					if target and target:health() > 0 then
						target = target:aabb().p
						if target.x < pos.p.x then
							this:sprite_mirrored( true )
						else
							this:sprite_mirrored( false )
						end
						local angle = math.atan2( target.y - pos.p.y, target.x - pos.p.x )
						if this:sprite_mirrored() then
							angle = math.pi + angle
						end
						CreateBullet("manpac_projectile", pos.p.x, pos.p.y, this:id(), this:sprite_mirrored(), angle * 180 / math.pi, 0 )
						Wait( 200 )
					end
				end
				shooting_thread = nil
			end )
			Resume( shooting_thread )
		end]]
	elseif state == STATE.SWEEP_UPPER_RETREAT then
		if pos.p.x + 3*pos.W > screen[1] then
			this:vel{ x = -3, y = 0 }
			this:sprite_mirrored( true )
		else 
			state = STATE.SWEEP_UPPER
			if orbitpacs_count == 0 then
				create_orbitpacs(this)
			end
			this:vel{ x = 0, y = 0 }
			SetObjPos( this, waypoints[1][1], waypoints[1][2] )
			this:sprite_mirrored( false )
		end
	elseif state == STATE.SWEEP_LOWER_RETREAT then
		if pos.p.x - 3*pos.W < screen[3] then
			this:vel{ x = 3, y = 0 }
			this:sprite_mirrored( false )
		else 
			state = STATE.SWEEP_LOWER
			if orbitpacs_count == 0 then
				create_orbitpacs(this)
			end
			this:vel{ x = 0, y = 0 }
			SetObjPos( this, waypoints[4][1], waypoints[4][2] )
			this:sprite_mirrored( true )
		end
	elseif state == STATE.SWEEP_UPPER then
		if pos.p.x - 3*pos.W < screen[3] then
			this:vel{ x = 10, y = 0 }
		else 
			this:sprite_mirrored( true )
			state = STATE.MOVE
			starting_point = waypoints[2]
			this:vel{ x = 0, y = 0 }
			final_destination = final_destination + 1
			if final_destination > 7 then final_destination = 5 end
			destination = waypoints[ final_destination ]
			movement_t = -math.pi / 2 
			last_update = Loader.time
		end
	elseif state == STATE.SWEEP_LOWER then
		if pos.p.x  + 3*pos.W > screen[1] then
			this:vel{ x = -10, y = 0 }
		else 
			this:sprite_mirrored( false )
			state = STATE.MOVE
			starting_point = waypoints[3]
			this:vel{ x = 0, y = 0 }
			final_destination = final_destination + 1
			if final_destination > 7 then final_destination = 5 end
			destination = waypoints[ final_destination ]
			movement_t = -math.pi / 2 
			last_update = Loader.time
		end
	elseif state == STATE_RETREAT then
		if pos.p.x  - 3*pos.W < screen[3] then
			if orbitpacs_count > 0 then
				destroy_orbitpacs()
			end
			this:sprite_mirrored( false )
			this:vel{ x = 6, y = 0 }
			this:touchable( false )
		elseif not retreat_thread then
			retreat_thread = NewMapThread( function()
					this:touchable( true )
					this:vel{ x = 0, y = 0 }
					this:sprite_mirrored( true )
					Wait( 1000 )
					for i = 1, 10 do
						CreateEnemy( "manpac-clockwise", screen[1] - iff( i % 2 == 0, 64, 96 ), screen[2] + i * 44 )
					end
					Wait( 4000 )
					create_orbitpacs( this )
					SetObjPos( this, waypoints[2][1], waypoints[2][2] )
					state = STATE.MOVE
					starting_point = waypoints[2]
					destination = waypoints[7]
					last_update = Loader.time
					movement_t = -math.pi/2
			end )
			Resume( retreat_thread )
		end
	end
end

local function manpac_touch( this )
	local toucher = GetObjectUserdata( ObjectPopInt( this ) )
	Game.touch_push( this, toucher )
	DamageObject( toucher, 40 )
	Resume( NewMapThread( function()
		Wait(200)
		if this:object_present() and this:health() >= 0 then
			this:touchable( true )
		end
	end ))
end

destroy_orbitpacs = function( obj )
	orbitpacs_count = 0
	if not mapvar.tmp.orbitpacs then
		mapvar.tmp.orbitpacs = {}
	end
	for k, v in pairs( mapvar.tmp.orbitpacs ) do
		if v and k:object_present() then
			DamageObject( k, 9000 )
		end
	end
end

create_orbitpacs = function( obj )
	destroy_orbitpacs()
	orbitpacs_count = 20
	local orbitpac
	for i = 1, 8 do
		orbitpac = GetObjectUserdata( CreateEnemy( "manpac-attached", -900, -900 ) )
		orbitpack_processor( orbitpac, obj, 96-16, 2*math.pi*(i/8), 0.01 )
	end
	for i = 1, 12 do
		orbitpac = GetObjectUserdata( CreateEnemy( "manpac-attached", -900, -900 ) )
		orbitpack_processor( orbitpac, obj, 128-16, 2*math.pi*(i/12), -0.015 )
	end
end

local function manpac_hurt( obj )
	local damage_type = ObjectPopInt( obj )
	local damage = ObjectPopInt( obj )
	if not ObjectOnScreen( obj ) then
		return
	end
	if state == STATE.RETREAT then
		return
	end
	local health = obj:health() - damage
	if health < max_health / 2 and not mapvar.tmp.phase_two then
		if shooting_thread then
			StopThread( shooting_thread )
		end
		mapvar.tmp.phase_two = true
		state = STATE.RETREAT
		halth = max_health / 2
	end
	WidgetSetSize( boss_bar, 243 * health/max_health, 11 )
	obj:health( health )
end

local function manpac_init( obj )
	waypoints =
	{
		{ -406, -34 },
		{  529, -34 },
		{ -406, 218 },
		{  529, 218 },
		{ -161, 16, false },
		{ 294, 30, true },
		{ 66, 195 },
	}
	retreat_thread = nil
	dir = 0
	last_dir_change = Loader.time
	last_shot = 0
	last_update = Loader.time
	shooting_thread = nil
	movement_t = -math.pi/2
	shifts = 0
	starting_point = waypoints[2]
	destination = waypoints[7]
	final_destination = 7
	last_update = Loader.time
	create_orbitpacs( obj )
	SetObjProcessor( obj, manpac_processor )
	SetObjSpriteMirrored( obj, true )
	
	boss_bar_bkg = CreateWidget( constants.wt_Picture, "", nil, CONFIG.scr_width / 2 - 137, CONFIG.scr_height - 3 * 21, 1, 1 )
	WidgetSetSprite( boss_bar_bkg, "pear15boss_bar", "background" )
	WidgetSetSpriteColor( boss_bar_bkg, { 0.8, 0.4, 0.4, 0.5 } )
	boss_bar = CreateWidget( constants.wt_Picture, "", nil, CONFIG.scr_width / 2 - 137 + 24, CONFIG.scr_height - 3 * 21 + 5, 243, 11 )
	WidgetSetSprite( boss_bar, "pear15boss_bar", "bar" )
	WidgetSetSpriteRenderMethod( boss_bar, constants.rsmCrop )
	WidgetSetSpriteColor( boss_bar, { 0.8, 0.4, 0.4, 0.5 } )
	Game.AddCleanup( function() DestroyWidget(boss_bar) DestroyWidget(boss_bar_bkg) end )
	mapvar.tmp.boss_object = obj
end

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComSetHealth; param = max_health },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComRealW; param = 128 },
			{ com = constants.AnimComRealH; param = 128 },
			{ dur = 1 },
			{ param = manpac_init },
			{ com = constants.AnimComSetAnim; txt = "move" }	
		}
	},
	{ 
		name = "pain";
		frames = 
		{
			{ param = manpac_hurt },
			{ com = constants.AnimComRecover; }
		}
	},
	{ 
		name = "touch";
		frames = 
		{
			{ param = manpac_touch },
			{ com = constants.AnimComRecover; }
		}
	},
	{ 
		name = "move";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 100; num = 0; },
			{ dur = 100; num = 1; },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 1; },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "die";
		frames =
		{
			{ com = constants.AnimComPlaySound; txt = "slowpoke-death.ogg" },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ param = destroy_orbitpacs },
			{ param = function( obj )
				local pos = obj:aabb().p
				CreateParticleSystem( "pblood_gravity", pos.x, pos.y )
				CreateParticleSystem( "pblood_gravity2", pos.x, pos.y )
				CreateParticleSystem( "pmeat", pos.x, pos.y )
			end },
			{ com = constants.AnimComInitW; param = 131 },
			{ com = constants.AnimComInitH; param = 132 },
			{ dur = 75; num = 3 },
			{ com = constants.AnimComInitW; param = 141 },
			{ com = constants.AnimComInitH; param = 147 },
			{ dur = 75; num = 4 },
			{ com = constants.AnimComInitW; param = 162 },
			{ com = constants.AnimComInitH; param = 166 },
			{ dur = 75; num = 5 },
			{ com = constants.AnimComInitW; param = 188 },
			{ com = constants.AnimComInitH; param = 207 },
			{ dur = 75; num = 6 },
			{ com = constants.AnimComInitW; param = 203 },
			{ com = constants.AnimComInitH; param = 209 },
			{ dur = 75; num = 7 },
			{ com = constants.AnimComInitW; param = 213 },
			{ com = constants.AnimComInitH; param = 219 },
			{ dur = 75; num = 8 },
			{ param  = function(obj)
				if Loader.level.boss_defeated then
					Loader.level.boss_defeated( obj )
				end
			end },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
