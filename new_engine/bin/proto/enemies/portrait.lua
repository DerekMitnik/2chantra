texture = "portraits";
facing = constants.facingFixed

z = -0.8;

physic = 1;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 1;

mass = -1;

phys_one_sided = 0;

phys_max_x_vel = 5;
phys_max_y_vel = 5;

gravity_x = 0;
gravity_y = 0;

faction_id = -28;
ghost_to = constants.physEverything - constants.physSprite

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 29 },
			{ com = constants.AnimComRealH; param = 42 },
			{ param = function( obj )
				if not mapvar.tmp.portraits then
					mapvar.tmp.portraits = {}
				end
				mapvar.tmp.portraits[obj] = true
			end },
			{ dur = 1; num = 29 },
			{ param = function( obj )
				local fraem = ObjectPopInt( obj )
				if fraem > 0 then
					return 0, nil, fraem
				else
					return 0, nil, math.random( 0, 27 )
				end
			end },
		}
	},
	{
		name = "fall";
		frames =
		{
			{ param = function( obj )
				obj:gravity( { x = 0, y = 0.8 } )
			end },
			{ com = constants.AnimComPlaySound; txt = "glass.ogg" },
			{ com = constants.AnimComRealW; param = 30 },
			{ com = constants.AnimComRealH; param = 46 },
			{ dur = 100; num = 30 },
			{ com = constants.AnimComJump; param = function( obj )
				return iff( obj:suspected_plane(), 5, 4 )
			end },
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 11 },
			{ dur = 100; num = 31 }
		}
	}
}
