parent = "enemies/house-dwall1l"

animations =
{
	{
		name = "idle";
		frames =
		{
			{ dur = 1; num = 238 }
		}
	},
	{
		name = "75%";
		frames =
		{
			{ dur = 1; num = 238 } 
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 238 }                                      
		}
	},
	{
		name = "25%";
		frames =
		{
			{ dur = 1; num = 332 }                                      
		}
	}
}
