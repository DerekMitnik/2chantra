--forbidden
name = "turret-wall";

FunctionName = "CreateEnemy";

-- �������� �������

texture = "turret-wall";

offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

z = -0.00001;
physic = 1;
phys_ghostlike = 1;
phys_bullet_collidable = 1;

physic = 1;
phys_solid = 1;

mass = -1;

faction_id = 1;
faction_hates = { -1, -2 };

mp_count = 1;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 32 },
			{ com = constants.AnimComSetHealth; param = 100*difficulty },
			{ com = constants.AnimComSetAnim; txt = "wait" }	
		}
	},
	{ 
		name = "wait";
		frames = 
		{
			{ dur = 1 },
			{ dur = 1; com = constants.AnimComWait },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		-- ��������
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComJumpIfIntEquals; param = Game.damage_types.force_alt; txt = "forcegun" },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ param = function(this) Game.FlashObject( this ) end },
			{ com = constants.AnimComRecover}
		}
	},
	{ 
		name = "forcegun";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComPop },
			{ com = constants.AnimComPushInt; param = 9000; },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComRecover }
		}
	},
	{ 
		-- ��������
		name = "idle";
		frames = 
		{
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 2 },
			{ com = constants.AnimComSetAnim; txt = "fire" }
		}
	},
	{
		name = "fire";
		frames =
		{
			{ dur = 100; num = 4 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 30 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 100; num = 5; com = constants.AnimComAngledShot; txt = "turret-wall-shot" },
			{ dur = 100; num = 6 },
			{ dur = 100; num = 4 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 30 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 100; num = 5; com = constants.AnimComAngledShot; txt = "turret-wall-shot" },
			{ dur = 100; num = 6 },
			{ dur = 100; num = 4 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 30 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 100; num = 5; com = constants.AnimComAngledShot; txt = "turret-wall-shot" },
			{ com = constants.AnimComSetAnim; txt = "reload" }
		}
	},
	{ 
		-- ��������
		name = "reload";
		frames = 
		{
			{ dur = 50; num = 6 },
			{ dur = 100; num = 7 },
			{ dur = 100; num = 8 },
			{ dur = 100; num = 9 },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{ 
		-- ��������
		name = "die";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion_big" },
			{ param = function( obj )
				Game.turret_destroyed( obj )
				obj:solid_to( 0 )
				obj:sprite_z( -0.4 )
				Info.RevealEnemy( "TURRET" )
				local pos = obj:aabb().p
				Game.AddCombo( 1 )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "turret-wall", { 39, 16 } ) 
				Game.CreateScoreItems( 3, pos.x, pos.y )
			end },
			{ com = constants.AnimComDestroyObject }	
		}
	},
	{
		name = "final_count";
		frames =
		{
			{ param = function( obj )
				local zero = { x = 0, y = 0 }
				obj:solid_to( 0 )
				obj:gravity( zero )
				obj:sprite_z( 1.05 )
				obj:vel( zero )
				obj:own_vel( zero )
				obj:acc( zero )
			end },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealH; param = 32 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 3 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 2 },
			{ com = constants.AnimComJump; param = 6 }
		}
	},
}
