texture = "crate";
facing = constants.facingFixed

z = -0.008;

physic = 1;
phys_solid = 1;
phys_one_sided = 0;
phys_bullet_collidable = 1;

mass = 5;

ghost_to = 16;

phys_one_sided = 0;

phys_max_x_vel = 5;
phys_max_y_vel = 20;

gravity_x = 0;
gravity_y = 0.8;

faction_id = 3;

isometric_depth_x = 0.0001;
isometric_depth_y = 0.0001;

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealH; param = 64 },
			{ com = constants.AnimComRealW; param = 64 },
			{ com = constants.AnimComRealX; param = 13 },
			{ com = constants.AnimComRealY; param = 13 },
			{ dur = 50; num = 2 },
			{ com = constants.AnimComSetAnim; txt = "idle" },
			{ com = constants.AnimComSetAnim; txt = "crate4"; param = 232 },
			{ com = constants.AnimComSetAnim; txt = "crate1"; param = 232 },
			{ com = constants.AnimComSetAnim; txt = "crate2"; param = 232 },
			{ com = constants.AnimComSetAnim; txt = "crate3" }
		}
	},
	{
		name = "crate1";
		frames =
		{
			{ dur = 50; num = 0 },
		}
	},
	{
		name = "crate2";
		frames =
		{
			{ dur = 50; num = 1 },
		}
	},
	{
		name = "crate3";
		frames =
		{
			{ dur = 50; num = 2 },
		}
	},
	{
		name = "crate4";
		frames =
		{
			{ dur = 50; num = 3 },
		}
	},
	{
		name = "pain";
		frames =
		{
			{ com = constants.AnimComRecover },
		}
	},
	{
		name = "idle",
		frames = 
		{
			{ com = constants.AnimComRealH; param = 64 },
			{ com = constants.AnimComRealW; param = 64 },
			{ com = constants.AnimComRealX; param = 13 },
			{ com = constants.AnimComRealY; param = 13 },
			{ dur = 50; num = 2 },
			{ param = function(crate)
					local cratex = crate:aabb().p.x
					local plane = crate:suspected_plane()

					if plane then
						local aabb = plane:aabb()
						local objxleft = aabb.p.x - aabb.W;
						local objxright = aabb.p.x + aabb.W;
						
						if cratex > objxright then
							SetObjAnim( crate:id(), "rollright", false );
						elseif cratex < objxleft then
							SetObjAnim( crate:id(), "rollleft", false );
						end
					end
				end
			},
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "rollright",
		frames = 
		{
			{ com = constants.AnimComRealH; param = 64 },
			{ com = constants.AnimComRealW; param = 64 },
			{ com = constants.AnimComRealX; param = 68 },
			{ com = constants.AnimComRealY; param = 68 },
			{ com = constants.AnimComSetVelX; param = 2000 },
			{ dur = 100; num = 6 },
			{ com = constants.AnimComRealX; param = 79 },
			{ com = constants.AnimComRealY; param = 60 },
			{ com = constants.AnimComSetVelX; param = 2000 },
			{ dur = 100; num = 5 },
			{ com = constants.AnimComRealX; param = 92 },
			{ com = constants.AnimComRealY; param = 50 },
			{ com = constants.AnimComSetVelX; param = 2000 },
			{ dur = 100; num = 4 },
			{ com = constants.AnimComAdjustY; param = 32 },
			{ com = constants.AnimComSetVelY; param = 5000 },			
			{ com = constants.AnimComSetAnim; txt = "idle" },
		}
	},
	{
		name = "rollleft",
		frames = 
		{
			{ com = constants.AnimComRealH; param = 64 },
			{ com = constants.AnimComRealW; param = 64 },
			{ com = constants.AnimComRealX; param = 92 },
			{ com = constants.AnimComRealY; param = 68 },
			{ com = constants.AnimComSetVelX; param = -2000 },
			{ dur = 100; num = 4 },
			{ com = constants.AnimComRealX; param = 81 },
			{ com = constants.AnimComRealY; param = 60 },
			{ com = constants.AnimComSetVelX; param = -2000 },
			{ dur = 100; num = 5 },
			{ com = constants.AnimComRealX; param = 66 },
			{ com = constants.AnimComRealY; param = 50 },
			{ com = constants.AnimComSetVelX; param = -2000 },
			{ dur = 100; num = 6 },
			{ com = constants.AnimComAdjustY; param = 32 },
			{ com = constants.AnimComSetVelY; param = 5000 },			
			{ com = constants.AnimComSetAnim; txt = "idle" },
		}
	}
}
