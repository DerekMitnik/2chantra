parent = "enemies/house-dwall1r"

animations =
{
	{
		name = "idle";
		frames =
		{
			{ dur = 1; num = 244 }
		}
	},
	{
		name = "75%";
		frames =
		{
			{ dur = 1; num = 244 } 
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 244 }                                      
		}
	},
	{
		name = "25%";
		frames =
		{
			{ dur = 1; num = 346 }                                      
		}
	}
}
