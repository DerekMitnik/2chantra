health = 100;

drops_shadow = 1;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
mp_count = 1;

gravity_x = 0;
gravity_y = 0.8;

faction_id = 1;
faction_hates = { -1, -2 };

-- �������� �������

texture = "pear15tank";

z = -0.1;

mass = -1

local stubborn = true

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 50 },
			{ com = constants.AnimComRealW; param = 148 },
			{ com = constants.AnimComRealH; param = 106 },
			{ com = constants.AnimComSetHealth; param = 9000 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ param = function() stubborn = true end },
			{ com = constants.AnimComMirror; }
		}
	},
	{ 
		name = "roll_in";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 148 },
			{ param = function(obj) obj:solid_to( 255 ) end },
			{ com = constants.AnimComRealH; param = 106 },
			{ com = constants.AnimComSetAccX; param = -2000 },
			{ dur = 100, num = 0 },
			{ dur = 100, num = 1 },
			{ dur = 100, num = 2 },
			{ dur = 100, num = 3 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "pain";
		frames =
		{
			{ param = function( obj )
				local pos = obj:aabb().p
				CreateParticleSystem( "pexplosion_sparks", pos.x, pos.y )
				local damage_type = ObjectPopInt( obj )
				local damage = ObjectPopInt( obj )
				if damage_type == Game.damage_types.ghost then
					damage = damage * 3000 / 80
					stubborn = false
				end
				local health = obj:health( obj:health() - damage )
				WidgetSetSize( mapvar.tmp.boss_bar, math.max( 1, 243 * health / 9000 ), 11 )
				if health <= 0 then
					obj:solid_to( 0 )
					obj:health( 1 )
					mapvar.tmp.boss_dead = true
					DestroyWidget( mapvar.tmp.boss_bar )
					DestroyWidget( mapvar.tmp.boss_bar_bkg )
					mapvar.tmp.boss_bar = nil
					mapvar.tmp.boss_bar_bkg = nil
					mapvar.tmp.boss_defeated = true
					if stubborn then
						Game.ProgressAchievement( "STUBBORN", 1 )
					end
					Resume( NewMapThread( function()
						local aabb = obj:aabb()
						for i = 1, 5 do
							CreateEnemy( "big_safe_explosion", math.random( aabb.p.x - aabb.W, aabb.p.x + aabb.W ), math.random( aabb.p.y - aabb.H, aabb.p.y + aabb.H ) )
							Wait( 300 )
						end
						SetObjDead( obj )
					end))
				else
					Game.FlashObject( obj )
				end
			end },
			{ com = constants.AnimComRecover },
		}
	},
	{
		name = "touch";
		frames = 
		{
			{ param = function( obj )
				local toucher = GetObjectUserdata( ObjectPopInt( obj ) )
				Game.touch_push( obj, toucher )
				DamageObject( toucher, 10 )
			end },
			{ com = constants.AnimComSetTouchable; param = 1; },
			{ com = constants.AnimComRecover }
		}
	},
	{
		name = "stop";
		frames =
		{
			{ com = constants.AnimComStop },
			{ dur = 100, num = 0 },
			{ dur = 100, num = 4 },
			{ dur = 100, num = 5 },
			{ dur = 100, num = 6 },
			{ com = constants.AnimComLoop }
		}
	}
}



