parent = "enemies/omich_runner_turn";

animations =
{
	{ 
		name = "move";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 36 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealH; param = 58 },
			{ com = constants.AnimComRealX; param = 3 },
			{ com = constants.AnimComRealY; param = 0 },
			{ num = 0; dur = 100 },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 1 },
			{ num = 1; dur = 100 },
			{ com = constants.AnimComRealY; param = 2 },
			{ num = 2; dur = 100 },
			{ com = constants.AnimComRealY; param = 1 },
			{ num = 3; dur = 100 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealX; param = 3 },
			{ num = 4; dur = 100 },
			{ com = constants.AnimComRealY; param = -1 },
			{ num = 5; dur = 100 },
			{ com = constants.AnimComRealY; param = -2 },
			{ com = constants.AnimComRealX; param = 5 },
			{ num = 6; dur = 100 },
			{ com = constants.AnimComRealY; param = -1 },
			{ com = constants.AnimComRealX; param = 3 },
			{ num = 7; dur = 100 },
			{ com = constants.AnimComStop },
			{ num = 1; dur = 500 },
			{ com = constants.AnimComRealY; param = -2 },
			{ num = 8; dur = 100 },
			{ com = constants.AnimComRealY; param = -1 },
			{ num = 9; dur = 100 },
			{ com = constants.AnimComRealY; param = 0 },
			{ num = 10; dur = 100 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ dur = 0; num = 9; com = constants.AnimComAngledShot; txt = "omich_projectile" },
			{ num = 1; dur = 500 },
			{ com = constants.AnimComSetRelativeAccX; param = 500 },
			{ com = constants.AnimComLoop }
		}
	},
}
