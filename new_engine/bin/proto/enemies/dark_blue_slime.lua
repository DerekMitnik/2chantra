parent = "enemies/blue_slime"
facing = constants.facingMoonwalking

offscreen_distance = 1280
color = {61/255, 104/255, 153/255, 1}
overlay = {0};
ocolor = {{ 1, 1, 1, 1}}

local sprite_iterator = 0
local sprite_iterator_direction = 1
local next_frame = function()
	sprite_iterator = sprite_iterator + sprite_iterator_direction
	if sprite_iterator == 3 or sprite_iterator == 0 then
		sprite_iterator_direction = -sprite_iterator_direction
	end
	return 0, nil, sprite_iterator
end

gravity_y = -0.8

animations = 
{
	{ 
		-- Создание
		name = "init";
		frames = 
		{
			{ param = function( obj )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
				SetObjSpriteAngle( obj, math.pi )
				mapvar.tmp[obj] = {}
				mapvar.tmp[obj].drop = false
			end },
			
			{ com = constants.AnimComPushInt; param = -22},
			{ com = constants.AnimComPushInt; param = 20},
			{ com = constants.AnimComMPSet; param = 1 },
			
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 50 * 39/25 },
			{ com = constants.AnimComRealH; param = 50 },
			{ com = constants.AnimComSetHealth; param = 10 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{
		name = "drop";
		frames = {
			{ com = constants.AnimComPushInt; param = 1},
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComPushInt; param = 0},
			{ com = constants.AnimComCreateEffect; txt = "slime_trail", param = 4 },
			
			{ param = function( obj )
				local children = GetChildren( obj )
				if not children then return end
				for k, v in pairs( children ) do
					mapvar.tmp[v] = 
					{
						start_color = { 61/255, 104/255, 153/255, 1 },
						end_color = { 61/255, 104/255, 153/255, 0 }
					}
				end
			end },
			
			{ com = constants.AnimComSetAnim; txt = "move" },
		}
	},
	{ 
		-- Создание
		name = "move";
		frames = 
		{
			{ param = function( obj )
				local plane = obj:suspected_plane()
				local o1 = obj:aabb()
				if mapvar.tmp[obj].drop then return end
				local target = obj:target()
				if target and obj:gravity().y < 0 then
					local o2 = target:aabb()
					if math.abs(o1.p.x - o2.p.x) <= 3*o1.W and o2.p.y > o1.p.y and o2.p.y - o1.p.y < 300 then
						mapvar.tmp[obj].drop = true
						obj:gravity( {x = 0, y = 0.8} )
						obj:facing( constants.facingNormal )
						if o1.p.x <= o2.p.x then
							SetObjSpriteMirrored( obj, false )
						else
							SetObjSpriteMirrored( obj, true )
						end
						SetObjSpriteAngle( obj, 0 )
						SetObjAnim( obj, "drop", false )
					end
				end
			end },
			{ com = constants.AnimComSetRelativeAccX; param = 450 },
			{ dur = 150; num = 0, param = next_frame },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "target_dead";
		frames =
		{
			{ com = constants.AnimComClearTarget },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{ 
		name = "die";
		frames = 
		{
			{ com = constants.AnimComStop; },
			{ param = function( obj )
				local x,y = GetCamPos()
				if obj:aabb().p.y + obj:aabb().H < y - CONFIG.scr_width / 2 and not mapvar.tmp[obj].drop then
					Game.ProgressAchievement( "SLIMES", 1 )
				end
			end },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "psewerslime3", param = 0 },
			{ com = constants.AnimComRealW; param = 40 * 67/25 },
			{ com = constants.AnimComRealH; param = 40 * 35/25 },
			{ com = constants.AnimComDestroyObject; param = 3 },
			{ num = 4; dur = 100 },
			{ num = 5; dur = 100 },
			{ num = 6; dur = 100 },
			{ param = function( obj )
				local pos = obj:aabb().p
				Game.AddCombo( 1 )
				--Info.RevealEnemy( info_handle )
				Info.RevealEnemy( "BLUE_SLIME" )
				Game.EnemyDead( obj:last_player_damage(), obj:last_player_damage_type(), "blue_slime", { 39, 25 } ) 
				Game.CreateScoreItems( 2, pos.x, pos.y )
			end },
			{ num = 7; dur = 100 },
			{ num = 8; dur = 100 },
			{ com = constants.AnimComDestroyObject }
		}
	},
}



