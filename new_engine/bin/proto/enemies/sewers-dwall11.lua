parent = "enemies/sewers-dwall1"

animations =
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealX; param = -16 },
			{ com = constants.AnimComRealY; param = -16 },
			{ dur = 1; num = 31 }
		}
	},
	{
		name = "75%";
		frames =
		{
			{ dur = 1; num = 32 } 
		}
	},
	{
		name = "50%";
		frames =
		{
			{ dur = 1; num = 33 }                                      
		}
	},
	{
		name = "25%";
		frames =
		{
			{ dur = 1; num = 35 }                                      
		}
	}
}
