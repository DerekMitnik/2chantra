--forbidden
parent = "enemies/manpac"

animations = 
{
	{ 
		name = "move_forward";
		frames = 
		{
			{ com = constants.AnimComSetVelX; param = 5000 },
			{ com = constants.AnimComPlaySound; txt = "pacman.ogg" },
			{ dur = 25; num = 0; com = constants.AnimComSetVelX; param = 5000 },
			{ dur = 25; num = 1; com = constants.AnimComSetVelX; param = 5000 },
			{ dur = 25; num = 2; com = constants.AnimComSetVelX; param = 5000 },
			{ dur = 25; num = 3; com = constants.AnimComSetVelX; param = 5000 },
			{ dur = 25; num = 4; com = constants.AnimComSetVelX; param = 5000 },
			{ dur = 25; num = 5; com = constants.AnimComSetVelX; param = 5000 },
			{ dur = 25; num = 6; com = constants.AnimComSetVelX; param = 5000 },
			{ dur = 25; num = 0; com = constants.AnimComSetVelX; param = 5000 },
			{ dur = 25; num = 1; com = constants.AnimComSetVelX; param = 5000 },
			{ dur = 25; num = 2; com = constants.AnimComSetVelX; param = 5000 },
			{ dur = 25; num = 3; com = constants.AnimComSetVelX; param = 5000 },
			{ dur = 25; num = 4; com = constants.AnimComSetVelX; param = 5000 },
			{ dur = 25; num = 5; com = constants.AnimComSetVelX; param = 5000 },
			{ dur = 25; num = 6; com = constants.AnimComSetVelX; param = 5000 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "shot";
		frames = {
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{ 
		name = "die";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = "pacman-die.ogg" },
			{ com = constants.AnimComCallFunction, txt = "manpac_kb_die" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 8000 },
			{ com = constants.AnimComSetGravity },
			{ com = constants.AnimComSetTouchable; param = 0 },
			{ com = constants.AnimComMapVarAdd; param = 15; txt = "score" },
			{ com = constants.AnimComMapVarAdd; param = 1; txt = "kills" },
			{ num = 7; dur = 25 },
			{ num = 8; dur = 25 },
			{ dur = 0 },
			{ num = 9; dur = 1 },
			{ com = constants.AnimComJumpIfOnPlane; param = 14 },
			{ com = constants.AnimComJump; param = 8 },
			{ dur = 0 },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ num = 10; dur = 25; },
			{ num = 11; dur = 25; },
			{ num = 12; dur = 25; },
			{ num = 13; dur = 500; },
			{ com = constants.AnimComPushInt; param = 320 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ dur = 1; num = 0; com = constants.AnimComJumpIfCloseToCamera; param = 19 },
			{ com = constants.AnimComDestroyObject }
		}
	}	
}



