parent = "enemies/small_orb";
texture = "small_orb_hw"

animations = 
{
	{ 
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 45 },
			{ com = constants.AnimComRealH; param = 45 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetNearestWaypoint },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 2+8+64 },
			{ com = constants.AnimComFlyToWaypoint; param = 500 },
			{ dur = 100; num = 0; com = constants.AnimComWaitForTarget; param = 320; txt = "waypoints_stop" },
			{ com = constants.AnimComLoop },
		}

	},
	{
		name = "waypoints_stop";
		frames =
		{
			{ com = constants.AnimComPushInt; param = 2+8+64 },
			{ com = constants.AnimComFlyToWaypoint; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "move" }
		}
	},
	{
		name = "die";
		frames =
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-safe" },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
