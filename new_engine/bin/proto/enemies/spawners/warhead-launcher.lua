name = "warhead-launcher";

if Editor then
	texture = "portrait-lolwut";
	renderMethod = constants.rsmStretch;
end

ghost_to = 0;

-- �������� �������

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 16 },
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComSetAnim; txt = "working" }	
		}
	},
	{ 
		name = "working";
		frames = 
		{
			{ param = function( this )
				local pos = this:aabb().p
				SetObjPos( CreateEnemy( "warhead-short", pos.x, pos.y ), pos.x, pos.y )
			end },
			{ dur = 5000 },
			{ com = constants.AnimComLoop }
		}
	}
}



