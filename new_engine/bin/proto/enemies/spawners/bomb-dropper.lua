--forbidden
name = "spawers/bomb-dropper";

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 0;
phys_max_x_vel = 50;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 1;

FunctionName = "CreateEnemy";

-- �������� �������

z = -0.1;

image_width = 1;
image_height = 1;
frame_width = 1;
frame_height = 1;
frames_count = 1;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComSetInvisible; param = 1 },
			{ com = constants.AnimComRealW; param = 1 },
			{ com = constants.AnimComRealH; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "working" }	
		}
	},
	{ 
		name = "working";
		frames = 
		{
			{ dur = 800 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "bomb"; param = 0 },
			{ com = constants.AnimComLoop }
		}
	},
	{ 
		name = "off";
		frames = 
		{
			{ dur = 800 },
			{ com = constants.AnimComLoop }
		}
	}

}



