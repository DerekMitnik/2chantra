--170, 62
name = "survival-land";

physic = 0;
phys_solid = 0;
phys_bullet_collidable = 0;
phys_max_x_vel = 5;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
phys_ghostlike = 1;

FunctionName = "CreateEnemy";

-- �������� �������

z = -0.009;

image_width = 1;
image_height = 1;
frame_width = 1;
frame_height = 1;
frames_count = 1;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 1 },
			{ com = constants.AnimComRealH; param = 1 },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "inactive" }	
		}
	},
	{
		name = "inactive";
		frames =
		{
			{ num = 0 }
		}
	},
	{ 
		name = "spawn-btard";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 1 },
			{ com = constants.AnimComRealH; param = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemy; txt = "btard-survival" },
			{ com = constants.AnimComSetAnim; txt = "inactive" }
		}
	},
	{ 
		name = "spawn-btard-ny";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 1 },
			{ com = constants.AnimComRealH; param = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemy; txt = "btard-ny" },
			{ com = constants.AnimComSetAnim; txt = "inactive" }
		}
	},
	{ 
		name = "spawn-btard-com";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 1 },
			{ com = constants.AnimComRealH; param = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemy; txt = "btard-com" },
			{ com = constants.AnimComSetAnim; txt = "inactive" }
		}
	},
	{ 
		name = "robot";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 1 },
			{ com = constants.AnimComRealH; param = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemy; txt = "btard" },
			{ com = constants.AnimComSetAnim; txt = "inactive" }
		}
	}
}



