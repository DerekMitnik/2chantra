--170, 62
name = "slowpoke-lift";

texture = "slowpoke-lift";

physic = 0;
phys_solid = 0;
phys_bullet_collidable = 0;
phys_max_x_vel = 5;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
phys_ghostlike = 1;

FunctionName = "CreateEnemy";

-- �������� �������

z = -0.009;

image_width = 1;
image_height = 1;
frame_width = 1;
frame_height = 1;
frames_count = 1;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 1 },
			{ com = constants.AnimComRealH; param = 1 },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 1 },
			{ com = constants.AnimComRealH; param = 1 },
			{ com = constants.AnimComPushInt; param = 320 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ dur = 1; num = 0; com = constants.AnimComJumpIfCloseToCamera; param = 6 },
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ dur = 50; num = 1 },
			{ dur = 50; num = 2 },
			{ dur = 150; num = 3 },
			{ dur = 50; num = 9 },
			{ dur = 50; num = 8 },
			{ dur = 50; num = 7 },
			{ dur = 50; num = 6 },
			{ dur = 50; num = 5 },
			{ dur = 50; num = 4 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 30 },
			{ com = constants.AnimComPushInt; param = 10 },
			{ com = constants.AnimComCreateEnemy; txt = "slowpoke" },
			{ dur = 2500; num = 0 },
			{ com = constants.AnimComLoop }
		}
	}
}



