name = "btard-com-spawner-left";

icon = "icon-btard-left";

physic = 0;
phys_solid = 0;
phys_bullet_collidable = 0;
phys_max_x_vel = 5;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 1;

FunctionName = "CreateEnemy";

-- �������� �������

z = -0.1;

image_width = 1;
image_height = 1;
frame_width = 1;
frame_height = 1;
frames_count = 1;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComSetInvisible; param = 1 },
			{ com = constants.AnimComRealW; param = 1 },
			{ com = constants.AnimComRealH; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "offscreen" }	
		}
	},
	{ 
		name = "offscreen";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 500 },
			{ com = constants.AnimComPushInt; param = 260 },
			{ dur = 1; num = 0; com = constants.AnimComJumpIfCloseToCameraLeft; param = 4 },
			{ com = constants.AnimComLoop },
			{ dur = 0 },
			{ com = constants.AnimComJumpIfObjectExists; param = 10 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 200 },
			{ com = constants.AnimComJumpRandom; param = 11 },
			{ com = constants.AnimComCreateEnemy; txt = "btard-com" },
			{ com = constants.AnimComSetAnim; txt = "onscreen" },
			{ com = constants.AnimComCreateEnemy; txt = "btard-com" },
			{ com = constants.AnimComSetAnim; txt = "onscreen" }
		}
	},
	{ 
		-- ��������
		name = "onscreen";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 400 },
			{ com = constants.AnimComPushInt; param = 410 },
			{ dur = 1; num = 0; com = constants.AnimComJumpIfCloseToCamera; param = 4 },
			{ com = constants.AnimComSetAnim; txt = "offscreen" },
			{ dur = 1 },
			{ com = constants.AnimComLoop }
		}
	}
}



