--forbidden
physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 5;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 1;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

drops_shadow = 1;

gravity_x = 0;
gravity_y = 0.8;

FunctionName = "CreateEnemy";

-- �������� �������

texture = "kb";

z = -0.002;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 70 },
			{ com = constants.AnimComRealH; param = 94 },
			{ com = constants.AnimComMirror; },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{ 
		-- ��������
		name = "idle";
		frames = 
		{
			{ dur = 200; num = 0 },
			{ dur = 50; num = 1 },
			{ dur = 50; num = 2 },
			{ dur = 50; num = 3 },
			{ dur = 100; num = 4 },
			{ dur = 50; num = 3 },
			{ dur = 50; num = 2 },
			{ dur = 50; num = 1 },
			{ com = constants.AnimComLoop }	
		}
	},
	{
		name = "shoot";
		frames =
		{
			{ com = constants.AnimComPop },
			{ num = 5; dur = 100 },
			{ num = 6; dur = 100 },
			{ num = 7; dur = 100 },
			{ num = 8; dur = 700 },
			{ num = 9; dur = 50; com = constants.AnimComPlaySound; param = 1; txt = "shot.ogg" },
			{ num = 10; dur = 50 },
			{ num = 8; dur = 700 }
		}
	}

}



