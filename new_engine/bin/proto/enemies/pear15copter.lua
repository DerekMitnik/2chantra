health = 100;

physic = 1;
phys_solid = 0;
phys_max_x_vel = 5;
phys_max_y_vel = 5;
mp_count = 2;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

drops_shadow = 1;

gravity_x = 0;
gravity_y = 0;


faction_id = 0;
faction_hates = { 1, 2 };

texture = "pear15copter";

z = -0.1;

overlay = {0, 1};
ocolor = {{0, 0, 0, 0}, {0, 0, 0, 0}}

shadow_width = 0.1;

ghost_to = constants.physEverything;

animations = 
{
	{ 
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 419 },
			{ com = constants.AnimComRealH; param = 224 },
			{ com = constants.AnimComPushInt; param = 42 },
			{ com = constants.AnimComPushInt; param = 32 },
			{ com = constants.AnimComMPSet; param = 1 },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEffect; txt = "pear15copter-door"; param = 5 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ num = 0 }
		}
	}
}



