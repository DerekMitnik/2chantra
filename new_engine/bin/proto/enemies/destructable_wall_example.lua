texture = "phys_floor";
facing = constants.facingFixed

z = -0.008;

physic = 1;
phys_solid = 1;
phys_one_sided = 0;
phys_bullet_collidable = 1;

mass = -1;

phys_one_sided = 0;

phys_max_x_vel = 0;
phys_max_y_vel = 0;

gravity_x = 0;
gravity_y = 0;

faction_id = 3;

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealH; param = 128 },             --[ Changing default size. ]
			{ com = constants.AnimComRealW; param = 128 },
			{ com = constants.AnimComSetHealth; param = 100 },         --[ Setting the starting health for our wall ]
			{ com = constants.AnimComSetAnim; txt = "idle" },
		}
	},
	{
		name = "idle";
		frames =
		{
			{ param = function( obj )
				SetObjSpriteColor( obj, {1, 1, 1, 1} ) 
				SetObjSpriteRenderMethod( obj, constants.rsmRepeatXY )
			end },
			{ dur = 1; num = 0 }                                       --[ Our normal, everyday sprite ]
		}
	},
	{
		name = "75%";
		frames =
		{
			{ param = function( obj )                                  --[ The wall is hit and now has less than 75% of its health left ]
				SetObjSpriteColor( obj, {1, .75, .75, 1} )         --[ Color change for demonstration purposes ]
				local pos = obj:aabb().p
				CreateParticleSystem( "pblood_gravity", pos.x, pos.y ) --[ Create some particles as "debris" ]
			end },
			{ dur = 1; num = 0 }                                       --[ This frame persists after the animation is done. We could use a sprite of a cracked wall here or something ]
		}
	},
	{
		name = "50%";
		frames =
		{
			{ param = function( obj )                                  --[ The wall is hit and now has less than 50% of its health left ]
				SetObjSpriteColor( obj, {1, .5, .5, 1} )           --[ Color change for demonstration purposes ]
				local pos = obj:aabb().p
				CreateParticleSystem( "pblood_gravity", pos.x, pos.y ) --[ Create some particles as "debris" ]
			end },
			{ dur = 1; num = 0 }                                       --[ This frame persists after the animation is done. We could use a sprite of a cracked wall here or something ]
		}
	},
	{
		name = "25%";
		frames =
		{
			{ param = function( obj )                                  --[ The wall is hit and now has less than 25% of its health left ]
				SetObjSpriteColor( obj, {1, .25, .25, 1} )         --[ Color change for demonstration purposes ]
				local pos = obj:aabb().p
				CreateParticleSystem( "pblood_gravity", pos.x, pos.y ) --[ Create some particles as "debris" ]
			end },
			{ dur = 1; num = 0 }                                       --[ This frame persists after the animation is done. We could use a sprite of the cracked wall here or something ]
		}
	},
	{
		name = "pain";
		frames =
		{
			{ com = constants.AnimComPop },                            --[ Pop incoming damage type from the stack. We don't need it in this example ]
			{ com = constants.AnimComReduceHealth },                   --[ Reduce wall health by the ammount of damage received ]
			{ param = function ( obj )
				local health = obj:health()                        --[ Get remaining health and go to appropriate animation ]
				if health < 0 then
					return
				elseif health < 25 then
					SetObjAnim( obj, "25%", false )
				elseif health < 50 then
					SetObjAnim( obj, "50%", false )
				elseif health < 75 then
					SetObjAnim( obj, "75%", false )
				else
					SetObjAnim( obj, "idle", false )
				end
			end }
		}
	},
	{
		-- The wall is completely destroyed
		name = "die";
		frames =
		{
			{ param = function( obj ) 
				local pos = obj:aabb().p
				CreateParticleSystem( "pmeat", pos.x, pos.y )
			end },
			{ com = constants.AnimComDestroyObject; param = 2 }       --[ This frame is not actually required but we better explicitly destroy the object and all its children anyway ]
		}
	}
}
