texture = "tsustruction";
facing = constants.facingFixed

z = -0.8;

physic = 1;
phys_solid = 1;

mass = -1;

phys_one_sided = 1;

phys_max_x_vel = 5;
phys_max_y_vel = 5;

gravity_x = 0;
gravity_y = 0;

faction_id = -28;
ghost_to = constants.physEverything - constants.physSprite - constants.physPlayer

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 70 },
			{ com = constants.AnimComRealH; param = 75 },
			{ param = function( obj )
				if not mapvar.tmp.portraits then
					mapvar.tmp.portraits = {}
				end
				mapvar.tmp.portraits[obj] = true
			end },
			{ dur = 1; num = 3 },
		}
	},
	{
		name = "fall";
		frames =
		{
			{ param = function( obj )
				obj:solid_to( 0 )
				obj:solid( false )
			end },
			{ com = constants.AnimComPlaySound; txt = "wood.ogg" },
			{ dur = 200; num = 4 },
			{ dur = 100; num = 5 }
		}
	}
}
