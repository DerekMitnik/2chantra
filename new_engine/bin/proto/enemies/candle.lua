texture = "big_mistake";
facing = constants.facingFixed

z = -0.7;

physic = 1;
phys_solid = 0;

mass = -1;

phys_one_sided = 0;

phys_max_x_vel = 5;
phys_max_y_vel = 5;

gravity_x = 0;
gravity_y = 0;

faction_id = -28;
ghost_to = constants.physEverything - constants.physSprite

animations = WithLabels
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 14 },
			{ com = constants.AnimComRealH; param = 36 },
			{ param = function( obj )
				mapvar.tmp.candle = obj
			end },
			{ label = "LOOP" },
			{ dur = 100; num = 2 },
			{ dur = 100; num = 3 },
			{ com = constants.AnimComJump; param = "LOOP" }
		}
	},
	{
		name = "fall";
		frames =
		{
			{ com = constants.AnimComRealX; param = 20 },
			{ param = function( obj )
				obj:gravity( { x = 0, y = 0.8 } )
			end },
			{ com = constants.AnimComRealW; param = 22 },
			{ com = constants.AnimComInitH; param = 47 },
			{ dur = 100; num = 4 },
			{ com = constants.AnimComJump; param = function( obj )
				return iff( obj:suspected_plane(), 5, 4 )
			end },
			{ com = constants.AnimComRealW; param = 28 },
			{ com = constants.AnimComRealH; param = 25 },
			{ param = function(obj)
				mapvar.tmp.fire = true
			end },
			{ dur = 100; num = 5 }
		}
	}
}
