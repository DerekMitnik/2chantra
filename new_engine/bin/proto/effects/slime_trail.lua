--forbidden

z = -0.11;
phys_ghostlike = 1;
effect = 1;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

animations = 
{
	{ 
		name = "idle";
		frames = 
		{
			{ dur = 1 },
			{ param = function( obj )
				local start_color = { 0, 0, 0, 1 }
				local end_color = { 0, 0, 0, 1 }
				local var_color = { 0, 0, 0, 0 }
				if mapvar.tmp[obj] then
					start_color = mapvar.tmp[obj].start_color or start_color
					end_color = mapvar.tmp[obj].end_color or end_color
					var_color = mapvar.tmp[obj].var_color or var_color
				end
				CreateParticleSystem( "psewerslime_trace", obj, 0, 0, 10, { start_color = start_color, end_color = end_color, var_color = var_color } )
			end },
			{ }
		}
	},
	
}
