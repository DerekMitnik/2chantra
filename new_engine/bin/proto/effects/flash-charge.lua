--texture = "bullets";

z = 0.0;
effect = 1;
color = { 1, 1, 1, .2 }

physic = 1;
phys_ghostlike = 1;

animations = 
{
	{ 
		name = "idle";
		frames = 
		{
--			{ com = constants.AnimComRealX; param = 38 },
--			{ com = constants.AnimComRealY; param = 6  },
--			{ com = constants.AnimComRealW; param = 11 },
--			{ com = constants.AnimComRealH; param = 11 },
--			{ dur = 50; num = 4 },
--			{ dur = 50; num = 5 },
--			{ dur = 50; num = 6 },
--			{ param = function( obj )
--				local parent = GetParent( obj )
--				if parent then
--					if parent:weapon_charge_time() > 1000 then
--						SetObjSpriteColor( obj, { 1, 1, 1, 1 } )
--					elseif parent:weapon_charge_time() > 500 then
--						SetObjSpriteColor( obj, { 1, 1, 1, .5 } )
--					end
--				end	
--			end },
			{ param = function( obj )
				Game.StartWeaponCharge( obj:parent(), obj )
			end }
--			{ com = constants.AnimComLoop }
		}
	}
}
