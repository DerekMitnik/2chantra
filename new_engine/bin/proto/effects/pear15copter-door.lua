texture = "pear15copter";

z = -0.09;
phys_ghostlike = 1;
phys_max_x_vel = 9000;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

effect = 1;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealW; param = 57 },
			{ com = constants.AnimComRealH; param = 96 },
			{ com = constants.AnimComSetHealth; param = 150 },
			{ param = function( obj )
				mapvar.tmp.pear15copter_door = obj
			end },
			{ dur = 1; num = 1 },
		}
	},
	{
		name = "open";
		frames =
		{
			{ com = constants.AnimComSetRelativeVelX; param = 1200; num = 1 },
			{ com = constants.AnimComSetVelY; param = 150; num = 1 },
			{ param = function( obj )
				local pos = obj:aabb().p
				local new_pos = obj:aabb().p
				Resume( NewMapThread( function()
					while new_pos.x < pos.x + 57 do
						Wait(1)
						new_pos = obj:aabb().p
					end
					obj:acc( obj:vel{ x = 0, y = 0 } )
				end ))
			end },
			{ num = 1 }
		}

	}
}
