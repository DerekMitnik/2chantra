texture = "boss_wd_1";

z = -0.95;
phys_ghostlike = 1;
physic = 1;
phys_bullet_collidable = 0;
phys_max_x_vel = 9000;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

ghost_to = constants.physEverything;

effect = 1;
ghost_to = constants.physBullet

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealW; param = 600 },
			{ com = constants.AnimComRealH; param = 100 },
			{ com = constants.AnimComSetHealth; param = 150 },
			{ dur = 1; num = 0 },
			{ com = constants.AnimComSetAnim; txt = "loop" }
		}
	},
	{
		name = "loop";
		frames =
		{
			{ dur = 100; num = 0; },
			{ dur = 100; num = 1; },
			{ dur = 100; num = 2; },
			{ dur = 100; num = 3; },
			{ dur = 100; num = 4; },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "loop2";
		frames =
		{
			{ dur = 100; num = 5; },
			{ dur = 100; num = 6; },
			{ dur = 100; num = 7; },
			{ dur = 100; num = 8; },
			{ dur = 100; num = 9; },
			{ com = constants.AnimComLoop }
		}
	},
}
