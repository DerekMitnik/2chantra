parent = "effects/boss_wdlhand";

texture = "boss_wdrhand";

animations = 
{
	{
		name = "down";
		frames =
		{
			{ dur = 50; num = 4; },
			{ com = constants.AnimComPushInt; param = 80 },
			{ com = constants.AnimComPushInt; param = 150 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "wd-punch"; param = 3 },
			{ dur = 50; num = 3; },
			{ param = function( obj )
				mapvar.tmp[obj]:solid_to( 0 )
			end },
			{ dur = 50; num = 2; },
			{ dur = 50; num = 1; },
			{ dur = 50; num = 0; },
			{ com = constants.AnimComPlaySound; txt = "weapons/grenade-explosion.ogg" },
			{ dur = 400; num = 0; },
			{ dur = 100; num = 1; },
			{ dur = 100; num = 2; },
			{ param = function( obj )
				mapvar.tmp[obj]:solid_to( constants.physEverything )
			end },
			{ dur = 100; num = 3; },
			{ dur = 100; num = 4; },
			{ com = constants.AnimComSetAnim; txt = "uploop" }
		}
	}
}
