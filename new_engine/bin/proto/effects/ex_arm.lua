--forbidden

z = -0.11;
phys_ghostlike = 1;
effect = 1;
--offscreen_distance = 640
--offscreen_behavior = constants.offscreenNone

mp_count = 1;

texture = "ex_arm"

animations = 
{
	{ 
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 1 },
			{ com = constants.AnimComRealY; param = 8 }, 
			{ com = constants.AnimComSetAnim; txt = "loop" },
		}
	},
	{ 
		name = "loop";
		frames = 
		{
			{ dur = 100; num = 0 },
		}
	},
	{ 
		name = "shoot";
		frames = 
		{
			{ com = constants.AnimComPushInt, param = 50 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100; num = 1; param = function(arm)
				local shooter = GetParent( arm )
				local mirror = arm:sprite_mirrored();
				local angle = arm:sprite_angle();
				local lx = 50;
				local ly = -3;
				local x = arm:aabb().p.x;
				local y = arm:aabb().p.y;
				if mirror then  
					x = x - lx*math.cos(angle) - ly*math.sin(angle);
					y = y - lx*math.sin(angle) + ly*math.cos(angle);
				else
					x = x + lx*math.cos(angle) - ly*math.sin(angle);
					y = y + lx*math.sin(angle) + ly*math.cos(angle);
				end
				--local flash = CreateEffect("flash-straight",x,y,arm:id(),0)
				local bul = GetObjectUserdata(CreateBullet("../weapons/pear15soh_primary", x, y, shooter:id(), mirror, 180/3.1415926*angle, 0));
			  end
			},
			--{ com = constants.AnimComPushInt; param = 50; },
			--{ com = constants.AnimComPushInt; param = 0; },
			--{ dur = 100; num = 1; com = constants.AnimComAimedShot; txt = "../weapons/pear15unyl_primary"; },
			--{ dur = 100; num = 1; com = constants.AnimComAimedShot; txt = "slowpoke-projectile"; },
			{ com = constants.AnimComSetAnim; txt = "idle" },
		}
	},
}
