--forbidden
name = "flash-straight-sfg5000";
texture = "flash-straight-sfg5000";
FunctionName = "CreateSprite";
physic = 1;
phys_ghostlike = 1;

z = 0.0;
image_width = 512;
image_height = 32;
frame_width = 128;
frame_height = 32;
effect = 1;
phys_ghostlike = 1;

animations = 
{
	{ 
		name = "idle";
		frames = 
		{
			{ param = function(obj)
				SetObjProcessor( obj, function( this )
					local parent = this:parent()
					if parent then
						SetObjSpriteAngle( this, parent:target_weapon_angle() )
					end
				end )
			end },
			{ com = constants.AnimComRealX; param = 64 },
			{ com = constants.AnimComRealY; param = -12 },
			{ com = constants.AnimComRealW; param = 1 },
			{ com = constants.AnimComRealH; param = 15 },
			{ com = constants.AnimComRealY; param = 7 },
			{ dur = 30; num = 0 },
			{ dur = 30; num = 1 },
			{ dur = 30; num = 2 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
