texture = "flash-straight";

physic = 1;
phys_ghostlike = 1;

z = 0.0;
effect = 1;
local dx = 0;
local dy = 12;

animations = 
{
	{ 
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 64 },
			{ com = constants.AnimComRealY; param = -12 },
			{ com = constants.AnimComRealW; param = 1 },
			{ com = constants.AnimComRealH; param = 15 },
			{ com = constants.AnimComRealY; param = 7 },
			{ dur = 1; num = 0; AnimComSetInvisible; param = 1 },
			{ com = constants.AnimComCustomWeapon; param = 0 },
			{ dur = 50; num = 0; AnimComSetInvisible; param = 0 },
			{ com = constants.AnimComPushInt; param = dx },
			{ com = constants.AnimComPushInt; param = dy },
			{ com = constants.AnimComCustomWeapon; param = 1 },
			{ dur = 50; num = 1 },
			{ dur = 50; num = 2 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
