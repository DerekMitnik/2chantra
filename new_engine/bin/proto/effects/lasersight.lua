--forbidden

z = -0.11;
phys_ghostlike = 1;
effect = 1;
offscreen_distance = 640
offscreen_behavior = constants.offscreenNone

texture = nil

animations = 
{
	{ 
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "loop" }
		}
	},
	{ 
		name = "loop";
		frames = 
		{
			{ dur = 50; num = 3 },
			{ param = function( obj )
				if not obj:parent():object_present() then return end
				local angle = obj:parent():weapon_angle();
				angle = -angle*180/3.1415926
				if obj:parent():sprite_mirrored() then
				angle = angle + 180
				end
				local bul = CreateRay( "lasersightscope", obj:parent():id(), angle, obj:aabb().p.x, obj:aabb().p.y )
			end },
			{ com = constants.AnimComLoop }
		}
	}
	
}
