--forbidden
reload_time = 0;
clip_reload_time = 121;
multiple_targets = 1;

shots_per_clip = 1;

is_ray_weapon = 1;
time_to_live = 1;

bullet_damage = 10;

texture = "laser-color";
flash = "flash-straight";

z = -0.002;

damage_type = Game.damage_types.laser;

hit_effect = "laser_collision_effect";
end_effect = "laser_collision_effect";

local sound_shoot = "weapons/laser.ogg"

local t = 50

overlay = {0}
color = { 160/255, 247/255, 1, 1 }
oclor = { {1,1,1,1} }

animations = 
{
	{
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComRealW; param =  32},
			{ com = constants.AnimComPlaySound; txt = sound_shoot; param = 1 },
			{ com = constants.AnimComRealY; param = 0 },			-- param = 3 ����� ��� �������� ����, ��, �������, ���� ��� ����
			{ dur = t; num = 0; com = constants.AnimComInitH; param = 5 },
			{ dur = t; num = 1 },
			{ dur = t; num = 2 },
			{ dur = t; num = 3 },
			{ dur = t; num = 4 },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		name = "die";
		frames = 
		{
			{ durr = hurr }
		}
	}
}
