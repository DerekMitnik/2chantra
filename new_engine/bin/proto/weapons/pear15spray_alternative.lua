--Scores ~124618 at weapontest

flash = "flash-straight";

reload_time = 550;
bullets_per_shot = 10;
damage_type = 2;

-- �������� ����
bullet_damage = 0;
bullet_vel = 0;
ghost_to = 255;

-- �������� ������� ����

recoil = 4;

z = -0.002;

-- ������ ���������, ����� ���� �����
local sound_shoot = "weapons/blaster_shot.ogg"

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = sound_shoot; param = 1 },
			{ dur = 1; num = 0; com = constants.AnimComInitH; param = 1 },
			{ com = constants.AnimComDestroyObject; param = function( obj )
				local shooter = GetParent( obj )
				local angle = shooter:weapon_angle() * (180/math.pi)
				local mirror = shooter:sprite_mirrored()
				local x, y = GetMP( shooter, 0 )
				local p = shooter:aabb().p
				if mirror then
					x = p.x - x
					y = p.y + y
				else
					x = p.x + x
					y = p.y + y
				end

				local bul
				for i=1,8 do
					local bul = GetObjectUserdata( CreateBullet( "pear15spray_bullet_alt", x, y, shooter:id(), mirror, angle + math.random( -30, 30 ), 0 ) )
					bul:player_num(obj:player_num())
					SetObjSpriteMirrored( bul, mirror )
				end

				return 0
			end }
		}
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		-- ����������� ����
		name = "die";
		frames = 
		{
			{ com = constants.AnimComDestroyObject }
		}
	}
}
