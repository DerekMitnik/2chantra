--forbidden
name = "flamer";

reload_time = 20;
bullets_per_shot = 0;
can_hit_multiple_targets = 1;


-- �������� ����
bullet_damage = 3;
bullet_vel = 2.5;

-- �������� ������� ����
texture = "flamer";

z = -0.001;

image_width = 256;
image_height = 32;
frame_width = 32;
frame_height = 32;
frames_count = 8;
z = -0.002;

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComPlaySound; txt = "weapons/flame.ogg"; param = 1 },
			{ com = constants.AnimComInitWH; param = 8 },
			{ com = constants.AnimComRealX; param = 16 },
			{ com = constants.AnimComRealY; param = 12 },
			{  },
			{ dur = 30; num = 0 },
			{ com = constants.AnimComRealX; param = 12 },
			{ com = constants.AnimComRealY; param = 12 },
			{  },
			{  },
			{ dur = 30; num = 1 },
			{ com = constants.AnimComRealX; param = 10 },
			{ com = constants.AnimComRealY; param = 12 },
			{  },
			{  },
			{ dur = 30; num = 2 },
			{ com = constants.AnimComRealX; param = 9 },
			{ com = constants.AnimComRealY; param = 12 },
			{  },
			{  },
			{ com = constants.AnimComPushInt; param = 128 },
			{ com = constants.AnimComJumpRandom; param = 31 },
			{ dur = 30; num = 3 },
			{ dur = 30; num = 4 },
			{ dur = 30; num = 5 },
			{ com = constants.AnimComRealX; param = 4 },
			{ com = constants.AnimComRealY; param = 13 },
			{  },
			{  },
			{ dur = 30; num = 6 },
			{ dur = 30; num = 7 },
			{ com = constants.AnimComDestroyObject },
			{ dur = 30; num = 5 },
			{ dur = 30; num = 4 },
			{ dur = 30; num = 3 },
			{ com = constants.AnimComRealX; param = 4 },
			{ com = constants.AnimComRealY; param = 13 },
			{  },
			{  },
			{ dur = 60; num = 6 },
			{ dur = 60; num = 7 },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComStop },
			{ com = constants.AnimComSetMaxVelY; param = 10000 },
			{ com = constants.AnimComRealX; param = 5 },
			{ com = constants.AnimComRealY; param = 7 },
			{ com = constants.AnimComRealW; param = 21 },
			{ com = constants.AnimComRealH; param = 19 },
			{ dur = 100; num = 8 },
			{ dur = 100; num = 9 },
			{ dur = 100; num = 10 },
			{ dur = 100; num = 11 },
			{ dur = 100; num = 8 },
			{ dur = 100; num = 9 },
			{ dur = 100; num = 10 },
			{ dur = 100; num = 11 },
			{ dur = 100; num = 8 },
			{ dur = 100; num = 9 },
			{ dur = 100; num = 10 },
			{ dur = 100; num = 11 },
			{ dur = 100; num = 8 },
			{ dur = 100; num = 9 },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		--�����������
		name = "die";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComStop },
			{ com = constants.AnimComSetMaxVelY; param = 10000 },
			{ com = constants.AnimComRealX; param = 5 },
			{ com = constants.AnimComRealY; param = 7 },
			{ com = constants.AnimComRealW; param = 21 },
			{ com = constants.AnimComRealH; param = 19 },
			{ dur = 100; num = 8 },
			{ dur = 100; num = 9 },
			{ dur = 100; num = 10 },
			{ dur = 100; num = 11 },
			{ dur = 100; num = 8 },
			{ dur = 100; num = 9 },
			{ dur = 100; num = 10 },
			{ dur = 100; num = 11 },
			{ dur = 100; num = 8 },
			{ dur = 100; num = 9 },
			{ dur = 100; num = 10 },
			{ dur = 100; num = 11 },
			{ dur = 100; num = 8 },
			{ dur = 100; num = 9 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
