--forbidden
name = "sfg9000";
flash = "flash-straight";

reload_time = 225;
bullets_per_shot = 0;
clip_reload_time = 150;
damage_type = 2;

hurts_same_type = 1;

-- �������� ����
bullet_damage = 20;
bullet_vel = 7;
--bullet_vel = 1;

-- �������� ������� ����
texture = "bullets";
overlay = {0};

push_force = 1.0;

z = -0.002;

image_width = 256;
image_height = 64;
frame_width = 64;
frame_height = 32;
frames_count = 7;

-- ������ ���������, ����� ���� �����
local sound_shoot = "weapons/blaster_shot.ogg"

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 148 },
			{ com = constants.AnimComPushInt; param = 160 },
			{ com = constants.AnimComPushInt; param = 148 },
			{ com = constants.AnimComPushInt; param = 160 },
			{ com = constants.AnimComPushInt; param = 168 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComControlledOverlayColor },
			{ com = constants.AnimComRealX; param = 38 },
			{ com = constants.AnimComRealY; param = 12 },
			{ com = constants.AnimComRealW; param = 11 },
			{ com = constants.AnimComPlaySound; txt = sound_shoot; param = 1 },
			{ dur = 1; num = 0; com = constants.AnimComRealH; param = 7 },
			{ dur = 100; num = 0; com = constants.AnimComRealH; param = 5 },
		}
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		-- ����������� ����
		name = "die";
		frames = 
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComRealX; param = 38 },
			{ com = constants.AnimComRealY; param = 12 },
			{ com = constants.AnimComRealW; param = 11 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ dur = 100; num = 4 },
			{ dur = 100; num = 5 },
			{ dur = 100; num = 6 },
			{ dur = 100; num = 7 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
