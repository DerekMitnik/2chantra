flash = "flash-straight";

reload_time = 500;
bullets_per_shot = 10;
clip_reload_time = 0;

-- �������� ����
bullet_damage = 65;
bullet_vel = 3;
damage_type = Game.damage_types.force_alt;
--bullet_vel = 1;

-- �������� ������� ����
texture = "gravity";
overlay = {0};
ocolor = {{1, 1, 1, 1}}

push_force = -15.0;
bounce = 1.0;

z = -0.002;

-- ������ ���������, ����� ���� �����
local sound_shoot = "weapons/forcegun_alt.ogg"

animations = 
{
	{
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 168 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComPushInt; param = 148 },
			{ com = constants.AnimComPushInt; param = 160 },
			{ com = constants.AnimComPushInt; param = 148 },
			{ com = constants.AnimComPushInt; param = 160 },
			{ com = constants.AnimComControlledOverlayColor },
			{ com = constants.AnimComSetShielding; param = 1 },
			{ com = constants.AnimComSetLifetime; param = 10000 },
			{ com = constants.AnimComInitWH; param = 18 },
			{ com = constants.AnimComPlaySound; txt = sound_shoot; param = 1 },
			{ param = function( obj )
				local pos = obj:aabb().p
				local flare = CreateSprite( "circle", pos.x, pos.y )
				flare = GetObjectUserdata( flare )
				SetObjSpriteColor( flare, { .8, .4, .4, .4 } )
				SetObjRectangle( flare, 64, 32 )
				SetObjSpriteAngle( flare, obj:sprite_angle() )
				SetObjSpriteRenderMethod( flare, constants.rsmStretch )
				SetObjProcessor( flare, function( object )
					if obj and obj:object_present() and obj:sprite_cur_anim() ~= "die" then
						local p = obj:aabb().p
						SetObjPos( object, p.x, p.y )
					else
						mapvar.tmp[object:id()] = ( mapvar.tmp[object:id()] or 64 ) * 0.8;
						if ( mapvar.tmp[object:id()] < 2 ) then
							SetObjDead( object )
							return
						end
						SetObjRectangle( object, mapvar.tmp[object:id()], mapvar.tmp[object:id()] / 2 )
					end 
				end )
			end },
			{ num = 0; },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{
		name = "fly";
		frames =
		{
			{ dur = 50; num = 0 },
			{ dur = 50; num = 1 },
			{ dur = 50; num = 2 },
			{ dur = 50; num = 1 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "miss";
		frames =
		{
			{ param = function( obj )
				mapvar.tmp[obj:id()] = (mapvar.tmp[obj:id()] or 0) + 1
				if mapvar.tmp[obj:id()] > 5 then
					SetObjAnim( obj, "final_miss", false )
				end
			end },
			{ com = constants.AnimComSetAnim; txt = "fly" },
		}
	},
	{
		name = "final_miss";
		frames = 
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		name = "die";
		frames = 
		{
			{ com = constants.AnimComStop },
			{ dur = 50; num = 0 },
			{ dur = 50; num = 1 },
			{ dur = 50; num = 2 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
