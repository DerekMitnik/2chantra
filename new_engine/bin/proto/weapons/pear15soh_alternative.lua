name = "grenade";

physic = 1;

reload_time = 500;
bullets_per_shot = 10;
flash = "flash-straight";
charge_effect = "flash-charge";
charge_hold = 1500;

push_force = 1.0;

-- �������� ����
bullet_damage = 0;
bullet_vel = 12;

-- �������� ������� ����
texture = "pear15-grenade";

z = -0.002;

bounce = 0.6;

animations = 
{
	{
		name = "fly";
		frames =
		{
			{ com = constants.AnimComSetAccX; param = 0 },
			{ com = constants.AnimComSetAccY; param = 0 },
			{ dur = 80; num = 0 },
		}
	},
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComInitW; param = 23 },
			{ com = constants.AnimComInitH; param = 23 },
			{ com = constants.AnimComRealX; param = -5 },
			{ com = constants.AnimComRealY; param = -5 },
			{ com = constants.AnimComSetProcessor; param = RegisterFunction( function( obj )
					local id = obj:id()
					if not mapvar.tmp[id] then
						mapvar.tmp[id] = { lifetime = 1000, last_update = Loader.time, dead = false }
						obj:acc( {x=0, y=0} )
						obj:gravity( {x=0, y=0.4} )
						obj:max_x_vel( 1000 )
						obj:max_y_vel( 1000 )
						local vel = obj:own_vel()
						local angle
						if vel.x == 0 then
							if vel.y > 0 then
								angle = 1.5707963267949
							else
								angle = -1.5707963267949
							end
						else
							angle = math.atan2( vel.y, vel.x )
						end
						local charge = obj:charge()
						if charge > 1000 then
							charge = 1000
						end
						if math.abs(vel.x) > math.abs(vel.y) then
							if obj:sprite_mirrored() then
								angle = angle + 0.39269908169872 * (charge/1000)
							else
								angle = angle - 0.39269908169872 * (charge/1000)
							end
						end
						vel.x = math.cos(angle) * ( 0.6 + 0.5 * (charge/1000) )
						vel.y = math.sin(angle) * ( 0.6 + 0.5 * (charge/1000) )
						obj:acc( vel )
						obj:own_vel( { x = 0, y = 0 } )
					end
					if mapvar.tmp[id].lifetime < 800 then
						obj:acc( { x = 0, y = 0 } )
					end
					mapvar.tmp[id].lifetime = mapvar.tmp[id].lifetime - (Loader.time - mapvar.tmp[id].last_update)
					mapvar.tmp[id].last_update = Loader.time
					if ( not mapvar.tmp[id].dead ) and mapvar.tmp[id].lifetime < 0 then
						mapvar.tmp[id].dead = true
						SetObjAnim( obj, "die_of_age", false )
					end
			end ) },
			{ com = constants.AnimComPlaySound; txt = "weapons/grenade-launch.ogg"; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{
		name = "miss";
		frames =
		{
			{ param = function( obj )
				SetObjSpriteAngle( obj, 2 * math.pi * math.random() ) 
				
				local vel = obj:vel()
				local pos = obj:aabb().p
				
				vel = math.sqrt( math.pow( vel.x, 2 ) + math.pow( vel.y, 2 ) )
				PlaySnd( "weapons/grenade-bounce.ogg", true, pos.x, pos.y, false, vel/10 )
				--print("bounce! ", pos.x, " ", pos.y, " :", vel)
				
			end },
			--{ com = constants.AnimComPlaySound; txt = "weapons/grenade-bounce.ogg"; param = 0 },
			{ com = constants.AnimComRecover }
		}
	},
	{
		name = "die_of_age";
		frames =
		{
			{ com = constants.AnimComInitW; param = 25 },
			{ com = constants.AnimComInitH; param = 25 },
			{ dur = 80; num = 3 },
			{ dur = 80; num = 4 },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		--�����������
		name = "die";
		frames =
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-grenade" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -40 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "grenade-shard" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -40 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "grenade-shard" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -40 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "grenade-shard" },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
