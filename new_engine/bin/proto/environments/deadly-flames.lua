script_on_enter = function( ud, x, y, material, this )
	if ud == mapvar.tmp.boss_object or ud:type() ~= constants.objEnemy or ud:gravity().y == 0 then
		return
	end
	DamageObject( ud, 1, Game.damage_types.deadly_fire )
end

