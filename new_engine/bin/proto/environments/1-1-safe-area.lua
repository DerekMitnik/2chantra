material = 0;

walk_vel_multiplier = 1;
jump_vel_multiplier = 1;
bounce_bonus = 0;

gravity_bonus_x = 0;
gravity_bonus_y = 0;

script_on_enter = "safespot_enter";
script_on_leave = "safespot_leave";

sprites =
{
	"dust-run",
	"dust-land",
	"dust-stop"
}

sounds =
{
	"foot-left.ogg",
	"foot-right.ogg",
	"stop.ogg"
}