material = 2; --water

walk_vel_multiplier = 0.5;
jump_vel_multiplier = 1;

gravity_bonus_x = 0;
gravity_bonus_y = 0;

script_on_enter = function( ud, x, y, material, this )
	if not ObjectOnScreen( ud ) or ud:type() == constants.objParticleSystem then
		return
	end
	local psystem = CreateParticleSystem( "psewerslime", x, y )
	if psystem then
		local x1 = this:aabb().p.x - this:aabb().W;
		local x2 = this:aabb().p.x + this:aabb().W;
		local y1 = this:aabb().p.y - this:aabb().H - 200;
		local y2 = this:aabb().p.y - this:aabb().H + 10;
		AddParticleArea(psystem, {x1, y1, x2, y2})
	end
end

script_on_leave = function( ud, x, y, old_material, new_material, this )
	if not ObjectOnScreen( ud ) or ud:type() == constants.objParticleSystem then
		return
	end
	local psystem = CreateParticleSystem( "psewerslime", x, y )
	if psystem then
		local x1 = this:aabb().p.x - this:aabb().W;
		local x2 = this:aabb().p.x + this:aabb().W;
		local y1 = this:aabb().p.y - this:aabb().H - 200;
		local y2 = this:aabb().p.y - this:aabb().H + 10;
		AddParticleArea(psystem, {x1, y1, x2, y2})
	end
end

script_on_stay = function( ud, x, y, this )
	if ud:type() ~= constants.objPlayer and ud:type() ~= constants.objEnemy then
		return
	end
	if not mapvar.tmp.drown then mapvar.tmp.drown = {} end
	if not mapvar.tmp.drown[ ud ] then mapvar.tmp.drown[ ud ] = { last_check = Loader.time, time_underwater = 0 } end
	local d = mapvar.tmp.drown[ ud ]
	if Loader.time - d.last_check < 100 then
		return
	end
	if ud:type() == constants.objEnemy then
		local tb = mapvar.tmp[ud]
		if type( tb ) == 'table' and tb.type and tb.type == "green_slime" then
			if tb.cannot_drown or ud:health() <= 0 then
				return
			end
			if tb.size <= 32 then
				SetObjDead( ud )
				return
			end
			local o1 = ud:aabb()
			local o2 = this:aabb()
			if o1.p.y - o1.H >= o2.p.y - o2.H and o1.p.y + o1.H <= o2.p.y + o2.H and
			   o1.p.x - o1.W >= o2.p.x - o2.W and o1.p.x + o1.W <= o2.p.x + o2.W then
				local old_h = o1.H
				o1.H = (o1.p.y - (o2.p.y - o2.H)) + tb.size / 2 + 10
				o1.W = o1.W * ( o1.H / old_h )
				o1.p.y = o1.p.y - ( o1.H - old_h ) / 2
				SetObjRectangle( ud, o1.W, o1.H )
				SetObjPos( ud, o1.p.x, o1.p.y )
				tb.cannot_drown = true
			end
		end
		return
	end
	local dt = Loader.time - d.last_check
	local o1 = ud:aabb()
	local o2 = this:aabb()
	if o1.p.y - o1.H >= o2.p.y - o2.H and o1.p.y + o1.H <= o2.p.y + o2.H and
	   o1.p.x - o1.W >= o2.p.x - o2.W and o1.p.x + o1.W <= o2.p.x + o2.W then
		d.time_underwater = d.time_underwater + dt
		if d.time_underwater > 3000 then
			DamageObject( ud, 10 )
		end
	else
		d.time_underwater = 0
	end
	d.last_check = Loader.time
end

sprites =
{
}

sounds =
{
}
