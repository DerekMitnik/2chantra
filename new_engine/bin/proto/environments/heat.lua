name = "default_environment";
material = 0;

walk_vel_multiplier = 1;
jump_vel_multiplier = 1;
bounce_bonus = 0;

gravity_bonus_x = 0;
gravity_bonus_y = 0;

sprites =
{
	"dust-run",
	"dust-land",
	"dust-stop"
}

sounds =
{
	"foot-left.ogg",
	"foot-right.ogg",
	"stop.ogg"
}

script_on_enter = function( ud, x, y, material, this )
	if not mapvar.tmp.heat_started then
		return
	end
	if ud == GetPlayerCharacter(1) then
		mapvar.tmp.pl1_in_heat = true
	elseif ud == GetPlayerCharacter(2) then
		mapvar.tmp.pl2_in_heat = true
	--[[elseif ud:type() == constants.objEnemy then
		DamageObject( ud, 1, Game.damage_types.fire )]]
	end
end

script_on_leave = function( ud, x, y, old_material, new_material, this )
	if ud == GetPlayerCharacter(1) then
		mapvar.tmp.pl1_in_heat = false
	elseif ud == GetPlayerCharacter(2) then
		mapvar.tmp.pl2_in_heat = false
	end
end