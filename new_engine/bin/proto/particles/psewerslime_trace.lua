--forbidden
texture = "pliquid";
--FunctionName = "CreateSprite";

z = 0.2;

start_color = {61/255, 114/255, 163/255, 1};
--start_color = {255/255, 114/255, 163/255, 1};
var_color = { .1, .1, .1, 0.0 };
end_color = {61/255, 114/255, 163/255, 0};

max_particles = 400;
particle_life_min = 40;
particle_life_max = 60;

start_size = 8.0;
size_variability = 1.0;
end_size = 6.0;

particle_life = 50;
particle_life_var = 2;

system_life = -1;
emission = 2;
particle_min_speed = 5;
particle_max_speed = 10;
particle_min_angle = 0;
particle_max_angle = 180;
particle_min_param = 0;
particle_max_param = 3;
particle_min_trace = 0;
particle_max_trace = 0;
trajectory_type = constants.pttLine;
trajectory_param1 = 2;
trajectory_param2 = 1;
affected_by_wind = 1;
gravity_x = 0;
gravity_y = 0.8;

physic = 1;
bounce = 0;
