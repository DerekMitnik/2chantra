--forbidden
name = "ptest3";
texture = "pblood";

z = 0.9;

start_color = { 1.0, 1.0, 1.0, 1.0 };
var_color = { 0.2, 0.0, 0.0, 0.0 };
end_color = { 0.3, 1.0, 1.0, 1.0 };

max_particles = 100;
particle_life_min = 10;
particle_life_max = 30;

start_size = 4.0;
size_variability = 3.0;
end_size = 2.0;

particle_life = 50;
particle_life_var = 2;

system_life = 5;
emission = 10;

particle_min_speed = 50;
particle_max_speed = 10;
particle_min_angle = 0;
particle_max_angle = 360;
particle_min_param = 0;
particle_max_param = 3;
particle_trajectory_type = constants.pttPseudoDepth;
particle_trajectory_param1 = 90;
particle_trajectory_param2 = 8;
affected_by_wind = 1;