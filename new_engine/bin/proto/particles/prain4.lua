--forbidden
texture = "pliquid";
--FunctionName = "CreateSprite";

z = -1.052;

start_color = { 174/255, 174/255, 183/255, 1 };
var_color = { 0, 0, .1, 0 };
end_color = { 24/255, 36/255, 56/255, 0 };

max_particles = 512;
particle_life_min = 45;
particle_life_max = 80;

start_size = 1.0;
size_variability = 0.2;
end_size = 1.0;

particle_life = 85;
particle_life_var = 2;

system_life = -1;
emission = 100;
particle_min_speed = 3;
particle_max_speed = 5;
particle_min_angle = 90;
particle_max_angle = 90;
particle_min_param = 0;
particle_max_param = 3;
particle_min_trace = 3;
particle_max_trace = 5;
trajectory_type = constants.pttLine;
trajectory_param1 = 90;
trajectory_param2 = 8;
affected_by_wind = 1;
gravity_x = 0;
gravity_y = 0.5;

physic = 0;
bounce = 0.01;

particle_anim = constants.paBounce;
max_bounce = 3;

particle_max_abs_speed = 24;
