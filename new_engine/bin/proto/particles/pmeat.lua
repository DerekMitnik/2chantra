--forbidden
texture = "psolid";
--FunctionName = "CreateSprite";

z = 0.6;

start_color = { .7, .1, .1, 1.0 };
var_color = { .1, .1, .1, 0.0 };
end_color = { .4, .1, .1, 0 };

max_particles = 100;
particle_life_min = 5;
particle_life_max = 20;

start_size = 10.0;
size_variability = 3.0;
end_size = 9.0;

particle_life = 45;
particle_life_var = 2;

system_life = 5;
emission = 20;
particle_min_speed = 20;
particle_max_speed = 26;
particle_min_angle = 0;
particle_max_angle = 360;
particle_min_param = 0;
particle_max_param = 3;
particle_min_trace = 0;
particle_max_trace = 0;
trajectory_type = constants.pttLine;
trajectory_param1 = 90;
trajectory_param2 = 8;
affected_by_wind = 1;
gravity_x = 0;
gravity_y = 0.8;

physic = 1;
bounce = 0.05;
