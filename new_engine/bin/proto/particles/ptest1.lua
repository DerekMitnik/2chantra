--forbidden
name = "ptest1";
texture = "wakabamark";

z = 0.9;

start_color = { 1.0, 1.0, 1.0, 1.0 };
var_color = { 1.0, 1.0, 1.0, 1.0 };
end_color = { 1.0, 1.0, 1.0, 1.0 };

max_particles = 100;
particle_life_min = 10;
particle_life_max = 30;

start_size = 16.0;
size_variability = 8.0;
end_size = 32.0;

particle_life = 100;
particle_life_var = 50;

system_life = 5;
emission = 10;

particle_min_speed = 0;
particle_max_speed = 1;
particle_min_angle = 0;
particle_max_angle = 360;
particle_min_param = 0;
particle_max_param = 3;
particle_min_trace = 0;
particle_max_trace = 0;
particle_trajectory_type = constants.pttLine;
particle_trajectory_param1 = 90;
particle_trajectory_param2 = 8;
affected_by_wind = 0;
gravity_x = 0;
gravity_y = 0.0;