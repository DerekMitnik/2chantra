--forbidden
texture = "pliquid";
--FunctionName = "CreateSprite";

z = 0.6;

start_color = { 174/255, 174/255, 183/255, 1 };
var_color = { 0, 0, .1, 0 };
end_color = { 24/255, 36/255, 56/255, 0 };

max_particles = 512;
particle_life_min = 5;
particle_life_max = 20;

start_size = 3.0;
size_variability = 1.0;
end_size = 3.0;

particle_life = 45;
particle_life_var = 2;

system_life = -1;
emission = 20;
particle_min_speed = 12;
particle_max_speed = 24;
particle_min_angle = 90;
particle_max_angle = 90;
particle_min_param = 0;
particle_max_param = 3;
particle_min_trace = 10;
particle_max_trace = 20;
trajectory_type = constants.pttLine;
trajectory_param1 = 90;
trajectory_param2 = 8;
affected_by_wind = 1;
gravity_x = 0;
gravity_y = 0.8;

physic = 1;
bounce = 0.01;

particle_anim = constants.paBounce;
max_bounce = 3;

particle_max_abs_speed = 24;
