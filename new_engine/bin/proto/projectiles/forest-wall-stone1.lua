reload_time = 200;
bullets_per_shot = 0;
damage_type = 2;
bullet_damage = 0;

-- �������� ������� ����
texture = "forest-wall1";

offscreen_distance = 640
offscreen_behavior = constants.offscreenNone


z = -0.03;

physic = 1;
phys_ghostlike = 1;
--phys_solid = 1;
--phys_one_sided = 0;
--phys_bullet_collidable = 1;

bullet_vel = 0

animations =
{
	{
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComInitH; param = 18 },
			{ com = constants.AnimComRealW; param = 17 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 160 },
			{ com = constants.AnimComSetGravity },
			{ com = constants.AnimComPushInt; param = 400 },
			{ com = constants.AnimComPushInt; param = 8000 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 360 },
			{ com = constants.AnimComRandomAngledSpeed; param = 1 },
			{ com = constants.AnimComSetMaxVelY; param = 8000 },
			{ dur = 1 },
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComSetAnim; txt = "fly"}
		}
	},
	{
		name = "fly";
		frames = 
		{	
			{ num = 2; dur = 100 },
			{ num = 3; dur = 100 },
			{ num = 4; dur = 100 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "miss";
		frames =
		{
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		name = "die";
		frames =
		{
			{ com = constants.AnimComDestroyObject }
		}
	}
}