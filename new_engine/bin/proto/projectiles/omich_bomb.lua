physic = 1;

bullet_damage = 15;
bullet_vel = 0;
damage_type = Game.damage_types.substances;

texture = "penguin_omsk";

z = -0.002;

ghost_to = constants.physEverything;

local sound_crash = "omich-bomb-crash.ogg"


animations = 
{
	{
		name = "fly";
		frames =
		{
			{ dur = 100; num = 19 },
			{ dur = 100; num = 20 },
			{ dur = 100; num = 21 },
			{ param = function( obj )
				if obj:vel().y >= 0 then
					obj:solid_to( constants.physEverything )
				end
			end },
			{ com = constants.AnimComLoop }
		}
	},
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0; num = 19 },
			{ com = constants.AnimComPushInt; param = 400; num = 19 },
			{ com = constants.AnimComSetGravity; num = 19 }, 
			{ com = constants.AnimComRealX; param = 0; num = 0 },
			{ com = constants.AnimComRealY; param = 0; num = 0 },
			{ com = constants.AnimComRealW; param = 9; num = 15 },
			{ com = constants.AnimComRealH; param = 19; num = 17 },
			{ com = constants.AnimComSetVelY; param = -4000 },
			{ com = constants.AnimComSetMaxVelX; param = 5000; num = 19 },
			{ com = constants.AnimComSetMaxVelY; param = 8000; num = 19 },
			{ com = constants.AnimComSetAnim; txt = "fly"; num = 19 }
		}
	},
	{
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die"}
		}
	},
	{
		name = "bounce";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die"}
		}
	},
	{
		--�����������
		name = "die";
		frames =
		{
			{ com = constants.AnimComStop; num = 19 },
			{ com = constants.AnimComJump; param = function()
				if difficulty > 1.5 then
					return 4
				else
					return 1
				end
			end
			},
			{ com = constants.AnimComPlaySound; txt = sound_crash; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-omsk"; num = 19 },
			{ com = constants.AnimComJump, param = 5 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-grenade"; num = 19 },
			{ com = constants.AnimComDestroyObject; num = 19 }
		}
	}
}
