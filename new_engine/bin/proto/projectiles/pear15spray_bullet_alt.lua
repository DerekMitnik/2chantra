--Scores ~77969 at weapontest

bullet_damage = 5;
bullet_vel = 9;
damage_type = Game.damage_types.spray_alt;

texture = "bullets";
overlay = {0};
ocolor = {{1, 1, 1, 1}}

push_force = 1.0;

z = -0.002;


animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 148 },
			{ com = constants.AnimComPushInt; param = 160 },
			{ com = constants.AnimComPushInt; param = 148 },
			{ com = constants.AnimComPushInt; param = 160 },
			{ com = constants.AnimComPushInt; param = 168 },
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComControlledOverlayColor },
			{ com = constants.AnimComSetLifetime; param = 3000 },
			{ com = constants.AnimComRealX; param = 38 },
			{ com = constants.AnimComRealY; param = 7 },
			{ com = constants.AnimComInitWH; param = 14 },
			{ dur = 100; num = 0; },
			{ param = function( obj )
				local pos = obj:aabb().p
				local flare = CreateSprite( "circle", pos.x, pos.y )
				flare = GetObjectUserdata( flare )
				SetObjSpriteColor( flare, { .4, .4, .8, .4 } )
				SetObjRectangle( flare, 64, 32 )
				SetObjSpriteAngle( flare, obj:sprite_angle() )
				SetObjSpriteRenderMethod( flare, constants.rsmStretch )
				SetObjProcessor( flare, function( object )
					if obj and obj:object_present() and obj:sprite_cur_anim() ~= "die" then
						local p = obj:aabb().p
						SetObjPos( object, p.x, p.y )
					else
						mapvar.tmp[object:id()] = ( mapvar.tmp[object:id()] or 64 ) * 0.8;
						if ( mapvar.tmp[object:id()] < 2 ) then
							SetObjDead( object )
							return
						end
						SetObjRectangle( object, mapvar.tmp[object:id()], mapvar.tmp[object:id()] / 2 )
					end 
				end )
			end }
		}
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		-- ����������� ����
		name = "die";
		frames = 
		{
			{ com = constants.AnimComStop },
			{ com = constants.AnimComRealX; param = 25 },
			{ com = constants.AnimComRealY; param = 9 },
			{ dur = 100; num = 4 },
			{ dur = 100; num = 5 },
			{ dur = 100; num = 6 },
			{ dur = 100; num = 7 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
