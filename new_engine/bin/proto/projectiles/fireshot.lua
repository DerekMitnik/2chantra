--forbidden
texture = "fireshot";

reload_time = 200;
bullets_per_shot = 0;
damage_type = 2;

hurts_same_type = 1;
faction_hates = {3}

local diff = (difficulty-1)/5+1;

-- �������� ����
bullet_damage = 10;
bullet_vel = 2.5;

-- �������� ������� ����
--texture = "fireshot";

push_force = 1;

z = -0.001;

image_width = 64;
image_height = 16;
frames_count = 4;

phys_bullet_collidable = 1;

-- ������ ���������, ����� ���� �����
local sound_shoot = "blaster_shot"

local flare_processor = function(obj)
	local t = mapvar.tmp[obj]
	if t.parent and t.parent:object_present() then
		local p = t.parent:aabb().p
		SetObjPos( obj, p.x, p.y )
	else
		SetObjDead( obj )
	end
end

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 12 },
			{ com = constants.AnimComRealH; param = 12 },
			{ com = constants.AnimComSetLifetime; param = 10000 },
			{ param = function( obj )
				local flare = GetObjectUserdata( CreateSprite( "circle", obj:aabb().p.x, obj:aabb().p.y ) )
				mapvar.tmp[flare] = { parent = obj, size = { 64, 64 } }
				SetObjSpriteColor( flare, { 209/255, 146/255, 20/255, 0.25 } )
				SetObjSpriteRenderMethod( flare, constants.rsmStretch )
				SetObjSpriteBlendingMode( flare, constants.bmSrcA_One )
				SetObjProcessor( flare, flare_processor )
				SetObjRectangle( flare, 64, 64 )
				flare:sprite_z( -0.0005 )
			end },
			{ dur = 1; },
			{ param = function( obj )
				CreateParticleSystem( "ppermaspark", obj, 0, 0, 10 )
			end },
		}
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "psparkhit"; param = 2 },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		-- ����������� ����
		name = "die";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "psparkhit"; param = 2 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
