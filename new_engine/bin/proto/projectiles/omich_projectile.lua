physic = 1;

bullet_damage = 15;
bullet_vel = 1;

phys_bullet_collidable = 1;

texture = "penguin_omsk";

z = -0.002;

local sound_crash = "omich-bomb-crash.ogg"

animations = 
{
	{
		name = "fly";
		frames =
		{
			{ com = constants.AnimComRealW; param = 18; num = 15 },
			{ com = constants.AnimComRealH; param = 23; num = 17 },
			{ dur = 100; num = 19 },
			{ dur = 100; num = 20 },
			{ dur = 100; num = 21 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0; num = 0 },
			{ com = constants.AnimComRealY; param = 0; num = 0 },
			{ com = constants.AnimComRealW; param = 18; num = 15 },
			{ com = constants.AnimComRealH; param = 23; num = 17 },
			{ com = constants.AnimComSetAnim; txt = "fly"; num = 19 }
		}
	},
	{
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die"}
		}
	},
	{
		name = "bounce";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die"}
		}
	},
	{
		--�����������
		name = "die";
		frames =
		{
			{ com = constants.AnimComStop; num = 19 },
			{ com = constants.AnimComJump; param = function()
				if difficulty > 1.5 then
					return 4
				else
					return 1
				end
			end
			},
			{ com = constants.AnimComPlaySound; txt = sound_crash; param = 0 },
			{ com = constants.AnimComDestroyObject; num = 19 }
		}
	}
}
