damage_type = 2;
multiple_targets = 0;

is_ray_weapon = 1;
time_to_live = 0;

-- �������� ����
bullet_damage = 0;
--bullet_vel = 15;

-- �������� ������� ����
texture = "laser";

z = -0.002;

end_effect = "projectiles/custom/ray1hit";

local t = 25

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComRealW; param =  32},
			{ com = constants.AnimComRealY; param = 2 },			-- param = 3 ����� ��� �������� ����, ��, �������, ���� ��� ����
			{ dur = t; num = 10; com = constants.AnimComRealH; param = 5 },
			{ dur = t; num = 11 },
			{ dur = t; num = 12 },
			{ dur = t; num = 13 },
			{ dur = t; num = 14 },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		-- ����������� ����
		name = "die";
		frames = 
		{
		}
	}
}
