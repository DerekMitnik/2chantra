parent = "projectiles/explosion"

animations = 
{
	{
		--�����������
		name = "die";
		frames =
		{
			{ com = constants.AnimComInitWH; param = 72 },
			{ com = constants.AnimComStop, param = function( obj )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
			end },
			{ param = function( obj )
				local pos = obj:aabb().p
				local flare = CreateSprite( "circle", pos.x, pos.y )
				flare = GetObjectUserdata( flare )
				SetObjSpriteColor( flare, { 1, .8, 0, 1 } )
				SetObjRectangle( flare, 80, 80 )
				SetObjPos( flare, pos.x, pos.y )
				SetObjSpriteRenderMethod( flare, constants.rsmStretch )
				SetObjProcessor( flare, function( object )
						if not mapvar.tmp[object:id()] then mapvar.tmp[object:id()] = Loader.time end
						local r = 80 + 40 * (Loader.time - mapvar.tmp[object:id()])/200
						SetObjSpriteColor( object, { 1, .8, 0, 1-(Loader.time - mapvar.tmp[object:id()])/200 } )
						if ( r > 120 ) then
							SetObjDead( object )
							return
						end
						SetObjRectangle( object, r, r )
				end ) 
			end },
			{ com = constants.AnimComPlaySound; txt = "weapons/grenade-explosion.ogg" },
			{ com = constants.AnimComCallFunction; txt = "_ShakeCamera" },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 20 },
			{ com = constants.AnimComCreateParticles; txt = "pexplosion_big_center"; },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 20 },
			{ com = constants.AnimComCreateParticles; txt = "pexplosion_big"; },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 20 },
			{ com = constants.AnimComCreateParticles; txt = "pexplosion_sparks"; },
			{ dur = 80; num = 0 },
			{ com = constants.AnimComInitWH; param = 45 },
			{ dur = 80; num = 1 },
			{ com = constants.AnimComInitWH; param = 108 },
			{ dur = 80; num = 2 },
			{ com = constants.AnimComInitWH; param = 78 },
			{ dur = 80; num = 3 },
			{ com = constants.AnimComInitWH; param = 72 },
			{ dur = 80; num = 4 },
			{ com = constants.AnimComInitWH; param = 67 },
			{ dur = 80; num = 5 },
			{ com = constants.AnimComSetSolidTo; param = 0 },
			{ com = constants.AnimComInitWH; param = 52 },
			{ dur = 80; num = 6 },
			{ com = constants.AnimComInitW; param = 62 },
			{ com = constants.AnimComInitH; param = 84 },
			{ dur = 80; num = 7 },
			{ com = constants.AnimComInitW; param = 105 },
			{ com = constants.AnimComInitH; param = 99 },
			{ dur = 80; num = 8 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
