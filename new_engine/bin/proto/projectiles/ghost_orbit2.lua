--forbidden
local diff = (difficulty-1)/5+1;

-- �������� ����
bullet_damage = 10;
bullet_vel = 0;
multiple_targets = 1;

trajectory_type = constants.pttOrbit;
trajectory_param1 = -0.01;
trajectory_param2 = 36;

particle_min_param = 0;
particle_max_param = 2*3.1415;

-- �������� ������� ����

z = -0.001;
k = -2;
ghost_to = 255;

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ dur = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemy; param = 4; txt = "ghost_attached" },
			{}
		}
	},
	{
		-- ����, ������� �� ��������� ����
		name = "diagdown";
		frames = 
		{
			{ dur = 0 }
		}
	},
	{
		-- ����, ������� �� ��������� �����
		name = "diagup";
		frames = 
		{
			{ dur = 0 }
		}
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pspark"; param = 2 },
			{ com = constants.AnimComSetAnim; txt = "diagdown" }
		}
	},
	{
		-- ����������� ����
		name = "die";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "pspark"; param = 2 },							{ com = constants.AnimComSetAnim; txt = "diagdown" }
		}
	}
}
