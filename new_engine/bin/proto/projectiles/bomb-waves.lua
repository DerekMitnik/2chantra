--forbidden
name = "bomb-waves";

physic = 1;

reload_time = 500;
bullets_per_shot = 0;

-- �������� ����
bullet_damage = 15;
bullet_vel = 0;

-- �������� ������� ����
texture = "heli";

z = -0.002;

--bounce = 0.75;

animations = 
{
	{
		name = "fly";
		frames =
		{
			{ dur = 100; num = 15 },
		}
	},
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0; num = 15 },
			{ com = constants.AnimComPushInt; param = 400; num = 15 },
			{ com = constants.AnimComSetGravity; num = 2 }, 
			{ com = constants.AnimComRealX; param = 0; num = 15 },
			{ com = constants.AnimComRealY; param = 0; num = 15 },
			{ com = constants.AnimComRealW; param = 9; num = 15 },
			{ com = constants.AnimComRealH; param = 19; num = 15 },
			{ com = constants.AnimComSetMaxVelX; param = 5000; num = 15 },
			{ com = constants.AnimComSetMaxVelY; param = 5000; num = 15 },
			{ com = constants.AnimComSetAnim; txt = "fly"; num = 15 }
		}
	},
	{
		-- ����, ������� �� ��������� ����
		name = "diagdown";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "straight"; num = 15 }
		}
	},
	{
		-- ����, ������� �� ��������� �����
		name = "diagup";
		frames = 
		{

			{ com = constants.AnimComSetAnim; txt = "straight"; num = 15 }
		}
	},
	{
		name = "miss";
		frames =
		{
			{ com = constants.AnimComSetAnim; txt = "die"}
		}
	},
	{
		--�����������
		name = "die";
		frames =
		{
			{ com = constants.AnimComStop; num = 2 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ com = constants.AnimComSetInvisible; param = 1 },
			{ dur = 100 },
			{ com = constants.AnimComPushInt; param = -20 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ com = constants.AnimComPushInt; param = 20 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ dur = 100 },
			{ com = constants.AnimComPushInt; param = -40 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ dur = 100 },
			{ com = constants.AnimComPushInt; param = -60 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ com = constants.AnimComPushInt; param = 60 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ dur = 100 },
			{ com = constants.AnimComPushInt; param = -80 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ com = constants.AnimComPushInt; param = 80 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ dur = 100 },
			{ com = constants.AnimComPushInt; param = -100 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ com = constants.AnimComPushInt; param = 100 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ dur = 100 },
			{ com = constants.AnimComPushInt; param = -110 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ com = constants.AnimComPushInt; param = 110 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ dur = 100 },
			{ com = constants.AnimComPushInt; param = -120 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ com = constants.AnimComPushInt; param = 120 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ dur = 100 },
			{ com = constants.AnimComPushInt; param = -130 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ com = constants.AnimComPushInt; param = 130 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ dur = 100 },
			{ com = constants.AnimComPushInt; param = -140 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ com = constants.AnimComPushInt; param = 140 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ dur = 100 },
			{ com = constants.AnimComPushInt; param = -150 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ com = constants.AnimComPushInt; param = 150 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ dur = 100 },
			{ com = constants.AnimComPushInt; param = -150 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },
			{ com = constants.AnimComPushInt; param = 150 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateEnemyBullet; txt = "explosion-triple"; num = 2 },

			{ com = constants.AnimComDestroyObject; num = 2 }
		}
	}
}