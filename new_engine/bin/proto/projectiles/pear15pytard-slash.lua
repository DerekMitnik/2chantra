hurts_same_type = 1;
multiple_targets = 1;

z = 0.25;
bullet_damage = 80;
pish_force = 2.0*difficulty;
bullet_vel = 1.5;

damage_type = Game.damage_types.pfake_ghost;
hurts_same_type = 3;

animations = 
{
	{ 
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ dur = 500 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
