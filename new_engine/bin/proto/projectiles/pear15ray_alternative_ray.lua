multiple_targets = -1;
damage_type = Game.damage_types.super_laser;

time_to_live = 1;

bullet_damage = 30;

ray_first_frame_shift = -2;

texture = "superlaser";

z = 0.6;

end_effect = "pear15_ray_alternative_end";

local t = 100

local function get_smaller( obj )
	mapvar.tmp[obj:id()] = mapvar.tmp[obj:id()] * 0.8
	return mapvar.tmp[obj:id()]
end

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComSetInvisible; param = 1 },
			{ com = constants.AnimComRealW; param =  32},
			{ com = constants.AnimComRealH; param =  32},
			{ dur = 1; num = 2 },
			{ com = constants.AnimComSetInvisible; param = 0 },
			{ com = constants.AnimComRealH; param = function( obj )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
				return mapvar.tmp[obj:id()]
			end },
			{ dur = t; num = 2; com = constants.AnimComInitH; param = get_smaller },
			{ dur = t; num = 3; com = constants.AnimComInitH; param = get_smaller },
			{ dur = t; num = 2; com = constants.AnimComInitH; param = get_smaller },
			{ dur = t; num = 3; com = constants.AnimComInitH; param = get_smaller },
			{ dur = t; num = 2; com = constants.AnimComInitH; param = get_smaller },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		-- ����������� ����
		name = "die";
		frames = 
		{
			{ dur = 0; num = 2 }
		}
	}
}
