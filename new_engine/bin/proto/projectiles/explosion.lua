--forbidden
physic = 1;
phys_ghostlike = 1;
ghost_to = 8;
push_force = 4.0;

reload_time = 500;
bullets_per_shot = 0;
multiple_targets = 1;

-- �������� ����
bullet_damage = 60;
bullet_vel = 0;

-- �������� ������� ����
texture = "pear15-explosion";

damage_type = Game.damage_types.explosion;

z = 0;

bounce = 0;

animations = 
{
	{
		name = "straight";
		frames = 
		{
			{ dur = 1; },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		name = "die";
		frames =
		{
			{ com = constants.AnimComInitWH; param = 12 },
			{ com = constants.AnimComStop },
			{ param = function( obj )
				local pos = obj:aabb().p
				local flare = CreateSprite( "circle", pos.x, pos.y )
				flare = GetObjectUserdata( flare )
				SetObjSpriteColor( flare, { 1, .8, 0, 1 } )
				SetObjRectangle( flare, 80, 80 )
				SetObjPos( flare, pos.x, pos.y )
				SetObjSpriteRenderMethod( flare, constants.rsmStretch )
				SetObjProcessor( flare, function( object )
						if not mapvar.tmp[object:id()] then mapvar.tmp[object:id()] = Loader.time end
						local r = 80 + 40 * (Loader.time - mapvar.tmp[object:id()])/200
						SetObjSpriteColor( object, { 1, .8, 0, 1-(Loader.time - mapvar.tmp[object:id()])/200 } )
						if ( r > 120 ) then
							SetObjDead( object )
							return
						end
						SetObjRectangle( object, r, r )
				end ) 
			end },
			{ com = constants.AnimComPlaySound; txt = "weapons/grenade-explosion.ogg" },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -6 },
			{ com = constants.AnimComCreateParticles; txt = "pexplosion_sparks"; },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = -6 },
			{ com = constants.AnimComCreateParticles; txt = "pexplosion"; },
			{ dur = 80; num = 0 },
			{ com = constants.AnimComInitWH; param = 30 },
			{ dur = 80; num = 1 },
			{ com = constants.AnimComInitWH; param = 72 },
			{ dur = 80; num = 2 },
			{ com = constants.AnimComInitWH; param = 52 },
			{ dur = 80; num = 3 },
			{ com = constants.AnimComInitWH; param = 48 },
			{ dur = 80; num = 4 },
			{ com = constants.AnimComInitWH; param = 45 },
			{ dur = 80; num = 5 },
			{ com = constants.AnimComSetSolidTo; param = 0 },
			{ com = constants.AnimComInitWH; param = 48 },
			{ dur = 80; num = 6 },
			{ com = constants.AnimComInitW; param = 62 },
			{ com = constants.AnimComInitH; param = 56 },
			{ dur = 80; num = 7 },
			{ com = constants.AnimComInitW; param = 70 },
			{ com = constants.AnimComInitH; param = 66 },
			{ dur = 80; num = 8 },
			{ com = constants.AnimComDestroyObject }

		}
	}
}

