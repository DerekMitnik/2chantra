--forbidden
reload_time = 200;
bullets_per_shot = 0;
clip_reload_time = 150;
damage_type = 2;
multiple_targets = 0;
hurts_same_type = 1;

is_ray_weapon = 1;
time_to_live = 0;

-- �������� ����
bullet_damage = 75;
--bullet_vel = 15;

-- �������� ������� ����
texture = "laser-color";

z = -0.002;
--z = 1

frames_count = 15;

next_shift_y = 4;

end_effect = "effects/wd_lazor_end";
hit_effect = "effects/wd_lazor_end";

--local sound_shoot = "blaster_shot"

local t = 25

overlay = {0}
color = { 1, .1, .1, 1 }
oclor = { {1,1,1,1} }

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComRealW; param =  32},
			{ com = constants.AnimComRealY; param = 0 },			-- param = 3 ����� ��� �������� ����, ��, �������, ���� ��� ����
			{ dur = t; num = 0; com = constants.AnimComInitH; param = 5 },
			{ dur = t; num = 1 },
			{ dur = t; num = 2 },
			{ dur = t; num = 3 },
			{ dur = t; num = 4 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
