--forbidden
texture = "fireshot";

renderMethod = constants.rsmStretch;

damage_type = 2;


-- �������� ����
bullet_damage = 30;
bullet_vel = 2.5;

push_force = 1;

z = -0.001;


-- ������ ���������, ����� ���� �����

local flare_processor = function(obj)
	local t = mapvar.tmp[obj]
	if t.parent and t.parent:object_present() then
		local p = t.parent:aabb().p
		SetObjPos( obj, p.x, p.y )
	else
		SetObjDead( obj )
	end
end

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 12 },
			{ com = constants.AnimComRealH; param = 12 },
			{ com = constants.AnimComSetLifetime; param = 10000 },
			{ dur = 1 },
			{ param = function( obj )
				local flare = GetObjectUserdata( CreateSprite( "circle", obj:aabb().p.x, obj:aabb().p.y ) )
				mapvar.tmp[flare] = { parent = obj, size = { 64, 64 } }
				SetObjSpriteColor( flare, { 209/255, 146/255, 20/255, 0.1 } )
				SetObjSpriteRenderMethod( flare, constants.rsmStretch )
				SetObjSpriteBlendingMode( flare, constants.bmSrcA_One )
				SetObjProcessor( flare, flare_processor )
				SetObjRectangle( flare, 64, 64 )
				flare:sprite_z( -0.0005 )
				local last_time = Loader.time
				local t = 0
				obj:max_y_vel( 10 )
				--600 0.04
				--0 0.4
				--0.4 - mapvar.tmp[obj] * 0.96 / 600 
				local x = mapvar.tmp[obj]
				obj:gravity( { x = 0, y = -(((207 - 40 * (640+x)) / 400))/300 } )
				SetObjProcessor( obj, function( this )
					SetObjRectangle( this, 16 + 6*math.cos( t / 5 ), 16 + 6*math.cos( t / 5 ) )
					t = t + (Loader.time - last_time) / 10
					last_time = Loader.time
				end )
			end },
			{ dur = 1; },
			{ param = function( obj )
				CreateParticleSystem( "ppermaspark", obj, 0, 0, 10 )
			end },
		}
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "psparkhit"; param = 2 },
			{ com = constants.AnimComDestroyObject }
		}
	},
	{
		-- ����������� ����
		name = "die";
		frames = 
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateParticles; txt = "psparkhit"; param = 2 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
