--forbidden
local diff = (difficulty-1)/5+1;

-- �������� ����
bullet_damage = 20;
bullet_vel = 4;
multiple_targets = 0;

texture = "manpac";

-- �������� ������� ����

z = -0.001;
k = -2;

animations = 
{
	{
		-- ����, ������� �����
		name = "straight";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ com = constants.AnimComCreateEnemy; param = 4; txt = "manpac_attached" },
			{ dur = 25; num = 0 },
			{ dur = 25; num = 1 },
			{ dur = 25; num = 2 },
			{ dur = 25; num = 3 },
			{ dur = 25; num = 4 },
			{ dur = 25; num = 5 },
			--{ com = constants.AnimComLoop }
		}
	},
	{
		-- ����, ������� �� ��������� ����
		name = "diagdown";
		frames = 
		{
			{ dur = 0 }
		}
	},
	{
		-- ����, ������� �� ��������� �����
		name = "diagup";
		frames = 
		{
			{ dur = 0 }
		}
	},
	{
		--���� ������ � ����������� ������.
		name = "miss";
		frames =
		{
			{ com = constants.AnimComStartDying },
			{ com = constants.AnimComSetAnim; txt = "die" }
		}
	},
	{
		-- ����������� ����
		name = "die";
		frames = 
		{
			{ com = constants.AnimComSetBulletCollidable; param = 0 },
			{ com = constants.AnimComStop },
			{ com = constants.AnimComPlaySound; txt = "pacman-die.ogg" },
			{ com = constants.AnimComSetAccY; param = 8000 },
			{ num = 7; dur = 25 },
			{ num = 8; dur = 25 },
			{ num = 9; dur = 25 },
			{ num = 10; dur = 25; },
			{ num = 11; dur = 25; },
			{ num = 12; dur = 25; },
			{ num = 13; dur = 500; },
			{ com = constants.AnimComPushInt; param = 320 },
			{ com = constants.AnimComPushInt; param = 240 },
			{ dur = 1; num = 0; com = constants.AnimComJumpIfCloseToCamera; param = 10 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
