name = "turret-left";

main_weapon = "sfg9000";
alt_weapon = "rapid";
health = 100;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 5; 
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;



mp_count = 1;

FunctionName = "CreateEnemy";

-- �������� �������

texture = "turret";
z = -0.001;

image_width = 1024;
image_height = 2048;
frame_width = 128;
frame_height = 128;
frames_count = 93;

animations = 
{
	-- ����� ����, ��� ���������� ��� ���������
	{ 
		-- ����� 
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 37 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 0 },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComPushInt; param = -1 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComPushInt; param = -3 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 2 },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComPushInt; param = -1 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 3 },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		-- ���� 
		name = "walk";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 25 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComShootBeh; param = constants.csbFreeShooting },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComJumpIfXSpeedGreater; param = 11 },
			{ com = constants.AnimComPushInt; param = -32 },
			{ com = constants.AnimComPushInt; param = -58 },
			{ com = constants.AnimComCreateObject; txt = "dust-run"; param = 1 },
			{ com = constants.AnimComPushInt; param = 38 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ com = constants.AnimComPlaySound; txt = "foot-left" },
			{ dur = 100; num = 12 },
			{ com = constants.AnimComPushInt; param = 39 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 13 },
			{ com = constants.AnimComPushInt; param = 41 },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 14 },
			{ com = constants.AnimComPushInt; param = 42 },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 15 },
			--{ com = constants.AnimComShootBeh; param = constants.csbNoShooting },
			{ com = constants.AnimComPushInt; param = 43 },
			{ com = constants.AnimComPushInt; param = 3 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 16 },
			{ com = constants.AnimComPushInt; param = 42 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ com = constants.AnimComPlaySound; txt = "foot-right" },
			{ dur = 100; num = 17 },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComPushInt; param = 4 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 18 },
			{ com = constants.AnimComPushInt; param = 37 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 19 },
			--{ dur = 100; num = 19 },
			{ com = constants.AnimComShootX; param = 36 },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComPushInt; param = 2 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 10 },
			{ com = constants.AnimComPushInt; param = 36 },
			{ com = constants.AnimComPushInt; param = 3 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 11 },
			{ com = constants.AnimComJump; param = 9 },	
		}
	},
	{ 
		-- ������ 
		name = "jump";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComPushInt; param = -1; num = 45},
			{ com = constants.AnimComJumpIfYSpeedGreater; param = 10},
			{ dur = 100; num = 45; com = constants.AnimComRealH; param = 81 },
			{ dur = 100; num = 46; com = constants.AnimComRealH; param = 73 },
			{ dur = 100; num = 47; com = constants.AnimComRealH; param = 65 },
			{ com = constants.AnimComJumpIfYSpeedGreater; param = 10},
			{ com = constants.AnimComJump; param = 7 },
			{ dur = 0 },
			{ com = constants.AnimComSetAnim; txt = "fly" },
			{ dur = 100; num = 48; com = constants.AnimComRealH; param = 73 },
			{ dur = 100; num = 49; com = constants.AnimComRealH; param = 81 },
			--{ com = constants.AnimComJumpIfYSpeedGreater; param = 13},
			{ com = constants.AnimComSetAnim; txt = "jump" }
		}
	},
	{ 
		-- � ������ (����� �������� jump)
		name = "fly";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45, num = 47 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ dur = 100; num = 47; com = constants.AnimComRealH; param = 73 },
			{ dur = 100; num = 48; com = constants.AnimComRealH; param = 81 },
			{ dur = 100; num = 49; com = constants.AnimComRealH; param = 81 }
		}
	},
	{ 
		-- ������� 
		name = "sit";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 49 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComPushInt; param = 42 },
			{ com = constants.AnimComPushInt; param = -4 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 24; com = constants.AnimComRealH; param = 62 },
		}
	},
	{
		--� �����, �� ��� ������ ���� ������ �������. 
		name = "sitaimup";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 49 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComPushInt; param = 26 },
			{ com = constants.AnimComPushInt; param = -34 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 88; com = constants.AnimComRealH; param = 62 }
		}
	},
	{ 
		name = "sitaimdown";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 49 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComPushInt; param = 22 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 91; com = constants.AnimComRealH; param = 62 }
		}
	},
	{ 
		-- ����������� 
		name = "land";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 49 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComPlaySound; txt = "foot-right" },
			{ com = constants.AnimComPlaySound; txt = "foot-left" },
			{ com = constants.AnimComPushInt; param = -128 },
			{ com = constants.AnimComPushInt; param = -58 },
			{ com = constants.AnimComCreateObject; txt = "dust-land"; param = 0 },
			{ dur = 100; num = 43; com = constants.AnimComRealH; param = 62 },
			{ dur = 100; num = 44 },
			{ com = constants.AnimComSetAnim; txt = "idle"} 
		}
	},
	{
		name = "pain";
		frames =
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 36 },
			{ com = constants.AnimComRealW; param = 40 },
			{ dur = 100; num = 28; com = constants.AnimComRealH; param = 75 },
			{ dur = 100; num = 29 },
			{ com = constants.AnimComRecover }
			--{ com = constants.AnimComSetAnim; txt = "idle"} 
		}
	},
	{ 
		-- ������� 
		name = "die";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 36 },
			{ com = constants.AnimComRealW; param = 40 },
			{ dur = 100; num = 30; com = constants.AnimComRealH; param = 75 },
			{ dur = 100; num = 31 },
			{ dur = 100; num = 32 },
			{ dur = 100; num = 33 },
			{ dur = 100; num = 34 },
			{ dur = 100; num = 35 },
			{ dur = 100; num = 36 },
			{ dur = 100; num = 37 },
			{ dur = 100; num = 38 },
			{ dur = 100; num = 39 },
			{ dur = 100; num = 40 },
			{ dur = 100; num = 41 },
			{ dur = 100; num = 42 }
		}
	},
	{ 
		-- ������������ ���� 
		name = "aim";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 36 },
			{ com = constants.AnimComRealW; param = 40 },
			{ dur = 100; num = 21; com = constants.AnimComRealH; param = 75 }
		}
	},
	{ 
		-- �������� �� �������� ���������
		name = "shoot";
		frames = 
		{
			{ com = constants.AnimComSetAnimIfWeaponNotReady; txt = "aim" },
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 36 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComPushInt; param = -11 },
			{ com = constants.AnimComMPSet; param = 0 },
			--{ com = constants.AnimComJumpIfWeaponReady; param = 9},
			--{ dur = 100; num = 21 },
			--{ com = constants.AnimComSetAnim; txt = "aim" },
			{ com = constants.AnimComShootDir; param = constants.cgdNone },
			{ dur = 100; num = 22; com = constants.AnimComShoot },
			{ com = constants.AnimComSetAnim; txt = "aim" }
		}
	},
	{ 
		-- �������� ����
		name = "sitshoot";
		frames = 
		{
			{ com = constants.AnimComSetAnimIfWeaponNotReady; txt = "sit" },
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 49 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 62 },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComPushInt; param = -4 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ com = constants.AnimComShootDir; param = constants.cgdNone },
			{ dur = 100; num = 25; com = constants.AnimComShoot },
			{ dur = 100; num = 26 }
		}
	},
	{ 
		-- �������� � ������
		name = "jumpshoot";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComPushInt; param = 40 },
			{ com = constants.AnimComPushInt; param = -3 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ com = constants.AnimComShootDir; param = constants.cgdNone },
			{ com = constants.AnimComShoot },
			{ dur = 100; num = 47; com = constants.AnimComRealH; param = 65 },
			{ com = constants.AnimComSetAnim; txt = "fly" }
		}
	},
	{ 
		-- ������������ �����
		name = "gunaimup";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 36 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ com = constants.AnimComPushInt; param = -44 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 51; com = constants.AnimComRealH; param = 75 }
		}
	},
	{ 
		-- �������� ����� �����
		name = "gunliftaimup";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 36 },
			{ com = constants.AnimComRealW; param = 40 },
			{ dur = 100; num = 50; com = constants.AnimComRealH; param = 75 },
			{ dur = 100; num = 51 },
			{ com = constants.AnimComSetAnim; txt = "gunaimup" }
		}
	},
	{ 
		-- �������� �� �������� ����� ����� (����)
		name = "gunaimupshoot";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 36 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComPushInt; param = 26 },
			{ com = constants.AnimComPushInt; param = -42 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ com = constants.AnimComShootDir; param = constants.cgdUp },
			{ dur = 100; num = 52; com = constants.AnimComShoot },
			{ dur = 100; num = 53 },
			{ com = constants.AnimComSetAnim; txt = "gunaimup" }
		}
	},
	{ 
		name = "situpshoot";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 49 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComPushInt; param = 26 },
			{ com = constants.AnimComPushInt; param = -34 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ com = constants.AnimComShootDir; param = constants.cgdUp },
			{ dur = 50; num = 89; com = constants.AnimComShoot },
			{ dur = 100; num = 90 },
			{ com = constants.AnimComSetAnim; txt = "sitaimup" }
		}
	},
	{ 
		name = "sitdownshoot";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 49 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComPushInt; param = 22 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ com = constants.AnimComShootDir; param = constants.cgdDown },
			{ dur = 50; num = 92; com = constants.AnimComShoot },
			{ com = constants.AnimComSetAnim; txt = "sitaimdown" }
		}
	},
	{ 
		-- ������ � �������� ����� ������
		name = "walkgunaimup";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 37 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 45 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComShootBeh; param = constants.csbFreeShooting },
			{ com = constants.AnimComPushInt; param = 30 },
			{ com = constants.AnimComPushInt; param = -44 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 61 },
			{ com = constants.AnimComPushInt; param = 30 },
			{ com = constants.AnimComPushInt; param = -41 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 62 },
			{ com = constants.AnimComPushInt; param = 30 },
			{ com = constants.AnimComPushInt; param = -44 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 60 },
			{ dur = 100; num = 63 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ com = constants.AnimComPushInt; param = -42 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 64 },
			{ com = constants.AnimComPushInt; param = 30 },
			{ com = constants.AnimComPushInt; param = -44 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 65 },
			{ com = constants.AnimComPushInt; param = 31 },
			{ com = constants.AnimComPushInt; param = -41 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 66 },
			{ com = constants.AnimComPushInt; param = 32 },
			{ com = constants.AnimComPushInt; param = -40 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 67 },
			{ com = constants.AnimComPushInt; param = 32 },
			{ com = constants.AnimComPushInt; param = -40 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 68 },
			{ com = constants.AnimComPushInt; param = 29 },
			{ com = constants.AnimComPushInt; param = -40 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 69 },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		-- ������������ ����
		name = "gunaimdown";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 36 },
			{ com = constants.AnimComRealW; param = 40 },
			{ dur = 100; num = 81; com = constants.AnimComRealH; param = 75 }
		}
	},
	{ 
		-- ��������� ����� ����
		name = "gunliftaimdown";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 36 },
			{ com = constants.AnimComRealW; param = 40 },
			{ dur = 100; num = 80; com = constants.AnimComRealH; param = 75 },
			--{ dur = 100; num = 81 },
			{ com = constants.AnimComSetAnim; txt = "gunaimdown" }
		}
	},
	{ 
		--�������� �� ��������� ���� ����� (����)
		name = "gunaimdownshoot";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 45 },
			{ com = constants.AnimComRealY; param = 36 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComPushInt; param = 32 },
			{ com = constants.AnimComPushInt; param = 22 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ com = constants.AnimComShootDir; param = constants.cgdDown },
			{ dur = 100; num = 82; com = constants.AnimComShoot },
			--{ dur = 100; num = 81 },
			{ com = constants.AnimComSetAnim; txt = "gunaimdown" }
		}
	},
	{ 
		-- ������ � ��������� ���� ������
		name = "walkgunaimdown";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 40 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 45 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComShootBeh; param = constants.csbFreeShooting },
			{ com = constants.AnimComPushInt; param = 32 },
			{ com = constants.AnimComPushInt; param = 22 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ com = constants.AnimComPushInt; param = 32 },
			{ com = constants.AnimComPushInt; param = 20 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 70 },
			{ com = constants.AnimComPushInt; param = 32 },
			{ com = constants.AnimComPushInt; param = 23 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 71 },
			{ dur = 100; num = 72 },
			{ dur = 100; num = 73 },
			{ dur = 100; num = 74 },
			{ com = constants.AnimComPushInt; param = 32 },
			{ com = constants.AnimComPushInt; param = 26 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 75 },
			{ dur = 100; num = 76 },
			{ dur = 100; num = 77 },
			{ dur = 100; num = 78 },
			{ com = constants.AnimComPushInt; param = 32 },
			{ com = constants.AnimComPushInt; param = 21 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 79 },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		--������������ ����� (� ������)
		name = "jumpgunaimup";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 41 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 40 },
			{ dur = 100; num = 85; com = constants.AnimComRealH; param = 62 }
		}
	},
	{ 
		--�������� ����� ����� (� ������)
		name = "jumpgunliftaimup";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 41 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComPushInt; param = 30 },
			{ com = constants.AnimComPushInt; param = -32 },
			{ dur = 100; num = 84; com = constants.AnimComRealH; param = 62 },
			--{ dur = 100; num = 85 },
			{ com = constants.AnimComSetAnim; txt = "jumpgunaimup" }
		}
	},
	{ 
		--�������� �� �������� ����� ����� (� ������)
		name = "jumpgunaimupshoot";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 41 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 62 },
			{ com = constants.AnimComPushInt; param = 27 },
			{ com = constants.AnimComPushInt; param = -35 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ com = constants.AnimComShootDir; param = constants.cgdUp },
			{ dur = 100; num = 86; com = constants.AnimComShoot },
			--{ dur = 100; num = 85 },
			{ com = constants.AnimComSetAnim; txt = "jumpgunaimup" }
		}
	},
	{ 
		--������������ ���� (� ������)
		name = "jumpgunaimdown";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 41 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 40 },
			{ dur = 100; num = 56; com = constants.AnimComRealH; param = 62 }
		}
	},
	{ 
		--��������� ����� ���� (� ������)
		name = "jumpgunliftaimdown";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 41 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComPushInt; param = 27 },
			{ com = constants.AnimComPushInt; param = 28 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ dur = 100; num = 55; com = constants.AnimComRealH; param = 62 },
			--{ dur = 100; num = 56 },
			{ com = constants.AnimComSetAnim; txt = "jumpgunaimdown" }
		}
	},
	{ 
		-- �������� �� ��������� ���� ����� (� ������)
		name = "jumpgunaimdownshoot";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 41 },
			{ com = constants.AnimComRealY; param = 14 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 62 },
			{ com = constants.AnimComPushInt; param = 25 },
			{ com = constants.AnimComPushInt; param = 26 },
			{ com = constants.AnimComMPSet; param = 0 },
			{ com = constants.AnimComShootDir; param = constants.cgdDown },
			{ dur = 100; num = 57; com = constants.AnimComShoot },
			--{ dur = 100; num = 56 },
			{ com = constants.AnimComSetAnim; txt = "jumpgunaimdown" }
		}
	},
	{ 
		-- ��������� ����� ���� ��� �������.
		name = "stop";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 50 },
			{ com = constants.AnimComRealY; param = 38 },
			{ com = constants.AnimComRealW; param = 68 },
			{ com = constants.AnimComRealH; param = 74 },
			{ com = constants.AnimComPushInt; param = 37 },
			--{ com = constants.AnimComRealX; param = 25 },
			--{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComPlaySound; txt = "stop" },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 75 },
			{ com = constants.AnimComPushInt; param = 48 },
			{ com = constants.AnimComPushInt; param = -25 },
			{ com = constants.AnimComCreateObject; txt = "dust-stop"; param = 7 },
			{ dur = 100; num = 7 },
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	}
}



