name = "forest-details2";
texture = "forest-details2";
FunctionName = "CreateSprite";

z = -0.7;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComInitH; param = 81 },
			{ com = constants.AnimComRealW; param = 117 },
			{ num = 0; dur = 100 }
		}
	}
}