name = "forest-platform2";
texture = "forest-platform2";
FunctionName = "CreateSprite";

z = -0.7;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 8 },
			{ com = constants.AnimComReplaceWithRandomTile; txt = "forest-roots" },
			{ com = constants.AnimComDestroyObject }
		}
	}
}