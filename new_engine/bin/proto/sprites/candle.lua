texture = "candle_tower";

z = -0.6;

animations =
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealW; param = 16 },
			{ com = constants.AnimComRealH; param = 38 },
			{ param = function(this)
				local pos = this:aabb().p
				local flare = CreateSprite( "circle", pos.x, pos.y )
				SetObjRectangle( flare, 64, 64 )
				SetObjSpriteColor( flare, {1, 90/255, 64/255, 0.25} )
				SetObjSpriteRenderMethod( flare, constants.rsmStretch )
				SetObjPos( flare, pos.x, pos.y - 16 )
				local t = 0
				local x = pos.x
				local y = pos.y
				local last_time = Loader.time or 0
				local dt = 0
				SetObjProcessor( flare, function(obj)
					dt = Loader.time - last_time
					last_time = Loader.time
					t = t + dt / 200
					SetObjPos( obj, x, y + 2 * math.sin(t) )
				end )
			end },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComJump; param = 3 },
		}
	}
}
