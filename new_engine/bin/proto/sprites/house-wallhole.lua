texture = "house-burned-wallhole";

z = -0.8;

physic = 0;

animations = {
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealW; param = 64 },
			{ com = constants.AnimComRealH; param = 192 },
			{ num = 2 }
		}
	},
	{
		name = "break";
		frames =
		{
			{ com = constants.AnimComRealW; param = 64 },
			{ com = constants.AnimComRealH; param = 192 },
			{ param = function( obj )
				local pos = obj:aabb().p
				other = CreateSprite( "house-wallhole", pos.x, pos.y )
				SetObjPos( other, pos.x, pos.y, 0.5 )
				SetObjAnim( other, "top", false )
			end },
			{ num = 0 }
		}
	},
	{
		name = "top";
		frames =
		{
			{ com = constants.AnimComRealW; param = 64 },
			{ com = constants.AnimComRealH; param = 192 },
			{ num = 1 }
		}
	},
}
