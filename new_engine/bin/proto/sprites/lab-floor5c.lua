name = "lab-floor5c";
texture = "lab-floor5";
icon = "icon-floor5c";
FunctionName = "CreateSprite";

z = -0.8;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComInitH; param = 70 },
			{ com = constants.AnimComRealW; param = 26 },
			{ dur = 100; num = 2 }
		}
	}
}
