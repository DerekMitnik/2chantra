texture = "mountains-back1";

z = -1.09;

animations =
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealW; param = 1293 },
			{ com = constants.AnimComRealH; param = 335 },
			{ dur = 100, num = 0 },
			{ dur = 100, num = 1 },
			{ dur = 100, num = 2 },
			{ com = constants.AnimComLoop }
		}
	}
}
