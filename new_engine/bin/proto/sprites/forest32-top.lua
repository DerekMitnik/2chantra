--forbidden
name = "forest32-top";
texture = "forest32";
FunctionName = "CreateSprite";

z = -0.00001;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComInitH; param = 47 },
			{ com = constants.AnimComRealW; param = 64 },
			{ dur = 100; num = 1 }
		}
	}
}
