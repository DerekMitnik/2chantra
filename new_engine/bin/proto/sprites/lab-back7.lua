name = "lab-back7";
texture = "lab-back7";
FunctionName = "CreateSprite";

z = -0.81;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComInitH; param = 27 },
			{ com = constants.AnimComRealW; param = 62 },
			{ dur = 100; num = 0 }
		}
	}

}
