texture = "hb_pear15_sewers";

z = -0.8999;

animations =
{
	{
		name = "floor";
		frames =
		{
			{ com = constants.AnimComRealW; param = 64 },
			{ com = constants.AnimComRealH; param = 480 },
			{ num = 0 }
		}
	},
	{
		name = "pit_left";
		frames =
		{
			{ com = constants.AnimComRealW; param = 64 },
			{ com = constants.AnimComRealH; param = 480 },
			{ num = 1 }
		}
	},
	{
		name = "pit";
		frames =
		{
			{ com = constants.AnimComRealW; param = 64 },
			{ com = constants.AnimComRealH; param = 480 },
			{ num = 2 }
		}
	},
	{
		name = "pit_right";
		frames =
		{
			{ com = constants.AnimComRealW; param = 64 },
			{ com = constants.AnimComRealH; param = 480 },
			{ num = 3 }
		}
	},
}
