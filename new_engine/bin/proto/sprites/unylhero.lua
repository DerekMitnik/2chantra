name = "unylhero";
texture = "unylhero-player";
FunctionName = "CreateSprite";

z = -0.005;

physic = 1;
phys_solid = 1;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = {
	{
		name = "A";
		frames =
		{
			{ num = 0; dur = 50 },
			{ num = 1; dur = 50 },
			{ num = 2; dur = 50 },
			{ num = 3; dur = 50 },
			{ num = 0; dur = 1 }
		}
	},
	{
		name = "B";
		frames =
		{
			{ num = 4; dur = 50 },
			{ num = 5; dur = 50 },
			{ num = 6; dur = 50 },
			{ num = 7; dur = 50 },
			{ num = 4; dur = 1 }
		}
	},
	{
		name = "C";
		frames =
		{
			{ num = 8; dur = 50 },
			{ num = 9; dur = 50 },
			{ num = 10; dur = 50 },
			{ num = 11; dur = 50 },
			{ num = 8; dur = 1 }
		}
	},
	{
		name = "D";
		frames =
		{
			{ num = 12; dur = 50 },
			{ num = 13; dur = 50 },
			{ num = 14; dur = 50 },
			{ num = 15; dur = 50 },
			{ num = 12; dur = 1 }
		}
	},
	{
		name = "E";
		frames =
		{
			{ num = 16; dur = 50 },
			{ num = 17; dur = 50 },
			{ num = 18; dur = 50 },
			{ num = 19; dur = 50 },
			{ num = 16; dur = 1 }
		}
	}
}
