texture = "circle";

z = 0.6;

color = { 133/255, 111/255, 84/255, 0.4 }

animations =
{
	{
		name = "idle",
		frames =
		{
			{ com = constants.AnimComInitWH; param = 64 },
			{ param = function( obj )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
				SetObjSpriteBlendingMode( obj, constants.bmSrcA_One )
			end },
			{ dur = 100 }
		}
	}
}
