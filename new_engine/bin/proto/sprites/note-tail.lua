name = "note-tail";
texture = "unylhero";
FunctionName = "CreateSprite";

z = -0.007;

physic = 1;
phys_solid = 1;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = {
	{
		name = "A";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 20 }
		}
	},
	{
		name = "B";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 21 }
		}
	},
	{
		name = "C";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 22 }
		}
	},
	{
		name = "D";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 23 }
		}
	},
	{
		name = "E";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 24 }
		}
	}
}
