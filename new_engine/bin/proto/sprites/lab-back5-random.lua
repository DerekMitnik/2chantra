name = "lab-back5-random";
texture = "lab-back5";
FunctionName = "CreateSprite";

z = -0.81;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComInitH; param = 40 },
			{ com = constants.AnimComRealW; param = 35 },
			{ com = constants.AnimComPushInt; param = 200 },
			{ com = constants.AnimComJumpRandom; param = 4 },
			{ com = constants.AnimComSetAnim; txt = "1"},
			{ dur = 100; num = 0 }
		}
	},
	{
		name = "1";
		frames =
		{
			{ dur = 100; num = 1 }
		}
	}
}
