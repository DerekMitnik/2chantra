texture = "btardis";

z = -0.1;

local labels
animations, labels = WithLabels
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			--{ com = constants.AnimComInitW; param = 70 },
			--{ com = constants.AnimComInitH; param = 153 },
			{ param = function( obj )
				local pos = obj:aabb().p
				local flare = CreateSprite( "dcircle", pos.x, pos.y )
				vars.btardis_flare = flare
				flare = GetObjectUserdata( flare )
				SetObjSpriteColor( flare, { .4, .4, .8, .4 } )
				SetObjRectangle( flare, 128, 64 )
				--SetObjSpriteAngle( flare, obj:sprite_angle() )
				SetObjSpriteRenderMethod( flare, constants.rsmStretch )
				local l = 72
				SetObjPos( flare, pos.x, pos.y-l )
				SetObjProcessor( flare, function( object )
					if obj and obj:object_present() and obj:sprite_cur_anim() ~= "die" then
						local p = obj:aabb().p
						local t = GetCurTime() % 1000 - 500
						local l = 72
						SetObjSpriteColor( object, { .4, .4, .8, math.abs(math.sin(t/500)) } )
						SetObjRectangle( object, 128*(0.01+math.abs(math.sin(t/500))), 50 )
						local angle = obj:sprite_angle()
						SetObjPos( object, p.x + l*math.sin(angle), p.y - l*math.cos(angle) )
						--while angle <= 5 do angle = angle + math.pi end
						--while angle <= -1.3 do angle = angle + math.pi end
						SetObjSpriteAngle( object, angle )
						--Log(angle)
					else
						mapvar.tmp[object:id()] = ( mapvar.tmp[object:id()] or 64 ) * 0.8;
						if ( mapvar.tmp[object:id()] < 2 ) then
							SetObjDead( object )
							return
						end
						SetObjRectangle( object, mapvar.tmp[object:id()], mapvar.tmp[object:id()] / 2 )
					end 
				end )
			end },
			{ label = "LOOP" },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 70 },
			{ com = constants.AnimComRealH; param = 153 },
			{ dur = 150; num = 0 },
			{ com = constants.AnimComRealX; param = 2 },
			{ dur = 120; num = 1 },
			{ com = constants.AnimComRealX; param = 5 },
			{ dur = 120; num = 2 },
			{ com = constants.AnimComRealX; param = 2 },
			{ dur = 120; num = 3 },
			--{ com = constants.AnimComRealX; param = 0 },
			--{ dur = 100; num = 0 },
			{ com = constants.AnimComJump, param = "LOOP" },
		}
	},
}



