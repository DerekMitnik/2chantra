texture = "house-door";

z = -0.6;

animations =
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealW; param = 1 },
			{ com = constants.AnimComRealH; param = 1 },
			{ dur = 200, num = 0 },
			{ com = constants.AnimComRealX; param = 3 },
			{ dur = 1000, num = 1 },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 200, num = 0 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}
