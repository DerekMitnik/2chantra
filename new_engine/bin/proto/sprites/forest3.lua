name = "forest3";
texture = "forest3";
FunctionName = "CreateSprite";

z = -0.85;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComInitH; param = 47 },
			{ com = constants.AnimComRealW; param = 64 },
			{ com = constants.AnimComPushInt; param = 200 },
			{ com = constants.AnimComJumpRandom; param = 6 },
			{ com = constants.AnimComPushInt; param = 50 },
			{ com = constants.AnimComJumpRandom; param = 7 },
			{ com = constants.AnimComSetAnim; txt = "2"},
			{ com = constants.AnimComSetAnim; txt = "1"},
			{ dur = 100; num = 0 }
		}
	},
	{
		name = "1";
		frames =
		{
			{ com = constants.AnimComPushInt; param = 64 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateObject; txt = "forest13" },
			{ dur = 100; num = 1 }
		}
	},
	{
		name = "2";
		frames =
		{
			{ com = constants.AnimComPushInt; param = -64 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComCreateObject; txt = "forest12" },
			{ dur = 100; num = 2 }
		}
	},
	{
		name = "secret";
		frames =
		{
			{ com = constants.AnimComPushInt; param = 255 },
			{ com = constants.AnimComPushInt; param = 128 },
			{ com = constants.AnimComPushInt; param = 128 },
			{ com = constants.AnimComSetColor; param = 128 }
		}
	}
}
