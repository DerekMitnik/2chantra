name = "testslope1";
texture = "testslope1";
FunctionName = "CreateSprite";

image_width = 128;
image_height = 128;
frame_width = 128;
frame_height = 128;
frames_count = 0;

z = -0.1;

physic = 1;
phys_solid = 1;
phys_slope = 1;
phys_bullet_collidable = 1;