name = "forest-details1";
texture = "forest-details1";
FunctionName = "CreateSprite";

z = -0.7;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComInitH; param = 47 },
			{ com = constants.AnimComRealW; param = 64 },
			{ num = 0; dur = 100 }
		}
	}
}