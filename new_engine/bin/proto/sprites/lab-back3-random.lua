name = "lab-back3-random";
texture = "lab-back3";
FunctionName = "CreateSprite";

z = -0.81;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComInitH; param = 108 },
			{ com = constants.AnimComRealW; param = 35 },
			{ com = constants.AnimComPushInt; param = 200 },
			{ com = constants.AnimComJumpRandom; param = 14 },
			{ com = constants.AnimComPushInt; param = 200 },
			{ com = constants.AnimComJumpRandom; param = 15 },
			{ com = constants.AnimComPushInt; param = 200 },
			{ com = constants.AnimComJumpRandom; param = 16 },
			{ com = constants.AnimComPushInt; param = 200 },
			{ com = constants.AnimComJumpRandom; param = 17 },
			{ com = constants.AnimComPushInt; param = 200 },
			{ com = constants.AnimComJumpRandom; param = 18 },
			{ com = constants.AnimComPushInt; param = 200 },
			{ com = constants.AnimComJumpRandom; param = 19 },
			{ com = constants.AnimComJump; param = 20 },
			{ com = constants.AnimComSetAnim; txt = "1"},
			{ com = constants.AnimComSetAnim; txt = "2"},
			{ com = constants.AnimComSetAnim; txt = "3"},
			{ com = constants.AnimComSetAnim; txt = "4"},
			{ com = constants.AnimComSetAnim; txt = "5"},
			{ com = constants.AnimComSetAnim; txt = "6"},
			{ dur = 100; num = 0 }
		}
	},
	{
		name = "1";
		frames =
		{
			{ dur = 100; num = 1 }
		}
	},
	{
		name = "2";
		frames =
		{
			{ dur = 100; num = 2 }
		}
	},
	{
		name = "3";
		frames =
		{
			{ dur = 100; num = 3 }
		}
	},
		{
		name = "4";
		frames =
		{
			{ dur = 100; num = 4 }
		}
	},
	{
		name = "5";
		frames =
		{
			{ dur = 100; num = 5 }
		}
	},
	{
		name = "6";
		frames =
		{
			{ dur = 100; num = 6 }
		}
	}
}
