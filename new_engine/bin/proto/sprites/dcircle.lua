texture = "dcircle";

z = -0.1;

blendingMode = constants.bmSrcA_One;

animations =
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealW; param = 1 },
			{ com = constants.AnimComRealH; param = 1 },
			{ dur = 1 },
			{ param = function( this )
				if CONFIG_EXTRA and CONFIG_EXTRA.UseFlares and CONFIG_EXTRA.UseFlares == 0 then
					SetObjDead( this )
				end
			end }
		}
	}
}
