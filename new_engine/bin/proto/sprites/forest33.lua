name = "forest33";
texture = "forest33";
FunctionName = "CreateSprite";

z = -0.8;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComInitH; param = 47 },
			{ com = constants.AnimComRealW; param = 64 },
			{ com = constants.AnimComCreateObject; txt = "forest33-top" },
			{ dur = 100; num = 0 }
		}
	}
}
