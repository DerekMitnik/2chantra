--forbidden

texture = "helipoke";
z = -0.0001;

animations =
{
	{
		name = "idle";
		frames =
		{
			{ num = 0 }
		}
	},
	{
		name = "remove";
		frames =
		{
			{ com = constants.AnimComSetInvisible, param = 1 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}