texture = "killinlazor";

z = -0.899;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations =
{
	{
		name = "idle";
		frames =
		{
			{ dur = 50, num = 0 },
			{ dur = 50, num = 1 },
			{ com = constants.AnimComLoop }
		}
	}
}
