name = "note";
texture = "unylhero";
FunctionName = "CreateSprite";

z = -0.005;

physic = 1;
phys_solid = 1;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = {
	{
		name = "A";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 10 }
		}
	},
	{
		name = "B";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 12 }
		}
	},
	{
		name = "C";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 16 }
		}
	},
	{
		name = "D";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 14 }
		}
	},
	{
		name = "E";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 18 }
		}
	},
	{
		name = "A-complete";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 11 }
		}
	},
	{
		name = "B-complete";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 13 }
		}
	},
	{
		name = "C-complete";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 17 }
		}
	},
	{
		name = "D-complete";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 15 }
		}
	},
	{
		name = "E-complete";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 24 }
		}
	},
	{
		name = "f1";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 0 }
		}
	},
	{
		name = "f2";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 2 }
		}
	},
	{
		name = "f3";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 4 }
		}
	},
	{
		name = "f4";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 6 }
		}
	},
	{
		name = "f5";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 8 }
		}
	},
	{
		name = "f1-active";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 1 }
		}
	},
	{
		name = "f2-active";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 3 }
		}
	},
	{
		name = "f3-active";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 5 }
		}
	},
	{
		name = "f4-active";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 7 }
		}
	},
	{
		name = "f5-active";
		frames =
		{
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComRealW; param = 16 },
			{ num = 9 }
		}
	},
	{
		name = "note-correct";
		frames =
		{
			{ num = 25; dur = 50 },
			{ num = 26; dur = 50 },
			{ num = 25; dur = 50 },
			{ num = 26; dur = 50 },
			{ num = 25; dur = 50 },
			{ num = 26; dur = 50 },
			{ com = constants.AnimComSetAnim; txt = "invisible" }
		}
	},
	{
		name = "invisible";
		frames =
		{
			{ num = 27 }
		}
	}
}
