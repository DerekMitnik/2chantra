name = "lab-floor5-random";
texture = "lab-floor5";
FunctionName = "CreateSprite";

z = -0.8;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComInitH; param = 70 },
			{ com = constants.AnimComRealW; param = 26 },
			{ com = constants.AnimComPushInt; param = 200 },
			{ com = constants.AnimComJumpRandom; param = 4},
			{ com = constants.AnimComJump; param = 5 },
			{ com = constants.AnimComSetAnim; txt = "1"},
			{ dur = 100; num = 0 }
		}
	},
	{
		name = "1";
		frames =
		{
			{ dur = 100; num = 1 }
		}
	}
}
