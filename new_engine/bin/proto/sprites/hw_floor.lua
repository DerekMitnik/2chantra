texture = "hw_tiles";
z = -0.9;

animations =
{
	{
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 74 },
			{ com = constants.AnimComRealH; param = 30 },
			{ num = 21; dur = 150; },
			{ num = 22; dur = 150; },
			{ num = 37; dur = 150; },
			{ num = 22; dur = 150; },
			{ com = constants.AnimComJump; param = 2 }
		}
	}
}