texture = "hw_tiles";

z = -0.9;

animations =
{
	{
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 35 },
			{ com = constants.AnimComRealH; param = 148 },
			{ num = 2; dur = 150; },
			{ num = 39; dur = 150; },
			{ num = 40; dur = 150; },
			{ num = 39; dur = 150; },
			{ com = constants.AnimComJump; param = 2 }
		}
	}
}