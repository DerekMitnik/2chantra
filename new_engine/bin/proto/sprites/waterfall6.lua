texture = "waterfall2";
FunctionName = "CreateSprite";

z = -0.89999;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations =
{
	{
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealH; param = 48 },
			{ com = constants.AnimComRealW; param = 88 },
			{ dur = 100, num = 6 },
			{ dur = 100, num = 7 },
			{ dur = 100, num = 8 },
			{ com = constants.AnimComLoop }
		},
	}
}


