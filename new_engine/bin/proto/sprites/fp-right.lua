name = "fp-right";
texture = "fp-right";
FunctionName = "CreateSprite";

z = -0.008;

physic = 1;
phys_solid = 1;
phys_one_sided = 1;
phys_bullet_collidable = 1;

animations =
{
	{
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComInitH; param = 15 },
			{ com = constants.AnimComRealH; param = 23 },
			{ com = constants.AnimComRealW; param = 53 },
			{ com = constants.AnimComInitW; param = 44 },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComInitW; param = 32 },
			{ com = constants.AnimComRealX; param = 12 },
			{ com = constants.AnimComRealY; param = 13 },
			{ com = constants.AnimComSetAnim; txt = "loop" }
		}
	},
	{
		name = "loop";
		frames =
		{
			{ dur = 50, num = 0 },
			{ dur = 50, num = 1 },
			{ dur = 50, num = 2 },
			{ dur = 50, num = 1 },
			{ com = constants.AnimComLoop }
		}
	}
}