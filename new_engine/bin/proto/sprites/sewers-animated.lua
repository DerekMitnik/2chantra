--$(DESCRIPTION).This tileset was automatically generated by the map editor 'import tiles' function.

texture = "sewers"
z = -0.5

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 }
		}
	},
	{
		name = "tile1";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 6; dur = 200 },
			{ num = 15; dur = 150 },
			{ num = 24; dur = 200 },
			{ num = 15; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile2";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 14; dur = 200 },
			{ num = 23; dur = 150 },
			{ num = 32; dur = 200 },
			{ num = 23; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile3";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 42; dur = 200 },
			{ num = 51; dur = 150 },
			{ num = 24; dur = 200 },
			{ num = 51; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile4";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 41; dur = 200 },
			{ num = 50; dur = 150 },
			{ num = 59; dur = 200 },
			{ num = 50; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile5";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 60; dur = 200 },
			{ num = 69 },
			{ num = 78 },
			{ num = 69 },
			{ com = constants.AnimComLoop }
		}
	},
		{
		name = "tile6";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 68; dur = 200 },
			{ num = 77; dur = 150 },
			{ num = 86; dur = 200 },
			{ num = 77; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile7";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 95; dur = 200 },
			{ num = 104; dur = 150 },
			{ num = 113; dur = 200 },
			{ num = 104; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile8";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 122; dur = 200 },
			{ num = 131; dur = 150 },
			{ num = 140; dur = 200 },
			{ num = 131; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile9";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 103; dur = 200 },
			{ num = 112; dur = 150 },
			{ num = 121; dur = 200 },
			{ num = 112; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile10";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 130; dur = 200 },
			{ num = 139; dur = 150 },
			{ num = 5; dur = 200 },
			{ num = 139; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile11";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 7; dur = 200 },
			{ num = 16; dur = 150 },
			{ num = 25; dur = 200 },
			{ num = 16; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile12";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 26; dur = 200 },
			{ num = 35; dur = 150 },
			{ num = 44; dur = 200 },
			{ num = 35; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile13";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 43; dur = 200 },
			{ num = 52; dur = 150 },
			{ num = 61; dur = 200 },
			{ num = 52; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile14";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 53; dur = 200 },
			{ num = 62; dur = 150 },
			{ num = 71; dur = 200 },
			{ num = 62; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile15";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 96; dur = 200 },
			{ num = 105; dur = 150 },
			{ num = 114; dur = 200 },
			{ num = 105; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile16";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 115; dur = 200 },
			{ num = 124; dur = 150 },
			{ num = 133; dur = 200 },
			{ num = 124; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile17";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 142; dur = 200 },
			{ num = 8; dur = 150 },
			{ num = 17; dur = 200 },
			{ num = 8; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile18";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 49; dur = 200 },
			{ num = 58; dur = 150 },
			{ num = 67; dur = 200 },
			{ num = 58; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile19";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 173; dur = 200 },
			{ num = 174; dur = 150 },
			{ num = 175; dur = 200 },
			{ num = 174; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile20";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 189; dur = 200 },
			{ num = 190; dur = 150 },
			{ num = 191; dur = 200 },
			{ num = 190; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile21";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 205; dur = 200 },
			{ num = 206; dur = 150 },
			{ num = 207; dur = 200 },
			{ num = 206; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "tile22";
		frames =
		{
			{ com = constants.AnimComRealW; param = 32 },
			{ com = constants.AnimComRealH; param = 32 },
			{ num = 202; dur = 200 },
			{ num = 203; dur = 150 },
			{ num = 204; dur = 200 },
			{ num = 203; dur = 150 },
			{ com = constants.AnimComLoop }
		}
	},
}
