texture = "flames";

z = -0.7;

animations =
{
	{
		name = "idle",
		frames =
		{
			{ param = function( obj )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
			end },
			{ com = constants.AnimComRealW; param = 45 },
			{ com = constants.AnimComRealH; param = 67 },
			{ dur = 1 },
			{ param = function( obj )
				local pos = obj:aabb().p
				local flare = GetObjectUserdata( CreateSprite( "circle", pos.x, pos.y ) )
				SetObjSpriteColor( flare, {1, 157/255, 62/255, 0.1} )
				SetObjRectangle( flare, 256, 256 )
				SetObjSpriteRenderMethod( flare, constants.rsmStretch )
				SetObjSpriteBlendingMode( flare, constants.bmSrcA_One )
				flare:sprite_z( obj:sprite_z() )
				SetObjPos( flare, pos.x, pos.y-20 )
				CreateParticleSystem( "pfiresparks", obj, 0, 0, 10 )
				CreateParticleSystem( "pfiresmoke", obj, 0, 0, 0 )
			end },
			{},
			{ com = constants.AnimComRealX; param = -2 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 100; num = 2 },
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 100; num = 3 },
			{ com = constants.AnimComJump; param = 5 }
		}
	}
}
