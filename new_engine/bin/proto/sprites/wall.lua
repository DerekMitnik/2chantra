--forbidden
name = "wall";

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 5;
phys_max_y_vel = 50;
phys_jump_vel = 20;
phys_walk_acc = 3;
phys_one_sided = 0;
mp_count = 1;

FunctionName = "CreateEnemy";

-- �������� �������

z = -0.001;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			--{ com = constants.AnimComInitW; param = 11487-11477 },
			--{ com = constants.AnimComInitH; param = 336+76 },
			{ com = constants.AnimComSetHealth; param = 800*difficulty },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComSetGravity },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealW; param = 11487-11477 },
			{ com = constants.AnimComInitH; param = 336+76 },
			{ dur = 100 },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		-- ��������
		name = "pain";
		frames = 
		{
			{ com = constants.AnimComPop },
			{ com = constants.AnimComPushInt; param = 320 },
			{ com = constants.AnimComPushInt; param = 666 },
			{ com = constants.AnimComJumpIfCloseToCamera; param = 4 },
			{ com = constants.AnimComSetAnim; txt="idle" },
			{ com = constants.AnimComReduceHealth },
			{ com = constants.AnimComPlaySound; txt = "boss-hit" },
			{ com = constants.AnimComSetAnim; txt="idle" }
		}
	},
	{ 
		-- ��������
		name = "die";
		frames = 
		{
			{ com = constants.AnimComMapVarAdd; param = 1; txt = "bossdead"},
			{ com = constants.AnimComMapVarAdd; param = 1000; txt = "score"},
			{ com = constants.AnimComDestroyObject }	
		}
	}
}



