texture = "sewers-switch";

z = -0.25;

animations =
{
	{
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComRealH; param = 29 },
			{ com = constants.AnimComRealW; param = 21 },
			{ com = constants.AnimComSetAnim; txt = "closed" },
		}
	},
	{
		name = "closed";
		frames =
		{
			{ dur = 1; num = 0 }
		}
	},
	{
		name = "open";
		frames =
		{
			{ dur = 1; num = 1 }                                      
		}
	}
}
