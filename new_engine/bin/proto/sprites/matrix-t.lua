--forbidden
name = "matrix-t";
texture = "t";
FunctionName = "CreateSprite";

z = -0.25;
image_width = 48;
image_height = 16;
frame_width = 16;
frame_height = 16;
frames_count = 3;

animations = 
{
	{ 
		name = "idle";
		frames_count = 3;
		start_frame = 0;
		anim_speed = 200;
		real_x = 0;
		real_y = 0;
		real_mirror_x = 0;
		real_mirror_y = 0;
		real_width = 0;
		real_height = 0;
	}
}
