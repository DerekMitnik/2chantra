texture = "fp-left";
FunctionName = "CreateSprite";

z = -0.008;

animations =
{
	{
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComSetInvisible; param = 1 },
			{ com = constants.AnimComInitH; param = 14 },
			{ com = constants.AnimComRealH; param = 23 },
			{ com = constants.AnimComRealW; param = 72 },
			{ com = constants.AnimComInitW; param = 48 },
			{ com = constants.AnimComPushInt; param = 1 },
			{ com = constants.AnimComInitW; param = 32 },
			{ com = constants.AnimComRealX; param = 17 },
			{ com = constants.AnimComRealY; param = 13 },
		}
	},
	{
		name = "normal";
		frames =
		{
			{ com = constants.AnimComSetInvisible; param = 1 },
		}
	},
	{
		name = "secret";
		frames =
		{
			{ com = constants.AnimComSetInvisible; param = 0 },
			{ dur = 50, num = 0 },
			{ dur = 50, num = 1 },
			{ dur = 50, num = 2 },
			{ dur = 50, num = 1 },
			{ com = constants.AnimComLoop }
		}
	}
}