texture = "artard";

z = -0.6;

animations =
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealW; param = 49 },
			{ com = constants.AnimComRealH; param = 82 },
			{ dur = 1 },
			{ param = function( this )
				local pos = this:aabb().p
				local weasel = GetObjectUserdata( CreateSprite( "artard", pos.x + 5, pos.y ) )
				SetObjAnim( weasel, "easel", false )
				weasel:sprite_z( -0.7 )
				mapvar.tmp.easel = weasel
			end },
			{ com = constants.AnimComSetAnim; txt = "artard" }
		}
	},
	{
		name = "artard";
		frames =
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ dur = 150; num = 0 },
			{ dur = 150; num = 1 },
			{ dur = 150; num = 2 },
			{ com = constants.AnimComRealX; param = -1 },
			{ dur = 150; num = 3 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "easel";
		frames =
		{
			{ com = constants.AnimComRealW; param = 34 },
			{ com = constants.AnimComRealH; param = 77 },
			{ dur = 100; num = 4 }
		}
	},
	{
		name = "fall";
		frames =
		{
			{ dur = 100; num = 5; com = constants.AnimComRealH; param = 81 },
			{ dur = 100; num = 6; com = constants.AnimComRealH; param = 33 },
			{ dur = 100; num = 7 }
		}
	}
}
