texture = "story-background3";

z = -0.8;

animations =
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComRealW; param = 640 },
			{ com = constants.AnimComRealH; param = 480 },
			{ num = 0 }
		}
	},
	{
		name = "hole";
		frames =
		{
			{ com = constants.AnimComRealW; param = 108 },
			{ com = constants.AnimComRealH; param = 132 },
			{ num = 1 }
		}
	}
}
