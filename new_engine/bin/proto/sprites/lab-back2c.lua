name = "lab-back2c";
texture = "lab-back2";
FunctionName = "CreateSprite";

z = -0.85;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;

animations = 
{
	{
		name = "idle";
		frames =
		{
			{ com = constants.AnimComInitH; param = 48 },
			{ com = constants.AnimComRealW; param = 35 },
			{ dur = 100; num = 2 }
		}
	}
}
