--forbidden
texture = "crosshair-safe";

z = 0.1;

animations = 
{
	{ 
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComSetInvisible; param = 0 },
			{ dur = 100; num = 0 }
		}
	},
	{
		name = "invisible";
		frames = 
		{
			{ com = constants.AnimComSetInvisible; param = 1 },
			{ dur = 100; num = 0 }
		}
	}
}
