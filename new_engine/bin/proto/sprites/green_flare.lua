texture = "circle";

z = 0.6;

color = { 47/255, 144/255, 0/255, 0.4 }

animations =
{
	{
		name = "idle",
		frames =
		{
			{ com = constants.AnimComInitWH; param = 96 },
			{ param = function( obj )
				SetObjSpriteRenderMethod( obj, constants.rsmStretch )
				SetObjSpriteBlendingMode( obj, constants.bmSrcA_One )
				Resume( NewMapThread( function()
					local last_time = Loader.time or 0
					local t = 0
					local dt
					while true do
						dt = (Loader.time or 0) - last_time
						last_time = (Loader.time or 0)
						t = t + dt
						SetObjSpriteColor( obj, { color[1], color[2], color[3], 0.4 * math.cos( t / 1000 ) } )
						Wait(1)
					end
				end ))
			end },
			{ dur = 100 }
		}
	}
}
