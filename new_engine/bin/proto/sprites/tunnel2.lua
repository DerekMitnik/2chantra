texture = "tunnel2";

z = -0.1;

renderMethod = constants.rsmStretch;
blendingMode = constants.bmSrcA_OneMinusSrcA;
color = { 1, 1, 1, 0.5 }

animations = 
{
	{
		name = "idle",
		frames = 
		{
			{ num = 0; dur = 80 },
			{ com = constants.AnimComDestroyObject }
		}
	}
}