z = -0.005;

physic = 0;
phys_solid = 0;
phys_one_sided = 0;
phys_bullet_collidable = 0;
phys_ghostlike = 1;

animations =
{
	{
		name = "init";
		frames = 
		{
			{ com = constants.AnimComInitH; param = 1 },
			{ com = constants.AnimComInitW; param = 1 },
			{ com = constants.AnimComSetTouchable; param = 1 }
		}
	},
	{
		name = "loop";
		frames =
		{
			{}
		}
	},
	{
		name = "touch";
		frames =
		{
			{ com = constants.AnimComCallFunction; txt = "set_respawn_point" },
			{ dur = 1000 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimCOmSetAnim; txt = "loop" }
		}
	}
}