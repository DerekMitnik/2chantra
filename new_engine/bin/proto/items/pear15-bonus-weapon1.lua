texture = "gun";
z = -0.001;

trajectory_type = constants.pttGlobalSine;
trajectory_param1 = 0.5;
trajectory_param2 = 0.05;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 44 },
			{ com = constants.AnimComRealH; param = 24 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		-- ��������
		name = "idle";
		frames = 
		{
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		-- �����
		name = "touch";
		frames = 
		{

			{ param = function() 
				--local pos = obj:aabb().p
				PlaySnd( "weapon-pickup.ogg", true)
				
				mapvar.weapons[1] = true
				
				--push_pause()
				Info.RevealWeapon("soh")
				Game.game_key_pressed = function(key)
					if GamePaused() then
						return
					end
					for i = 1, 2 do
						for j = 1, 4 do
							if key == CONFIG.key_conf[i]["weapon_slot"..j] and j == 1 then
								char = GetPlayerCharacter()
								if char and mapvar.actors[1] and mapvar.actors[1].info.character == "pear15soh-unarmed" then
									mapvar.tmp.getweapon = true
								end
							end
						end
					end
				end
				--Game.ShowTip( 7, true, function() pop_pause() end )
			end },
			{ com = constants.AnimComDestroyObject }

		}
	}
	
}



