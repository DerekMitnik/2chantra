--forbidden
name = "candle";

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 0;
phys_max_y_vel = 50;

health = 1;

FunctionName = "CreateItem";

-- �������� �������

texture = "candle";
z = -0.2;

image_width = 32;
image_height = 64;
frame_width = 16;
frame_height = 38;
frames_count = 2;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 16 },
			{ com = constants.AnimComRealH; param = 38 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		-- ��������
		name = "idle";
		frames = 
		{
	
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		-- �����
		name = "touch";
		frames = 
		{
			{ com = constants.AnimComSetAnim; txt = "idle" }
		}
	},
	{ 
		-- �����
		name = "die";
		frames = 
		{
			{ com = constants.AnimComPushInt },
			{ com = constants.AnimComPushInt },
			{ com = constants.AnimComPushInt; param = 28 },
			{ com = constants.AnimComJumpRandom, param = 6 },
			{ com = constants.AnimComCreateItem; txt = "wakabamark" },
			{ com = constants.AnimComDestroyObject },
			{ dur = 0 },
			{ com = constants.AnimComCreateItem; txt = "ammo" },
			{ com = constants.AnimComDestroyObject }
		}
	}

}



