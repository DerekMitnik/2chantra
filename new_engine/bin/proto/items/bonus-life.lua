trajectory_type = constants.pttGlobalSine;
trajectory_param1 = 0.5;
trajectory_param2 = 0.05;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 0;
phys_max_x_vel = 0;
phys_max_y_vel = 50;

-- �������� �������

texture = "bonus-life";
z = -0.001;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 16 },
			{ com = constants.AnimComRealH; param = 16 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		-- ��������
		name = "idle";
		frames = 
		{
	
			{ dur = 50; num = 0 },
			{ dur = 50; num = 1 },
			{ dur = 50; num = 2 },
			{ dur = 50; num = 1 },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		-- �����
		name = "touch";
		frames = 
		{

			{ com = constants.AnimComPlaySound; txt = "life.ogg" },
			{ param = function( obj )
				local toucher = GetObjectUserdata( ObjectPopInt( obj ) )
				if toucher then
					Info.RevealItem( "LIFE" )
					local pos = obj:aabb().p
					Game.GainLife( toucher, pos.x, pos.y )
				end
			end },
			{ com = constants.AnimComDestroyObject }

		}
	}
	
}



