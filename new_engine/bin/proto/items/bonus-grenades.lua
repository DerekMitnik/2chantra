name = "bonus-grenades";

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 0;
phys_max_x_vel = 0;
phys_max_y_vel = 4;

FunctionName = "CreateItem";

-- �������� �������

texture = "weapons";
z = -0.001;

image_width = 128;
image_height = 512;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 22 },
			{ com = constants.AnimComRealH; param = 22 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComPushInt; param = 0 },
			{ com = constants.AnimComPushInt; param = 800 },
			{ com = constants.AnimComSetGravity },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		name = "idle";
		frames = 
		{
	
			{ dur = 50; num = 0 },
			{ dur = 50; num = 1 },
			{ dur = 50; num = 2 },
			{ dur = 50; num = 3 },
			{ dur = 50; num = 4 },
			{ dur = 50; num = 5 },
			{ dur = 50; num = 3 },
			{ dur = 50; num = 6 },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		-- �����
		name = "touch";
		frames = 
		{
			{ com = constants.AnimComGiveWeapon; txt = "grenade" },
			{ com = constants.AnimComDestroyObject }
		}
	}
	
}



