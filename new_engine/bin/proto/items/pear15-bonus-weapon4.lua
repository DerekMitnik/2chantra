texture = "gun";
z = -0.001;

trajectory_type = constants.pttGlobalSine;
trajectory_param1 = 0.5;
trajectory_param2 = 0.05;

animations = 
{
	{ 
		-- ��������
		name = "init";
		frames = 
		{
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ com = constants.AnimComRealW; param = 44 },
			{ com = constants.AnimComRealH; param = 24 },
			{ com = constants.AnimComSetTouchable; param = 1 },
			{ com = constants.AnimComSetAnim; txt = "idle" }	
		}
	},
	{ 
		-- ��������
		name = "idle";
		frames = 
		{
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComLoop }	
		}
	},
	{ 
		-- �����
		name = "touch";
		frames = 
		{

			{ param = function() 
				--local pos = obj:aabb().p
				PlaySnd( "weapon-pickup.ogg", true)
				
				--Game.info.weapons[2] = true
				--Game.info.weapons[3] = true
				mapvar.weapons[4] = true
				  
				Info.RevealWeapon("force")
				push_pause()
				Game.ShowTip( 8, true, function() pop_pause() end )
			end },
			{ com = constants.AnimComDestroyObject }

		}
	}
	
}



