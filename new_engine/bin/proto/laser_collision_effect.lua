--forbidden
name = "laser_collision_effect";

texture = "twinshot";
phys_bullet_collidable = 0;

z = 0.05;


frames_count = 1;

animations = 
{
	{
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComInitWH; param = 14 },
			{ com = constants.AnimComRealX; param = -4 },
			{ com = constants.AnimComRealY; param = -4 },
			{ dur = 100; num = 12; },
			{ com = constants.AnimComRealX; param = 1 },
			{ com = constants.AnimComRealY; param = 1 },
			{ dur = 100; num = 13 },
			{ dur = 100; num = 14; },
			{ com = constants.AnimComRealX; param = 2 },
			{ com = constants.AnimComRealY; param = 2 },
			{ dur = 100; num = 15 },
			{ com = constants.AnimComDestroyObject }
		}
	}
};