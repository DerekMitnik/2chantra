friction = 0.1;

sprites =
{
	"snow-run",
	"snow-land",
	"snow-stop"
}

sounds =
{
	"snow-left.ogg",
	"snow-right.ogg",
	"stop.ogg"
}
