--forbidden
name = "exchan";

additional_jumps = 0;
walljumps = 2;
walljump_vel = 20;
air_control = 0.1;

--main_weapon = "grenade";
main_weapon = "sfg9500";

--if difficulty < 1 then alt_weapon = "rocketlauncher"; end
--alt_weapon = "custom";
health = 120 / difficulty;

local diff = (difficulty-1)/5+1;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 2.25*2;
phys_max_y_vel = 100;
phys_jump_vel = 12;
phys_walk_acc = 2.25*2;
gravity_x = 0;
gravity_y = 0.3;

drops_shadow = 1;

mp_count = 4;

faction_id = -1;

FunctionName = "CreatePlayer";

-- �������� �������

--LoadTexture("soh-chan1024.png")
texture = "exchan";
z = -0.0015;

local ex_arm_bool = false;
local ex_arm_angle = 0;
local ex_arm1 = 0;
local ex_arm2 = 0;
local first_weapon = true;
local reload_cur = 10;
local reload_time = 10;
local delta = 0.1;

function get_enemy()
end

function show_arms(ex)
	SetObjInvisible(ex_arm1, false);
	SetObjInvisible(ex_arm2, false);	
end

function hide_arms(ex)
	SetObjInvisible(ex_arm1, true);
	SetObjInvisible(ex_arm2, true);	
end

function target_lock(ex)
	if ex_arm_bool == false then
		local arm1 = CreateEffect("ex_arm",0,0,ex:id(),1);
		ex_arm1 = GetObjectUserdata(arm1);
		ex_arm1:sprite_z(-0.0010)
		local arm2 = CreateEffect("ex_arm",0,0,ex:id(),2);
		ex_arm2 = GetObjectUserdata(arm2);
		ex_arm2:sprite_z(-0.0020)
		ex_arm_bool = true;
	end
	if ex:sprite_mirrored() == true then
		SetObjSpriteAngle(ex_arm1,-ex_arm_angle);
		SetObjSpriteAngle(ex_arm2,-ex_arm_angle);
	else
		SetObjSpriteAngle(ex_arm1,ex_arm_angle);
		SetObjSpriteAngle(ex_arm2,ex_arm_angle);
	end
	ex_arm_angle = ex_arm_angle + delta;
	if ex_arm_angle > 15/60 then
		delta = -delta
	end
	if ex_arm_angle < -15/60 then
		delta = -delta
	end
end

animations =
{
	{
	name = "idle";
	frames = 
		{
			{ param = show_arms },
			{ com = constants.AnimComRealX; param = 40 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 80 },
			{ com = constants.AnimComPushInt, param = 0 },
			{ com = constants.AnimComPushInt, param = -15 },
			{ com = constants.AnimComMPSet, param = 1 },
			{ com = constants.AnimComPushInt, param = 8 },
			{ com = constants.AnimComPushInt, param = -15 },
			{ com = constants.AnimComMPSet, param = 2 },
			{ dur = 100; num = 0 },
			{ param = target_lock },
			{ com = constants.AnimComPushInt, param = 0 },
			{ com = constants.AnimComPushInt, param = -16 },
			{ com = constants.AnimComMPSet, param = 1 },
			{ com = constants.AnimComPushInt, param = 8 },
			{ com = constants.AnimComPushInt, param = -16 },
			{ com = constants.AnimComMPSet, param = 2 },
			{ dur = 100; num = 1 },
			{ param = target_lock },
			{ com = constants.AnimComPushInt, param = 0 },
			{ com = constants.AnimComPushInt, param = -17 },
			{ com = constants.AnimComMPSet, param = 1 },
			{ com = constants.AnimComPushInt, param = 8 },
			{ com = constants.AnimComPushInt, param = -17 },
			{ com = constants.AnimComMPSet, param = 2 },
			{ dur = 100; num = 2 },
			{ com = constants.AnimComPushInt, param = 0 },
			{ com = constants.AnimComPushInt, param = -16 },
			{ com = constants.AnimComMPSet, param = 1 },
			{ com = constants.AnimComPushInt, param = 8 },
			{ com = constants.AnimComPushInt, param = -16 },
			{ com = constants.AnimComMPSet, param = 2 },
			{ param = target_lock },
			{ dur = 100; num = 3 },
			{ param = target_lock },
			{ com = constants.AnimComLoop },
		}
	},
	{
	name = "walk1";
	frames =
		{
			{ param = hide_arms },
			{ com = constants.AnimComRealX; param = 55 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 80 },
			{ dur = 100, num = 17 },
			{ dur = 100, num = 18 },
			{ dur = 100, num = 19 },
			{ dur = 100, num = 20 },
			{ dur = 100, num = 24 },
			{ dur = 100, num = 25 },
			{ dur = 100, num = 26 },
			{ dur = 100, num = 27 },
			{ dur = 100, num = 28 },
			{ dur = 100, num = 16 },
			{ com = constants.AnimComLoop },
		}
	},
	{
	name = "walk"; --"run"
	frames =
		{
			{ param = hide_arms },
			{ com = constants.AnimComRealX; param = 55 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 80 },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -128+30, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -25, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 4+1+2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 1, txt = nil },
			{ dur = 50, num = 8 },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -128+30, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -25, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 4+1+2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 1, txt = nil },
			{ dur = 50, num = 9 },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -128+30, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -25, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 4+1+2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 1, txt = nil },
			{ dur = 50, num = 10 },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -128+30, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -25, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 4+1+2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 1, txt = nil },
			{ dur = 50, num = 11 },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -128+30, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -25, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 4+1+2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 1, txt = nil },
			{ dur = 50, num = 12 },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -128+30, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -25, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 4+1+2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 1, txt = nil },
			{ dur = 50, num = 13 },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -128+30, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -25, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 4+1+2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 1, txt = nil },
			{ dur = 50, num = 14 },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -128+30, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -25, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 4+1+2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 1, txt = nil },
			{ dur = 50, num = 15 },
			{ com = constants.AnimComLoop },
		}
	},
	{
	name = "stop";
	frames =
		{
			{ param = hide_arms },
			{ com = constants.AnimComRealX; param = 48 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 80 },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 48, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -25, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 7, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 2, txt = nil },
			{ dur = 100, num = 40, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "idle" },
		}
	},
	{
	name = "jump";
	frames =
		{
			{ param = hide_arms },
			{ com = constants.AnimComRealX; param = 40 },
			{ com = constants.AnimComRealY; param = 15 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 80 },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -128, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -58, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 1, txt = nil },
			{ dur = 50, num = 32, com = constants.AnimComRealH, param = 80, txt = nil },
			--{ dur = 50, num = 33, com = constants.AnimComRealH, param = 75, txt = nil },
			--{ dur = 100, num = 34, com = constants.AnimComRealH, param = 70, txt = nil },
			{ com = constants.AnimComRealX; param = 40 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 80 },
			{ dur = 40, num = 48 },
			{ dur = 40, num = 49 },
			{ dur = 40, num = 50 },
			{ dur = 40, num = 51 },
			{ dur = 40, num = 52 },
			{ dur = 50, num = 53 },
			{ dur = 60, num = 54 },
			{ dur = 90, num = 55 },
			{ com = constants.AnimComJump; param = 12+10 }
		}
	},
	{
	name = "fly";
	frames =
		{
			{ param = hide_arms },
			{ com = constants.AnimComRealX; param = 40 },
			{ com = constants.AnimComRealY; param = 15 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 80 },
			{ dur = 75, num = 34, com = constants.AnimComRealH, param = 70, txt = nil },
			{ dur = 75, num = 33, com = constants.AnimComRealH, param = 75, txt = nil },
			{ dur = 75, num = 32, com = constants.AnimComRealH, param = 80, txt = nil },
		}
	},
	{
	name = "land";
	frames =
		{
			{ param = hide_arms },
			{ com = constants.AnimComRealX; param = 40 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 80 },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -128, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -58, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 1, txt = nil },
			{ dur = 100, num = 0, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "idle" },
		}
	},
	{
	name = "walljump";
	frames =
		{
			{ param = hide_arms },
			{ param = function() Log("anim = walljump") end },
			{ com = constants.AnimComRealX; param = 40 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 80 },
			{ dur = 50, num = 48 },
			{ dur = 50, num = 49 },
			{ dur = 50, num = 50 },
			{ dur = 50, num = 51 },
			{ dur = 50, num = 52 },
			{ dur = 50, num = 53 },
			{ dur = 50, num = 54 },
			{ dur = 50, num = 55 },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "fly" },
		}
	},
	{
	name = "shoot";
	frames =
		{
			{ param = show_arms },
			{ com = constants.AnimComRealX, param = 40, txt = nil },
			{ com = constants.AnimComRealY, param = 30, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 80, txt = nil },
			{ com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ com = constants.AnimComSetAnimIfWeaponNotReady, param = 0, txt = "aim" },
			{ com = constants.AnimComPushInt, param = 44 },
			{ com = constants.AnimComPushInt, param = -6 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			--{ dur = 100, num = 56, com = constants.AnimComShoot, param = 0, txt = nil },
			{ param = function(ex)
				if reload_cur == reload_time then
					if first_weapon == true then
						SetObjAnim(ex_arm1,"shoot",false);
						first_weapon = false;
					else
						SetObjAnim(ex_arm2,"shoot",false);
						first_weapon = true;
					end
				end
				reload_cur = reload_cur - 1;
				if reload_cur <= 0 then
					reload_cur = reload_time;
				end
			  end},
			{ com = constants.AnimComSetAnim, param = 0, txt = "idle" },
		}
	},
	{
	name = "sit";
	frames =
		{
			{ param = hide_arms },
			{ com = constants.AnimComRealX; param = 40 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 50 },
			{ dur = 100, num = 48 },
		}
	},
	{
	name = "roll";
	frames =
		{
			{ param = hide_arms },
			{ com = constants.AnimComRealX; param = 40 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 50 },
			{ dur = 50, num = 48 },
			{ dur = 50, num = 49 },
			{ dur = 50, num = 50 },
			{ dur = 50, num = 51 },
			{ dur = 50, num = 52 },
			{ dur = 50, num = 53 },
			{ dur = 50, num = 54 },
			{ dur = 50, num = 55 },
		}
	},
	{
	name = "lie";
	frames =
		{
			{ param = hide_arms },
			{ com = constants.AnimComRealX; param = 40 },
			{ com = constants.AnimComRealY; param = 30 },
			{ com = constants.AnimComRealW; param = 40 },
			{ com = constants.AnimComRealH; param = 50 },
			{ dur = 100, num = 48 },
		}
	},
}
