--forbidden
additional_jumps = 1;

main_weapon = "pear15unyl_primary";
alt_weapon = "pear15unyl_alternative";

health = 100;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 2.25; 
phys_max_y_vel = 20;
phys_jump_vel = 7.7;
phys_walk_acc = 2.25;
gravity_x = 0;
gravity_y = 0.25;

drops_shadow = 1;

mp_count = 2;

faction_id = -1;

faction_hates = { 1 }

isometric_depth_x = 0.001;

-- �������� �������

--LoadTexture("soh-chan1024.png")
texture = "unyl1024";
z = -0.0015;

aiming_speed = 0.15;

local labels
animations, labels = WithLabels
{
	{
	name = "idle";
	frames =
		{
			{ param = function( obj )
				if not mapvar then mapvar = {} end
				if not mapvar.tmp then mapvar.tmp = {} end
				if not mapvar.tmp.chartime then mapvar.tmp.chartime = {} end
				mapvar.tmp.chartime[obj] = 0
			end },
			{ dur = 0 },
			{ com = constants.AnimComJump, param = function( obj )
				if mapvar.tmp.fail then
					return labels.FAIL
				end
				if mapvar.tmp.talk and mapvar.tmp.talk[obj] then
					mapvar.tmp.talk[obj] = false
					return labels.TALK
				end
				return 2
			end },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = -50, txt = nil },
			{ com = constants.AnimComMPSet, param = 1, txt = nil },
			{ com = constants.AnimComPushInt, param = 30 },
			{ com = constants.AnimComPushInt, param = 2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 74, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = -3, txt = nil },
			{ dur = 100, num = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealY, param = -2 },
			{ dur = 100, num = 1 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealY, param = -1 },
			{ dur = 100, num = 2 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = -3 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealY, param = 0 },
			{ dur = 100, num = 3 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealY, param = -1 },
			{ dur = 100, num = 2 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealY, param = -2 },
			{ dur = 100, num = 1 },
			{ com = constants.AnimComJump, param = function( obj )
				if mapvar.tmp.talk and mapvar.tmp.talk[obj] then
					mapvar.tmp.talk[obj] = false
					return labels.TALK
				end
				if mapvar.tmp.cutscene then
					return 1
				end
				if mapvar.tmp.chartime[obj] > 35 then
					mapvar.tmp.chartime[obj] = 0
					return labels.GUITAR
				else
					mapvar.tmp.chartime[obj] = mapvar.tmp.chartime[obj] + 1
					return 1
				end
			end },
			--{ dur = 0, num = 0, com = constants.AnimComLoop, param = 0, txt = nil },
			{ label = "GUITAR" },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 18, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 26, txt = nil },
			{ dur = 100, num = 111, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 112, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 113, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 114, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 115, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 116, com = nil, param = 0, txt = nil },
			{ label = "GUITAR_LOOP" },
			{ dur = 100, num = 117, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 118, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 119, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 120, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 121, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 122, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 123, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 124, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 125, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 126, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 127, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 128, com = nil, param = 0, txt = nil },
			{ com = constants.AnimComJump, param = "GUITAR_LOOP" },
			{ dur = 100, num = 129, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 130, com = nil, param = 0, txt = nil },
			{ label = "TALK" },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 74, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 100, num = 131 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 132 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 133 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = -3 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 134 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 133 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 132 },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 74, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 100, num = 131 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 132 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 133 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = -3 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 134 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 133 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 132 },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 74, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 100, num = 131 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 132 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 133 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = -3 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 134 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 133 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 132 },
			{ com = constants.AnimComLoop },
			{ label = "FAIL" },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComRealW; param = 40; },
			{ com = constants.AnimComRealH; param = 76; },
			{ dur = 100; num = 31; com = constants.AnimComRealY; param = -5; },
			{ com = constants.AnimComRealY; param = -10; },
			{ dur = 100; num = 32; com = constants.AnimComRealH; param = 81; },
			{ com = constants.AnimComRealY; param = -7; },
			{ dur = 100; num = 33; com = constants.AnimComRealH; param = 78; },
			{ com = constants.AnimComRealY; param = -3; },
			{ dur = 100; num = 34; com = constants.AnimComRealH; param = 74; },
			{ com = constants.AnimComRealY; param = -2; },
			{ dur = 100; num = 35; com = constants.AnimComRealH; param = 73; },
			{ dur = 100; num = 36; com = constants.AnimComRealH; param = 60; },
			{ com = constants.AnimComRealY; param = -3; },
			{ dur = 100; num = 37; com = constants.AnimComRealH; param = 52; },
			{ dur = 100; num = 38; com = constants.AnimComRealH; param = 50; },
			{ dur = 100; num = 39; },
			{ dur = 100; num = 40; }
		}
	},
	{
	name = "walk";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 38, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 72, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComJumpIfXSpeedGreater, param = 14, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -12, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 13, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 0, txt = nil },

			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 0, txt = nil },
			{ com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 38 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 72, txt = nil },
			{ com = constants.AnimComRealX, param = -2, txt = nil },
			{ com = constants.AnimComRealY, param = -3, txt = nil },
			{ dur = 70, num = 12 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 39 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 3, txt = nil },
			{ com = constants.AnimComRealY, param = -2, txt = nil },
			{ dur = 70, num = 13 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 41 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 9, txt = nil },
			{ com = constants.AnimComRealY, param = -1, txt = nil },
			{ dur = 70, num = 14 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 43 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 10, txt = nil },
			{ com = constants.AnimComRealY, param = -1, txt = nil },
			{ dur = 70, num = 15 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 43 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 6, txt = nil },
			{ com = constants.AnimComRealY, param = -1, txt = nil },
			{ dur = 70, num = 16 },
			{ com = constants.AnimComMaterialSound, param = 1, txt = nil },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 39 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = -2, txt = nil },
			{ com = constants.AnimComRealY, param = -3, txt = nil },
			{ dur = 70, num = 17 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 39 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 4, txt = nil },
			{ com = constants.AnimComRealY, param = -2, txt = nil },
			{ dur = 70, num = 18 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 9, txt = nil },
			{ com = constants.AnimComRealY, param = -1, txt = nil },
			{ dur = 70, num = 19 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 37, txt = nil },
			{ com = constants.AnimComPushInt, param = -4, txt = nil },
			{ com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComRealX, param = 9, txt = nil },
			{ com = constants.AnimComRealY, param = -1, txt = nil },
			{ dur = 70, num = 10 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 36, txt = nil },
			{ com = constants.AnimComPushInt, param = -2, txt = nil },
			{ com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComRealX, param = 3, txt = nil },
			{ com = constants.AnimComRealY, param = -1, txt = nil },
			{ dur = 70, num = 11 },
			{ com = constants.AnimComJump, param = 14 },
		}
	},
	{
	name = "jump";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = 20 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComPushInt, param = -8 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 75, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 100, num = 44 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 46 },
			{ com = constants.AnimComPushInt, param = 15 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealH, param = 62 },
			{ dur = 100, num = 45 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 47 },
			{ com = constants.AnimComPushInt, param = 18 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealH, param = 57 },
			{ dur = 100, num = 46 },
			{ com = constants.AnimComJump; param = 21 }
		}
	},
	{
	name = "fly";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 47 },
			{ com = constants.AnimComPushInt, param = 18 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 57, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 75, num = 46 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 46 },
			{ com = constants.AnimComPushInt, param = 19 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealH, param = 62, txt = nil },
			{ dur = 75, num = 47 },
			{ com = constants.AnimComSetWeaponAngle, param = 20 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComPushInt, param = -8 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealH, param = 75, txt = nil },
			{ dur = 75, num = 48 },
		}
	},
	{
	name = "sit";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 40 },
			{ com = constants.AnimComPushInt, param = -8 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 100, num = 24, com = constants.AnimComRealH, param = 59, txt = nil },
		}
	},
	{
	name = "sitaimup";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ com = constants.AnimComPushInt, param = 20 },
			{ com = constants.AnimComPushInt, param = -44 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 16, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 100, num = 87, com = constants.AnimComRealH, param = 59, txt = nil },
		}
	},
	{
	name = "sitaimdown";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ com = constants.AnimComPushInt, param = 23 },
			{ com = constants.AnimComPushInt, param = 18 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 100, num = 90, com = constants.AnimComRealH, param = 59, txt = nil },
		}
	},
	{
	name = "land";
	frames =
		{
			{ param = function( obj )
				Game.CharacterLanded( obj )
			end },
			{ com = constants.AnimComSetWeaponAngle, param = 20 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComPushInt, param = 2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 57, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -128, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -68, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 1, txt = nil },
			{ dur = 100, num = 49 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComPushInt, param = 2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 74, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = -14, txt = nil },
			{ dur = 100, num = 43 },
			{ com = constants.AnimComSetAnimIfGunDirection, param = 1, txt = "gunaimup" },
			{ com = constants.AnimComSetAnimIfGunDirection, param = -1, txt = "gunaimdown" },
			{ com = constants.AnimComSetAnim, param = 0, txt = "idle" },
		}
	},
	{
	name = "pain";
	frames =
		{
			{ com = constants.AnimComJump; param = function(obj) 
				if mapvar.tmp.cutscene then
					ObjectPopInt( obj )
					ObjectPopInt( obj )
					return labels.RECOVER
				end
				Game.ResetCombo() 
				Game.characterHurt( obj, ObjectPopInt( obj ) ) 
				return labels.PAIN
			end },
			{ label = "PAIN" },
			{ dur = 0, num = 0, com = constants.AnimComPlaySound, param = 0, txt = "ouch.ogg" },
			{ dur = 0, num = 0, com = constants.AnimComReduceHealth, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComCreateParticles, param = 2, txt = "pblood_gravity_small" },
			{ com = constants.AnimComSetWeaponAngle, param = 15 },
			{ com = constants.AnimComPushInt, param = 30 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 3, txt = nil },
			{ com = constants.AnimComRealY, param = -2, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 100, num = 28, com = constants.AnimComRealH, param = 73, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 40, txt = nil },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = -9 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 6, txt = nil },
			{ com = constants.AnimComRealY, param = 2, txt = nil },
			{ dur = 100, num = 29, com = constants.AnimComRealH, param = 70, txt = nil },
			{ com = constants.AnimComSetWeaponAngle, param = 15 },
			{ com = constants.AnimComPushInt, param = 30 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 3, txt = nil },
			{ com = constants.AnimComRealY, param = -2, txt = nil },
			{ dur = 100, num = 28, com = constants.AnimComRealH, param = 73, txt = nil },
			{ label = "RECOVER" },
			{ com = constants.AnimComSetAnim, txt = "idle" },
		}
	},
	{
		name = "die";
		frames = 
		{
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComRealW; param = 40; },
			{ com = constants.AnimComRealH; param = 76; },
			{ com = constants.AnimComRealX; param = 0; },
			{ com = constants.AnimComRealY; param = 0; },
			{ com = constants.AnimComJump; param = function( obj )
				if not mapvar.tmp[obj] then
					mapvar.tmp[obj] = {}
				end
				if obj:on_plane() ~= 0 then
					mapvar.tmp[obj].death_frames = 0
					return labels.GROUND_DEATH
				else
					mapvar.tmp[obj].death_frames = 6
					return labels.AIR_DEATH
				end
			end },
			{ label = "AIR_DEATH" },
			{ dur = 100, num = 135 },
			{ com = constants.AnimComJump; param = function( obj )
				if obj:on_plane() ~= 0 then
					return labels.LAND_DEATH
				else
					return labels.AIR_DEATH
				end
			end },
			{ label = "GROUND_DEATH" },
			{ dur = 100; num = 31; com = constants.AnimComRealY; param = -5; },
			{ com = constants.AnimComRealY; param = -10; },
			{ dur = 100; num = 32; com = constants.AnimComRealH; param = 81; },
			{ com = constants.AnimComRealY; param = -7; },
			{ dur = 100; num = 33; com = constants.AnimComRealH; param = 78; },
			{ com = constants.AnimComRealY; param = -3; },
			{ dur = 100; num = 34; com = constants.AnimComRealH; param = 74; },
			{ com = constants.AnimComRealY; param = -2; },
			{ dur = 100; num = 35; com = constants.AnimComRealH; param = 73; },
			{ dur = 100; num = 36; com = constants.AnimComRealH; param = 60; },
			{ label = "LAND_DEATH" },
			{ com = constants.AnimComRealY; param = -3; },
			{ dur = 100; num = 37; com = constants.AnimComRealH; param = 52; },
			{ dur = 100; num = 38; com = constants.AnimComRealH; param = 50; },
			{ dur = 100; num = 39; },
			{ dur = 100; num = 40; },
			{ dur = 100; num = 41; },
			{ dur = 100; num = 42; },
			{ dur = 100; num = 41; },
			{ dur = 100; num = 40; },
			{ dur = 100; num = 41; },
			{ dur = 100; num = 42; },
			{ dur = 100; num = 41; },
			{ dur = 100; num = 40; },
			{ dur = 100; num = 41; },
			{ dur = 100; num = 42; },
			{ label = "DEATH_LOOP" },
			{ dur = 100, num = 41  },
			{ com = constants.AnimComJump; param = function(obj) 
				if mapvar.tmp.cutscene then
					obj:health(100)
					return labels.DEATH_RECOVERY
				end
				mapvar.tmp[obj].death_frames = mapvar.tmp[obj].death_frames - 1
				return iff( mapvar.tmp[obj].death_frames < 0, labels.DEATH_END, labels.DEATH_LOOP )
			end },
			{ label = "DEATH_END"; num = 41 },
			{ com = constants.AnimComDestroyObject },
			{ label = "DEATH_RECOVERY"; num = 41 },
			{ com = constants.AnimComStopMorphing; num = 41 },
			{ com = constants.AnimComSetAnim; txt = "idle" },
			{}
		}
	},
	{
	name = "aim";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 40 },
			{ com = constants.AnimComPushInt, param = -15 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 100, num = 22, com = constants.AnimComRealH, param = 71, txt = nil },
		}
	},
	{
	name = "shoot";
	frames =
		{
			{ com = constants.AnimComSetAnimIfWeaponNotReady, param = 0, txt = "aim" },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 40 },
			{ com = constants.AnimComPushInt, param = -15 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootDir, param = 0, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 71, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComShoot },
			{ dur = 50, num = 22, com = constants.AnimComReloadTime, param = 5, txt = nil },
			{ com = constants.AnimComPushInt, param = 27, txt = nil },
			{ com = constants.AnimComPushInt, param = -24, txt = nil },
			{ com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 50, num = 23, com = constants.AnimComReloadTime, param = 5 },
			{ com = constants.AnimComPushInt, param = 27, txt = nil },
			{ com = constants.AnimComPushInt, param = -28, txt = nil },
			{ com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComRealH, param = 72, txt = nil },
			{ dur = 50, num = 20, com = constants.AnimComReloadTime, param = 5 },
			{ com = constants.AnimComPushInt, param = 27, txt = nil },
			{ com = constants.AnimComPushInt, param = -24, txt = nil },
			{ com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComRealH, param = 71, txt = nil },
			{ dur = 50, num = 23, com = constants.AnimComReloadTime, param = 5 },
			{ com = constants.AnimComPushInt, param = 40 },
			{ com = constants.AnimComPushInt, param = -15 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 200, num = 22 },
			{ com = constants.AnimComSetAnim, txt = "aim" },
		}
	},
	{
	name = "sitshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetAnimIfWeaponNotReady, param = 0, txt = "sit" },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 42 },
			{ com = constants.AnimComPushInt, param = -9 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootDir, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 59, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComShoot },
			{ dur = 50, num = 25, com = constants.AnimComReloadTime, param = 2 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 30 },
			{ com = constants.AnimComPushInt, param = -23 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 1, txt = nil },
			{ com = constants.AnimComRealY, param = 1, txt = nil },
			{ dur = 50, num = 26, com = constants.AnimComReloadTime, param = 2 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 42 },
			{ com = constants.AnimComPushInt, param = -9 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 200, num = 25 },
			{ com = constants.AnimComSetAnim, txt = "sit" },
		}
	},
	{
	name = "jumpshoot";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ com = constants.AnimComPushInt, param = 46 },
			{ com = constants.AnimComPushInt, param = 19 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootDir, param = 0, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComRealH, param = 62, txt = nil },
			{ com = constants.AnimComShoot },
			{ dur = 100, num = 47 },
			{ com = constants.AnimComSetAnim, txt = "fly" },
		}
	},
	{
	name = "gunaimup";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ com = constants.AnimComPushInt, param = -5 },
			{ com = constants.AnimComPushInt, param = -49 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 13, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 100, num = 94, com = constants.AnimComRealH, param = 74, txt = nil },
		}
	},
	{
	name = "gunliftaimup";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = 24 },
			{ com = constants.AnimComPushInt, param = 24 },
			{ com = constants.AnimComPushInt, param = -45 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 73, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 8, txt = nil },
			{ dur = 100, num = 51 },
			{ com = constants.AnimComSetAnim, txt = "gunaimup" },
		}
	},
	{
	name = "gunloweraimup";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = 24 },
			{ com = constants.AnimComPushInt, param = 24 },
			{ com = constants.AnimComPushInt, param = -45 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 73, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 8, txt = nil },
			{ dur = 100, num = 51 },
			{ com = constants.AnimComSetAnim, txt = "idle" },
		}
	},
	{
	name = "gunaimupshoot";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ com = constants.AnimComPushInt, param = -5 },
			{ com = constants.AnimComPushInt, param = -49 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootDir, param = 1, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 74, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 13, txt = nil },
			{ com = constants.AnimComShoot },
			{ dur = 100, num = 95, com = constants.AnimComReloadTime, param = 2 },
			{ dur = 100, num = 96, com = constants.AnimComReloadTime, param = 2 },
			{ com = constants.AnimComSetAnim, txt = "gunaimup" },
		}
	},
	{
	name = "situpshoot";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ com = constants.AnimComPushInt, param = 20 },
			{ com = constants.AnimComPushInt, param = -45 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootDir, param = 1, txt = nil },
			{ com = constants.AnimComRealX, param = 1, txt = nil },
			{ com = constants.AnimComRealY, param = 16, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 59, txt = nil },
			{ com = constants.AnimComShoot },
			{ dur = 50, num = 88, com = constants.AnimComReloadTime, param = 2 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ com = constants.AnimComPushInt, param = 9 },
			{ com = constants.AnimComPushInt, param = -44 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 100, num = 89, com = constants.AnimComReloadTime, param = 2 },
			{ com = constants.AnimComSetAnim, txt = "sitaimup" },
		}
	},
	{
	name = "sitdownshoot";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ com = constants.AnimComPushInt, param = 23 },
			{ com = constants.AnimComPushInt, param = 18 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootDir, param = 2, txt = nil },
			{ com = constants.AnimComRealX, param = 4, txt = nil },
			{ com = constants.AnimComRealY, param = 2, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 59, txt = nil },
			{ com = constants.AnimComShoot },
			{ dur = 50, num = 90, com = constants.AnimComReloadTime, param = 2 },
			{ dur = 100, num = 91, com = constants.AnimComReloadTime, param = 2 },
			{ com = constants.AnimComSetAnim, txt = "sitaimdown" },
		}
	},
	{
	name = "walkgunaimup";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 38, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 72, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComJumpIfXSpeedGreater, param = 14, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -12, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 13, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 0, txt = nil },

			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 0, txt = nil },
			{ com = constants.AnimComSetWeaponAngle, param = 47 },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = -46 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ com = constants.AnimComRealW, param = 45, txt = nil },
			{ com = constants.AnimComRealH, param = 72, txt = nil },
			{ com = constants.AnimComRealX, param = -2, txt = nil },
			{ com = constants.AnimComRealY, param = 10, txt = nil },
			{ dur = 70, num = 62 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = -46 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 4, txt = nil },
			{ com = constants.AnimComRealY, param = 10, txt = nil },
			{ dur = 70, num = 63 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = -46 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 9, txt = nil },
			{ com = constants.AnimComRealY, param = 9, txt = nil },
			{ dur = 70, num = 64 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = -45 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 11, txt = nil },
			{ com = constants.AnimComRealY, param = 8, txt = nil },
			{ dur = 70, num = 65 },
			{ com = constants.AnimComSetWeaponAngle, param = 49 },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = -45 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 7, txt = nil },
			{ com = constants.AnimComRealY, param = 9, txt = nil },
			{ dur = 70, num = 66 },
			{ com = constants.AnimComMaterialSound, param = 1, txt = nil },
			{ com = constants.AnimComSetWeaponAngle, param = 50 },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = -45 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = -1, txt = nil },
			{ com = constants.AnimComRealY, param = 9, txt = nil },
			{ dur = 70, num = 67 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ com = constants.AnimComPushInt, param = 27 },
			{ com = constants.AnimComPushInt, param = -44 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 4, txt = nil },
			{ com = constants.AnimComRealY, param = 9, txt = nil },
			{ dur = 70, num = 68 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ com = constants.AnimComPushInt, param = 27 },
			{ com = constants.AnimComPushInt, param = -45 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 9, txt = nil },
			{ com = constants.AnimComRealY, param = 10, txt = nil },
			{ dur = 70, num = 69 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ com = constants.AnimComPushInt, param = 28, txt = nil },
			{ com = constants.AnimComPushInt, param = -45, txt = nil },
			{ com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComRealX, param = 9, txt = nil },
			{ com = constants.AnimComRealY, param = 9, txt = nil },
			{ dur = 70, num = 60 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = -39 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 10, txt = nil },
			{ dur = 70, num = 61 },
			{ com = constants.AnimComJump, param = 14 },
		}
	},
	{
	name = "gunaimdown";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = -90 },
			{ com = constants.AnimComPushInt, param = 3 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 100, num = 99, com = constants.AnimComRealH, param = 74, txt = nil },
		}
	},
	{
	name = "gunaimdownforward"; --?
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComShootDir, param = 1, txt = nil },
			{ com = constants.AnimComPushInt, param = 26 },
			{ com = constants.AnimComPushInt, param = 15 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 100, num = 81, com = constants.AnimComRealH, param = 72, txt = nil },
		}
	},
	{
	name = "gunliftaimdown";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = -23 },
			{ com = constants.AnimComPushInt, param = 30 },
			{ com = constants.AnimComPushInt, param = 10 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ com = constants.AnimComRealX, param = 2, txt = nil },
			{ com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 100, num = 81, com = constants.AnimComRealH, param = 71, txt = nil },
			{ com = constants.AnimComSetAnim, txt = "gunaimdown" },
		}
	},
	{
	name = "gunloweraimdown";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = -23 },
			{ com = constants.AnimComPushInt, param = 30 },
			{ com = constants.AnimComPushInt, param = 10 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ com = constants.AnimComRealX, param = 2, txt = nil },
			{ com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 100, num = 81, com = constants.AnimComRealH, param = 71, txt = nil },
			{ com = constants.AnimComSetAnim, txt = "idle" },
		}
	},
	{
	name = "gunaimdownshoot";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = -90 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootDir, param = 2, txt = nil },
			{ com = constants.AnimComRealX, param = 3, txt = nil },
			{ com = constants.AnimComRealY, param = -3, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 74, txt = nil },
			{ com = constants.AnimComShoot },
			{ dur = 100, num = 100, com = constants.AnimComReloadTime, param = 2 },
			{ com = constants.AnimComSetAnim, txt = "gunaimdown" },
		}
	},
	{
	name = "gunaimdownforwardshoot"; --?
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 72, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 23, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 14, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 2, txt = nil },
			{ com = constants.AnimComPushInt, param = 25 },
			{ com = constants.AnimComPushInt, param = 11 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 100, num = 82, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "gunaimdownforward" },
		}
	},
	{
	name = "walkgunaimdown";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 38, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 73, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComJumpIfXSpeedGreater, param = 14, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -12, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 13, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 0, txt = nil },

			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 0, txt = nil },
			{ com = constants.AnimComSetWeaponAngle, param = -48 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = 17 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ com = constants.AnimComRealW, param = 45, txt = nil },
			{ com = constants.AnimComRealH, param = 73, txt = nil },
			{ com = constants.AnimComRealX, param = -3, txt = nil },
			{ com = constants.AnimComRealY, param = -4, txt = nil },
			{ dur = 70, num = 72 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = 17 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 2, txt = nil },
			{ com = constants.AnimComRealY, param = -4, txt = nil },
			{ dur = 70, num = 73 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = 17 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 9, txt = nil },
			{ com = constants.AnimComRealY, param = -3, txt = nil },
			{ dur = 70, num = 74 },
			{ com = constants.AnimComSetWeaponAngle, param = -47 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = 17 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 10, txt = nil },
			{ com = constants.AnimComRealY, param = -4, txt = nil },
			{ dur = 70, num = 75 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = 18 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 6, txt = nil },
			{ com = constants.AnimComRealY, param = -5, txt = nil },
			{ dur = 70, num = 76 },
			{ com = constants.AnimComMaterialSound, param = 1, txt = nil },
			{ com = constants.AnimComSetWeaponAngle, param = -40 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComPushInt, param = 18 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = -3, txt = nil },
			{ com = constants.AnimComRealY, param = -5, txt = nil },
			{ dur = 70, num = 77 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = 18 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 3, txt = nil },
			{ com = constants.AnimComRealY, param = -4, txt = nil },
			{ dur = 70, num = 78 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = 17 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 8, txt = nil },
			{ com = constants.AnimComRealY, param = -3, txt = nil },
			{ dur = 70, num = 79 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = 17 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = 6, txt = nil },
			{ com = constants.AnimComRealY, param = -4, txt = nil },
			{ dur = 70, num = 70 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = 17 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealX, param = -4, txt = nil },
			{ com = constants.AnimComRealY, param = -3, txt = nil },
			{ dur = 70, num = 71 },
			{ com = constants.AnimComJump, param = 14 },
		}
	},
	{
	name = "jumpgunaimup";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComPushInt, param = -50 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 19, txt = nil },
			{ dur = 100, num = 103, com = constants.AnimComRealH, param = 62, txt = nil },
		}
	},
	{
	name = "jumpgunaimupshoot";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComPushInt, param = -47 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 62, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 19, txt = nil },
			{ dur = 100, num = 104, com = constants.AnimComShoot, param = 0, txt = nil },
			{ com = constants.AnimComSetAnim, txt = "jumpgunaimup" },
		}
	},
	{
	name = "jumpgunaimdown";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -90, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 5 },
			{ com = constants.AnimComPushInt, param = 37 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -90 },
			{ dur = 100, num = 97, com = constants.AnimComRealH, param = 62, txt = nil },
		}
	},
	{
	name = "jumpgunaimdownshoot";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = -90 },
			{ com = constants.AnimComPushInt, param = 5 },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 62, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 100, num = 98, com = constants.AnimComShoot, param = 0, txt = nil },
			{ com = constants.AnimComSetAnim, txt = "jumpgunaimdown" },
		}
	},
	{
	name = "jumpgunaimupforward";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 40 },
			{ com = constants.AnimComPushInt, param = -25 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 100, num = 85, com = constants.AnimComRealH, param = 62, txt = nil },
		}
	},
	{
	name = "jumpgunliftaimup"; --?
	frames =
		{
			--{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			--{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 45, txt = nil },
			--{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			--{ dur = 0, num = 0, com = constants.AnimComRealY, param = 15, txt = nil },
			--{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			--{ com = constants.AnimComPushInt, param = 32 },
			--{ com = constants.AnimComPushInt, param = -33 },
			--{ com = constants.AnimComMPSet, param = 0 },
			--{ com = constants.AnimComSetWeaponAngle, param = 20 },
			--{ dur = 100, num = 109, com = constants.AnimComRealH, param = 62, txt = nil },
			{ com = constants.AnimComSetAnim, txt = "jumpgunaimup" },
		}
	},
	{
	name = "jumpgunaimupforwardshoot";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = 49 },
			{ com = constants.AnimComPushInt, param = 39 },
			{ com = constants.AnimComPushInt, param = -24 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootDir, param = 1, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 62, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 100, num = 86, com = constants.AnimComShoot, param = 0, txt = nil },
			{ com = constants.AnimComSetAnim, txt = "jumpgunaimupforward" },
		}
	},
	{
	name = "jumpgunaimdownforward";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 100, num = 56, com = constants.AnimComRealH, param = 62, txt = nil },
		}
	},
	{
	name = "jumpgunliftaimdown"; --?
	frames =
		{
			--{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			--{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			--{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			--{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 27, txt = nil },
			--{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 28, txt = nil },
			--{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			--{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			--{ dur = 0, num = 0, com = constants.AnimComRealY, param = 17, txt = nil },
			--{ com = constants.AnimComPushInt, param = 37 },
			--{ com = constants.AnimComPushInt, param = -12 },
			--{ com = constants.AnimComMPSet, param = 0 },
			--{ com = constants.AnimComSetWeaponAngle, param = -15 },
			--{ dur = 100, num = 108, com = constants.AnimComRealH, param = 62, txt = nil },
			{ com = constants.AnimComSetAnim, txt = "jumpgunaimdown" },
		}
	},
	{
	name = "jumpgunaimdownforwardshoot";
	frames =
		{
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = 26 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComShootDir, param = 2, txt = nil },
			{ com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComRealH, param = 62, txt = nil },
			{ com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 100, num = 57, com = constants.AnimComShoot, param = 0, txt = nil },
			{ com = constants.AnimComSetAnim, txt = "jumpgunaimdownforward" },
		}
	},
	{
	name = "stop";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -20, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 75, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 48, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -25, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 7, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnimIfGunDirection, param = 1, txt = "gunaimup" },
			{ dur = 0, num = 0, com = constants.AnimComSetAnimIfGunDirection, param = -1, txt = "gunaimdown" },
			{ com = constants.AnimComSetWeaponAngle, param = 20 },
			{ com = constants.AnimComPushInt, param = 34 },
			{ com = constants.AnimComPushInt, param = 2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComRealW, param = 40 },
			{ com = constants.AnimComRealH, param = 75 },
			{ com = constants.AnimComRealX, param = 3 },
			{ com = constants.AnimComRealY, param = -5 },
			{ dur = 100, num = 7 },
			{ com = constants.AnimComSetAnim, txt = "idle" },
		}
	},
	{
	name = "morph_out";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetShadow, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPlaySound, param = 0, txt = "morph.ogg" },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 56, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -10, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -3, txt = nil },
			{ dur = 50, num = 92, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 92, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetBulletCollidable, param = 0, txt = nil },
			{ dur = 50, num = 92, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
		}
	},
	{
	name = "lie";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 46, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 31, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -2, txt = nil },
			{ dur = 200, num = 107, com = nil, param = 0, txt = nil },
		}
	},
	{
	name = "roll";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 46, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 31, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 11, txt = nil },
			{ dur = 600, num = 106, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 106, com = nil, param = 0, txt = nil },
		}
	},
	{
	name = "morph_in";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 56, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -10, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -3, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 92, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetBulletCollidable, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetShadow, param = 1, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 92, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 92, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetShadow, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComStopMorphing, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "idle" },
		}
	},
}
