--forbidden
name = "sohchan";

additional_jumps = 1;

main_weapon = "sfg9500";
alt_weapon = "grenade";
--main_weapon = "sfg9500";

--if difficulty < 1 then alt_weapon = "rocketlauncher"; end
--alt_weapon = "custom";
health = 120 / difficulty;

local diff = (difficulty-1)/5+1;

physic = 1;
phys_solid = 0;
phys_bullet_collidable = 1;
phys_max_x_vel = 2.25; 
phys_max_y_vel = 50;
phys_jump_vel = 8;
phys_walk_acc = 2.25;
gravity_x = 0;
gravity_y = 0.3;

drops_shadow = 1;

mp_count = 2;

faction_id = -1;

FunctionName = "CreatePlayer";

-- �������� �������

--LoadTexture("soh-chan1024.png")
texture = "soh-chan1024";
z = -0.0015;

animations =
{
	{
	name = "idle";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -50, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 0, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 1, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 2, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 3, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComLoop, param = 0, txt = nil },
		}
	},
	{
	name = "walk";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 72, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComJumpIfXSpeedGreater, param = 11, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -32, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -58, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 38, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 41 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 12, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 39, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 42 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 13, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 41, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 46 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 14, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 7, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 42, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 46 },
			{ com = constants.AnimComPushInt, param = -4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 15, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 43, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 44 },
			{ com = constants.AnimComPushInt, param = -2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 16, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 42, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 1, txt = nil },
			{ com = constants.AnimComPushInt, param = 44 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 17, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 44 },
			{ com = constants.AnimComPushInt, param = -1 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 18, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 37, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 41 },
			{ com = constants.AnimComPushInt, param = -3 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 70, num = 19, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootX, param = 36, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 9, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 70, num = 10, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 36, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 70, num = 11, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComJump, param = 11, txt = nil },
		}
	},
	{
	name = "jump";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = -6 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 45, com = constants.AnimComRealH, param = 81, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = -6 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 46, com = constants.AnimComRealH, param = 73, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = -8 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 47, com = constants.AnimComRealH, param = 65, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComJump; param = 19 }
		}
	},
	{
	name = "fly";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 34, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -13, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = -12 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 75, num = 47, com = constants.AnimComRealH, param = 73, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 6, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 37, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -11, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 38 },
			{ com = constants.AnimComPushInt, param = -10 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 75, num = 48, com = constants.AnimComRealH, param = 81, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 36, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -6, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 37 },
			{ com = constants.AnimComPushInt, param = -6 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 75, num = 49, com = constants.AnimComRealH, param = 81, txt = nil },
		}
	},
	{
	name = "sit";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 42, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 40 },
			{ com = constants.AnimComPushInt, param = -8 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 24, com = constants.AnimComRealH, param = 59, txt = nil },
		}
	},
	{
	name = "sitaimup";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 11, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 10, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 24, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -39, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 22 },
			{ com = constants.AnimComPushInt, param = -40 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 100, num = 88, com = constants.AnimComRealH, param = 59, txt = nil },
		}
	},
	{
	name = "sitaimdown";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 22, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 23 },
			{ com = constants.AnimComPushInt, param = 22 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -41 },
			{ dur = 100, num = 91, com = constants.AnimComRealH, param = 59, txt = nil },
		}
	},
	{
	name = "land";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -128, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -58, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 57, txt = nil },
			{ com = constants.AnimComPushInt, param = 37 },
			{ com = constants.AnimComPushInt, param = 4 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 43, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = 2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 44, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnimIfGunDirection, param = 1, txt = "gunaimup" },
			{ dur = 0, num = 0, com = constants.AnimComSetAnimIfGunDirection, param = -1, txt = "gunaimdown" },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "idle" },
		}
	},
	{
	name = "pain";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComPlaySound, param = 0, txt = "ouch.ogg" },
			{ dur = 0, num = 0, com = constants.AnimComPop, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComReduceHealth, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComCreateParticles, param = 2, txt = "pblood_gravity_small" },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 30, txt = nil },
			{ com = constants.AnimComPushInt, param = 23 },
			{ com = constants.AnimComPushInt, param = -16 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 33 },
			{ dur = 100, num = 28, com = constants.AnimComRealH, param = 73, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 15, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 40, txt = nil },
			{ com = constants.AnimComPushInt, param = 14 },
			{ com = constants.AnimComPushInt, param = -19 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 47 },
			{ dur = 100, num = 29, com = constants.AnimComRealH, param = 70, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 30, txt = nil },
			{ com = constants.AnimComPushInt, param = 23 },
			{ com = constants.AnimComPushInt, param = -16 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 26 },
			{ dur = 100, num = 28, com = constants.AnimComRealH, param = 73, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "idle" },
		}
	},
	{
	name = "die";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 76, txt = nil },
			{ com = constants.AnimComPushInt, param = 26 },
			{ com = constants.AnimComPushInt, param = 2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 25 },
			{ dur = 100, num = 30, com = constants.AnimComRealY, param = -2, txt = nil },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -32 },
			{ dur = 100, num = 31, com = constants.AnimComRealY, param = -2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -7, txt = nil },
			{ com = constants.AnimComPushInt, param = 26 },
			{ com = constants.AnimComPushInt, param = 25 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 15 },
			{ dur = 100, num = 32, com = constants.AnimComRealH, param = 81, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -4, txt = nil },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = 33 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 33, com = constants.AnimComRealH, param = 78, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 34, com = constants.AnimComRealH, param = 74, txt = nil },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = 34 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 35, com = constants.AnimComRealH, param = 73, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 36, com = constants.AnimComRealH, param = 60, txt = nil },
			{ com = constants.AnimComPushInt, param = 36 },
			{ com = constants.AnimComPushInt, param = 24 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 37, com = constants.AnimComRealH, param = 52, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = 23 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 38, com = constants.AnimComRealH, param = 50, txt = nil },
			{ dur = 100, num = 39, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 40, com = nil, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 34 },
			{ com = constants.AnimComPushInt, param = 16 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 41, com = constants.AnimComRealH, param = 37, txt = nil },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = 11 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 42, com = constants.AnimComRealH, param = 26, txt = nil },
		}
	},
	{
	name = "aim";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 39, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -14, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 40 },
			{ com = constants.AnimComPushInt, param = -15 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 21, com = constants.AnimComRealH, param = 72, txt = nil },
		}
	},
	{
	name = "shoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnimIfWeaponNotReady, param = 0, txt = "aim" },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 71, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 38, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -14, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 39 },
			{ com = constants.AnimComPushInt, param = -15 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 50, num = 22, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "aim" },
		}
	},
	{
	name = "sitshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 59, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnimIfWeaponNotReady, param = 0, txt = "sit" },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 38 },
			{ com = constants.AnimComPushInt, param = -7 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 50, num = 25, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 2, txt = nil },
			{ com = constants.AnimComPushInt, param = 38 },
			{ com = constants.AnimComPushInt, param = -8 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 26, com = nil, param = 0, txt = nil },
		}
	},
	{
	name = "jumpshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 73, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 34, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -7, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShoot, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 35 },
			{ com = constants.AnimComPushInt, param = -8 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 0 },
			{ dur = 100, num = 47, com = constants.AnimComRealH, param = 65, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "fly" },
		}
	},
	{
	name = "gunaimup";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 46, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 13, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 7, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 90, txt = nil },
			{ com = constants.AnimComPushInt, param = -8 },
			{ com = constants.AnimComPushInt, param = -44 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ dur = 100, num = 97, com = constants.AnimComRealH, param = 86, txt = nil },
		}
	},
	{
	name = "gunliftaimup";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 73, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 20, txt = nil },
			{ com = constants.AnimComPushInt, param = 33 },
			{ com = constants.AnimComPushInt, param = -24 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 24 },
			{ dur = 100, num = 50, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "gunaimup" },
		}
	},
	{
	name = "gunaimupshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 90, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 83, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 12, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -43, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 1, txt = nil },
			{ com = constants.AnimComPushInt, param = -6 },
			{ com = constants.AnimComPushInt, param = -41 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ dur = 100, num = 98, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 82, txt = nil },
			{ com = constants.AnimComPushInt, param = -5 },
			{ com = constants.AnimComPushInt, param = -41 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ dur = 100, num = 99, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "gunaimup" },
		}
	},
	{
	name = "situpshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 11, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 65, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 22, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -33, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 1, txt = nil },
			{ com = constants.AnimComPushInt, param = 22 },
			{ com = constants.AnimComPushInt, param = -34 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 50, num = 89, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 4, txt = nil },
			{ com = constants.AnimComPushInt, param = 24 },
			{ com = constants.AnimComPushInt, param = -37 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 100, num = 90, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "sitaimup" },
		}
	},
	{
	name = "sitdownshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 59, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 20, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 23, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 2, txt = nil },
			{ com = constants.AnimComPushInt, param = 22 },
			{ com = constants.AnimComPushInt, param = 21 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 50, num = 92, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "sitaimdown" },
		}
	},
	{
	name = "walkgunaimup";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 88, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 15, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -7, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 26, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -39, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 25 },
			{ com = constants.AnimComPushInt, param = -36 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 70, num = 61, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 9, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 25, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -38, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 26 },
			{ com = constants.AnimComPushInt, param = -39 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 47 },
			{ dur = 70, num = 62, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 10, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -6, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -41, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 27 },
			{ com = constants.AnimComPushInt, param = -37 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 70, num = 63, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 13, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -7, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -41, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 27 },
			{ com = constants.AnimComPushInt, param = -37 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 70, num = 64, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 22, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -10, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 27, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -41, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 26 },
			{ com = constants.AnimComPushInt, param = -34 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 70, num = 65, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 13, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -7, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -41, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 27 },
			{ com = constants.AnimComPushInt, param = -37 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 49 },
			{ dur = 70, num = 66, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 7, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -38, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 27 },
			{ com = constants.AnimComPushInt, param = -39 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 50 },
			{ dur = 70, num = 67, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 11, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 27, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -39, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 27 },
			{ com = constants.AnimComPushInt, param = -39 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 70, num = 68, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 13, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -39, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = -39 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 70, num = 69, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 21, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 25, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -39, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 70, num = 60, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComLoop, param = 0, txt = nil },
		}
	},
	{
	name = "gunaimdown";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -90, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComPushInt, param = 3 },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -90 },
			{ dur = 100, num = 106, com = constants.AnimComRealH, param = 72, txt = nil },
		}
	},
	{
	name = "gunaimdownforward";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ com = constants.AnimComPushInt, param = 26 },
			{ com = constants.AnimComPushInt, param = 15 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 100, num = 81, com = constants.AnimComRealH, param = 72, txt = nil },
		}
	},
	{
	name = "gunliftaimdown";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -20, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 34 },
			{ com = constants.AnimComPushInt, param = 0 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -23 },
			{ dur = 100, num = 80, com = constants.AnimComRealH, param = 71, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "gunaimdown" },
		}
	},
	{
	name = "gunaimdownshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -90, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 72, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 6, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 3 },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -90 },
			{ dur = 100, num = 107, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "gunaimdown" },
		}
	},
	{
	name = "gunaimdownforwardshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 72, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 23, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 14, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 2, txt = nil },
			{ com = constants.AnimComPushInt, param = 25 },
			{ com = constants.AnimComPushInt, param = 11 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 100, num = 82, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "gunaimdownforward" },
		}
	},
	{
	name = "walkgunaimdown";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 73, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 10, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 27, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 15, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 29 },
			{ com = constants.AnimComPushInt, param = 15 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 70, num = 70, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 4, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 27, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 15, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 28 },
			{ com = constants.AnimComPushInt, param = 15 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 70, num = 71, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 32, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 21, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 34 },
			{ com = constants.AnimComPushInt, param = 18 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -48 },
			{ dur = 70, num = 72, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 31, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 19, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 33 },
			{ com = constants.AnimComPushInt, param = 17 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 70, num = 73, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 30, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 15, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = 15 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 70, num = 74, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 8, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 17, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 30 },
			{ com = constants.AnimComPushInt, param = 16 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -47 },
			{ dur = 70, num = 75, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 30, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 19, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = 16 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 70, num = 76, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 29, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 19, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -5, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -3, txt = nil },
			{ com = constants.AnimComPushInt, param = 31 },
			{ com = constants.AnimComPushInt, param = 18 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -40 },
			{ dur = 70, num = 77, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -3, txt = nil },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = 18 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 70, num = 78, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 6, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 30, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 19, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = 17 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 70, num = 79, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComLoop, param = 0, txt = nil },
		}
	},
	{
	name = "jumpgunaimup";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 90, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 3, txt = nil },
			{ com = constants.AnimComPushInt, param = 5 },
			{ com = constants.AnimComPushInt, param = -59 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ dur = 100, num = 100, com = constants.AnimComRealH, param = 62, txt = nil },
		}
	},
	{
	name = "jumpgunaimupshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 90, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -62, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 5 },
			{ com = constants.AnimComPushInt, param = -57 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ dur = 100, num = 101, com = constants.AnimComShoot, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 6 },
			{ com = constants.AnimComPushInt, param = -56 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 90 },
			{ dur = 100, num = 102, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "jumpgunaimup" },
		}
	},
	{
	name = "jumpgunaimdown";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -90, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 8, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 17, txt = nil },
			{ com = constants.AnimComPushInt, param = -8 },
			{ com = constants.AnimComPushInt, param = 22 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -90 },
			{ dur = 100, num = 103, com = constants.AnimComRealH, param = 62, txt = nil },
		}
	},
	{
	name = "jumpgunaimdownshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -90, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 8, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 17, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -9, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 20, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = -9 },
			{ com = constants.AnimComPushInt, param = 20 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -90 },
			{ dur = 100, num = 104, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "jumpgunaimdown" },
		}
	},
	{
	name = "jumpgunaimupforward";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 24, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 10, txt = nil },
			{ com = constants.AnimComPushInt, param = 25 },
			{ com = constants.AnimComPushInt, param = -54 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 45 },
			{ dur = 100, num = 85, com = constants.AnimComRealH, param = 62, txt = nil },
		}
	},
	{
	name = "jumpgunliftaimup";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 15, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ com = constants.AnimComPushInt, param = 32 },
			{ com = constants.AnimComPushInt, param = -33 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 20 },
			{ dur = 100, num = 84, com = constants.AnimComRealH, param = 62, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "jumpgunaimup" },
		}
	},
	{
	name = "jumpgunaimupforwardshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 62, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 13, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 21, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -58, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 1, txt = nil },
			{ com = constants.AnimComPushInt, param = 21 },
			{ com = constants.AnimComPushInt, param = -58 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = 49 },
			{ dur = 100, num = 86, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "jumpgunaimupforward" },
		}
	},
	{
	name = "jumpgunaimdownforward";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 17, txt = nil },
			{ com = constants.AnimComPushInt, param = 26 },
			{ com = constants.AnimComPushInt, param = 3 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 100, num = 56, com = constants.AnimComRealH, param = 62, txt = nil },
		}
	},
	{
	name = "jumpgunliftaimdown";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 27, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 28, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 17, txt = nil },
			{ com = constants.AnimComPushInt, param = 37 },
			{ com = constants.AnimComPushInt, param = -12 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -15 },
			{ dur = 100, num = 55, com = constants.AnimComRealH, param = 62, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "jumpgunaimdown" },
		}
	},
	{
	name = "jumpgunaimdownforwardshoot";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -45, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 17, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 62, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 24, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 3, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMPSet, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComShootDir, param = 2, txt = nil },
			{ com = constants.AnimComPushInt, param = 26 },
			{ com = constants.AnimComPushInt, param = 2 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -45 },
			{ dur = 100, num = 57, com = constants.AnimComShoot, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "jumpgunaimdownforward" },
		}
	},
	{
	name = "stop";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = -20, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSound, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 40, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 75, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 48, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = -25, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPushInt, param = 7, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComMaterialSprite, param = 2, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnimIfGunDirection, param = 1, txt = "gunaimup" },
			{ dur = 0, num = 0, com = constants.AnimComSetAnimIfGunDirection, param = -1, txt = "gunaimdown" },
			{ com = constants.AnimComRealW, param = 40 },
			{ com = constants.AnimComRealH, param = 75 },
			{ com = constants.AnimComRealX, param = 0 },
			{ com = constants.AnimComRealY, param = -4 },
			{ com = constants.AnimComPushInt, param = 38 },
			{ com = constants.AnimComPushInt, param = 9 },
			{ com = constants.AnimComMPSet, param = 0 },
			{ com = constants.AnimComSetWeaponAngle, param = -14 },
			{ dur = 100, num = 7, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "idle" },
		}
	},
	{
	name = "morph_out";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComSetShadow, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComPlaySound, param = 0, txt = "morph.ogg" },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 56, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -10, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 94, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 94, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetBulletCollidable, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 94, com = nil, param = 0, txt = nil },
		}
	},
	{
	name = "lie";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 46, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 31, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = -5, txt = nil },
			{ dur = 200, num = 96, com = nil, param = 0, txt = nil },
		}
	},
	{
	name = "roll";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComShootBeh, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 46, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 31, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 6, txt = nil },
			{ dur = 600, num = 95, com = nil, param = 0, txt = nil },
			{ dur = 100, num = 95, com = nil, param = 0, txt = nil },
		}
	},
	{
	name = "morph_in";
	frames =
		{
			{ dur = 0, num = 0, com = constants.AnimComRealW, param = 56, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealH, param = 74, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealX, param = -10, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComRealY, param = 0, txt = nil },
			{ dur = 50, num = 94, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetBulletCollidable, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetShadow, param = 1, txt = nil },
			{ dur = 50, num = 94, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 94, com = nil, param = 0, txt = nil },
			{ dur = 50, num = 93, com = nil, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetShadow, param = 1, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetWeaponAngle, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComStopMorphing, param = 0, txt = nil },
			{ dur = 0, num = 0, com = constants.AnimComSetAnim, param = 0, txt = "idle" },
		}
	},
}
