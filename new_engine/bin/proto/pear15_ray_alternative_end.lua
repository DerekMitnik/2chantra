texture = "superlaser_end";

z = 1;

animations = 
{
	{
		name = "idle";
		frames = 
		{
			{ com = constants.AnimComInitW; param = 36 },
			{ com = constants.AnimComInitH; param = 67 },
			{ com = constants.AnimComRealX; param = 0 },
			{ com = constants.AnimComRealY; param = 0 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 0 },
			{ dur = 100; num = 1 },
			{ com = constants.AnimComDestroyObject }
		}
	}
};
