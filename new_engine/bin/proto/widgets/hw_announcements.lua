--forbidden
texture = "hw_announcements";

z = 1;

animations =
{
	{
		name = "normal";
		frames =
		{
			{ num = 0 }
		}
	},
	{
		name = "boss";
		frames =
		{
			{ num = 1 }
		}
	}
}
