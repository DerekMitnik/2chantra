texture = "heat_meter";

z = 0.9;

animations =
{
	{
		name = "background";
		frames =
		{
			{ num = 0 }
		}
	},
	{
		name = "bar";
		frames =
		{
			{ num = 1 }
		}
	}
}
