texture = "pear15-portraits";

z = 0.999;

animations = 
{
	{
		name = "left";
		frames =
		{
			{ dur = 100; num = 3 },
			{ com = constants.AnimComLoop }
		}
	},
	{
		name = "right";
		frames =
		{
			{ com = constants.AnimComMirror },
			{ dur = 100; num = 3 }
		}
	}
}
