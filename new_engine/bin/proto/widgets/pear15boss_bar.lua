texture = "boss_bar";

z = 0.9;

animations =
{
	{
		name = "background";
		frames =
		{
			{ num = 1 }
		}
	},
	{
		name = "bar";
		frames =
		{
			{ num = 0 }
		}
	}
}
