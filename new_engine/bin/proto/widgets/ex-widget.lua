--forbidden
name = "ex-widget";
texture = "ex";
FunctionName = "CreateSprite";

z = 0.999;

animations =
{
	{
		name = "idle";
		frames =
		{
			{ dur = 100; num = 3 },
			{ com = constants.AnimComLoop },
		}
	},
}
