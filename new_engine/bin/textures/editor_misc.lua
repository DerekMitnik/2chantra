count = 8;
main = {
	{ x = 32, y = 0, w = 32, h = 32 },		-- frame 0
	{ x = 0, y = 0, w = 32, h = 32 },		-- frame 1
	{ x = 32, y = 32, w = 32, h = 32 },		-- frame 2
	{ x = 0, y = 32, w = 32, h = 32 },		-- frame 3
	{ x = 96, y = 0, w = 32, h = 32 },		-- frame 4
	{ x = 64, y = 0, w = 32, h = 32 },		-- frame 5
	{ x = 96, y = 32, w = 32, h = 32 },		-- frame 6
	{ x = 64, y = 32, w = 32, h = 32 }		-- frame 7
}
