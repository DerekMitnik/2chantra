main = {
	{ x = 0, y = 0, w = 32, h = 5 },
	{ x = 0, y = 6, w = 32, h = 5 },
	{ x = 0, y = 12, w = 32, h = 5 },
	{ x = 0, y = 18, w = 32, h = 5 },
	{ x = 0, y = 24, w = 32, h = 5 }
}

overlay =
{
	{
		{ x = 0, y = 0, w = 32, h = 5, ox = 0, oy = 0 },
		{ x = 0, y = 6, w = 32, h = 5, ox = 0, oy = 0 },
		{ x = 0, y = 12, w = 32, h = 5, ox = 0, oy = 0 },
		{ x = 0, y = 18, w = 32, h = 5, ox = 0, oy = 0 },
		{ x = 0, y = 24, w = 32, h = 5, ox = 0, oy = 0 }
	}
}
