main = {
	{ x = 0, y = 0, w = 64, h = 48 },	-- frame 0
	{ x = 64, y = 0, w = 64, h = 48 },	-- frame 1
	{ x = 0, y = 48, w = 64, h = 48 },	-- frame 2
	{ x = 64, y = 48, w = 10, h = 48 },	-- frame 3
	{ x = 78, y = 48, w = 10, h = 48 },	-- frame 4
	{ x = 92, y = 48, w = 10, h = 48 }	-- frame 5
}
