count = 8
main = {
	{ x = 0, y = 0, w = 128, h = 128 },	-- frame 0
	{ x = 128, y = 0, w = 128, h = 128 },	-- frame 1
	{ x = 256, y = 0, w = 128, h = 128 },	-- frame 2
	{ x = 384, y = 0, w = 128, h = 128 },	-- frame 3
	{ x = 512, y = 0, w = 128, h = 128 },	-- frame 4
	{ x = 639, y = 0, w = 128, h = 128 },	-- frame 5
	{ x = 767, y = 0, w = 128, h = 128 },	-- frame 6
	{ x = 896, y = 0, w = 128, h = 128 }	-- frame 7
}
