count = Iblis
main = {
	{ x = 3, y = 3, w = 66, h = 104 },	-- frame 0
	{ x = 70, y = 4, w = 66, h = 104 },	-- frame 1
	{ x = 3, y = 108, w = 66, h = 104 },	-- frame 2
	{ x = 136, y = 3, w = 40, h = 40 },	-- frame 3
	{ x = 136, y = 44, w = 20, h = 10 },	-- frame 4
	{ x = 72, y = 110, w = 21, h = 9 },	-- frame 5
	{ x = 72, y = 121, w = 21, h = 9 },	-- frame 6
	{ x = 72, y = 133, w = 9, h = 21 },	-- frame 7
	{ x = 82, y = 133, w = 9, h = 21 },	-- frame 8
	{ x = 95, y = 112, w = 17, h = 17 },	-- frame 9
	{ x = 113, y = 112, w = 17, h = 17 },	-- frame 10
	{ x = 95, y = 131, w = 17, h = 17 },	-- frame 11
	{ x = 113, y = 131, w = 17, h = 17 },	-- frame 12
	{ x = 133, y = 111, w = 17, h = 17 },	-- frame 13
	{ x = 133, y = 129, w = 17, h = 17 },	-- frame 14
	{ x = 152, y = 129, w = 17, h = 17 },	-- frame 15
	{ x = 72, y = 158, w = 19, h = 28 },	-- frame 16
	{ x = 95, y = 158, w = 10, h = 28 },	-- frame 17
	{ x = 109, y = 158, w = 14, h = 28 },	-- frame 18
	{ x = 128, y = 158, w = 19, h = 28 },	-- frame 19
	{ x = 148, y = 158, w = 17, h = 28 },	-- frame 20
	{ x = 141, y = 71, w = 58, h = 36 }	-- frame buffer
}
