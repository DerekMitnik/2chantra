count = 25
main = {
	{ x = 18, y = 5, w = 11, h = 68 },	-- frame 0
	{ x = 5, y = 78, w = 40, h = 21 },	-- frame 1
	{ x = 6, y = 109, w = 40, h = 11 },	-- frame 2
	{ x = 52, y = 19, w = 11, h = 9 },	-- frame 3
	{ x = 52, y = 35, w = 11, h = 10 },	-- frame 4
	{ x = 52, y = 51, w = 9, h = 10 },	-- frame 5
	{ x = 52, y = 67, w = 12, h = 14 },	-- frame 6
	{ x = 52, y = 83, w = 13, h = 15 },	-- frame 7
	{ x = 52, y = 99, w = 13, h = 14 },	-- frame 8
	{ x = 52, y = 115, w = 14, h = 12 },	-- frame 9
	{ x = 70, y = 5, w = 9, h = 68 },	-- frame 10
	{ x = 81, y = 5, w = 10, h = 68 },	-- frame 11
	{ x = 94, y = 5, w = 9, h = 68 },	-- frame 12
	{ x = 105, y = 5, w = 9, h = 68 },	-- frame 13
	{ x = 115, y = 5, w = 9, h = 68 },	-- frame 14
	{ x = 126, y = 5, w = 11, h = 67 },	-- frame 15
	{ x = 136, y = 5, w = 11, h = 68 },	-- frame 16
	{ x = 150, y = 5, w = 10, h = 68 },	-- frame 17
	{ x = 164, y = 5, w = 10, h = 68 },	-- frame 18
	{ x = 176, y = 5, w = 10, h = 67 },	-- frame 19
	{ x = 190, y = 5, w = 9, h = 68 },	-- frame 20
	{ x = 202, y = 5, w = 9, h = 69 },	-- frame 21
	{ x = 215, y = 5, w = 9, h = 68 },	-- frame 22
	{ x = 72, y = 77, w = 40, h = 22 },	-- frame 23
	{ x = 116, y = 76, w = 42, h = 24 }	-- frame 24
}
