count = 6
main = {
	{ x = 0, y = 0, w = 16, h = 144 },		-- frame 0
	{ x = 16, y = 0, w = 16, h = 144 },		-- frame 1
	{ x = 32, y = 0, w = 16, h = 144 },		-- frame 2
	{ x = 48, y = 0, w = 16, h = 144 },		-- frame 3
	{ x = 64, y = 0, w = 16, h = 144 },		-- frame 4
	{ x = 80, y = 0, w = 16, h = 144 }		-- frame 5
}
