count = 8
main = {
	{ x = 0, y = 0, w = 89, h = 89 },	-- frame 0
	{ x = 89, y = 0, w = 89, h = 89 },	-- frame 1
	{ x = 0, y = 89, w = 89, h = 89 },	-- frame 2
	{ x = 89, y = 89, w = 89, h = 89 },	-- frame 3
	{ x = 0, y = 200, w = 200, h = 200 },	-- frame 4
	{ x = 200, y = 200, w = 200, h = 200 },	-- frame 5
	{ x = 400, y = 200, w = 200, h = 200 },	-- frame 6
	{ x = 600, y = 200, w = 200, h = 200 }	-- frame 7
}
