count = 28
main = {
	{ x = 0, y = 0, w = 16, h = 16 },	-- frame 0
	{ x = 16, y = 0, w = 16, h = 16 },	-- frame 1
	{ x = 0, y = 16, w = 16, h = 16 },	-- frame 2
	{ x = 16, y = 16, w = 16, h = 16 },	-- frame 3
	{ x = 0, y = 32, w = 16, h = 16 },	-- frame 4
	{ x = 16, y = 32, w = 16, h = 16 },	-- frame 5
	{ x = 0, y = 48, w = 16, h = 16 },	-- frame 6
	{ x = 16, y = 48, w = 16, h = 16 },	-- frame 7
	{ x = 32, y = 0, w = 16, h = 16 },	-- frame 8
	{ x = 48, y = 0, w = 16, h = 16 },	-- frame 9
	{ x = 32, y = 16, w = 16, h = 16 },	-- frame 10
	{ x = 48, y = 16, w = 16, h = 16 },	-- frame 11
	{ x = 32, y = 32, w = 16, h = 16 },	-- frame 12
	{ x = 48, y = 32, w = 16, h = 16 },	-- frame 13
	{ x = 32, y = 48, w = 16, h = 16 },	-- frame 14
	{ x = 48, y = 48, w = 16, h = 16 },	-- frame 15
	{ x = 64, y = 0, w = 16, h = 16 },	-- frame 16
	{ x = 80, y = 0, w = 16, h = 16 },	-- frame 17
	{ x = 64, y = 16, w = 16, h = 16 },	-- frame 18
	{ x = 80, y = 16, w = 16, h = 16 },	-- frame 19
	{ x = 96, y = 0, w = 16, h = 16 },	-- frame 20
	{ x = 96, y = 16, w = 16, h = 16 },	-- frame 21
	{ x = 113, y = 0, w = 15, h = 16 },	-- frame 22
	{ x = 113, y = 16, w = 15, h = 16 },	-- frame 23
	{ x = 113, y = 32, w = 15, h = 16 },	-- frame 24
	{ x = 62, y = 34, w = 24, h = 25 },	-- frame 25
	{ x = 85, y = 34, w = 25, h = 25 },	-- frame 26
	{ x = 0, y = 0, w = 0, h = 0 }	-- frame 27
}
