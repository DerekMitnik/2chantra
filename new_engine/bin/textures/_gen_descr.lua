-- ��� ������� ������� Lua-���� �������� ������ ��������, ������������� � ���������� �����
function gen_lua(name, fx, fy, fw, fh, width, height, count)
	name = name..".lua"
	local f = io.open (name, "w+")
	
	assert(width >= fw, string.format("assertion failed on %s, width(%d) >= fw(%d)", name, width, fw))
	assert(height >= fh, string.format("assertion failed on %s, height(%d) >= fh(%d)", name, height, fh))
	assert(width >= fx, string.format("assertion failed on %s, width(%d) >= fx(%d)", name, width, fx))
	assert(height >= fy, string.format("assertion failed on %s, height(%d) >= fy(%d)", name, height, fy))
	assert(width >= fx + fw, string.format("assertion failed on %s, width(%d) >= fx + fw(%d)", name, width, fx + fw))
	assert(height >= fy + fh, string.format("assertion failed on %s, height(%d) >= fy + fh(%d)", name, height, fy + fh))
	
	if count == 0 then count = 1 end
	
	f:write(string.format("count = %d\n", count))
	f:write("main = {\n")
	
	local colCount = 1;
	local colCount = math.floor(width / fw)
	
	for i = 0,(count-1) do
		local col = i % colCount
		local row = math.floor(i / colCount)

		local x1 = fx + col * fw
		local y1 = fy + row * fh

		f:write(string.format("\t{ x = %d, y = %d, w = %d, h = %d }", x1, y1, fw, fh))
		if i < (count-1) then f:write(",") end
		f:write(string.format("\t\t-- frame %d\n", i))
		
	end
	
	f:write("}\n")

	f:flush()
	f:close()

end

gen_lua("2nd_plan", 0, 0, 640, 480, 1024, 512, 1)
gen_lua("3nd_plan", 0, 0, 640, 480, 1024, 512, 1)
gen_lua("3nd_plan_test", 0, 0, 640, 480, 1024, 512, 1)
gen_lua("4nd_plan", 0, 0, 640, 480, 1024, 512, 1)
gen_lua("ammo", 0, 0, 32, 32, 256, 32, 6)
gen_lua("bkg-black", 0, 0, 512, 512, 512, 512, 1)
--gen_lua("btard-overlay1", 0, 0, fw, fh, width, height, count)
--gen_lua("btard_overlay", 0, 896, 256, 128, 1024, 2048, 28)		-- ��� �������� ������� ��� �������
gen_lua("btard_o", 0, 0, 256, 128, 1024, 2048, 28)
gen_lua("bullets-sfg5000", 0, 0, 64, 32, 256, 64, 7)
gen_lua("bullets", 0, 0, 64, 32, 256, 64, 7)
gen_lua("candle", 0, 0, 16, 38, 32, 64, 2)
gen_lua("dust-land", 0, 0, 128, 64, 512, 128, 6)
gen_lua("dust-run", 0, 0, 64, 64, 256, 128, 7)
gen_lua("dust-stop", 0, 0, 64, 64, 256, 64, 4)
gen_lua("endbutton", 0, 0, 125, 150, 256, 256, 2)
gen_lua("flamer", 0, 0, 32, 32, 256, 32, 8)
gen_lua("flash-angle", 0, 0, 128, 64, 512, 128, 7)
gen_lua("flash-straight", 0, 0, 128, 32, 512, 32, 3)
gen_lua("grenade-explosion", 0, 0, 64, 64, 256, 64, 4)
gen_lua("grenade", 0, 0, 32, 32, 256, 64, 10)
gen_lua("gui", 0, 0, 640, 42, 1024, 64, 1)
gen_lua("gui-blue", 0, 0, 256, 64, 256, 256, 3)
gen_lua("gui-red", 0, 0, 64, 32, 64, 128, 3)
gen_lua("invincibility-bubble", 0, 0, 128, 128, 128, 128, 1)
gen_lua("laser-like-angle", 0, 0, 32, 64, 32, 1024, 10)
gen_lua("laser-like", 0, 0, 32, 8, 32, 64, 5)
gen_lua("logo-cold", 0, 0, 512, 256, 512, 256, 1)
gen_lua("logo-hot", 0, 0, 512, 256, 512, 256, 1)
gen_lua("matrix_block", 0, 0, 32, 32, 128, 64, 5)
gen_lua("matrix1", 0, 0, 16, 144, 256, 512, 6)
--gen_lua("pblood", 0, 0, fw, fh, width, height, count)
--gen_lua("pteleport", 0, 0, fw, fh, width, height, count)
gen_lua("phys_floor", 0, 0, 32, 32, 32, 32, 1)
gen_lua("ruins-back-dark", 0, 0, 64, 64, 64, 64, 1)
gen_lua("ruins-back-dark-left", 0, 0, 64, 64, 64, 64, 1)
gen_lua("ruins-back-dark-right", 0, 0, 64, 64, 64, 64, 1)
gen_lua("ruins-back-edge-right", 0, 0, 18, 31, 32, 32, 1)
gen_lua("ruins-back-light", 0, 0, 64, 64, 64, 64, 1)
gen_lua("ruins-back-light-right-broken", 0, 0, 64, 64, 64, 64, 1)
gen_lua("ruins-back-light-right-broken2", 0, 0, 64, 64, 64, 64, 1)
gen_lua("ruins-back-light-right-broken-corner", 0, 0, 64, 64, 64, 64, 1)
gen_lua("ruins-back-top-broken", 0, 0, 128, 21, 128, 32, 1)
gen_lua("ruins-back-wall", 0, 0, 64, 64, 64, 64, 1)
gen_lua("ruins-back-window1", 0, 0, 23, 61, 32, 64, 1)
gen_lua("ruins-back-window2", 0, 0, 23, 64, 32, 64, 1)
gen_lua("ruins-back-window3", 0, 0, 23, 64, 32, 64, 1)
gen_lua("ruins-back-window4", 0, 0, 42, 227, 64, 256, 1)
gen_lua("ruins-block1", 0, 0, 31, 16, 32, 16, 1)
gen_lua("ruins-block2", 0, 0, 25, 16, 32, 16, 1)
gen_lua("ruins-block3", 0, 0, 26, 16, 32, 16, 1)
gen_lua("ruins-block4", 0, 0, 18, 31, 32, 32, 1)
gen_lua("ruins-block5", 0, 0, 18, 31, 32, 32, 1)
gen_lua("ruins-block6", 0, 0, 62, 63, 64, 64, 1)
gen_lua("ruins-column", 0, 0, 35, 146, 64, 256, 1)
gen_lua("sohchan", 0, 0, 128, 128, 1024, 2048, 93)
gen_lua("sohchan_figure", 0, 0, 125, 150, 256, 256, 2)
gen_lua("unyl_sprite", 0, 0, 128, 128, 1024, 2048, 86)
gen_lua("vegetable1", 0, 0, 32, 32, 256, 128, 19)
gen_lua("vegetable3", 0, 0, 32, 32, 128, 256, 23)
gen_lua("wakabamark", 0, 0, 16, 16, 256, 32, 21)
gen_lua("window", 0, 0, 78, 41, 128, 64, 1)
