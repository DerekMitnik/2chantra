count = 9
main = {
	{ x = 0, y = 0, w = 64, h = 43 },	-- frame 0
	{ x = 64, y = 0, w = 64, h = 43 },	-- frame 1
	{ x = 0, y = 46, w = 64, h = 43 },	-- frame 2
	{ x = 64, y = 46, w = 64, h = 43 },	-- frame 3
	{ x = 0, y = 91, w = 64, h = 43 },	-- frame 4
	{ x = 64, y = 91, w = 64, h = 43 },	-- frame 5
	{ x = 0, y = 137, w = 64, h = 43 },	-- frame 6
	{ x = 64, y = 137, w = 64, h = 43 },	-- frame 7
	{ x = 0, y = 183, w = 64, h = 43 }	-- frame 8
}
