count = 13
main = {
	{ x = 0, y = 2, w = 36, h = 72 },	-- frame 0
	{ x = 56, y = 2, w = 36, h = 72 },	-- frame 1
	{ x = 113, y = 2, w = 36, h = 72 },	-- frame 2
	{ x = 170, y = 2, w = 36, h = 72 },	-- frame 3
	{ x = 285, y = 2, w = 55, h = 70 },	-- frame 4
	{ x = 347, y = 2, w = 50, h = 70 },	-- frame 5
	{ x = 403, y = 2, w = 46, h = 70 },	-- frame 6
	{ x = 465, y = 1, w = 60, h = 70 },	-- frame 7
	{ x = 534, y = 2, w = 60, h = 70 },	-- frame 8
	{ x = 606, y = 2, w = 64, h = 70 },	-- frame 9
	{ x = 676, y = 2, w = 60, h = 70 },	-- frame 10
	{ x = 803, y = 2, w = 60, h = 70 },	-- frame 11
	{ x = 871, y = 2, w = 60, h = 70 }	-- frame 12
}
