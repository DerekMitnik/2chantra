local mapname
local command
local val
local command_effects = 
{
	["execute"]=function( str ) LockScore() (loadstring( str ) or function() Log("Bad execute:"..str) end)() end,
	["config"]=function( str ) dofile( "config/"..str..".lua" ) end,
	["character"]=function( str ) vars.character = str end,
	["difficulty"]=function( str ) local diff = tonumber(str) if diff then difficulty = diff end end, 
}

function parse_commandline( _ARGC, _ARGV )
	for i = 2, _ARGC do
		command = nil
		command, val = string.match( string.lower(_ARGV[i]), "^%-([a-z]+)=(.*)" )
		if command then
			if command_effects[command] then 
				command_effects[command](val)
			elseif Game and Game.command_effects and Game.command_effects[command] then
				Game.command_effects[command](val)
			else
				Log( string.format("unknown commandline parameter: %s", command) )
				print( string.format("unknown commandline parameter: %s", command) )
			end
		else
			mapname = _ARGV[i]
		end
	end
	if mapname then
		Game.LoadLastProfile()
		Game.custom_map = true
		LockScore()
		assert(Loader.startGame(mapname)); Menu.hideMainMenu();
	end
end
