-- A script type that is stored both as a script and as a function.
-- Required for editor.

local Script_mt = {
	__call = function( _, str, env  )
		local a = {}
		setmetatable( a, Script )
		local result
		a.script, result = loadstring( str )
		if not a.script then
			Log( string.format( "In Script creation: %s", result ) )
			return nil
		end
		if env then
			a.env = env
			local f_env = {}
			f_env.__index = function( _, key )
				return a.env[key] or _G[key]
			end
			setmetatable( f_env, f_env )
			setfenv( a.script, f_env )
		end
		a.string = str
		return a
	end
}

Script = {}
Script.__index = Script
setmetatable( Script, Script_mt )

Script.__call = function( ... )
	if (type(arg[1]) == 'table') then
		return table.remove( arg, 1 ).script( unpack( arg ) )
	else
		Log( "Error in Script metatable event __call!" )
	end
end

Script.__tostring = function( t )
	return t.string
end

function Script.update( t, str )
	if not (type(t) == 'table') then
		Log( "Error in Script:update: use ':', not '.'!" )
	else
		local result, msg
		result, msg = loadstring( str )
		if not result then
			Log( string.format("Error in Script:update, script not updated: %s", msg) )
			return
		end
		t.script = result
		t.string = str
	end
end
