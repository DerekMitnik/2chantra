require( "menus" )
require("weather")

Menu = PagedMenu.create()
Menu.mn = false

local firstKeyConfigPage = 102

local MAIN_MENU = 1
local OPTIONS1 = 2
local OPTIONS2 = 3
local CONTROLS_ROOT = 4
local SOUND_OPTIONS = 5
local GAME_CONFIG = 6
local GAME_OVER = 7
local LEVEL_END = 8
local LOAD_MENU = 9
local SAVE_MENU = 10
local SEND_SCORE = 11
local EXTRA = 29
local PRESS_BUTTON = 101
local START = 32

-------------------------------------------------------------------------------------------------------------------------------------------
wfonts = {}

_WidgetSetCaptionFont = WidgetSetCaptionFont
WidgetSetCaptionFont = function( w, font, size )
	if w then
		if font == "default" then
			wfonts[w] = nil
		else
			wfonts[w] = true
		end
	end
	_WidgetSetCaptionFont( w, font, size )
end

_DestroyWidget = DestroyWidget
DestroyWidget = function( w )
	if w then
		wfonts[w] = nil
		_DestroyWidget( w )
	end
end

_WidgetSetCaption = WidgetSetCaption
WidgetSetCaption = function( w, caption, multiline )
	if not wfonts[w] then
		local cpt = string.gsub( caption, "[�ɸ�]", 
		{ 
			["�"]="�",
			["�"]="�",
			["�"]="�",
			["�"]="�",
		} )
		_WidgetSetCaption( w, cpt, multiline )
	else
		_WidgetSetCaption( w, caption, multiline )
	end
end

-------------------------------------------------------------------------------------------------------------------------------------------
local dsx, dsy = GetCaptionSize( "dialogue", dictionary_string("Active profile:") )
local profile_window = CreateWidget( constants.wt_Panel, "", nil, 0, 0, 1, 1 )
WidgetSetSprite( profile_window, "window1" )
WidgetSetSpriteColor( profile_window, { 0, 0, 0, .5 } )
local profile_active = CreateWidget( constants.wt_Label, "", profile_window, 10, 10, 1, 1 )
WidgetSetCaptionFont( profile_active, "dialogue" )
WidgetSetCaption( profile_active, dictionary_string("Active profile:") )
WidgetSetCaptionColor( profile_active, { 1, 1, 1, 1 }, false )
local profile_name = CreateWidget( constants.wt_Button, "", profile_window, 10, 20+dsy, 1, 1 )
WidgetSetCaptionFont( profile_name, "dialogue" )
WidgetSetLMouseClickProc( profile_name, function() Menu.showProfileMenu() end )
WidgetSetCaptionColor( profile_name, { 1, 1, .7, 1 }, false )
WidgetSetCaptionColor( profile_name, { .7, .7, 1, 1 }, true )

WidgetSetFocusable( profile_window, false )
WidgetSetFocusable( profile_name, false )

Menu._show = Menu.show
Menu._hide = Menu.hide

local mouse_active = false
local code_possible = false
local code_key_proc = nil

local function removeCodeKeyProc()
	Loader.removeSlaveKeyReleaseProcessor(code_key_proc)
	code_key_proc = nil
end

Menu.show = function( mn, page )
	if page == 1 then
		if not code_possible then
			code_possible = true
			local code_progress = 1
			code_key_proc = Loader.addSlaveKeyReleaseProcessor( function(key)
				if not code_possible then
					removeCodeKeyProc()
				end
				local code = "uuddlrlrfj"
				local code_key =
				{
					["up"] = "u",
					["down"] = "d",
					["left"] = "l",
					["right"] = "r",
					["fire"] = "f",
					["jump"] = "j"
				}
				for k, v in pairs( code_key ) do
					if isConfigKeyPressed( key, k ) then
						if v == string.sub( code, code_progress, code_progress ) then
							code_progress = code_progress + 1
							if code_progress > #code then
								Game.GiveTutorialBonus()
								code_progress = 1
							end
							return (code_progress > 5)
						else
							code_progress = 1
							return false
						end
					end
				end
				code_progress = 1
				return false
			end )
		end
		Menu.updateProfileWidget()
		mouse_active = true
		GlobalSetMouseKeyReleaseProc( function( key )
			if not Menu.pages[1].visible then
				GlobalSetMouseKeyReleaseProc( nil )
				return
			end
			if key == 0 then
				local name = Game.profile.name
				local x, y = GetMousePos( true )
				local sx, sy = GetCaptionSize( "dialogue", name )
				local wx = math.max( sx, dsx ) + 20
				if x > CONFIG.scr_width - wx and y < sy + dsy + 30 then
					Menu.showProfileMenu()
				elseif old_mouse then
					old_mouse()
				end
			end
		end )
		WidgetSetVisible( profile_window, true )
	else
		if code_possible then
			code_possible = false
			removeCodeKeyProc()
		end
		WidgetSetVisible( profile_window, false )
		if mouse_active then
			GlobalSetMouseKeyReleaseProc( nil )
			mouse_active = false
		end
	end
	return Menu._show( mn, page )
end

Menu.hide = function( mn, page )
	if code_possible then
		code_possible = false
		removeCodeKeyProc()
	end
	WidgetSetVisible( profile_window, false )
	if mouse_active then
		GlobalSetMouseKeyReleaseProc( nil )
		mouse_active = false
	end
	Menu._hide( mn, page )
end

Menu.updateProfileWidget = function()
	local name = Game.profile.name
	local sx, sy = GetCaptionSize( "dialogue", name )
	local wx = math.max( sx, dsx ) + 20
	WidgetSetPos( profile_window, CONFIG.scr_width - math.max( sx, dsx ) - 30, 10 )
	WidgetSetPos( profile_active, (wx - dsx)/2, 10, true )
	WidgetSetPos( profile_name, (wx - sx)/2, 20 + dsy, true )
	WidgetSetSize( profile_window, math.max( sx, dsx ) + 20, sy + dsy + 30 )
	WidgetSetSize( profile_name, sx, sy )
	WidgetSetCaption( profile_name, name )
end

-------------------------------------------------------------------------------------------------------------------------------------------
--Page 1: Main menu
Menu:addPage( MAIN_MENU, SimpleMenu.create( {320, 300}, 0, 0, 30, 10 ) )
vars.game_mode = "STORY"
vars.start_map = "2011-1script"
vars.test_map = "trialmap"
vars.tutorial_map = "pear15tutorial"
vars.backstory_map = "pear15backstory"
vars.difficulty = 1
vars.menu_sounds = true
	
Menu:add( MWSpace.create( 0, 15 ), MAIN_MENU )
Menu:add( 
			MWFunctionButton.create( 
					{name="default", color={1,1,1,1}, active_color={.2,1,.2,1}}, 
					"START",
					0, 
					function (sender, param) 
						RemoveKeyProcessors()
						GlobalSetKeyReleaseProc( function(key) 
							if isConfigKeyPressed( key, "gui_nav_menu" ) or isConfigKeyPressed( key, "gui_nav_decline" )then
								Menu:goBack()
								RestoreKeyProcessors()
							end
						end )
						Menu:show(START)
						Menu:saveBackPage(MAIN_MENU)
					end, 
					nil,
					{name="wakaba-widget"}
			), MAIN_MENU 
		)

Menu:add( MWSpace.create( 0, 10 ), MAIN_MENU )
do
local load
load = Menu:add(
			MWFunctionButton.create(
					{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},
					"LOAD",
					0,
					function (sender, param)
						RemoveKeyProcessors()
						GlobalSetKeyReleaseProc( function(key)
							if isConfigKeyPressed( key, "gui_nav_menu" ) or isConfigKeyPressed( key, "gui_nav_decline" )then
								Menu:goBack()
								RestoreKeyProcessors()
							end
						end )
						Menu.showLoadMenu()
						Menu:saveBackPage(MAIN_MENU, function() Menu:gainFocus(load) end)
					end,
					nil,
					{name="wakaba-widget"}
			), MAIN_MENU
		)
end

do
local change_profile
change_profile = Menu:add(
			MWFunctionButton.create(
					{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},
					"CHANGE PROFILE",
					0,
					function (sender, param)
						Menu.showProfileMenu( function () Menu:gainFocus(change_profile) end )
					end,
					nil,
					{name="wakaba-widget"}
			), MAIN_MENU
		)
end

--Menu.pages[1].contents[load]:disable()
--Menu:add( MWSpace.create( 0, 10 ), MAIN_MENU )
--[[
local sx, sy = GetCaptionSize( "default",  dictionary_string("DIFFICULTY:") )
Menu:add( 
			MWLabeledButton.create( 
					{name="default", color={1,1,1,1}, active_color={1,0,0,1}}, 
					"DIFFICULTY:", 
					15, 
					MWCycleButton.create(
						{name="default", color={.8,.8,1,1}, active_color={.6,.6,1,1}},
						"",
						-1,
						{ name = "wakaba-widget" },
						function ( sender, new_value )
							if new_value > 1 then
								WidgetSetCaptionColor( sender, {1, .3, .3, 1}, false )
								WidgetSetCaptionColor( sender, {1, .1, .1, 1}, true )
							elseif new_value < 1 then
								WidgetSetCaptionColor( sender, {.3, 1, .3, 1}, false )
								WidgetSetCaptionColor( sender, {.0, 1, .0, 1}, true )
							else
								WidgetSetCaptionColor( sender, {.8, .8, 1, 1}, false )
								WidgetSetCaptionColor( sender, {.6, .6, 1, 1}, true )
							end
						end,
						"difficulty",
						{1, 2, 0.5},
						{"Just another ktd.", "Novak-kun mode!", "I'm _tsu young to die."}
					)
			), MAIN_MENU
		)
--]]
--[[
local sx, sy = GetCaptionSize( "default",  dictionary_string("MODE:") )
Menu:add( 
			MWLabeledButton.create( 
					{name="default", color={1,1,1,1}, active_color={1,0,0,1}}, 
					"MODE:", 
					15, 
					MWCycleButton.create(
						{name="default", color={.8,.8,1,1}, active_color={.6,.6,1,1}},
						"",
						-1,
						{ name = "wakaba-widget" },
						function ( sender, new_value )
						--
						--	if new_value == "STORY" then
						--		vars.start_map = "1-1script"
						--	elseif new_value == "SURVIVAL" then
						--		vars.start_map = "survival"
						--	else
						--		vars.start_map = "1-1script"
						--	end
						--
							vars.start_map = "2011-lab"
						end,
						"game_mode",
						{"STORY"}
					)
			) 
		)
--]]
--Menu:add( MWSpace.create( 0, 10 ) )
do
local highscores
highscores = Menu:add(
			MWFunctionButton.create(
					{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},
					"HIGHSCORES",
					0,
					function (sender, param)
						Menu:hide()
						Highscores.showFullTable( function() Menu:show(MAIN_MENU); Menu:gainFocus(highscores) end )
					end,
					nil,
					{name="wakaba-widget"}
			), MAIN_MENU
		)
end

do
local extra
extra = Menu:add(
			MWFunctionButton.create(
					{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},
					"EXTRA",
					0,
					function (sender, param)
						RemoveKeyProcessors()
						GlobalSetKeyReleaseProc( function(key)
							if isConfigKeyPressed( key, "gui_nav_menu" ) or isConfigKeyPressed( key, "gui_nav_decline" )then
								Menu:goBack()
								RestoreKeyProcessors()
							end
						end )
						Menu:show(EXTRA)
						Menu:saveBackPage(MAIN_MENU, function() Menu:gainFocus(extra) end)
					end,
					nil,
					{name="wakaba-widget"}
			), MAIN_MENU
		)
end

do
local options
options = Menu:add(
			MWFunctionButton.create(
					{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},
					"OPTIONS",
					0,
					function (sender, param)
						RemoveKeyProcessors()
						GlobalSetKeyReleaseProc( function(key)
							if isConfigKeyPressed( key, "gui_nav_menu" ) or isConfigKeyPressed( key, "gui_nav_decline" )then
								Menu:goBack()
								RestoreKeyProcessors()
							end
						end )
						Menu:show(OPTIONS1)
						Menu:saveBackPage(MAIN_MENU, function() Menu:gainFocus(options) end)
					end,
					nil,
					{name="wakaba-widget"}
			), MAIN_MENU
		)
end

Menu:add( MWSpace.create( 0, 10 ), MAIN_MENU )

Menu:add( 
			MWFunctionButton.create( 
					{name="default", color={1,.5,.5,1}, active_color={1,.0,.0,1}}, 
					"EXIT",
					0, 
					ExitGame, 
					nil,
					{name="wakaba-widget"}
			), MAIN_MENU
		)
Menu:add( MWSpace.create( 0, 15 ) )

function Menu.setMainMenuVisible()
end

function Menu.showMainMenu()
	InitNewGame()
	Menu.mn = true
	SetCamLag( 0 )
	Game.AttachCamToPlayers()
	CamMoveToPos( 0, 0 )

	CONFIG.backcolor_r = 0.000000;
	CONFIG.backcolor_g = 0.000000;
	CONFIG.backcolor_b = 0.000000;
	
	LoadProfileConfig()
	--FadeOutGlobalVolume(5000, true, nil)

	vars.menu_sounds = true
	PlayBackMusic( "music/nsmpr_iwbtt.it", 0.4 )
	LetterBox( false )
	
	local tunnel_parts = {}
	local dt = 0
	local spare
	local tunnel_color = {0, 0, 0, 1}
	local last_tunnel_update = GetCurTime() - 4000
	local tunnel_start_time = GetCurTime()
	local btardis
	
	vars.menu_thread = NewMapThread( function()
		local num = 0
		local timer = 0
		local lightning = math.random( 100, 500 )
		local origin = { 0, 0 }
		btardis = GetObjectUserdata( CreateSprite( "btardis", 0, 0 ) )
		local skip_key = false
		local everything_in_place = false
		local press_any_key
		local menu_shown = false
		Game.LoadLastProfile()
		local prof = not Game.profile.default
		SetSkipKey( function() skip_key = true end, 1000 )
		while true do
			dt = GetCurTime() - (last_tunnel_update or 0)
			spare = (spare or 0) + dt
			local sp = spare / 10
			last_tunnel_update = last_tunnel_update + dt
			for i = 1, sp do
				spare = spare - 10
				if true then
					for k, v in pairs( tunnel_parts ) do
						v.angle = v.angle + 0.01
						v.size = v.size * 1.01
						v.z = v.z + 0.0001
						v.color_part = math.min( v.color_part + 0.05, 1)
						v.x = v.x * 0.99
						v.y = v.y * 0.99
						v.flash = v.flash * 0.9
						SetObjPos( v.sprite, v.x, v.y, v.z )
						SetObjSpriteAngle( v.sprite, v.angle )
						SetObjSpriteColor( v.sprite, 
							{ math.min(v.color[1] * v.color_part + v.flash, 1), 
							  math.min(v.color[2] * v.color_part + v.flash, 1), 
							  math.min(v.color[3] * v.color_part + v.flash, 1), 1 } )
						SetObjRectangle( v.sprite, v.size * 640, v.size * 640 )
						if v.size > 12 then
							SetObjDead( v.sprite )
							table.remove( tunnel_parts, k )
						end
					end
					timer = timer + 1
					if not skip_key then
						if timer == 1000 then
							vars.textwidget = CreateWidget( constants.wt_Picture, "\o/", nil, 0, 0, 2, 1 )
							vars.tws = 1
							vars.twy = 0
							WidgetSetFocusable( vars.textwidget, false )
							WidgetSetSprite( vars.textwidget, "logo-cold" )
							WidgetSetSpriteRenderMethod( vars.textwidget, constants.rsmStretch )
						elseif timer == 2100 then
							--DestroyWidget( vars.textwidget )
							--vars.textwidget = nil
						end
						if vars.textwidget and vars.tws < 256 then
							vars.tws = vars.tws * 1.05
							vars.twy = (CONFIG.scr_height - vars.tws)/2
							vars.twx = CONFIG.scr_width/2 - vars.tws
							WidgetSetPos( vars.textwidget, vars.twx, math.floor(vars.twy) )
							WidgetSetSize( vars.textwidget,  2 * vars.tws, vars.tws )
						elseif vars.textwidget and vars.twy > 10 and timer >= 1500 then
							vars.twy = vars.twy - 0.25
							WidgetSetPos( vars.textwidget, vars.twx, math.floor(vars.twy) )
						elseif vars.textwidget and timer >= 1500 and not vars.menued then
							vars.menued = true
							WidgetSetSpriteColor( Menu.pages[MAIN_MENU].window.widget, { 0, 0, 0, .5 } )
							WidgetSetSpriteColor( Menu.pages[EXTRA].window.widget, { 0, 0, 0, .5 } )
							WidgetSetSpriteColor( Menu.pages[START].window.widget, { 0, 0, 0, .5 } )
							everything_in_place = true
							local psx, psy = GetCaptionSize( "default", dictionary_string("AUTO_TEXT_8") )
							press_any_key = CreateWidget( constants.wt_Label, "", nil, (CONFIG.scr_width-psx)/2, (CONFIG.scr_height+psy)/2, 1, 1 )
							WidgetSetCaptionColor( press_any_key, {1,1,1,1}, false )
							WidgetSetCaption( press_any_key, dictionary_string("AUTO_TEXT_8") )
							Resume( NewThread( function()
								local vis = true
								while not skip_key do
									vis = not vis
									if press_any_key then
										WidgetSetVisible( press_any_key, vis )
									end
									Wait( 1000 )
								end
							end ))
						end
					else
							if press_any_key then
								DestroyWidget( press_any_key )
								press_any_key = nil
							end
							if not everything_in_place then
								everything_in_place = true
								if not vars.textwidget then
									vars.textwidget = CreateWidget( constants.wt_Picture, "\o/", nil, 0, 0, 2, 1 )
									WidgetSetFocusable( vars.textwidget, false )
									WidgetSetSprite( vars.textwidget, "logo-cold" )
									WidgetSetSpriteRenderMethod( vars.textwidget, constants.rsmStretch )
								end
								WidgetSetPos( vars.textwidget, CONFIG.scr_width/2-256, 10 )
								WidgetSetSize( vars.textwidget, 512, 256 )
								vars.menued = true
								WidgetSetSpriteColor( Menu.pages[MAIN_MENU].window.widget, { 0, 0, 0, .5 } )
								WidgetSetSpriteColor( Menu.pages[EXTRA].window.widget, { 0, 0, 0, .5 } )
								WidgetSetSpriteColor( Menu.pages[START].window.widget, { 0, 0, 0, .5 } )
							end
							if not menu_shown then
								Menu:show(MAIN_MENU)
								menu_shown = true
							end
							if not prof then
								prof = true
								Menu:hide()
								Menu.newProfile()
							end
					end
					num = num + 1
					if num > 10 then
						local spr = GetObjectUserdata(CreateSprite( "tunnel", 0, 0 ))
						--SetObjSpriteBlendingMode( spr, constants.bmNone )
						SetObjSpriteColor( spr, {0,0,0,1} )
						table.insert( tunnel_parts, { sprite = spr, angle = math.random(0, 2*math.pi), z = -0.5, size = 0.25, color = { tunnel_color[1], tunnel_color[2], tunnel_color[3] }, color_part = 0, x = origin[1], y = origin[2], flash = 0 } )
						tunnel_color[1] = 0.5 + math.sin( GetCurTime() / 1000 ) / 4
						tunnel_color[2] = 0.5 + math.sin( GetCurTime() / 2000 ) / 4
						tunnel_color[3] = 0.5 + math.sin( GetCurTime() / 3000 ) / 4
						origin[1] = 100* math.cos( GetCurTime()/1000 )
						origin[2] = 100* math.cos( GetCurTime()/2030 )
						num = num - 10
					end
					lightning = lightning - 1
					if lightning <= 0 then
						lightning = math.random( 50, 300 )
						if #tunnel_parts >= 1 and GetCurTime() > tunnel_start_time + 2000 then
							local l = math.random(1, #tunnel_parts )
							if vars.menu_sounds then  PlaySnd( "strike_short.ogg", true ) end
							local lght = GetObjectUserdata( CreateSprite( "tunnel2", tunnel_parts[l].x, tunnel_parts[l].y ) )
							--SetObjSpriteBlendingMode( lght, constants.bmNone )
							SetObjPos( lght, tunnel_parts[l].x, tunnel_parts[l].y, tunnel_parts[l].z )
							SetObjRectangle( lght, 640*tunnel_parts[l].size, 640*tunnel_parts[l].size )
							SetObjSpriteAngle( lght, tunnel_parts[l].angle )
							for j = math.max(1, l-30), math.min(#tunnel_parts, l+30) do
								tunnel_parts[j].flash = 1 - math.abs( l - j )/30
							end
						end
					end
					SetObjPos( btardis, 100*math.sin( GetCurTime() / 1500 ), 100*math.sin( GetCurTime() / 2500 ) )
					SetObjSpriteAngle( btardis, (math.cos( GetCurTime()/ 10000 ) + 0.25*math.sin(GetCurTime()/20000)) * 2 * math.pi )
				end
			end
			Wait( 1 ) 
		end
	end )
	Menu.setMainMenuVisible = function( what )
		if vars.textwidget then
			WidgetSetVisible( vars.textwidget, what )
		end
		for k, v in pairs( tunnel_parts ) do
			SetObjInvisible( v.sprite, not what )
		end
		if btardis then
			SetObjInvisible( btardis, not what )
			SetObjInvisible( vars.btardis_flare, not what )
		end
	end
	vars.menu_thread_cleanup = function()
		if vars.textwidget then
			DestroyWidget( vars.textwidget )
			vars.menued = nil
			vars.textwidget = nil
		end
	end
	Resume( vars.menu_thread )
	Menu:hide()
end

function Menu.hideMainMenu()
	Menu.mn = false
	Weather.clean_slave_weather_proc(Menu)
	if Menu.logo then DestroyWidget( Menu.logo ) end
	Menu:hide()
end
-------------------------------------------------------------------------------------------------------------------------------------------
--Pages 2 and 3: Options from main menu and from in-game
Menu:addPage( OPTIONS1, SimpleMenu.create( {320, 300}, 0, 0, 30, 10 ) )
Menu:addPage( OPTIONS2, SimpleMenu.create( {320, 240}, 0, 0, 30, 10 ) )

function Menu.createCommonOptions( page )
		Menu:add( MWSpace.create(0, 15), page )
		local CONTROLS_BTN
		CONTROLS_BTN = Menu:add(
			MWFunctionButton.create(
				{name="default", color = {1, 1, 1, 1}, active_color = {1, .2, .2, 1}},
				"CONTROLS",
				0,
				function (sender, param)
					--Menu.logo_visible = WidgetGetVisible( Menu.logo )
					--WidgetSetVisible( Menu.logo, false )
					RemoveKeyProcessors()
					GlobalSetKeyReleaseProc( function(key) 
						if isConfigKeyPressed( key, "gui_nav_menu" ) or isConfigKeyPressed( key, "gui_nav_decline" )then
							Menu:goBack()
							RestoreKeyProcessors()
						end
					end )
					Menu:show(CONTROLS_ROOT)
					Menu:saveBackPage(page, function() Menu:gainFocus(CONTROLS_BTN) end)
				end,
				nil,
				{ name = "wakaba-widget" }
			),
			page
		)
		local AUDIO_BTN
		AUDIO_BTN = Menu:add(
			MWFunctionButton.create(
				{name="default", color = {1, 1, 1, 1}, active_color = {1, .2, .2, 1}},
				"AUDIO",
				0,
				function (sender, param)
					RemoveKeyProcessors()
					GlobalSetKeyReleaseProc( function(key) 
						if isConfigKeyPressed( key, "gui_nav_menu" ) or isConfigKeyPressed( key, "gui_nav_decline" )then
							Menu:goBack()
							SaveConfig()
							RestoreKeyProcessors()
						end
					end )
					LoadProfileConfig()
					Menu:updateSoundOptions()
					Menu:show(SOUND_OPTIONS)
					Menu:saveBackPage(page, function() Menu:gainFocus(AUDIO_BTN) end)
				end,
				nil,
				{ name = "wakaba-widget" }
			),
			page
		)
		local VIDEO_BTN
		VIDEO_BTN = Menu:add(
			MWFunctionButton.create(
				{name="default", color = {1, 1, 1, 1}, active_color = {1, .2, .2, 1}},
				"VIDEO",
				0,
				function (sender, param)
					RemoveKeyProcessors()
					Menu:showVideoOptionsPage( page , nil, function() Menu:gainFocus(VIDEO_BTN) end)
				end,
				nil,
				{ name = "wakaba-widget" }
			),
			page
		)
		--[[
		Menu:add(
			MWFunctionButton.create(
				{name="default", color = {1, 1, 1, 1}, color_active = {.7, .7, 1, 1}},
				"GAME",
				0,
				function (sender, param)
					Menu:show(6)
					Menu:saveBackPage(page)
				end,
				nil,
				{ name = "wakaba-widget" }
			),
			page
		)
		--]]
end

--[[
Menu:add(
	MWComposition.create(
		30, {	
				MWFunctionButton.create({name="default", color = {1, .8, .8, .8}, color_active = {1, .7, .7, 1}},
				"RESUME",
				0,
				function (sender, param)
					Menu:hide()
					pop_pause()
					Menu.mn = false
				end,
				nil,
				{ name = "wakaba-widget" }),
				
				MWFunctionButton.create({name="default", color = {1, .8, .8, .8}, color_active = {1, .7, .7, 1}},
				"EXIT",
				0,
				function (sender, param)
					Loader.exitGame()
				end,
				nil,
				{ name = "wakaba-widget" }),
			}
	),
	3
)
-]]
Menu:add( MWSpace.create(0, 15), OPTIONS1 )
Menu:add( MWSpace.create(0, 15), OPTIONS2 )

--[[
vars.music = "CHIPTUNE"
Menu:add( 
			MWLabeledButton.create( 
					{name="default", color={1,1,1,1}, active_color={1,0,0,1}}, 
					"MUSIC:", 
					15, 
					MWCycleButton.create(
						{name="default", color={.8,.8,1,1}, active_color={.6,.6,1,1}},
						"",
						-1,
						{ name = "wakaba-widget", pos = { -(sx+40), -5 } },
						function (sender, value)
							if value == "CHIPTUNE" then
								PlayBackMusic("music/nsmpr_choice.it")
							else
								PlayBackMusic("music/nsmpr_EpilepticMarch.ogg")
							end
						end,
						"music",
						{"CHIPTUNE", "FULL"},
						nil
					)
			),
			3
		)
--]]

Menu:add(
		MWFunctionButton.create({name="default", color = {1, 1, 1, 1}, active_color = {.2, .2, 1, 1}},
				"RESUME",
				0,
				function (sender, param)
					Menu:hide()
					pop_pause()
					Menu.mn = false
				end,
				nil,
				{ name = "wakaba-widget" }),
		OPTIONS2
)

Menu:add(
		MWFunctionButton.create({name="default", color = {1, .8, .8, 1}, active_color = {1, .2, .2, 1}},
				"EXIT",
				0,
				function (sender, param)
					Loader.exitGame()
				end,
				nil,
				{ name = "wakaba-widget" }),
		OPTIONS2
)

Menu:add( MWSpace.create(0, 15), OPTIONS2 )
Menu:add(
		MWFunctionButton.create({name="default", color = {1, 1, 1, 1}, active_color = {.2, .2, 1, 1}},
				"I GIVE UP!",
				0,
				function (sender, param)
					Menu:hide()
					Game.ShowSolution(
						function()
							Menu:show()
						end )
				end,
				nil,
				{ name = "wakaba-widget" }),
		OPTIONS2
)

Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, .8, .8, 1}, active_color = {1, .2, .2, 1}},
		"BACK",
		0,
		function (sender, param)
			RestoreKeyProcessors()
			Menu:goBack()
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	OPTIONS1
)

local RESTART_BUTTON
RESTART_BUTTON = Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, 1, 1, 1}, active_color = {.2, .2, 1, 1}},
		"RESTART",
		0,
		function (sender, param)
			if Loader.level.NO_RESTART then
				return
			end

			Menu:hide()

			local text1 = dictionary_string( "Are you sure you want to restart the level?" )
			local text2 = dictionary_string( "NO" )
			local text3 = dictionary_string( "YES" )

			local sx1, sy1 = GetCaptionSize( "dialogue", text1 )
			local sx2, sy2 = GetCaptionSize( "default", text2 )
			local sx3, sy3 = GetCaptionSize( "default", text3 )
	
			local window = CreateWidget( constants.wt_Panel, "", nil, 0, 0, 40+sx1, 70+sy1+sy2+sy3 )
			WidgetSetSprite( window, "window1" )
			WidgetSetFocusable( window, false )

			local label = CreateWidget( constants.wt_Label, "", window, 20, 20, sx1, sy1 )
			WidgetSetCaptionFont( label, "dialogue" )
			WidgetSetCaptionColor( label, { 0.8, 0.8, 0.8, 1 }, false, {0,0,0,1} )
			WidgetSetCaption( label, text1 )

			local button1 = CreateWidget( constants.wt_Button, "", window, 20+(sx1-sx2)/2, 40+sy1, sx2, sy2 )
			WidgetSetCaptionColor( button1, {1,1,1,1}, false, {0,0,0,1} )
			WidgetSetCaptionColor( button1, {.5,.5,1,1}, true, {0,0,0,1} )
			WidgetSetCaption( button1, text2 )
			WidgetSetLMouseClickProc( button1, function()
				DestroyWidget( window )
				RestoreKeyProcessors()
				Menu:show(OPTIONS2)
			end )

			local button2 = CreateWidget( constants.wt_Button, "", window, 20+(sx1-sx3)/2, 50+sy1+sy2, sx3, sy3 )
			WidgetSetCaptionColor( button2, {1,1,1,1}, false, {0,0,0,1} )
			WidgetSetCaptionColor( button2, {.5,.5,1,1}, true, {0,0,0,1} )
			WidgetSetCaption( button2, text3 )
			WidgetSetLMouseClickProc( button2, function()
				DestroyWidget( window )
				RestoreKeyProcessors()
				Loader.restartGame()
			end )
			
			WidgetSetPos( window, (CONFIG.scr_width-40-sx1)/2, (CONFIG.scr_height-70-sy1-sy2-sy3)/2 )

			RemoveKeyProcessors()
			GlobalSetKeyDownProc( function( key )
				if IsConfKey( key, config_keys.gui_nav_menu ) or IsConfKey( key, config_keys.gui_nav_decline ) then
					RestoreKeyProcessors()
					DestroyWidget( window )
					Menu:show( OPTIONS2 )
				end
			end )

			WidgetGainFocus( button1 )
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	OPTIONS2
)

function Menu.prepareIngameMenu()
	if Loader.level.NO_RESTART then
		Menu.pages[OPTIONS2].contents[RESTART_BUTTON]:disable()
	else
		Menu.pages[OPTIONS2].contents[RESTART_BUTTON]:enable()
	end
end

do
local INFO_BTN
INFO_BTN = Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, 1, 1, 1}, active_color = {.2, 1, .2, 1}},
		"INFO",
		0,
		function (sender, param)
			Info.ShowInfoMenu( Menu, 3 , function () Menu:gainFocus(INFO_BTN) end )
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	OPTIONS2
)
end
--[[
Menu:add(


	MWFunctionButton.create(
		{name="default", color = {1, .8, .8, .8}, color_active = {1, .7, .7, 1}},
		"SAVE GAME",
		0,
		function (sender, param)
			Menu:saveBackPage(3)
			Menu.showSaveMenu()
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	3
)
--]]
--Menu:add( MWSpace.create(0, 15), 3 )

Menu.createCommonOptions(OPTIONS1)
Menu.createCommonOptions(OPTIONS2)

Menu:add( MWSpace.create(0, 15), OPTIONS1 )
Menu:add( MWSpace.create(0, 15), OPTIONS2 )
--------------------------------------------------------------------------------------------------------------------------------------------
--Page 4: controls config

local NEW_CONFIG = deep_copy( CONFIG )

Menu:addPage( CONTROLS_ROOT, SimpleMenu.create( {320, 300}, 0, 0, 30, 10 ) )
do
	Menu:add( MWSpace.create(0, 15), CONTROLS_ROOT )

	for i = 1,constants.configKeySetsNumber do
		local button
		button = MWFunctionButton.create(
							{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},

							(dictionary_string("CONTROLLER") .. " " .. i),
							0,
							function (sender, param, sender_obj)
								RemoveKeyProcessors()
								NEW_CONFIG = deep_copy( CONFIG )
								Menu.resetAllKeyConfigButtonName()
								Menu.lock()
								Menu:saveBackPage(CONTROLS_ROOT, function() button:gainFocus() end)
								Menu:show(sender_obj.keyConfigPageNumber + firstKeyConfigPage)
								Menu:move(CONFIG.scr_width/2, CONFIG.scr_height/2, sender_obj.keyConfigPageNumber + firstKeyConfigPage)
								EnableScreenshots( false )
								if GUI then
									GUI:hide_push()
								end
							end,
							nil,
							{name="wakaba-widget"}
						)
						
		button.setText = function(self, t)
			self.text = dictionary_string("CONTROLLER") .. " " .. self.keyConfigPageNumber+1
			self:updateText()
			if self.menu and self.widget and WidgetGetVisible(self.widget) then 
				self.menu:redraw() 
			end
		end
		
		button.keyConfigPageNumber = i - 1
		Menu:add( button, CONTROLS_ROOT )
	end	
	Menu:add( MWSpace.create(0, 15), CONTROLS_ROOT )
	Menu:add(

		MWFunctionButton.create(
			{name="default", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
			"BACK",
			0,
			function (sender, param)
				--WidgetSetVisible( Menu.logo, (Menu.logo_visible or false) )
				RestoreKeyProcessors()
				--Menu:show(Menu.back)
				Menu:goBack()
				--Menu:show(oldBack or Menu.back)
			end,
			nil,
			{ name = "wakaba-widget" }
		),
		CONTROLS_ROOT
	)	
	Menu:add( MWSpace.create(0, 15), 4 )	
end
--------------------------------------------------------------------------------------------------------------------------------------------
--Page 5: audio config

local MASTER_VOLUME
local MUSIC_VOLUME
local SOUND_VOLUME

function Menu:updateSoundOptions()
	LoadProfileConfig()
	Menu.pages[SOUND_OPTIONS].contents[MASTER_VOLUME].elements[1]:updateBar( CONFIG.volume )
	Menu.pages[SOUND_OPTIONS].contents[MUSIC_VOLUME].elements[1]:updateBar( CONFIG.volume_music )
	Menu.pages[SOUND_OPTIONS].contents[SOUND_VOLUME].elements[1]:updateBar( CONFIG.volume_sound )
end

Menu:addPage( SOUND_OPTIONS, SimpleMenu.create( {320, 300}, 0, 0, 30, 10 ) )
Menu:add( MWSpace.create(0, 15), SOUND_OPTIONS )
Menu:add( MWCaption.create( nil, "VOLUME", 0 ), SOUND_OPTIONS )
Menu:add( MWSpace.create(0, 15), SOUND_OPTIONS )
MASTER_VOLUME = Menu:add( MWLabeledButton.create(
			{name="default", color={1,1,1,1}, active_color={1,0,0,1}},
			"MASTER",
			30,
			MWBar.create(
				{
					background={ name="gui", anim="snd_bkg", w = 1, h = 18 },
					start={ name="gui", anim="snd_main", w = 16, h = 16 },
					bar={ name="gui", anim="snd_block", w = 16, h = 16 }
				},
				{},
				{ name = "volume", start = GetMasterSoundVolume(), per_bar = 0.1, max_bars = 10, max_value = 1.0, min_value = 0, bar_size = { 160, 16 } },
				function ( sender, new_value )
					if (not Menu.mute) or (Menu.mute == "OFF") then
						CONFIG.volume = new_value
						SetMasterSoundVolume( new_value )
						SaveConfig()	
					end
				end,
				{ name = "wakaba-widget", pos = {-20, 0}}
			),
			5,
			1
		),
		SOUND_OPTIONS
)
MUSIC_VOLUME = Menu:add(	MWLabeledButton.create(
			{name="default", color={1,1,1,1}, active_color={1,0,0,1}},
			"MUSIC",
			30,
			MWBar.create(
				{
					background={ name="gui", anim="snd_bkg", w = 1, h = 18 },
					start={ name="gui", anim="snd_main", w = 16, h = 16 },
					bar={ name="gui", anim="snd_block", w = 16, h = 16 }
				},
				{},
				{ name = "volume_music", start = GetMasterMusicVolume(), per_bar = 0.1, max_bars = 10, max_value = 1.0, min_value = 0, bar_size = { 160, 16 } },
				function ( sender, new_value )
					if (not Menu.mute) or (Menu.mute == "OFF") then
						SetMasterMusicVolume( new_value )
						SaveConfig()	
					end
				end,
				{ name = "wakaba-widget", pos = {-20, 0}}
			),
			5,
			1
		),
		SOUND_OPTIONS
)
SOUND_VOLUME = Menu:add(	MWLabeledButton.create(
			{name="default", color={1,1,1,1}, active_color={1,0,0,1}},
			"SOUND",
			30,
			MWBar.create(
				{
					background={ name="gui", anim="snd_bkg", w = 1, h = 18 },
					start={ name="gui", anim="snd_main", w = 16, h = 16 },
					bar={ name="gui", anim="snd_block", w = 16, h = 16 }
				},
				{},
				{ name = "volume_sound", start = GetMasterSFXVolume(), per_bar = 0.1, max_bars = 10, max_value = 1.0, min_value = 0, bar_size = { 160, 16 } },
				function ( sender, new_value )
					if (not Menu.mute) or (Menu.mute == "OFF") then
						SetMasterSFXVolume( new_value )
						SaveConfig()	
					end
				end,
				{ name = "wakaba-widget", pos = {-20, 0}}
			),
			5,
			1
		),
		SOUND_OPTIONS
)
Menu:add( MWSpace.create(0, 5), SOUND_OPTIONS )
local sx, sy = GetCaptionSize( "default",  dictionary_string("MUTE:") )
Menu:add( 
			MWLabeledButton.create( 

					{name="default", color={1,1,1,1}, active_color={1,0,0,1}}, 
					"MUTE:", 
					15, 
					MWCycleButton.create(
						{name="default", color={.8,.8,1,1}, active_color={.6,.6,1,1}},
						"",
						-1,
						{ name = "wakaba-widget" },
						function (sender, new_value)
							Menu.mute = new_value
							if new_value == "ON" then
								Menu.mute_volume = GetMasterSoundVolume()
								SetMasterSoundVolume(0)
							else
								SetMasterSoundVolume(Menu.mute_volume)
							end
						end,
						"mute",
						{"OFF", "ON"},
						{"MUTE_OFF", "MUTE_ON"}
					)
			),
			SOUND_OPTIONS
		)
Menu:add( MWSpace.create(0, 15), SOUND_OPTIONS )
Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
		"BACK",
		0,
		function (sender, param)
			RestoreKeyProcessors()
			SaveConfig()
			Menu:goBack()
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	SOUND_OPTIONS
)
Menu:add( MWSpace.create(0, 15), SOUND_OPTIONS )

--------------------------------------------------------------------------------------------------------------------------------------------
--Page 6: game config
Menu:addPage( GAME_CONFIG, SimpleMenu.create( {320, 300}, 0, 0, 30, 10 ) )
vars.weather = CONFIG.weather
vars.language = CONFIG.language

Menu:add( MWSpace.create(0, 15), GAME_CONFIG )

local sx, sy = GetCaptionSize( "default",  dictionary_string("WEATHER EFFECTS:") )

Menu:add( 
			MWLabeledButton.create( 
					{name="default", color={1,1,1,1}, active_color={1,0,0,1}}, 
					"WEATHER EFFECTS:", 
					15, 
					MWCycleButton.create(
						{name="default", color={.8,.8,1,1}, active_color={.6,.6,1,1}},
						"",
						-1,
						{ name = "wakaba-widget" },
						function (sender, new_value)
							CONFIG.weather = new_value
							LoadConfig()
							SaveConfig()
							Weather.check_weather()
						end,
						"weather",
						{1, 0},
						{"WEATHER_FULL", "WEATHER_NONE"}
					)
			),
			GAME_CONFIG
		)
Menu:add( 
			MWLabeledButton.create( 
					{name="default", color={1,1,1,1}, active_color={1,0,0,1}}, 
					"LANGUAGE:", 
					15, 
					MWCycleButton.create(
						{name="default", color={.8,.8,1,1}, active_color={.6,.6,1,1}},
						"",
						-1,
						{ name = "wakaba-widget"},
						function (sender, new_value)
							CONFIG.language = new_value
							LoadConfig()
							SaveConfig()
							LangChanger.LoadLanguage()
						end,
						"language",
						{"english", "russian"},
						{"ENGLISH", "RUSSIAN"}
					)
			),
			GAME_CONFIG
		)

Menu:add( MWSpace.create(0, 15), GAME_CONFIG )
Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
		"BACK",
		0,
		function (sender, param)
			RestoreKeyProcessors()
			Menu:goBack()
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	GAME_CONFIG
)
		
Menu:add( MWSpace.create(0, 15), GAME_CONFIG )

-------------------------------------------------------------------------------------------------------------------------------------------
--Page 7: Game Over

local RESTART_BUTTON
local SEND_BUTTON

function Menu.showGameOver()
	StopBackMusic()
	if mapvar and mapvar.tmp and mapvar.tmp.goodend then
		PlaySnd( "music/iie_win_jingle.it", true )
	else
		PlaySnd( "music/iie_lose_jingle.it", false )
	end
	if not Menu.gow then
		local widget = CreateWidget( constants.wt_Picture, "GAME OVER, MAN", nil, CONFIG.scr_width/2-173, 150, 1, 1 )
		WidgetSetSprite( widget, "gui" )
		WidgetSetAnim( widget, "game_over" )
		WidgetSetVisible( widget, true )
		WidgetSetFocusable( widget, false )
		Menu.gow = widget
	else
		WidgetSetVisible( Menu.gow, true )
	end
	if mapvar and mapvar.tmp and mapvar.tmp.goodend then
		WidgetSetVisible( Menu.gow, false )
	end
	if Loader.level and Loader.level.NO_RESTART then
		Menu.pages[GAME_OVER].contents[RESTART_BUTTON]:disable()
		Menu.pages[GAME_OVER].dont_grab_focus = true
		WidgetGainFocus(Menu.pages[GAME_OVER].contents[SEND_BUTTON].widget)
	else
		Menu.pages[GAME_OVER].dont_grab_focus = false
		Menu.pages[GAME_OVER].contents[RESTART_BUTTON]:enable()
	end
end

local function gameover_exit()
	WidgetSetVisible( Menu.gow, false )
	Menu.goshown = false
	Menu:hide()
	Menu:clearBackStack()
	Loader.exitGame()
end

Menu:addPage( GAME_OVER, SimpleMenu.create( {320, 280}, 0, 0, 30, 10 ) )
Menu:add( MWSpace.create(0, 15), GAME_OVER )
RESTART_BUTTON = Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, .8, .8, .8}, color_active = {1, .7, .7, 1}},
		"RESTART",
		0,
		function (sender, param)
			WidgetSetVisible( Menu.gow, false )
		Loader.restartGame()
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	GAME_OVER
)
SEND_BUTTON = Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, .8, .8, .8}, color_active = {1, .7, .7, 1}},
		"SEND SCORES AND EXIT TO MENU",
		0,
		function (sender, param)
			WidgetSetVisible( Menu.gow, false )
			Menu:hide()
			Menu:clearBackStack()
			Highscores.showMenu( function()
				Loader.exitGame()
				Menu.goshown = false
			end, function() 
				if mapvar and mapvar.tmp and mapvar.tmp.goodend then
					WidgetSetVisible( Menu.gow, false )
				else
					WidgetSetVisible( Menu.gow, true )
				end
				Menu:show(GAME_OVER) 
			end )
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	GAME_OVER
)
Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, .8, .8, .8}, color_active = {1, .7, .7, 1}},
		"TO MENU",
		0,
		function (sender, param)
			gameover_exit()
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	GAME_OVER
)
Menu:add( MWSpace.create(0, 15), GAME_OVER )


-------------------------------------------------------------------------------------------------------------------------------------------
--Page 8: End level
Menu:addPage( LEVEL_END, SimpleMenu.create( {320, 300}, 0, 0, 30, 10 ) )
Menu:add( MWSpace.create(0, 15), LEVEL_END )
Menu:add( MWCaption.create( nil, "Congratulations", 0 ), LEVEL_END )
Menu:add( MWSpace.create(0, 15), LEVEL_END )
Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, .8, .8, .8}, color_active = {1, .7, .7, 1}},
		"CONTINUE",
		0,
		function (sender, param)
			StopAllSnd()
			pop_pause()
			FadeIn(0)
			SwitchLighting(true)
			Loader.ChangeLevel(Loader.map_name)
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	LEVEL_END
)
Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, .8, .8, .8}, color_active = {1, .7, .7, 1}},
		"SAVE GAME",
		0,
		function (sender, param)
			Menu:saveBackPage(8)
			Menu.showSaveMenu()
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	LEVEL_END
)
Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, .8, .8, .8}, color_active = {1, .7, .7, 1}},
		"EXIT",
		0,
		function (sender, param)
			Menu.unlock()
			pop_pause()
			FadeIn(0)
			SwitchLighting(true)
			Loader.exitGame()
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	LEVEL_END
)
Menu:add( MWSpace.create(0, 15), LEVEL_END )


function Menu.showEndLevelMenu(nextLevel)
	FadeOut({0, 0, 0}, 0)
	SwitchLighting(false)
	StopBackMusic()
	PlaySnd( "music/iie_win_jingle.it", true )
	Menu.lock()
	push_pause(true)
	Loader.map_name = nextLevel
	Menu:show(LEVEL_END)
end

-------------------------------------------------------------------------------------------------------------------------------------------
--Page 9: Load game
--Page 10: Save game

local function createLoadSaveMenu(menu_num, label, button_func, func_get_show_func)

	
	Menu:addPage( menu_num, SimpleMenu.create( {320, 300}, 0, 0, 30, 10 ) )

	Menu:add( MWSpace.create(0, 15), menu_num )
	Menu:add( MWCaption.create( nil, label, 0 ), menu_num )
	Menu:add( MWSpace.create(0, 15), menu_num )
	local slot_buttons = {}
	for i = 1, 12 do
		local button = MWFunctionButton.create(
				{name="default", color = {1, .8, .8, .8}, color_active = {1, .7, .7, 1}},
				"Slot" .. i,
				0,
				button_func,
				nil,
				{ name = "wakaba-widget" }
			)
		slot_buttons[i] = button
		button.slot_num = i
		Menu:add(button, menu_num)
	end
	Menu:add( MWSpace.create(0, 15), menu_num )
	Menu:add(
		MWFunctionButton.create(
			{name="default", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
			"BACK",
			0,
			function (sender, param)
				RestoreKeyProcessors()
				Menu:goBack()
			end,
			nil,
			{ name = "wakaba-widget" }
		),
		menu_num
	)
	Menu:add( MWSpace.create(0, 15), menu_num )
	return slot_buttons
end



local function load_button_lclick (sender, param)
	local butt = MWFunctionButton.widgets[sender]
	if butt and butt.slot_num then 
		RestoreKeyProcessors()
		Menu.hideMainMenu()
		Saver:loadGame(butt.slot_num)
	end
end

local function save_button_lclick (sender, param)
	local butt = MWFunctionButton.widgets[sender]
	if butt and butt.slot_num then  
		local info = {}
		info.mapname = Loader.level.name or "UNNAMED MAP"
		RestoreKeyProcessors()
		Saver:saveGame(butt.slot_num, info)
		Menu:goBack()
	end
end


local load_buttons = createLoadSaveMenu(LOAD_MENU, "Load game", load_button_lclick, get_load_button_show_func)
local save_buttons = createLoadSaveMenu(SAVE_MENU, "Save game", save_button_lclick, get_save_button_show_func)

function Menu.showSaveMenu()
	for k,v in pairs(save_buttons) do
		if Saver:doesExist(v.slot_num) then
			MWTextFocusable.setWidgetText(v.widget, Saver:getSlotName(v.slot_num))
			Menu.pages[SAVE_MENU]:redraw()
		else
			MWTextFocusable.setWidgetText(v.widget, "None")
			Menu.pages[SAVE_MENU]:redraw()
		end
	end
	Menu:show(SAVE_MENU)
end

function Menu.showLoadMenu()
	for k,v in pairs(load_buttons) do
		if Saver:doesExist(v.slot_num) then
			MWTextFocusable.setWidgetText(v.widget, Saver:getSlotName(v.slot_num))
			Menu.pages[LOAD_MENU]:redraw()
			v:enable()
		else
			MWTextFocusable.setWidgetText(v.widget, "None")
			Menu.pages[LOAD_MENU]:redraw()
			v:disable()
		end
	end
	Menu:show(LOAD_MENU)	
end
-------------------------------------------------------------------------------------------------------------------------------------------
--Page 11: Send score

vars.nickname = "Anonymous"


Menu:addPage( SEND_SCORE, SimpleMenu.create( {320, 300}, 0, 0, 30, 10 ) )
Menu:add( MWSpace.create(0, 15), SEND_SCORE )
Menu:add( MWCaption.create( nil, "SEND SCORE", 0 ), SEND_SCORE )
Menu:add( MWSpace.create(0, 15), SEND_SCORE )

local scores_points_caption = MWCaption.create({name="dialogue", color={.7, 1, .7, 1}, active_color={1,1,1,1}}, "0", nil)
local scores_seconds_caption = MWCaption.create({name="dialogue", color={.7, 1, .7, 1}, active_color={1,1,1,1}}, "0", nil)
local scores_nickname_caption = MWInputField.create({name="dialogue", color={.7, .7, .7, 1}, active_color={1,1,1,1}}, nil, -1, nil, nil, "nickname", 16)
Menu:add(
	MWLabeledButton.create({name="dialogue", color = {1, .8, .8, .8}, color_active = {.7, .7, .7, 1}}, "POINTS", 10, 
		scores_points_caption, 
		nil, nil),
	SEND_SCORE
)
Menu:add(
	MWLabeledButton.create({name="dialogue", color = {1, .8, .8, .8}, color_active = {.7, .7, .7, 1}}, "SECONDS", 10, 
		scores_seconds_caption, 
		nil, nil),
	SEND_SCORE
)

Menu:add( MWSpace.create(0, 15), 11 )

Menu:add(
	MWLabeledButton.create({name="dialogue", color = {1, .8, .8, .8}, color_active = {.7, .7, .7, 1}}, "Name", 10, 
		scores_nickname_caption, 
		nil, nil),
	SEND_SCORE
)

Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, .8, .8, .8}, color_active = {1, .7, .7, 1}},
		"SEND",
		0,
		function (sender, param)
			local points = (mapvar and mapvar.score) and mapvar.score or 0
			local seconds = (mapvar and mapvar.game_timer) and mapvar.game_timer or 0
			--Log("Sending scores ", points, " ", seconds, " ", vars.nickname)
			local res = SendHighscores(vars.nickname, points, seconds/1000) 
			if res then
				gameover_exit()
			else
				Menu.scores_status_caption:show(true)
				Menu.scores_status_caption:setText("Sending failed")
			end
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	SEND_SCORE
)
Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
		"BACK",
		0,
		function (sender, param)
			WidgetSetVisible( Menu.gow, true )
			RestoreKeyProcessors()
			Menu:goBack()
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	SEND_SCORE
)
Menu:add( MWSpace.create(0, 15), SEND_SCORE )
Menu.scores_status_caption = MWCaption.create( nil, "", 0 )
Menu:add( Menu.scores_status_caption, SEND_SCORE )


Menu:add( MWSpace.create(0, 15), SEND_SCORE )

function Menu.showScoresMenu()
	local points = (mapvar and mapvar.score) and mapvar.score or 0
	local seconds = (mapvar and mapvar.game_timer) and mapvar.game_timer or 0
	scores_points_caption:setText(points)
	scores_seconds_caption:setText(seconds/1000)
	scores_nickname_caption:setText(vars.nickname)
	WidgetSetVisible( Menu.gow, false )
	Menu:show(SEND_SCORE)
	Menu.scores_status_caption:show(false)
end

-------------------------------------------------------------------------------------------------------------------------------------------

for i=2,11 do
	Menu:hide(i)
end

-------------------------------------------------------------------------------------------------------------------------------------------
--Page 101: Press button
Menu:addPage( PRESS_BUTTON, SimpleMenu.create( {320, 300}, 0, 0, 30, 10 ) )
Menu:add( MWSpace.create(0, 15), PRESS_BUTTON )
Menu:add( MWCaption.create( nil, "PRESS BUTTON", 0 ), PRESS_BUTTON )
Menu:add( MWSpace.create(0, 15), PRESS_BUTTON )
Menu:hide(PRESS_BUTTON)

-------------------------------------------------------------------------------------------------------------------------------------------
--Page 102 - 110: Key config for key sets


local TIMEOUT = 300
local last_change = 0

local function getKeyConfigButtonName(code)
	return (code ~= 0) and ( GetKeyName(code) or "BAD_BUTTON" ) or "(undefined)"
end

function Menu.resetAllKeyConfigButtonName()
	for i = 1,constants.configKeySetsNumber do
		Menu.resetKeyConfigButtonName(i)
	end
end

function Menu.resetKeyConfigButtonName(num)
	for key, val in pairs( Menu.configKeyWidgets[num] ) do
		val.elements[1]:setText(getKeyConfigButtonName(NEW_CONFIG.key_conf[num][key]))
	end
end

function setButt(sender, param, sender_obj)
	--Log("key = ", sender_obj.configKey, " setNum = ", sender_obj.keysSetNum)
	if GetCurTime() - last_change < 300 then
		return
	end
	
	local oldGlobalOnReleaseProc = GlobalGetKeyReleaseProc()
	--Log("oldGlobalOnReleaseProc = ", oldGlobalOnReleaseProc)
	
	local function keyWaiter(keyCode)
		--Log("in keyWaiter key = ", sender_obj.configKey, " setNum = ", sender_obj.keysSetNum)
		--Log("keycode = ", keyCode, " - ", GetKeyName(keyCode))
		
		GlobalSetKeyReleaseProc(oldGlobalOnReleaseProc)
		oldGlobalOnReleaseProc = GlobalGetKeyReleaseProc()
		if NEW_CONFIG.key_conf[sender_obj.keysSetNum] then
			NEW_CONFIG.key_conf[sender_obj.keysSetNum][sender_obj.configKey] = keyCode
			Menu.resetKeyConfigButtonName(sender_obj.keysSetNum)
			last_change = GetCurTime()
		end
		
		Menu:show(sender_obj.keysSetNum - 1 + firstKeyConfigPage)
		Menu:move(CONFIG.scr_width/2, CONFIG.scr_height/2, sender_obj.keysSetNum - 1 + firstKeyConfigPage)
		WidgetGainFocus( sender )

		return false
	end

	GlobalSetKeyReleaseProc(keyWaiter)
	Menu:show(PRESS_BUTTON)
end

function clearButt(sender, param, sender_obj)
	if CONFIG.key_conf[sender_obj.keysSetNum] then
		CONFIG.key_conf[sender_obj.keysSetNum][sender_obj.configKey] = 0
		Menu.resetKeyConfigButtonName(sender_obj.keysSetNum)
	end
end	

local function controlsButton( key, name, num, label_size )
	local sx, sy = GetCaptionSize( "default", dictionary_string(name)..":" )
	local key_name = getKeyConfigButtonName(NEW_CONFIG.key_conf[num] and NEW_CONFIG.key_conf[num][key] or 0)
	--Log("key = ", key," val = ", key_val, " name = ", key_name)	
	
	local button = MWFunctionButton.create(
				{name="default", color={.8,.8,1,1}, active_color={.2,.2,1,1}},
				key_name,
				-1,
				setButt,

				key,
				{ name = "wakaba-widget" },
				clearButt
			)
	button.configKey = key
	button.keysSetNum = num

	ret = MWLabeledButton.create(
			{name="default", color={1,1,1,1}, active_color={1,0,0,1}},
			name,
			label_size - sx + 15,
			button
	)
	return ret
end

local function createKeyConfigPage(num)
	local presetNum = 600 + num
	local pageNum = num - 1 + firstKeyConfigPage
	--Log("crating keyConfPage #", pageNum)
	Menu:addPage( pageNum, SimpleMenu.create( {320, 240}, 0, 0, 30, 10 ) )
	Menu:addPage( presetNum, SimpleMenu.create( {320, 240}, 0, 0, 30, 10 ) )
	--Menu:add( MWSpace.create(0, 15), pageNum )
	--Menu:add( MWCaption.create( font={name="default", color={1,1,1,1} }, ("CONTROLLER " .. (pageNum - firstKeyConfigPage)), -1, nil )
	Menu:add( MWSpace.create(0, 15), presetNum )
	Menu:add(
		MWFunctionButton.create(
			{name="dialogue", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
			"IICHANTRA STYLE ( DEFAULT STYLE )",
			0,
			function (sender, param)
				if GetCurTime() <= last_change + TIMEOUT then
					return
				end
				NEW_CONFIG.key_conf[num].left = keys["left"];
				NEW_CONFIG.key_conf[num].right = keys["right"];
				NEW_CONFIG.key_conf[num].up = keys["up"];
				NEW_CONFIG.key_conf[num].down = keys["down"];
				NEW_CONFIG.key_conf[num].jump = keys["x"];
				NEW_CONFIG.key_conf[num].fire = keys["c"];
				NEW_CONFIG.key_conf[num].alt_fire = keys["f"];
				NEW_CONFIG.key_conf[num].sit = keys["z"];
				NEW_CONFIG.key_conf[num].change_weapon = keys["lshift"];
				NEW_CONFIG.key_conf[num].player_use = keys["d"];
				NEW_CONFIG.key_conf[num].change_player = keys["a"];
				NEW_CONFIG.key_conf[num].weapon_slot1 = keys["1"];
				NEW_CONFIG.key_conf[num].weapon_slot2 = keys["2"];
				NEW_CONFIG.key_conf[num].weapon_slot3 = keys["3"];
				NEW_CONFIG.key_conf[num].weapon_slot4 = keys["4"];
				NEW_CONFIG.key_conf[num].gui_nav_prev = keys["up"];
				NEW_CONFIG.key_conf[num].gui_nav_next = keys["down"];
				NEW_CONFIG.key_conf[num].gui_nav_decline = keys["x"];
				NEW_CONFIG.key_conf[num].gui_nav_screenshot = keys["f11"];
				NEW_CONFIG.key_conf[num].gui_nav_menu = keys["esc"];
				NEW_CONFIG.key_conf[num].gui_nav_accept = keys["enter"];

				Menu.resetKeyConfigButtonName(num)
				Menu:show( pageNum )
			end,
			nil,
			{ name = "wakaba-widget" }
		),
		presetNum
	)
	Menu:add(
		MWFunctionButton.create(
			{name="dialogue", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
			"LESS KEYS",
			0,
			function (sender, param)
				if GetCurTime() <= last_change + TIMEOUT then
					return
				end
				NEW_CONFIG.key_conf[num].left = keys["left"];
				NEW_CONFIG.key_conf[num].right = keys["right"];
				NEW_CONFIG.key_conf[num].up = keys["up"];
				NEW_CONFIG.key_conf[num].down = keys["down"];
				NEW_CONFIG.key_conf[num].jump = keys["x"];
				NEW_CONFIG.key_conf[num].fire = keys["c"];
				NEW_CONFIG.key_conf[num].alt_fire = keys["f"];
				NEW_CONFIG.key_conf[num].sit = keys["down"];
				NEW_CONFIG.key_conf[num].change_weapon = keys["d"];
				NEW_CONFIG.key_conf[num].player_use = keys["up"];
				NEW_CONFIG.key_conf[num].change_player = keys["a"];
				NEW_CONFIG.key_conf[num].weapon_slot1 = keys["1"];
				NEW_CONFIG.key_conf[num].weapon_slot2 = keys["2"];
				NEW_CONFIG.key_conf[num].weapon_slot3 = keys["3"];
				NEW_CONFIG.key_conf[num].weapon_slot4 = keys["4"];
				NEW_CONFIG.key_conf[num].gui_nav_prev = keys["up"];
				NEW_CONFIG.key_conf[num].gui_nav_next = keys["down"];
				NEW_CONFIG.key_conf[num].gui_nav_decline = keys["x"];
				NEW_CONFIG.key_conf[num].gui_nav_menu = keys["esc"];
				NEW_CONFIG.key_conf[num].gui_nav_accept = keys["c"];

				Menu.resetKeyConfigButtonName(num)
				Menu:show( pageNum )
			end,
			nil,
			{ name = "wakaba-widget" }
		),
		presetNum
	)
	Menu:add(
		MWFunctionButton.create(
			{name="dialogue", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
			"CLASSIC STYLE",
			0,
			function (sender, param)
				if GetCurTime() <= last_change + TIMEOUT then
					return
				end
				NEW_CONFIG.key_conf[num].left = keys["left"];
				NEW_CONFIG.key_conf[num].right = keys["right"];
				NEW_CONFIG.key_conf[num].up = keys["up"];
				NEW_CONFIG.key_conf[num].down = keys["down"];
				NEW_CONFIG.key_conf[num].jump = keys["space"];
				NEW_CONFIG.key_conf[num].fire = keys["z"];
				NEW_CONFIG.key_conf[num].alt_fire = keys["x"];
				NEW_CONFIG.key_conf[num].sit = keys["lshift"];
				NEW_CONFIG.key_conf[num].change_weapon = keys["tab"];
				NEW_CONFIG.key_conf[num].player_use = keys["z"];
				NEW_CONFIG.key_conf[num].change_player = keys["a"];
				NEW_CONFIG.key_conf[num].gui_nav_accept = keys["enter"];
				
				Menu.resetKeyConfigButtonName(num)
				Menu:show( pageNum )
			end,
			nil,
			{ name = "wakaba-widget" }
		),
		presetNum
	)
	Menu:add(
		MWFunctionButton.create(
			{name="dialogue", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
			"TOUHOU STYLE",
			0,
			function (sender, param)
				if GetCurTime() <= last_change + TIMEOUT then
					return
				end
				NEW_CONFIG.key_conf[num].left = keys["left"];
				NEW_CONFIG.key_conf[num].right = keys["right"];
				NEW_CONFIG.key_conf[num].up = keys["up"];
				NEW_CONFIG.key_conf[num].down = keys["down"];
				NEW_CONFIG.key_conf[num].jump = keys["x"];
				NEW_CONFIG.key_conf[num].fire = keys["z"];
				NEW_CONFIG.key_conf[num].alt_fire = keys["c"];
				NEW_CONFIG.key_conf[num].sit = keys["lshift"];
				NEW_CONFIG.key_conf[num].change_weapon = keys["tab"];
				NEW_CONFIG.key_conf[num].player_use = keys["d"];
				NEW_CONFIG.key_conf[num].change_player = keys["a"];
				NEW_CONFIG.key_conf[num].gui_nav_accept = keys["enter"];
				
				Menu.resetKeyConfigButtonName(num)
				Menu:show( pageNum )
			end,
			nil,
			{ name = "wakaba-widget" }
		),
		presetNum
	)
	Menu:add(
		MWFunctionButton.create(
			{name="dialogue", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
			"DEVIL MAY CRY 3 STYLE",
			0,
			function (sender, param)
				if GetCurTime() <= last_change + TIMEOUT then
					return
				end
				NEW_CONFIG.key_conf[num].left = keys["a"];
				NEW_CONFIG.key_conf[num].right = keys["d"];
				NEW_CONFIG.key_conf[num].up = keys["w"];
				NEW_CONFIG.key_conf[num].down = keys["s"];
				NEW_CONFIG.key_conf[num].jump = keys["k"];
				NEW_CONFIG.key_conf[num].fire = keys["j"];
				NEW_CONFIG.key_conf[num].alt_fire = keys["i"];
				NEW_CONFIG.key_conf[num].sit = keys["l"];
				NEW_CONFIG.key_conf[num].change_weapon = keys["q"];
				NEW_CONFIG.key_conf[num].player_use = keys["u"];
				NEW_CONFIG.key_conf[num].change_player = keys["n"];
				NEW_CONFIG.key_conf[num].gui_nav_accept = keys["enter"];
				
				Menu.resetKeyConfigButtonName(num)
				Menu:show( pageNum )
			end,
			nil,
			{ name = "wakaba-widget" }
		),
		presetNum
	)
	Menu:add(
		MWFunctionButton.create(
			{name="dialogue", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
			"GAMEPAD",
			0,
			function (sender, param)
				if GetCurTime() <= last_change + TIMEOUT then
					return
				end
				NEW_CONFIG.key_conf[num].left = keys["j1_hat_left"];
				NEW_CONFIG.key_conf[num].right = keys["j1_hat_right"];
				NEW_CONFIG.key_conf[num].up = keys["j1_hat_up"];
				NEW_CONFIG.key_conf[num].down = keys["j1_hat_down"];
				NEW_CONFIG.key_conf[num].jump = keys["j1_button3"];
				NEW_CONFIG.key_conf[num].fire = keys["j1_button1"];
				NEW_CONFIG.key_conf[num].alt_fire =keys["j1_button2"];
				NEW_CONFIG.key_conf[num].sit = keys["j1_button6"];
				NEW_CONFIG.key_conf[num].player_use = keys["j1_button4"];
				NEW_CONFIG.key_conf[num].gui_nav_accept = keys["j1_button1"];
				NEW_CONFIG.key_conf[num].gui_nav_decline = keys["j1_button3"];
				NEW_CONFIG.key_conf[num].gui_nav_prev = keys["j1_hat_up"];
				NEW_CONFIG.key_conf[num].gui_nav_next = keys["j1_hat_down"];
				NEW_CONFIG.key_conf[num].gui_nav_menu = keys["j1_button10"];
				NEW_CONFIG.key_conf[num].weapon_slot1 = keys["j1_axis1-"];
				NEW_CONFIG.key_conf[num].weapon_slot2 = keys["j1_axis0-"];
				NEW_CONFIG.key_conf[num].weapon_slot3 = keys["j1_axis0+"];
				NEW_CONFIG.key_conf[num].weapon_slot4 = keys["j1_axis1+"];
				
				Menu.resetKeyConfigButtonName(num)
				Menu:show( pageNum )
			end,
			nil,
			{ name = "wakaba-widget" }
		),
		presetNum
	)
	Menu:add( MWSpace.create(0, 15), presetNum )
	Menu:add(
		MWFunctionButton.create(
			{name="default", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
			"BACK",
			0,
			function (sender, param)
				Menu:move(CONFIG.scr_width/2, CONFIG.scr_height/2, pageNum)
				Menu:show( pageNum )
			end,
			nil,
			{ name = "wakaba-widget" }
		),
		presetNum
	)
	Menu:add( MWSpace.create(0, 15), presetNum )
	Menu:add( MWSpace.create(0, 15), pageNum )
	Menu:add(
		MWFunctionButton.create(
			{name="default", color = {1, 1, 1, 1}, color_active = {.7, .7, 1, 1}},
			"PRESETS",
			0,
			function (sender, param)
				if GetCurTime() <= last_change + TIMEOUT then
					return
				end
				Menu:show( presetNum )
			end,
			nil,
			{ name = "wakaba-widget" }
		),
		pageNum
	)
	Menu:add( MWSpace.create(0, 15), pageNum )
	local buttons =
	{
		{ "left", "LEFT" },
		{ "right", "RIGHT" },
		{ "up", "UP" },
		{ "down", "DOWN" },
		{ "sit", "CROUCH" },
		{ "fire", "FIRE" },
		{ "alt_fire", "ALT.FIRE" },
		{ "jump", "JUMP" },
		{ "player_use", "USE" },
		{ "gui_nav_menu", "MENU" },
		{ "gui_nav_accept", "ACCEPT" },
		{ "gui_nav_decline", "CANCEL" },
		{ "gui_nav_next", "MENU NEXT" },
		{ "gui_nav_prev", "MENU BACK" },
		{ "weapon_slot1", "WEAPON 1" },
		{ "weapon_slot2", "WEAPON 2" },
		{ "weapon_slot3", "WEAPON 3" },
		{ "weapon_slot4", "WEAPON 4" },
		{ "change_weapon", "CHANGE WEAPON" },
		{ "gui_nav_screenshot", "SCREENSHOT" }
	}
	local labelx = 0
	local lsx
	for k, v in pairs( buttons ) do
		lsx = GetCaptionSize( "default", dictionary_string(v[2]) ) + 15
		labelx = math.max( labelx, lsx+15 )
	end
	if not Menu.configKeyWidgets then Menu.configKeyWidgets = {} end
	Menu.configKeyWidgets[num] = {}
	for i=1, #buttons do
		Menu.configKeyWidgets[num][buttons[i][1]] = controlsButton( buttons[i][1], buttons[i][2], num, labelx, widget_key )
		Menu:add( Menu.configKeyWidgets[num][buttons[i][1]], pageNum )
	end
	Menu:add(
		MWFunctionButton.create(
			{name="default", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
			"BACK",
			0,
			function (sender, param)
				RestoreKeyProcessors()
				if GUI then
					GUI:show_pop()
				end
				CONFIG = deep_copy(NEW_CONFIG)
				LoadConfig()
				SaveConfig()
				Menu.unlock()
				Menu:goBack()
				EnableScreenshots( true )
			end,
			nil,
			{ name = "wakaba-widget" }
		),
		pageNum
	)
	Menu:add(
		MWFunctionButton.create(
			{name="default", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
			"CANCEL",
			0,
			function (sender, param)
				RestoreKeyProcessors()
				if GUI then
					GUI:show_pop()
				end
				Menu.unlock()
				Menu:goBack()
				EnableScreenshots( true )
			end,
			nil,
			{ name = "wakaba-widget" }
		),
		pageNum
	)
	Menu:add( MWSpace.create(0, 15), pageNum )
	Menu:hide(pageNum)
	Menu:hide(presetNum)
end

for i = 1,constants.configKeySetsNumber do
	createKeyConfigPage(i)
end

Menu:addPage( 28, SimpleMenu.create( {320, 300}, 0, 0, 30, 10 ) )
Menu:add( MWSpace.create(10, 135), 28 )
Menu:add( MWCaption.create( nil, "NETGAME", 0 ), 28 )
Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
		"BACK",
		0,
		function (sender, param)
			RestoreKeyProcessors()
			Menu:goBack()
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	28
)
Menu:add( MWSpace.create(10, 135), 28 )
Menu:add(
	MWFunctionButton.create(
		{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
		"HOST",
			0, 
			function (sender, param) 
				Menu.hideMainMenu()
				difficulty = vars.difficulty
				assert(Loader.startGame( vars.test_map ))
				h()
			end, 
			nil,
			{name="wakaba-widget"}
	),
	28
)
Menu:add( MWSpace.create(5, 135), 28 )
Menu:add(
	MWFunctionButton.create(
		{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
		"CLIENT",
			0, 
			function (sender, param) 
				Menu.hideMainMenu()
				difficulty = vars.difficulty
				assert(Loader.startGame( vars.test_map ))
				c()
			end, 
			nil,
			{name="wakaba-widget"}
	),
	28
)
Menu:add( MWSpace.create(10, 135), 28 )
Menu:hide(28)

local default_resolutions =
[[resolutions =
{
	["17:9"] =
	{
		"2048x1080"
	},
	["16:9"] =
	{
		"854x480", "1280x720", "1366x768", "1920x1080"
	},
	["16:10"] = 
	{
		"1280x800", "1440x900", "1680x1050", "1920x1200", "2560x1600" 
	},
	["5:3"] =
	{
		"800x480", "1280x768"
	},
	["8:5"] =
	{
		"320x200", "1280x800", "1440x900", "1680x1050", "1920x1200", "2460x1600"
	},
	["3:2"] =
	{
		"480x320", "720x480", "1152x768", "1280x854", "1440x960"
	},
	["4:3"] =
	{
		"320x240", "640x480", "768x575", "800x600", "1024x768", "1280x960", "1400x1050", "1600x1200", "2048x1536"
	},
	["5:4"] =
	{
		"1280x1024", "2560x2048"
	}
}
]]

function Menu:loadResolutions()
	local file = build_relative_path( constants.path_config .. "resolutions.lua" )
	local test = io.open( file, "r" )
	local success = false
	if test then
		test:close()
		local sbox = {}
		local lfile = __loadfile( file )
		if type( lfile ) == 'function' then
			setfenv( lfile, sbox )
			local success, result = pcall( lfile )
			resolutions = deep_copy( sbox.resolutions )
			success = true
		end
	end
	if not success then
		loadstring(default_resolutions)()
		test = io.open( file, "w" )
		test:write( default_resolutions )
		test:close()
	end
end

function Menu:showVideoOptionsPage( back_page, ratio, back_event )
	local pg = SimpleMenu.create( {CONFIG.scr_width/2, CONFIG.scr_height/2}, 0, 0, 15, 15 )
	self:loadResolutions()
	resolutions = resolutions or { ["4:3"]={ "640x480" } }
	Menu:hide()

	local confirmed = true
	local cancelThread = nil
	local function stopCancelThread()
		if cancelThread then StopThread(cancelThread) end
	end

	GlobalSetKeyReleaseProc( function(key) 
		if confirmed and (isConfigKeyPressed( key, "gui_nav_menu" ) or isConfigKeyPressed( key, "gui_nav_decline" )) then
			pg:hide()
			pg:cleanup()
			SaveConfig()
			Menu:show(back_page)
			if back_event then back_event() end
			RestoreKeyProcessors()
		end
	end )

	local areyousure = function( back_menu, restore_settings )
		confirmed = false
		local pg13 = SimpleMenu.create( {CONFIG.scr_width/2, CONFIG.scr_height/2}, 0, 0, 15, 15 )
		pg13:add( MWSpace.create(0, 15) )
		pg13:add(
			MWCaption.create(
				{name="default", color={1,1,1,1}},
				"ARE YOU SURE YOU WANT TO KEEP THESE SETTINGS?",
				0
			)
		)
		pg13:add( MWSpace.create(0, 15) )
		pg13:add(
			MWFunctionButton.create(
				{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},
				"NO",
				0,
				function( sender, param )
					confirmed = true
					stopCancelThread()
					restore_settings()
					pg13:hide()
					pg13:cleanup()
					back_menu:show()	
				end
			)
		)
		pg13:add(
			MWFunctionButton.create(
				{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},
				"YES",
				0,
				function( sender, param )
					confirmed = true
					stopCancelThread()
					pg13:hide()
					pg13:cleanup()
					back_menu:show()
				end
			)
		)
		pg13:add( MWSpace.create(0, 15) )
		cancelThread = NewThread( function()
			Wait( 5000 )
			if not confirmed then
				restore_settings()
				pg13:hide()
				pg13:cleanup()
				back_menu:show()
			end
			cancelThread = nil
		end )
		Resume(cancelThread)
	end
	
	pg:add( MWSpace.create(0, 15) )

	local ar_btn = pg:add(
		MWFunctionButton.create(
				{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},
				dictionary_string("ASPECT RATIO")..": "..(ratio or "4:3"),
				0,
				function( sender, param )
					pg:hide()
					pg:cleanup()
					local ratios = {}
					local nextratio = 1
					for k, v in pairs( resolutions ) do
						table.insert( ratios, k )
					end
					for i=1,#ratios do
						if ratios[i] == ratio then
							nextratio = i+1
						end
					end
					if nextratio > #ratios then
						nextratio = 1
					end
					return Menu:showVideoOptionsPage( back_page, ratios[nextratio], back_event )
				end
			)
	)
	pg:gainFocus(ar_btn)
	pg:add(
		MWFunctionButton.create(
			{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},
			dictionary_string("FULLSCREEN")..": "..CONFIG.fullscreen,
			0,
			function( sender, param )
				local old_fullscreen = CONFIG.fullscreen
				if CONFIG.fullscreen == 0 then
					CONFIG.fullscreen = 1
					WidgetSetCaption( sender, "FULLSCREEN: 1" )
				else
					CONFIG.fullscreen = 0
					WidgetSetCaption( sender, "FULLSCREEN: 0" )
				end
				LoadConfig()
				pg:hide()
				areyousure( pg, function() CONFIG.fullscreen = old_fullscreen; LoadConfig() end )
			end
		)
	)
	pg:add(
		MWFunctionButton.create(
			{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},
			dictionary_string("VSYNC")..": "..CONFIG.vert_sync,
			0,
			function( sender, param )
				local old_sync = CONFIG.vert_sync
				if CONFIG.vert_sync == 0 then
					CONFIG.vert_sync = 1
					WidgetSetCaption( sender, "VSYNC: 1" )
				else
					CONFIG.vert_sync = 0
					WidgetSetCaption( sender, "VSYNC: 0" )
				end
				LoadConfig()
				pg:hide()
				areyousure( pg, function() CONFIG.vert_sync = old_sync; LoadConfig() end )
			end
		)
	)

	pg:add( MWSpace.create(0, 15) )

	local res = resolutions[ratio or "4:3"] or {}
--	local act_res = nil
	for k, v in pairs( res ) do
		pg:add(
			MWFunctionButton.create(
				{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}},
				v,
				0,
				function( sender, param )
					local oww, owh, ofw, ofh = CONFIG.window_width, CONFIG.window_height, CONFIG.full_width, CONFIG.full_height
					local resstr = WidgetGetCaption( sender )
					local w, h = string.match( resstr, "(%d+)x(%d+)" )
					w, h = tonumber(w), tonumber(h)
					CONFIG.window_width = w
					CONFIG.window_height = h
					CONFIG.full_width = w
					CONFIG.full_height = h
					LoadConfig()
					pg:hide()
					areyousure( pg, function() 
						CONFIG.window_width, CONFIG.window_height, CONFIG.full_width, CONFIG.full_height = oww, owh, ofw, ofh
						LoadConfig() 
					end )
				end
			)
		)
	end

	pg:add( MWSpace.create(0, 15) )

	pg:add(
		MWFunctionButton.create(
			{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},
			"BACK",
			0,
			function( sender, param )
				pg:hide()
				pg:cleanup()
				SaveConfig()
				Menu:show(back_page)
				if back_event then back_event() end
				RestoreKeyProcessors()
			end
		)
	)

	pg:add( MWSpace.create(0, 15) )
end

function Menu.lock()
	if not Menu.locked then
		Menu.locked = {}
	end
	table.insert( Menu.locked, 1 )
end

function Menu.unlock()
	table.remove( Menu.locked, #Menu.locked )
end

function Menu.showProfileMenu( back_event )
	Menu:hide()
	local page_widgets = {}
	local profiles = Saver.getProfilesList()
	local pnum = #profiles
	local fw, fh = GetCaptionSize( "dialogue", "i" )
	fh = fh + 10
	local wsx = 0
	wsx = math.max( wsx, GetCaptionSize( "dialogue", "iiiiiiiiiiii" ) )
	wsx = math.max( wsx, GetCaptionSize( "dialogue", dictionary_string( "(new profile)" ) ) )
	wsx = math.max( wsx, GetCaptionSize( "dialogue", dictionary_string( "[up]" ) ) )
	wsx = math.max( wsx, GetCaptionSize( "dialogue", dictionary_string( "[down]" ) ) )
	wsx = math.max( wsx, GetCaptionSize( "dialogue", dictionary_string( "back" ) ) )
	wsx = wsx + 20
	local per_page = math.floor( ((3 * CONFIG.scr_height / 4) - 40) / fh) - 5
	local page = 0
	local window = CreateWidget( constants.wt_Panel, "", nil, CONFIG.scr_width / 2 - wsx / 2, CONFIG.scr_height / 8, wsx, 3 * CONFIG.scr_height / 4 )
	WidgetSetSprite( window, "window1" )
	WidgetSetFocusable( window, false )
	local sx, sy = GetCaptionSize( "dialogue", dictionary_string( "(new profile)" ) )
	local y = 20
	local up
	local down
	WidgetSetSpriteColor( window, { 0, 0, 0, .5 } )

	local show_page = function()
		local ssx, ssy
		for k, v in pairs( page_widgets ) do
			DestroyWidget( v )
		end
		y = 20 + 2 * fh
		page_widgets = {}
		for i = page * per_page + 1, math.min( #profiles, (page + 1) * per_page ) do
			ssx, ssy = GetCaptionSize( "dialogue", profiles[i] )
			local w = CreateWidget( constants.wt_Button, "", window, 0, 0, wsx, sy )
			WidgetSetPos( w, 10, y, true )
			WidgetSetCaptionFont( w, "dialogue" )
			WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, false )
			WidgetSetCaption( w, profiles[i] )
			WidgetSetLMouseClickProc( w, function()
				Saver:loadProfile( profiles[i] )
				LoadProfileConfig()
				Menu.resetAllKeyConfigButtonName()
				DestroyWidget( window )
				Menu:show( MAIN_MENU )
				Game.SaveLastProfile()
			end )
			table.insert( page_widgets, w )
			y = y + fh
		end
		if page > 0 then
			WidgetSetVisible( up, true )
		else
			WidgetSetVisible( up, false )
		end
		if #profiles > (page + 1) * per_page then
			if down then
				WidgetSetVisible( down, true )
			end
		else
			if down then
				WidgetSetVisible( down, false )
			end
		end
	end

	local new = CreateWidget( constants.wt_Button, "", window, 0, 0, sx, sy )
	WidgetSetPos( new, 10, y, true )
	WidgetSetCaptionFont( new, "dialogue" )
	WidgetSetCaptionColor( new, { 1, 1, 0.5, 1 }, false )
	WidgetSetCaption( new, dictionary_string( "(new profile)" ) )
	WidgetSetLMouseClickProc( new, function()
		WidgetSetVisible( window, false )
		Menu.newProfile( window, back_event )
	end )
	y = y + fh
	sx, sy = GetCaptionSize( "dialogue", dictionary_string( "[up]" ) )
	up = CreateWidget( constants.wt_Button, "", window, 0, 0, sx, sy )
	WidgetSetPos( up, 10, y, true )
	WidgetSetCaptionFont( up, "dialogue" )
	WidgetSetCaptionColor( up, { 1, 1, 1, 1 }, false )
	WidgetSetCaption( up, dictionary_string( "[up]" ) )
	WidgetSetLMouseClickProc( up, function()
		page = page - 1
		show_page()
	end )
	WidgetSetVisible( up, false )
	y = y + fh

	show_page()
	sx, sy = GetCaptionSize( "dialogue", dictionary_string( "[down]" ) )
	down = CreateWidget( constants.wt_Button, "", window, 0, 0, sx, sy )
	WidgetSetPos( down, 10, y, true )
	WidgetSetCaptionFont( down, "dialogue" )
	WidgetSetCaptionColor( down, { 1, 1, 1, 1 }, false )
	WidgetSetCaption( down, dictionary_string( "[down]" ) )
	WidgetSetLMouseClickProc( down, function()
		page = page + 1
		show_page()
	end )
	WidgetSetVisible( down, false )
	if #profiles > (page + 1) * per_page then
		WidgetSetVisible( down, true )
	end
	sx, sy = GetCaptionSize( "dialogue", dictionary_string( "back" ) )
	local back = CreateWidget( constants.wt_Button, "", window, 0, 0, sx, sy )
	WidgetSetPos( back, 10, 3 * CONFIG.scr_height / 4 - sy - 20, true )
	WidgetSetCaptionFont( back, "dialogue" )
	WidgetSetCaptionColor( back, { 1, 1, 0.5, 1 }, false )
	WidgetSetCaption( back, dictionary_string( "back" ) )
	WidgetSetBackClickProc( back, function()
		DestroyWidget( window )
		Menu:show( MAIN_MENU )
		if back_event then back_event() end
	end )
end

function Menu.newProfile( back_widget, back_event )
	local window = CreateWidget( constants.wt_Panel, "", nil, CONFIG.scr_width/2 - 128, CONFIG.scr_height/2 - 32, 256, 96 )
	WidgetSetSprite( window, "window1" )
	WidgetSetFocusable( window, false )
	WidgetSetSpriteColor( window, { 0, 0, 0, .5 } )
	
	local sx, sy = GetCaptionSize( "default", dictionary_string( "ENTER PROFILE NAME" ) )
	local title = CreateWidget( constants.wt_Label, "", window, CONFIG.scr_width/2 - sx, CONFIG.scr_height/2 - 32 - 15 - 2*sy, 1, 1 )
	WidgetSetCaptionFont( title, "default", 2 )
	WidgetSetCaptionColor( title, {1,1,1,1}, false, {0,0,0,1} )
	WidgetSetCaption( title, dictionary_string( "ENTER PROFILE NAME" ) )
	
	sx, sy = GetCaptionSize( "dialogue", dictionary_string("Name")..":" )
	local label = CreateWidget( constants.wt_Label, "", window, CONFIG.scr_width/2 - 64 - sx/2, CONFIG.scr_height/2 - 5, 1, 1 )
	WidgetSetCaptionFont( label, "dialogue" )
	WidgetSetCaptionColor( label, {1,1,1,1}, false )
	WidgetSetCaption( label, dictionary_string("Name")..":" )
	local i_x, i_y = GetCaptionSize( "dialogue", "XXXXXXXXXXXXXXXX" )
	local back_box = CreateWidget( constants.wt_Widget, "", window, CONFIG.scr_width/2 - 35, CONFIG.scr_height/2 - 7, i_x + 5, i_y + 5 )
	WidgetSetColorBox( back_box, {.3,.3,.3,.5} )
	WidgetSetBorderColor( back_box, {1, 1, 1, 1}, true )
	WidgetSetZ( back_box, 1.05 )
	
	local input = CreateWidget( constants.wt_Textfield, "", window, CONFIG.scr_width/2 - 30, CONFIG.scr_height/2 - 5, i_x, i_y )
	WidgetSetFocusProc( input, function()
		WidgetSetColorBox( back_box, {.5,.5,.5,.5} )
		WidgetSetBorder( back_box, true )
	end )
	local unfocus = function()
		WidgetSetColorBox( back_box, {.3,.3,.3,.5} )
		WidgetSetBorder( back_box, false )
	end
	WidgetSetUnFocusProc( input, unfocus )
	WidgetSetCaptionFont( input, "dialogue" )
	local inputGoodColors = {{ .7, .7, .7, 1 }, { 1, 1, 1, 1 }}
	local inputBadColors  = {{ .7, .3, .3, 1 }, { 1, .5, .5, 1 }}
	WidgetSetCaptionColor( input, inputGoodColors[1], false )
	WidgetSetCaptionColor( input, inputGoodColors[2], true )
	WidgetSetMaxTextfieldSize( input, 16 )
	WidgetSetCaption( input, Game.RandomName() )
	WidgetSetKeyInputProc(input, function()
		local text = WidgetGetCaption(input)
		
		if Game.isGoodProfileName(text) then
			WidgetSetCaptionColor( input, inputGoodColors[1], false )
			WidgetSetCaptionColor( input, inputGoodColors[2], true )
		else
			WidgetSetCaptionColor( input, inputBadColors[1], false )
			WidgetSetCaptionColor( input, inputBadColors[2], true )
		end
	
	end)
	
	if back_widget then
		sx, sy = GetCaptionSize( "dialogue", dictionary_string( "back" ) )
		local back = CreateWidget( constants.wt_Button, "", window, CONFIG.scr_width/2 - 64, CONFIG.scr_height/2 + 32, sx, sy )
		WidgetSetCaptionFont( back, "dialogue" )
		WidgetSetCaptionColor( back, { 1, 1, 0.5, 1 }, false )
		WidgetSetCaption( back, dictionary_string( "back" ) )
		WidgetSetBackClickProc( back, function()
			if GetFocusLock() then
				return true
			end
			DestroyWidget( window )
			WidgetSetVisible( back_widget, true )
		end )
		WidgetSetFocusProc( back, unfocus )
	end
	
	sx, sy = GetCaptionSize( "dialogue", dictionary_string( "create" ) )
	local ok = CreateWidget( constants.wt_Button, "", window, CONFIG.scr_width/2 + 64 - sx, CONFIG.scr_height/2 + 32, sx, sy )
	WidgetSetCaptionFont( ok, "dialogue" )
	WidgetSetCaptionColor( ok, { 1, 1, 0.5, 1 }, false )
	WidgetSetCaption( ok, dictionary_string( "create" ) )
	WidgetSetLMouseClickProc( ok, function()
		if not Game.CreateProfile( WidgetGetCaption( input ) ) then return; end
		DestroyWidget( window )
		if back_widget then DestroyWidget( back_widget ); PopBackEvent() end
		PopBackEvent() -- pops back event added in Menu.showProfileMenu() or nothing
		Menu:show(MAIN_MENU)
		Game.SaveLastProfile()
		if back_event then back_event() end
	end )
	WidgetSetFocusProc( ok, unfocus )
	
	WidgetGainFocus( input )
end
-------------------------------------------------------------------------------------------------------------------------------------------
--Page 29: Extra

Menu:addPage( EXTRA, SimpleMenu.create( {320, 300}, 0, 0, 30, 10 ) )
Menu:add( MWSpace.create( 0, 15 ), EXTRA )

do
local INFO_BUTTON
INFO_BUTTON = MWFunctionButton.create(
		{name="default", color = {1, 1, 1, 1}, active_color = {.2, 1, .2, 1}},
		"INFO",
		0,
		function (sender, param)
			Info.ShowInfoMenu( Menu, 29, function()
				INFO_BUTTON:gainFocus()
			end )
		end,
		nil,
		{ name = "wakaba-widget" }
	)
Menu:add(INFO_BUTTON, EXTRA)
end
Menu:add( MWSpace.create( 0, 15 ), EXTRA )

do
local default_playlist = [[#Format:
#
#DIR:(directory);
#Make following files appear in (directory).
#
#FILE:(name):(visible_name):(custom_value);
#Creates a virtual file named (name) that is shown as (visible_name) in game and passses (custom_value) to event from OpenFile.
#In this case custom value is the relative path of the file to play.
#
#Example:
#FILE:myfile.mp3:My track:../../music/myfile.mp3;

DIR:/sounds/;
	FILE:nsmpr_g.it:Gee:music/nsmpr_g.it;
	FILE:iie_finish.it:Finish jingle:music/iie_finish.it;
	FILE:nsmpr_iwbtt.it:I wanna be the timelord:music/nsmpr_iwbtt.it;
	FILE:nsmpr_choice.it:Choice:music/nsmpr_choice.it;
	FILE:iie_win_jingle.it:Win jingle:music/iie_win_jingle.it;
	FILE:iie_kmp.it:Kill me please:music/iie_kmp.it;
	FILE:nsmpr_lab.it:Laboratory:music/nsmpr_lab.it;
	FILE:nsmpr_another.it:Another:music/nsmpr_another.it;
	FILE:iie_lose_jingle.it:Lose jingle:music/iie_lose_jingle.it;
	FILE:nsmpr_k5300.it:K5300:music/nsmpr_k5300.it;
	FILE:nsmpr_wti.it:wti:music/nsmpr_wti.it;
	FILE:iie_lab.it:Dirty work:music/iie_lab.it;
	FILE:iie_survivor.it:Survivor:music/iie_survivor.it;
	FILE:iie_escape.it:Escape:music/iie_escape.it;
	FILE:nsmpr_import.it:Import:music/nsmpr_import.it;
	FILE:iie_whbt.it:We have to break through:music/iie_whbt.it;
	FILE:iie_street.it:Because she said "no":music/iie_street.it;
	FILE:iie_boss.it:Boss music:music/iie_boss.it;
	FILE:iie_w2gh.it:Welcome to the ghost house:music/iie_w2gh.it;
	FILE:iie_wd.it:Well done:music/iie_wd.it;
	FILE:nsmpr_title.it:Title:music/nsmprt_title.it;
	FILE:nsmpr_woom.it:WOOM!:music/nsmpr_woom.it;
	FILE:nsmpr_newplc.it:New place:music/nsmpr_nwplc.it;
]]
local MUSIC_ROOM_BUTTON
MUSIC_ROOM_BUTTON = MWFunctionButton.create(
					{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
					"MUSIC ROOM",
					0, 
					function(sender, param) 
						local virtual_files = {}
						local test = io.open( constants.path_home.."playlist", "r" )
						local dir, file, value1, value2, volume
						local current_dir = "/"
						if test then
							test:close()
						else
							test = io.open( constants.path_home.."playlist", "w" )
							test:write( default_playlist )
							test:close()
						end
						for line in io.lines( constants.path_home.."playlist" ) do
							if not string.match( line, "%s*#" ) then
								dir = string.match( line, 'DIR:([^;]+);' )
								if dir then
									current_dir = dir
								else
									file, value1, value2 = string.match( line, "FILE:([^:]+):([^:]+):([^;]+);" );
									Log(value2)
									Log(volume)
									if file then
										table.insert( virtual_files, { current_dir, value1, file, value2, {volume = 1} } )
									else
										if not (line == "" or string.match( line, "%s+" )) then
											Log( "something strange in playlist: "..line )
										end
									end
								end
							end
						end
						RemoveKeyProcessors()
						GlobalSetKeyReleaseProc( function(key)
						end )
						StopBackMusic()
						vars.menu_sounds = false
						OpenFile{ path = "/sounds/",
							preprocess = function( name, path )
								return string.gsub( name, "%.[^%.]+$", "" )
							end,
							event = function( name, path, virtual_value, custom_values )
								if not virtual_value then
									PlayBackMusic( string.gsub(path..name, "/?sounds/", "" ) )
								else
									PlayBackMusic( virtual_value, custom_values and custom_values.volume )
								end
								return false
							end,
							root = "/sounds/",
							cancel_event = function()
								PlayBackMusic( "music/nsmpr_iwbtt.it", 0.4 )
								vars.menu_sounds = true
								RestoreKeyProcessors()
								Menu:show(EXTRA)
								MUSIC_ROOM_BUTTON:gainFocus()
							end,
							lock_size = true,
							virtual_files = virtual_files,
							slot_width = 300,
							limits = { 1, nil } }
						Menu:hide(EXTRA)
					end,
					nil,
					{name="wakaba-widget"}
			)
Menu:add(MUSIC_ROOM_BUTTON, EXTRA )
end
Menu:add( MWSpace.create( 0, 15 ), EXTRA )

if vars.dir_menu then
	vars.dir_menu:clear()
end
vars.dir_menu = ScrollingMenu.create( {CONFIG.scr_width/2, CONFIG.scr_height/2}, 0, 0, 30, 10, 20 )
vars.dir_menu:hide()

do
local CUSTOM_MAP_BUTTON
CUSTOM_MAP_BUTTON = MWFunctionButton.create( 
					{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
					"CUSTOM MAP",
					0, 
					function (sender, param)
						RemoveKeyProcessors()
						GlobalSetKeyReleaseProc( function(key)
						end )
						OpenFile{ path = "/scripts/levelscripts/",
							preprocess = function( name, path )
								if custom_maps and custom_maps[name] and custom_maps[name].name then
									if custom_maps[name].use_a_dictionary == 1 then
										return dictionary_string(custom_maps[name].name)
									else
										return custom_maps[name].name
									end
								else
									return string.gsub( name, "%.lua$", "" )
								end
							end,
							filter = function( name, path )
								if custom_maps and custom_maps[name] then return true end
								if not string.match( name, "%.lua$" ) then return false end
								local fname = string.gsub( name, "%.lua$", ".info" )
								local test = io.open( constants.path_scripts .. "levelscripts/".. fname, "r" )
								if test then
									test:close()
									custom_maps[name] = {}
									for line in io.lines( constants.path_scripts .. "levelscripts/".. fname ) do
										local param, value = string.match( line, "(%S+)%s*=%s*(.+)" )
										if param then
											custom_maps[name][param] = (tonumber(value)) or value
										end
									end
									return true
								end
								return false
							end,
							event = function( name, path )
								RestoreKeyProcessors()
								RestoreKeyProcessors()
								LockScore()
								Game.custom_map = true
								if custom_maps and custom_maps[name] and custom_maps[name].character_selection == 0 then
									Game.SkipCharSelMenu( string.gsub( name, "%.lua$", "" ) )
								else
									Game.ShowCharSelMenu( Menu, string.gsub( name, "%.lua$", "" ), function() Menu.showMainMenu() end )
								end
								return true
							end,
							root = "/scripts/levelscripts/",
							cancel_event = function()
								RestoreKeyProcessors()
								Menu:show(EXTRA)
								CUSTOM_MAP_BUTTON:gainFocus()
							end,
							lock_size = true,
							limits = { 2, nil } }
						Menu:hide(EXTRA)
					end, 
					nil,
					{name="wakaba-widget"}
			)
Menu:add( CUSTOM_MAP_BUTTON, EXTRA )
end
--[[
Menu:add( MWSpace.create( 0, 15 ), 29 )

Menu:add( 
			MWFunctionButton.create( 
					{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
					"NETGAME",
					0, 
					function (sender, param) 
						Menu:show(28)
						Menu:saveBackPage(1)
					end, 
					nil,
					{name="wakaba-widget"}
			),
			29
		)
--]]
Menu:add( MWSpace.create( 0, 15 ), EXTRA )

do
local HIDE_MENU_BTN
HIDE_MENU_BTN = Menu:add(
			MWFunctionButton.create(
					{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
					"HIDE MENU",
					0, 
					function(sender, param) 
						Menu:hide(29)
						WidgetSetPos( vars.textwidget, CONFIG.scr_width/2-256, 9000 )
						
						Resume( NewMapThread( function()
							local skip_key = false
							SetSkipKey( function() skip_key = true end, 200 )
							while not skip_key do
								Wait(1)
							end
							WidgetSetPos( vars.textwidget, CONFIG.scr_width/2-256, 10 )
							Menu:show(EXTRA)
							Menu:gainFocus(HIDE_MENU_BTN)
						end ) )
					end,
					nil,
					{name="wakaba-widget"}
			),
			EXTRA
		)
end

Menu:add( MWSpace.create( 0, 15 ), EXTRA )

Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, .7, .7, 1}, active_color = {1, .2, .2, 1}},
		"BACK",
		0,
		function (sender, param)
			RestoreKeyProcessors()
			Menu:goBack()
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	EXTRA
)
Menu:add( MWSpace.create( 0, 15 ), EXTRA )
Menu:hide(EXTRA)
-------------------------------------------------------------------------------------------------------------------------------------------
--Page 32: Start

Menu:addPage( START, SimpleMenu.create( {320, 300}, 0, 0, 30, 10 ) )
Menu:add( MWSpace.create( 0, 15 ), START )

Menu:add( 
			MWFunctionButton.create( 
					{name="default", color={1,1,1,1}, active_color={.2,1,.2,1}}, 
					"NEW GAME",
					0, 
					function (sender, param)
						RestoreKeyProcessors()
						difficulty = vars.difficulty
						--assert(Loader.startGame( vars.start_map ))
						if vars.textwidget then DestroyWidget( vars.textwidget ) end
						Menu.hideMainMenu()

						local function begin_game()
							Game.custom_map = false
							Game.ShowCharSelMenu( Menu, "pear15opening", function() Menu.showMainMenu() end )
						end

						if not (Game.profile.achievement_progress["TUTORIAL_REMINDER"] 
						   or Game.profile.achievement_progress["LICENSED_TO_PLAY"] == Achievements.UNLOCKED) then
							local text1 = dictionary_string( "TUTORIAL_WARNING" )
							local text2 = dictionary_string( "Yes" )
							local text3 = dictionary_string( "No" )
							local window = CreateWidget( constants.wt_Panel, "", nil, 0, 0, 1, 1 )
							WidgetSetFocusable( window, false )
							local sx, sy = GetMultilineCaptionSize( "dialogue", text1, 260, 1 )
							local wx, wy =  sx + 45, sy + 60 + 40
							local text = CreateWidget( constants.wt_Label, "", window, 30, 30, sx, sy )
							WidgetSetCaptionFont( text, "dialogue" )
							WidgetSetCaptionColor( text, { .8, .8, .8, 1 }, false, { .0, .0, .0, 1 } )
							WidgetSetCaption( text, text1, true )
							WidgetSetFocusable( text, false )
							
							local yes
							local no
							local yes_focused = false
							local function onfocus(id) yes_focused = (id == yes) end
							local no_event
							local slaveKeyProc = Loader.addSlaveKeyProcessor(function (key)
								if isConfigKeyPressed( key, "right" ) or isConfigKeyPressed( key, "left" ) then
									if yes_focused then 
										WidgetGainFocus(no)
									else 
										WidgetGainFocus(yes) 
									end
								elseif isConfigKeyPressed(key, "gui_nav_menu") or isConfigKeyPressed(key, "gui_nav_decline") then
									no_event()
								end
							end)

							local ssx, ssy = GetCaptionSize( "default", text2 )
							yes = CreateWidget( constants.wt_Button, "", window, wx/4 - ssx/2, wy - 30, ssx, ssy )
							WidgetSetCaptionFont( yes, "default" )
							WidgetSetCaptionColor( yes, { 1, 1, 1, 1 }, false, {0, 0, 0, 1} )
							WidgetSetCaptionColor( yes, { .2, .2, 1, 1 }, true, {0, 0, 0, 1} )
							WidgetSetCaption( yes, text2 )
							WidgetSetLMouseClickProc( yes, function()
								DestroyWidget( window )
								Game.profile.achievement_progress["TUTORIAL_REMINDER"] = 1
								Saver:saveProfile()
								Loader.removeSlaveKeyProcessor(slaveKeyProc)
								begin_game()
							end )
							WidgetSetFocusProc(yes, onfocus)
							WidgetGainFocus( yes )
							
							ssx, ssy = GetCaptionSize( "default", text3 )
							no = CreateWidget( constants.wt_Button, "", window, 3*wx/4 - ssx/2, wy - 30, ssx, ssy )
							WidgetSetCaptionFont( no, "default" )
							WidgetSetCaptionColor( no, { 1, 1, 1, 1 }, false, {0, 0, 0, 1} )
							WidgetSetCaptionColor( no, { .2, .2, 1, 1 }, true, {0, 0, 0, 1} )
							WidgetSetCaption( no, text3 )
							no_event = function()
								DestroyWidget( window )
								--Game.profile.achievement_progress["TUTORIAL_REMINDER"] = 1
								--Saver:saveProfile()
								Loader.removeSlaveKeyProcessor(slaveKeyProc)
								Menu:show(START)
								--Loader.startGame( vars.tutorial_map )
							end
							WidgetSetLMouseClickProc( no, no_event )
							WidgetSetFocusProc(no, onfocus)

							WidgetSetSize( window, wx, wy )
							WidgetSetPos( window, (CONFIG.scr_width-wx)/2, (CONFIG.scr_height-wy)/2 )
							WidgetSetSprite( window, "window1" )
							WidgetSetSpriteColor( window, { 0, 0, 0, .5 } )
						else
							begin_game()
						end
					end, 
					nil,
					{name="wakaba-widget"}
			), START
		)
Menu:add( MWSpace.create( 0, 15 ), START )

Menu:add( MWSpace.create( 0, 15 ), START )
Menu:add( 
			MWFunctionButton.create( 
					{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
					"TUTORIAL",
					0, 
					function (sender, param) 
						RestoreKeyProcessors()
						difficulty = 1
						--assert(Loader.startGame( vars.start_map ))
						if vars.textwidget then DestroyWidget( vars.textwidget ) end
						Menu.hideMainMenu()
						Loader.startGame( vars.tutorial_map )
					end, 
					nil,
					{name="wakaba-widget"}
			), START
		)

Menu:add( MWSpace.create( 0, 15 ), START )
Menu:add( 
			MWFunctionButton.create( 
					{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
					"BACKSTORY",
					0, 
					function (sender, param)
						RestoreKeyProcessors()
						difficulty = vars.difficulty
						--assert(Loader.startGame( vars.start_map ))
						if vars.textwidget then DestroyWidget( vars.textwidget ) end
						Menu.hideMainMenu()
						Loader.startGame( vars.backstory_map )
					end, 
					nil,
					{name="wakaba-widget"}
			), START
		)

Menu:add( MWSpace.create( 0, 15 ), START )
Menu:add(
	MWFunctionButton.create(
		{name="default", color = {1, .8, .8, 1}, color_active = {1, .7, .7, 1}},
		"BACK",
		0,
		function (sender, param)
			RestoreKeyProcessors()
			Menu:goBack()
		end,
		nil,
		{ name = "wakaba-widget" }
	),
	START
)
Menu:add( MWSpace.create( 0, 15 ), START )
Menu:hide(START)
