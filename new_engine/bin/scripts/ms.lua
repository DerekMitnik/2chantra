MAP = "ms-test"
SET = "testset"
TRACK = "music/nsmpr_title.it"

--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------

ms = {}

function ms_heartbeat()
	ms.bars = GetBackMusicFFT()
	local l, r = GetBackMusicLevel()
	ms.vol = (l+r)/2
	ms.time = GetCurTime()
	
	for key,value in pairs(ms.rules) do
		value.total = 0
	end

	for i=1,#ms.bars do
		for key,value in pairs(ms.rules) do
			if type( value.from ) == 'number' and i >= value.from and i < value.to then
				value.total = value.total + ms.bars[i]
			end
		end
	end

	for key,value in pairs(ms.rules) do
		if not value.last_val then value.last_val = {0, 0, 0} end
		table.remove( value.last_val, 1 )
		if type( value.from ) == 'number' then
			value.last_val[3] = value.total / (value.to - value.from)
		else	--HERE: Different triggers
			value.last_val[3] = ms.vol
		end

		if value.last_val[3] < value.last_val[2] and value.last_val[2] > value.last_val[1] then
			value.score = ( value.score or 0 ) + value.last_val[2] - value.last_val[3]
			if ms.time - (value.last_event_time or -10000) > value.timeout then
				--Log( ms.time - (value.last_event_time or -10000), " ", ms.time, " ", value.last_event_time )
				for j=#value.objects,1,-1 do
					if (value.score or 0) > value.objects[j][1] then
						ms_spawn( key, value.objects[j][2] )
						--value.score = value.score - value.objects[j][1]
						value.score = 0
						value.last_event_time = ms.time
						break
					end
				end
			end
		end

		if value.score and value.score > 0 then
			value.score = math.max( 0, value.score - value.cooldown )
		end
	end
end

function ms_spawn( index, name )
	local rule = ms.rules[index]
	rule.spawnpoint = ((rule.spawnpoint or 0) + 1)

	if rule.type == "ENEMY" then
		--rule.spawnpoint = (((rule.spawnpoint or -1) + 1) % #(Loader.level.spawn_points[1] or {0})) + 1
		if rule.spawnpoint > #Loader.level.spawn_points[1] then rule.spawnpoint = 1 end
		CreateEnemy( name, Loader.level.spawn_points[1][rule.spawnpoint][1], Loader.level.spawn_points[1][rule.spawnpoint][2])
	else
		--rule.spawnpoint = (((rule.spawnpoint or -1) + 1) % #(Loader.level.spawn_points[2] or {0})) + 1
		if rule.spawnpoint > #Loader.level.spawn_points[2] then rule.spawnpoint = 1 end
		CreateItem( name, Loader.level.spawn_points[2][rule.spawnpoint][1], Loader.level.spawn_points[2][rule.spawnpoint][2])
	end
end

function ms_main()
	while true do
		ms_heartbeat()
		Wait(1)
	end
end

ms.rules = dofile("scripts/ms/"..SET..".lua")
Loader.level = dofile( "scripts/"..MAP..".lua" )
PlayBackMusic( TRACK )
Resume( NewMapThread( ms_main ))

return Loader.level
