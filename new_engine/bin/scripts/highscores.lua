Highscores = {}

if not Editor then
	require( "json" )
end

local test = '[{"pk": 193, "model": "highscores.score", "fields": {"achievements": 0, "version": 2, "seconds": 28, "character": "pear15soh", "score": 100, "mode": 1, "date": "2012-02-14 10:53:48", "position": null, "nickname": "ktd"}}, {"pk": 160, "model": "highscores.score", "fields": {"achievements": 0, "version": 2, "seconds": 28, "character": "pear15soh", "score": 100, "mode": 1, "date": "2012-02-13 17:18:52", "position": null, "nickname": "ktd"}}, {"pk": 159, "model": "highscores.score", "fields": {"achievements": 0, "version": 2, "seconds": 28, "character": "pear15soh", "score": 100, "mode": 1, "date": "2012-02-12 04:12:36", "position": null, "nickname": "ktd"}}, {"pk": 160, "model": "highscores.score", "fields": {"achievements": 0, "version": 2, "seconds": 28, "character": "pear15soh", "score": 100, "mode": 1, "date": "2012-02-13 17:18:52", "position": null, "nickname": "ktd"}}, {"pk": 193, "model": "highscores.score", "fields": {"achievements": 0, "version": 2, "seconds": 28, "character": "pear15soh", "score": 100, "mode": 1, "date": "2012-02-14 10:53:48", "position": 9, "nickname": "ktd"}}]'
--Log( serialize( "t", Json.Decode( test ) ) )
--local hs_debug = 1

local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
local function base64(data)
    return ((data:gsub('.', function(x) 
        local r,b='',x:byte()
        for i=8,1,-1 do r=r..(b%2^i-b%2^(i-1)>0 and '1' or '0') end
        return r;
    end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
        if (#x < 6) then return '' end
        local c=0
        for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
        return b:sub(c+1,c+1)
    end)..({ '', '==', '=' })[#data%3+1])
end

local months =
{
	"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
}
function Highscores.prepareDate( date_string )
	local year, month, day, hour, minute = string.match( date_string, "(%d+)%-(%d+)%-(%d+) (%d+):(%d+):(%d+)" )
	if not year then
		return date_string
	end
	local cur_date = os.date( "*t" )
	if tonumber(year) == cur_date.year then
		year = ""
	else
		year = " "..year
	end
	month = tonumber( month )
	return string.format( "%s %s%s, %s:%s", day, dictionary_string( months[month] ), year, hour, minute )
end

function Highscores.prepareNick( nick )
	return base64(cp1251_utf8(string.sub( nick, 1, 16 )))
end

function Highscores.getAchievements()
	local ach_num = 0
	for k, v in pairs( Achievements.info ) do
		ach_num = ach_num + 1
	end
	local ret = 0
	local i = 1
	for k, v in sorted_pairs( Achievements.info ) do
		if Game.profile.achievement_progress and Game.profile.achievement_progress[k] and Game.profile.achievement_progress[k] == Achievements.UNLOCKED then
			ret = setbit( ret, bit(i) )
		end
		i = i + 1
	end
	return ret
end

local local_highscores_tables = {}

function Highscores.updateTable( json, mode )
	local function deunicode( str )
		local num = string.match( str, "\\u(....)" )
		num = hex_dec( num )
		if num <= 1103 and num >= 1040 then
			return string.char( num - 848 )
		elseif num == 1025 then
			return '\168'
		elseif num == 1105 then
			return '\184'
		else
			return '?'
		end
	end
	local_highscores_tables[mode] = Json.Decode( string.gsub(utf8_cp1251(json), "\\u....", deunicode) )
end

function Highscores.showFullTable( back_event )
	local mode = 3
	local page = 0
	
	local background = CreateWidget( constants.wt_Widget, "", nil, -10, -10, CONFIG.scr_width + 20, CONFIG.scr_height + 20 )
	WidgetSetColorBox( background, { 0, 0, 0, 0.9 } )
	WidgetSetZ( background, 1 )
	
	local line1 = dictionary_string( "#" )
	local line2 = dictionary_string( "Name" )
	local line3 = dictionary_string( "Char" )
	local line4 = dictionary_string( "Score" )
	local line5 = dictionary_string( "Date" )
	local line6 = dictionary_string( "Time" )
	local line7 = dictionary_string( "BACK" )
	local line8 = dictionary_string( "MODE: SINGLEPLAYER" )
	local line9 = dictionary_string( "MODE: MULTIPLAYER" )
	
	local w
	local name_size = GetCaptionSize( "dialogue", "XXXXXXXXXXXXXXXX" )
	local window = background
	local color = { 1, 1, 0.6, 1 }
	local tab = local_highscores_tables[mode]
	
	w = CreateWidget( constants.wt_Label, "", background, 40, 20, 1, 1 )
	WidgetSetCaptionFont( w, "dialogue" )
	WidgetSetCaptionColor( w, color, false )
	WidgetSetCaption( w, line1 )
		
	sx, sy = GetCaptionSize( "dialogue", line2 )
	w = CreateWidget( constants.wt_Label, "", window, 80+name_size/2-sx/2, 20, 1, 1 )
	WidgetSetCaptionFont( w, "dialogue" )
	WidgetSetCaptionColor( w, color, false )
	WidgetSetCaption( w, line2  )

	w = CreateWidget( constants.wt_Label, "", window, 90+name_size-10, 20, 1, 1 )
	WidgetSetCaptionFont( w, "dialogue" )
	WidgetSetCaptionColor( w, color, false )
	WidgetSetCaption( w, line3 )

	w = CreateWidget( constants.wt_Label, "", window, 110+name_size+30, 20, 1, 1 )
	WidgetSetCaptionFont( w, "dialogue" )
	WidgetSetCaptionColor( w, color, false )
	WidgetSetCaption( w, line4 )
		
	sx, sy = GetCaptionSize( "dialogue", line5 )
	w = CreateWidget( constants.wt_Label, "", window, 120+2.25*name_size+20-sx/2-20, 20, 1, 1 )
	WidgetSetCaptionFont( w, "dialogue" )
	WidgetSetCaptionColor( w, color, false )
	WidgetSetCaption( w, line5 )

	w = CreateWidget( constants.wt_Label, "", window, 100+3*name_size+30, 20, 1, 1 )
	WidgetSetCaptionFont( w, "dialogue" )
	WidgetSetCaptionColor( w, color, false )
	WidgetSetCaption( w, line6 )
	
	local function ShowLine( y, num, color, cplace, parent )
		w = CreateWidget( constants.wt_Label, "", parent, 40, y, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, color, false )
		WidgetSetCaption( w, tostring( cplace ) )
		
		sx, sy = GetCaptionSize( "dialogue", tab[num].fields.nickname )
		w = CreateWidget( constants.wt_Label, "", parent, 80+name_size/2-sx/2, y, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, color, false )
		WidgetSetCaption( w, tab[num].fields.nickname )

		w = CreateWidget( constants.wt_Label, "", parent, 90+name_size, y, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, color, false )
		WidgetSetCaption( w, string.sub(tab[num].fields.character, 1, 1) )

		w = CreateWidget( constants.wt_Label, "", parent, 80+name_size+50, y, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, color, false )
		WidgetSetCaption( w, tab[num].fields.score )
		
		local dt = Highscores.prepareDate(tab[num].fields.date)
		sx, sy = GetCaptionSize( "dialogue", dt )
		w = CreateWidget( constants.wt_Label, "", parent, 100+1.5*name_size+120-sx/2, y, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, color, false )
		WidgetSetCaption( w, dt )

		w = CreateWidget( constants.wt_Label, "", parent, 80+3*name_size+50, y, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, color, false )
		local seconds
		seconds = tab[num].fields.seconds
		seconds = string.format( "%02i:%02i", math.floor(seconds/60), math.floor(seconds)%60 )
		WidgetSetCaption( w, seconds )
	end
	
	local per_page = math.floor((CONFIG.scr_height - 150) / 20)
	local func_error
	
	local page_widget = nil
	local function ShowPage()
		if page_widget then
			DestroyWidget( page_widget )
		end
		page_widget = CreateWidget( constants.wt_Widget, "", background, 0, 0, 1, 1 )
		local y = 60
		local col = {1,1,1,1}
		for i = (per_page*page)+1, math.min( #tab, per_page*(page+1) ) do
			for k, v in pairs( tab[i] ) do
				if type( k ) == 'string' then
					local new_k = string.gsub( k, "[^a-zA-Z]", "" )
					tab[i][new_k] = v
				end
			end
			if tab[i].fields then
				for k, v in pairs( tab[i].fields ) do
					if type( k ) == 'string' then
						local new_k = string.gsub( k, "[^a-zA-Z]", "" )
						tab[i].fields[new_k] = v
					end
				end
				if tab[i].fields.nickname == Game.profile.name then
					col = color
				else
					col = { .7, .7, .7, 1 }
				end
				ShowLine( y, i, col, i, page_widget )
			else
				return (func_error or function() end)()
			end
			y = y + 20
		end
	end

	w = CreateWidget( constants.wt_Button, "", background, 20, CONFIG.scr_height - 60, 200, sy )
	local mode_widget = w
	WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, false )
	WidgetSetCaptionColor( w, { .3, .3, 1, 1 }, true )
	local func_mode = function( sender )
		mode = 7 - mode
		if sender then
			WidgetSetCaption( sender, iff( mode == 3, line8, line9 ) )
		end
		Highscores.updateTable( GetHighscores(mode), mode )
		tab = local_highscores_tables[mode]
		if tab == "ERROR_SEND" then
			func_error()
		else
			page = 0
			ShowPage()
		end
	end
	WidgetSetLMouseClickProc( w, func_mode )
	WidgetSetCaption( w, line8 )
	
	sx, sy = GetCaptionSize( "default", line6 )
	w = CreateWidget( constants.wt_Button, "", background, 20, CONFIG.scr_height - 40, sx, sy )
	WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, false )
	WidgetSetCaptionColor( w, { .3, .3, 1, 1 }, true )
	local func_back = function()
		RestoreKeyProcessors()
		DestroyWidget( background )
		back_event()
	end
	WidgetSetLMouseClickProc( w, func_back )
	WidgetSetCaption( w, line7 )
	

	w = CreateWidget( constants.wt_Button, "", background, CONFIG.scr_width-80-16, CONFIG.scr_height - 60, 16, 16 )
	WidgetSetSprite( w, "up_down", true, 0 )
	local func_up = function()
		if page == 0 then
			return
		end
		page = page - 1
		ShowPage()
	end
	WidgetSetFocusProc( w, function( sender ) WidgetSetSpriteColor( sender, { .3, .3, 1, 1 } ) end )
	WidgetSetUnFocusProc( w, function( sender ) WidgetSetSpriteColor( sender, { 1, 1, 1, 1 } ) end )
	WidgetSetLMouseClickProc( w, func_up )
	WidgetSetZ( w, 1.05 )
	
	w = CreateWidget( constants.wt_Button, "", background, CONFIG.scr_width-80-16, CONFIG.scr_height - 40, 16, 16 )
	WidgetSetSprite( w, "up_down", true, 1 )
	local func_down = function()
		if page >= math.floor(#tab / per_page) then
			return
		end
		page = page + 1
		ShowPage()
	end
	WidgetSetFocusProc( w, function( sender ) WidgetSetSpriteColor( sender, { .3, .3, 1, 1 } ) end )
	WidgetSetUnFocusProc( w, function( sender ) WidgetSetSpriteColor( sender, { 1, 1, 1, 1 } ) end )
	WidgetSetLMouseClickProc( w, func_down )
	WidgetSetZ( w, 1.05 )
	
	RemoveKeyProcessors()
	GlobalSetKeyDownProc( function( key )
		if IsConfKey( key, config_keys.gui_nav_decline ) or IsConfKey( key, config_keys.gui_nav_menu ) then
			func_back()
			return true
		elseif IsConfKey( key, config_keys.gui_nav_next ) then
			func_down()
			return true
		elseif IsConfKey( key, config_keys.gui_nav_prev ) then
			func_up()
			return true
		elseif IsConfKey( key, config_keys.gui_nav_accept ) then
			func_mode( mode_widget )
			return true
		end
		return false
	end )
	
	func_error = function()
		RestoreKeyProcessors()
		DestroyWidget( background )
		Highscores.showError( "HIGHSCORES_ERROR", function()
			back_event()
		end, "ERROR" )
	end
	
	Highscores.updateTable( GetHighscores(mode), mode )
	tab = local_highscores_tables[mode]
	if tab == "ERROR_SEND" then
		func_error()
	else
		ShowPage()
	end
end

function Highscores.showTable( json, event, title )
	local function deunicode( str )
		local num = string.match( str, "\\u(....)" )
		num = hex_dec( num )
		if num <= 1103 and num >= 1040 then
			return string.char( num - 848 )
		elseif num == 1025 then
			return '\168'
		elseif num == 1105 then
			return '\184'
		else
			return '?'
		end
	end
	local tab = Json.Decode( string.gsub(utf8_cp1251(json), "\\u....", deunicode) )
	local place = tab[#tab].fields.position
	local higher = math.min( place - 1, 2 )
	local lower = #tab - 1 - higher

	local FONT_X, FONT_Y = GetCaptionSize( "dialogue", "I" )

	local window = CreateWidget( constants.wt_Panel, "", nil, 0, 0, 1, 1 )
	WidgetSetSprite( window, "window1" )
	
	local w
	local sx, sy
	
	local text1 = dictionary_string( title or "Congratulations" )
	local text2 = dictionary_string( "OK" )
	local name_size = GetCaptionSize( "dialogue", "XXXXXXXXXXXXXXXX" )
	local wx, wy = 100+4*name_size, 1
	
	sx, sy = GetCaptionSize( "default", text1 )
	w = CreateWidget( constants.wt_Label, "", window, wx/2 - sx/2, 20, 1, 1 )
	WidgetSetCaptionColor( w, { 1, 1, .7, 1 }, false )
	WidgetSetCaption( w, text1 )

	local cy = 20 + sy + 20

	local line1 = dictionary_string( "#" )
	local line2 = dictionary_string( "Name" )
	local line3 = dictionary_string( "Char" )
	local line4 = dictionary_string( "Score" )
	local line5 = dictionary_string( "Date" )
	local line6 = dictionary_string( "Time" )

	local function ShowLine( y, num, color, cplace, title )
		w = CreateWidget( constants.wt_Label, "", window, 20, y, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, color, false )
		WidgetSetCaption( w, iff( title, line1, tostring( cplace )) )
		
		sx, sy = GetCaptionSize( "dialogue", iff( title, line2, tab[num].fields.nickname ) )
		w = CreateWidget( constants.wt_Label, "", window, 60+name_size/2-sx/2, y, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, color, false )
		WidgetSetCaption( w, iff( title, line2, tab[num].fields.nickname)  )

		if title then
			w = CreateWidget( constants.wt_Label, "", window, 70+name_size-10, y, 1, 1 )
		else
			w = CreateWidget( constants.wt_Label, "", window, 70+name_size, y, 1, 1 )
		end
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, color, false )
		WidgetSetCaption( w, iff(title, line3, string.sub(tab[num].fields.character, 1, 1)) )

		w = CreateWidget( constants.wt_Label, "", window, 80+name_size+50, y, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, color, false )
		WidgetSetCaption( w, iff(title, line4, tab[num].fields.score) )
		
		if title then
			sx, sy = GetCaptionSize( "dialogue", iff( title, line5, tab[num].fields.nickname ) )
			w = CreateWidget( constants.wt_Label, "", window, 80+2.25*name_size+20-sx/2, y, 1, 1 )
		else
			w = CreateWidget( constants.wt_Label, "", window, 80+1.5*name_size+50, y, 1, 1 )
		end
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, color, false )
		WidgetSetCaption( w, iff(title, line5, tab[num].fields.date) )

		w = CreateWidget( constants.wt_Label, "", window, 80+3*name_size+50, y, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, color, false )
		local seconds
		if not title then
			seconds = tab[num].fields.seconds
			seconds = string.format( "%02i:%02i", math.floor(seconds/60), math.floor(seconds)%60 )
		end
		WidgetSetCaption( w, iff(title, line6, seconds ) )
	end

	ShowLine( cy, 1, { 1, 1, 0.6, 1 }, 0, true )
	cy = cy + FONT_Y + 10
	for i=0, higher-1 do
		ShowLine( cy, higher-i, { 1, 1, 1, 1 }, place - higher - 1 + i )
		cy = cy + FONT_Y + 10
	end
	ShowLine( cy, #tab, { 1, 1, 0.6, 1 }, place )
	cy = cy + FONT_Y + 10
	for i=1, lower do
		ShowLine( cy, higher+i, { 1, 1, 1, 1 }, place + i )
		cy = cy + FONT_Y + 10
	end
	cy = cy + 20
	sx, sy = GetCaptionSize( "default", text2 )
	w = CreateWidget( constants.wt_Button, "", window, wx/2 - sx/2, cy, 1, 1 )
	WidgetSetCaptionColor( w, { 1, 1, .7, 1 }, false )
	WidgetSetCaptionColor( w, { .6, 1, .6, 1 }, true )
	WidgetSetCaption( w, text2 )
	WidgetSetLMouseClickProc( w, function()
		DestroyWidget( window )
		if event then
			event()
		end
	end )
	cy = cy + 10

	wy = cy + 20
	WidgetSetSize( window, wx, wy )
	WidgetSetPos( window, (CONFIG.scr_width-wx)/2, (CONFIG.scr_height-wy)/2 )
end

function Highscores.showError( err, event, title )
	local window, w, sx, sy

	local text1 = ""
	if title then
		text1 = dictionary_string( title )
	end
	local text2 = dictionary_string( err or "?" )
	local text3 = dictionary_string( "OK" )

	local cy = 20

	window = CreateWidget( constants.wt_Panel, "", nil, 0, 0, 1, 1 )
	WidgetSetSprite( window, "window1" )
	if title then
		sx, sy = GetCaptionSize( "default", text1 )
		w = CreateWidget( constants.wt_Label, "", window, 120 - sx/2, 20, 200, 1 )
		WidgetSetCaptionFont( w, "default" )
		WidgetSetCaptionColor( w, {1,1,.7,1}, false, {0,0,0,1} )
		WidgetSetCaption( w, text1, true )
		cy = cy + sy + 20
	end
	w = CreateWidget( constants.wt_Label, "", window, 20, cy, 200, 1 )
	WidgetSetCaptionFont( w, "dialogue" )
	WidgetSetCaptionColor( w, {.8,.8,.8,1}, false, {0,0,0,1} )
	WidgetSetCaption( w, text2, true )
	sx, sy = GetMultilineCaptionSize( "dialogue", text2, 200, 1 )
	cy = cy + sy + 10
	local csx, csy = GetCaptionSize( "default", text3 )
	w = CreateWidget( constants.wt_Button, "", window, 120 - csx/2, cy+10, 40, 10 )
	WidgetSetCaptionFont( w, "default" )
	WidgetSetCaptionColor( w, {1,.8,.8,1}, false, {0,0,0,1} )
	WidgetSetCaptionColor( w, {1,.4,.4,1}, true, {0,0,0,1} )
	WidgetSetCaption( w, text3 )
	WidgetSetLMouseClickProc( w, function()
		DestroyWidget( window )
		if event then
			event()
		end
	end )
	cy = cy + 20
	local contx, conty = 260, cy+20
	WidgetSetSize( window, contx, conty )
	WidgetSetPos( window, (CONFIG.scr_width-contx)/2, (CONFIG.scr_height-conty)/2 )
end


function Highscores.send( nick, actor, mode )
--[[	if hs_debug == 1 then
		return true, test
	elseif hs_debug == 2 then
		return false, "SCORE_LOCKED"
	elseif hs_debug == 3 then
		return false, "SERVER_ERROR"
	elseif hs_debug == 4 then
		return false, "VALIDATION_ERROR"
	end]]
	local mode = (mode or 1) + 2
	if Game.custom_map then
		return false, "CUSTOM_MAP_SCORE_LOCK"
	end
	if GetScoreLock() then
		return false, "SCORE_LOCKED"
	end
	local seconds = (mapvar and mapvar.game_timer) and mapvar.game_timer or 0
	seconds = seconds / 1000
	local check, score = ProtectedVariable( Game.variables.score_start + actor )
	if not check then
		return false, "SCORE_LOCKED"
	end
	local characters = 
	{
		pear15soh = "A",
		pear15unyl = "B",
	}
	local character = characters[mapvar.actors[actor].info.character] or string.sub(mapvar.actors[actor].info.character, 1, 16)
	local success, result = SendHighscores( Highscores.prepareNick(nick), score, seconds, mode, Highscores.getAchievements(), character )
		
	local results =
	{
		["Your score is compromised"] = "SCORE_LOCKED",
		["Cannot send score: score must be positive or zero"] = "SCORE_LOCKED",
		["Cannot send score: seconds must be positive"] = "SCORE_LOCKED",
		["Cannot send score: nickname is too long"] = "SCORE_LOCKED",
		["Cannot send score: http_send_request failed"] = "SERVER_ERROR",
	}
	
	if not success then
		return false, results[result] or "UNKNOWN_ERROR"
	end
	
	if string.match( result, "^VALIDATION_ERROR" ) then
		return false, "VALIDATION_ERROR"
	end
	
	if string.match( result, "^ERROR" ) then
		return false, "UNKNOWN_ERROR"
	end
	
	return success, result
end

function Highscores.showMenu( event, back_event )
	local lock = GetScoreLock()
	local players = #mapvar.actors
	local window

	local text1 = dictionary_string( "Score:" )
	local text2 = dictionary_string( "Time:" )
	local text3 = dictionary_string( "Name:" )
	local text4 = dictionary_string( "PLAYER 1" )
	local text5 = dictionary_string( "PLAYER 2" )
	local text6 = dictionary_string( "SEND" )
	local text7 = dictionary_string( "BACK" )
	local text8 = dictionary_string( "BAD_SCORE" )

	local sx, sy
	local COLUMN_1 = 0
	sx, sy = GetCaptionSize( "dialogue", text1 ); COLUMN_1 = math.max( COLUMN_1, sx )
	sx, sy = GetCaptionSize( "dialogue", text2 ); COLUMN_1 = math.max( COLUMN_1, sx )
	sx, sy = GetCaptionSize( "dialogue", text3 ); COLUMN_1 = math.max( COLUMN_1, sx )
	COLUMN_1 = COLUMN_1 + 40
	local w
	local name_size = GetCaptionSize( "dialogue", "XXXXXXXXXXXXXXXX" )

	if lock then
		window = CreateWidget( constants.wt_Panel, "", nil, 0, 0, 1, 1 )
		WidgetSetSprite( window, "window1" )
		w = CreateWidget( constants.wt_Label, "", window, 20, 20, 200, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {.8,.8,.8,1}, false )
		WidgetSetCaption( w, text8, true )
		sx, sy = GetMultilineCaptionSize( "dialogue", text8, 200, 1 )
		local csx, csy = GetCaptionSize( "dialogue", text7 )
		w = CreateWidget( constants.wt_Button, "", window, 120 - csx/2, sy + 40, 40, 10 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,.8,.8,1}, false )
		WidgetSetCaptionColor( w, {1,.4,.4,1}, true )
		WidgetSetCaption( w, text7 )
		WidgetSetLMouseClickProc( w, function()
			DestroyWidget( window )
			if back_event then
				back_event()
			end
		end )
		local contx, conty = 240, sy + 70
		WidgetSetSize( window, contx, conty )
		WidgetSetPos( window, (CONFIG.scr_width-contx)/2, (CONFIG.scr_height-conty)/2 )
	elseif players == 1 then
		local _, score1 = ProtectedVariable( Game.variables.score_start + 1 )
		local seconds = (mapvar and mapvar.game_timer) and mapvar.game_timer or 0
		local colorbox
		local input
		seconds = seconds / 1000
		seconds = string.format( "%02i:%02i", math.floor( seconds / 60 ), math.floor( seconds ) % 60 )
		window = CreateWidget( constants.wt_Panel, "", nil, 0, 0, 1, 1 )
		WidgetSetSprite( window, "window1" )

		local contx, conty = 0, 20
		w = CreateWidget( constants.wt_Label, "", window, 10, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaption( w, text1 )
		sx, sy = GetCaptionSize( "dialogue", tostring( score1 ) )
		w = CreateWidget( constants.wt_Label, "", window, 10+COLUMN_1+name_size - sx, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaption( w, tostring( score1 ) )

		conty = conty + sy + 10
		w = CreateWidget( constants.wt_Label, "", window, 10, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaption( w, text2 )
		sx, sy = GetCaptionSize( "dialogue", tostring( seconds ) )
		w = CreateWidget( constants.wt_Label, "", window, 10+COLUMN_1+name_size - sx, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaption( w, tostring( seconds ) )

		conty = conty + sy + 10
		w = CreateWidget( constants.wt_Label, "", window, 10, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaption( w, text3 )
		sx, sy = GetCaptionSize( "dialogue", tostring( seconds ) )
		w = CreateWidget( constants.wt_Widget, "", window, 10+COLUMN_1-5, conty-5, name_size+10, sy+10 )
		WidgetSetColorBox( w, { 0, 0, 0, 1 } )
		WidgetSetZ( w, 1.01 )
		colorbox = w
		w = CreateWidget( constants.wt_Textfield, "", window, 10+COLUMN_1, conty, name_size, sy )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, { .7, .7, .7, 1 }, false )
		WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, true )
		WidgetSetMaxTextfieldSize( w, 16 )	
		WidgetSetCaption( w, string.sub( Game.profile.name, 1, 16 ) )
		WidgetSetBorderColor( colorbox, { 1, 1, 1, 1 }, true )
		WidgetSetFocusProc( w, function()
			WidgetSetColorBox( colorbox, { .2, .2, .2, 1 } )
			WidgetSetZ( colorbox, 1.01 )
			WidgetSetBorder( colorbox, true )
		end )
		WidgetSetUnFocusProc( w, function()
			WidgetSetColorBox( colorbox, { 0, 0, 0, 1 } )
			WidgetSetZ( colorbox, 1.01 )
			WidgetSetBorder( colorbox, false )
		end )
		input = w
		--[[local x, y = 10+COLUMN_1, conty
		WidgetSetKeyInputProc( w, function()
			local c = WidgetGetCaption( input )
			local sx, sy = GetCaptionSize( "dialogue", input )
			WidgetSetPos( input, x + name_size - sx - 20, y, true )
		end )
		sx_sy = GetCaptionSize( "dialogue", string.sub( Game.profile.name, 1, 16 ) )
		WidgetSetPos( input, x + name_size - sx - 20, y )]]

		conty = conty + sy + 20
		contx = 20 + COLUMN_1 + name_size + 5
		sx, sy = GetCaptionSize( "dialogue", text6 )
		w = CreateWidget( constants.wt_Button, "", window, contx/4 - sx/2, conty, sx, sy )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaptionColor( w, {.6,.6,1,1}, true )
		WidgetSetCaption( w, text6 )
		WidgetSetLMouseClickProc( w, function()
			local success, result = Highscores.send( WidgetGetCaption( input ), 1, Game.info.mode or #mapvar.actors )
			DestroyWidget( window )
			if success then
				Highscores.showTable( result, function() event() end )
			else
				Highscores.showError( result, function() event() end )
			end
		end )
		sx, sy = GetCaptionSize( "dialogue", text7 )
		w = CreateWidget( constants.wt_Button, "", window, 3*contx/4 - sx/2, conty, sx, sy )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaptionColor( w, {.6,.6,1,1}, true )
		WidgetSetCaption( w, text7 )
		WidgetSetLMouseClickProc( w, function()
			DestroyWidget( window )
			if back_event then
				back_event()
			end
		end )
		conty = conty + sy + 20
		WidgetSetSize( window, contx, conty )
		WidgetSetPos( window, (CONFIG.scr_width-contx)/2, (CONFIG.scr_height-conty)/2 )
	elseif players == 2 then
		local _, score1 = ProtectedVariable( Game.variables.score_start + 1 )
		local _, score2 = ProtectedVariable( Game.variables.score_start + 2 )
		local seconds = (mapvar and mapvar.game_timer) and mapvar.game_timer or 0
		local colorbox1, colorbox2
		local input1, input2
		seconds = seconds / 1000
		seconds = string.format( "%02i:%02i", math.floor( seconds / 60 ), math.floor( seconds ) % 60 )
		window = CreateWidget( constants.wt_Panel, "", nil, 0, 0, 1, 1 )
		WidgetSetSprite( window, "window1" )

		local contx, conty = 40 + 2*COLUMN_1 + 2*name_size + 30 + 5, 20
	
		sx, sy = GetCaptionSize( "dialogue", text4 )
		w = CreateWidget( constants.wt_Label, "", window, 10+(COLUMN_1+name_size-sx)/2, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,.7,1}, false )
		WidgetSetCaption( w, text4 )
		
		conty = conty + 15 + sy
		w = CreateWidget( constants.wt_Label, "", window, 10, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaption( w, text1 )
		sx, sy = GetCaptionSize( "dialogue", tostring( score1 ) )
		w = CreateWidget( constants.wt_Label, "", window, 10+COLUMN_1+name_size - sx, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaption( w, tostring( score1 ) )

		conty = conty + sy + 10
		w = CreateWidget( constants.wt_Label, "", window, 10, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaption( w, text2 )
		sx, sy = GetCaptionSize( "dialogue", tostring( seconds ) )
		w = CreateWidget( constants.wt_Label, "", window, 10+COLUMN_1+name_size - sx, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaption( w, tostring( seconds ) )

		conty = conty + sy + 10
		w = CreateWidget( constants.wt_Label, "", window, 10, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaption( w, text3 )
		sx, sy = GetCaptionSize( "dialogue", tostring( seconds ) )
		w = CreateWidget( constants.wt_Widget, "", window, 10+COLUMN_1-5, conty-5, name_size+10, sy+10 )
		WidgetSetColorBox( w, { 0, 0, 0, 1 } )
		WidgetSetZ( w, 1.01 )
		colorbox1 = w
		w = CreateWidget( constants.wt_Textfield, "", window, 10+COLUMN_1, conty, name_size, sy )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, { .7, .7, .7, 1 }, false )
		WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, true )
		WidgetSetMaxTextfieldSize( w, 16 )	
		WidgetSetCaption( w, string.sub( Game.profile.name, 1, 16 ) )
		WidgetSetBorderColor( w, { 1, 1, 1, 1 }, true )
		WidgetSetFocusProc( w, function()
			WidgetSetColorBox( colorbox1, { .2, .2, .2, 1 } )
			WidgetSetZ( colorbox1, 1.01 )
			WidgetSetBorder( colorbox1, true )
		end )
		WidgetSetUnFocusProc( w, function()
			WidgetSetColorBox( colorbox1, { 0, 0, 0, 1 } )
			WidgetSetZ( colorbox1, 1.01 )
			WidgetSetBorder( colorbox1, false )
		end )
		input1 = w
		--[[local x, y = 10+COLUMN_1, conty
		WidgetSetKeyInputProc( w, function()
			local c = WidgetGetCaption( input )
			local sx, sy = GetCaptionSize( "dialogue", input )
			WidgetSetPos( input, x + name_size - sx - 20, y, true )
		end )
		sx_sy = GetCaptionSize( "dialogue", string.sub( Game.profile.name, 1, 16 ) )
		WidgetSetPos( input, x + name_size - sx - 20, y )]]

		contx, conty = 40 + 2*COLUMN_1 + 2*name_size + 30 + 5, 20
		local cx = 10 + COLUMN_1 + name_size + 30
	
		sx, sy = GetCaptionSize( "dialogue", text4 )
		w = CreateWidget( constants.wt_Label, "", window, cx+(COLUMN_1+name_size-sx)/2, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,.7,1}, false )
		WidgetSetCaption( w, text5 )
		
		conty = conty + 15 + sy
		w = CreateWidget( constants.wt_Label, "", window, cx, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaption( w, text1 )
		sx, sy = GetCaptionSize( "dialogue", tostring( score1 ) )
		w = CreateWidget( constants.wt_Label, "", window, cx+COLUMN_1+name_size - sx, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaption( w, tostring( score1 ) )

		conty = conty + sy + 10
		w = CreateWidget( constants.wt_Label, "", window, cx, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaption( w, text2 )
		sx, sy = GetCaptionSize( "dialogue", tostring( seconds ) )
		w = CreateWidget( constants.wt_Label, "", window, cx+COLUMN_1+name_size - sx, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaption( w, tostring( seconds ) )

		conty = conty + sy + 10
		w = CreateWidget( constants.wt_Label, "", window, cx, conty, 1, 1 )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaption( w, text3 )
		sx, sy = GetCaptionSize( "dialogue", tostring( seconds ) )
		w = CreateWidget( constants.wt_Widget, "", window, cx+COLUMN_1-5, conty-5, name_size+10, sy+10 )
		WidgetSetColorBox( w, { 0, 0, 0, 1 } )
		WidgetSetZ( w, 1.01 )
		colorbox2 = w
		w = CreateWidget( constants.wt_Textfield, "", window, cx+COLUMN_1, conty, name_size, sy )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, { .7, .7, .7, 1 }, false )
		WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, true )
		WidgetSetMaxTextfieldSize( w, 16 )	
		WidgetSetCaption( w, string.sub( Game.RandomName(), 1, 16 ) )
		WidgetSetBorderColor( colorbox2, { 1, 1, 1, 1 }, true )
		WidgetSetFocusProc( w, function()
			WidgetSetColorBox( colorbox2, { .2, .2, .2, 1 } )
			WidgetSetZ( colorbox2, 1.01 )
			WidgetSetBorder( colorbox2, true )
		end )
		WidgetSetUnFocusProc( w, function()
			WidgetSetColorBox( colorbox2, { 0, 0, 0, 1 } )
			WidgetSetZ( colorbox2, 1.01 )
			WidgetSetBorder( colorbox2, false )
		end )
		input2 = w
		--[[local x, y = 10+COLUMN_1, conty
		WidgetSetKeyInputProc( w, function()
			local c = WidgetGetCaption( input )
			local sx, sy = GetCaptionSize( "dialogue", input )
			WidgetSetPos( input, x + name_size - sx - 20, y, true )
		end )
		sx_sy = GetCaptionSize( "dialogue", string.sub( Game.profile.name, 1, 16 ) )
		WidgetSetPos( input, x + name_size - sx - 20, y )]]	

		conty = conty + sy + 20
		sx, sy = GetCaptionSize( "dialogue", text6 )
		w = CreateWidget( constants.wt_Button, "", window, contx/4 - sx/2, conty, sx, sy )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaptionColor( w, {.6,.6,1,1}, true )
		WidgetSetCaption( w, text6 )
		WidgetSetLMouseClickProc( w, function()
			local success1, result1 = Highscores.send( WidgetGetCaption( input1 ), 1, Game.info.mode or #mapvar.actors )
			local success2, result2 = Highscores.send( WidgetGetCaption( input2 ), 2, Game.info.mode or #mapvar.actors )

			--TODO: show results
			DestroyWidget( window )

			local pl2 = function()
				if success2 then
					Highscores.showTable( result2, function() event() end, "PLAYER 2" )
				else
					Highscores.showError( result, function() event() end, "PLAYER 2" )
				end
			end
			if success1 then
				Highscores.showTable( result1, function() pl2() end, "PLAYER 1" )
			else
				Highscores.showError( result, function() pl2() end, "PLAYER 1" )
			end
		end )
		sx, sy = GetCaptionSize( "dialogue", text7 )
		w = CreateWidget( constants.wt_Button, "", window, 3*contx/4 - sx/2, conty, sx, sy )
		WidgetSetCaptionFont( w, "dialogue" )
		WidgetSetCaptionColor( w, {1,1,1,1}, false )
		WidgetSetCaptionColor( w, {.6,.6,1,1}, true )
		WidgetSetCaption( w, text7 )
		WidgetSetLMouseClickProc( w, function()
			DestroyWidget( window )
			if back_event then
				back_event()
			end
		end )
		conty = conty + sy + 20
		WidgetSetSize( window, contx, conty )
		WidgetSetPos( window, (CONFIG.scr_width-contx)/2, (CONFIG.scr_height-conty)/2 )
	else
		error( "Higscores.showMenu(): OH GOD WHAT" )
	end	
end
