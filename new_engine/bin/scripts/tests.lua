Test = {}
--[[
Provides insight on using some functions by providing examples.
--]]

--behaves a little more like print than Log
local function LogPrint( ... )
	local ret = {}
	for k, v in ipairs( arg ) do
		table.insert( ret, tostring( v ) )
	end
	Log( table.concat( ret, " " ) )
end

function Test.Vector2()
	LogPrint( "-----------------------" )
	LogPrint( "   Vector2 metatable   " )
	LogPrint( "-----------------------" )
	t = GetPlayerCharacter():aabb().p
	LogPrint( t )
	LogPrint( t:normalize() )
	LogPrint( t:perp() )
	LogPrint( t + { x = 2, y = 3 } )
	LogPrint( t + Vector2.new( 2, 3 ) )
	LogPrint( t.x, t[1] )
	LogPrint( t:angle() )
	LogPrint( Vector2.new( 2, 3 ) )
	LogPrint( Vector2.new{ 2, 3 } )
	LogPrint( Vector2.new{ x=2, y=3 }:angle() )
	LogPrint( Vector2( 2, 3 ) )
end

function Test.Color()
	LogPrint( "-----------------------" )
	LogPrint( "    Color metatable    " )
	LogPrint( "-----------------------" )
	t = GetPlayerCharacter():sprite_color()
	LogPrint( t )
	LogPrint( Color.new( 255, 128, 0 ) )
	LogPrint( Color.new( 1, 0.5, 0 ) )
	LogPrint( Color.new( 255, 0.5, 0, 1, true ) )
	LogPrint( Color.new( 255, 128, 0, 255 ) )
	LogPrint( Color.new( 1, 0.5, 0, 0.5 ) )
	LogPrint( Color( 1, 0.5, 0 ) )
end

function Test.CustomObjectFunctions()
	LogPrint( "-----------------------" )
	LogPrint( "Custom object functions" )
	LogPrint( "-----------------------" )
	RegisterCustomObjectFunction( "hello_world", function( obj ) LogPrint( string.format("Hello from %s!", tostring(obj)) ) end )
	RegisterCustomObjectFunction( "set", function( obj, name, value ) 
		if not mapvar.tmp[obj] then
			mapvar.tmp[obj] = {}
			mapvar.tmp[obj][name] = value
		end
	end )
	RegisterCustomObjectFunction( "get", function( obj, name )
		if not mapvar.tmp[obj] then
			return nil
		end
		return mapvar.tmp[obj][name]
	end )

	obj = GetPlayerCharacter()
	obj:hello_world()
	obj:set( "important value", 28 )
	LogPrint( obj:get( "important value" ) )
end

function Test.Script()
	LogPrint( "-----------------------" )
	LogPrint( "      Script type      " )
	LogPrint( "-----------------------" )
	require( "pear15-script" )

	local script = Script( [[Log( "test" )]] )
	LogPrint( "This prints 'test':" )
	script()
	LogPrint("These are the actual script and its string:" )
	LogPrint( script.script )
	LogPrint( script )

	LogPrint( "Trying to load an invalid script." )
	script = Script( [[function this_is_an_error()]] )
	LogPrint( string.format( "This should be nil: %s", tostring(script) ) )

	script = Script( [[Log("script v.1")]] )
	local a = { scr = script }
	local b = { scr = script }
	Log( "Scripts in both tables are the same." )
	a.scr()
	b.scr()
	LogPrint( "Updating script with Script:update from one of the tables." )
	a.scr:update( [[Log("script v.2")]] )
	LogPrint( "Both scripts are still the same." )
	a.scr()
	b.scr()
	LogPrint( "Updating scripts by replacing it." )
	a.scr = Script( [[Log("script v.3")]] )
	LogPrint( "Now scripts in tables are different." )
	a.scr()
	b.scr()

	LogPrint( "Currently loaded script:" )
	LogPrint( script )
	LogPrint( "Updating it with invalid script." )
	script:update( [[function this is an error()]] )
	LogPrint( "It shouldn't have changed:" )
	LogPrint( script )
	LogPrint( "And running it from the table still works:" )
	a.scr()

	LogPrint( "Trying to update the script using '.' instead of ':' :" )
	script.update( [[Log("This is a valid script")]] )

	local local_val = 28
	LogPrint( "Local values can be stored in an addional parameter table to at least partially emulate closure." )
	script = Script( [[Log(local_val); a.scr()]], { local_val = local_val, a = a } )
	script()
	LogPrint( "Updating table and number. Note that number would remain the same because it is stored by value..." )
	a.scr:update( [[Log("script v.28")]] )
	local_val = 3
	script()
end

function Test.ObjectValues()
	LogPrint( "-----------------------" )
	LogPrint( "     Object values     " )
	LogPrint( "-----------------------" )
	require( "pear15-metamethods" )

	obj = GetPlayerCharacter()

	LogPrint( "Store value and later use it." )
	obj:set( "value", "Some value" )
	LogPrint( "Some value:", obj:get( "value" ) )

	LogPrint( "Methods other than 'get' and 'set' also check for type and return default values." )
	obj:set_number( "number", 28 )
	LogPrint( "0, because it is a string:", obj:get_number( "value" ) )
	LogPrint( "28, as it was set:", obj:get_number( "number" ) )
	LogPrint( "0, because the value is not set:", obj:get_number( "empty" ) )
	LogPrint( "Length of default vector (0, 0) is zero:", obj:get_vector("value"):len() )
	
	LogPrint( "These methods can also be used both as getter and setter at the same time, just like with default userdata methods:" )
	local vect = obj:vector( "vector_value", Vector2.new( 2, 8 ) )
	LogPrint( vect, obj:vector("vector_value"), obj:get( "vector_value" ) )

	LogPrint( "Executing a script is always safe because of the default value." )
	obj:script("test", Script([[Log("Test script")]]) )
	obj:script("test")()
	obj:script("empty_script")()
end
