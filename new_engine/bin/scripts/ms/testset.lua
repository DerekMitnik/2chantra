return
{
	{ 
		name = "bass",
		from = 1,
		to = 4,
		type = "ENEMY",
		timeout = 1000,
		cooldown = 0.001,
		objects =
		{
				{ 0.1, "btard" },
				{ 0.2, "btard-ambush" },
				{ 0.25, "btard-ny" },
				{ 0.3, "superbtard" },
		}
	},
	{ 
		name = "low",
		from = 3,
		to = 6,
		type = "ENEMY",
		timeout = 500,
		cooldown = 0.001,
		objects =
		{
				{ 0.25, "slowpoke" },
				{ 0.3, "slowpoke-moonwalking" },
				{ 0.35, "technopoke" },
		}
	},
	{ 
		name = "mid",
		from = 6,
		to = 64,
		type = "ENEMY",
		timeout = 500,
		cooldown = 0.001,
		objects =
			{
				{ 0.1, "btard" },
				{ 0.2, "btard-ambush" },
				{ 0.25, "btard-ny" },
				{ 0.3, "superbtard" },
			}
	},
	{ 
		name = "high",
		from = 64,
		to = 256,
		type = "ENEMY",
		timeout = 500,
		cooldown = 0.001,
		objects =
			{
				{ 0.25, "slowpoke" },
				{ 0.3, "slowpoke-moonwalking" },
				{ 0.35, "technopoke" },
			}
	},
	{ 
		name = "volume1",
		from = "VOLUME",
		type = "ITEM",
		timeout = 500,
		cooldown = 0.001,
		objects =
		{
				{ 3000, "ammo" },
				{ 5000, "vegetable1" },
				{ 10000, "vegetable3" },
		}
	},
}