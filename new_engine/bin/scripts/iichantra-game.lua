require( "iichantra-weaponshop" )

assert(false, "Too old and broken")

Game = {}
Game.actors = {}

last_heartbeat_time = 0

_NetConnect = NetConnect
_NetHost = NetHost

NetConnect = function( host, port )
	NETGAME = true
	_NetConnect( host, port )
end

NetHost = function( port )
	NETGAME = true
	_NetHost( port )
end

function Game.Heartbeat()
	if (GetCurTime() or 0) - last_heartbeat_time < 1000 then return end
	if (GamePaused()) then return end
	last_heartbeat_time = (GetCurTime() or 0)

	if not GetPlayer() then return end
	-- if not mapvar.sync then 
		-- mapvar.sync = 0
	-- end
	-- SOH >0, Unyl < 0
	-- ds = 0.2
	-- local plnum = GetPlayerNum()
	-- if  plnum == 2 then
		-- ds = -0.2
	-- end
--Halloween: no need for sync
	-- ds = 0
	-- if mapvar.weapon and mapvar.weapon[mapvar.active_char or "CIRNO"] == "bonus_sync" then
		-- ds = 0
	-- end
	-- if (ds*mapvar.sync < 0) or (mapvar.sync == 0 and ds<0) then
		-- ds = ds * (difficulty or 1)
	-- else
		-- ds = ds / (difficulty or 1)
	-- end
	-- mapvar.sync = mapvar.sync + ds
	-- if mapvar.sync > 30 then mapvar.sync = 30 end
	-- if mapvar.sync < -30 then mapvar.sync = -30 end
	
	-- local char = {}
	-- char[1] = GetPlayer(1)
	-- char[2] = GetPlayer(2)

	-- if mapvar.sync > 17 or mapvar.sync < -17 then
		-- PlaySnd( "sync-siren.ogg", true )
	-- end
	-- if mapvar.sync < -20 and plnum == 2 then
		-- DamageObject(char[2].id, 5*math.floor(mapvar.sync/(-10))*(difficulty or 1))
	-- end
	-- if mapvar.sync > 20 and plnum == 1 then
		-- DamageObject(char[1].id, 5*math.floor(mapvar.sync/10)*(difficulty or 1))
	-- end

	local char
	if mapvar.char then
		char = GetPlayer(mapvar.char[mapvar.active_char] or 10)
	end
	if char and mapvar.weapon then
	-- for i=1,2 do
		if mapvar.weapon[mapvar.active_char] == "bonus_ammo" then
			AddBonusAmmo(char.id, 30)
		elseif mapvar.weapon[mapvar.active_char] == "bonus_health" then
			AddBonusHealth(char.id, 20)
		end
	end
end

function StabilizeSync( id, ammount )
	local bonus = -1
	if mapvar.sync < 0 then
		bonus = 1
	end
	mapvar.sync = mapvar.sync + bonus*math.min(math.abs(mapvar.sync), ammount)
end

function Game.CharacterSystem( old_id, new_id )
	
	local char1 = GetPlayer(1).id
	local char2 = GetPlayer(2).id
	local diff = ((difficulty or 1)-1)/5+1;

	if new_id == char2 then
		--changing to Unyl-chan	
		k = ( 30 + mapvar.sync ) / 30
		mapvar = { jump_vel = 10+1.5*k, walk_acc=2.25+k/2, max_health=120/(difficulty or 1), max_x_vel = (2.25+k/2) }
		ReplacePrimaryWeapon(old_id, "sfg9000")
		if mapvar.sync > 20 and secrets then
			for key, value in pairs(secrets) do
				SetObjAnim(value, "secret", false)
			end
			if Loader and Loader.level and Loader.level.placeSecrets then
				Loader.level.placeSecrets()
			end
		end
	else
		k = ( 30 - mapvar.sync ) / 30	
		mapvar = { jump_vel = 10, walk_acc=2.25, max_health=(120+80*k)/(difficulty or 1), max_x_vel = 2.25 }
		if mapvar.sync < -20 then
			ReplacePrimaryWeapon(new_id, "rapid")
		end
		if secrets then
			for key, value in pairs(secrets) do
				SetObjAnim(value, "normal", false)
			end
			if Loader and Loader.level and Loader.level.removeSecrets then
				Loader.level.removeSecrets()
			end
		end
	end
	--Log(serialize("mapvar", mapvar))

	if not mapvar.bonuses then mapvar.bonuses = {} end
	if not mapvar.weapon then mapvar.weapon = {} end
	do	-- определение active_char
		local active_num = iff( new_id == char1, 1, 2 )
		local active_char = "CIRNO"
		for kk, kv in pairs(mapvar.char) do
			if kv == active_num then active_char = kk; break; end
		end
		mapvar.active_char = active_char
		
		if Loader.level.char then 
			mapvar.active_char = Loader.level.char
		end
	end

	mapvar.bonuses[0] = mapvar.bonuses[mapvar.active_char]
	mapvar.weapon[0] = mapvar.weapon[mapvar.active_char]
	

	SetPlayerStats(new_id, mapvar)
end

function varchange( varname, change )
	if varname == "score" then
		if not mapvar.next_life then mapvar.next_life = 5000 end
		if mapvar.score >= mapvar.next_life then
			mapvar.lives = mapvar.lives + 1
			mapvar.next_life = mapvar.next_life * 2
		end
		mapvar.score = math.min( mapvar.score, 10000-(mapvar.score_part_two or 10000)+change, ((mapvar.score_part_three or 0)/(-2))+change )
		mapvar.score_part_two = 10000 - mapvar.score
		mapvar.score_part_three = -2*mapvar.score
	end
end

function BonusCombo( first, second, what1, what2 )
	return ( (what1 == first) and (what2 == second) ) or ( (what1 == second) and (what2 == first) )
end

function RemoveDoubleBonus( player_name, bonus_type, char1_id, char2_id )
	Wait(10000)
	if bonus_type == "bonus_speed" then
		-- Тут все равно обоим чарам меняется скорость, так что не нужны заморочки с определением активного чара
		local char1 = GetPlayer(1)
		local char2 = GetPlayer(2)
		if (char1 and char1.id == char1_id) then AddBonusSpeed(char1.id, -3) end
		if (char2 and char2.id == char2_id) then AddBonusSpeed(char2.id, -3) end
	elseif bonus_type == "bonus_invul" then
		local num = mapvar.char[player_name]
		if num == 1 or num == 2 then 
			SetObjInvincible(GetPlayer(num).id, false)
		end
	end
	mapvar.weapon[player_name] = nil
	if player_name == mapvar.active_char then mapvar.weapon[0] = nil end
end

function WeaponSystem( id, weapon )

	if Loader and Loader.level and Loader.level.WeaponBonus then
		Loader.level.WeaponBonus( id, weapon )
	end
	
	local char1 = (GetPlayer(1) or { id = -1 })
	local char2 = (GetPlayer(2) or { id = -1 })
	
	do	-- определение active_char
		local active_num = iff( id == char1.id, 1, 2 )
		local active_char = "CIRNO"
		for kk, kv in pairs(mapvar.char) do
			if kv == active_num then active_char = kk; break; end
		end
		mapvar.active_char = active_char
		
		if Loader.level.char then 
			mapvar.active_char = Loader.level.char
		end
	end
	
	if not mapvar.bonuses then mapvar.bonuses = {} end
	

	if not mapvar.bonuses[mapvar.active_char ] then mapvar.bonuses[mapvar.active_char ] = {} end

	local bt = mapvar.bonuses[mapvar.active_char ]
	if #bt == 2 then
		table.remove(bt, 1)
	end
	if not mapvar.weapon then mapvar.weapon = {} end
	table.insert( bt, weapon )
	if mapvar.active_char and #bt == 2 then
		--circle
		if BonusCombo("circle", "triangle", bt[1], bt[2]) then
			mapvar.weapon[mapvar.active_char ] = "flamer"
			mapvar.weapon[0] = "flamer"
			return "flamer"
		end
		if BonusCombo("circle", "square", bt[1], bt[2]) then
			mapvar.weapon[mapvar.active_char ] = "grenade"
			mapvar.weapon[0] = "grenade"
			return "grenade"
		end
		if BonusCombo("circle", "diamond", bt[1], bt[2]) then
			mapvar.weapon[mapvar.active_char ] = "overcharged"
			mapvar.weapon[0] = "overcharged"
			return "overcharged"
		end
		if BonusCombo("circle", "line", bt[1], bt[2]) then
			mapvar.weapon[mapvar.active_char ] = "bouncy"
			mapvar.weapon[0] = "bouncy"
			return "bouncy"
		end

		--square (not enix)
		if BonusCombo("square", "triangle", bt[1], bt[2]) then
			mapvar.weapon[mapvar.active_char ] = "rocketlauncher"
			mapvar.weapon[0] = "rocketlauncher"
			return "rocketlauncher"
		end
		if BonusCombo("square", "diamond", bt[1], bt[2]) then
			mapvar.weapon[mapvar.active_char ] = "fragmentation"
			mapvar.weapon[0] = "fragmentation"
			return "fragmentation"
		end
		if BonusCombo("square", "line", bt[1], bt[2]) then
			mapvar.weapon[mapvar.active_char ] = "twinshot"
			mapvar.weapon[0] = "twinshot"
			return "twinshot"
		end

		--triangle
		if BonusCombo("triangle", "line", bt[1], bt[2]) then
			mapvar.weapon[mapvar.active_char ] = "wave"
			mapvar.weapon[0] = "wave"
			return "wave_weapon"
		end
		if BonusCombo("triangle", "diamond", bt[1], bt[2]) then
			mapvar.weapon[mapvar.active_char ] = "spread"
			mapvar.weapon[0] = "spread"
			return "spread"
		end

		--line
		if BonusCombo("line", "diamond", bt[1], bt[2]) then
			mapvar.weapon[mapvar.active_char ] = "mikuru_beam"
			mapvar.weapon[0] = "mikuru_beam"
			return "mikuru_beam"
		end

		--check out mah doubles
		if BonusCombo("circle", "circle", bt[1], bt[2]) then
			mapvar.weapon[mapvar.active_char ] = "bonus_sync"
			mapvar.weapon[0] = "bonus_sync"
			mapvar.sync = 0
			mapvar.bonuses[mapvar.active_char ] = {}
			Resume(NewThread(RemoveDoubleBonus), mapvar.active_char , "bonus_sync")
		end
		if BonusCombo("square", "square", bt[1], bt[2]) then
			mapvar.weapon[mapvar.active_char ] = "bonus_ammo"
			mapvar.weapon[0] = "bonus_ammo"
			mapvar.bonuses[mapvar.active_char ] = {}
			mapvar.bonus_thread = NewThread(RemoveDoubleBonus)
			Resume(NewThread(RemoveDoubleBonus), mapvar.active_char , "bonus_ammo")
		end
		if BonusCombo("triangle", "triangle", bt[1], bt[2]) then
			mapvar.weapon[mapvar.active_char ] = "bonus_speed"
			mapvar.weapon[0] = "bonus_speed"
			AddBonusSpeed(char1.id, 3)
			AddBonusSpeed(char2.id, 3)
			mapvar.bonuses[mapvar.active_char ] = {}
			mapvar.bonus_thread = NewThread(RemoveDoubleBonus)
			Resume(NewThread(RemoveDoubleBonus), mapvar.active_char , "bonus_speed", char1.id, char2.id)
		end
		if BonusCombo("diamond", "diamond", bt[1], bt[2]) then
			mapvar.weapon[mapvar.active_char ] = "bonus_invul"
			mapvar.weapon[0] = "bonus_invul"
			SetObjInvincible(id, true)
			mapvar.bonuses[mapvar.active_char ] = {}
			mapvar.bonus_thread = NewThread(RemoveDoubleBonus)
			Resume(NewThread(RemoveDoubleBonus), mapvar.active_char , "bonus_invul")
		end
		if BonusCombo("line", "line", bt[1], bt[2]) then
			mapvar.weapon[mapvar.active_char ] = "bonus_health"
			mapvar.weapon[0] = "bonus_health"
			mapvar.bonuses[mapvar.active_char ] = {}
			mapvar.bonus_thread = NewThread(RemoveDoubleBonus)
			Resume(NewThread(RemoveDoubleBonus), mapvar.active_char , "bonus_health")
		end

	end
	return nil
end

SetOnChangePlayerProcessor(Game.CharacterSystem);

function Game.InitCustomWeapon()
	mapvar.weapon = 
	{ 
		reload_time = 200, 
		clip_reload_time = 800; 
		shots_per_clip = 6; 
		ammo_per_shot = 1; 
		color = { 0.2, 1, 0.2, 1 };
		bullet_param = 
		{
			
			{
				type = "energy";
				look = "custom/energy1";
				color = { 0.2, 1, 0.2, 1 };
				damage = 20;
				vel = 8;
			},
			{
				type = "energy";
				look = "custom/energy1";
				color = { 0.3, 0.8, 0.3, 1 };
				damage = 10;
				vel = 7;
			},
			{
				type = "energy";
				look = "custom/energy1";
				color = { 0.8, 0.2, 0.2, 1 };
				damage = 0;
				vel = 2;
				explosion = "explosion";
			},
			{
				type = "ray";
				look = "custom/ray1";
				color = { 1, 1, 1, 1 };
				max_targets = 1;
				damage = 10;
			}
		},
		bullets =
		{
			{
				num = 1,
				pos = {0, 0},
				ang = 0
			},
			{
				num = 2,
				pos = {0, -15},
				ang = -5
			},
			{
				num = 2,
				pos = {0, 15},
				ang = 5
			},
			{
				num = 3,
				pos = {-15, 0},
				ang = 0
			},
			{
				num = 4,
				pos = {0, 0},
				ang = 0
			}
		},
		explosions = {}
	}
end

function UpdateCustomWeapon( id )
	if not mapvar.weapon then
		Game.InitCustomWeapon()
	end
	SetCustomWeapon( id, mapvar.weapon )
end

function flare_processor( id )
	if not mapvar.tmp.obj or not mapvar.tmp.obj[id] then
		return
	end
	local state = mapvar.tmp.obj[id]
	local obj = nil
	if state.obj and state.obj > 0 then
		obj = GetObject( state.obj )
	end
	if obj then
		SetObjPos( id, obj.aabb.p.x, obj.aabb.p.y )
	else
		state.size_x = state.size_x * 0.9
		state.size_y = state.size_y * 0.9
		if state.size_x < 2 or state.size_y < 2 then
			SetObjDead( id )
			return
		end
		SetObjRectangle( id, state.size_x, state.size_y )
	end
end
	
function custom_weapon( id, shooter, param, dir, x, y )
	UpdateCustomWeapon( shooter )
	if param == 0 then
		SetObjSpriteColor(id, mapvar.weapon.color)
	elseif param == 1 then
		local pl = GetObject(shooter)
		local b
		local w = mapvar.weapon
		local sx, sy
		local rad
		local flare
		y = y - 12
		for k, v in pairs( mapvar.weapon.bullets ) do
			rad = (-dir / 180) * math.pi
			sx =  v.pos[1]*math.cos(rad) + v.pos[2]*math.sin(rad)
			sy = -v.pos[1]*math.sin(rad) + v.pos[2]*math.cos(rad)
			if pl.sprite.mirrored then
				sx = -sx
				sy = -sy
			end
			local ang = v.ang
			if not tonumber(ang) then 
				local lo, hi = string.match(ang, "rnd%(%s*([%-%d%.]+)%s*,%s*([%-%d%.]+)%s*%)" )
				lo, hi = tonumber(lo), tonumber(hi)
				ang = math.random(  lo, hi )
			end
			if w.bullet_param[v.num].type == "energy" then
				b = CreateBullet( w.bullet_param[v.num].look, x + sx, y + sy, shooter, pl.sprite.mirrored, dir + ang * math.pi, 0 )
				SetObjSpriteColor( b,  w.bullet_param[v.num].color )		
				SetCustomBullet(b, w.bullet_param[v.num])
				if w.bullet_param[v.num].flare then
					if not mapvar.tmp.obj then mapvar.tmp.obj = {} end
					local rad_ang = (dir + ang * math.pi) * math.pi / 180
					--Bullet flare
					flare = CreateSprite( w.bullet_param[v.num].flare, x, y )
					SetObjRectangle( flare, w.bullet_param[v.num].flare_size_x, w.bullet_param[v.num].flare_size_y )
					SetObjSpriteRenderMethod( flare, constants.rsmStretch )
					SetObjSpriteColor( flare, w.bullet_param[v.num].flare_color )
					SetObjSpriteAngle( flare, rad_ang )
					SetObjProcessor( flare, flare_processor )
					mapvar.tmp.obj[flare] = { obj = b, size_x = w.bullet_param[v.num].flare_size_x, size_y = w.bullet_param[v.num].flare_size_y }
					--Muzzle flash flare
					flare = CreateSprite( w.bullet_param[v.num].flare, x, y )
					SetObjPos( flare, x, y )
					SetObjRectangle( flare, 2*w.bullet_param[v.num].flare_size_x, 2*w.bullet_param[v.num].flare_size_y )
					SetObjSpriteRenderMethod( flare, constants.rsmStretch )
					SetObjSpriteColor( flare, w.bullet_param[v.num].flare_color )
					SetObjSpriteAngle( flare, rad_ang )
					SetObjProcessor( flare, flare_processor )
					mapvar.tmp.obj[flare] = { obj = -1, size_x = 2*w.bullet_param[v.num].flare_size_x, size_y = 2*w.bullet_param[v.num].flare_size_y }
				end
			elseif w.bullet_param[v.num].type == "ray" then
				rad = dir + ang * math.pi
				if not pl.sprite.mirrored then
					rad = -rad
				else
					rad = -rad + 180
				end
				b = CreateRay( w.bullet_param[v.num].look, shooter, rad, x + sx, y + sy, w.bullet_param[v.num].damage, w.bullet_param[v.num].max_targets )
				SetObjSpriteColor( b,  w.bullet_param[v.num].color )
			end
			if w.bullet_param[v.num].explosion then
				w.explosions[b] = w.bullet_param[v.num].explosion
			end
		end
	elseif param == 2 then
		if mapvar.weapon.explosions[id] then
			SetObjPos( CreateBullet( mapvar.weapon.explosions[id], x, y, shooter, false, 0, 0 ), x, y )
			mapvar.weapon.explosions[id] = nil
		end
	end
end

function Game.InitWeapons( pl )
	mapvar.player_info = mapvar.player_info or {}
	if stat.player_info then
		mapvar.player_info = stat.player_info
		return
	end
	mapvar.player_info[pl] = {}
	mapvar.player_info[pl].weapons = {}
	mapvar.player_info[pl].active_weapon = 1
	
	--[[
	UpdateWeaponSlot( 1, 2 , 
		{ set_name = "Custom Weapon 1", 
		reload_time = 200, clip_reload_time = 800, shots_per_clip = 6, ammo_per_shot = 1,
		color = { 0.2, 1, 0.2, 1 };
		bullet_param = 
			{
				{
					type = "energy";
					look = "custom/energy1";
					color = { 0.2, 1, 0.2, 1 };
					damage = 20;
					vel = 8;
				}
			},
		bullets =
		{
			{
				num = 1,
				pos = {0, 0},
				ang = 0
			}
		},
		explosions = {}
	})
	
	UpdateWeaponSlot( 1, 3 , 
		{ set_name = "Another Custom Weapon",
		reload_time = 200, clip_reload_time = 800, shots_per_clip = 6, ammo_per_shot = 1,
		color = { 0.2, 1, 0.2, 1 };
		bullet_param = 
			{
				{
					type = "energy";
					look = "custom/energy1";
					color = { 0.2, 1, 0.2, 1 };
					damage = 20;
					vel = 8;
				}
			},
		bullets =
		{
			{
				num = 1,
				pos = {0, 0},
				ang = -10
			},
			{
				num = 1,
				pos = {0, 0},
				ang = 10
			}
		},
		explosions = {}
	})--]]
end

function Game.game_key_pressed( key )
	if isConfigKeyPressed(key, "weapon_slot1") then
		mapvar.player_info[1].active_weapon = 1
		SwitchWeapon( nil, false )
	elseif isConfigKeyPressed(key, "weapon_slot2") then
		if mapvar.player_info[1].weapons[2] then
			mapvar.player_info[1].active_weapon = 2
			SetPlayerAltWeapon( nil, mapvar.player_info[1].weapons[2].name )
			mapvar.weapon = deep_copy( mapvar.player_info[1].weapons[2].param )
			SwitchWeapon( nil, true )
		end
	elseif isConfigKeyPressed(key, "weapon_slot3") then
		if mapvar.player_info[1].weapons[3] then
			mapvar.player_info[1].active_weapon = 3
			SetPlayerAltWeapon( nil, mapvar.player_info[1].weapons[3].name )
			mapvar.weapon = deep_copy( mapvar.player_info[1].weapons[3].param )
			SwitchWeapon( nil, true )
		end
	elseif isConfigKeyPressed(key, "weapon_slot4") then
		if mapvar.player_info[1].weapons[2] then
			mapvar.player_info[1].active_weapon = 4
			SetPlayerAltWeapon( nil, mapvar.player_info[4].weapons[2].name )
			mapvar.weapon = deep_copy( mapvar.player_info[4].weapons[2].param )
			SwitchWeapon( nil, true )
		end
	elseif NETGAME and key == keys["t"] and not mapvar.in_chat then
		mapvar.in_chat = true
		mapvar.subchat = CreateWidget(constants.wt_Label, "console_text", nil, 100, CONFIG.scr_height/2 - 20, 25500, CONFIG.scr_height/2 )
		WidgetSetCaptionColor( mapvar.subchat, { 1, 1, 0.5, 1}, false )
		WidgetSetCaptionFont( mapvar.subchat, "dialogue" )
		WidgetSetCaption( mapvar.subchat, "SAY" )
		WidgetSetZ(mapvar.subchat, 0.99999)
		mapvar.chat = CreateWidget(constants.wt_Textfield, "console_text", nil, 150, CONFIG.scr_height/2 - 20, 25500, CONFIG.scr_height/2 )
		WidgetSetCaptionColor( mapvar.chat, { 1, 1, 1, 1}, true )
		WidgetSetCaptionFont( mapvar.chat, "dialogue" )
		WidgetSetZ(mapvar.chat, 0.99999)

		WidgetGainFocus( 0 )
		WidgetGainFocus( mapvar.chat )
		WidgetGainFocus( mapvar.chat )
	elseif NETGAME and key == keys["enter"] and mapvar.in_chat then
		ChatMessage( WidgetGetCaption( mapvar.chat ) )
		DestroyWidget( mapvar.chat )
		DestroyWidget( mapvar.subchat )
		mapvar.in_chat = false
	else
		return false
	end
	return false
end

function Game.game_key_released( key )
	if Editor then
		return false
	end
	if isConfigKeyPressed(key, "change_weapon") then
		repeat 
			mapvar.player_info[1].active_weapon = mapvar.player_info[1].active_weapon + 1
		until mapvar.player_info[1].weapons[mapvar.player_info[1].active_weapon] or mapvar.player_info[1].active_weapon > 4
		if mapvar.player_info[1].active_weapon > 4 then
			mapvar.player_info[1].active_weapon = 1
			SwitchWeapon( nil, false )
		else
			SetPlayerAltWeapon( nil, mapvar.player_info[1].weapons[mapvar.player_info[1].active_weapon].name )
			mapvar.weapon = deep_copy( mapvar.player_info[1].weapons[mapvar.player_info[1].active_weapon].param )
			SwitchWeapon( nil, true )
		end
	else
		return false
	end
	return false
end

function UpdateWeaponSlot( pl, num, weapon )
	mapvar.player_info[pl].weapons[num] = {}
	mapvar.player_info[pl].weapons[num].name = "custom"
	mapvar.player_info[pl].weapons[num].param = weapon
end

if not Editor then
	_GlobalSetKeyDownProc = GlobalSetKeyDownProc
	GlobalSetKeyDownProc = function( func )
		_GlobalSetKeyDownProc( function( key )
			if not Game.game_key_pressed( key ) then
				if func then
					return func( key )
				else
					return false
				end
			else
				return true
			end
		end )
	end
	_GlobalSetKeyReleaseProc = GlobalSetKeyReleaseProc
	GlobalSetKeyReleaseProc = function( func )
		_GlobalSetKeyReleaseProc( function( key )
			if not Game.game_key_released( key ) then
				if func then
					return func( key )
				else
					return false
				end
			else
				return true
			end
		end )
	end
end

function Game.GetNextCharacter()
	local spawn = { 0, 0 }
	if Loader.level and Loader.level.spawnpoints then
		spawn = random( Loader.level.spawnpoints )
	end
	return { "super-sohchan", spawn }
end

function Game.CreatePlayerTable()
	return {
		score = 0,
		lives = 0
	}
end

