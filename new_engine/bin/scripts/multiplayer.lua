function OnChatMessage( nick, message )
	if not chat_widget then
		chat_message = ""
		chat_message_timeouts = {}
		chat_widget = CreateWidget( constants.wt_Label, "chat", nil, 0, 150, 640, 480 )
		WidgetSetFocusable( chat_widget, false )
		WidgetSetCaptionColor( chat_widget, {1, 1, 1, 1}, false )
		WidgetSetCaptionFont( chat_widget, "dialogue" )
		Resume( NewThread( function()
			while true do
				if chat_message_timeouts[1] then
					for k, v in pairs( chat_message_timeouts ) do
						chat_message_timeouts[k] = v - 1
					end
					if ( chat_message_timeouts[1] <= 0 ) then
						chat_message = string.gsub( chat_message, "^.-/n", "" )
						WidgetSetCaption( chat_widget, chat_message, true )
						table.remove( chat_message_timeouts, 1 )
					end
				end
				Wait( 1 )
			end
		end ))
	end
	chat_message = chat_message .. string.format( "/cffffff</cffff55%s/cffffff>: %s/n", nick, message )
	table.insert( chat_message_timeouts, 500 )
	WidgetSetCaption( chat_widget, chat_message, true )
end

function h()
	NetHost( 1111 )
end

function c()
	NetConnect( "127.0.0.1", 1111 )
end
