local t = {}

function t.BuildTrackPart( track, part, context )
	local ud
	local x = context.x
	local y = 0
	local TheVegetable = context.TheVegetable
	local cx = x
	local w
	local anim
	if part == "=" then

		table.insert( track.tiles, {x, "="} )

		w = t.parts[1][1]
		anim = t.parts[1][2]

		for i=1,3 do
			ud = GetObjectUserdata( CreateSprite( t.sprite, cx, y ) )
			SetObjAnim( ud, anim, false )
			SetObjPos( ud, cx+w/2, 240 )
			cx = cx + w
		end

		return 3 * w

	elseif part == "1" or part == "!" then

		table.insert( track.tiles, {x, "="} )

		w = t.parts[1][1]
		anim = t.parts[1][2]

		for i=1,3 do
			if i == 2 then
				ud = GetObjectUserdata( CreateItem( "vegetable_hb1", cx, y+350+16 ) )
				SetObjPos( ud, cx+w/2, y+350+16 )
				TheVegetable( ud, iff( part == "!", -96, -32 ) )
			end
			ud = GetObjectUserdata( CreateSprite( t.sprite, cx, y ) )
			SetObjAnim( ud, anim, false )
			SetObjPos( ud, cx+w/2, 240 )
			cx = cx + w
		end

		return 3 * w
		
	elseif part == "2"  or part == "@" then

		table.insert( track.tiles, {x, "="} )

		w = t.parts[1][1]
		anim = t.parts[1][2]

		for i=1,3 do
			if i == 2 then
				ud = GetObjectUserdata( CreateItem( "vegetable_hb2", cx, y+350+32 ) )
				SetObjPos( ud, cx+w/2, y+350+32 )
				TheVegetable( ud, iff( part == "@", -96, -32 ) )
			end
			ud = GetObjectUserdata( CreateSprite( t.sprite, cx, y ) )
			SetObjAnim( ud, anim, false )
			SetObjPos( ud, cx+w/2, 240 )
			cx = cx + w
		end

		return 3 * w


	elseif part == "3" or part == "#" then

		table.insert( track.tiles, {x, "="} )

		w = t.parts[1][1]
		anim = t.parts[1][2]

		for i=1,3 do
			if i == 2 then
				ud = GetObjectUserdata( CreateItem( "vegetable_hb3", cx, y+350+48 ) )
				SetObjPos( ud, cx+w/2, y+350+48 )
				TheVegetable( ud, iff( part == "#", -96, -32 ) )
			end
			ud = GetObjectUserdata( CreateSprite( t.sprite, cx, y ) )
			SetObjAnim( ud, anim, false )
			SetObjPos( ud, cx+w/2, 240 )
			cx = cx + w
		end

		return 3 * w


	elseif part == "[" then
		
		table.insert( track.tiles, {x, "["} )

		w = t.parts[2][1]
		anim = t.parts[2][2]

		ud = GetObjectUserdata( CreateSprite( t.sprite, cx, y ) )
		SetObjAnim( ud, anim, false )
		SetObjPos( ud, cx+w/2, 240 )
		cx = cx + w

		return w

	elseif part == " " then

		table.insert( track.tiles, {x, " "} )

		w = 64

		return 3 * w

	elseif part == "]" then

		table.insert( track.tiles, {x, "]"} )

		w = t.parts[4][1]
		anim = t.parts[4][2]

		ud = GetObjectUserdata( CreateSprite( t.sprite, cx, y ) )
		SetObjAnim( ud, anim, false )
		SetObjPos( ud, cx+w/2, 240 )
		cx = cx + w

		return w
	
	elseif part == "~" then
		if not track.finish_line then
			track.finish_line = x
		end
		table.insert( track.tiles, {x, "~"} )

		w = t.parts[1][1]
		anim = t.parts[1][2]

		for i=1,3 do
			ud = GetObjectUserdata( CreateSprite( t.sprite, cx, y ) )
			SetObjAnim( ud, anim, false )
			SetObjPos( ud, cx+w/2, 240 )
			cx = cx + w
		end

		return 3 * w

	end
	return 0
end

function t.GetFloorZ( x, y, context )
	local tile, start = context.GetTileType( x )
	if tile == "=" or tile == "~" then
		return 0
	elseif tile == " " then
		return 1024
	elseif tile == "[" then
		return iff( x - start - 128 > (y - t.top) / 2, 1024, 0 )
	elseif tile == "]" then
		return iff( x - start > (y - t.top) / 2, 0, 1024 )
	end
end

t.top = 320
t.bottom = 456

t.parts =
{
	{ 64, "floor" },
	{ 128, "pit_left" },
	{},
	{ 6*32, "pit_right" }
}

t.sprite = "hb_pear15_mountain";

t.InitTrack = function( context )

	CreateMap( {
		{constants.ObjRibbon, "forest-back", 1.000000, 1.000000, 0, 0, -1.100000, true, nil, nil, nil, nil, false, false},
		{constants.ObjRibbon, "forest-back1", 0.500000, 1.000000, 0, 256, -1.099999, true, nil, nil, nil, nil, true, false},
	}, 4 )
	
end

return t
