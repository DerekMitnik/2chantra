local GUIc = {}
GUIc.__index = GUIc

local Bar = {}
Bar.__index = Bar

function Bar.create( widget, x, y, size_x, size_y, color, max, gui )
	local b = {}
	setmetatable( b, Bar )
	b.widget = widget
	b.size_x = size_x
	b.size_y = size_y
	b.color = color
	b.gui = gui
	b.value = 1
	b.target = 1
	b.max = max
	b.precision = 1 / size_x
	b.x = x
	b.y = y
	b.text_widget = CreateWidget( constants.wt_Label, "bar text", widget, x, y, 1, 1 )
	b.text = "1"
	b.redness = 0
	WidgetSetFocusable( b.text_widget, false )
	WidgetSetCaptionFont( b.text_widget, "dialogue" )
	WidgetSetCaptionColor( b.text_widget, { 1, 1, 1, 1 }, false, { 0, 0, 0, 1 } )
	return b
end

function Bar:updateValue( value, instant )
	self.target = math.max( 0, math.min( 1, value/self.max ) )
	self.text = tostring( value )
	if instant then
		self.value = self.target
	end
end

function Bar:update()
	if math.abs( self.value - self.target ) <= self.precision then
		self.value = self.target
	else
		self.value = self.value + ( self.target - self.value ) * 0.1
	end
	local color = { self.color[1] + self.gui.tt * 0.2, self.color[2] + self.gui.tt * 0.2, self.color[3] + self.gui.tt * 0.2, self.color[4] }
	if self.redness > 0 then
		local redness = self.redness
		color[1] = color[1] * (1 - redness) + redness
		color[2] = color[2] * (1 - redness)
		color[3] = color[3] * (1 - redness)
		WidgetSetCaptionColor( self.text_widget, { 1, 1-redness, 1-redness, 1 }, false, { 0, 0, 0, 1 } )
		self.redness = self.redness - self.gui.dt / 1000
	else
		WidgetSetCaptionColor( self.text_widget, { 1, 1, 1, 1 }, false, { 0, 0, 0, 1 } )
		self.redness = 0
	end
	WidgetSetSize( self.widget, self.size_x * self.value, self.size_y )
	WidgetSetSpriteColor( self.widget, color )
	local sx, sy = GetCaptionSize( "dialogue", self.text )
	WidgetSetPos( self.text_widget, self.x + (self.size_x - sx)/2, self.y + (self.size_y - sy)/2 )
	WidgetSetCaption( self.text_widget, self.text )
end

function Bar:destroy()
end

function Bar:flashRed()
	self.redness = 1
end

function GUIc:createPlayerWidgets( player, x, y, reversed )
	local bars_background = "player"..player.."_bars_background"
	self.widgets[bars_background] = CreateWidget( constants.wt_Picture, "GUI", nil, x+0, y+0, 129, 44 )
	WidgetSetSprite( self.widgets[bars_background], "pear15gui", "bars_back" )
	WidgetSetFocusable( self.widgets[bars_background], false )
	local bar1 = "player"..player.."_bar1"
	self.widgets[bar1] = CreateWidget( constants.wt_Picture, "GUI", self.widgets[bars_background], x+20, y+12, 101, 11 )
	WidgetSetSprite( self.widgets[bar1], "pear15gui", "bar1" )
	WidgetSetFocusable( self.widgets[bar1], false )
	WidgetSetSpriteRenderMethod( self.widgets[bar1], constants.rsmCrop )
	local bar2 = "player"..player.."_bar2"
	self.widgets[bar2] = CreateWidget( constants.wt_Picture, "GUI", self.widgets[bars_background], x+18, y+28, 101, 11 )
	WidgetSetSprite( self.widgets[bar2], "pear15gui", "bar2" )
	WidgetSetFocusable( self.widgets[bar2], false )
	WidgetSetSpriteRenderMethod( self.widgets[bar2], constants.rsmCrop )

	local weapon1_1 = "weapon"..player.."_1"
	self.widgets[weapon1_1] = CreateWidget( constants.wt_Picture, "GUI", self.widgets[bars_background], x+133, y+7, 1, 1 )
	WidgetSetSprite( self.widgets[weapon1_1], "pear15gui", "weapon1" )
	WidgetSetSpriteColor( self.widgets[weapon1_1], { 1, 1, 1, 1 } )
	WidgetSetFocusable( self.widgets[weapon1_1], false )
	local weapon1_2 = "weapon"..player.."_2"
	self.widgets[weapon1_2] = CreateWidget( constants.wt_Picture, "GUI", self.widgets[bars_background], x+133+14, y+7, 1, 1 )
	WidgetSetSprite( self.widgets[weapon1_2], "pear15gui", "weapon2" )
	WidgetSetSpriteColor( self.widgets[weapon1_2], { 1, 1, 1, 1 } )
	WidgetSetFocusable( self.widgets[weapon1_2], false )
	local weapon1_3 = "weapon"..player.."_3"
	self.widgets[weapon1_3] = CreateWidget( constants.wt_Picture, "GUI", self.widgets[bars_background], x+133+28, y+7, 1, 1 )
	WidgetSetSprite( self.widgets[weapon1_3], "pear15gui", "weapon3" )
	WidgetSetSpriteColor( self.widgets[weapon1_3], { 1, 1, 1, 1 } )
	WidgetSetFocusable( self.widgets[weapon1_3], false )
	local weapon1_4 = "weapon"..player.."_4"
	self.widgets[weapon1_4] = CreateWidget( constants.wt_Picture, "GUI", self.widgets[bars_background], x+133+3*14, y+7, 1, 1 )
	WidgetSetSprite( self.widgets[weapon1_4], "pear15gui", "weapon4" )
	WidgetSetSpriteColor( self.widgets[weapon1_4], { 1, 1, 1, 1 } )
	WidgetSetFocusable( self.widgets[weapon1_4], false )
	local weapon1_icon = "weapon"..player.."_icon"
	self.widgets[weapon1_icon] = CreateWidget( constants.wt_Picture, "GUI", self.widgets[bars_background], x+145, y+17, 1, 1 )
	WidgetSetSprite( self.widgets[weapon1_icon], "pear15gui", "weapon_icon_soh" )
	WidgetSetFocusable( self.widgets[weapon1_icon], false )
	WidgetSetSpriteColor( self.widgets[weapon1_icon], {182/255, 181/255, 194/255, 1} )
	g:changeWeapon( player, 1 )

	local score1_label = "score"..player.."_label"
	self.widgets[score1_label] = CreateWidget( constants.wt_Label, "GUI", self.widgets[bars_background], x+5, y+50, 10, 10 )
	WidgetSetFocusable( self.widgets[score1_label], false )
	WidgetSetCaptionFont( self.widgets[score1_label], "dialogue" )
	WidgetSetCaptionColor( self.widgets[score1_label], { 1, 1, 1, 1 }, false, { 0, 0, 0, 1 } )
	WidgetSetCaption( self.widgets[score1_label], "SCORE" )
	local score1 = "score"..player
	local sx, sy = GetCaptionSize( "dialogue", "0" )
	self.widgets[score1] = CreateWidget( constants.wt_Label, "GUI", self.widgets[bars_background], x+180-sx, y+50, 10, 10 )
	WidgetSetFocusable( self.widgets[score1], false )
	WidgetSetCaptionFont( self.widgets[score1], "dialogue" )
	WidgetSetCaptionColor( self.widgets[score1], { 1, 1, 1, 1 }, false, { 0, 0, 0, 1 } )
	WidgetSetCaption( self.widgets[score1], "0" )

	local lives_label = "lives"..player.."_label"
	self.widgets[lives_label] = CreateWidget( constants.wt_Label, "GUI", self.widgets[bars_background], x+5, y+65, 10, 10 )
	WidgetSetFocusable( self.widgets[lives_label], false )
	WidgetSetCaptionFont( self.widgets[lives_label], "dialogue" )
	WidgetSetCaptionColor( self.widgets[lives_label], { 1, 1, 1, 1 }, false, { 0, 0, 0, 1 } )
	WidgetSetCaption( self.widgets[lives_label], "LIVES" )
	local life = "player"..player.."_life1"
	self.widgets[life] = CreateWidget( constants.wt_Picture, "GUI", self.widgets[bars_background], x+170, y+67, 10, 10 )
	WidgetSetSprite( self.widgets[life], "pear15gui", "heart" )
	WidgetSetVisible( self.widgets[life], false )
	WidgetSetSpriteColor( self.widgets[life], {182/255, 181/255, 194/255, 1} )
	life = "player"..player.."_life2"
	self.widgets[life] = CreateWidget( constants.wt_Picture, "GUI", self.widgets[bars_background], x+160, y+67, 10, 10 )
	WidgetSetSprite( self.widgets[life], "pear15gui", "heart" )
	WidgetSetVisible( self.widgets[life], false )
	WidgetSetSpriteColor( self.widgets[life], {182/255, 181/255, 194/255, 1} )
	life = "player"..player.."_life3"
	self.widgets[life] = CreateWidget( constants.wt_Picture, "GUI", self.widgets[bars_background], x+150, y+67, 10, 10 )
	WidgetSetSprite( self.widgets[life], "pear15gui", "heart" )
	WidgetSetVisible( self.widgets[life], false )
	WidgetSetSpriteColor( self.widgets[life], {182/255, 181/255, 194/255, 1} )
	life = "player"..player.."_life4"
	self.widgets[life] = CreateWidget( constants.wt_Picture, "GUI", self.widgets[bars_background], x+140, y+67, 10, 10 )
	WidgetSetSprite( self.widgets[life], "pear15gui", "heart" )
	WidgetSetVisible( self.widgets[life], false )
	WidgetSetSpriteColor( self.widgets[life], {182/255, 181/255, 194/255, 1} )
	life = "player"..player.."_life5"
	self.widgets[life] = CreateWidget( constants.wt_Picture, "GUI", self.widgets[bars_background], x+130, y+67, 10, 10 )
	WidgetSetSprite( self.widgets[life], "pear15gui", "heart" )
	WidgetSetVisible( self.widgets[life], false )
	WidgetSetSpriteColor( self.widgets[life], {182/255, 181/255, 194/255, 1} )
	
	local player_replacement = "player"..player.."_replacement"
	sx, sy = GetCaptionSize( "dialogue", "Press "..GetKeyName( CONFIG.key_conf[player].gui_nav_accept ).." to join." )
	self.widgets[player_replacement] = CreateWidget( constants.wt_Label, "GUI", nil, x+90-sx/2, y+5, 180, 10 )
	WidgetSetFocusable( self.widgets[player_replacement], false )
	WidgetSetCaptionColor( self.widgets[player_replacement], { 1, 1, 1, 1 }, false, { 0, 0, 0, 1 } )
	WidgetSetCaptionFont( self.widgets[player_replacement], "dialogue" )
	self:updatePlayerReplacement(player_replacement)
	WidgetSetVisible( self.widgets[player_replacement], false )
	
	local widget = CreateWidget( constants.wt_Picture, "GUI", self.widgets[bars_background], 0, 0, 10, 10 )
	WidgetSetSprite( widget, "pear15gui", "weapon_charge_background" )
	WidgetSetFixedPosition( widget, false )
	WidgetSetFocusable( widget, false )
	WidgetSetVisible( widget, false )
	self.widgets["player"..player.."_weapon_charge_background"] = widget
	widget = CreateWidget( constants.wt_Picture, "GUI", self.widgets["player"..player.."_weapon_charge_background"], 2, 2, 10, 10 )
	WidgetSetSprite( widget, "pear15gui", "weapon_charge_bar" )
	WidgetSetFixedPosition( widget, false )
	WidgetSetFocusable( widget, false )
	WidgetSetSpriteRenderMethod( widget, constants.rsmStretch )
	self.widgets["player"..player.."_weapon_charge_bar"] = widget

	if player == 1 then
		self.bars[1] = Bar.create( g.widgets[bar1], x+20, y+12, 101, 11, {44/255, 162/255, 78/255, 1}, 120, g )
		self.bars[2] = Bar.create( g.widgets[bar2], x+18, y+28, 101, 11, {212/255, 207/255, 58/255, 1}, 300, g )
	elseif player == 2 then
		self.bars[3] = Bar.create( g.widgets[bar1], x+20, y+12, 101, 11, {44/255, 162/255, 78/255, 1}, 120, g )
		self.bars[4] = Bar.create( g.widgets[bar2], x+18, y+28, 101, 11, {212/255, 207/255, 58/255, 1}, 300, g )
	end
end

function GUIc.create()
	g = {}
	setmetatable(g, GUIc)
	g.widgets = {}
	g.bars = {}

	g:createPlayerWidgets( 1, 0, 0 )
	g:createPlayerWidgets( 2, 450, 0 )

	g.widgets.combo = CreateWidget( constants.wt_Picture, "GUI", g.widgets.bars_background, 207, 9, 110, 56 )
	WidgetSetSprite( g.widgets.combo, "pear15gui", "combo" )
	WidgetSetSpriteRenderMethod( g.widgets.combo, constants.rsmStretch )
	WidgetSetFocusable( g.widgets.combo, false )
	WidgetSetVisible( g.widgets.combo, false )
	g.widgets.combox = CreateWidget( constants.wt_Picture, "GUI", g.widgets.bars_background, 207+120, 9+28-6, 1, 1 )
	WidgetSetSprite( g.widgets.combox, "pear15gui", "x" )
	WidgetSetFocusable( g.widgets.combox, false )
	WidgetSetVisible( g.widgets.combox, false )
	g.widgets.combo1 = CreateWidget( constants.wt_Picture, "GUI", g.widgets.bars_background, 207+120+20, 19, 1, 1 )
	WidgetSetSprite( g.widgets.combo1, "pear15gui", "0" )
	WidgetSetFocusable( g.widgets.combo1, false )
	WidgetSetVisible( g.widgets.combo1, false )
	g.widgets.combo2 = CreateWidget( constants.wt_Picture, "GUI", g.widgets.bars_background, 207+120+20+20, 19, 1, 1 )
	WidgetSetSprite( g.widgets.combo2, "pear15gui", "1" )
	WidgetSetFocusable( g.widgets.combo2, false )
	WidgetSetVisible( g.widgets.combo2, false )
	g.widgets.combo3 = CreateWidget( constants.wt_Picture, "GUI", g.widgets.bars_background, 207+120+20+40, 19, 1, 1 )
	WidgetSetSprite( g.widgets.combo3, "pear15gui", "+" )
	WidgetSetFocusable( g.widgets.combo3, false )
	WidgetSetVisible( g.widgets.combo3, false )
	g.widgets.info = CreateWidget( constants.wt_Picture, "GUI", g.widgets.player1_bars_backround, 207+120+20+40, 19, 1, 1 )
	WidgetSetSprite( g.widgets.info, "pear15gui", "info" )
	WidgetSetFocusable( g.widgets.info, false )
	WidgetSetVisible( g.widgets.info, false )
	WidgetSetSpriteRenderMethod( g.widgets.info, constants.rsmStretch )
	
	g.t = 0
	g.combosize = 1
	g.time = GetCurTime()
	WidgetSetVisible( g.widgets.player1_bars_background, false )
	WidgetSetVisible( g.widgets.player2_bars_background, false )
	g.thread = NewThread(GUIc.update)
	g.visible = false
	return g
end

function GUIc:showPlayer( player )
	if not Game.actors[ player ] then
		WidgetSetVisible( self.widgets["player"..player.."_bars_background"], false )
		if Game.info.can_join then
			WidgetSetVisible( self.widgets["player"..player.."_replacement"], true )
		else
			WidgetSetVisible( self.widgets["player"..player.."_replacement"], false )
		end
	else
		-- TODO if player is dead then...
		self["character"..player] = Game.GetPlayerCharacter( player )
		WidgetSetVisible( self.widgets["player"..player.."_bars_background"], true )
		WidgetSetVisible( self.widgets["player"..player.."_replacement"], false )
		local ch = self["character"..player]
		if ch then
			if ch:weapon_ready() and (ch:alt_weapon_ready() or ch:ammo() == 0) then
				WidgetSetSpriteColor( self.widgets["weapon"..player.."_icon"], {182/255, 181/255, 194/255, 1} )
			else
				WidgetSetSpriteColor( self.widgets["weapon"..player.."_icon"], {182/255, 81/255, 94/255, 1} )
			end
		end
		if Game.mapvar.actors[ player ].info and Game.mapvar.actors[ player ].info.lives then
			for i = 1, 5 do
				WidgetSetVisible( self.widgets["player"..player.."_life"..i], i <= Game.mapvar.actors[ player ].info.lives )
			end
		end
		--[[if self["player"..player.."_charging"] then
			local char = self["character"..player]
			if not self["player"..player.."_charge_effect"]:object_present() then
				self["player"..player.."_charging"] = false
				WidgetSetVisible( self.widgets["player"..player.."_weapon_charge_background"], false )
			end
			else
				local pos = char:aabb()
				WidgetSetPos( self.widgets["player"..player.."_weapon_charge_background"], pos.p.x - 32, pos.p.y - pos.H - 18 )
				local charge = math.min(1, char:weapon_charge_time()/self["player"..player.."_max_charge"])
				WidgetSetSpriteColor( self.widgets["player"..player.."_weapon_charge_bar"], { 0.6*charge, charge, 1-charge, 1 } )
				WidgetSetSize( self.widgets["player"..player.."_weapon_charge_bar"], 60*charge, 8 )
			end
		end]]
	end
end

function GUIc:hidePlayer( player )
	WidgetSetVisible( self.widgets["player"..player.."_bars_background"], false )
	WidgetSetVisible( self.widgets["player"..player.."_replacement"], false )
end

function GUIc:show()
	for i=1,2 do
		self:showPlayer( i )
	end
	if self.character1 and self.character1:object_present() then
		self.bars[1].max = self.character1:health_max()/2
		self.bars[1]:updateValue( self.character1:health(), true)
		self.bars[2]:updateValue( self.character1:ammo(), true)
	end
	if self.character2 and self.character2:object_present() then
		self.bars[3].max = self.character2:health_max()/2
		self.bars[3]:updateValue( self.character2:health(), true)
		self.bars[4]:updateValue( self.character2:ammo(), true)
	end
	self:init()
	self.visible = true
	self.thread = NewThread( self.update )
	Resume( self.thread, self )
end

function GUIc:hide()
	for i=1,2 do
		self:hidePlayer( i )
	end
	WidgetSetVisible( self.widgets.info, false )
	self.visible = false
end

function GUIc:hide_push()
	if not self.widgets then
		return
	end
	if not self.visible then
		self.hidden_level = (self.hidden_level or 0) + 1
	end
	self:hide()
end

function GUIc:show_pop()
	if not self.widgets then
		return
	end
	if self.hidden_level and self.hidden_level >= 0 then
		self.hidden_level = self.hidden_level - 1
	end
	if (self.hidden_level or -1) < 0 then
		self:show()
	end
end

function GUIc:destroy()
	DestroyWidget( self.widgets.player1_bars_background )
	DestroyWidget( self.widgets.player2_bars_background )
	DestroyWidget( self.widgets.player1_replacement )
	DestroyWidget( self.widgets.player2_replacement )
	for k, v in pairs( self.bars ) do
		v:destroy()
	end
	self.widgets = nil
	self.visible = false
	StopThread( self.thread )
	self.thread = nil
end

function GUIc:init( player )
	if not Loader.level then
		return
	end
	if not player or player == 1 then
		if not self.character1 or not self.character1:object_present() then
			self.character1 = Game.GetPlayerCharacter( 1 )
		end
		if self.character1 and self.character1:object_present() then
			self.bars[1]:updateValue( self.character1:health(), true)
			self.bars[2]:updateValue( self.character1:ammo(), true)
		end
		if Game.mapvar.actors[1] and Game.mapvar.actors[1].info and Game.mapvar.actors[1].info.score then
			self:updateScore( 1, Game.mapvar.actors[1].info.score )
		end
	end
	if Game.GetPlayersNum() > 1 then
		if not player or player == 2 then
			if not self.character2 or not self.character2:object_present() then
				self.character2 = Game.GetPlayerCharacter( 2 )
			end
			if self.character2 and self.character2:object_present() then
				self.bars[3]:updateValue( self.character2:health(), true)
				self.bars[4]:updateValue( self.character2:ammo(), true)
			end
		end
		self:updateScore( 2, Game.mapvar.actors[2].info.score )
	end
end

function GUIc:showInfo()
	WidgetSetVisible( self.widgets.info, true )
	self.info_size = 0.05
end

function GUIc:updateInfo()
	if not self.info_size then
		return
	end
	if self.info_size < 2 then
		self.info_size = self.info_size + ( 2 - self.info_size ) * 0.2
		if self.info_size > 1.98 then
			self.info_size = 2
		end
		WidgetSetSize( self.widgets.info, 20 * self.info_size, 26 * self.info_size )
		WidgetSetPos( self.widgets.info, 482 - 10 * self.info_size, 445 - 13 * self.info_size )
	else
		self.info_size = self.info_size + self.dt
		if self.info_size > 3000 then
			WidgetSetVisible( self.widgets.info, false )
			self.info_size = nil
			return
		end
		local ltt = 0.75 + 0.25 * math.cos( self.t * 10 )
		local col = { ltt, ltt, ltt, 1 }
		WidgetSetSpriteColor( self.widgets.info, col )
	end
end

function GUIc:updateCombo( combo )
	if combo < 2 then
		WidgetSetVisible( self.widgets.combo, false )
		WidgetSetVisible( self.widgets.combox, false )
		WidgetSetVisible( self.widgets.combo1, false )
		WidgetSetVisible( self.widgets.combo2, false )
		WidgetSetVisible( self.widgets.combo3, false )
		return
	end
	if combo >= 100 then
		WidgetSetAnim( self.widgets.combo1, "9", false )
		WidgetSetAnim( self.widgets.combo2, "9", false )
		WidgetSetAnim( self.widgets.combo3, "+", false )
		WidgetSetVisible( self.widgets.combo, true )
		WidgetSetVisible( self.widgets.combox, true )
		WidgetSetVisible( self.widgets.combo1, true )
		WidgetSetVisible( self.widgets.combo2, true )
		WidgetSetVisible( self.widgets.combo3, true )
	elseif combo >= 10 then
		WidgetSetAnim( self.widgets.combo1, tostring( math.floor(combo / 10) ), false )
		WidgetSetAnim( self.widgets.combo2, tostring( combo - 10 * math.floor(combo / 10) ), false )
		WidgetSetVisible( self.widgets.combo, true )
		WidgetSetVisible( self.widgets.combox, true )
		WidgetSetVisible( self.widgets.combo1, true )
		WidgetSetVisible( self.widgets.combo2, true )
		WidgetSetVisible( self.widgets.combo3, false )
	else
		WidgetSetAnim( self.widgets.combo1, tostring( combo ), false )
		WidgetSetVisible( self.widgets.combo, true )
		WidgetSetVisible( self.widgets.combox, true )
		WidgetSetVisible( self.widgets.combo1, true )
		WidgetSetVisible( self.widgets.combo2, false )
		WidgetSetVisible( self.widgets.combo3, false )
	end
	local ltt = math.cos( self.t * 10 )
	local col = { 241/255 + 0.2 * ltt, 231/255 + 0.2 * ltt, 40/255 + 0.2 * ltt, 1 }
	WidgetSetSpriteColor( self.widgets.combo, col )
	WidgetSetSpriteColor( self.widgets.combox, col )
	WidgetSetSpriteColor( self.widgets.combo1, col )
	WidgetSetSpriteColor( self.widgets.combo2, col )
	WidgetSetSpriteColor( self.widgets.combo3, col )
	if self.combosize < 1.05 then
		self.combosize = 1
	else
		self.combosize = self.combosize + (1 - self.combosize) * 0.2
	end
	WidgetSetSize( self.widgets.combo, 110 * self.combosize, 56 * self.combosize )
	WidgetSetPos( self.widgets.combo, 207 - (self.combosize-1) * 55, 9 - (self.combosize-1) * 28 )
end

function GUIc:comboIncrease()
	if self.combosize < 1.25 then
		self.combosize = 1.5
	end
end

function GUIc:healthDecrease( num )
	if num == 1 then
		self.bars[1]:flashRed()
	elseif num == 2 then
		self.bars[3]:flashRed()
	end
end

function GUIc:updateScore( player, score )
	local sx, sy = GetCaptionSize( "dialogue", tostring(score) )
	if player == 1 then
		WidgetSetPos( self.widgets.score1, 180-sx, 50 )
		WidgetSetCaption( self.widgets.score1, tostring(score) )
	elseif player == 2 then
		WidgetSetPos( self.widgets.score2, 450+180-sx, 50 )
		WidgetSetCaption( self.widgets.score2, tostring(score) )
	end
end

function GUIc:changeWeapon( player, number )
	for i=1, 4 do
		if i == number then
			WidgetSetSpriteColor( self.widgets["weapon"..player.."_"..i], {1,1,.5,1} )
		else
			WidgetSetSpriteColor( self.widgets["weapon"..player.."_"..i], {159/255,155/255,166/255,1} )
		end
	end
	if number == 0 then
		WidgetSetAnim( self.widgets["weapon"..player.."_icon"], "weapon_icon_none", false )
	elseif number == 1 then
		local unyl = false
		if Game.actors then
			if Game.actors[player] and Game.actors[player].info and Game.actors[player].info.character == "pear15unyl" then
				unyl = true
			end
		else
			if vars.character == "pear15unyl" then
				unyl = player == 1
			else
				unyl = player == 2
			end
		end
		if not unyl then
			WidgetSetAnim( self.widgets["weapon"..player.."_icon"], "weapon_icon_soh", false )
		else
			WidgetSetAnim( self.widgets["weapon"..player.."_icon"], "weapon_icon_unyl", false )
		end
	else
		WidgetSetAnim( self.widgets["weapon"..player.."_icon"], "weapon_icon_"..number, false )
	end
end

function GUIc:updatePlayers()
	for k, v in pairs( Game.actors ) do
		SetDefaultActor( k )
		local ud = GetPlayerCharacter( k )
		self["character"..k] = ud
		self.bars[2*k-1].max = ud:health_max()/2
	end
end

function GUIc:updatePlayerReplacement(player)
	local player_replacement = "player"..player.."_replacement"
	if self.widgets[player_replacement] then
		WidgetSetCaption( self.widgets[player_replacement],  "Press /cffff55"..GetKeyName( CONFIG.key_conf[player].gui_nav_accept ).."/cffffff to join.", true )
	end
end

function GUIc:update()
	while self.visible do
		if self.character1 and self.character1:object_present() then
			self.bars[1]:updateValue( self.character1:health())
			self.bars[2]:updateValue( self.character1:ammo())
		else
			self:init(1)
		end
		if self.character2 and self.character2:object_present() and #Game.actors > 1 then
			self.bars[3]:updateValue( self.character2:health())
			self.bars[4]:updateValue( self.character2:ammo())
		else
			self:init(2)
		end
		for i=1,2 do
			self:showPlayer( i )
		end
		local time = GetCurTime()
		self.dt = time - self.time
		self.t = self.t + self.dt / 1000
		self.time = time
		while self.t > 2 * math.pi do
			self.t = self.t - 2 * math.pi
		end
		self.tt = math.cos( self.t )
		for k, v in pairs( self.bars ) do
			v:update()
		end
		self:updatePlayerReplacement(1)
		self:updatePlayerReplacement(2)
		self:updateCombo( Game.GetCombo() )
		self:updateInfo()
		Wait( 1 )
	end
end

function GUIc:showGameOver()
end

function GUIc:updateLives( player, lives )
end

function GUIc:hideCombo()
	WidgetSetVisible( self.widgets.combo, false )
	WidgetSetVisible( self.widgets.combox, false )
	WidgetSetVisible( self.widgets.combo1, false )
	WidgetSetVisible( self.widgets.combo2, false )
	WidgetSetVisible( self.widgets.combo3, false )
end

function GUIc:startWeaponCharge( player, max_charge, effect )
	local char = GetPlayerCharacter( player )
	if not char then
		return
	end
	local back = self.widgets["player"..player.."_weapon_charge_background"]
	local bar = self.widgets["player"..player.."_weapon_charge_bar"]
	local _old_proc = GetObjProcessor( char )
	local eff = effect
	WidgetSetVisible( back, true )
	WidgetSetVisible( bar, true )
	WidgetSetFixedPosition( back, false )
	WidgetSetFixedPosition( bar, false )
	local pos = char:aabb()
	WidgetSetPos( back, pos.p.x - 32, pos.p.y - pos.H - 18 )
	WidgetSetSpriteColor( self.widgets["player"..player.."_weapon_charge_bar"], { 0, 0, 1, 1 } )
	WidgetSetSize( bar, 1, 8 )
	SetObjProcessor( char, function( this )
		local charge = this:weapon_charge_time()
		if (not eff:object_present()) or charge == 0 then
			if eff:object_present() then
				SetObjDead( eff )
			end
			this:weapon_charge_time(0)
			WidgetSetVisible( back, false )
			WidgetSetVisible( bar, false )
			SetObjProcessor( char, _old_proc )
		else
			first_charge = false
			charge = math.max( 0, math.min( 1, charge / max_charge ))
			local pos = this:aabb()
			WidgetSetPos( back, pos.p.x - 32, pos.p.y - pos.H - 18 )
			WidgetSetSpriteColor( self.widgets["player"..player.."_weapon_charge_bar"], { 0.6*charge, charge, 1-charge, 1 } )
			WidgetSetSize( bar, 60*charge, 8 )
		end
		if _old_proc then
			_old_proc( this )
		end
	end )
	self["player"..player.."_charging"] = true
	self["player"..player.."_max_charge"] = max_charge
	self["player"..player.."_charge_effect"] = effect
end

return GUIc
