song = {

speed = 0.2,
name = "ndr's mitoll riff (hard)",
file = "music/ndr.ogg",
--delay = 4600,
--delay = 2000,
delay = 4550,
song = "ndr",

notes =
	{

		{time = 2243, dur = 174, nt="A" },
		{time = 2404, dur = 186, nt="D" },
		{time = 2630, dur = 174, nt="A" },
		{time = 2897, dur = 200, nt="D" },
		{time = 3123, dur = 227, nt="A" },
		{time = 3376, dur = 200, nt="D" },
		{time = 3616, dur = 293, nt="A" },
		{time = 3869, dur = 254, nt="D" },
		{time = 4096, dur = 240, nt="B" },
		{time = 4376, dur = 187, nt="D" },
		{time = 4603, dur = 200, nt="B" },
		{time = 4843, dur = 253, nt="D" },
		{time = 5122, dur = 201, nt="B" },
		{time = 5349, dur = 280, nt="D" },
		{time = 5615, dur = 267, nt="B" },
		{time = 5828, dur = 267, nt="D" },
		{time = 6095, dur = 267, nt="A" },
		{time = 6349, dur = 279, nt="D" },
		{time = 6656, dur = 293, nt="A" },
		{time = 6895, dur = 254, nt="D" },
		{time = 7135, dur = 307, nt="A" },
		{time = 7388, dur = 280, nt="D" },
		{time = 7655, dur = 252, nt="A" },
		{time = 7855, dur = 253, nt="D" },
		{time = 8068, dur = 280, nt="B" },
		{time = 8322, dur = 305, nt="D" },
		{time = 8615, dur = 252, nt="B" },
		{time = 8827, dur = 321, nt="D" },
		{time = 9108, dur = 226, nt="B" },
		{time = 9361, dur = 306, nt="D" },
		{time = 9601, dur = 253, nt="B" },
		{time = 9934, dur = 66, nt="B" },
		{time = 9840, dur = 188, nt="D" },
		{time = 10148, dur = 119, nt="A" },
		{time = 10148, dur = 145, nt="D" },
		{time = 10321, dur = 132, nt="A" },
		{time = 10321, dur = 146, nt="D" },
		{time = 10587, dur = 107, nt="A" },
		{time = 10587, dur = 119, nt="D" },
		{time = 10760, dur = 106, nt="D" },
		{time = 10786, dur = 94, nt="A" },
		{time = 10933, dur = 160, nt="A" },
		{time = 10933, dur = 226, nt="D" },
		{time = 11307, dur = 159, nt="A" },
		{time = 11307, dur = 173, nt="D" },
		{time = 11613, dur = 93, nt="A" },
		{time = 11586, dur = 147, nt="D" },
		{time = 11786, dur = 93, nt="A" },
		{time = 11800, dur = 93, nt="D" },
		{time = 11933, dur = 133, nt="A" },
		{time = 11947, dur = 239, nt="D" },
		{time = 12146, dur = 254, nt="B" },
		{time = 12372, dur = 267, nt="D" },
		{time = 12613, dur = 240, nt="B" },
		{time = 12865, dur = 268, nt="D" },
		{time = 13119, dur = 227, nt="B" },
		{time = 13332, dur = 361, nt="D" },
		{time = 13625, dur = 281, nt="B" },
		{time = 13879, dur = 187, nt="D" },
		{time = 14066, dur = 92, nt="A" },
		{time = 14146, dur = 119, nt="D" },
		{time = 14212, dur = 107, nt="A" },
		{time = 14319, dur = 120, nt="D" },
		{time = 14359, dur = 120, nt="A" },
		{time = 14572, dur = 106, nt="D" },
		{time = 14572, dur = 106, nt="A" },
		{time = 14732, dur = 93, nt="D" },
		{time = 14758, dur = 107, nt="A" },
		{time = 14918, dur = 120, nt="D" },
		{time = 14945, dur = 120, nt="A" },
		{time = 15171, dur = 120, nt="A" },
		{time = 15171, dur = 134, nt="D" },
		{time = 15398, dur = 107, nt="A" },
		{time = 15411, dur = 121, nt="D" },
		{time = 15572, dur = 106, nt="A" },
		{time = 15598, dur = 94, nt="D" },
		{time = 15732, dur = 120, nt="D" },
		{time = 15732, dur = 133, nt="A" },
		{time = 16065, dur = 146, nt="C" },
		{time = 16077, dur = 174, nt="B" },
		{time = 16371, dur = 107, nt="C" },
		{time = 16384, dur = 134, nt="B" },
		{time = 16571, dur = 120, nt="C" },
		{time = 16598, dur = 133, nt="B" },
		{time = 16784, dur = 121, nt="C" },
		{time = 16811, dur = 134, nt="B" },
		{time = 16997, dur = 120, nt="C" },
		{time = 17051, dur = 120, nt="B" },
		{time = 17184, dur = 106, nt="C" },
		{time = 17238, dur = 120, nt="B" },
		{time = 17370, dur = 107, nt="C" },
		{time = 17424, dur = 160, nt="B" },
		{time = 17531, dur = 120, nt="C" },
		{time = 17677, dur = 94, nt="A" },
		{time = 17717, dur = 94, nt="D" },
		{time = 17931, dur = 66, nt="D" },
		{time = 17931, dur = 93, nt="A" },
		{time = 18116, dur = 121, nt="C" },
		{time = 18104, dur = 146, nt="B" },
		{time = 18344, dur = 93, nt="C" },
		{time = 18357, dur = 107, nt="B" },
		{time = 18490, dur = 93, nt="C" },
		{time = 18531, dur = 119, nt="B" },
		{time = 18703, dur = 94, nt="C" },
		{time = 18743, dur = 94, nt="B" },
		{time = 18864, dur = 120, nt="C" },
		{time = 18890, dur = 294, nt="B" },
		{time = 19036, dur = 187, nt="C" },
		{time = 19397, dur = 120, nt="D" },
		{time = 19690, dur = 120, nt="D" },
		{time = 19890, dur = 93, nt="D" },
		{time = 20036, dur = 80, nt="D" },
		{time = 20169, dur = 108, nt="D" },
		{time = 20343, dur = 80, nt="D" },
		{time = 20476, dur = 94, nt="D" },
		{time = 20622, dur = 94, nt="D" },
		{time = 20769, dur = 120, nt="D" },
		{time = 20983, dur = 186, nt="D" },
		{time = 21115, dur = 108, nt="C" },
		{time = 21329, dur = 173, nt="B" },
		{time = 21476, dur = 120, nt="A" },
		{time = 21556, dur = 160, nt="B" },
		{time = 21675, dur = 148, nt="A" },
		{time = 21782, dur = 213, nt="B" },
		{time = 21955, dur = 134, nt="A" },
		{time = 22035, dur = 121, nt="B" },
		{time = 22262, dur = 106, nt="C" },
		{time = 22422, dur = 93, nt="C" },
		{time = 22555, dur = 106, nt="C" },
		{time = 22741, dur = 94, nt="C" },
		{time = 22902, dur = 92, nt="C" },
		{time = 23102, dur = 133, nt="C" },
		{time = 23381, dur = 148, nt="D" },
		{time = 23715, dur = 107, nt="D" },
		{time = 23928, dur = 106, nt="D" },
		{time = 24088, dur = 133, nt="D" },
		{time = 24301, dur = 160, nt="C" },
		{time = 24448, dur = 133, nt="D" },
		{time = 24541, dur = 147, nt="C" },
		{time = 24714, dur = 120, nt="D" },
		{time = 24794, dur = 133, nt="C" },
		{time = 24888, dur = 106, nt="D" },
		{time = 24954, dur = 147, nt="C" },
		{time = 25074, dur = 120, nt="D" },
		{time = 25154, dur = 133, nt="C" },
		{time = 25341, dur = 159, nt="B" },
		{time = 25474, dur = 120, nt="A" },
		{time = 25554, dur = 146, nt="B" },
		{time = 25660, dur = 147, nt="A" },
		{time = 25740, dur = 147, nt="B" },
		{time = 25887, dur = 80, nt="C" },
		{time = 25873, dur = 134, nt="A" },
		{time = 25941, dur = 120, nt="B" },
		{time = 26061, dur = 120, nt="C" },
		{time = 26127, dur = 107, nt="D" },
		{time = 26380, dur = 94, nt="D" },
		{time = 26514, dur = 106, nt="D" },
		{time = 26753, dur = 107, nt="D" },
		{time = 26913, dur = 107, nt="D" },
		{time = 27100, dur = 106, nt="D" },
		{time = 27433, dur = 146, nt="A" },
		{time = 27713, dur = 107, nt="A" },
		{time = 27952, dur = 108, nt="A" },
		{time = 28126, dur = 120, nt="A" },
		{time = 28353, dur = 120, nt="A" },
		{time = 28539, dur = 107, nt="A" },
		{time = 28780, dur = 66, nt="A" },
		{time = 28939, dur = 107, nt="A" },
		{time = 29099, dur = 160, nt="A" },
		{time = 29205, dur = 187, nt="B" },
		{time = 29379, dur = 133, nt="C" },
		{time = 29472, dur = 120, nt="D" },
		{time = 29552, dur = 147, nt="C" },
		{time = 29658, dur = 94, nt="D" },
		{time = 29739, dur = 93, nt="C" },
		{time = 29819, dur = 106, nt="D" },
		{time = 29899, dur = 120, nt="C" },
		{time = 29965, dur = 120, nt="D" },
		{time = 30072, dur = 67, nt="C" },
		{time = 30112, dur = 200, nt="B" },
		{time = 30392, dur = 93, nt="B" },
		{time = 30526, dur = 132, nt="B" },
		{time = 30765, dur = 106, nt="B" },
		{time = 30938, dur = 147, nt="B" },
		{time = 31164, dur = 160, nt="A" },
		{time = 31445, dur = 160, nt="B" },
		{time = 31725, dur = 106, nt="B" },
		{time = 31938, dur = 120, nt="B" },
		{time = 32084, dur = 174, nt="C" },
		{time = 32377, dur = 148, nt="D" },
		{time = 32485, dur = 159, nt="C" },
		{time = 32631, dur = 133, nt="D" },
		{time = 32724, dur = 146, nt="C" },
		{time = 32844, dur = 106, nt="D" },
		{time = 32898, dur = 120, nt="C" },
		{time = 33084, dur = 199, nt="B" },
		{time = 33231, dur = 173, nt="A" },
		{time = 33351, dur = 146, nt="B" },
		{time = 33497, dur = 147, nt="A" },
		{time = 33577, dur = 173, nt="B" },
		{time = 33710, dur = 147, nt="A" },
		{time = 33804, dur = 146, nt="B" },
		{time = 33977, dur = 106, nt="C" },
		{time = 34032, dur = 158, nt="D" },
		{time = 34404, dur = 92, nt="D" },
		{time = 34564, dur = 93, nt="D" },
		{time = 34763, dur = 94, nt="D" },
		{time = 34897, dur = 120, nt="D" },
		{time = 35096, dur = 174, nt="A" },
		{time = 35444, dur = 119, nt="B" },
		{time = 35697, dur = 119, nt="B" },
		{time = 35896, dur = 106, nt="B" },
		{time = 36070, dur = 132, nt="B" },
		{time = 36323, dur = 226, nt="C" },
		{time = 36496, dur = 133, nt="D" },
		{time = 36789, dur = 80, nt="C" },
		{time = 36936, dur = 160, nt="D" },
		{time = 37082, dur = 147, nt="C" },
		{time = 37309, dur = 160, nt="B" },
		{time = 37469, dur = 120, nt="A" },
		{time = 37535, dur = 161, nt="B" },
		{time = 37669, dur = 119, nt="A" },
		{time = 37736, dur = 146, nt="B" },
		{time = 37868, dur = 134, nt="A" },
		{time = 37935, dur = 134, nt="B" },
		{time = 38055, dur = 120, nt="A" },
		{time = 38122, dur = 67, nt="B" },
		{time = 38335, dur = 120, nt="D" },
		{time = 38495, dur = 107, nt="D" },
		{time = 38642, dur = 106, nt="D" },
		{time = 38828, dur = 121, nt="D" },
		{time = 39068, dur = 187, nt="D" },
		{time = 39428, dur = 133, nt="B" },
		{time = 39708, dur = 119, nt="B" },
		{time = 39961, dur = 93, nt="B" },
		{time = 40121, dur = 133, nt="B" },
		{time = 40348, dur = 119, nt="C" },
		{time = 40521, dur = 94, nt="C" },
		{time = 40747, dur = 94, nt="C" },
		{time = 40921, dur = 93, nt="C" },
		{time = 41160, dur = 67, nt="B" },
		{time = 41160, dur = 107, nt="C" },
		{time = 41347, dur = 146, nt="B" },
		{time = 41454, dur = 160, nt="A" },
		{time = 41547, dur = 173, nt="B" },
		{time = 41667, dur = 133, nt="A" },
		{time = 41760, dur = 147, nt="B" },
		{time = 41880, dur = 134, nt="A" },
		{time = 41960, dur = 133, nt="B" },
		{time = 42187, dur = 133, nt="C" },
		{time = 42546, dur = 174, nt="C" },
		{time = 42694, dur = 132, nt="D" },
		{time = 42786, dur = 134, nt="C" },
		{time = 42880, dur = 120, nt="D" },
		{time = 42947, dur = 160, nt="C" },
		{time = 43067, dur = 160, nt="D" },
		{time = 43173, dur = 107, nt="C" },
		{time = 44079, dur = 107, nt="A" },
		{time = 44280, dur = 92, nt="A" },
		{time = 44506, dur = 80, nt="A" },
		{time = 44679, dur = 94, nt="A" },
		{time = 44892, dur = 80, nt="A" },
		{time = 45066, dur = 80, nt="A" },
		{time = 45239, dur = 80, nt="A" },
		{time = 45412, dur = 93, nt="A" },
		{time = 45585, dur = 120, nt="A" },
		{time = 45772, dur = 120, nt="A" },
		{time = 45998, dur = 120, nt="B" },
		{time = 46185, dur = 120, nt="B" },
		{time = 46411, dur = 121, nt="B" },
		{time = 46599, dur = 119, nt="B" },
		{time = 46772, dur = 106, nt="B" },
		{time = 47038, dur = 119, nt="C" },
		{time = 47211, dur = 107, nt="C" },
		{time = 47371, dur = 107, nt="C" },
		{time = 47558, dur = 80, nt="C" },
		{time = 47704, dur = 94, nt="C" },
		{time = 47877, dur = 94, nt="C" },
		{time = 48011, dur = 106, nt="C" },
		{time = 48144, dur = 80, nt="A" },
		{time = 48304, dur = 66, nt="A" },
		{time = 48451, dur = 93, nt="A" },
		{time = 48664, dur = 93, nt="A" },
		{time = 48865, dur = 92, nt="A" },
		{time = 49090, dur = 134, nt="A" },
		{time = 49304, dur = 134, nt="A" },
		{time = 49543, dur = 120, nt="A" },
		{time = 49771, dur = 119, nt="A" },
		{time = 49970, dur = 106, nt="A" },
		{time = 50144, dur = 119, nt="B" },
		{time = 50332, dur = 131, nt="B" },
		{time = 50583, dur = 107, nt="B" },
		{time = 50770, dur = 174, nt="B" },
		{time = 50996, dur = 134, nt="B" },
		{time = 51157, dur = 120, nt="C" },
		{time = 51329, dur = 107, nt="C" },
		{time = 51516, dur = 106, nt="C" },
		{time = 51702, dur = 108, nt="C" },
		{time = 51876, dur = 160, nt="C" },
		{time = 52169, dur = 200, nt="C" },
		{time = 52157, dur = 252, nt="B" },
		{time = 52542, dur = 147, nt="D" },
		{time = 52569, dur = 187, nt="B" },
		{time = 52782, dur = 107, nt="D" },
		{time = 53009, dur = 133, nt="D" },
		{time = 53208, dur = 134, nt="D" },
		{time = 53462, dur = 93, nt="D" },
		{time = 53595, dur = 107, nt="D" },
		{time = 53782, dur = 93, nt="D" },
		{time = 53915, dur = 133, nt="D" },
		{time = 54128, dur = 120, nt="A" },
		{time = 54541, dur = 81, nt="A" },
		{time = 54795, dur = 93, nt="A" },
		{time = 55008, dur = 93, nt="A" },
		{time = 55248, dur = 80, nt="A" },
		{time = 55475, dur = 66, nt="A" },
		{time = 55661, dur = 120, nt="A" },
		{time = 55874, dur = 147, nt="A" },
		{time = 56101, dur = 160, nt="C" },
		{time = 56488, dur = 160, nt="C" },
		{time = 56740, dur = 134, nt="C" },
		{time = 56981, dur = 106, nt="C" },
		{time = 57194, dur = 93, nt="C" },
		{time = 57380, dur = 107, nt="C" },
		{time = 57554, dur = 120, nt="C" },
		{time = 57713, dur = 108, nt="C" },
		{time = 57873, dur = 160, nt="B" },
		{time = 58154, dur = 132, nt="D" },
		{time = 58473, dur = 120, nt="D" },
		{time = 58699, dur = 121, nt="D" },
		{time = 58873, dur = 120, nt="D" },
		{time = 59060, dur = 120, nt="D" },
		{time = 59233, dur = 106, nt="D" },
		{time = 59447, dur = 66, nt="D" },
		{time = 59579, dur = 120, nt="D" },
		{time = 59806, dur = 120, nt="C" },
		{time = 59992, dur = 121, nt="D" },
		{time = 60419, dur = 133, nt="D" },
		{time = 60686, dur = 106, nt="D" },
		{time = 60912, dur = 107, nt="D" },
		{time = 61099, dur = 120, nt="D" },
		{time = 61273, dur = 119, nt="D" },
		{time = 61472, dur = 120, nt="D" },
		{time = 61712, dur = 120, nt="C" },
		{time = 61899, dur = 92, nt="C" },
		{time = 62112, dur = 186, nt="B" },
		{time = 62485, dur = 120, nt="B" },
		{time = 62699, dur = 119, nt="B" },
		{time = 62885, dur = 147, nt="B" },
		{time = 63098, dur = 186, nt="B" },
		{time = 63284, dur = 147, nt="A" },
		{time = 63378, dur = 159, nt="B" },
		{time = 63525, dur = 132, nt="A" },
		{time = 63591, dur = 174, nt="B" },
		{time = 63871, dur = 107, nt="C" },
		{time = 64098, dur = 146, nt="C" },
		{time = 64484, dur = 147, nt="C" },
		{time = 64698, dur = 120, nt="D" },
		{time = 64938, dur = 185, nt="C" },
		{time = 65123, dur = 174, nt="D" },
		{time = 65243, dur = 174, nt="C" },
		{time = 65364, dur = 160, nt="D" },
		{time = 65484, dur = 146, nt="C" },
		{time = 65604, dur = 306, nt="B" },
		{time = 66043, dur = 174, nt="B" },
		{time = 66376, dur = 174, nt="B" },
		{time = 66643, dur = 146, nt="B" },
		{time = 66897, dur = 173, nt="A" },
		{time = 67056, dur = 134, nt="B" },
		{time = 67310, dur = 133, nt="B" },
		{time = 67430, dur = 146, nt="A" },
		{time = 67509, dur = 214, nt="B" },
		{time = 67696, dur = 160, nt="A" },
		{time = 67789, dur = 133, nt="B" },
		{time = 68030, dur = 119, nt="D" },
		{time = 68229, dur = 134, nt="D" },
		{time = 68415, dur = 268, nt="D" },
		{time = 68642, dur = 187, nt="C" },

		{time = 69317, dur = 200, nt="A" },
		{time = 69583, dur = 133, nt="B" },
		{time = 69796, dur = 134, nt="C" },
		{time = 70036, dur = 120, nt="D" },
		{time = 70329, dur = 94, nt="D" },
		{time = 70476, dur = 94, nt="D" },
		{time = 70730, dur = 106, nt="D" },
		{time = 70889, dur = 106, nt="D" },
		{time = 71075, dur = 108, nt="D" },
		{time = 71449, dur = 160, nt="B" },
		{time = 71769, dur = 94, nt="D" },
		{time = 71982, dur = 107, nt="D" },
		{time = 72156, dur = 92, nt="D" },
		{time = 72368, dur = 94, nt="D" },
		{time = 72542, dur = 93, nt="D" },
		{time = 72781, dur = 107, nt="D" },
		{time = 73035, dur = 93, nt="D" },
		{time = 73301, dur = 160, nt="A" },
		{time = 73515, dur = 160, nt="B" },
		{time = 73715, dur = 107, nt="B" },
		{time = 74074, dur = 134, nt="C" },
		{time = 74474, dur = 120, nt="C" },
		{time = 74700, dur = 94, nt="C" },
		{time = 74848, dur = 106, nt="C" },
		{time = 75061, dur = 120, nt="D" },
		{time = 75474, dur = 106, nt="D" },
		{time = 75754, dur = 109, nt="D" },
		{time = 75981, dur = 92, nt="D" },
		{time = 76127, dur = 107, nt="D" },
		{time = 76340, dur = 107, nt="D" },
		{time = 76487, dur = 107, nt="D" },
		{time = 76687, dur = 94, nt="D" },
		{time = 76927, dur = 106, nt="D" },
		{time = 77126, dur = 108, nt="D" },
		{time = 77340, dur = 213, nt="A" },
		{time = 77527, dur = 239, nt="B" },
		{time = 77807, dur = 93, nt="B" },
		{time = 77873, dur = 133, nt="C" },
		{time = 78126, dur = 253, nt="D" },
		{time = 78433, dur = 80, nt="D" },
		{time = 78566, dur = 81, nt="D" },
		{time = 78780, dur = 92, nt="D" },
		{time = 78940, dur = 92, nt="D" },
		{time = 79126, dur = 107, nt="D" },
		{time = 79446, dur = 106, nt="D" },
		{time = 79752, dur = 107, nt="D" },
		{time = 80005, dur = 80, nt="D" },
		{time = 80139, dur = 106, nt="D" },
		{time = 80352, dur = 94, nt="D" },
		{time = 80499, dur = 93, nt="D" },
		{time = 80752, dur = 107, nt="D" },
		{time = 80965, dur = 133, nt="D" },
		{time = 81244, dur = 120, nt="E" },
		{time = 81498, dur = 107, nt="E" },
		{time = 81685, dur = 120, nt="E" },
		{time = 81898, dur = 106, nt="E" },
		{time = 82098, dur = 106, nt="D" },
		{time = 82445, dur = 106, nt="D" },
		{time = 82657, dur = 94, nt="D" },
		{time = 82804, dur = 94, nt="D" },
		{time = 82938, dur = 93, nt="D" },
		{time = 83137, dur = 107, nt="D" },
		{time = 83471, dur = 106, nt="D" },
		{time = 83790, dur = 94, nt="D" },
		{time = 83977, dur = 94, nt="D" },
		{time = 84111, dur = 106, nt="D" },
		{time = 84364, dur = 93, nt="D" },
		{time = 84524, dur = 92, nt="D" },
		{time = 84724, dur = 106, nt="D" },
		{time = 85070, dur = 133, nt="D" },
		{time = 85376, dur = 227, nt="A" },
		{time = 85523, dur = 280, nt="B" },
		{time = 85763, dur = 120, nt="D" },
		{time = 85963, dur = 147, nt="D" },
		{time = 86176, dur = 1840, nt="D" },
		{time = 88109, dur = 1758, nt="D" },
		{time = 90041, dur = 1986, nt="C" },
		{time = 91987, dur = 266, nt="B" },
		{time = 92227, dur = 120, nt="C" },
		{time = 92281, dur = 132, nt="D" },
		{time = 92441, dur = 159, nt="D" },
		{time = 92573, dur = 173, nt="C" },
		{time = 92720, dur = 120, nt="D" },
		{time = 92840, dur = 160, nt="C" },
		{time = 92947, dur = 120, nt="D" },
		{time = 93053, dur = 120, nt="C" },
		{time = 93133, dur = 134, nt="D" },
		{time = 93239, dur = 121, nt="C" },
		{time = 93320, dur = 120, nt="D" },
		{time = 93413, dur = 133, nt="C" },
		{time = 93520, dur = 146, nt="D" },
		{time = 93613, dur = 147, nt="C" },
		{time = 93706, dur = 160, nt="D" },
		{time = 93813, dur = 93, nt="C" },
		{time = 94159, dur = 161, nt="A" },
		{time = 94226, dur = 214, nt="B" },
		{time = 94519, dur = 107, nt="D" },
		{time = 94705, dur = 94, nt="D" },
		{time = 94919, dur = 107, nt="D" },
		{time = 95132, dur = 107, nt="D" },
		{time = 95306, dur = 106, nt="D" },
		{time = 95452, dur = 121, nt="D" },
		{time = 95625, dur = 67, nt="E" },
		{time = 95599, dur = 106, nt="D" },
		{time = 96078, dur = 134, nt="E" },
		{time = 96479, dur = 119, nt="D" },
		{time = 96705, dur = 119, nt="D" },
		{time = 96918, dur = 120, nt="D" },
		{time = 97158, dur = 120, nt="D" },
		{time = 97371, dur = 94, nt="D" },
		{time = 97572, dur = 92, nt="D" },
		{time = 97744, dur = 147, nt="D" },
		{time = 98065, dur = 106, nt="E" },
		{time = 98224, dur = 107, nt="D" },
		{time = 98612, dur = 105, nt="D" },
		{time = 98824, dur = 93, nt="D" },
		{time = 99024, dur = 106, nt="D" },
		{time = 99210, dur = 94, nt="D" },
		{time = 99370, dur = 107, nt="D" },
		{time = 99544, dur = 79, nt="D" },
		{time = 99664, dur = 159, nt="D" },
		{time = 100024, dur = 333, nt="B" },
		{time = 100517, dur = 93, nt="D" },
		{time = 100690, dur = 107, nt="D" },
		{time = 100930, dur = 106, nt="D" },
		{time = 101183, dur = 106, nt="D" },
		{time = 101437, dur = 93, nt="D" },
		{time = 101596, dur = 94, nt="D" },
		{time = 101823, dur = 106, nt="D" },
		{time = 102089, dur = 147, nt="A" },
		{time = 102502, dur = 120, nt="D" },
		{time = 102729, dur = 107, nt="D" },
		{time = 103023, dur = 106, nt="D" },
		{time = 103262, dur = 106, nt="D" },
		{time = 103476, dur = 106, nt="D" },
		{time = 103649, dur = 120, nt="D" },
		{time = 103823, dur = 119, nt="D" },
		{time = 104088, dur = 108, nt="E" },
		{time = 104302, dur = 93, nt="D" },
		{time = 104728, dur = 94, nt="D" },
		{time = 104968, dur = 94, nt="D" },
		{time = 105235, dur = 93, nt="D" },
		{time = 105475, dur = 93, nt="D" },
		{time = 105621, dur = 107, nt="D" },
		{time = 105861, dur = 133, nt="D" },
		{time = 106074, dur = 147, nt="B" },
		{time = 106461, dur = 93, nt="D" },
		{time = 106714, dur = 93, nt="D" },
		{time = 106994, dur = 93, nt="D" },
		{time = 107207, dur = 93, nt="D" },
		{time = 107407, dur = 107, nt="D" },
		{time = 107594, dur = 93, nt="D" },
		{time = 107807, dur = 106, nt="D" },
		{time = 108073, dur = 147, nt="A" },
		{time = 108447, dur = 106, nt="D" },
		{time = 108739, dur = 108, nt="D" },
		{time = 108993, dur = 121, nt="D" },
		{time = 109220, dur = 93, nt="D" },
		{time = 109419, dur = 83, nt="D" },
		{time = 109593, dur = 93, nt="D" },
		{time = 109766, dur = 94, nt="D" },
		{time = 109952, dur = 80, nt="D" },
		{time = 110179, dur = 174, nt="A" },
		{time = 110273, dur = 186, nt="B" },
		{time = 110433, dur = 119, nt="A" },
		{time = 110499, dur = 160, nt="B" },
		{time = 110632, dur = 107, nt="A" },
		{time = 110699, dur = 160, nt="B" },
		{time = 110819, dur = 120, nt="A" },
		{time = 110899, dur = 146, nt="B" },
		{time = 111059, dur = 200, nt="C" },
		{time = 111352, dur = 106, nt="C" },
		{time = 111205, dur = 333, nt="D" },
		{time = 111552, dur = 106, nt="C" },
		{time = 111606, dur = 133, nt="D" },
		{time = 111725, dur = 107, nt="C" },
		{time = 111792, dur = 147, nt="D" },
		{time = 111899, dur = 120, nt="C" },
		{time = 111992, dur = 107, nt="D" },
		{time = 112138, dur = 187, nt="A" },
		{time = 112485, dur = 107, nt="D" },
		{time = 112671, dur = 108, nt="D" },
		{time = 112965, dur = 107, nt="D" },
		{time = 113192, dur = 92, nt="D" },
		{time = 113391, dur = 94, nt="D" },
		{time = 113577, dur = 108, nt="D" },
		{time = 113739, dur = 132, nt="D" },
		{time = 114044, dur = 107, nt="B" },
		{time = 114431, dur = 106, nt="D" },
		{time = 114738, dur = 92, nt="D" },
		{time = 115004, dur = 107, nt="D" },
		{time = 115283, dur = 94, nt="D" },
		{time = 115524, dur = 93, nt="D" },
		{time = 115684, dur = 93, nt="D" },
		{time = 115883, dur = 94, nt="D" },
		{time = 116097, dur = 133, nt="C" },
		{time = 116510, dur = 93, nt="D" },
		{time = 116803, dur = 94, nt="D" },
		{time = 117056, dur = 94, nt="D" },
		{time = 117256, dur = 120, nt="D" },
		{time = 117483, dur = 120, nt="D" },
		{time = 117683, dur = 120, nt="D" },
		{time = 118056, dur = 120, nt="E" },
		{time = 118509, dur = 94, nt="D" },
		{time = 118789, dur = 93, nt="D" },
		{time = 119016, dur = 106, nt="D" },
		{time = 119269, dur = 93, nt="D" },
		{time = 119482, dur = 93, nt="D" },
		{time = 119656, dur = 92, nt="D" },
		{time = 119855, dur = 147, nt="D" },
		{time = 120121, dur = 108, nt="A" },
		{time = 120482, dur = 120, nt="D" },
		{time = 120722, dur = 93, nt="D" },
		{time = 120961, dur = 120, nt="D" },
		{time = 121201, dur = 121, nt="D" },
		{time = 121442, dur = 106, nt="D" },
		{time = 121655, dur = 120, nt="D" },
		{time = 121827, dur = 94, nt="D" },
		{time = 122054, dur = 134, nt="C" },
		{time = 122507, dur = 107, nt="D" },
		{time = 122761, dur = 93, nt="D" },
		{time = 123028, dur = 92, nt="D" },
		{time = 123267, dur = 106, nt="D" },
		{time = 123507, dur = 93, nt="D" },
		{time = 123680, dur = 94, nt="D" },
		{time = 123880, dur = 107, nt="D" },
		{time = 124107, dur = 120, nt="B" },
		{time = 124427, dur = 106, nt="D" },
		{time = 124760, dur = 93, nt="D" },
		{time = 125000, dur = 107, nt="D" },
		{time = 125239, dur = 107, nt="D" },
		{time = 125466, dur = 107, nt="D" },
		{time = 125653, dur = 120, nt="D" },
		{time = 126066, dur = 120, nt="A" },
		{time = 126426, dur = 106, nt="D" },
		{time = 126759, dur = 93, nt="D" },
		{time = 126999, dur = 119, nt="D" },
		{time = 127252, dur = 107, nt="D" },
		{time = 127465, dur = 107, nt="D" },
		{time = 127665, dur = 107, nt="D" },
		{time = 127852, dur = 120, nt="D" },
		{time = 128092, dur = 93, nt="E" },
		{time = 128279, dur = 92, nt="E" },
		{time = 128545, dur = 93, nt="E" },
		{time = 128704, dur = 94, nt="E" },
		{time = 128931, dur = 107, nt="E" },
		{time = 129358, dur = 93, nt="A" },
		{time = 129571, dur = 147, nt="A" },
		{time = 129851, dur = 120, nt="A" },
		{time = 130051, dur = 133, nt="B" },
		{time = 130318, dur = 66, nt="A" },
		{time = 130478, dur = 66, nt="A" },
		{time = 130731, dur = 66, nt="A" },
		{time = 130877, dur = 54, nt="A" },
		{time = 131104, dur = 94, nt="A" },
		{time = 131490, dur = 93, nt="A" },
		{time = 131797, dur = 80, nt="A" },
		{time = 132024, dur = 80, nt="A" },
		{time = 132183, dur = 67, nt="A" },
		{time = 132423, dur = 80, nt="A" },
		{time = 132596, dur = 67, nt="A" },
		{time = 132796, dur = 107, nt="A" },
		{time = 133103, dur = 120, nt="A" },
		{time = 133369, dur = 108, nt="C" },
		{time = 133570, dur = 92, nt="D" },
		{time = 133796, dur = 93, nt="D" },
		{time = 134023, dur = 92, nt="D" },
		{time = 134209, dur = 147, nt="A" },
		{time = 134476, dur = 93, nt="A" },
		{time = 134636, dur = 79, nt="A" },
		{time = 134823, dur = 66, nt="A" },
		{time = 135129, dur = 93, nt="A" },
		{time = 135489, dur = 80, nt="A" },
		{time = 135769, dur = 80, nt="A" },
		{time = 136022, dur = 53, nt="A" },
		{time = 136168, dur = 67, nt="A" },
		{time = 136408, dur = 67, nt="A" },
		{time = 136568, dur = 67, nt="A" },
		{time = 136781, dur = 81, nt="A" },
		{time = 136994, dur = 67, nt="A" },
		{time = 137168, dur = 80, nt="A" },
		{time = 137367, dur = 94, nt="D" },
		{time = 137541, dur = 107, nt="D" },
		{time = 137701, dur = 133, nt="D" },
		{time = 137714, dur = 187, nt="C" },
		{time = 137848, dur = 133, nt="D" },
		{time = 137954, dur = 120, nt="C" },
		{time = 138034, dur = 133, nt="D" },
		{time = 138155, dur = 199, nt="B" },
		{time = 138434, dur = 80, nt="D" },
		{time = 138568, dur = 106, nt="D" },
		{time = 138754, dur = 80, nt="D" },
		{time = 138887, dur = 94, nt="D" },
		{time = 139127, dur = 147, nt="A" },
		{time = 139500, dur = 107, nt="D" },
		{time = 139740, dur = 107, nt="D" },
		{time = 140007, dur = 80, nt="D" },
		{time = 140140, dur = 106, nt="D" },
		{time = 140353, dur = 94, nt="D" },
		{time = 140513, dur = 80, nt="D" },
		{time = 140727, dur = 93, nt="D" },
		{time = 140886, dur = 107, nt="D" },
		{time = 141100, dur = 120, nt="D" },
		{time = 141353, dur = 80, nt="A" },
		{time = 141553, dur = 106, nt="A" },
		{time = 141860, dur = 79, nt="A" },
		{time = 142046, dur = 67, nt="A" },
		{time = 142193, dur = 66, nt="A" },
		{time = 142379, dur = 93, nt="D" },
		{time = 142526, dur = 106, nt="D" },
		{time = 142712, dur = 94, nt="D" },
		{time = 142859, dur = 93, nt="D" },
		{time = 143032, dur = 107, nt="D" },
		{time = 143392, dur = 146, nt="B" },
		{time = 143712, dur = 93, nt="D" },
		{time = 143899, dur = 92, nt="D" },
		{time = 144045, dur = 107, nt="D" },
		{time = 144232, dur = 106, nt="D" },
		{time = 144392, dur = 120, nt="D" },
		{time = 144472, dur = 146, nt="C" },
		{time = 144565, dur = 120, nt="D" },
		{time = 144685, dur = 120, nt="C" },
		{time = 144751, dur = 107, nt="D" },
		{time = 144845, dur = 186, nt="C" },
		{time = 144965, dur = 119, nt="D" },
		{time = 145151, dur = 133, nt="A" },
		{time = 145405, dur = 92, nt="D" },
		{time = 145591, dur = 107, nt="D" },
		{time = 145791, dur = 119, nt="D" },
		{time = 146004, dur = 706, nt="E" },
		{time = 146750, dur = 160, nt="B" },
		{time = 146937, dur = 94, nt="B" },
		{time = 147071, dur = 186, nt="A" },
		{time = 147444, dur = 106, nt="A" },
		{time = 147644, dur = 133, nt="A" },
		{time = 147977, dur = 106, nt="D" },
		{time = 148137, dur = 106, nt="D" },
		{time = 148350, dur = 106, nt="D" },
		{time = 148523, dur = 93, nt="D" },
		{time = 148709, dur = 94, nt="D" },
		{time = 148869, dur = 94, nt="D" },
		{time = 149043, dur = 107, nt="D" },
		{time = 149256, dur = 227, nt="B" },
		{time = 149443, dur = 172, nt="A" },
		{time = 149563, dur = 186, nt="B" },
		{time = 149709, dur = 213, nt="A" },
		{time = 149829, dur = 173, nt="B" },
		{time = 150002, dur = 120, nt="A" },
		{time = 150162, dur = 134, nt="D" },
		{time = 150415, dur = 94, nt="D" },
		{time = 150549, dur = 107, nt="D" },
		{time = 150762, dur = 66, nt="D" },
		{time = 150882, dur = 107, nt="D" },
		{time = 151055, dur = 147, nt="D" },
		{time = 151389, dur = 239, nt="C" },
		{time = 151708, dur = 147, nt="C" },
		{time = 151989, dur = 92, nt="C" },
		{time = 152135, dur = 200, nt="C" },
		{time = 152362, dur = 146, nt="B" },
		{time = 152508, dur = 214, nt="A" },
		{time = 152668, dur = 199, nt="B" },
		{time = 152827, dur = 201, nt="A" },
		{time = 152948, dur = 186, nt="B" },
		{time = 153108, dur = 146, nt="A" },
		{time = 153334, dur = 107, nt="D" },
		{time = 153494, dur = 93, nt="D" },
		{time = 153641, dur = 280, nt="D" },
		{time = 153974, dur = 107, nt="D" },
		{time = 154120, dur = 267, nt="B" },
		{time = 154401, dur = 92, nt="D" },
		{time = 154547, dur = 94, nt="D" },
		{time = 154734, dur = 93, nt="D" },
		{time = 154894, dur = 93, nt="D" },
		{time = 155067, dur = 107, nt="D" },
		{time = 155467, dur = 120, nt="D" },
		{time = 155666, dur = 120, nt="D" },
		{time = 155814, dur = 159, nt="A" },
		{time = 155934, dur = 173, nt="B" },
		{time = 156067, dur = 160, nt="A" },
		{time = 156320, dur = 93, nt="D" },
		{time = 156467, dur = 106, nt="D" },
		{time = 156746, dur = 107, nt="D" },
		{time = 156906, dur = 121, nt="D" },
		{time = 157133, dur = 93, nt="A" },
		{time = 157159, dur = 107, nt="D" },
		{time = 157386, dur = 120, nt="A" },
		{time = 157452, dur = 147, nt="B" },
		{time = 157599, dur = 134, nt="A" },
		{time = 157666, dur = 173, nt="B" },
		{time = 157813, dur = 159, nt="A" },
		{time = 157933, dur = 133, nt="B" },
		{time = 158039, dur = 80, nt="A" },
		{time = 158119, dur = 93, nt="D" },
		{time = 158319, dur = 93, nt="D" },
		{time = 158506, dur = 67, nt="D" },
		{time = 158745, dur = 94, nt="D" },
		{time = 158906, dur = 92, nt="D" },
		{time = 159092, dur = 106, nt="D" },
		{time = 159319, dur = 186, nt="A" },
		{time = 159479, dur = 186, nt="B" },
		{time = 159638, dur = 387, nt="A" },
		{time = 159998, dur = 187, nt="B" },
		{time = 160345, dur = 106, nt="D" },
		{time = 160518, dur = 106, nt="D" },
		{time = 160772, dur = 106, nt="D" },
		{time = 160946, dur = 119, nt="D" },
		{time = 161131, dur = 120, nt="D" },
		{time = 161318, dur = 132, nt="A" },
		{time = 161384, dur = 160, nt="B" },
		{time = 161531, dur = 133, nt="A" },
		{time = 161638, dur = 160, nt="B" },
		{time = 161757, dur = 174, nt="A" },
		{time = 161877, dur = 134, nt="B" },
		{time = 161997, dur = 134, nt="A" },
		{time = 162131, dur = 107, nt="D" },
		{time = 162370, dur = 67, nt="D" },
		{time = 162477, dur = 107, nt="D" },
		{time = 162651, dur = 80, nt="D" },
		{time = 162837, dur = 107, nt="D" },
		{time = 163010, dur = 147, nt="D" },
		{time = 163290, dur = 133, nt="B" },
		{time = 163477, dur = 120, nt="B" },
		{time = 163677, dur = 159, nt="B" },
		{time = 163836, dur = 120, nt="C" },
		{time = 164076, dur = 201, nt="A" },
		{time = 164343, dur = 106, nt="C" },
		{time = 164489, dur = 107, nt="C" },
		{time = 164742, dur = 94, nt="C" },
		{time = 164903, dur = 106, nt="C" },
		{time = 165103, dur = 186, nt="B" },
		{time = 165249, dur = 120, nt="A" },
		{time = 165343, dur = 133, nt="B" },
		{time = 165449, dur = 107, nt="A" },
		{time = 165530, dur = 132, nt="B" },
		{time = 165636, dur = 120, nt="A" },
		{time = 165716, dur = 147, nt="B" },
		{time = 165916, dur = 133, nt="B" },
		{time = 165836, dur = 253, nt="A" },
		{time = 166103, dur = 133, nt="D" },
		{time = 166396, dur = 93, nt="D" },
		{time = 166542, dur = 120, nt="D" },
		{time = 166755, dur = 94, nt="D" },
		{time = 166915, dur = 94, nt="D" },
		{time = 167102, dur = 120, nt="D" },
		{time = 167382, dur = 159, nt="A" },
		{time = 167581, dur = 120, nt="A" },
		{time = 167649, dur = 199, nt="B" },
		{time = 167848, dur = 160, nt="A" },
		{time = 167982, dur = 146, nt="B" },
		{time = 168088, dur = 147, nt="A" },
		{time = 168395, dur = 108, nt="C" },
		{time = 168541, dur = 94, nt="C" },
		{time = 168768, dur = 94, nt="C" },
		{time = 168928, dur = 120, nt="C" },
		{time = 169167, dur = 40, nt="A" },
		{time = 169128, dur = 107, nt="B" },
		{time = 169261, dur = 146, nt="A" },
		{time = 169355, dur = 146, nt="B" },
		{time = 169501, dur = 119, nt="A" },
		{time = 169568, dur = 132, nt="B" },
		{time = 169700, dur = 161, nt="A" },
		{time = 169781, dur = 160, nt="B" },
		{time = 169887, dur = 160, nt="A" },
		{time = 170061, dur = 439, nt="E" },
		{time = 170660, dur = 133, nt="E" },
		{time = 171060, dur = 214, nt="E" },
		{time = 171500, dur = 227, nt="E" },
		{time = 171967, dur = 2039, nt="E" },
		{time = 174193, dur = 119, nt="MUSIC_STOP" },



	}

}

return song
