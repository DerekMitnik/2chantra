_weapon_modules = {}

function AddWeaponModule( id, module )
	local module = module or {}
	module.name = module.name or "Unnamed Module"
	module.description = module.description or "This is a weapon module. Or something."
	module.req = module.req or {}
	module.inc = module.inc or {}
	module.type = module.type or "weapon"
	module.apply = module.apply or function() end
	module.cost = module.cost or 1
	module.energy_use = module.energy_use or 0
	_weapon_modules[id] = module
end

AddWeaponModule( "TEST", {} )
AddWeaponModule( "BASE_10", 
	{ name = "SFG 1000", description = "The most simple of all the upgradable weapons. Holds up to 10 wakabas of upgrades.",
	  type = "base", cost = 5 } 
	)
AddWeaponModule( "AMMO_ENERGY_DAMAGE10", 
	{ name = "Small energy amplifier", description = "Boosts a single energy projectile every shot, increasing its damage by 10.",
	  type = "energy ammo", cost = 4, apply = function(ammo) ammo.damage = ammo.damage + 10 end, energy_use = 3 } 
	)
AddWeaponModule( "WEAPON_SPLIT2", 
	{ name = "Second barrel", description = "Provides the means for shooting two projectiles at once instead of just one. Double the fun.",
	  type = "weapon split", cost = 2, apply = function(weapon) 
		local new_bullet = deep_copy(weapon.bullets[1])
		weapon.bullets[1].pos[2] = weapon.bullets[1].pos[2] - 20
		new_bullet.pos[2] = new_bullet.pos[2] + 20
		table.insert( weapon.bullet( new_bullet ) )
	end } )
AddWeaponModule( "WEAPON_SPREAD3", 
	{ name = "Second barrel", description = "Provides the means for shooting two projectiles at once instead of just one. Double the fun.",
	  type = "weapon split", cost = 2, apply = function(weapon) 
		local new_bullet = deep_copy(weapon.bullets[1])
		weapon.bullets[1].pos[2] = weapon.bullets[1].pos[2] - 20
		new_bullet.pos[2] = new_bullet.pos[2] + 20
		table.insert( weapon.bullet( new_bullet ) )
	end } )

function ShowWeaponShop( modules )
	mapvar.wakabas = mapvar.wakabas or 10
	push_pause( true )
	RemoveKeyProcessors()
	local l_window1 = ScrollingMenu.create( {CONFIG.scr_width/4, CONFIG.scr_height/2}, 0, 0, 30, 10, 32 )
	local l_window2 = SimpleMenu.create( {3*CONFIG.scr_width/4, CONFIG.scr_height/2}, 0, 0, 0, 0 )
	local l_window3 = ScrollingMenu.create( {CONFIG.scr_width/4, CONFIG.scr_height/2}, 0, 0, 30, 10, 32 )
	local l_window4 = SimpleMenu.create( {3*CONFIG.scr_width/4, CONFIG.scr_height/2}, 0, 0, 0, 0 )
	local l_window5 = SimpleMenu.create( {3*CONFIG.scr_width/4, CONFIG.scr_height/2}, -1, 0, 0, 0 )
	local w_desc = CreateWidget( constants.wt_Label, "DESCRIPTION", nil, 0, 0, CONFIG.scr_width*0.8 - 20, 100 )
	l_window1.min_size = { CONFIG.scr_width/4, CONFIG.scr_height/2 }
	l_window3.min_size = { CONFIG.scr_width/4, CONFIG.scr_height/2 }
	WidgetSetCaptionFont( w_desc, "dialogue" )
	WidgetSetCaptionColor( w_desc, { 1, 1, 1, 1 }, false )
	local mod
	local inv
	if not mapvar.inventory then mapvar.inventory = {} end
	local function inv_s()
		l_window3:clear()
		for k, v in pairs( mapvar.inventory ) do
			local num = k
			mod = _weapon_modules[k]
			if mod then
				l_window3:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
					mod.name .. " ("..v..") : "..mod.cost,
					0,
					function()
						mapvar.wakabas = (mapvar.wakabas or 0) + _weapon_modules[num].cost
						mapvar.inventory[ k ] = mapvar.inventory[ k ] - 1
						if mapvar.inventory[ k ] == 0 then
							mapvar.inventory[ k ] = nil
						end
						inv_s()
					end, 
					nil, 
					{name="wakaba-widget"},
					nil,
					function()
						WidgetSetPos( w_desc, l_window2.window.x + 10, l_window2.window.y + 10 )
						local desc = _weapon_modules[num].description
						if _weapon_modules[num].energy_use > 0 then
							desc = "/cffff00Energy use: ".._weapon_modules[num].energy_use.."/cffffff. " .. desc
						end
						WidgetSetCaption( w_desc, desc, true )
					end ))
			end			
		end
		l_window5:clear()
		l_window5:add( MWSpace.create( 5, 100 ) )
		l_window5:add( MWCaption.create( {name = "dialogue", color = {1,1,1,1}},
				"Wakabas: "..(mapvar.wakabas or 0),
				0 ))
		l_window5:add( MWSpace.create( 5, 30 ) )
	end
	inv_s()
	for k, v in pairs( modules ) do
		local num = v
		mod = _weapon_modules[ v ]
		inv = mapvar.inventory[ v ] or 0
		if mod then
			l_window1:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
				mod.name .. " : "..mod.cost,
				0,
				function()
					if mapvar.wakabas < _weapon_modules[ num ].cost then
						return
					end
					mapvar.wakabas = mapvar.wakabas - _weapon_modules[ num ].cost
					if not l_window2 or l_window2.appearing or not l_window2.window then
						return
					end
					mapvar.inventory[ v ] = ( mapvar.inventory[ v ] or 0 ) + 1
					inv_s()
				end, 
				nil, 
				{name="wakaba-widget"},
				nil,
				function()
					if l_window2.window then
						WidgetSetPos( w_desc, l_window2.window.x + 10, l_window2.window.y + 10 )
						local desc = _weapon_modules[num].description
						if _weapon_modules[num].energy_use > 0 then
							desc = "/cffff00Energy use: ".._weapon_modules[num].energy_use.."/cffffff. " .. desc
						end
						WidgetSetCaption( w_desc, desc, true )
					end
				end ))
		end
	end
	l_window2:add( MWSpace.create( 60, CONFIG.scr_width * 0.8) )
	l_window4:add( MWSpace.create( 5, 30 ) )
	l_window4:add( MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"CLOSE",
			0,
			function()
				pop_pause()
				RestoreKeyProcessors()
				MakeWindowDisappear( l_window1, {3*CONFIG.scr_width/4, 0-l_window1.window.h} )
				MakeWindowDisappear( l_window2, {CONFIG.scr_width/2, CONFIG.scr_height+30 } )
				MakeWindowDisappear( l_window3, {CONFIG.scr_width/4, 0-l_window3.window.h} )
				MakeWindowDisappear( l_window4, {CONFIG.scr_width/2, CONFIG.scr_height+20} )
				MakeWindowDisappear( l_window5, {l_window2.window.x, CONFIG.scr_height+20} )
				DestroyWidget( w_desc )
				ShowWeaponScreen( true, modules )
			end, 
			nil, 
			{name="wakaba-widget"}
		))
	l_window4:add( MWSpace.create( 5, 30 ) )
	MakeWindowAppear( l_window1, {3*CONFIG.scr_width/4, -2*CONFIG.scr_height}, {3*CONFIG.scr_width/4, CONFIG.scr_height/2} )
	MakeWindowAppear( l_window2, {CONFIG.scr_width/2, 2*CONFIG.scr_height}, {CONFIG.scr_width/2, CONFIG.scr_height-30} )
	MakeWindowAppear( l_window3, {CONFIG.scr_width/4, -2*CONFIG.scr_height}, {CONFIG.scr_width/4, CONFIG.scr_height/2} )
	MakeWindowAppear( l_window4, {CONFIG.scr_width/2, 2*CONFIG.scr_height}, {CONFIG.scr_width/2, CONFIG.scr_height-70} )
	MakeWindowAppear( l_window5, {l_window2.window.x, 2*CONFIG.scr_height}, {l_window2.window.x, CONFIG.scr_height-70} )
end

function UpgradeScreen( weapon, shop, modules )
	push_pause( true )
	RemoveKeyProcessors()
	if weapon.constructor then
		weapon.in_progess = deep_copy( weapon.constructor )
	else
		weapon.in_progress = { space = 0, space_used = 0, modules_used = {}, ammo_use = 0 }
	end
	local l_window1 = SimpleMenu.create( {3*CONFIG.scr_width/4, CONFIG.scr_height/2}, 0, 0, 30, 15 )
	local l_window2 = SimpleMenu.create( {3*CONFIG.scr_width/4, CONFIG.scr_height/2}, 0, 0, 0, 0 )
	local l_window3 = ScrollingMenu.create( {CONFIG.scr_width/4, CONFIG.scr_height/2}, 0, 0, 30, 10, 32 )
	local l_window4 = SimpleMenu.create( {3*CONFIG.scr_width/4, CONFIG.scr_height/2}, 0, 0, 0, 0 )
	l_window3.min_size = { CONFIG.scr_width/4, CONFIG.scr_height/2 }
	l_window1.min_size = { CONFIG.scr_width/8, CONFIG.scr_height/8 }
	local w_desc = CreateWidget( constants.wt_Label, "DESCRIPTION", nil, 0, 0, CONFIG.scr_width*0.8 - 20, 100 )
	WidgetSetCaptionFont( w_desc, "dialogue" )
	WidgetSetCaptionColor( w_desc, { 1, 1, 1, 1 }, false )
	_weapon_modules.apply = function( module ) end
	inv_s = function( tp )
		l_window3:clear()
		for k, v in pairs( mapvar.inventory or {} ) do
			local num = k
			mod = _weapon_modules[k]
			if mod and mod.type == tp then
				l_window3:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
					mod.name .. " ("..v..")",
					0,
					function()
						if weapon.in_progress.space - weapon.in_progress.space_used < _weapon_modules[k].cost then
							return
						end
						mapvar.inventory[ k ] = mapvar.inventory[ k ] - 1
						if mapvar.inventory[ k ] == 0 then
							mapvar.inventory[ k ] = nil
						end
						inv_s( tp )
						_weapon_modules.apply( _weapon_modules[k] )
					end, 
					nil, 
					{name="wakaba-widget"},
					nil,
					function()
						WidgetSetPos( w_desc, l_window2.window.x + 10, l_window2.window.y + 10 )
						WidgetSetCaption( w_desc, _weapon_modules[num].description, true )
					end ))
			end			
		end
	end
	l_window2:add( MWSpace.create( 60, CONFIG.scr_width * 0.8) )
	l_window4:add( MWSpace.create( 5, 30 ) )
	l_window4:add( MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"CLOSE",
			0,
			function()
				pop_pause()
				RestoreKeyProcessors()
				DestroyWidget( w_desc )
				MakeWindowDisappear( l_window2, {CONFIG.scr_width/2, CONFIG.scr_height+30 } )
				MakeWindowDisappear( l_window3, {CONFIG.scr_width/4, 0-l_window3.window.h} )
				MakeWindowDisappear( l_window4, {CONFIG.scr_width/2, CONFIG.scr_height+20} )
				ShowWeaponScreen( shop, modules )
			end, 
			nil, 
			{name="wakaba-widget"}
		))
	l_window4:add( MWSpace.create( 5, 30 ) )
	local active_butt
	l_window1:add( MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"BASE",
			0,
			function( sender )
				if active_butt then
					WidgetSetCaptionColor( active_butt, {1,1,1,1}, false )
				end
				active_butt = sender
				WidgetSetCaptionColor( active_butt, {.5,.7,1,1}, false )
				inv_s( "base" )
				_weapon_modules.apply = function( module )
					for k, v in pairs( weapon.in_progress.modules_used ) do
						mapvar.player_info[pl].inventory[ v ] = ( mapvar.player_info[pl].inventory[ v ] or 0 ) + 1
						weapon.in_progress.space_used = 0
						weapon.in_progress.space = module.space
						weapon.in_progress.ammo_use = 0
					end
					inv_s( "base" )
				end
			end, 
			nil, 
			{name="wakaba-widget"}
		))
	l_window1:add( MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"WEAPON",
			0,
			function( sender )
				if active_butt then
					WidgetSetCaptionColor( active_butt, {1,1,1,1}, false )
				end
				active_butt = sender
				WidgetSetCaptionColor( active_butt, {.5,.7,1,1}, false )
				inv_s( "weapon" )
				_weapon_modules.apply = function( module )
					weapon.in_progress.space_used = weapon.in_progress.space_used + module.cost
					weapon.in_progress.ammo_use = weapon.in_progress.ammo_use + module.ammo_use
				end
			end, 
			nil, 
			{name="wakaba-widget"}
		))
	l_window1:add( MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"AMMO",
			0,
			function( sender )
				if active_butt then
					WidgetSetCaptionColor( active_butt, {1,1,1,1}, false )
				end
				active_butt = sender
				WidgetSetCaptionColor( active_butt, {.5,.7,1,1}, false )
				inv_s( "shot" )
			end, 
			nil, 
			{name="wakaba-widget"}
		))
	MakeWindowAppear( l_window1, {3*CONFIG.scr_width/4, -2*CONFIG.scr_height}, {3*CONFIG.scr_width/4, CONFIG.scr_height/4} )
	MakeWindowAppear( l_window2, {CONFIG.scr_width/2, 2*CONFIG.scr_height}, {CONFIG.scr_width/2, CONFIG.scr_height-30} )
	MakeWindowAppear( l_window3, {CONFIG.scr_width/4, -2*CONFIG.scr_height}, {CONFIG.scr_width/4, CONFIG.scr_height/2} )
	MakeWindowAppear( l_window4, {CONFIG.scr_width/2, 2*CONFIG.scr_height}, {CONFIG.scr_width/2, CONFIG.scr_height-70} )
end

function ShowUpgradeScreen( pl, shop, shop_items )
	push_pause( true )
	RemoveKeyProcessors()
	local l_window = SimpleMenu.create( {CONFIG.scr_width/2, CONFIG.scr_height/2}, 0, 0, 30, 10 )
	l_window:add( MWSpace.create( 15, 15 ) )
	for i=2, 4 do
		if mapvar.player_info[pl].weapons[i] then
			l_window:add( MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
				mapvar.player_info[pl].weapons[i].param.set_name or "CUSTOM WEAPON",
				0,
				function()
					MakeWindowDisappear( l_window, { CONFIG.scr_width/2, CONFIG.scr_height + l_window.window.h } )
					pop_pause()
					RestoreKeyProcessors()
					UpgradeScreen( mapvar.player_info[pl].weapons[i].param, shop, shop_items )
				end, 
				nil, 
				{name="wakaba-widget"} ))
		end
	end
	l_window:add( MWSpace.create( 5, 15 ) )
	l_window:add( MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
				"BACK",
				0,
				function()
					MakeWindowDisappear( l_window, { CONFIG.scr_width/2, CONFIG.scr_height + l_window.window.h } )
					pop_pause()
					RestoreKeyProcessors()
					return ShowWeaponScreen( shop, shop_items )
				end, 
				nil, 
				{name="wakaba-widget"} ))
	l_window:add( MWSpace.create( 15, 15 ) )
	MakeWindowAppear( l_window, {CONFIG.scr_width/2, -CONFIG.scr_height}, {CONFIG.scr_width/2, CONFIG.scr_height/2} )
end

function ShowWeaponScreen( shop_enabled, shop_modules )
	push_pause( true )
	local sm = shop_modules or {}
	RemoveKeyProcessors()
	local l_window = SimpleMenu.create( {CONFIG.scr_width/2, -CONFIG.scr_height}, 0, 0, 30, 10 )
	l_window:add( MWSpace.create( 15, 15 ) )
	if shop_enabled then
		l_window:add( MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"BUY WEAPON MODULES",
			0,
			function()
				MakeWindowDisappear( l_window, { CONFIG.scr_width/2, CONFIG.scr_height + l_window.window.h } )
				pop_pause()
				RestoreKeyProcessors()
				ShowWeaponShop( sm )
			end, 
			nil, 
			{name="wakaba-widget"} ))
	else
		l_window:add( MWCaption.create( {name="default", color={.5,.5,.5,1}}, "BUY WEAPON MODULES", 0 ) )
	end
	l_window:add( MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"UPGRADE WEAPONS",
			0,
			function()
				MakeWindowDisappear( l_window, { CONFIG.scr_width/2, CONFIG.scr_height + l_window.window.h } )
				pop_pause()
				RestoreKeyProcessors()
				ShowUpgradeScreen(1, shop_enabled, shop_modules)
			end, 
			nil, 
			{name="wakaba-widget"} ))
	l_window:add( MWSpace.create( 15, 5 ) )
	l_window:add( MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"CLOSE",
			0,
			function()
				MakeWindowDisappear( l_window, { CONFIG.scr_width/2, CONFIG.scr_height + l_window.window.h } )
				pop_pause()
				RestoreKeyProcessors()
			end, 
			nil, 
			{name="wakaba-widget"} ))
	l_window:add( MWSpace.create( 15, 15 ) )
	
	MakeWindowAppear( l_window, {CONFIG.scr_width/2, -CONFIG.scr_height}, {CONFIG.scr_width/2, CONFIG.scr_height/2} )
end

