LangChanger = {}

LangChanger.langChangeProcs = {}

function LangChanger.LoadLanguage()
	local file
	if string.match( constants.__BUILD_TYPE, "deb" ) then
		file = string.format( "config/languages/%s.lua", (CONFIG.language or "russian") )
	else
		file = build_relative_path( string.format( "%slanguages/%s.lua", constants.path_config, (CONFIG.language or "russian") ) )
	end
	local sbox = {}
	local test = io.open( file, "r" )
	if test then
		test:close()
		local lfile = loadfile( file )
		if type(lfile) == 'function' then
			setfenv( lfile, sbox )
			pcall( lfile )
			if type( sbox.language ) == 'string' then
				language = sbox.language
			end
			dictionary = deep_copy( sbox.dictionary )
			for k,v in pairs(LangChanger.langChangeProcs) do
				v:onLangChange()
			end
		end
	end
end

function LangChanger.Register(who)
	--Log("LangChanger.Register ", who)
	if not who then 
		Log(debug.traceback())
	end
	if who.onLangChange and type(who.onLangChange) == "function" then
		LangChanger.langChangeProcs[who] = who
	end
end

function LangChanger.Unregister(who)
	--Log("LangChanger.Unregister ", who)
	LangChanger.langChangeProcs[who] = nil
end

