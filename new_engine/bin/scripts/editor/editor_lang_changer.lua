LangChanger = {}

LangChanger.langChangeProcs = {}

function LangChanger.LoadLanguage()
	--Log("LoadLanguage() ", CONFIG.language)
	local file = string.format( "../config/languages/%s.lua", (CONFIG.language or "english") )
	local sbox = {}
	local test = io.open( file, "r" )
	if test then
		test:close()
		local lfile = loadfile( file )
		if type(lfile) == 'function' then
			setfenv( lfile, sbox )
			pcall( lfile )
			if type( sbox.language ) == 'string' then
				language = sbox.language
			end
			dictionary = deep_copy( sbox.dictionary )
			for k,v in pairs(LangChanger.langChangeProcs) do
				v:onLangChange()
			end
		end
	end
end

function LangChanger.Register(who)
	--Log("LangChanger.Register ", who)
	if not who then 
		Log(debug.traceback())
	end
	if who.onLangChange and type(who.onLangChange) == "function" then
		LangChanger.langChangeProcs[who] = who
	end
end

function LangChanger.Unregister(who)
	--Log("LangChanger.Unregister ", who)
	LangChanger.langChangeProcs[who] = nil
end

