Editor.saveAsMenu = function()
	Editor.changeState( Editor.STATE.ADDITIONAL_MENU )
	Editor.popup_enter( "Enter map name:", function( str )
			str = str or "map"
			local file = io.open("levels/"..".lua")
			if file then
				file:close()
				Editor.confirm("File already exists. Do you want to overwrite it?", function() 
					Editor.current_map = str
					local warnings = Editor.save(str) 
					if #warnings > 0 then
						Editor.longPopup{ title="A FRIENDLY REMINDER", message=warnings, event=function() Editor.changeState(Editor.STATE.RUNNING) end }
					else
						Editor.changeState(Editor.STATE.RUNNING)
					end
				end, Editor.saveAsMenu)
			else
				Editor.current_map = str
				local warnings = Editor.save(str)
				if #warnings > 0 then
					Editor.longPopup{ title="A FRIENDLY REMINDER", message=warnings, event=function() Editor.changeState( Editor.STATE.RUNNING ) end }
				else
					Editor.changeState(Editor.STATE.RUNNING)
				end
			end
		end,
		function()
			 Editor.changeState( Editor.STATE.RUNNING )
		end)
end

Editor.saveMenu = function()
	if Editor.current_map then
		Editor.changeState( Editor.STATE.ADDITIONAL_MENU )
		local warnings = Editor.save(Editor.current_map)
		if #warnings > 0 then
			Editor.longPopup{ title="A FRIENDLY REMINDER", message=warnings, event=function() Editor.changeState( Editor.STATE.RUNNING ) end }
		else
			Editor.popup("Map saved", function() Editor.changeState( Editor.STATE.RUNNING ) end)
		end
	else
		Editor.saveAsMenu()
	end
end

Editor.openMenu = function()
	Editor.changeState( Editor.STATE.ADDITIONAL_MENU )
	Editor.confirm( "Are you sure you want to close this map and open another one?", 
		function()
			Editor.path = "scripts/levelscripts"
			RemoveKeyProcessors()
			Editor.show_dir( "scripts/levelscripts", function( filename )
					local file = io.open(filename, "r")
					local first_line = file:read()
					file:close()
					if first_line and string.match( first_line, "EDITABLE MAP") then
						return true
					end
					return false
				end,
				function(a) return string.gsub(a, "%.lua$", "") end, function(filename)
				RestoreKeyProcessors()
				Editor.current_map = string.gsub(string.gsub(filename, "^scripts/levelscripts/", ""), "%.lua$", "")
				Editor.open(Editor.current_map)
				Editor.changeState( Editor.STATE.RUNNING )
			end, function() RestoreKeyProcessors() Editor.changeState( Editor.STATE.RUNNING ) end )
		end,
		function()
			Editor.changeState( Editor.STATE.RUNNING )
		end)
end


function Editor.open( filename )
	StopBackMusic()
	Editor.editor_state = "map_editor"
	EditorSetState(Editor.editor_state)
	local _PlayBackMusic = PlayBackMusic
	function PlayBackMusic( track )
		Editor.mus_track = track
	end
	Editor.InitScripts()
	Editor.init()
	local success, result = Loader.startGame( filename )
	Console.addMessage( result )
	if not success then Log(result) return end
	--Menu.hideMainMenu()
	--GUI:hide()
	local player = GetPlayer() or { aabb = { p = {x = 0, y = 0} } }
	if player then
		Editor.x, Editor.y = player.aabb.p.x, player.aabb.p.y
	end
	SetCamAttachedObj(0)
	SetCamUseBounds( false )
	CamMoveToPos( Editor.x, Editor.y )
	Editor.init_script = ""
	Editor.map_script = ""
	Editor.extractScripts( filename )
	Resume( NewMapThread( Editor._main_thread ) )
	Editor.preprocessMap()
	PlayBackMusic = _PlayBackMusic
end

function Editor.startMap( filename )
	Editor.InitScripts()
	Editor.init()
	
	Editor.editor_state = "off"
	EditorSetState(Editor.editor_state)
	--SetCamObjOffset = function() end
	_SetCamObjOffset = function() end
	Menu = {}
	Menu.lock = function() end
	Menu.unlock = function() end
	Game.ShowLevelResults = function() Editor.open( Editor.current_map ) end
	Menu.showMainMenu = function() Editor.open( Editor.current_map ) end
	
	local success, result = Loader.startGame( filename )
	Console.addMessage( result )
	if not success then Log(result) return end
	--Menu.hideMainMenu()
	--GUI:hide()
	local player = GetPlayer() or { aabb = { p = {x = 0, y = 0} } }
	if player then
		Editor.x, Editor.y = player.aabb.p.x, player.aabb.p.y
	end
	Editor.init_script = ""
	Editor.map_script = ""
	Editor.extractScripts( filename )
	Resume( NewMapThread( Editor._main_thread ) )
	--Editor.preprocessMap()
end

function Editor.new()
	InitNewGame()
	Editor.init()
	--Menu.hideMainMenu()
	--GUI:hide()
	SetCamUseBounds( false )
	Editor.DefaultScripts()
	CamMoveToPos( Editor.x, Editor.y )
	Resume( NewMapThread( Editor._main_thread ) )
end

function Editor.preprocessMap()
	local _CreateEnemy = CreateEnemy
	local _CreateWaypoint = CreateWaypoint
	local _CreateItem = CreateItem
	local _CreatePlayer = CreatePlayer
	local _CreateSprite = CreateSprite
	local _SetEnemyWaypoint = SetEnemyWaypoint
	local _SetNextWaypoint = SetNextWaypoint
	local _ObjectPushInt = ObjectPushInt
	local mini_id = 1
	
	Editor.specials_map = {}

	CreateEnemy = function( ... )
		local obj = _CreateEnemy( unpack(arg) )
		if not obj then return end
		Editor.setObjParam( obj, "SPECIAL_CREATION", true )
		Editor.specials_map[obj] = mini_id
		mini_id = mini_id + 1
		return obj
	end
	CreateWaypoint = function( ... )
		local obj = _CreateWaypoint( unpack(arg) )
		if not obj then return end
		Editor.setObjParam( obj, "SPECIAL_CREATION", true )
		Editor.specials_map[obj] = mini_id
		mini_id = mini_id + 1
		return obj
	end
	CreateItem = function ( ... )
		local obj = _CreateItem( unpack(arg) )
		if not obj then return end
		Editor.setObjParam( obj, "SPECIAL_CREATION", true )
		Editor.specials_map[obj] = mini_id
		mini_id = mini_id + 1
		return obj
	end
	CreatePlayer = function( ... )
		local obj = _CreatePlayer( unpack(arg) )
		if not obj then return end
		Editor.setObjParam( obj, "SPECIAL_CREATION", true )
		Editor.specials_map[obj] = mini_id
		mini_id = mini_id + 1
		return obj
	end
	CreateSprite = function( ... )
		local obj = _CreateSprite( unpack(arg) )
		if not obj then return end
		Editor.setObjParam( obj, "SPECIAL_CREATION", true )
		Editor.specials_map[obj] = mini_id
		mini_id = mini_id + 1
		return obj
	end
	SetNextWaypoint = function( id1, id2 )
		Editor.setObjParam( id1, "WAYPOINT", id2 )
		EditorSetLink( id1, id2 )
		_SetNextWaypoint( id1, id2 )
	end
	SetEnemyWaypoint = function( id1, id2 )
		Editor.setObjParam( id1, "WAYPOINT", id2 )
		EditorSetLink( id1, id2 )
		_SetEnemyWaypoint( id1, id2 )
	end
	ObjectPushInt = function( id, value, ... )
		local param = Editor.getObjParam( id, "STACK" ) or {}
		table.insert( param, value )
		Editor.setObjParam( id, "STACK", param )
		_ObjectPushInt( id, value, unpack(arg) )
	end

	Loader.level.SpecialCreation()
	Editor.setCharacters( Loader.level.characters )
	Editor.createSpawnpoints( Loader.level.spawnpoints )

	SetEnemyWaypoint = _SetEnemyWaypoint
	SetNextWaypoint = _SetNextWaypoint
	CreateEnemy = _CreateEnemy
	CreateWaypoint = _CreateWaypoint
	CreateItem = _CreateItem
	CreatePlayer = _CreatePlayer
	CreateSprite = _CreateSprite

	local map_data = EditorDumpMap()
	for k, v in pairs( map_data ) do
		Editor.prepareObject( v )
	end
end

function Editor.specialCreation( obj )
	local ret = {}
	local id2k = {}
	local id = 1
	local k, v
	for k, v in pairs( obj ) do
		id2k[v.id] = id
		if v.type == Editor.TYPES.OBJ_ENEMY then
			table.insert( ret, string.format("local obj%i = CreateEnemy( '%s', %i, %i )", id, v.proto, v.aabb.p.x, v.aabb.p.y) )
			table.insert( ret, string.format("SetObjSpriteMirrored( obj%i, %s )", id, tostring(v.sprite.mirrored)) )
			id = id + 1
		elseif v.type == Editor.TYPES.OBJ_WAYPOINT then
			table.insert( ret, string.format("local obj%i = CreateWaypoint( %i, %i, %i, %i )", id, v.aabb.p.x, v.aabb.p.y, 2*v.aabb.W, 2*v.aabb.H) )
			id = id + 1
		elseif v.type == Editor.TYPES.OBJ_ITEM then
			if v.group then
				table.insert( ret, string.format("local obj%i = CreateItem( '%s', %i, %i )", id, v.proto,  v.aabb.p.x-v.aabb.W, v.aabb.p.y-v.aabb.H) )
				id = id + 1
				table.insert( ret, string.format("local obj%i = CreateItem( '%s', %i, %i )", id, v.proto,  v.aabb.p.x+v.aabb.W, v.aabb.p.y+v.aabb.H) )
				id = id + 1
				table.insert( ret, string.format("GroupObjects( obj%i, obj%i )", id-2, id-1) )
			else
				table.insert( ret, string.format("local obj%i = CreateItem( '%s', %i, %i )", id, v.proto,  v.aabb.p.x, v.aabb.p.y) )
				id = id + 1
			end
		elseif v.type == Editor.TYPES.OBJ_SPRITE then
			if v.group then
				table.insert( ret, string.format("local obj%i = CreateSprite( '%s', %i, %i )", id, v.proto,  v.aabb.p.x-v.aabb.W, v.aabb.p.y-v.aabb.H) )
				id = id + 1
				table.insert( ret, string.format("local obj%i = CreateSprite( '%s', %i, %i )", id, v.proto,  v.aabb.p.x+v.aabb.W, v.aabb.p.y+v.aabb.H) )
				id = id + 1
				table.insert( ret, string.format("GroupObjects( obj%i, obj%i )", id-2, id-1) )
			else
				table.insert( ret, string.format("local obj%i = CreateSprite( '%s', %i, %i )", id, v.proto,  v.aabb.p.x, v.aabb.p.y) )
				id = id + 1
			end
		elseif v.type == Editor.TYPES.OBJ_PLAYER then
			if ( #Editor.characters == 0 ) then
				table.insert( Editor.characters, v.proto );
			end
			table.insert( Editor.spawnpoints, { v.aabb.p.x, v.aabb.p.y  } )
		elseif v.type == Editor.TYPES.OBJ_TILE then
			table.insert( ret, string.format("local obj%i = CreateSprite( '%s', %i, %i )", id, v.proto, v.aabb.p.x, v.aabb.p.y) )
			table.insert( ret, string.format("SetObjAnim( obj%i, %i, false )", id, v.sprite.frame ) )
			table.insert( ret, string.format("SetObjPos( obj%i, %i, %i, %i )", id, v.aabb.p.x, v.aabb.p.y, v.sprite.z ) )
			id = id + 1
		end
	end
	table.insert( ret, "local object" )
	for i=1, #obj do
		k = id2k[ obj[i].id ]
		v = obj[i]
		if Editor.getObjParam( v.id, "WAYPOINT" ) then
			if v.type == Editor.TYPES.OBJ_ENEMY then
				table.insert( ret, string.format( "SetEnemyWaypoint( obj%i, obj%i )", k, id2k[Editor.getObjParam( v.id, "WAYPOINT" )] ))
			elseif v.type == Editor.TYPES.OBJ_WAYPOINT then
				table.insert( ret, string.format( "SetNextWaypoint( obj%i, obj%i )", k, id2k[Editor.getObjParam( v.id, "WAYPOINT" )] ))
			end
		end
		if Editor.getObjParam( v.id, "STACK" ) then
			local stack = Editor.getObjParam( v.id, "STACK" )
			for i=1,#stack do
				table.insert( ret, string.format( "ObjectPushInt( obj%i, %i )", k, stack[i] ))
			end
		end
		if Editor.getObjParam( v.id, "CREATION_SCRIPT" ) then
			table.insert( ret, string.format("object = obj%i", k ) )
			local script = Editor.getObjParam( v.id, "CREATION_SCRIPT" )
			script = string.gsub( script, "\n$", "" )
			script = string.gsub( script, "\n\t", "\n" )
			script = string.gsub( script, "^\t", "" )
			table.insert( ret, "if not Editor then" )
			table.insert( ret, string.format("--$(OBJ%i_CREATION_SCRIPT)+", k ) )
			table.insert( ret, script )
			table.insert( ret, string.format("--$(OBJ%i_CREATION_SCRIPT)-", k ) )
			table.insert( ret, "end" )
		end
	end
	return "\t"..table.concat( ret, "\n\t" ).."\n" 
end

function Editor.save( filename )
	local warnings = {}
	local phys_stat = false
	local player_obj = false
	local map_data = EditorDumpMap()
	local out = io.open( "levels/"..filename..".lua", "w" )
	local specials = {}
	local line
	out:write("CreateMap({\n")
	for key, o in pairs( map_data ) do
		if Editor.getObjParam( o.id, "SPECIAL_CREATION" ) then
			table.insert( specials, o )
		end
		if not Editor.ObjInVoid( o.id ) and not Editor.getObjParam( o.id, "DONT_SAVE" )
		and not Editor.getObjParam( o.id, "SPECIAL_CREATION" ) then
			line = nil
			if o.flags.physic and not o.phFlags.dynamic then
				phys_stat = true
			end
			if o.type == Editor.TYPES.OBJ_PLAYER then
--				player_obj = true
--				line = string.format('\t{constants.ObjPlayer, "%s", %i, %i},\n', o.proto, o.aabb.p.x, o.aabb.p.y)
			elseif o.type == Editor.TYPES.OBJ_SPRITE then
				if o.polygon then
					local poly = o.polygon
					local poly_st = { "{" }
					for i=1, #poly do
						table.insert( poly_st, "{" )
						table.insert( poly_st, poly[i][1] )
						table.insert( poly_st, "," )
						table.insert( poly_st, poly[i][2] )
						table.insert( poly_st, "}," )
					end
					table.insert( poly_st, "}" )
					poly_st = table.concat( poly_st );
					line = string.format('\t{constants.ObjPolygon, %s, %i, %i, one_sided=%s, solid_to=%i},\n', poly_st, o.aabb.p.x, o.aabb.p.y, tostring(o.phFlags.oneSided), o.solid_to )
				elseif o.group then
					line = string.format('\t{constants.ObjGroup, "%s", %i, %i, %i, %i, solid_to=%i},\n', o.proto, o.aabb.p.x-o.aabb.W, o.aabb.p.y-o.aabb.H, o.aabb.p.x+o.aabb.W, o.aabb.p.y+o.aabb.H, o.solid_to) 
				elseif not o.proto then
					line = string.format('\t{constants.ObjBox, %i, %i, %i, %i, %f, { %f, %f, %f, %f }},\n', o.aabb.p.x-o.aabb.W, o.aabb.p.y-o.aabb.H, o.aabb.p.x+o.aabb.W, o.aabb.p.y+o.aabb.H, o.sprite.z, o.sprite.color[1], o.sprite.color[2], o.sprite.color[3], o.sprite.color[4]) 
				else
					if o.sprite and ( o.sprite.cur_anim ~= "idle" ) then
						line = string.format('\t{constants.ObjSprite, "%s", %i, %i, %f, "%s", solid_to=%i},\n', o.proto, o.aabb.p.x, o.aabb.p.y, o.sprite.z, o.sprite.cur_anim, o.solid_to)
					else
						line = string.format('\t{constants.ObjSprite, "%s", %i, %i, %f, solid_to=%i},\n', o.proto, o.aabb.p.x, o.aabb.p.y, o.sprite.z, o.solid_to)
					end
				end
			elseif o.type == Editor.TYPES.OBJ_ENEMY then
				line = string.format('\t{constants.ObjEnemy, "%s", %i, %i, %f, solid_to=%i},\n', o.proto, o.aabb.p.x, o.aabb.p.y, (o.sprite.z or 0), o.solid_to)
			elseif o.type == Editor.TYPES.OBJ_ITEM then
				line = string.format('\t{constants.ObjItem, "%s", %i, %i, solid_to=%i},\n', o.proto, o.aabb.p.x, o.aabb.p.y, o.solid_to)
			elseif o.type == Editor.TYPES.OBJ_SPAWNER then
				local proto = Editor.getObjParam( o.id, "PROP_OBJ" )		
				local count = Editor.getObjParam( o.id, "PROP_COUNT")
				local dir =   Editor.getObjParam( o.id, "PROP_DIR" )
				local delay = Editor.getObjParam( o.id, "PROP_DELAY")
				local size =  Editor.getObjParam( o.id, "PROP_SIZE")
				local dist =  Editor.getObjParam( o.id, "PROP_RESET_DIST")
				local rdelay =  Editor.getObjParam( o.id, "RESPAWN_DELAY")
				line = string.format('\t{constants.ObjSpawner, "%s", %i, %i, %i, %i, %i, %i, %i, respawn_delay = %i},\n', proto, o.aabb.p.x, o.aabb.p.y, count, delay, dir, size, dist, rdelay)
			elseif o.type == Editor.TYPES.OBJ_TILE then
				line = string.format('\t{constants.ObjTile, "%s", %i, %i, %i, %f},\n', o.proto, o.aabb.p.x, o.aabb.p.y, o.sprite.frame, o.sprite.z)
			elseif o.type == Editor.TYPES.OBJ_RIBBON then
				local s1 = iff( o.ubl, tostring(o.bl), "nil" )
				local s2 = iff( o.ubt, tostring(o.bt), "nil" )
				local s3 = iff( o.ubr, tostring(o.br), "nil" )
				local s4 = iff( o.ubb, tostring(o.bb), "nil" )
				line = string.format('\t{constants.ObjRibbon, "%s", %f, %f, %i, %i, %f, %s, %s, %s, %s, %s, %s, %s},\n', o.proto, o.k.x, o.k.y, o.aabb.p.x, o.aabb.p.y, o.sprite.z, tostring(o.from_proto), s1, s2, s3, s4, tostring(o.repeat_x), tostring(o.repeat_y))
			end
			if line then
				if o.sprite and o.sprite.mirrored then
					line = string.gsub( line, "},\n", ", mirrored=true},\n" )
				end
				out:write( line )
			end
		end
	end
	out:write("}, "..Editor.MAP_FORMAT_VERSION..")\n")
	out:close()
	local out = io.open( "scripts/levelscripts/"..filename..".lua", "w" )
	out:write("--EDITABLE MAP\n\n")
	out:write("local LEVEL = {}\n")
	out:write("InitNewGame()\n")
	out:write(string.format('dofile("levels/%s.lua")\n\n', filename))
	out:write(string.format("CONFIG.backcolor_r = %f\n", CONFIG.backcolor_r))
	out:write(string.format("CONFIG.backcolor_g = %f\n", CONFIG.backcolor_g))
	out:write(string.format("CONFIG.backcolor_b = %f\n", CONFIG.backcolor_b))
	out:write("LoadConfig()\n\n")
	if Editor.mus_track then
		out:write(string.format('PlayBackMusic("%s")\n\n', Editor.mus_track))
	else
		out:write("StopBackMusic()\n\n")
	end
	out:write(string.format("LEVEL.secrets = %d\n\n", 0))
	out:write(string.format("function LEVEL.InitScript()\n--$(INIT_SCRIPT)+\n%s--$(INIT_SCRIPT)-\n\tLoader.level.SpecialCreation()\nend\n\n", Editor.scripts.init_script))
	out:write(string.format("function LEVEL.MapScript(player)\n--$(MAP_SCRIPT)+\n%s--$(MAP_SCRIPT)-\nend\n\n", Editor.scripts.map_script))
	out:write(string.format("function LEVEL.WeaponBonus()\n--$(WEAPON_BONUS)+\n%s--$(WEAPON_BONUS)-\nend\n\n", Editor.scripts.weapon_bonus))
	out:write(string.format("function LEVEL.placeSecrets()\n--$(PLACE_SECRETS)+\n%s--$(PLACE_SECRETS)-\nend\n\n", Editor.scripts.place_secrets))
	out:write(string.format("function LEVEL.removeSecrets()\n--$(REMOVE_SECRETS)+\n%s--$(REMOVE_SECRETS)-\nend\n\n", Editor.scripts.remove_secrets))
	out:write(string.format("function LEVEL.SpecialCreation()\n--$(SPECIAL_CREATION)+\n%s--$(SPECIAL_CREATION)-\nend\n\n", Editor.specialCreation(specials)))
	--out:write(string.format("function LEVEL.GetNextCharacter()\n--$(GET_NEXT_CHARACTER)+\n%s--$(GET_NEXT_CHARACTER)-\nend\n\n", Editor.scripts.get_next_character or ""))
	out:write(string.format("function map_trigger(id, trigger)\n--$(MAP_TRIGGER)+\n%s--$(MAP_TRIGGER)-\nend\n\n", Editor.scripts.map_trigger))
	out:write(string.format("%s\n", Editor.spawnPoints()));
	out:write("function LEVEL.SetLoader(l)\n\tLEVEL.missionStarter=l\nend\n\n")
	out:write(string.format("LEVEL.name = '%s'\n\n", filename))
	out:write("return LEVEL\n")
	out:close()
	local out = io.open( "scripts/levelscripts/"..filename..".info", "w" )
	out:write("name = "..filename.."\n")
	out:close()
	--[[if not player_obj then
		table.insert( warnings, "There are no player characters on your map." )
	end]]
	if not phys_stat then
		table.insert( warnings, "There are no static physic objects on the map (like 'phys-empty')." )
	end
	return warnings
end

function Editor.ImportTiles()
	--function Editor.show_dir( root_dir, filter, preprocess, event, cancel_event, preview )
	Editor.path = "textures"
	Editor.show_dir( "textures", 
		function(a)
			if string.match( a, "%.png$" ) then
				local tst = io.open( string.gsub(a, "%.png$", "%.lua"), "r" )
				if tst then
					tst:close()
					return false
				end
				return true
			else
				return false
			end
		end,
		function (a)
			return string.gsub( a, "%.png", "" )
		end,
		function ( filename )
			Editor.valuesPopup( "IMPORT PARAMETERS", nil, { {"tile width", "FLOAT", 64}, {"tile height", "FLOAT", 64}, {"tiles by x", "INT", 1}, {"tiles by y", "INT", 1}, {"default z", "FLOAT", 0.0 } },
				function(p)
					local w = p["tile width"] or 64
					local h = p["tile height"] or 64
					local cx = p["tiles by x"] or 1
					local cy = p["tiles by y"] or 1
					local z = p["default z"] or 0
					local out = io.open( string.gsub(filename, "%.png", "%.lua"), "r" )
					local function complete()
						if out then
							out:close()
						end
						out = io.open( string.gsub(filename, "%.png", "%.lua"), "w" )
						--out:write( "count = "..cx*cy.."\n" )
						out:write( "main = {\n" )
						for x = 0, cx-1 do
							for y = 0, cy-1 do
								out:write( string.format("\t{ x = %i, y = %i, w = %i, h = %i },\n", x*w, y*h, w, h) )
							end
						end
						out:write( "}\n" )
						out:close()
						local name = string.match(filename, "([^/]+)%.png")
						out = io.open( "proto/sprites/"..name..".lua", "w" )
						out:write( "--$(DESCRIPTION).This tileset was automatically generated by the map editor 'import tiles' function.\n\n" )
						out:write( "texture = \""..name.."\"\n")
						out:write( "z = \""..z.."\"\n")
						out:close()
						Editor.SelectObject( name, "TILE", "proto/sprites/"..name..".lua" )
						Editor.changeState( Editor.STATE.RUNNING )
					end
					if out then
						Editor.confirm( "It seems like "..filename.." already has a description. Do you want to overwrite it?", complete, Editor.ImportTiles )
					else
						complete()
					end
				end,
				function()
					Editor.ImportTiles()
				end
			)
		end,
		function()
			Editor.changeState( Editor.STATE.RUNNING )
		end)
end
