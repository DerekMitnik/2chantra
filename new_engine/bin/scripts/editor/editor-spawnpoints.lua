Editor.characters = {}
Editor.spawnpoints = {}
Editor.spawn_character = nil

function Editor.setCharacters( characters )
	if not characters then return end
	Editor.characters = characters
	Editor.spawn_character = characters[1]
end

function Editor.createSpawnpoints( spawnpoints )
	if not spawnpoints then return end
	for k, v in pairs( spawnpoints ) do
		if true then
			local point = CreatePlayer( Editor.spawn_character or "pear15soh", v[1], v[2] )
			SetObjPos( point, v[1], v[2] )
			Editor.setObjParam( point, "SPECIAL_CREATION", true )
		end
	end
	Editor.spawnpoints = spawnpoints
end

function Editor.spawnPoints()
	local ret = {}
	table.insert( ret, "LEVEL.characters = {" )
	for k, v in pairs( Editor.characters ) do
		table.insert( ret, '"' )
		table.insert( ret, v )
		table.insert( ret, '",' )
	end
	table.insert( ret, "}\n" )
	table.insert( ret, "LEVEL.spawnpoints = {" )
	for k, v in pairs( EditorDumpMap() ) do
		if v.type == Editor.TYPES.OBJ_PLAYER and not Editor.ObjInVoid( v.id ) then
			table.insert( ret, '{' )
			table.insert( ret, v.aabb.p.x )
			table.insert( ret, ',' )
			table.insert( ret, v.aabb.p.y )
			table.insert( ret, '},' )
		end
	end
	table.insert( ret, "}\n" )
	return table.concat( ret )
end

print( Editor.spawnPoints )
