Editor.HISTORY_LENGTH = 64

function Editor.ClearHistory()
	if not Editor.history then
		Editor.history = {}
		Editor.history_depth = 0
		return
	end
	for i=1, #Editor.history do
		Editor.HistoryCleanup( Editor.history[i] )
	end
	Editor.history = {}
	Editor.history_depth = 0
end

--[[
debug_id = 0
function debug_print()
	local st = ""
	for i=1,#Editor.history do
		if i == #Editor.history - Editor.history_depth then
			st = st .. " ("..Editor.history[i].id..")"
		else
			st = st .. " "..Editor.history[i].id
		end
	end
	Log(st)
end
--]]

function Editor.AddHistoryEvent( event )
--[[
	event.id = debug_id
	debug_id = debug_id + 1
	Log( "Add ",event.id )
	debug_print()
--]]
	if Editor.history_depth > 0 then
		for i=Editor.history_depth-1,0,-1 do
			Editor.HistoryCleanup( Editor.history[#Editor.history-i] )
		end
		for i=0,Editor.history_depth-1 do
			table.remove( Editor.history, #Editor.history-i )
		end
		Editor.history_depth = 0
	end
	table.insert( Editor.history, event )
	if #Editor.history > Editor.HISTORY_LENGTH then
		Editor.HistoryCleanup( Editor.history[1] )
		table.remove( Editor.history, 1 )
	end
end

function Editor.Undo()
	if Editor.history_depth >= #Editor.history then
		Editor.FloatyText( "Nothing to undo." )
		return
	end
	Editor.UndoEvent( Editor.history[#Editor.history-Editor.history_depth] )
	Editor.history_depth = Editor.history_depth + 1
--[[
	Log("Undo complete")
	debug_print()
--]]
end

function Editor.Redo()
	if Editor.history_depth == 0 then
		Editor.FloatyText( "Nothing to redo." )
		return
	end
	Editor.history_depth = Editor.history_depth - 1
	Editor.RedoEvent( Editor.history[#Editor.history-Editor.history_depth] )
--[[
	Log("Redo complete")
	debug_print()
--]]
end

function Editor.UndoEvent( event )
--[[
	Log( "Undo ",event.id )
	debug_print()
--]]
	if event.type == "MOVEMENT" then
		for k, v in pairs(event.from) do
			v.obj.aabb.p.x = v.pos.x
			v.obj.aabb.p.y = v.pos.y
			if v.pos.z then
				v.obj.sprite.z = v.pos.z
			end
			SetObjPos( v.obj.id, v.pos.x, v.pos.y, v.pos.z )
		end
	elseif event.type == "DELETION" then
		for k, v in pairs(event.obj) do
			Editor.RetrieveFromVoid( v.param.id )
			if v.group then
				table.insert(v,group, v.param.id)
			end
		end
	elseif event.type == "CREATION" then
		for k, v in pairs(event.obj) do
			Editor.BanishToVoid( v )
		end
	elseif event.type == "GROUP" then
		for k, v in pairs(event.from) do
			Editor.RemoveFromGroup( v.obj.id )
			Editor.groups[ v.obj.id ] = v.group
			if v.group then
				table.insert( v.group, v.obj )
			end
		end
	elseif event.type == "FRAME" then
		for k,v in pairs(event.from) do
			SetObjAnim( v.obj.id, v.frame, false )
		end
	elseif event.type == "SPRITE_COLOR" then
		for k,v in pairs(event.from) do
			SetObjSpriteColor( v.obj.id, v.color )
		end
	elseif event.type == "RIBBON" then
		event.obj = deep_copy( event.from )
		local obj = event.obj
		local s1 = iff( obj.ubl, obj.bl, nil )
		local s2 = iff( obj.ubt, obj.bt, nil )
		local s3 = iff( obj.ubr, obj.br, nil )
		local s4 = iff( obj.ubb, obj.bb, nil )
		SetRibbonObjK( obj.id, obj.k.x, obj.k.y )
		SetRibbonObjBounds( obj.id, s1, s2, s3, s4 )
		SetRibbonObjRepitition( obj.id, obj.repeat_x, obj.repeat_y )
	elseif event.type == "PARAM_CHANGE" then
		for k, v in pairs( event.from ) do
			Editor.setObjParam( event.obj.id, k, v )
		end
	elseif event.type == "SPAWNER_ENEMY_CONVERSION" then
		if event.to_spawner then
			Editor.EnemyConversion( event.obj, true )
		else
			Editor.SpawnerConversion( event.obj, true )
		end
	elseif event.type == "POLYGON_CREATE" then
		Editor.BanishToVoid( event.polygon )
		local nxt
		for i=1,#event.markers do
			Editor.RetrieveFromVoid( event.markers[i].id, true )
			nxt = i + 1
			if i == #event.markers then
				nxt = 1
			end
			EditorSetLink( event.markers[i].id, event.markers[ nxt ].id )
		end
	elseif event.type == "POLYGON_DECOMPOSE" then
		Editor.RetrieveFromVoid( event.polygon.id )
		for k, v in pairs( event.markers ) do
			Editor.BanishToVoid( v, true )
		end
	end
	event.undone = true
	Editor.FloatyText( "Undo: "..(event.text or "something") )
end

function Editor.RedoEvent( event )
--[[
	Log( "Redo ",event.id )
	debug_print()
--]]
	if event.type == "MOVEMENT" then
		for k, v in pairs(event.to) do
			v.obj.aabb.p.x = v.pos.x
			v.obj.aabb.p.y = v.pos.y
			if v.pos.z then
				v.obj.sprite.z = v.pos.z
			end
			SetObjPos( v.obj.id, v.pos.x, v.pos.y, v.pos.z )
		end
	elseif event.type == "DELETION" then
		for k, v in pairs(event.obj) do
			Editor.BanishToVoid( v.param )
			if v.group then
				for k1, v1 in pairs( v.group ) do
					if v1.id == v.param..id then
						table.remove( v.group, k1 )
						break
					end
				end
			end
		end
	elseif event.type == "CREATION" then
		for k, v in pairs(event.obj) do
			Editor.RetrieveFromVoid( v.id )
		end
	elseif event.type == "GROUP" then
		for k, v in pairs(event.to) do
			Editor.RemoveFromGroup( v.obj.id )
			Editor.groups[ v.obj.id ] = v.group
			if v.group then
				table.insert( v.group, v.obj )
			end
		end
	elseif event.type == "FRAME" then
		for k,v in pairs(event.to) do
			SetObjAnim( v.obj.id, v.frame, false )
		end
	elseif event.type == "SPRITE_COLOR" then
		for k,v in pairs(event.to) do
			SetObjSpriteColor( v.obj.id, v.color )
		end
	elseif event.type == "RIBBON" then
		event.obj = deep_copy( event.to )
		local obj = event.obj
		local s1 = iff( obj.ubl, obj.bl, nil )
		local s2 = iff( obj.ubt, obj.bt, nil )
		local s3 = iff( obj.ubr, obj.br, nil )
		local s4 = iff( obj.ubb, obj.bb, nil )
		SetRibbonObjK( obj.id, obj.k.x, obj.k.y )
		SetRibbonObjBounds( obj.id, s1, s2, s3, s4 )
		SetRibbonObjRepitition( obj.id, obj.repeat_x, obj.repeat_y )
	elseif event.type == "PARAM_CHANGE" then
		for k, v in pairs( event.to ) do
			Editor.setObjParam( event.obj.id, k, v )
		end
	elseif event.type == "SPAWNER_ENEMY_CONVERSION" then
		if event.to_spawner then
			Editor.SpawnerConversion( event.obj, true )
		else
			Editor.EnemyConversion( event.obj, true )
		end
	elseif event.type == "POLYGON_CREATE" then
		Editor.RetrieveFromVoid( event.polygon.id )
		for k, v in pairs( event.markers ) do
			Editor.BanishToVoid( v, true )
		end
	elseif event.type == "POLYGON_DECOMPOSE" then
		Editor.BanishToVoid( event.polygon )
		local nxt
		for i=1,#event.markers do
			Editor.RetrieveFromVoid( event.markers[i].id, true )
			nxt = i + 1
			if i == #event.markers then
				nxt = 1
			end
			EditorSetLink( event.markers[i].id, event.markers[ nxt ].id )
		end
	end
	event.undone = false
	Editor.FloatyText( "Redo: "..(event.text or "something") )
end

function Editor.HistoryCleanup( event )
--[[
	Log( "Cleanup ",event.id )
	debug_print()
--]]
	if event.type == "DELETION" and not event.undone then
		for k, v in pairs(event.obj) do
			Editor.LostInVoid( v.param.id )
			Editor.groups[v.param.id] = nil
		end
	elseif event.type == "CREATION" and event.undone then
		for k, v in pairs(event.obj) do
			Editor.LostInVoid( v.id )
		end
	elseif event.type == "POLYGON_CREATE" then
		if event.undone then
			Editor.LostInVoid( event.polygon.id )
		else
			for k, v in pairs( event.markers ) do
				Editor.LostInVoid( v.id )
			end
		end
	elseif event.type == "POLYGON_DECOMPOSE" then
		if not event.undone then
			Editor.LostInVoid( event.polygon.id )
		else
			for k, v in pairs( event.markers ) do
				Editor.LostInVoid( v.id )
			end
		end
	end
end

function Editor.EmptyVoid()
	if not Editor.void then
		return
	end
	for k,v in pairs( Editor.void ) do
		SetObjDead( k )
	end
	Editor.void = nil
end

function Editor.ObjInVoid( id )
	if Editor.sensitive_spots then
		for k, v in pairs( Editor.sensitive_spots ) do
			if id == v.sprite then
				return true
			end
		end
	end
	if not Editor.void or not Editor.void[id] then
		return false
	end
	return true
end

function Editor.BanishToVoid( obj, silent )
	if not Editor.void then
		Editor.void = {}
	end
	if Editor.getObjParam( obj.id, "DELETE_EVENT" ) and not silent then
		Editor.getObjParam( obj.id, "DELETE_EVENT" )( obj )
	end
	Editor.setObjParam( obj.id, "DONT_SELECT", true )
	Editor.setObjParam( obj.id, "DONT_SAVE", true )
	Editor.setObjParam( obj.id, "DEAD", true )
	Editor.void[obj.id] = obj
	SetPhysObjBorderColor( obj.id, {1,0.5,0,0}, false)
	SetObjPos( obj.id, -9000, -9000, CONFIG.far_z )
	SetObjInvisible( obj.id, true )
end

function Editor.RetrieveFromVoid( id, silent )
	if not Editor.void[id] then
		return
	end
	Editor.setObjParam( id, "DONT_SELECT", false )
	Editor.setObjParam( id, "DONT_SAVE", false )
	Editor.setObjParam( id, "DEAD", false )
	SetPhysObjBorderColor( id, {1,0.5,0,1}, false)
	local z = 0
	if Editor.void[id].sprite then
		z = Editor.void[id].sprite.z
	end
	SetObjPos( id, Editor.void[id].aabb.p.x, Editor.void[id].aabb.p.y, z )
	Editor.void[id] = nil
	SetObjInvisible( id, false )
	if Editor.getObjParam( id, "RESTORE_EVENT" ) and not silent then
		Editor.getObjParam( id, "RESTORE_EVENT" )( GetObject(id) )
	end
end

function Editor.LostInVoid( id )
	if Editor.void and not Editor.void[id] then
		return
	end
	SetObjDead( id )
end
