function Editor.InitWaypoints()
	Editor.waypoints = {}
	Editor.waypoint_origin = nil
end

function Editor._AddWaypoint( who, where, what )
	local wp = what or CreateWaypoint( where[1], where[2], 32, 32 )
	Editor.setObjParam( who.id, "SPECIAL_CREATION", true )
	Editor.setObjParam( wp, "SPECIAL_CREATION", true )
	Editor.setObjParam( who.id, "WAYPOINT", wp )
	EditorSetLink( who.id, wp )
end

function Editor._AddWaypointSelection(o)
	RegEditorGetObjectsProc(Editor.selectedObjectsProc)
	local obj
	local wp
	for k, v in pairs(o) do
		obj =  GetObject(v)
		if obj.type == Editor.TYPES.OBJ_WAYOINT then
			wp = obj.id
			break
		end
	end
	local mx, my = GetMousePos()
	Editor._AddWaypoint( Editor.waypoint_origin, {mx, my}, wp )
end

function Editor.AddWaypoint( who )
	Editor.waypoint_origin = who
	RegEditorGetObjectsProc(Editor._AddWaypointSelection)
end

function Editor.Waypoint()
	local obj
	for k, v in pairs( Editor.selection ) do
		if v.type == Editor.TYPES.OBJ_WAYPOINT or v.type == Editor.TYPES.OBJ_ENEMY and not Editor.getObjParam( v.id, "CONVERTED" ) then
			if obj then return end
			obj = v
		end
	end
	Editor.AddWaypoint( obj )
end
