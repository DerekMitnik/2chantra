menus = PagedMenu.create()
menus2 = {}
menus3 = PagedMenu.create()

if not Editor then Editor = {} end

menus:addPage( 1, SimpleMenu.create( {CONFIG.scr_width/2, CONFIG.scr_height/2}, 0, 0, 30, 10 ) )

function Editor.show_dir( root_dir, filter, preprocess, event, cancel_event, preview )
	local dir, dirs = ListDirContents( Editor.path )
	dirs = dirs or 0
	dir = dir or {}
	local files = {}
	for i = #dir, dirs+1, -1 do
		table.insert( files, table.remove( dir, i ) )
	end
	table.sort( files )
	table.sort( dir )
	for i = 1, #files do
		table.insert( dir, files[i] )
	end

	Editor.menu:clear(2)
	if not preprocess then preprocess = function(a) return a end end
	if not filter then filter = function(a) return true end end

	if Editor.path ~= root_dir then
		menus:add(
				MWFunctionButton.create(
				{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
				"..",
				0, 
				function()
					if string.match(Editor.path, "/[^/]+$") then
						Editor.path = string.gsub(Editor.path, "/[^/]+$", "")
					else
						Editor.path = ""
					end
					Editor.show_dir( root_dir, filter, preprocess, event, cancel_event, preview )
				end,
				nil,
				{name="wakaba-widget"}
				),
			2
		)
	end
	menus:add(
				MWFunctionButton.create(
				{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
				"cancel",
				0, 
				function()
					Editor.menu:hide(2)
					cancel_event()
				end,
				nil,
				{name="wakaba-widget"}
				),
			2
		)

	for i=1,dirs do
		menus:add(
				MWFunctionButton.create(
				{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
				preprocess(dir[i]),
				0, 
				function() 
					Editor.path = Editor.path.."/"..dir[i]
					Editor.show_dir( root_dir, filter, preprocess, event, cancel_event, preview )
				end,
				dir[i],
				{name="wakaba-widget"}
				),
			2
		)
	end

	for i=dirs+1, #dir do
		if filter(Editor.path.."/"..dir[i]) then
			menus:add(
					MWFunctionButton.create(
					{name="dialogue", color={.6,.6,.6,1}, active_color={.2,.2,.6,1}}, 
					preprocess(dir[i]),
					0, 
					function()
						Editor.path = Editor.path.."/"..dir[i]
						Editor.menu:hide(2)
						event( Editor.path )
					end,
					dir[i],
					{name="wakaba-widget"},
					nil, function() if preview then preview( Editor.path.."/"..dir[i] ) end end
					),
				2
			)
		end
	end
	
	Editor.menu:move( CONFIG.scr_width/2, CONFIG.scr_height/2, 2 )
	Editor.menu:show( 2 )
end

function Editor.ptype_show_dir()
	local dir, dirs = ListDirContents( Editor.path )
	Editor.menu:clear(2)

	if Editor.path ~= "proto" then
		menus:add(
				MWFunctionButton.create(
				{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
				"..",
				0, 
				Editor.ptype_back,
				nil,
				{name="wakaba-widget"}
				),
			2
		)
	end

	for i=1,dirs do
		menus:add(
				MWFunctionButton.create(
				{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
				dir[i],
				0, 
				Editor.ptype_dir,
				dir[i],
				{name="wakaba-widget"}
				),
			2
		)
	end

	for i=dirs+1, #dir do
		menus:add(
				MWFunctionButton.create(
				{name="default", color={.8,.8,.8,.8}, active_color={.2,.2,1,1}}, 
				dir[i],
				0, 
				Editor.ptype_file,
				dir[i],
				{name="wakaba-widget"}
				),
			2
		)
	end
	
	Editor.menu:move( CONFIG.scr_width/2, CONFIG.scr_height/2, 2 )
	Editor.menu:show( 2 )
end

function Editor.ptype_selected( id, ptype )
	Editor.ptype = ptype
	if ptype == "ENEMY" then
		Editor.path = "proto/enemies"
	elseif ptype == "SPRITE" then
		Editor.path = "proto/sprites"
	elseif ptype == "PLAYER" then
		Editor.path = "proto/characters"
	elseif ptype == "ITEM" then
		Editor.path = "proto/items"
	elseif ptype == "TILE" then
		Editor.path = "proto/sprites"
	elseif ptype == "PHYSIC GROUP" then
		Editor.path = "proto/sprites"
	elseif ptype == "ACTIVE GROUP" then
		Editor.path = "proto/items"
	elseif ptype == "RIBBON" then
		Editor.path = "proto/sprites"
	else
		Editor.path = "proto"
	end
	local preview
	local preview_window
	local preview_widget
	if ptype ~= "PHYSIC GROUP" and ptype ~= "ACTIVE GROUP" then
		preview_widget = CreateWidget( constants.wt_Button, "PREVIEW", nil, CONFIG.scr_width - 138, CONFIG.scr_height - 138, 128, 128 )
		preview_window = Window.create( CONFIG.scr_width - 148, CONFIG.scr_height - 148, 148, 148, "window1" )
		description_widget = CreateWidget( constants.wt_Label, "PREVIEW", nil, CONFIG.scr_width - 286, 20, 276, CONFIG.scr_height - 188 )
		description_window = Window.create( CONFIG.scr_width - 296, 10, 296, CONFIG.scr_height - 168, "window1" )
		description_window:setVisible(false)
		WidgetSetCaptionColor( description_widget, { 1, 1, 1, 1 }, false )
		WidgetSetCaptionFont( description_widget, "dialogue" )
		preview = function( filename )
			WidgetSetSize( preview_widget, 128, 128 )
			WidgetSetPos( preview_widget, CONFIG.scr_width - 138, CONFIG.scr_height - 138 )
			local info, sb = Editor.emulateObject( Editor.EDITOR_TYPES[Editor.ptype], filename )
			if info and info.texture then
				WidgetSetSprite( preview_widget, info.texture, true, info.num )
				WidgetSetSpriteColor( preview_widget, sb.color or {1,1,1,1} )
			else
				WidgetSetSprite( preview_widget, "editor_misc", true )
			end
			local var = Editor.extractVariables( filename )
			if var.description then
				description_window:setVisible(true)
				WidgetSetCaption( description_widget, var.description, true )
				WidgetSetVisible( description_widget, true )
			else
				description_window:setVisible(false)
				WidgetSetVisible( description_widget, false )
			end
			WidgetSetZ( preview_widget, 1 )
		end
	end
	Editor.show_dir( Editor.path, nil, 
		function(a) 
			return string.gsub(a, "%.lua", "") 
		end,
		function (filename)
			Editor.ptype_file( -1, filename )
			if preview_window then
				preview_window:destroy()
				description_window:destroy()
			end
			if preview_widget then
				DestroyWidget( preview_widget )
				DestroyWidget( description_widget )
			end
		end,
		function ()
			Editor.changeState( Editor.STATE.RUNNING )
			if preview_window then
				preview_window:destroy()
				description_window:destroy()
			end
			if preview_widget then
				DestroyWidget( preview_widget )
				DestroyWidget( description_widget )
			end
		end,
		preview
	)
end

function Editor.ptype_back()
	Editor.path = string.gsub(Editor.path, "/[^/]+$", "")
	Editor.ptype_show_dir()	
end

function Editor.ptype_dir( id, name )
	Editor.path = Editor.path.."/"..name
	Editor.ptype_show_dir()	
end

function Editor.ptype_file( id, name )
	local proto_name = string.gsub(name, "%.lua$", "")
	proto_name = string.gsub( proto_name, "proto/", "" )
	full_path = Editor.path .. "/" .. name
	if Editor.ptype == "SPRITE" then
		proto_name = string.gsub( proto_name, "^sprites/", "" )
	elseif Editor.ptype == "ENEMY" then
		proto_name = string.gsub( proto_name, "^enemies/", "" )
	elseif Editor.ptype == "PLAYER" then
		proto_name = string.gsub( proto_name, "^characters/", "" )
	elseif Editor.ptype == "TILE" then
		proto_name = string.gsub( proto_name, "^sprites/", "" )
	elseif Editor.ptype == "ITEM" then
		proto_name = string.gsub( proto_name, "^items/", "" )
	elseif Editor.ptype == "PHYSIC GROUP" then
		proto_name = string.gsub( proto_name, "^sprites/", "" )
	elseif Editor.ptype == "ACTIVE GROUP" then
		proto_name = string.gsub( proto_name, "^items/", "" )
	elseif Editor.ptype == "RIBBON" then
		proto_name = string.gsub( proto_name, "^sprites/", "" )
	end
	Editor.menu:hide()
	Editor.changeState( Editor.STATE.RUNNING )
	Editor.SelectObject(proto_name, Editor.ptype, name)
end

for key, value in pairs( {"PLAYER", "SPRITE", "TILE", "ENEMY", "ITEM", "PHYSIC GROUP", "ACTIVE GROUP", "RIBBON"} ) do
	menus:add(
		MWFunctionButton.create(
				{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
				value,
				0, 
				Editor.ptype_selected,
				value,
				{name="wakaba-widget"}
		),
		1
	)
end
menus:add(
		MWSpace.create( 10, 5 ),
		1
	)
menus:add(
		MWFunctionButton.create(
				{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
				"CANCEL",
				0, 
				function() Editor.menu:hide() Editor.changeState( Editor.STATE.RUNNING ) end,
				nil,
				{name="wakaba-widget"}
		),
		1
	)

menus:addPage( 2, ScrollingMenu.create( {CONFIG.scr_width/2, CONFIG.scr_height/2}, 0, 0, 30, 10 ) )
menus:addPage( 3, SimpleMenu.create( {CONFIG.scr_width/2, CONFIG.scr_height/2}, 0, 0, 30, 10 ) )


if not RestoreKeyProcessors then
	function RestoreKeyProcessors()
	end
end

function RemoveKeyProcessors()
	local on_press = GlobalGetKeyDownProc()
	local on_release = GlobalGetKeyReleaseProc()
	local mouse_press = GlobalGetMouseKeyDownProc()
	local mouse_release = GlobalGetMouseKeyReleaseProc()
	local restore_proc = RestoreKeyProcessors
	GlobalSetKeyDownProc( nil )
	GlobalSetKeyReleaseProc( nil )
	GlobalSetMouseKeyDownProc( nil )
	GlobalSetMouseKeyReleaseProc( nil )
	RestoreKeyProcessors = function()
		GlobalSetKeyDownProc( on_press )
		GlobalSetKeyReleaseProc( on_release )
		GlobalSetMouseKeyDownProc( mouse_press )
		GlobalSetMouseKeyReleaseProc( mouse_release )
		RestoreKeyProcessors = restore_proc
	end
end

function Editor.confirm( message, sure, not_sure )
	RemoveKeyProcessors()
	local l_window = SimpleMenu.create( {CONFIG.scr_width/2, CONFIG.scr_height/2}, 0, 0, 30, 10 )
	l_window:add( MWSpace.create( 15, 15 ) )
	l_window:add( MWCaption.create( {name="dialogue", color={1,1,1,1}}, message, 0 ) )
	l_window:add( MWSpace.create( 15, 15 ) )
	l_window:add( MWComposition.create( 10, {
		MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"YES",
			0,
			function()
				l_window:clear()
				l_window:hide()
				RestoreKeyProcessors()
				sure()
			end, 
			nil, 
			{name="wakaba-widget"} ),
		MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"NO",
			0,
			function()
				l_window:clear()
				l_window:hide()
				RestoreKeyProcessors()
				not_sure()
			end, 
			nil, 
			{name="wakaba-widget"} )
	}))
	l_window:add( MWSpace.create( 15, 15 ) )
	l_window:show()
end

function Editor.popup( message, event )
	RemoveKeyProcessors()
	local l_window = SimpleMenu.create( {CONFIG.scr_width/2, CONFIG.scr_height/2}, 0, 0, 30, 10 )
	l_window:add( MWSpace.create( 15, 15 ) )
	l_window:add( MWCaption.create( {name="dialogue", color={1,1,1,1}}, message, 0 ) )
	l_window:add( MWSpace.create( 15, 15 ) )
	l_window:add( MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"OK",
			0,
			function()
				l_window:clear()
				l_window:hide()
				RestoreKeyProcessors()
				event()
			end, 
			nil, 
			{name="wakaba-widget"} ))
	l_window:add( MWSpace.create( 15, 15 ) )
	l_window:show()
end

function Editor.longPopup( var )
	var = var or {}
	var.event = var.event or function() end
	RemoveKeyProcessors()
	local l_window = SimpleMenu.create( {CONFIG.scr_width/2, CONFIG.scr_height/2}, 0, 0, 30, 10 )
	l_window:add( MWSpace.create( 15, 15 ) )
	if var.title then
		l_window:add( MWCaption.create( {name="default", color={.8,.9,.3,1}}, var.title, 0 ) )
		l_window:add( MWSpace.create( 15, 15 ) )
	end
	var.message = var.message or { "Someone forgot to put a message here, that jerk." }
	if #var.message > 16 then
		var.message[17] = "... and "..(#var.message-16).." more."
	end
	for i=1,math.min(17, #var.message) do
		l_window:add( MWCaption.create( {name="dialogue", color={1,1,1,1}}, var.message[i], 0 ) ) --TODO: multiline
	end
	l_window:add( MWSpace.create( 15, 15 ) )
	l_window:add( MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"OK",
			0,
			function()
				l_window:clear()
				l_window:hide()
				RestoreKeyProcessors()
				var.event()
			end, 
			nil, 
			{name="wakaba-widget"} ))
	l_window:add( MWSpace.create( 15, 15 ) )
	l_window:show()
end

function Editor.popup_enter( message, ok, cancel, default )
	if not default then default = (Editor.current_map or "map") end
	vars.__input = default
	RemoveKeyProcessors()
	local l_window = SimpleMenu.create( {CONFIG.scr_width/2, CONFIG.scr_height/2}, 0, 0, 30, 10 )
	l_window:add( MWSpace.create( 15, 15 ) )
	l_window:add( MWCaption.create( {name="dialogue", color={1,1,1,1}}, message, 0 ) )
	l_window:add( MWInputField.create( {name="dialogue", color={1,1,1,1}, active_color={.8,.8,1,1}}, 
		default, 0, {name="wakaba-widget"}, nil, "__input", 20 ) )
	l_window:add( MWSpace.create( 15, 15 ) )
	l_window:add( MWComposition.create( 10, {
		MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"OK",
			0,
			function()
				l_window:clear()
				l_window:hide()
				RestoreKeyProcessors()
				ok(vars.__input)
			end, 
			nil, 
			{name="wakaba-widget"} ),
		MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"CANCEL",
			0,
			function()
				l_window:clear()
				l_window:hide()
				RestoreKeyProcessors()
				cancel()
			end, 
			nil, 
			{name="wakaba-widget"} )
	}))
	l_window:add( MWSpace.create( 15, 15 ) )
	l_window:show()
end

function str2col( str )
	if not str then str = { 0, 0, 0, 1 } end
	local r,g,b,a = string.match( str, "%[([^,]+),([^,]+),([^,]+),([^%]]+)%]" )
	if r then
		return { tonumber(r), tonumber(g), tonumber(b), tonumber(a) }
	else
		return { 0, 0, 0, 0 }
	end
end

function Editor.valuesPopup( title, subtitle, properties, event, cancel_event, object )
	Editor.changeState( Editor.STATE.IN_MENU )
	RemoveKeyProcessors()
	local l_window = SimpleMenu.create( {CONFIG.scr_width/2, CONFIG.scr_height/2}, 0, 0, 30, 10 )
	l_window:add( MWSpace.create( 15, 15 ) )
	if title then l_window:add( MWCaption.create( {name="default", color={1,1,.2,1}}, title, 0 ) ) end
	if subtitle then l_window:add( MWCaption.create( {name="dialogue", color={1,1,.2,1}}, subtitle, 0 ) ) end
	l_window:add( MWSpace.create( 15, 15 ) )
	local p = {}
	local p_back = {}
	for i=1, #properties do
		p[i] = properties[i]
		if p[i][2] == "FLOAT" then
			l_window:add( MWComposition.create( 10, {
				MWCaption.create( {name = "dialogue", color = {1,1,1,1}},
					p[i][1],
					-1 ),
				MWInputField.create( {name = "dialogue", color = {1,1,1,1}, active_color={.8,.8,1,1}},
					p[i][3],
					-1,
					nil,
					function( sender, value )
						local vf = tonumber(value)
						if vf or value == "nil" then
							p_back[p[i][1]] = value
							WidgetSetCaptionColor( sender, {1,1,1,1}, false )
							WidgetSetCaptionColor( sender, {.8,.8,1,1}, true )
						else
							WidgetSetCaptionColor( sender, {1,.6,.6,1}, false )
							WidgetSetCaptionColor( sender, {1,.2,.2,1}, true )
						end
					end, 
					p[i][1], 
					20 )
			}))
		elseif p[i][2] == "INT" then
			l_window:add( MWComposition.create( 10, {
				MWCaption.create( {name = "dialogue", color = {1,1,1,1}},
					p[i][1],
					-1 ),
				MWInputField.create( {name = "dialogue", color = {1,1,1,1}, active_color={.8,.8,1,1}},
					p[i][3],
					-1,
					nil,
					function( sender, value )
						local vf = tonumber(value)
						if (vf and vf == math.floor(vf)) or value == "nil" then
							p_back[p[i][1]] = value
							WidgetSetCaptionColor( sender, {1,1,1,1}, false )
							WidgetSetCaptionColor( sender, {.8,.8,1,1}, true )
						else
							WidgetSetCaptionColor( sender, {1,.6,.6,1}, false )
							WidgetSetCaptionColor( sender, {1,.2,.2,1}, true )
						end
					end, 
					p[i][1], 
					20 )
			}))
		elseif p[i][2] == "BOOL" then
			local val = {"true", "false"}
			if p[i][3] == "false" then val = {"false", "true"} end
			l_window:add( MWComposition.create( 10, {
				MWCaption.create( {name = "dialogue", color = {1,1,1,1}},
					p[i][1],
					-1 ),
				MWCycleButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.8,.8,1,1}},
					p[i][3],
					-1,
					nil,
					function( sender, value )
						p_back[p[i][1]] = value
					end, 
					p[i][1], 
					val )
			}))
		elseif p[i][2] == "LIST" then
			local val = {}
			local capt = {}
			for k, v in pairs( p[i][4] ) do
				table.insert( val, k )
				table.insert( capt, v )
			end
			local shifts = #val
			while shifts > 0 and p[i][3] ~= val[1] do
				shifts = shifts - 1
				table.insert( val, table.remove( val, 1 ) )
				table.insert( capt, table.remove( capt, 1 ) )
			end
			vars[p[i][1]] = p[i][3]
			l_window:add( MWComposition.create( 10, {
				MWCaption.create( {name = "dialogue", color = {1,1,1,1}},
					p[i][1],
					-1 ),
				MWCycleButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.8,.8,1,1}},
					capt[p[i][3]],
					-1,
					nil,
					function( sender, value )
						p_back[p[i][1]] = value
					end, 
					p[i][1], 
					val,
					capt,
					p[i][3] )
			}))
		elseif p[i][2] == "STACK" then
			p_back[p[i][1]] = p[i][3]
			local capt = "empty"
			if p[i][3] then
				capt = "{ "..table.concat(p[i][3], ", ").." }"
			end
			local element = 0
			local change_func = function()
				if #p_back[p[i][1]] > 0 then
					l_window.contents[element].elements[2]:setText( "{ "..table.concat(p_back[p[i][1]], ", ").." }" )
				else
					l_window.contents[element].elements[2]:setText( "empty" )
				end
			end
			element = l_window:add( MWComposition.create( 10, {
				MWCaption.create( {name = "dialogue", color = {1,1,1,1}},
					p[i][1],
					-1 ),
				MWCaption.create( {name = "dialogue", color = {1,1,.3,1}},
					capt,
					-1 ),
				MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.8,.8,1,1}},
					"PUSH",
					-1,
					function( sender )
						l_window:hide()
						Editor.popup_enter( "Enter int value: ", 
							function( new_val )
								new_val = tonumber( new_val )
								if new_val then
									if not p_back[p[i][1]] then p_back[p[i][1]] = {} end
									table.insert( p_back[p[i][1]], new_val )
									l_window:show()
									change_func()
								end
							end,
							function()
								l_window:show()
							end,
							"0" )
					end),
				MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.8,.8,1,1}},
					"POP",
					-1,
					function( sender )
						if p_back[p[i][1]] then
							table.remove( p_back[p[i][1]], #p_back[p[i][1]] )
							change_func()
						end
					end
				)
			}))
		elseif p[i][2] == "COLOR" then
			p_back[p[i][1]] = p[i][3]
			l_window:add( MWComposition.create( 10, {
				MWCaption.create( {name = "dialogue", color = {1,1,1,1}},
					p[i][1],
					-1 ),
				MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.8,.8,1,1}},
					p[i][3],
					-1,
					function( sender )
						RestoreKeyProcessors()
						l_window:hide()
						local col = str2col( p_back[p[i][1]] )
						local butte = sender
						ColorPicker( col )
						Editor.colorSelected = function()
							RemoveKeyProcessors()
							local col = {}
							table.insert( col, tostring(vars.red) )
							table.insert( col, tostring(vars.green) )
							table.insert( col, tostring(vars.blue) )
							table.insert( col, tostring(vars.alpha) )
							local str = "["..table.concat(col, ", ").."]"
							p_back[p[i][1]] = str
							col = {}
							table.insert( col, string.sub(tostring(vars.red),1,3) )
							table.insert( col, string.sub(tostring(vars.green),1,3) )
							table.insert( col, string.sub(tostring(vars.blue),1,3) )
							table.insert( col, string.sub(tostring(vars.alpha),1,3) )
							local str = "["..table.concat(col, ", ").."]"
							WidgetSetCaption( butte, str )
							l_window:show()
						end
					end )
			}))
		end
	end
	l_window:add( MWComposition.create( 10, {
		MWCaption.create( {name = "dialogue", color = {1,1,1,1}},
			"obstruction",
			-1 ),
		MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.8,.8,1,1}},
			"[...]",
			-1,
			function( sender, value )
				local solid = object.solid_to or 255
				local bits = {}
				for i=7,0,-1 do
					if solid >= math.pow( 2, i ) then
						solid = solid - math.pow( 2, i )
						bits[i] = true
					else
						bits[i] = false
					end
				end
				l_window:hide()
				local window = CreateWidget( constants.wt_Panel, "", nil, CONFIG.scr_width/2 - 100, CONFIG.scr_height/2 - 100, 200, 200 )
				WidgetSetSprite( window, "window1" )
				WidgetSetFocusable( window, false )
				local y = 15
				local values = { 
						{ constants.physPlayer, "Characters" }, 
						{ constants.physBullet, "Projectiles" },
						{ constants.physEffect, "Effects" },
						{ constants.physSprite, "Sprites" },
						{ constants.physItem, "Items" },
						{ constants.physEnemy, "Enemies" },
						{ constants.physEnvironment, "Environments" },
						{ constants.physParticles, "Particles" }
				}
				local w
				local color
				for i=1, #values do
						w = CreateWidget( constants.wt_Label, "", window, 0, 0, 1, 1 )
						WidgetSetPos( w, 15, y, true )
						WidgetSetCaptionFont( w, "dialogue" )
						WidgetSetCaptionColor( w, {1,1,1,1}, false )
						WidgetSetCaption( w, values[i][2] )
						local checkbox = CreateWidget( constants.wt_Button, "", window, 0, 0, 10, 10 )
						WidgetSetPos( checkbox, 150, y, true )
						color = { 0, 0, 0, 1 }
						if bits[i-1] then
							color = { 1, 1, 1, 1 }
						end
						WidgetSetColorBox( checkbox, color )
						WidgetSetLMouseClickProc( checkbox, function(sender)
							bits[i-1] = not bits[i-1]
							local color = { 0, 0, 0, 1 }
							if bits[i-1] then
								color = { 1, 1, 1, 1 }
							end
							WidgetSetColorBox( sender, color )
						end )
						y = y + 15
				end
				local back = CreateWidget( constants.wt_Button, "", window, 0, 0, 50, 10 )
				WidgetSetPos( back, 75, 200-15-15, true )
				WidgetSetCaptionFont( back, "dialogue" )
				WidgetSetCaption( back, "BACK" )
				WidgetSetCaptionColor( back, {1,1,1,1}, true )
				WidgetSetLMouseClickProc( back, function()
					DestroyWidget( window )
					l_window:show()
					local ret = 0
					for i=0,7 do
						if bits[i] then
							ret = ret + math.pow( 2, i )
						end
					end
					Log( ret )
					p_back["obstruction"] = ret
				end )
			end, 
			nil, 
			nil )
	}))
	l_window:add( MWSpace.create( 15, 15 ) )
	l_window:add( MWComposition.create( 10, {
		MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"OK",
			0,
			function()
				l_window:clear()
				l_window:hide()
				RestoreKeyProcessors()
				if event then event( p_back ) end
			end, 
			nil, 
			{name="wakaba-widget"} ),
		MWFunctionButton.create( {name = "default", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"CANCEL",
			0,
			function()
				l_window:clear()
				l_window:hide()
				RestoreKeyProcessors()
				if cancel_event then cancel_event() end
			end, 
			nil, 
			{name="wakaba-widget"} )
	}))
	l_window:add( MWSpace.create( 15, 15 ) )
	l_window.lock_size = true
	l_window:show()
end

function Editor.PropertiesMenu()
	local prop = 0
	local obj
	for k, v in pairs( Editor.selection ) do
		if not Editor.getObjParam( v.id, "NO_PROPERTIES" ) then
			prop = prop + 1
			obj = v
		end
	end
	if prop ~= 1 then
		return
	end
	local properties = Editor.getProperties( obj.type, obj.id )
	Editor.valuesPopup( (obj.proto or "Something without a name"), "id "..(obj.id or "-1"), properties,
		function(p_back)
			Editor.setProperties( obj.type, obj.id, p_back )
			Editor.changeState( Editor.STATE.RUNNING )
		end,
		function()
			Editor.changeState( Editor.STATE.RUNNING )
		end,
		obj
	)
end

function Editor.ContextMenu()
	Editor.changeState( Editor.STATE.IN_MENU )
	RemoveKeyProcessors()
	local x,y = GetMousePos()
	local cx, cy = GetCamPos()
	x,y = x - cx + CONFIG.scr_width/2, y - cy + CONFIG.scr_height/2
	local l_window = SimpleMenu.create( {x, y}, -1, -1, 10, 10 )
	local fclose = function()
		l_window:clear()
		l_window:hide()
		RestoreKeyProcessors()
		Editor.changeState( Editor.STATE.RUNNING )		
	end
	
	local enemies = 0
	local spawners = 0
	local waypoints = 0
	local properties = 0
	local polygons = 0
	local complete_polygons = 0
	local sprites = 0
	if Editor.selection then
		for k, v in pairs( Editor.selection ) do
			if v.type == Editor.TYPES.OBJ_ENEMY then
				if Editor.getObjParam( v.id, "CONVERTED" ) then
					spawners = spawners + 1
				else
					enemies = enemies + 1
				end
			end
			if v.type == Editor.TYPES.OBJ_WAYPOINT then
				waypoints = waypoints + 1
			end
			if not Editor.getObjParam( v.id, "NO_PROPERTIES" ) then
				properties = properties + 1
			end
			if Editor.getObjParam( v.id, "POLYGON" ) then
				polygons = polygons + 1
			end
			if v.polygon then
				complete_polygons = complete_polygons + 1
			end
			if v.sprite then
				sprites = sprites + 1
			end
		end
	end
	
	l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Undo (Ctrl+Z)",
			0,
			function()
				fclose()
				Editor.Undo()
			end ))
	l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Redo (Ctrl+Y)",
			0,
			function()
				fclose()
				Editor.Redo()
			end ))
	if #Editor.selection > 1 then
		l_window:add( MWSpace.create( 0, 5 ) )
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Group (Alt+G)",
			0,
			function()
				fclose()
				Editor.GroupObjects()
			end ))
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Ungroup (Ctrl+Alt+G)",
			0,
			function()
				fclose()
				Editor.UngroupObjects()
			end ))
	end
	if #Editor.selection > 0 then
		l_window:add( MWSpace.create( 0, 5 ) )
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Move (Ctrl+rclick, WASD)",
			0,
			function()
				fclose()
				Editor.action = { type = "MOVEMENT", from = {}, to = {}, text = "move object" }
				local tab
				for k, v in pairs(Editor.selection) do
					tab = { obj = v, pos = { x = v.aabb.p.x, y = v.aabb.p.y } }
					if v.sprite then
						tab.pos.z = v.sprite.z
					end
					table.insert( Editor.action.from, tab )
				end
				table.insert( Editor.threads, { Editor.drag_objects_thread, param =
					{ x = Editor.mouse_x, y = Editor.mouse_y, stop_condition = function() return Editor.held.mouse0 end,
					event = function()
						local tab
						for k, v in pairs(Editor.selection) do
							tab = { obj = v, pos = { x = v.aabb.p.x, y = v.aabb.p.y } }
							if v.sprite then
								tab.pos.z = v.sprite.z
							end
							table.insert( Editor.action.to, tab )
						end
					end }})
					Editor.AddHistoryEvent( Editor.action )
			end ))
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Directional cloning (L+dir)",
			0,
			function()
				fclose()
				Editor.DirectionalCloning( Editor.selection )
			end ))
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Set snapping anchor (Ctrl+P)",
			0,
			function()
				fclose()
				Editor.SetAnchor( Editor.selection )
			end ))
		if Editor.anchor then
			l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
				"Snap to anchor (P)",
				0,
				function()
					fclose()
					Editor.DropToAnchor( Editor.selection )
				end ))
		end
		l_window:add( MWSpace.create( 0, 5 ) )
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Previous tile ([)",
			0,
			function()
				fclose()
				Editor.prevTile()
			end ))
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Next tile (])",
			0,
			function()
				fclose()
				Editor.nextTile()
			end ))
		l_window:add( MWSpace.create( 0, 5 ) )
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Copy (Ctrl+C)",
			0,
			function()
				fclose()
				Editor.copy()
			end ))
		if polygons == 1 then
			l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
				"Add next polygon vertex (.)",
				0,
				function()
					fclose()
					Editor.nextPolygonMarker()
				end ))
		end
		if polygons > 0 then
			l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
				"Complete polygon (Ctrl+.)",
				0,
				function()
					fclose()
					Editor.completePolygon(false)
				end ))
			l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
				"Complete one-sided polygon (Ctrl+Shift+.)",
				0,
				function()
					fclose()
					Editor.completePolygon(true)
				end ))
		elseif complete_polygons == 1 then
			l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
				"Alter polygon (Ctrl+.)",
				0,
				function()
					fclose()
					Editor.decomposePolygon()
				end ))
		end
	else
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Create polygon (.)",
			0,
			function()
				fclose()
				Editor.createPolygon()
			end ))
	end
	if Editor.clipboard and #Editor.clipboard > 0 then
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Paste (Ctrl+V)",
			0,
			function()
				fclose()
				Editor.paste()
			end ))
	end
	if enemies > 0 then
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Convert enemy to spawner",
			0,
			function()
				fclose()
				Editor.SpawnerConversion( Editor.selection )
			end ))
	end
	if sprites > 0 then
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Mirror objects (Ctrl+F)",
			0,
			function()
				fclose()
				Editor.FlipObjects()
			end ))
	end
	if spawners > 0 then
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Convert spawner to enemy",
			0,
			function()
				fclose()
				Editor.EnemyConversion( Editor.selection )
			end ))
	end
	if enemies + waypoints == 1  then
		l_window:add( MWSpace.create( 0, 5 ) )
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Add waypoint (Ctrl+W)",
			0,
			function()
				fclose()
				Editor.Waypoint()
			end ))
	end
	if properties == 1 then
		l_window:add( MWSpace.create( 0, 5 ) )
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Import creation script...",
			0,
			function()
				fclose()
				Editor.changeState( Editor.STATE.ADDITIONAL_MENU )
				Editor._ImportScript( "temp",  function() 
						local obj
						for k,v in pairs( Editor.selection ) do
							if not Editor.getObjParam( v.id, "NO_PROPERTIES" ) then
								obj = v
								break
							end
						end
						Editor.setObjParam( obj.id, "CREATION_SCRIPT", Editor.scripts.temp )
						Editor.setObjParam( obj.id, "SPECIAL_CREATION", true )
					end,
					function() Editor.changeState( Editor.STATE.RUNNING ) end )
			end ))
		l_window:add( MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"Properties... (Enter)",
			0,
			function()
				fclose()
				Editor.PropertiesMenu()
			end ))
	end
	l_window:show()
	local w, h  = l_window.window.w, l_window.window.h
	if x + w > CONFIG.scr_width then
		x = CONFIG.scr_width - w
	end
	if y + h > CONFIG.scr_height then
		y = CONFIG.scr_height - h
	end
	l_window:move( x, y )
	local func = function()
		local mx, my = GetMousePos()
		local cmx, cmy = GetCamPos()
		mx,my = mx - cmx + CONFIG.scr_width/2, my - cmy + CONFIG.scr_height/2
		if mx < x or mx > x + w or my < y or my > y + h then
			fclose()
		end
	end 
	GlobalSetMouseKeyDownProc( func )
	GlobalSetKeyDownProc( func )
end

menus:hide(1)
menus:hide(2)
menus:hide(3)

--menus2:addPage( 1, ScrollingMenu.create( {0, 0}, -1, -1, 30, 5, 16 ) )

function menus2.show()
	Editor.palette.show()
end

function menus2.hide()
	Editor.palette.hide()
end

function Editor.newMapMenu()
	Editor.confirm( "Are you sure you want to close this map and create a new one?", 
		function()
			Editor.new()
			Editor.current_map = nil
			Editor.changeState( Editor.STATE.RUNNING )
		end,
		function()
			Editor.changeState( Editor.STATE.RUNNING )
		end)
end

menus3:addPage( 1, SimpleMenu.create( {CONFIG.scr_width, 0}, 1, -1, 30, 10 ) )
menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"START GAME (F5)",
			0,
			function(sender) 
				Editor.held[keys.tab] = false
				Editor.menu3:hide()
				Editor.menu2:hide()
				Editor.startMap(Editor.current_map)
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)

menus3:add( MWSpace.create( 0, 15 ) )
menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"NEW MAP (Ctrl+Shift+N)",
			0, 
			function(sender) 
				Editor.held[keys.tab] = false
				Editor.menu3:hide()
				Editor.menu2:hide()
				Editor.newMapMenu()
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)

menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"OPEN MAP (Ctrl+O)",
			0, 
			function(sender) 
				Editor.held[keys.tab] = false
				Editor.menu3:hide()
				Editor.menu2:hide()
				Editor.openMenu()
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)

menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"SAVE MAP (Ctrl+S)",
			0, 
			function(sender) 
				Editor.held[keys.tab] = false
				Editor.menu3:hide()
				Editor.menu2:hide()
				Editor.saveMenu()
			end,
			nil,
			{name="wakaba-widget"}
	),
	1

)
menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"SAVE MAP AS... (Ctrl+Shift+S)",
			0, 
			function(sender) 
				Editor.held[keys.tab] = false
				Editor.menu3:hide()
				Editor.menu2:hide()
				Editor.saveAsMenu()
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)
menus3:add( MWSpace.create( 0, 15 ) )
menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"LOAD PROTOTYPE... (Ctrl+L)",
			0, 
			function(sender) 
				Editor.menu3:hide()
				Editor.menu2:hide()
				Editor.loadProtoMenu()
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)
menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"IMPORT TILES... ",
			0, 
			function(sender) 
				Editor.menu3:hide()
				Editor.menu2:hide()
				Editor.ImportTiles()
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)
menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"SCRIPTS...",
			0, 
			function(sender) 
				Editor.menu3:hide()
				Editor.menu2:hide()
				Editor.ScriptMenu()
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)
menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"SELECT MUSIC...",
			0, 
			function(sender) 
				Editor.menu3:hide()
				Editor.menu2:hide()
				Editor.path = "sounds/music"
				Editor.changeState( Editor.STATE.ADDITIONAL_MENU )
				Editor.show_dir( "sounds", nil, nil,
					function( filename )
						Editor.mus_track = string.gsub( filename, "^sounds/", "" )
						StopBackMusic()
						Editor.changeState( Editor.STATE.RUNNING )
					end,
					function()
						Editor.mus_track = nil
						StopBackMusic()
						Editor.changeState( Editor.STATE.RUNNING )
					end,
					function( filename )
						PlayBackMusic(string.gsub( filename, "^sounds/", "" ))
					end)
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)
menus3:add( MWSpace.create( 0, 15 ) )
menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"SAVE PALETTE...",
			0, 
			function(sender) 
				Editor.palette.saveMenu()
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)
menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"OPEN PALETTE...",
			0, 
			function(sender) 
				Editor.palette.openMenu()
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)
menus3:add( MWSpace.create( 0, 15 ) )
menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"GRID...",
			0, 
			function(sender) 
				Editor.menu3:hide()
				Editor.menu2:hide()
				Editor.changeState( Editor.STATE.ADDITIONAL_MENU )
				local param = {
					{"show grid", "BOOL", tostring(Editor.grid.show) },
					{"snap to grid", "BOOL", tostring(Editor.grid.snap) },
					{"snap distance", "FLOAT", Editor.grid.dist},
					{"x", "INT", Editor.grid.x},
					{"y", "INT", Editor.grid.y},
					{"color", "COLOR", "["..table.concat(Editor.grid.color, ", ").."]"},
				}
				Editor.valuesPopup( "GRID SETUP", nil, param,
					function(p_back)
						Editor.grid_menu( p_back )
						Editor.changeState( Editor.STATE.RUNNING )
					end,
					function()
						Editor.changeState( Editor.STATE.RUNNING )
					end
				)
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)
menus3:add( MWSpace.create( 0, 15 ) )
menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"OPTIONS...",
			0, 
			function(sender) 
				Editor.held[keys.tab] = false
				Editor.menu3:hide()
				Editor.menu2:hide()
				Editor.changeState( Editor.STATE.ADDITIONAL_MENU )
				Editor.menu:show(4)
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)
--[[
menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"RESET K",
			0, 
			function(sender) 
				for key, value in pairs(Editor.selection) do
					if value.type == 13 then
						SetRibbonObjK(value.id, 0, 0)
					end
				end
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)
menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"TOGGLE X REPETITION",
			0, 
			function(sender) 
				for key, value in pairs(Editor.selection) do
					if value.type == 13 then
						value.repeat_x = not value.repeat_x
						SetRibbonObjRepitition(value.id, value.repeat_x, value.repeat_y)
					end
				end
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)
menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"TOGGLE Y REPETITION",
			0, 
			function(sender) 
				for key, value in pairs(Editor.selection) do
					if value.type == 13 then
						value.repeat_y = not value.repeat_y
						SetRibbonObjRepitition(value.id, value.repeat_x, value.repeat_y)
					end
				end
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)
menus3:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"MINIMAP: OFF",
			0, 
			function(sender) 
				Editor.minimap.show = not (Editor.minimap.show or false)
				if Editor.minimap.show then
					Editor.updateMinimap("RESIZE")
				else
					Editor.updateMinimap("HIDE")
				end
				WidgetSetCaption( sender, "MINIMAP: "..iff( Editor.minimap.show, "ON", "OFF" ) )
			end,
			nil,
			{name="wakaba-widget"}
	),
	1
)
--]]
menus3:hide(1)

function hsb2rgb( h, s, b )
	local hi = math.floor( h / 60 ) % 6
	local f = h/60 - math.floor(h/60)
	local p = b*(1-s)
	local q = b*(1-f*s)
	local t = b*(1-(1-f)*s)
	if hi == 0 then
		return b, t, p
	elseif hi == 1 then
		return q, b, p
	elseif hi == 2 then
		return p, b, t
	elseif hi == 3 then
		return p, q, b
	elseif hi == 4 then
		return t, p, b
	else
		return b, p, q
	end
end

function rgb2hsb( r, g, b )
	local max, min = math.max( r, g, b ), math.min( r, g, b )
	local h, s, v
	if max == min then
		h = 0
	elseif max == r and g >= b then
		h = 60 * (g-b)/(max-min)
	elseif max == r and g < b then
		h = 60 * (g-b)/(max-min) + 360
	elseif max == g then
		h = 60 * (b-r)/(max-min) + 120
	else
		h = 60 * (r-g)/(max-min) + 240
	end
	if max == 0 then
		s = 0
	else
		s = 1 - min/max
	end
	v = max
	return h, s, v
end

function ColorPicker( color )
	local color_str_length = 6
	RemoveKeyProcessors()
	local _color_picker_widgets = {}
	vars.r, vars.g, vars.b, vars.a = unpack( color )
	vars.h, vars.s, vars.v = rgb2hsb( vars.r, vars.g, vars.b )
	local window = Window.create( CONFIG.scr_width/2 - 200, CONFIG.scr_height/2 - 150, 400, 400, "window1" )
	local hsw = CreateWidget(constants.wt_Picture, "window", nil, CONFIG.scr_width/2-160, CONFIG.scr_height/2-128, 256, 256)
	table.insert( _color_picker_widgets, hsw )
	WidgetSetSprite(hsw, "editor_hs")
	local hsx, hsy = CONFIG.scr_width/2-160, CONFIG.scr_height/2-128
	local hsp = CreateWidget(constants.wt_Picture, "window", nil, CONFIG.scr_width/2-160, CONFIG.scr_height/2-128, 9, 9)
	table.insert( _color_picker_widgets, hsp )
	WidgetSetSprite(hsp, "editor_colorpicker", "hs")
	local bw = CreateWidget(constants.wt_Picture, "window", nil, CONFIG.scr_width/2+128, CONFIG.scr_height/2-128, 32, 256)
	table.insert( _color_picker_widgets, bw )
	local bx, by = CONFIG.scr_width/2+128, CONFIG.scr_height/2-128
	WidgetSetSprite(bw, "editor_b")
	local bp = CreateWidget(constants.wt_Picture, "window", nil, CONFIG.scr_width/2-160, CONFIG.scr_height/2-128, 9, 9)
	table.insert( _color_picker_widgets, bp )
	WidgetSetSprite(bp, "editor_colorpicker", "b")
	Editor.changeState( Editor.STATE.IN_MENU )
	local lr, lg, lb = hsb2rgb( vars.h, vars.s, 1 )
	WidgetSetSpriteColor( bw, { lr, lg, lb, 1 } )
	WidgetSetSpriteColor( hsw, { vars.v, vars.v, vars.v, 1 } )
	WidgetSetPos( hsp, hsx + (vars.h/360)*256-4, hsy + (1-vars.s)*256 - 4)
	WidgetSetPos( bp, bx - 15, by + (1-vars.v)*256 - 5 )
	
	local awl = CreateWidget( constants.wt_Label, "MWInputField", nil, CONFIG.scr_width/2 - 160, CONFIG.scr_height/2 + 138, 60, 15 )
	table.insert( _color_picker_widgets, awl )
	WidgetSetCaptionFont( awl, "dialogue" )
	WidgetSetCaption( awl, "ALPHA: " )
	local aw = CreateWidget( constants.wt_Textfield, "MWInputField", nil, CONFIG.scr_width/2 - 100, CONFIG.scr_height/2 + 138, 100, 15 )
	table.insert( _color_picker_widgets, aw )
	WidgetSetCaptionFont( aw, "dialogue" )
	WidgetSetCaption( aw, (vars.a or "as fuck!") )
	WidgetSetCaptionColor( aw, {.8, .8, .1, 1}, true )
	WidgetSetCaptionColor( aw, {1, 1, 1, 1}, false )
	local rwl = CreateWidget( constants.wt_Label, "MWInputField", nil, CONFIG.scr_width/2 - 160, CONFIG.scr_height/2 + 158, 60, 15 )
	table.insert( _color_picker_widgets, rwl )
	WidgetSetCaptionFont( rwl, "dialogue" )
	WidgetSetCaption( rwl, "RED: " )
	local rw = CreateWidget( constants.wt_Textfield, "MWInputField", nil, CONFIG.scr_width/2 - 100, CONFIG.scr_height/2 + 158, 100, 15 )
	table.insert( _color_picker_widgets, rw )
	WidgetSetCaptionFont( rw, "dialogue" )
	WidgetSetCaption( rw, (vars.r or "as fuck!") )
	WidgetSetCaptionColor( rw, {.8, .8, .1, 1}, true )
	WidgetSetCaptionColor( rw, {1, 1, 1, 1}, false )
	local rwb = CreateWidget( constants.wt_Textfield, "MWInputField", nil, CONFIG.scr_width/2 - 20, CONFIG.scr_height/2 + 158, 100, 15 )
	table.insert( _color_picker_widgets, rwb )
	WidgetSetCaptionFont( rwb, "dialogue" )
	WidgetSetCaption( rwb, math.floor((vars.r or 0) * 255) )
	WidgetSetCaptionColor( rwb, {.4, .4, .1, 1}, true )
	WidgetSetCaptionColor( rwb, {.8, .8, .8, 1}, false )
	local gwl = CreateWidget( constants.wt_Label, "MWInputField", nil, CONFIG.scr_width/2 - 160, CONFIG.scr_height/2 + 173, 60, 15 )
	table.insert( _color_picker_widgets, gwl )
	WidgetSetCaptionFont( gwl, "dialogue" )
	WidgetSetCaption( gwl, "GREEN: " )
	local gw = CreateWidget( constants.wt_Textfield, "MWInputField", nil, CONFIG.scr_width/2 - 100, CONFIG.scr_height/2 + 173, 100, 15 )
	table.insert( _color_picker_widgets, gw )
	WidgetSetCaptionFont( gw, "dialogue" )
	WidgetSetCaption( gw, (vars.g or "as fuck!") )
	WidgetSetCaptionColor( gw, {.8, .8, .1, 1}, true )
	WidgetSetCaptionColor( gw, {1, 1, 1, 1}, false )
	local gwb = CreateWidget( constants.wt_Textfield, "MWInputField", nil, CONFIG.scr_width/2 - 20, CONFIG.scr_height/2 + 173, 100, 15 )
	table.insert( _color_picker_widgets, gwb )
	WidgetSetCaptionFont( gwb, "dialogue" )
	WidgetSetCaption( gwb, math.floor((vars.g or 0)*255) )
	WidgetSetCaptionColor( gwb, {.4, .4, .1, 1}, true )
	WidgetSetCaptionColor( gwb, {.8, .8, .8, 1}, false )
	local bwl = CreateWidget( constants.wt_Label, "MWInputField", nil, CONFIG.scr_width/2 - 160, CONFIG.scr_height/2 + 188, 60, 15 )
	table.insert( _color_picker_widgets, bwl )

	WidgetSetCaptionFont( bwl, "dialogue" )
	WidgetSetCaption( bwl, "BLUE: " )
	local cbw = CreateWidget( constants.wt_Textfield, "MWInputField", nil, CONFIG.scr_width/2 - 100, CONFIG.scr_height/2 + 188, 100, 15 )
	table.insert( _color_picker_widgets, cbw )
	WidgetSetCaptionFont( cbw, "dialogue" )
	WidgetSetCaption( cbw, (vars.b or "as fuck!") )
	WidgetSetCaptionColor( cbw, {.8, .8, .1, 1}, true )
	WidgetSetCaptionColor( cbw, {1, 1, 1, 1}, false )
	local cbwb = CreateWidget( constants.wt_Textfield, "MWInputField", nil, CONFIG.scr_width/2 - 20, CONFIG.scr_height/2 + 188, 100, 15 )
	table.insert( _color_picker_widgets, cbwb )
	WidgetSetCaptionFont( cbwb, "dialogue" )
	WidgetSetCaption( cbwb, math.floor((vars.b or 0) * 255) )
	WidgetSetCaptionColor( cbwb, {.4, .8, .4, 1}, true )
	WidgetSetCaptionColor( cbwb, {.8, .8, .8, 1}, false )
	
	local function update_color()
		WidgetSetCaption( rw, string.sub( tostring(vars.r), 1, color_str_length ) )
		WidgetSetCaption( gw, string.sub( tostring(vars.g), 1, color_str_length ) )
		WidgetSetCaption( cbw, string.sub( tostring(vars.b), 1, color_str_length ) )
		WidgetSetCaption( rwb, tostring( math.floor(vars.r*255)) )
		WidgetSetCaption( gwb, tostring( math.floor(vars.g*255)) )
		WidgetSetCaption( cbwb, tostring( math.floor(vars.b*255)) )
	end
	
	local preview_back1 = CreateWidget(constants.wt_Widget, "preview", nil, CONFIG.scr_width/2+116, CONFIG.scr_height/2+138, 32, 84)
	table.insert( _color_picker_widgets, preview_back1 )
	WidgetSetColorBox( preview_back1, {0, 0, 0, 1} )
	WidgetSetZ( preview_back1, 0.95 )
	local preview_back2 = CreateWidget(constants.wt_Widget, "preview", nil, CONFIG.scr_width/2+90, CONFIG.scr_height/2+164, 84, 32)
	table.insert( _color_picker_widgets, preview_back2 )
	WidgetSetColorBox( preview_back2, {1, 1, 1, 1} )
	WidgetSetZ( preview_back2, 0.94 )
	local preview = CreateWidget(constants.wt_Widget, "preview", nil, CONFIG.scr_width/2+100, CONFIG.scr_height/2+148, 64, 64)
	table.insert( _color_picker_widgets, preview )
	WidgetSetColorBox( preview, color )
	WidgetSetBorder( preview, true )
	WidgetSetBorderColor( preview, { 1-color[1], 1-color[2], 1-color[3], 1 }, false )
	WidgetSetKeyInputProc( aw, function()
			local text = WidgetGetCaption( aw )

			text = tonumber( text )
			if text then
				WidgetSetCaptionColor( aw, {.8, .8, .1, 1}, true )
				WidgetSetCaptionColor( aw, {1, 1, 1, 1}, false )
				vars.a = text
				WidgetSetColorBox( preview, { vars.r, vars.g, vars.b, vars.a } )
				WidgetSetBorderColor( preview, { 1-vars.r, 1-vars.g, 1-vars.b, 1 }, false )
			else
				WidgetSetCaptionColor( aw, {1, .5, .5, 1}, true )
				WidgetSetCaptionColor( aw, {.5, 0, 0, 1}, false )
			end
	end )
	local function colorInput( widget, color, other_widget )
		WidgetSetKeyInputProc( widget, function()
			local text = WidgetGetCaption( widget )
			text = tonumber( text )
			if text and text >= 0 and text <= 1 then
				WidgetSetCaptionColor( widget, {.8, .8, .1, 1}, true )
				WidgetSetCaptionColor( widget, {1, 1, 1, 1}, false )
				vars[color] = text
				vars.h, vars.s, vars.v = rgb2hsb( vars.r, vars.g, vars.b )
				local lr, lg, lb = hsb2rgb( vars.h, vars.s, 1 )
				WidgetSetCaption( other_widget, tostring(math.floor(255 * text)) )
				WidgetSetSpriteColor( bw, { lr, lg, lb, 1 } )
				WidgetSetSpriteColor( hsw, { vars.v, vars.v, vars.v, 1 } )
				WidgetSetColorBox( preview, { vars.r, vars.g, vars.b, vars.a } )
				WidgetSetBorderColor( preview, { 1-vars.r, 1-vars.g, 1-vars.b, 1 }, false )
				WidgetSetPos( hsp, hsx + (vars.h/360)*256-4, hsy + (1-vars.s)*256 - 4)
				WidgetSetPos( bp, bx - 15, by + (1-vars.v)*256 - 5 )
			else
				WidgetSetCaptionColor( widget, {1, .5, .5, 1}, true )
				WidgetSetCaptionColor( widget, {.5, 0, 0, 1}, false )
			end
		end)
	end
	local function colorInputByte( widget, color, other_widget )
		WidgetSetKeyInputProc( widget, function()
			local text = WidgetGetCaption( widget )
			text = tonumber( text )
			if text and text >= 0 and text <= 255 and math.floor(text) == text then
				WidgetSetCaptionColor( widget, {.4, .4, .1, 1}, true )
				WidgetSetCaptionColor( widget, {.8, .8, .8, 1}, false )
				vars[color] = text / 255
				vars.h, vars.s, vars.v = rgb2hsb( vars.r, vars.g, vars.b )
				local lr, lg, lb = hsb2rgb( vars.h, vars.s, 1 )
				WidgetSetCaption( other_widget, string.sub(tostring(text / 255), 1, color_str_length) )
				WidgetSetSpriteColor( bw, { lr, lg, lb, 1 } )
				WidgetSetSpriteColor( hsw, { vars.v, vars.v, vars.v, 1 } )
				WidgetSetColorBox( preview, { vars.r, vars.g, vars.b, vars.a } )
				WidgetSetBorderColor( preview, { 1-vars.r, 1-vars.g, 1-vars.b, 1 }, false )
				WidgetSetPos( hsp, hsx + (vars.h/360)*256-4, hsy + (1-vars.s)*256 - 4)
				WidgetSetPos( bp, bx - 15, by + (1-vars.v)*256 - 5 )
			else
				WidgetSetCaptionColor( widget, {1, .5, .5, 1}, true )
				WidgetSetCaptionColor( widget, {.5, 0, 0, 1}, false )
			end
		end)
	end
	colorInput( rw, "r", rwb )
	colorInput( gw, "g", gwb )
	colorInput( cbw, "b", cbwb )
	colorInputByte( rwb, "r", rw )
	colorInputByte( gwb, "g", gw )
	colorInputByte( cbwb, "b", cbw )
	local ok = CreateWidget(constants.wt_Button, "ok", nil, CONFIG.scr_width/2-10, CONFIG.scr_height/2+220, 20, 10)
	table.insert( _color_picker_widgets, ok )
	WidgetSetCaptionColor( ok, {.8, .8, .1, 1}, true )
	WidgetSetCaptionColor( ok, {1, 1, 1, 1}, false )
	WidgetSetCaption( ok, "OK" )
	local func = function( key )
		if key > 0 then return false end
		local x,y = Editor.mouse_relative_x, Editor.mouse_relative_y
		if x >= hsx and x < hsx+256 and y >= hsy and y < hsy+256 then
			vars.h = ((x - hsx)/256) * 360
			if vars.h == 360 then vars.h = 0 end
			vars.s = (hsy + 256 - y)/256
			local lr, lg, lb = hsb2rgb( vars.h, vars.s, 1 )
			WidgetSetSpriteColor( bw, { lr, lg, lb, 1 } )
			vars.r, vars.g, vars.b = hsb2rgb( vars.h, vars.s, vars.v )
			update_color()
			WidgetSetColorBox( preview, { vars.r, vars.g, vars.b, vars.a } )
			WidgetSetBorderColor( preview, { 1-vars.r, 1-vars.g, 1-vars.b, 1 }, false )
		elseif x >= bx and x < bx+32 and y >= by and y < by+256 then
			vars.v = (by + 256 - y) / 256
			WidgetSetSpriteColor( hsw, { vars.v, vars.v, vars.v, 1 } )
			vars.r, vars.g, vars.b = hsb2rgb( vars.h, vars.s, vars.v )
			WidgetSetColorBox( preview, { vars.r, vars.g, vars.b, vars.a } )
			WidgetSetBorderColor( preview, { 1-vars.r, 1-vars.g, 1-vars.b, 1 }, false )
			update_color()
		end
		WidgetSetPos( hsp, hsx + (vars.h/360)*256-4, hsy + (1-vars.s)*256 - 4)
		WidgetSetPos( bp, bx - 15, by + (1-vars.v)*256 - 5 )
		return false
	end
	GlobalSetKeyDownProc( func )
	GlobalSetMouseKeyDownProc( func )
	WidgetSetLMouseClickProc( ok, function()
		RestoreKeyProcessors()
		for k, v in pairs( _color_picker_widgets ) do
			DestroyWidget( v )
		end
		window:destroy()
		vars.red, vars.green, vars.blue, vars.alpha = vars.r, vars.g, vars.b, vars.a
		Editor.colorSelected()
	end )
end

function Editor.FloatyText( text )
	Editor.FloatyLocationText( text, GetMousePos() )
end

function Editor.FloatyLocationText( text, x, y )
	Resume( NewMapThread( function()
		local wd = GetCaptionSize( "default", text )
		local x, y = x, y
		x = x - wd/2
		local w = CreateWidget( constants.wt_Label, "message", nil, x, y, 1, 1 )
		WidgetSetFixedPosition( w, false )
		WidgetSetCaption( w, text )
		local last_update = GetCurTime()
		local spare_time = 0
		local i = 0
		while i < 100 do
			WidgetSetPos( w, x, y-i/10 )
			WidgetSetCaptionColor( w, {1, 1, 1, 1-0.01*i}, false )
			while spare_time < 5 do
				Wait(1)
				spare_time = GetCurTime() - last_update
				last_update = GetCurTime()
			end
			while spare_time >= 5 do
				spare_time = spare_time - 5
				i = i + 1
			end
		end
		DestroyWidget( w )
	end ))
end

function Editor.loadProtoMenu()
	Editor.changeState( Editor.STATE.ADDITIONAL_MENU )
	Editor.menu:show(1)
end

function Editor.restoreZ()
end

function Editor.hideSelection()
--[[	EditorToggleBorders(false)
	if Editor.selection then
		for k, v in pairs( Editor.selection ) do
			SetPhysObjBorderColor(v.id, {1,0.5,0,0}, false)
		end
	end
	local map_data = EditorDumpMap()
	local oldz = {}
	for key, o in pairs( map_data ) do
		if o.sprite and o.sprite.z > 0.85 and not Editor.getObjParam( o.id, "NOT_REAL" ) then
			oldz[o.id] = o.sprite.z
			SetObjPos( o.id, o.aabb.p.x, o.aabb.p.y, 0.85 )
		end
	end
	Editor.restoreZ = function()
		Editor.restoreZ = function() end
		local obj
		for k, v in pairs( oldz ) do
			obj = GetObject( k )
			SetObjPos( k, obj.aabb.p.x, obj.aabb.p.y, v )
		end
	end
	WidgetSetBorder( Editor.game_screen_widget, false )]]
end

function Editor.restoreSelection()
--[[	EditorToggleBorders(Editor.old_borders or false)
	if Editor.selection then
		for k, v in pairs( Editor.selection ) do
			if not Editor.getObjParam( v.id, "NO_BORDER" ) then
				SetPhysObjBorderColor(v.id, Editor.selection_color, true)
			end
		end
	end
	WidgetSetBorder( Editor.game_screen_widget, Editor.show_game_screen )
	Editor.restoreZ()]]
end

menus:addPage( 4, SimpleMenu.create( {CONFIG.scr_width/2, CONFIG.scr_height/2}, 0, 0, 30, 10 ) )
menus:add( MWSpace.create( 15, 15 ), 4 )
menus:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"Show object borders: OFF",
			0, 
			function(sender) 
				Editor.old_borders = not (Editor.old_borders or false) 
				EditorToggleBorders(Editor.old_borders) 
				WidgetSetCaption( sender, "Show object borders: "..iff( Editor.old_borders, "ON", "OFF" ) )
			end,
			nil,
			{name="wakaba-widget"}
	),
	4
)
menus:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"Show selection: ON",
			0, 
			function(sender) 
				if Editor.selection_color[4] > 0 then
					Editor.old_selection_alpha = Editor.selection_color[4]
					Editor.selection_color[4] = 0
					WidgetSetCaption( sender, "Show selection: OFF")
				else
					Editor.selection_color[4] = (Editor.old_selection_alpha or 0)
					WidgetSetCaption( sender, "Show selection: ON")
				end
				for key, value in pairs(Editor.selection) do
					SetPhysObjBorderColor( value.id, Editor.selection_color, true )
				end
			end,
			nil,
			{name="wakaba-widget"}
	),
	4
)
menus:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"Show game screen size: OFF",
			0, 
			function(sender) 
				Editor.show_game_screen = not Editor.show_game_screen
				if Editor.show_game_screen then
					WidgetSetBorder( Editor.game_screen_widget, true )
					WidgetSetCaption( sender, "Show game screen size: ON")
				else
					WidgetSetBorder( Editor.game_screen_widget, false )
					WidgetSetCaption( sender, "Show game screen size: OFF")
				end
			end,
			nil,
			{name="wakaba-widget"}
	),
	4
)
menus:add(
	MWFunctionButton.create(
			{name="dialogue", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"Background color...",
			0, 
			function(sender) 
				Editor.colorSelected = function()
					CONFIG.backcolor_r = vars.red
					CONFIG.backcolor_g = vars.green
					CONFIG.backcolor_b = vars.blue
					LoadConfig()
				end
				ColorPicker( { CONFIG.backcolor_r, CONFIG.backcolor_g, CONFIG.backcolor_b, 1 } )
			end,
			nil,
			{name="wakaba-widget"}
	),
	4
)
menus:add( MWSpace.create( 15, 15 ), 4 )
menus:add(
	MWFunctionButton.create(
			{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}}, 
			"OK",
			0, 
			function(sender) 
				Editor.changeState( Editor.STATE.RUNNING )
				Editor.menu:hide()
			end,
			nil,
			{name="wakaba-widget"}
	),
	4
)
menus:hide(4)

Editor.menu = menus
Editor.menu2 = menus2
Editor.menu3 = menus3
