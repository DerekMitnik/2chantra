Editor = {}

Editor.TYPES = {
	OBJ_PLAYER = constants.objPlayer,
	OBJ_SPRITE = constants.objSprite,
	OBJ_ENEMY = constants.objEnemy,
	OBJ_ITEM = constants.objItem,
	OBJ_SPAWNER = constants.objSpawner,
	OBJ_TILE = constants.objTile,
	OBJ_RIBBON = constants.objRibbon,
	OBJ_WAYPOINT = constants.objWaypoint,
}
Editor.MAP_FORMAT_VERSION = 4
need("routines.lua")
need("menus.lua")
need("editor/editor-undo.lua")
need("editor/editor-properties.lua")
need("editor/editor-saveload.lua")
need("editor/editor-snap.lua")
need("editor/editor-palette.lua")
need("editor/editor-scripts.lua")
need("editor/editor-selection.lua")
need("editor/editor-waypoints.lua")
need("editor/editor-proto.lua")
need("editor/editor-menus.lua")
need("editor/editor-spawnpoints.lua")
need("editor/editor-loader.lua")

__setfenv = setfenv
setfenv = debug.setfenv

function Editor.destroy()
	DestroyWidget( Editor.game_screen_widget )
	Editor.menu:clear()
	Editor.menu:hide()
	if Editor.menu2.clear then
		Editor.menu2:clear()
		Editor.menu2:hide()
	end
	RemoveKeyProcessors()
	RestoreKeyProcessors = function() end
	Editor.menu3:clear()
	Editor.menu3:hide()
end

function Editor.changeState( new_state )
	Editor.state = new_state
	if new_state == Editor.STATE.RUNNING then
		Editor.restoreSelection()
		Editor.held[keys.lctrl] = false
		Editor.held[keys.rctrl] = false
		Editor.held[keys.tab] = false
	else
		Editor.hideSelection()
	end
end

function Editor.init()
	Editor.ClearParam()
	Editor.ClearHistory()
	Editor.EmptyVoid()
	Editor.InitSnap()
	Editor.InitSelection()
	Editor.InitWaypoints()
--	Editor.initProtoEditor()
	Editor.mus_track = nil
	Editor.global_scale = 1;
	if not Editor.created then
		Editor.created = true
		CONFIG.backcolor_r = 00.28
		CONFIG.backcolor_g = 00.28
		CONFIG.backcolor_b = 00.28
		--[[if CONFIG.scr_width < 1024 then 
			CONFIG.scr_width = 1024
			CONFIG.window_width = 1024
			CONFIG.scr_height = 768
			CONFIG.window_height = 768
		end--]]
		LoadConfig()
		Editor.scr_width, Editor.scr_height = CONFIG.scr_width, CONFIG.scr_height
		Editor.selection_color = {1, 1, 1, 0.5}
		Editor.show_game_screen = false
		Editor.game_screen_widget = CreateWidget( constants.wt_Widget, "game screen", nil, CONFIG.scr_width/2-320, CONFIG.scr_height/2-240, 640, 480 )
		WidgetSetColorBox( Editor.game_screen_widget, {0, 0, 0, 0} )
		WidgetSetBorderColor( Editor.game_screen_widget, {1, .5, .5, 1}, false )
		WidgetSetBorderColor( Editor.game_screen_widget, {1, .5, .5, 1}, true )
		Editor.STATE = { RUNNING = 1, IN_MENU = 2, ADDITIONAL_MENU = 3, PAINTING = 4 }
		Editor.tools = Editor.createTools()
		Editor.widgets = {}
		Editor.widgets_v = {}
		Editor.shown = {}
		Editor.populateWidgetGroups()
		Editor.threads = {}
		RegEditorGetObjectsProc(Editor.selectedObjectsProc);
		GlobalSetKeyReleaseProc(Editor.keyRelease)
		GlobalSetKeyDownProc(Editor.keyProcessor)
		GlobalSetMouseKeyReleaseProc(Editor.mouseRelease);
		GlobalSetMouseKeyDownProc(Editor.mousePress);
	end
	Editor.state = Editor.STATE.RUNNING
	Editor.x, Editor.y = 0, 0
	Editor.tool = Editor.tools.selection
	Editor.clipboard = {}
	Editor.held = {}
	Editor.key_held = {}
	Editor.groups = {}
	Editor.ClearPalette()
	Editor.menu2:hide()
	Editor.menu3:hide()
	Editor.minimap = {}
	Editor.updateGrid()
--	Editor.init = function () end
--	Editor.editProto( nil, "proto/characters/unylchan.lua" )
end

function Editor.addWidgetGroup( name, group )
	if Editor.widgets[name] then
		for k, v in pairs(Editor.widgets[name]) do
			DestroyWidget( v )
		end
	end
	Editor.widgets[name] = group
	Editor.widgets_v[name] = {}
	Editor.showWidgetGroup( name, false )
end

function Editor.showWidgetGroup( name, show )
	Editor.shown[name] = false
	if show then
		for key, value in pairs( Editor.widgets[name] ) do
			WidgetSetVisible( value, Editor.widgets_v[name][key] or true )
		end
	else
		for key, value in pairs( Editor.widgets[name] ) do
			Editor.widgets_v[name][key] = WidgetGetVisible( value )
			WidgetSetVisible( value, false )
		end
	end
end
----------------------------------------------------------------------------------------------------------------------------------
--							TOOLS									--
----------------------------------------------------------------------------------------------------------------------------------
function Editor._tool()
	return 
	{
		onMousePress = function() end,
		onMouseRelease = function() end,
		onKey = function() end,
		onKeyPress = function() end,
		onKeyRelease = function() end,
		selection = function() end
	}
end

function Editor.prevTile()
	if #Editor.selection > 0 then
		local action = {type="FRAME", from={}, to={}, text="change tile"}
		for key, value in pairs( Editor.selection ) do
			if value.type == Editor.TYPES.OBJ_TILE then
				table.insert( action.from, { obj = value, frame = value.sprite.frame } )
				value.sprite.frame = value.sprite.frame - 1
				SetObjAnim( value.id, value.sprite.frame, false )
				table.insert( action.to, { obj = value, frame = value.sprite.frame } )
			elseif value.type == Editor.TYPES.OBJ_SPRITE then
				table.insert( action.from, { obj = value, frame = value.sprite.cur_anim } )
				local anim = tonumber( string.match( value.sprite.cur_anim, "^tile(%d+)") or " " )
				if anim then
					anim = anim - 1
					if anim <= 0 then anim = "idle" else anim = "tile"..anim end
					SetObjAnim( value.id, anim, false )
				end
				Editor.selection[key] = GetObject( value.id )
				local x, y =  Editor.selection[key].aabb.p.x,  Editor.selection[key].aabb.p.y -  Editor.selection[key].aabb.H
				Editor.FloatyLocationText( Editor.selection[key].sprite.cur_anim, x, y )
				table.insert( action.to, { obj = value, frame = Editor.selection[key].sprite.cur_anim } )
			end
		end
		Editor.AddHistoryEvent( action )
	end
end

function Editor.nextTile()
	if #Editor.selection > 0 then
		local action = {type="FRAME", from={}, to={}, text="change tile"}
		for key, value in pairs( Editor.selection ) do
			if value.type == Editor.TYPES.OBJ_TILE then
				table.insert( action.from, { obj = value, frame = value.sprite.frame } )
				value.sprite.frame = value.sprite.frame + 1
				SetObjAnim( value.id, value.sprite.frame, false )
				table.insert( action.to, { obj = value, frame = value.sprite.frame } )
			elseif value.type == Editor.TYPES.OBJ_SPRITE then
				table.insert( action.from, { obj = value, frame = value.sprite.cur_anim } )
				local anim = tonumber( string.match( value.sprite.cur_anim, "^tile(%d+)") or " " )
				if anim then
					anim = anim + 1
					SetObjAnim( value.id, "tile"..anim, false )
				else
					SetObjAnim( value.id, "tile1", false )
					SetObjPos( value.id, value.aabb.p.x, value.aabb.p.y )
				end
				Editor.selection[key] = GetObject( value.id )
				local x, y =  Editor.selection[key].aabb.p.x,  Editor.selection[key].aabb.p.y -  Editor.selection[key].aabb.H
				Editor.FloatyLocationText( Editor.selection[key].sprite.cur_anim, x, y )
				table.insert( action.to, { obj = value, frame = Editor.selection[key].sprite.cur_anim } )
			end
		end
		Editor.AddHistoryEvent( action )
	end
end

function Editor.createTools()
	local ret = {}

	ret.selection = Editor._tool()
	ret.selection.onMousePress = 
	function( key )
		if key == 0 then
			WidgetSetColorBox( Editor.widgets.selection[1], Editor.selection_color )
			table.insert( Editor.threads, { Editor.resize_widget_thread, param =
				{ x = Editor.mouse_x, y = Editor.mouse_y, widget = Editor.widgets.selection[1], event = 
					function( param )
						Editor.showWidgetGroup( "selection", false )
						Editor.tool.area = ( math.abs(Editor.mouse_x - param.x) > 5 and math.abs(Editor.mouse_y - param.y) > 5 )
						local x,y = Editor.mouse_x, Editor.mouse_y
						if not Editor.tool.area then
							x,y = param.x, param.y
						end
						EditorGetObjects(param.x,param.y,x,y)
					end,
				  event_param = { x = Editor.mouse_x, y = Editor.mouse_y },
				  stop_condition = "KEY_RELEASED", key = "mouse0"
				 }})
		elseif key == 1 and Editor.held[keys.lctrl] then
			Editor.action = { type = "MOVEMENT", from = {}, to = {}, text = "move object" }
			local tab
			for k, v in pairs(Editor.selection) do
				tab = { obj = v, pos = { x = v.aabb.p.x, y = v.aabb.p.y } }
				if v.sprite then
					tab.pos.z = v.sprite.z
				end
				table.insert( Editor.action.from, tab )
			end
			table.insert( Editor.threads, { Editor.drag_objects_thread, param =
				{ x = Editor.mouse_x, y = Editor.mouse_y, stop_condition = function() return not Editor.held[keys.lctrl] or not Editor.held.mouse1 end,
				event = function()
					local tab
					for k, v in pairs(Editor.selection) do
						tab = { obj = v, pos = { x = v.aabb.p.x, y = v.aabb.p.y } }
						if v.sprite then
							tab.pos.z = v.sprite.z
						end
						table.insert( Editor.action.to, tab )
					end
				end }})
				Editor.AddHistoryEvent( Editor.action )
		elseif key == 1 then
			local rclick_time = GetCurTime()
			table.insert( Editor.threads, { Editor.drag_camera_thread, param =
				{ x = Editor.mouse_x, y = Editor.mouse_y, stop_condition = "KEY_RELEASED", key = "mouse1",
				event = function() if GetCurTime() - rclick_time < 300 then Editor.ContextMenu() end end }})
		end
	end
	ret.selection.selectionEvent =
	function( objects )
		if not Editor.state == Editor.STATE.RUNNING then
			return
		end
		for k,v in pairs( objects ) do
			if Editor.getObjParam( v, "DONT_SELECT" ) then
				table.remove( objects, k )
			end
		end
		local modifier = "NONE"

		if Editor.held[keys.lshift] or Editor.held[keys.rshift] then modifier = "ADD"
		elseif Editor.held[keys.lalt] or Editor.held[keys.ralt] then modifier = "REMOVE"
		end

		if modifier == "NONE" then
			Editor.newSelection()
		end

		local obj = {}
		for key, value in pairs( objects ) do
			table.insert( obj, GetObject(value) )
		end
		--table.sort( obj, function(a,b) if not a.sprite or not b.sprite then return true end return a.sprite.z>b.sprite.z end )
		if Editor.tool.front_only then
			obj = { obj[1] }
		end

		if modifier == "NONE" then
			Editor.selectionSet(nil)
		end
		if Editor.tool.area then
			for key, value in pairs(obj) do
				if modifier == "ADD" then
					Editor.selectionSet( value, true )
				elseif modifier == "REMOVE" then
					Editor.selectionSet( value, false )
				else
					Editor.selectionSet( value, true )
				end
			end
		else
			if Editor.last_selected then
				local os = obj[1]
				for i = 1, #obj-1 do
					if obj[i].id == Editor.last_selected.id then
						os = obj[i+1]
						break
					end
				end
				Editor.last_selected = os
				if modifier ~= "ADD" then
					Editor.selectionSet( Editor.last_selected, false )
				end
				Editor.selectionSet( os, true )
			else
				Editor.last_selected = obj[1]
				if modifier == "REMOVE" then
					Editor.selectionSet( obj[1], false )
				else
					Editor.selectionSet( obj[1], true )
				end
			end
		end
	end
	ret.selection.onKey = function(key)
		local dx, dy = 0, 0
		local got_change = false
		if key == keys.w then
			got_change = true
			dy = -1
		elseif key == keys.s then
			got_change = true
			dy = 1
		elseif key == keys.a then
			got_change = true
			dx = -1
		elseif key == keys.d then
			got_change = true
			dx = 1
		elseif (Editor.held[keys.lctrl] or Editor.held[keys.ctrl]) and key == keys.t then
			Editor.TestRun()
		elseif key == keys.left and Editor.held[keys.lctrl] then
			Editor.ResizePhysic( 4 )
		elseif key == keys.right and Editor.held[keys.lctrl] then
			Editor.ResizePhysic( 6 )
		elseif key == keys.up and Editor.held[keys.lctrl] then
			Editor.ResizePhysic( 8 )
		elseif key == keys.down and Editor.held[keys.lctrl] then
			Editor.ResizePhysic( 2 )
		elseif key >= keys["0"] and key <= keys["9"] then
			if (Editor.held[keys.lctrl] or Editor.held[keys.rctrl]) and key ~= keys["0"] then
				Editor.saveSelection( key - keys["0"] )
			elseif Editor.held[keys.lshift] or Editor.held[keys.rshift] then
				Editor.addSavedSelection( key - keys["0"] )
			else
				Editor.restoreSavedSelection( key - keys["0"] )
			end
		elseif key == keys.f and (Editor.held[keys.lctrl] or Editor.held[keys.rctrl]) then
			Editor.FlipObjects()
		end
		if got_change then
			local action = { type = "MOVEMENT", from = {}, to = {}, text = "move object" }
			local tab
			for key, value in pairs( Editor.selection ) do
				tab = { obj = value, pos = { x = value.aabb.p.x, y = value.aabb.p.y } }
				if value.sprite then
					tab.pos.z = value.sprite.z
				end
				table.insert( action.from, tab )
				value.aabb.p.x, value.aabb.p.y = value.aabb.p.x + dx, value.aabb.p.y + dy
				SetObjPos( value.id, value.aabb.p.x, value.aabb.p.y )
				tab = { obj = value, pos = { x = value.aabb.p.x, y = value.aabb.p.y } }
				if value.sprite then
					tab.pos.z = value.sprite.z
				end
				table.insert( action.to, tab )
			end
			Editor.AddHistoryEvent( action )
		end
	end
	ret.selection.onKeyPress = function(key)
		if key == keys.lbracket then
			Editor.prevTile()
			return
		elseif key == keys.rbracket then
			Editor.nextTile()
			return
		elseif key == keys.enter then
			Editor.PropertiesMenu()
		elseif key == keys.delete then
			if #Editor.selection > 0 then
				local action = { type = "DELETION", obj = {}, text="delete objects" }
				for key, value in pairs( Editor.selection ) do
					--SetObjDead( value.id )
					table.insert( action.obj, { param = value, group = Editor.groups[ value.id ] } )
					Editor.BanishToVoid( value )
					if Editor.groups[ value.id ] then
						for k, v in pairs( Editor.groups[ value.id ] ) do
							if v.id == value.id then
								table.remove( Editor.groups[value.id], k )
								break
							end
						end
						--Editor.groups[value.id] = nil
					end
				end
				Editor.AddHistoryEvent( action )
				Editor.updateMinimap("UPDATE")
				Editor.selection = {}
			end
		elseif key == keys.k then
			Editor.tool.old_k = {}
			for key, value in pairs( Editor.selection ) do
				if value.type == Editor.TYPES.OBJ_RIBBON then
					Editor.tool.old_k[value.id] = shallow_copy(value.k)
				end
			end
			table.insert( Editor.threads, { Editor.change_value_thread, param =
				{ x = Editor.mouse_x, y = Editor.mouse_y, k = 10, update_event =
					function( cx, cy )
						for key, value in pairs( Editor.selection ) do
							if value.type == Editor.TYPES.OBJ_RIBBON then
								local k = Editor.tool.old_k[value.id] or {x=0, y=0}
								value.k.x = k.x + cx
								value.k.y = k.y + cy
								SetRibbonObjK( value.id, value.k.x, value.k.y )
							end
						end
					end,
					event_param = { x = Editor.mouse_x, y = Editor.mouse_y },
	  				stop_condition = "KEY_RELEASED", key = keys.k}
			})
		elseif key == keys.f5 then
			Editor.startMap(Editor.current_map)
			return
		elseif key == keys.esc and Editor.editor_state == "off" then
			Editor.open(Editor.current_map)
			return
		elseif key == keys.n and (Editor.held[keys.lctrl] or Editor.held[keys.rctrl]) and (Editor.held[keys.lshift] or Editor.held[keys.rshift]) then
			Editor.newMapMenu()
			return
		elseif key == keys.s and (Editor.held[keys.lctrl] or Editor.held[keys.rctrl]) and (Editor.held[keys.lshift] or Editor.held[keys.rshift]) then
			Editor.saveAsMenu()
			return
		elseif key == keys.s and (Editor.held[keys.lctrl] or Editor.held[keys.rctrl]) then
			Editor.saveMenu()
			return
		elseif key == keys.o and (Editor.held[keys.lctrl] or Editor.held[keys.rctrl]) then
			Editor.openMenu()
			return
		elseif key == keys.p and (Editor.held[keys.lctrl] or Editor.held[keys.rctrl]) and #Editor.selection > 0 then
			Editor.SetAnchor( Editor.selection )
			return
		elseif key == keys.p and #Editor.selection > 0 then
			Editor.DropToAnchor( Editor.selection )
			return
		elseif key == keys.g and (Editor.held[keys.lctrl] or Editor.held[keys.rctrl]) and (Editor.held[keys.lalt] or Editor.held[keys.ralt]) then
			Editor.toggleSnap()
		elseif key == keys.g and (Editor.held[keys.lctrl] or Editor.held[keys.rctrl]) then
			Editor.toggleGrid()
		elseif key == keys.period and (Editor.held[keys.lctrl] or Editor.held[keys.rctrl]) then
			local markers = 0
			for k, v in pairs( Editor.selection ) do
				if Editor.getObjParam( v.id, "POLYGON" ) then
					markers = markers + 1
				end
			end
			if markers > 0 then
				Editor.completePolygon( Editor.held[keys.lshift] or Editor.held[keys.rshift] )
			else
				Editor.decomposePolygon()
			end
		elseif key == keys.period then
			local markers = 0
			for k, v in pairs( Editor.selection ) do
				if Editor.getObjParam( v.id, "POLYGON" ) then
					markers = markers + 1
				end
			end
			if markers == 1 then
				Editor.nextPolygonMarker()
			else
				Editor.createPolygon()
			end
		elseif key == keys.z and not Editor.held[keys.lctrl] then
			Editor.widgets.z = {}
			Editor.tool.old_z = {}
			Editor.action = { type = "MOVEMENT", from = {}, to = {}, text = "change objects z" }
			for key, value in pairs( Editor.selection ) do
				if value.sprite then
					if not Editor.held[keys.lalt] then
						Editor.widgets.z[value.id] = CreateWidget(constants.wt_Widget, "z", nil, 0, 0, 1, 1)
						WidgetSetVisible(Editor.widgets.z[value.id], true)
					end
					Editor.tool.old_z[value.id] = value.sprite.z
					table.insert( Editor.action.from, { obj = value, pos = { x = value.aabb.p.x, y = value.aabb.p.y, z = value.sprite.z } } )
				end
			end
			table.insert( Editor.threads, { Editor.change_value_thread, param =
				{ x = Editor.mouse_x, y = Editor.mouse_y, k = CONFIG.scr_height/2, update_event =
					function( _, change )
						local col, y, h
						for key, value in pairs( Editor.selection ) do
							if value.sprite then
								local total = math.min( CONFIG.near_z, math.max(CONFIG.far_z, Editor.tool.old_z[value.id] + change ) )
								value.sprite.z = Editor.tool.old_z[value.id] + change
								if value.sprite.z > CONFIG.far_z then
									value.sprite.z = CONFIG.far_z
								elseif value.sprite.z < CONFIG.near_z then
									value.sprite.z = CONFIG.near_z
								end
								SetObjPos( value.id, value.aabb.p.x, value.aabb.p.y, value.sprite.z )
							end
							if Editor.widgets.z[value.id] then
								h = (math.abs(value.sprite.z)/CONFIG.far_z) * math.max( value.aabb.H, 20 )
								if value.sprite.z > 0 then
									col = { 0, 0, 1, 0.5 }
									y = value.aabb.p.y - h
								else
									col = { 1, 0, 0, 0.5 }
									y = value.aabb.p.y
								end
								WidgetSetColorBox( Editor.widgets.z[value.id], col )
								WidgetSetPos( Editor.widgets.z[value.id], value.aabb.p.x+value.aabb.W, y )
								WidgetSetPos( Editor.widgets.z[value.id], value.aabb.p.x+value.aabb.W, y )
								WidgetSetFixedPosition( Editor.widgets.z[value.id], false )								
								WidgetSetSize( Editor.widgets.z[value.id], 10, h )								
							end
						end
					end, event = 
					function( param )
						for key, value in pairs(Editor.selection) do
							if Editor.widgets.z[value.id] then
								DestroyWidget(Editor.widgets.z[value.id])
							end
							if value.sprite then
								table.insert( Editor.action.to, { obj = value, pos = { x = value.aabb.p.x, y = value.aabb.p.y, z = value.sprite.z } } )
							end
						end
						Editor.AddHistoryEvent( Editor.action )
					end,
				  event_param = { x = Editor.mouse_x, y = Editor.mouse_y },
				  stop_condition = "KEY_RELEASED", key = keys.z
				 }})
		end
	end

	return ret
end

----------------------------------------------------------------------------------------------------------------------------------
--							KEYBOARD & MOUSE							--
----------------------------------------------------------------------------------------------------------------------------------
function Editor.keyProcessor( key )
	if not Editor.held[key] then
		Editor.held[key] = true
		Editor.keyPress( key )
	end
	if Editor.state == Editor.STATE.RUNNING then
		Editor.tool.onKey( key )
	end
end

function Editor.keyPress( key )
	if Editor.CheckSensitiveKeys( key ) then
		return true
	end
	if key == keys.tab and Editor.state == Editor.STATE.RUNNING then
		Editor.changeState( Editor.STATE.IN_MENU )
		Editor.menu2:show()
		Editor.menu3:show(1)
	elseif key == keys["substract"] or key == keys["minus"] then
		if Editor.global_scale > 1 then 
			Editor.global_scale = Editor.global_scale - 1 
		else 
			Editor.global_scale = Editor.global_scale / 2 
		end 
		SetCamScale(Editor.global_scale, Editor.global_scale)
	elseif key == keys["add"] or key == keys["equals"] then
		if Editor.global_scale > 1 then 
			Editor.global_scale = Editor.global_scale + 1 
		else 
			Editor.global_scale = Editor.global_scale * 2 
		end 
		SetCamScale(Editor.global_scale, Editor.global_scale)
	elseif key == keys.l and (Editor.held[keys.lctrl] or Editor.held[keys.rctrl]) then
		Editor.changeState( Editor.STATE.IN_MENU )
		Editor.loadProtoMenu()
	elseif key == keys.l and #Editor.selection > 0 then
		Editor.DirectionalCloning( Editor.selection )
		return
	elseif Editor.state == Editor.STATE.RUNNING then
		Editor.tool.onKeyPress( key )
	end
end

function Editor.UpdateGroup( obj, selection )
	local function ugr( group )
		if not group then return end
		local in_selection = false
		if group then
			for k, v in pairs(group) do
				in_selection = false
				for k1, v1 in pairs(Editor.selection) do
					if v1.id == v.id then
						in_selection = true
						break
					end
				end
				if not in_selection and selection then
					Editor.selectionSet( v, true )
				elseif in_selection and not selection then
					Editor.selectionSet( v, false )
				end
			end
		end
	end
	ugr( Editor.groups[obj.id] )
	ugr( Editor.getObjParam( obj.id, "PERMAGROUP" ) )
end

function Editor.RemoveFromGroup( id )
	if not Editor.groups[ id ] then
		return
	end
	for k, v in pairs( Editor.groups[id] ) do
		if v.id == id then
			table.remove( Editor.groups[id], k )
			break
		end
	end
end

function Editor.GroupObjects()
	if #Editor.selection > 1 then
		Editor.FloatyText( "GROUP OBJECTS" )
		local grp = {}
		local action = { type = "GROUP", from = {}, to = {}, text = "group objects" }
		for k, v in pairs( Editor.selection ) do
			table.insert( action.from, { obj = v, group = Editor.groups[ v.id ] } )
			if Editor.groups[ v.id ] then
				Editor.RemoveFromGroup( v.id )
			end
			table.insert( grp, v )
			Editor.groups[ v.id ] = grp
			table.insert( action.to, { obj = v, group = Editor.groups[ v.id ] } )
		end
		Editor.AddHistoryEvent( action )
	end
end

function Editor.UngroupObjects()
	if #Editor.selection > 1 then
		Editor.FloatyText( "UNGROUP OBJECTS" )
		local action = { type = "GROUP", from = {}, to = {}, text = "ungroup objects" }
		for k, v in pairs( Editor.selection ) do
			table.insert( action.from, { obj = v, group = Editor.groups[ v.id ] } )
			if Editor.groups[ v.id ] then
				Editor.RemoveFromGroup( v.id )
			end
			Editor.groups[ v.id ] = nil
			table.insert( action.to, { obj = v, group = nil } )
		end
		Editor.AddHistoryEvent( action )
	end
end

function Editor.copy()
	Editor.clipboard = deep_copy(Editor.selection)
end

function Editor.paste()
	Editor.newSelection()
	Editor.selectionSet()
	local cx, cy = 0, 0
	local size = #Editor.clipboard
	for key, value in pairs( Editor.clipboard ) do
		cx = cx + value.aabb.p.x
		cy = cy + value.aabb.p.y
	end
	cx = cx / size
	cy = cy / size
	local obj
	local action = { type = "CREATION", obj = {}, text = "copy object" }
	local new_groups = {}
	local new_objects = {}
	local old_groups = {}
	local group_old
	for key, value in pairs( Editor.clipboard ) do
		obj = Editor.cloneObject( value, math.floor(Editor.mouse_x+value.aabb.p.x-cx+0.5), math.floor(Editor.mouse_y+value.aabb.p.y-cy+0.5) )
		if obj then
			local o = GetObject( obj )
			if Editor.groups[ value.id ] then
				group_old = false
				for k,v in pairs( old_groups ) do
					if Editor.groups[value.id] == v then
						group_old = k
						break
					end
				end
				if not group_old then
					local grp = { o }
					Editor.groups[o.id] = grp
					table.insert( new_groups, grp )
					table.insert( old_groups, Editor.groups[value.id] )
				else
					table.insert( new_groups[group_old], o )
					Editor.groups[o.id] = new_groups[group_old]
				end
			end
			Editor.selectionSet( o, true )
			table.insert( action.obj, GetObject(obj) )
		end
	end
	Editor.AddHistoryEvent( action )
end

function Editor.keyRelease( key )
	Editor.held[key] = false
	if Editor.state == Editor.STATE.RUNNING then
		Editor.tool.onKeyRelease( key )
	end
	
	if key == keys.z and Editor.held[keys.lctrl] then
		Editor.Undo()
	elseif key == keys.y and Editor.held[keys.lctrl] then
		Editor.Redo()
	end
	
	if key == keys.g and Editor.held[keys.lalt] then
		if Editor.held[keys.lctrl] then
			Editor.UngroupObjects()			
		else
			Editor.GroupObjects()
		end
		return
	end
	
	if key == keys.c and (Editor.held[keys.lctrl] or Editor.held[keys.rctrl]) then
		Editor.copy()
	elseif key == keys.v and (Editor.held[keys.lctrl] or Editor.held[keys.rctrl]) then
		Editor.paste()
	elseif key == keys.tab and Editor.state == Editor.STATE.IN_MENU then
		Editor.changeState( Editor.STATE.RUNNING )
		Editor.menu2:hide()
		Editor.menu3:hide()
	end
end

function Editor.mousePress( key )
	Editor.held["mouse"..key] = true
	if key == 0 then
		local x, y = GetMousePos()
		if Editor.CheckSensitiveSpot( x, y ) then
			return
		end
	end
	if Editor.state == Editor.STATE.RUNNING then
		Editor.tool.onMousePress( key )
	elseif Editor.state == Editor.STATE.PAINTING and key == 1 then
			table.insert( Editor.threads, { Editor.drag_camera_thread, param =
			{ x = Editor.mouse_x, y = Editor.mouse_y, stop_condition = "KEY_RELEASED", key = "mouse1",
		 }})
	end
end

function Editor.mouseRelease( key )
	Editor.held["mouse"..key] = false
	if Editor.state == Editor.STATE.RUNNING then
		Editor.tool.onMouseRelease( key )
	end
end

----------------------------------------------------------------------------------------------------------------------------------
--							WIDGET GROUPS								--
----------------------------------------------------------------------------------------------------------------------------------

function Editor.populateWidgetGroups()
	local group = {}  --main

	local widget = CreateWidget( constants.wt_Label, "position", nil, 5, CONFIG.scr_height-15, 1, 1 )
	WidgetSetCaptionFont( widget, "default" )
	WidgetSetCaptionColor( widget, {1,1,1,1}, false )
	WidgetSetCaption( widget, "(0, 0)" )
	group.position = widget
	local widget = CreateWidget( constants.wt_Label, "selection", nil, 5, CONFIG.scr_height-30, 1, 1 )
	WidgetSetCaptionFont( widget, "default" )
	WidgetSetCaptionColor( widget, {1,1,1,1}, false )
	WidgetSetCaption( widget, "NONE" )
	group.selection = widget


	Editor.addWidgetGroup( "main", group )
	Editor.showWidgetGroup( "main", true )

	group = {}	 --selection
	widget = CreateWidget( constants.wt_Widget, "selection", nil, 0, 0, 1, 1 )
	WidgetSetColorBox( widget, Editor.selection_color )
	group[1] = widget

	Editor.addWidgetGroup( "selection", group )
end

----------------------------------------------------------------------------------------------------------------------------------
--							THREADS									--
----------------------------------------------------------------------------------------------------------------------------------

--Main thread, updates interface and stuff.
function Editor._main_thread()
	while Editor.state ~= Editor.STATE.LEAVE_MAP do
		Editor.mouse_x, Editor.mouse_y = GetMousePos()
		Editor.mouse_relative_x, Editor.mouse_relative_y = GetMousePos( true )
		WidgetSetCaption( Editor.widgets.main.position, string.format("(%s, %s)", Editor.mouse_x, Editor.mouse_y) )
		for key, value in pairs( Editor.threads ) do
			value[1]( key, value.param )
			if Editor.checkStopEventCondition( Editor.threads[key] ) then
				table.remove(Editor.threads, key)
				if value.param.event then
					value.param.event( value.param.event_param or value.param or {} )
					Editor.updateMinimap("UPDATE")
				end
			end
		end
		if CONFIG.scr_width ~= Editor.scr_width or CONFIG.scr_height ~= Editor.scr_height then
			WidgetSetPos( Editor.widgets.main.selection, 5, CONFIG.scr_height-30 )
			WidgetSetPos( Editor.widgets.main.position, 5, CONFIG.scr_height-15 )
			Log("resolution change")
			Editor.menu3:move(CONFIG.scr_width, 0)
			Editor.scr_width, Editor.scr_height = CONFIG.scr_width, CONFIG.scr_height
			Editor.updateMinimap("RESIZE")
		end
		Wait(1)
	end
end

function Editor.checkStopEventCondition( event )
	if type(event.param.stop_condition) == "function" then
		return event.param.stop_condition( event ) or false
	elseif event.param.stop_condition == "KEY_RELEASED" then
		return not Editor.held[ event.param.key ]
	elseif event.param.stop_condition == "KEY_PRESSED" then
		return Editor.held[ event.param.key ]
	end
	return false
end

function Editor.resize_widget_thread( id, param )
	WidgetSetVisible( param.widget, true )
	local mx, my = Editor.snap( nil, Editor.mouse_x, Editor.mouse_y )
	WidgetSetPos( param.widget, Editor.x + ( math.min( mx, param.x ) - Editor.x ) * Editor.global_scale, Editor.y + ( math.min( my, param.y ) - Editor.y ) * Editor.global_scale )
	WidgetSetSize( param.widget, math.abs( mx - param.x ) * Editor.global_scale, math.abs( my - param.y ) * Editor.global_scale )
	WidgetSetFixedPosition( Editor.widgets.selection[1], (param.fixed or false) )
	local stop = false
end

function Editor.drag_camera_thread( id, param )
	Editor.x, Editor.y = Editor.x - (Editor.mouse_x - param.x), Editor.y - (Editor.mouse_y - param.y)
	CamMoveToPos( Editor.x, Editor.y )
	Editor.updateMinimap("CAMERA")
end

function Editor.drag_objects_thread( id, param )
	local hull = Editor.hull( Editor.selection )
	local x, y = Editor.mouse_x - param.x, Editor.mouse_y - param.y
	x, y = Editor.snap( hull, x, y )
	for key, value in pairs( Editor.selection ) do
		value.aabb.p.x, value.aabb.p.y = value.aabb.p.x + x, value.aabb.p.y + y
		SetObjPos( value.id, value.aabb.p.x, value.aabb.p.y )
	end
	Editor.threads[id].param.x, Editor.threads[id].param.y = Editor.threads[id].param.x + x, Editor.threads[id].param.y + y
end

function Editor.change_value_thread( id, param )
	local x, y = Editor.mouse_x, Editor.mouse_y
	if param.update_event then
		param.update_event( (x - param.x)/(param.x or 1), (y - param.y)/(param.k or 1) )
	end
end

function Editor.empty_thread()
end

----------------------------------------------------------------------------------------------------------------------------------
--							MINIMAP								--
----------------------------------------------------------------------------------------------------------------------------------
function Editor.updateMinimap( etype, param )
	if not Editor.minimap.show and etype ~= "HIDE" then
		return
	end
	if etype == "RESIZE" then
		Editor.minimap.size = math.min( CONFIG.scr_width/4, CONFIG.scr_height/4 )
		if not Editor.minimap.back then
			local widget = CreateWidget( constants.wt_Widget, "minimap bck", nil, 0, 0, 1, 1 )
			WidgetSetColorBox( widget, {0,0,0,1} )
			Editor.minimap.back = widget
		end
		if Editor.minimap.widgets then
			for key, value in pairs(Editor.minimap.widgets) do
				DestroyWidget( value )
			end
		end
		Editor.minimap.widgets = {}
		WidgetSetPos( Editor.minimap.back, CONFIG.scr_width-Editor.minimap.size, CONFIG.scr_height-Editor.minimap.size )
		WidgetSetSize( Editor.minimap.back, Editor.minimap.size, Editor.minimap.size )
		Editor.map = EditorDumpMap()
		Editor.minimap.mx, Editor.minimap.my, Editor.minimap.Mx, Editor.minimap.My = nil, nil, nil, nil
		for key, value in pairs( Editor.map ) do
			if value.phFlags and value.phFlags.solid then
				if not Editor.minimap.mx or value.aabb.p.x - value.aabb.W < Editor.minimap.mx then
					Editor.minimap.mx = value.aabb.p.x - value.aabb.W
				end
				if not Editor.minimap.my or value.aabb.p.y - value.aabb.H < Editor.minimap.my then
					Editor.minimap.my = value.aabb.p.y - value.aabb.H
				end
				if not Editor.minimap.Mx or value.aabb.p.x + value.aabb.W > Editor.minimap.Mx then
					Editor.minimap.Mx = value.aabb.p.x + value.aabb.W
				end
				if not Editor.minimap.My or value.aabb.p.y + value.aabb.H > Editor.minimap.My then
					Editor.minimap.My = value.aabb.p.y + value.aabb.H
				end
			end
		end
		local mapx, mapy = CONFIG.scr_width-Editor.minimap.size, CONFIG.scr_height-Editor.minimap.size
		local minmapsize = Editor.minimap.size
		local k = math.min( minmapsize/(Editor.minimap.Mx-Editor.minimap.mx), minmapsize/(Editor.minimap.My-Editor.minimap.my) )
		for key, value in pairs( Editor.map ) do
			if value.phFlags and value.phFlags.solid then
				local widget = CreateWidget( constants.wt_Widget, "-_-", nil, mapx+(value.aabb.p.x-value.aabb.W-Editor.minimap.mx)*k,
													  mapy+(value.aabb.p.y-value.aabb.H-Editor.minimap.my)*k, 
													  math.max(2*value.aabb.W*k, 1), 
													  math.max(2*value.aabb.H*k, 1) )
				WidgetSetColorBox( widget, {1,1,1,1} )
				Editor.minimap.widgets[value.id] = widget
			end
		end
		if Editor.minimap.camera then
			DestroyWidget( Editor.minimap.camera )	
		end
		Editor.minimap.camera = CreateWidget( constants.wt_Widget, "T_T", nil, 0, 0, CONFIG.scr_width*k, CONFIG.scr_height*k)
		WidgetSetColorBox( Editor.minimap.camera, {0,0,1,0.5} )
		WidgetSetPos( Editor.minimap.camera, mapx + k*(Editor.x-CONFIG.scr_width/2-Editor.minimap.mx), mapy + k*(Editor.y-CONFIG.scr_height/2-Editor.minimap.my) )
	elseif etype == "HIDE" then
		DestroyWidget( Editor.minimap.back )
		Editor.minimap.back = nil
		DestroyWidget( Editor.minimap.camera )
		Editor.minimap.camera = nil
		if Editor.minimap.widgets then
			for key, value in pairs(Editor.minimap.widgets) do
				DestroyWidget( value )
			end
		end
	elseif etype == "UPDATE" then
		if param and not param.physFlags then
			return
		end
		Editor.updateMinimap("RESIZE")	--���� ���.
	elseif etype == "CAMERA" then
		local mapx, mapy = CONFIG.scr_width-Editor.minimap.size, CONFIG.scr_height-Editor.minimap.size
		local minmapsize = Editor.minimap.size
		local k = math.min( minmapsize/(Editor.minimap.Mx-Editor.minimap.mx), minmapsize/(Editor.minimap.My-Editor.minimap.my) )

		if not Editor.minimap.camera then
			Editor.minimap.camera = CreateWidget( constants.wt_Widget, "T_T", nil, 0, 0, CONFIG.scr_width*k, CONFIG.scr_height*k)
			WidgetSetColorBox( Editor.minimap.camera, {0,0,1,0.5} )
		end
		WidgetSetPos( Editor.minimap.camera, mapx + k*(Editor.x-CONFIG.scr_width/2-Editor.minimap.mx), mapy + k*(Editor.y-CONFIG.scr_height/2-Editor.minimap.my) )
	end
end

----------------------------------------------------------------------------------------------------------------------------------
--							GENERAL FUNCTIONS							--
----------------------------------------------------------------------------------------------------------------------------------

function Editor.selectedObjectsProc(o)
	if Editor.state == Editor.STATE.RUNNING then
		Editor.tool.selectionEvent(o)
	end
end

function Editor.inSelection( o )
	for k, v in pairs( Editor.selection ) do
		if v.id == o.id then
			return true
		end
	end
	return false
end

function Editor.cloneObject( o, x, y )
	if Editor.getObjParam( o.id, "DONT_SAVE") then
		return
	end
	if o.type == Editor.TYPES.OBJ_PLAYER then
		local obj = CreatePlayer( o.proto, x-o.creation_shift.x, y-o.creation_shift.y )
		Editor.setObjParam( obj, "SPECIAL_CREATION", true )
		return obj
	elseif o.type == Editor.TYPES.OBJ_SPRITE then
		if o.group then
			local grp = CreateSprite( o.proto, x-o.aabb.W-o.creation_shift.x, y-o.aabb.H-o.creation_shift.y )
			GroupObjects( grp, CreateSprite("phys-empty", x+o.aabb.W-o.creation_shift.x-1, y+o.aabb.H-o.creation_shift.y-1) )
			return grp
		elseif not o.proto then
			return CreateColorBox( x-o.aabb.W, y-o.aabb.H, o.aabb.p.x+o.aabb.W, o.aabb.p.y+o.aabb.H, o.sprite.color)
		else
			local obj = CreateSprite( o.proto, x-o.creation_shift.x, y-o.creation_shift.y, o.sprite.z)
			if ( o.sprite ) then
				SetObjAnim( obj, o.sprite.cur_anim, false )
			end
			local z = nil
			if o.sprite then z = o.sprite.z end
			SetObjPos( obj, x, y, z )
			return obj
		end
	elseif o.type == Editor.TYPES.OBJ_ENEMY then
		local obj = CreateEnemy( o.proto, x-o.creation_shift.x, y-o.creation_shift.y)
--		SetObjPos( obj, x, y )
		return obj
	elseif o.type == Editor.TYPES.OBJ_ITEM then
		return CreateItem( o.proto, x-o.creation_shift.x, y-o.creation_shift.y)
	elseif o.type == Editor.TYPES.OBJ_SPAWNER then
		local spawn = CreateSpawner( Editor.getObjParam( o.id, "PROP_OBJ"), x, y, Editor.getObjParam( o.id, "PROP_COUNT"), Editor.getObjParam( o.id, "PROP_DIR" ),
					Editor.getObjParam( o.id, "PROP_DELAY"), Editor.getObjParam( o.id, "PROP_SIZE"), 
					Editor.getObjParam( o.id, "PROP_RESET_DIST"), Editor.getObjParam( o.id, "RESPAWN_DELAY"))
		SetObjPos( spawn, x, y )
		Editor.prepareObject( GetObject( spawn ) )
		return spawn
	elseif o.type == Editor.TYPES.OBJ_TILE then
		local obj = CreateSprite( o.proto, x-o.creation_shift.x, y-o.creation_shift.y, o.sprite.z )
		SetObjAnim( obj, o.sprite.frame, false )
		SetObjPos( obj, x, y, o.sprite.z )
		return obj
	elseif o.type == Editor.TYPES.OBJ_RIBBON then
		local rib = CreateRibbonObj( o.proto, o.k.x, o.k.y, x-o.creation_shift.x, y-o.creation_shift.y, o.sprite.z )
		SetRibbonObjBounds( o.ubl, o.ubt, o.ubr, o.ubb )
		SetRibbonObjRepitition( o.repeat_x, o.repeat_y )
		return rib
	end
end

Editor.creation_function = 
{
	["SPRITE"] = function( widget, param )
		Editor.changeState( Editor.STATE.PAINTING )
		Editor.menu2:hide()
		Editor.menu3:hide()
		Editor.newSelection()
		local x,y = Editor.mouse_x, Editor.mouse_y
		Editor.selectionSet( nil )
		local obj = CreateSprite( param.proto, x, y )
		SetObjPos( obj, x, y )
		Editor.selectionSet( GetObject(obj), true )
		table.insert( Editor.threads, { Editor.drag_objects_thread, param = { x = x, y = y, 
        		stop_condition = "KEY_PRESSED", key = "mouse0", event = function() 
				Editor.AddHistoryEvent( {type = "CREATION", obj = { Editor.selection[1] }, text = "create sprite"} ) 
				Editor.changeState( Editor.STATE.RUNNING )
			end
		 }})
	end,
	["ENEMY"] = function( widget, param )
		Editor.changeState( Editor.STATE.PAINTING )
		Editor.menu2:hide()
		Editor.menu3:hide()
		Editor.newSelection()
		local x,y = Editor.mouse_x, Editor.mouse_y
		Editor.selectionSet( nil )
		local obj = CreateEnemy( param.proto, x, y )
		SetObjPos( obj, x, y )
		Editor.selectionSet( GetObject(obj), true )
		table.insert( Editor.threads, { Editor.drag_objects_thread, param = { x = x, y = y, 
        		stop_condition = "KEY_PRESSED", key = "mouse0", event = function() 
					Editor.AddHistoryEvent( {type = "CREATION", obj = { Editor.selection[1] }, text = "create enemy" } )
					Editor.changeState( Editor.STATE.RUNNING )
			end
		 }})
	end,
	["ITEM"] = function( widget, param )
		Editor.changeState( Editor.STATE.PAINTING )
		Editor.menu2:hide()
		Editor.menu3:hide()
		Editor.newSelection()
		local x,y = Editor.mouse_x, Editor.mouse_y
		Editor.selectionSet( nil )
		local obj = CreateItem( param.proto, x, y )
		SetObjPos( obj, x, y )
		Editor.selectionSet( GetObject(obj), true )
		table.insert( Editor.threads, { Editor.drag_objects_thread, param = { x = x, y = y, 
        		stop_condition = "KEY_PRESSED", key = "mouse0", event = function() 
				Editor.AddHistoryEvent( {type = "CREATION", obj = { Editor.selection[1] }, text = "create item"} ) 
				Editor.changeState( Editor.STATE.RUNNING )
			end
		 }})
	end,
	["PLAYER"] = function( widget, param )
		Editor.changeState( Editor.STATE.PAINTING )
		Editor.menu2:hide()
		Editor.menu3:hide()
		Editor.newSelection()
		local x,y = Editor.mouse_x, Editor.mouse_y
		Editor.selectionSet( nil )
		local obj = CreatePlayer( param.proto, x, y )
		Editor.setObjParam( obj, "SPECIAL_CREATION", true )
		SetObjPos( obj, x, y )
		Editor.selectionSet( GetObject(obj), true )
		table.insert( Editor.threads, { Editor.drag_objects_thread, param = { x = x, y = y, 
        		stop_condition = "KEY_PRESSED", key = "mouse0", event = function() 
				Editor.AddHistoryEvent( {type = "CREATION", obj = { Editor.selection[1] }, text = "create character"} ) 
				Editor.changeState( Editor.STATE.RUNNING )
			end
		 }})
	end,
	["TILE"] = function( widget, param )
		Editor.changeState( Editor.STATE.PAINTING )
		Editor.menu2:hide()
		Editor.menu3:hide()
		Editor.newSelection()
		local x,y = Editor.mouse_x or 0, Editor.mouse_y or 0
		Editor.selectionSet( nil )
		local tile = CreateSprite( param.proto, x, y )
		SetObjPos( tile, x, y )
		SetObjAnim( tile, (param.extra or 0), false )
		Editor.selectionSet( GetObject( tile ), true )
		table.insert( Editor.threads, { Editor.drag_objects_thread, param = { x = x, y = y, 
        		stop_condition = "KEY_PRESSED", key = "mouse0", event = function() 
				Editor.AddHistoryEvent( {type = "CREATION", obj = { Editor.selection[1] }, text = "create tile"} ) 
				Editor.changeState( Editor.STATE.RUNNING )
			end
		 }})
	end,
	["RIBBON"] = function( widget, param )
		Editor.changeState( Editor.STATE.PAINTING )
		Editor.menu2:hide()
		Editor.menu3:hide()
		Editor.newSelection()
		local x,y = Editor.mouse_x, Editor.mouse_y
		Editor.selectionSet( nil )
		local rib = CreateRibbonObj( param.proto, 0, 0, x, y, 0, true )
		SetRibbonObjRepitition( rib, true, false )
		SetObjPos( rib, x, y )
		Editor.selectionSet( GetObject( rib ), true )
		table.insert( Editor.threads, { Editor.drag_objects_thread, param = { x = x, y = y, 
        		stop_condition = "KEY_PRESSED", key = "mouse0", event = function() 
				Editor.AddHistoryEvent( {type = "CREATION", obj = { Editor.selection[1] }, text = "create ribbon"} ) 
				Editor.changeState( Editor.STATE.RUNNING )
			end
		 }})
	end,
	["_PHYSIC GROUP"] = function( param )
		Editor.newSelection()
		local x,y = Editor.mouse_x, Editor.mouse_y
		Editor.selectionSet( nil )
		editor_color = nil
		dofile( param.path )
		WidgetSetColorBox( Editor.widgets.selection[1], editor_color or Editor.selection_color )
		WidgetSetVisible( Editor.widgets.selection[1], true )
		x, y = Editor.snap( nil, x, y )
		table.insert( Editor.threads, { Editor.resize_widget_thread, param = { x = x, y = y, widget = Editor.widgets.selection[1],
        		stop_condition = "KEY_RELEASED", key = "mouse0", event_param = { x = x, y = y, proto = param.proto }, event = function( param )
				Editor.changeState( Editor.STATE.RUNNING )
				local mx, my = Editor.snap( nil, GetMousePos() )
				local grp = CreateSprite( param.proto, param.x, param.y )
				GroupObjects( grp, CreateSprite( param.proto, mx, my ) )
				Editor.selectionSet( GetObject(grp), true )
				WidgetSetVisible( Editor.widgets.selection[1], false )
				Editor.AddHistoryEvent( {type = "CREATION", obj = { Editor.selection[1] }, text = "create physic group"} )
			end
		 }})
	end,
	["PHYSIC GROUP"] = function( widget, param )
		Editor.changeState( Editor.STATE.PAINTING )
		Editor.menu2:hide()
		Editor.menu3:hide()
		table.insert( Editor.threads, { Editor.empty_thread, param = { 	stop_condition = "KEY_PRESSED", key = "mouse0",
						event = Editor.creation_function["_PHYSIC GROUP"], event_param = { proto = param.proto, path = param.path }
		 }})
	end,
	["_ACTIVE GROUP"] = function( param )
		Editor.newSelection()
		local x,y = Editor.mouse_x, Editor.mouse_y
		Editor.selectionSet( nil )
		editor_color = nil
		dofile( param.path )
		x, y = Editor.snap( nil, x, y )
		WidgetSetColorBox( Editor.widgets.selection[1], editor_color or Editor.selection_color )
		WidgetSetVisible( Editor.widgets.selection[1], true )
		table.insert( Editor.threads, { Editor.resize_widget_thread, param = { x = x, y = y, widget = Editor.widgets.selection[1],
        		stop_condition = "KEY_RELEASED", key = "mouse0", event_param = { x = x, y = y, proto = param.proto }, event = function( param )
				Editor.changeState( Editor.STATE.RUNNING )
				local mx, my = Editor.snap( nil, GetMousePos() )
				local grp = CreateItem( param.proto, param.x, param.y )
				GroupObjects( grp, CreateItem( param.proto, mx, my ) )
				Editor.selectionSet( GetObject(grp), true )
				WidgetSetVisible( Editor.widgets.selection[1], false )
				Editor.setObjParam( grp, "SPECIAL_CREATION", true )
				Editor.AddHistoryEvent( {type = "CREATION", obj = { Editor.selection[1] }, text = "create active group"} )
			end
		 }})
	end,
	["ACTIVE GROUP"] = function( widget, param )
		Editor.changeState( Editor.STATE.PAINTING )
		Editor.menu2:hide()
		Editor.menu3:hide()
		table.insert( Editor.threads, { Editor.empty_thread, param = { 	stop_condition = "KEY_PRESSED", key = "mouse0",
						event = Editor.creation_function["_ACTIVE GROUP"], event_param = { proto = param.proto, path = param.path }
		 }})
	end,
	["_ENVIRONMENT"] = function( param )
		Editor.newSelection()
		local x,y = Editor.mouse_x, Editor.mouse_y
		Editor.selectionSet( nil )
		editor_color = nil
		dofile( param.path )
		x, y = Editor.snap( nil, x, y )
		WidgetSetColorBox( Editor.widgets.selection[1], editor_color or Editor.selection_color )
		WidgetSetVisible( Editor.widgets.selection[1], true )
		table.insert( Editor.threads, { Editor.resize_widget_thread, param = { x = x, y = y, widget = Editor.widgets.selection[1],
        		stop_condition = "KEY_RELEASED", key = "mouse0", event_param = { x = x, y = y, proto = param.proto }, event = function( param )
				Editor.changeState( Editor.STATE.RUNNING )
				local mx, my = Editor.snap( nil, GetMousePos() )
				local grp = CreateEnvironment( param.proto, param.x, param.y )
				GroupObjects( grp, CreateEnvironment( param.proto, mx, my ) )
				Editor.selectionSet( GetObject(grp), true )
				WidgetSetVisible( Editor.widgets.selection[1], false )
				Editor.AddHistoryEvent( {type = "CREATION", obj = { Editor.selection[1] }, text = "create environment"} )
			end
		 }})
	end,
	["ENVIRONMENT"] = function( widget, param )
		Editor.changeState( Editor.STATE.PAINTING )
		Editor.menu2:hide()
		Editor.menu3:hide()
		table.insert( Editor.threads, { Editor.empty_thread, param = { 	stop_condition = "KEY_PRESSED", key = "mouse0",
						event = Editor.creation_function["_ENVIRONMENT"], event_param = { proto = param.proto, path = param.path }
		 }})
	end,
	["_BOX"] = function( param )
		Editor.newSelection()
		local x,y = Editor.mouse_x, Editor.mouse_y
		Editor.selectionSet( nil )
		editor_color = nil
		x, y = Editor.snap( nil, x, y )
		WidgetSetColorBox( Editor.widgets.selection[1], Editor.tool.box_color )
		WidgetSetVisible( Editor.widgets.selection[1], true )
		table.insert( Editor.threads, { Editor.resize_widget_thread, param = { x = x, y = y, widget = Editor.widgets.selection[1],
        		stop_condition = "KEY_RELEASED", key = "mouse0", event_param = { x = x, y = y, proto = param.proto }, event = function( param )
				Editor.changeState( Editor.STATE.RUNNING )
				local mx, my = Editor.snap( nil, GetMousePos() )
				local grp = CreateColorBox( param.x, param.y, mx, my, -1, { vars.red, vars.green, vars.blue, vars.alpha } )
				Editor.selectionSet( GetObject(grp), true )
				Editor.AddHistoryEvent( {type = "CREATION", obj = { Editor.selection[1] }, text = "create color box"} )
				WidgetSetVisible( Editor.widgets.selection[1], false )
			end
		 }})
	end,
	["BOX"] = function( widget, param )
		Editor.colorSelected = function()
			Editor.changeState( Editor.STATE.PAINTING )
			Editor.menu:hide(3)
			Editor.tool.box_color = { vars.red or 0, vars.green or 0, vars.blue or 0, vars.alpha or 1 }
			table.insert( Editor.threads, { Editor.empty_thread, param = { 	stop_condition = "KEY_PRESSED", key = "mouse0",
						event = Editor.creation_function["_BOX"], event_param = { proto = param.proto, path = param.path }
			 }})
		end
		Editor.changeState( Editor.STATE.PAINTING )
		Editor.menu2:hide()
		Editor.menu3:hide()
		ColorPicker( Editor.tool.box_color or {0.5, 0.5, 0.5, 1} )
	end
}

function Editor.GuessOSType()
	Editor.os = os.getenv("OS")
	if Editor.os then
		Editor.sh_prefix = ""
		Editor.sh_type = "windows"
		return
	end
	local f, err = io.popen( "uname -o" )
	if f then
		Editor.os = string.gsub( f:read("*a"), "\n", "" )
		Editor.sh_prefix = "LD_LIBRARY_PATH=.:LD_LIBRARY_PATH ./"
		Editor.sh_type = "linux"
		return
	end
	Editor.os = "wtf"
	Editor.sh_type = "wtf"
end

function Editor.GuessExecFile()
	local ftester, err
	local names
	if Editor.sh_type == "linux" then
		names = { "iichantra.release", "iiChantra.Release", "iichantra.debug", "iiChantra.Debug" }
	elseif Editor.sh_type == "windows" then
		names = { "iiChantra.Release.exe", "iiChantra.Debug.exe" }
	end
	for k, v in pairs( names ) do
		Editor.exec_file = v
		ftester, err = io.open( Editor.exec_file, "r" )
		if ftester then
			ftester:close()
			return
		end
	end
	Editor.exec_file = nil
end

function Editor.TestRun()
	if not Editor.sh_type then Editor.GuessOSType()	end
	if not Editor.exec_file then Editor.GuessExecFile() end
	if Editor.sh_type == "wtf" then
		Editor.changeState( Editor.STATE.ADDITIONAL_MENU )
		Editor.popup("Test running is not supported on your system, sorry.", function() Editor.changeState( Editor.STATE.RUNNING ) end)
		return
	end
	if not Editor.current_map then
		Editor.changeState( Editor.STATE.ADDITIONAL_MENU )
		Editor.popup("You need to save map at least once first.", function() Editor.changeState( Editor.STATE.RUNNING ) end)
		return
	end
	Editor.save(Editor.current_map)
	if Editor.exec_file then
		os.execute( string.format("%s%s %s", Editor.sh_prefix, Editor.exec_file, Editor.current_map) )
	end
end
