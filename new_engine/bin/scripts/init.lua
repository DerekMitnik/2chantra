file = io.open( constants.path_data .. "versions" )

local EXPECTED_VERSION = "1.0.0"
local GAME_VERSION = "1.0"

if file then
	EXPECTED_VERSION = file:read()
	GAME_VERSION = file:read()
	file:close()
end

LoadDefaultConfig() 	   --game config init

custom_maps = {}

require = function( what )
	return need( what .. ".lua" )
end

SwitchLighting = function() end


RegisterArchive( "data/pear15.zip" )
RegisterArchive( "data/json.zip" )

local patches = string.match( GAME_VERSION, "%d+%.(%d+)" )
if patches then
	patches = tonumber( patches )
	if patches then
		for i=1,patches do
			RegisterArchive( string.format("data/pear15_patch%i.zip", i) )
		end
	end
end

need("commandline.lua")
need("routines.lua")
need("lang_changer.lua")
LangChanger.LoadLanguage()

name_of_script = 'init'
Loader = need('loader.lua')
Loader.start()

parse_commandline( _ARGC, _ARGV )

if constants.__VERSION ~= EXPECTED_VERSION then
	local a, b, c = string.match( EXPECTED_VERSION, "(%d+)%.(%d+)%.(%d+)" )
	local version1 = tonumber(a) * 10000 + tonumber(b) * 100 + tonumber(c)
	a, b, c = string.match( constants.__VERSION, "(%d+)%.(%d+)%.(%d+)" )
	local version2 = tonumber(a) * 10000 + tonumber(b) * 100 + tonumber(c)
	if version2 < version1 then
		local sx, sy = GetCaptionSize( "default", "YOUR BINARY IS OUT OF DATE" )
		local w = CreateWidget( constants.wt_Label, "", nil, (CONFIG.scr_width - sx)/2, CONFIG.scr_height - (sy/2) - 20, 1, 1 )
		WidgetSetCaption( w, "YOUR BINARY IS OUT OF DATE" )
		WidgetSetCaptionColor( w, { 1, .3, .3, 1 }, false )
	end
end

