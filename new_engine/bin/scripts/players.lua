Players = {}

function Players.CreateNewCharacter(num, character, spawnpoint, table)
	SetDefaultActor( num )
	local id = CreatePlayer( character, spawnpoint[1], spawnpoint[2] )
	Game.actors[num] = {}
	Game.actors[num].char = GetPlayer() --Информация о текущем персонаже
	Game.actors[num].info = (Game.CreatePlayerTable and Game.CreatePlayerTable()) or NewPlayerTable( id, table ) --Отдельная функция, потому что может разниться от игры к игре
	if Game.AddPlayerVars then Game.AddPlayerVars( num ) end
end

function NewPlayerTable( player_object, table )
	return { score = 0, lives = 3, current_object = player_object } --Тут мы, скажем, что-то запоминаем, назначаем метаметоды и т.д.
end

function Players.RevivePlayer( num, params )
	SetDefaultActor( num )
	RevivePlayer( params )
	Game.actors[num].char = GetPlayer() --Информация о текущем персонаже
end

function Players.SetPlayerStats( num, table )
	SetPlayerStats( Game.actors[num].char.id, table.char )
	Game.actors[num].info = table.info
end

function Players.SwitchPlayer( num, params )
	SetDefaultActor( num )
	SwitchPlayer( params )
	Game.actors[num].char = GetPlayer() --Информация о текущем персонаже
end

function Players.NewActor()
	if not Loader.level then
		return nil
	end
	local char = (Loader.level and Loader.level.GetNextCharacter and Loader.level.GetNextCharacter()) or (Game.GetNextCharacter and Game.GetNextCharacter())
	if char and type(char) == 'table' and char[1] and char[2] then
		CreateActor()
		local id = CreatePlayer(char[1], char[2][1], char[2][2])
		local num = GetDefaultActor()
		local table = (Game.actors[num] and Game.actors[num].info) or (Game.CreatePlayerTable and Game.CreatePlayerTable()) or {}
		Game.actors[num] = {}
		Game.actors[num].char = GetPlayer();
		Game.actors[num].info = table;
		if Game.AddPlayerVars then Game.AddPlayerVars( num ) end
		return Game.actors[num]
	end
end

function Players.UpdatePlayers(table)
	--Log("Players.UpdatePlayers " .. serialize("Game.actors", Game.actors))
	for num, value in pairs(Game.actors) do
		if num <= GetActorCount() then
			Log("Не рекомендуется использовать ObjPlayer на карте")
		else
			if not Game.actors[num].info then
				local n = CreateActor()
				if n ~= num then
					Log("Где-то напортачено с вызовами CreateActor()")
				end
			end
			if not Game.actors[num].char then
				--Log("CreateNewPlayer num=", num, " charname=", Loader.level.characters[num])
				--Players.CreateNewCharacter(num, Loader.level.characters[num], Loader.level.spawnpoints[num], table) --Переделать
				local char = (Loader.level and Loader.level.GetNextCharacter and Loader.level.GetNextCharacter()) or (Game.GetNextCharacter and Game.GetNextCharacter())
				local id = CreatePlayer(char[1], char[2][1], char[2][2])
				local table = (Game.CreatePlayerTable and Game.CreatePlayerTable()) or {}
				Game.actors[num] = Game.actors[num] or {}
				Game.actors[num].char = GetPlayer();
				Game.actors[num].info = Game.actors[num].info or table;
				if Game.AddPlayerVars then Game.AddPlayerVars( num ) end
			end
		end
	end
end

--[[
function Game.GetNextCharacter()
	local spawn = { 0, 0 }
	if Loader.level and Loader.level.spawnpoints then
		spawn = random( Loader.level.spawnpoints )
	end
	return { "super-sohchan", spawn }
end

function Game.CreatePlayerTable()
	return {
		score = 0,
		lives = 0
	}
end
--]]
