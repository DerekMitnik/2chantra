need("serialize.lua")
need("routines.lua")
need("menus.lua")
need("")

-----------------------------------
-- utils

function LogT(t)
	Log(serialize("t", t))
end

function clamp(v, l, h) if v < l then return l; elseif v > h then return h; else return v; end end

function point_in_box(x, y, w, h, px, py)
	return not( px < x or px > x + w or py < y or py > y + h )
end

-------------------------

TexEd = {
	held = {},
	mouse_held = {},
	--selection = {},
	--full_tex = { move = {} },
	full_tex_size = {},
	tex = {},
	ctrls = {},
	tex_boxes = {}
}

local image_file = 'btard'
-- local image_file = 'ammo'
local texture_obj = nil
local example = {}
local main_tex = {}


function TexEd.init()
	InitNewGame()
	
	TexEd.global_scale = 1
	
	TexEd.tex_boxes_left = 120
	
	GlobalSetKeyReleaseProc(TexEd.keyRelease)
	GlobalSetKeyDownProc(TexEd.keyPress)
	GlobalSetMouseKeyReleaseProc(TexEd.mouseRelease);
	GlobalSetMouseKeyDownProc(TexEd.mousePress);
	
	TexEd.initTexBoxMain()
	TexEd.initTexBoxOverlay()
	TexEd.initTexture()
	TexEd.initTexBoxExample()
	
	
	TexEd.selectFrame(1)
	
	Resume(NewThread(TexEd.mainThread))
	-- open_texture()
end

-----------------------------------------------

function TexEd.keyPress(key)
	-- Log("TexEd.keyPress ", key)
	local first_press = not TexEd.held[key]
	TexEd.held[key] = true
	if key == keys.q then
		TexEd.selectNextFrame()
	elseif key == keys.w then
		TexEd.selectPrevFrame()
	elseif key == keys.left then
		if TexEd.held[keys.lshift] then
			TexEd.relResizeCurFrameMain(-1 * (TexEd.held[keys.lctrl] and 10 or 1), 0)
		else
			TexEd.relMoveCurFrameMain(-1 * (TexEd.held[keys.lctrl] and 10 or 1), 0)
		end
	elseif key == keys.right then
		if TexEd.held[keys.lshift] then
			TexEd.relResizeCurFrameMain(1 * (TexEd.held[keys.lctrl] and 10 or 1), 0)
		else
			TexEd.relMoveCurFrameMain(1 * (TexEd.held[keys.lctrl] and 10 or 1), 0)
		end
	elseif key == keys.up then
		if TexEd.held[keys.lshift] then
			TexEd.relResizeCurFrameMain(0, -1 * (TexEd.held[keys.lctrl] and 10 or 1))
		else
			TexEd.relMoveCurFrameMain(0, -1 * (TexEd.held[keys.lctrl] and 10 or 1))
		end
	elseif key == keys.down then
		if TexEd.held[keys.lshift] then
			TexEd.relResizeCurFrameMain(0, 1 * (TexEd.held[keys.lctrl] and 10 or 1))
		else
			TexEd.relMoveCurFrameMain(0, 1 * (TexEd.held[keys.lctrl] and 10 or 1))
		end
	elseif key == keys.tab and first_press then
		local x, y = GetMousePos(true)
		local w1 = CreateWidget( constants.wt_Widget, "vert", nil, 0, 0, x, CONFIG.scr_height );
		WidgetSetFocusable( w1, false )
		WidgetSetBorderColor( w1, {.5, .5, .5, 1}, false )
		WidgetSetBorder( w1, true )
		local w2 = CreateWidget( constants.wt_Widget, "hor", nil, 0, 0, CONFIG.scr_width, y );
		WidgetSetFocusable( w2, false )
		WidgetSetBorderColor( w2, {.5, .5, .5, 1}, false )
		WidgetSetBorder( w2, true )
		Resume( NewThread( function()
			local x, y
			while TexEd.held[keys.tab] do
				x,y = GetMousePos(true)
				WidgetSetSize( w1, x, CONFIG.scr_height )
				WidgetSetSize( w2, CONFIG.scr_width, y )
				Wait( 1 ) 
			end
			DestroyWidget( w1 )
			DestroyWidget( w2 )
		end ))
	elseif key == keys["substract"] or key == keys["minus"] then
		TexEd.global_scale = math.max(TexEd.global_scale - 1, 1) 
		TexEd.scaleChanged()
	elseif key == keys["add"] or key == keys["equals"] then
		TexEd.global_scale = TexEd.global_scale + 1 
		TexEd.scaleChanged()
	end
end

function TexEd.keyRelease(key)
	-- Log("TexEd.keyRelease ", key)
	TexEd.held[key] = false
	
	if key == keys.f2 then
		TexEd.saveTextureFrames()
	end
end

function TexEd.mousePress(key)
	-- Log("TexEd.mousePress ", key)
	TexEd.mouse_held[key] = true
	for k,v in pairs(TexEd.tex_boxes) do
		v:mousePress(key)
		if ( v.mwidget ) then
			WidgetSetVisible( v.mwidget, false )
		end
	end
end

function TexEd.mouseRelease(key)
	-- Log("TexEd.mouseRelease ", key)
	TexEd.mouse_held[key] = false
	for k,v in pairs(TexEd.tex_boxes) do
		v.mouse = -1
	end
end


-----------------------------------------------


function TexEd.mainThread()
	while not TexEd.exiting do
		TexEd.mouse_x, TexEd.mouse_y = GetMousePos()
		TexEd.mouse_x_rel, TexEd.mouse_y_rel = GetMousePos(true)

		TexEd.updateMousePosWidget()
		for k,v in pairs(TexEd.tex_boxes) do
			v:mouseHover(TexEd.mouse_x_rel, TexEd.mouse_y_rel)
		end
		
		Wait(1)
	end
end

----------------------------------------------

function mouseTracker(pressed_key, func, pre)
	local bx = TexEd.mouse_x_rel
	local by = TexEd.mouse_y_rel
	while TexEd.mouse_held[pressed_key] do
		local dx = TexEd.mouse_x_rel - bx
		local dy = TexEd.mouse_y_rel - by
		
		if pre then
			func(unpack(pre), dx, dy)
		else
			func(dx, dy)
		end
		
		bx = TexEd.mouse_x_rel
		by = TexEd.mouse_y_rel
		Wait(1)
	end
end


----------------------------------------------

local TextureBox = {}
local TextureBox_mt = { __index = TextureBox }
local ExampleTextureBox = child(TextureBox)

function TextureBox.create(...)
	local a = {}
	setmetatable(a, TextureBox_mt)
	
	a:init(...)

	return a
end

function ExampleTextureBox.create(...)
	--LogT(ExampleTextureBox)
	local a = ExampleTextureBox.bareCreate()
	a:init(...)
	return a
end

function TextureBox:init(name, image, x, y, w, h)
	--LogT{name, image, x, y, w, h}

	self.name = LoadTexture(image, {}, name..image)
	local tf = DumpTexFrames(self.name)
	TexEd.full_tex_size = { w = tf.main[1].w, h = tf.main[1].h }
	self.size = TexEd.full_tex_size
	self.pos = { x = 0, y = 0 }
	self.mouse = -1
	
	self:initProto()
	self:initWidget(x,y,w,h)
end

function TextureBox:initProto()
	local tprt = { texture = self.name }
	self.prt = LoadPrototypeTable(tprt)
end

function TextureBox:initWidget(x,y,w,h)
	self.wi = {}
	self.wi.wi = CreateWidget(constants.wt_Picture, "", nil, x, y, w, h)
	WidgetSetVisible(self.wi.wi, true)
	WidgetSetBorder(self.wi.wi, true)
	WidgetSetSprite(self.wi.wi, self.prt)
	WidgetSetSpriteRenderMethod(self.wi.wi, constants.rsmStretch)
	self.mwidget = CreateWidget( constants.wt_Widget, "mouse widget", self.wi.wi, 0, 0, 1, 1 )
	WidgetSetVisible( self.mwidget, false )
	WidgetSetFocusable( self.mwidget, false )
	WidgetSetBorderColor( self.mwidget, {1, 1, 1, 1}, false )
	WidgetSetBorder( self.mwidget, true )
	self:resizeFullTextureObj(x, y, w, h)
	self:relShiftFullTexture(0,0)
end

function ExampleTextureBox:init(image, x, y, w, h)
	--LogT{image, x, y, w, h}

	self.name = LoadTexture(image, {}, "__example_"..image)
	ChangeTexFrames(self.name, {count = 1; main ={ }; overlay = { {} } })

	self.size = TexEd.full_tex_size
	self.pos = { x = 0, y = 0 }
	
	self:initProto()
	self:initWidget(x,y,w,h)
	-- WidgetSetSpriteRenderMethod(self.wi.wi, constants.rsmStandart)
	-- WidgetSetSpriteRenderMethod(self.wi.wi, constants.rsmCrop)
end

function ExampleTextureBox:initProto()
	local tprt = { 
		texture = self.name; 
		overlay = {0};
		ocolor = {{1, 1, 1, 1}};
		animations = { { name = "idle"; frames = { {num = 0} }} }
	}
	self.prt = LoadPrototypeTable(tprt)
end


function TextureBox:curFrameUpdated()
	
end

function ExampleTextureBox:curFrameUpdated()
	self:relShiftFullTexture(0, 0)
end

function TextureBox:mousePress(key)
	if not self.wi then return end
	if not point_in_box(self.wi.x, self.wi.y, self.wi.w, self.wi.h, TexEd.mouse_x_rel, TexEd.mouse_y_rel) then return end
	
	if key == 0 then
		if TexEd.held[keys.lctrl] then
			self:beginMoveSelection()
		else
			local x, y = self.wi.x - self.pos.x + self.selection.x, self.wi.y - self.pos.y + self.selection.y
			local w, h = self.selection.w * TexEd.global_scale, self.selection.h * TexEd.global_scale
			if (self.mouse ~= 5) and (self.mouse ~= -1) and self.dir_resize_proc then
				self:beginDirectionalResize( self.mouse )
			elseif self.mouse == 5 then
				self:beginMoveSelection()
			else
				self:initMoveFullTexture()
			end
		end
	elseif key == 1 then
		if TexEd.held[keys.lctrl] then
			self:beginSelection()
		end
	end
end

function TextureBox:mouseHover( mx, my )
	local x, y = self.wi.x - self.pos.x + self.selection.x, self.wi.y - self.pos.y + self.selection.y
	local w, h = self.selection.w * TexEd.global_scale, self.selection.h * TexEd.global_scale
	if point_in_box(x, y, w/5, h/5, mx, my ) then
		if self.mouse ~= 7 then
			self.mouse = 7
			WidgetSetVisible( self.mwidget, true )
			WidgetSetPos( self.mwidget, x, y )
			WidgetSetSize( self.mwidget, w/5, h/5 )
		end
	elseif point_in_box(x+w/5, y, 3*w/5, h/5, mx, my ) then
		if not self.mouse ~= 8 then
			self.mouse = 8
			WidgetSetVisible( self.mwidget, true )
			WidgetSetPos( self.mwidget, x+w/5, y )
			WidgetSetSize( self.mwidget, 3*w/5, h/5 )
		end
	elseif point_in_box(x+4*w/5, y, w/5, h/5, mx, my ) then
		if self.mouse ~= 9 then
			self.mouse = 9
			WidgetSetVisible( self.mwidget, true )
			WidgetSetPos( self.mwidget, x+4*w/5, y )
			WidgetSetSize( self.mwidget, w/5, h/5 )
		end
	elseif point_in_box(x, y+h/5, w/5, 3*h/5, mx, my ) then
		if self.mouse ~= 4 then
			self.mouse = 4
			WidgetSetVisible( self.mwidget, true )
			WidgetSetPos( self.mwidget, x, y+h/5 )
			WidgetSetSize( self.mwidget, w/5, 3*h/5 )
		end
	elseif point_in_box(x+w/5, y+h/5, 3*w/5, 3*h/5, mx, my ) then
		if self.mouse ~= 5 then
			self.mouse = 5
			WidgetSetVisible( self.mwidget, true )
			WidgetSetPos( self.mwidget, x+w/5, y+h/5 )
			WidgetSetSize( self.mwidget, 3*w/5, 3*h/5 )
		end
	elseif point_in_box(x+4*w/5, y+h/5, w/5, 3*h/5, mx, my ) then
		if self.mouse ~= 6 then
			self.mouse = 6
			WidgetSetVisible( self.mwidget, true )
			WidgetSetPos( self.mwidget, x+4*w/5, y+h/5 )
			WidgetSetSize( self.mwidget, w/5, 3*h/5 )
		end
	elseif point_in_box(x, y+4*h/5, w/5, h/5, mx, my ) then
		if self.mouse ~= 1 then
			self.mouse = 1
			WidgetSetVisible( self.mwidget, true )
			WidgetSetPos( self.mwidget, x, y+4*h/5 )
			WidgetSetSize( self.mwidget, w/5, h/5 )
		end
	elseif point_in_box(x+w/5, y+4*h/5, 3*w/5, h/5, mx, my ) then
		if self.mouse ~= 2 then
			self.mouse = 2
			WidgetSetVisible( self.mwidget, true )
			WidgetSetPos( self.mwidget, x+w/5, y+4*h/5 )
			WidgetSetSize( self.mwidget, 3*w/5, h/5 )
		end
	elseif point_in_box(x+4*w/5, y+4*h/5, w/5, h/5, mx, my ) then
		if self.mouse ~= 3 then
			self.mouse = 3
			WidgetSetVisible( self.mwidget, true )
			WidgetSetPos( self.mwidget, x+4*w/5, y+4*h/5 )
			WidgetSetSize( self.mwidget, w/5, h/5 )
		end
	else
		self.mouse = -1
		WidgetSetVisible( self.mwidget, false )
	end
end

function TextureBox:resizeFullTextureObj(x, y, w, h)
	self.wi.x = x or self.wi.x
	x = self.wi.x
	self.wi.y = y or self.wi.y
	y = self.wi.y
	self.wi.w = w or self.wi.w 
	w = self.wi.w
	self.wi.h = h or self.wi.h
	h = self.wi.h
end

function TextureBox:initMoveFullTexture()
	Resume(NewThread(mouseTracker), 0, self.relShiftFullTexture, {self})
end

function TextureBox:relShiftFullTexture(dx, dy)
	local x = math.max(self.pos.x - dx, 0)
	local y = math.max(self.pos.y - dy, 0)
	
	local w = math.min(self.wi.w / TexEd.global_scale, self.size.w)
	local h = math.min(self.wi.h / TexEd.global_scale, self.size.h)
	
	x = math.min(x, self.size.w - w)
	y = math.min(y, self.size.h - h)

	local ww = math.min(self.wi.w, self.size.w * TexEd.global_scale)
	local wh = math.min(self.wi.h, self.size.h * TexEd.global_scale)
	WidgetSetSize(self.wi.wi, ww, wh)
	
	-- Log("moveFullTexture ", x, " ", y, " ", w, " ", h)
	
	ChangeTexFrames(self.name, {count = 1; main ={ {["x"] = x, ["y"] = y, ["w"] = w, ["h"] = h} }})
	
	self:moveSelectionWithTex(x, y)
	self.pos.x = x
	self.pos.y = y
end

-- function ExampleTextureBox:initMoveFullTexture()
-- end

function ExampleTextureBox:relShiftFullTexture(dx, dy)
	local fm = TexEd.getCurrentFrameMain()
	local fo = TexEd.getCurrentFrameCurOverlay()
	local fx, fy = fm.x, fm.y	
	local fw = math.max(fm.w, fo.w + fo.ox)
	local fh = math.max(fm.h, fo.h + fo.oy)
	
	local x = math.max(self.pos.x - dx, 0)
	local y = math.max(self.pos.y - dy, 0)
	local w = math.min(self.wi.w / TexEd.global_scale, fw)
	local h = math.min(self.wi.h / TexEd.global_scale, fh)
	x = math.min(x, fw - w)
	y = math.min(y, fh - h)

	local ww = math.min(self.wi.w, fw * TexEd.global_scale)
	local wh = math.min(self.wi.h, fh * TexEd.global_scale)
	WidgetSetSize(self.wi.wi, ww, wh)
	
	ChangeTexFrames(self.name, {
		count = 1; 
		main ={ {["x"] = x + fx, ["y"] = y + fy, ["w"] = w, ["h"] = h} }; 
		overlay = { { {["x"] = x + fo.x, ["y"] = y + fo.y, ["w"] = fo.w, ["h"] = fo.h, ["ox"] = fo.ox, ["oy"] = fo.oy} } }
	})
	
	self:moveSelectionWithTex(x, y)
	self.pos.x = x
	self.pos.y = y
end

----------------------------------------------

function TextureBox:initSelection(x, y, w, h)
	if not self.selection then
		self.selection = {}
	end
	
	self.selection.x = x
	self.selection.y = y
	self.selection.w = w
	self.selection.h = h
	self.selection.base_ox = self.pos.x * TexEd.global_scale
	self.selection.base_oy = self.pos.y * TexEd.global_scale
	
	local x = self.selection.x * TexEd.global_scale - self.selection.base_ox + self.wi.x
	local y = self.selection.y * TexEd.global_scale - self.selection.base_oy + self.wi.y
	local w = self.selection.w * TexEd.global_scale
	local h = self.selection.h * TexEd.global_scale
	
	if not self.selection.wi then
		
		self.selection.wi = CreateWidget(constants.wt_Widget, "selection", self.wi.wi,
			x, y, w, h)
		WidgetSetBorder(self.selection.wi, true)
		WidgetSetBorderColor(self.selection.wi, {1, .5, 0, 1}, false)
		WidgetSetFocusable(self.selection.wi, false)
	else
		WidgetSetPos(self.selection.wi, x, y)
		WidgetSetSize(self.selection.wi, w, h)
	end
end

function TextureBox:removeSelection()
	if not self.selection then return end
	if self.selection.wi then
		DestroyWidget(self.selection.wi)
	end
	self.selection = {}
end

function TextureBox:updateSelection()
	if not self.frame_get_func then return end
	local f = self.frame_get_func()
	-- Log(self)
	-- LogT(f)
	if f then
		self:initSelection(f.x, f.y, f.w, f.h)
	else
		self:removeSelection()
	end
end

function TextureBox:setSelectionData(frame_get_func, resize_proc, move_proc, dir_resize_proc)
	if not self.selection then
		self.selection = {}
	end
	
	self.frame_get_func = frame_get_func
	self.resize_proc = resize_proc
	self.move_proc = move_proc
	self.dir_resize_proc = dir_resize_proc
end

function TextureBox:beginSelection()
	-- Log("TexEd.beginSelection()")
	Resume(NewThread(mouseTracker), 1, self.resize_proc)
end

function TextureBox:beginMoveSelection()
	if not (self.selection and self.selection.wi) then return end
	Resume(NewThread(mouseTracker), 0, self.move_proc)
end

function TextureBox:beginDirectionalResize( dir )
-- 7 8 9
-- 4   6
-- 1 2 3
	if not (self.selection and self.selection.wi) then return end
	Resume(NewThread(mouseTracker), 0, self.dir_resize_proc( dir ))
end

function TextureBox:moveSelectionWithTex(ox, oy)
	if self.selection and self.selection.wi then 
		self.selection.base_ox = ox * TexEd.global_scale
		self.selection.base_oy = oy * TexEd.global_scale
		local x = self.selection.x * TexEd.global_scale
		local y = self.selection.y * TexEd.global_scale
		WidgetSetPos(self.selection.wi, x - self.selection.base_ox + self.wi.x, y - self.selection.base_oy + self.wi.y)
	end
end
--------------------------------------------------


function TexEd.initTexBoxMain()
	local x = TexEd.tex_boxes_left
	local y = 1
	local w = CONFIG.scr_width - x - 2
	local h = CONFIG.scr_height - y - 2 - CONFIG.scr_height/2

	TexEd.tex_boxes[1] = TextureBox.create("__full_", image_file, x, y, w, h)
	TexEd.tex_boxes[1]:setSelectionData(TexEd.getCurrentFrameMain, TexEd.relResizeCurFrameMain, TexEd.relMoveCurFrameMain, function( dir )
		local kx, ky = 1, 1
		if dir == 8 or dir == 2 then
			kx = 0
		elseif dir == 4 or dir == 6 then
			ky = 0
		end
		if dir == 7 or dir == 8 or dir == 9 then
			ky = -1
		end
		if dir == 7 or dir == 4 or dir == 4 then
			kx = -1
		end
		return function( dx, dy )
			local f = TexEd.getCurrentFrameMain()
			local w = f.w + dx * kx
			local h = f.h + dy * ky
			local x = f.x
			local y = f.y
			if ( w < 0 ) then w = 0 end
			if ( h < 0 ) then h = 0 end
			if dir == 7 then
				TexEd.setCurFrameData(x - kx*dx, y - ky*dy, w, h)
			elseif dir == 8 then
				TexEd.setCurFrameData(x, y - ky*dy, w, h)
			elseif dir == 9 then
				TexEd.setCurFrameData(x, y - ky*dy, w, h)
			elseif dir == 4 then
				TexEd.setCurFrameData(x - kx*dx, y, w, h)
			else
				TexEd.setCurFrameData(x, y, w, h)
			end
		end
	end)
end

function TexEd.initTexBoxOverlay()
	local x = TexEd.tex_boxes_left
	local y = CONFIG.scr_height/2 + 2 
	local w = (CONFIG.scr_width - x - 2) * 2 / 3
	local h = CONFIG.scr_height - y - 2
	
	TexEd.tex_boxes[2] = TextureBox.create("__overlay_", image_file, x, y, w, h)
	TexEd.tex_boxes[2]:setSelectionData(TexEd.getCurrentFrameCurOverlay, TexEd.relResizeCurFrameCurOverlay, TexEd.relMoveCurFrameCurOverlay, function( dir )
		local kx, ky = 1, 1
		if dir == 8 or dir == 2 then
			kx = 0
		elseif dir == 4 or dir == 6 then
			ky = 0
		end
		if dir == 7 or dir == 8 or dir == 9 then
			ky = -1
		end
		if dir == 7 or dir == 4 or dir == 4 then
			kx = -1
		end
		return function( dx, dy )
			local f = TexEd.getCurrentFrameCurOverlay()
			local w = f.w + dx * kx
			local h = f.h + dy * ky
			local x = f.x
			local y = f.y
			if ( w < 0 ) then w = 0 end
			if ( h < 0 ) then h = 0 end
			if dir == 7 then
				TexEd.setCurFrameData(x - kx*dx, y - ky*dy, w, h, f.ox, f.oy)
			elseif dir == 8 then
				TexEd.setCurFrameData(x, y - ky*dy, w, h, f.ox, f.oy)
			elseif dir == 9 then
				TexEd.setCurFrameData(x, y - ky*dy, w, h, f.ox, f.oy)
			elseif dir == 4 then
				TexEd.setCurFrameData(x - kx*dx, y, w, h, f.ox, f.oy)
			else
				TexEd.setCurFrameData(x, y, w, h, f.ox, f.oy)
			end
		end
	end )
end

function TexEd.initTexBoxExample()
	local x =  TexEd.tex_boxes_left + (CONFIG.scr_width - TexEd.tex_boxes_left + 2) * 2 / 3
	local y = CONFIG.scr_height/2 + 2 
	local w = CONFIG.scr_width - x - 2
	local h = CONFIG.scr_height - y - 2
	
	-- Log(x, "  ", w)
	
	TexEd.tex_boxes[3] = ExampleTextureBox.create(TexEd.tex.name, x, y, w, h)
	
	local function getExOvData()
		local o = deep_copy(TexEd.getCurrentFrameCurOverlay())
		o.x = o.ox
		o.y = o.oy
		return o
	end
		
	TexEd.tex_boxes[3]:setSelectionData(getExOvData, nil, TexEd.relMoveCurFrameCurOverlayOffset)
end
-------------------------------------------

function TexEd.scaleChanged()
	TexEd.updateCurrentFrame()
	for k,v in pairs(TexEd.tex_boxes) do
		v:relShiftFullTexture(0, 0)
	end
end


--------------------------------------------

function TexEd.initTexture()
	TexEd.tex.name = LoadTexture(image_file)
	TexEd.tex.frames = DumpTexFrames(TexEd.tex.name)
	TexEd.tex.cur_frame = 1
	TexEd.tex.cur_overlay = 0
	
	--TexEd.tex.frames_filename = table.concat({"textures\\", TexEd.tex.name, ".lua"})
	TexEd.tex.frames_filename = table.concat({"", TexEd.tex.name, ".lua"})
	Log("TexEd.tex.frames_filename ", TexEd.tex.frames_filename )
	
	if false then 
		TexEd.ctrls = nil
	else
		TexEd.initFramesList()
		TexEd.initFrameInfoShower()
		TexEd.initOverlayList()
		TexEd.initOverlayInfoShower()
		TexEd.initMousePosWidget()
	end
	
	TexEd.selectFrame(1)
	TexEd.selectOverlay(1)
end

function TexEd.saveTextureFrames()
	if not TexEd.tex or not TexEd.tex.frames then return end
	
	if not TexEd.tex.frames_filename then 
		Log("WTF! Where is name!")
		return
	end
	
	Log("Saving file to ", TexEd.tex.frames_filename)
	local ffile = io.open(TexEd.tex.frames_filename, "w")
	
	local t = {}
	local fr = TexEd.tex.frames
	local ins = function(what) table.insert(t, what) end
	local nl = function() ins("\n") end
	local insn = function(what) ins(what); nl(); end
	

	--LogT(TexEd.tex.frames)
	
	insn(string.format("count = %d", fr.count))
	insn("main = {")
	for k,v in ipairs(fr.main) do
		local comma = k < fr.count and "," or ""
		insn(string.format("\t{ x = %d, y = %d, w = %d, h = %d }%s\t-- frame %d", v.x, v.y, v.w, v.h, comma, k-1))
	end
	insn("}")
	nl()
	
	if fr.overlay and #fr.overlay > 0 then
		insn("overlay = {")
		for _,o in ipairs(fr.overlay) do
			insn("\t{")
			for k,v in ipairs(o) do
				local comma = k < fr.count and "," or ""
				insn(string.format("\t\t{ x = %d, y = %d, w = %d, h = %d, ox = %d, oy = %d }%s\t-- frame %d", v.x, v.y, v.w, v.h, v.ox, v.oy, comma, k-1))
			end
			ins("\t}")
			if _ < #fr.overlay then ins(",") end
			nl()
		end
		insn("}")
	end
	ffile:write(table.concat(t))
	
	ffile:close()
	Log("File successfully saved")
end

--------------------------------------------

function TexEd.selectNextFrame()
	TexEd.selectFrame(TexEd.tex.cur_frame + 1)
end

function TexEd.selectPrevFrame()
	TexEd.selectFrame(TexEd.tex.cur_frame - 1)
end

function TexEd.selectFrame(n)
	n = clamp(n, 1, TexEd.tex.frames.count)
	TexEd.tex.cur_frame = n
	
	for k,v in pairs(TexEd.tex_boxes) do
		v:updateSelection()
		v:curFrameUpdated()
	end

	TexEd.updateFrameInfoShower()
	TexEd.updateOverlayInfoShower()
	if TexEd.ctrls then 
		TexEd.ctrls.__frameInput:setText(TexEd.tex.cur_frame)
	end
end

function TexEd.addFrame()
	local f = { x = 0, y = 0, w = 1, h = 1 }
	local o = { x = 0, y = 0, w = 1, h = 1, ox = 0, oy = 0 }
	local fr = TexEd.tex.frames
	
	fr.count = fr.count + 1	
	table.insert(fr.main, f)
	assert(#fr.main == fr.count)
	if fr.overlay then
		for k,v in ipairs(fr.overlay) do
			table.insert(v, o)
			assert(#v == fr.count)
		end
	end
	return fr.count
end

function TexEd.addFrameAndSelect()
	TexEd.selectFrame(TexEd.addFrame())
end

function TexEd.removeFrame(n)
	local fr = TexEd.tex.frames
	
	fr.count = fr.count - 1	
	table.remove(fr.main, n)
	assert(#fr.main == fr.count)	
	if fr.overlay then
		for k,v in ipairs(fr.overlay) do
			table.remove(v, n)
			assert(#v == fr.count)
		end
	end
	
	if fr.count == 0 then
		TexEd.addFrame()
	end
	return math.min(n, fr.count)
end

function TexEd.removeCurrentFrame()
	local cur = TexEd.tex.cur_frame
	return TexEd.removeFrame(cur)
end

function TexEd.removeCurrentFrameAndSelect()
	TexEd.selectFrame(TexEd.removeCurrentFrame())
end

function TexEd.selectOverlay(n)
	if not TexEd.tex.frames.overlay then 
		TexEd.tex.cur_overlay = 0
	else
		TexEd.tex.cur_overlay = clamp(n, 1, #TexEd.tex.frames.overlay)
	end
	
	for k,v in pairs(TexEd.tex_boxes) do
		v:updateSelection()
	end
	
	TexEd.updateFrameInfoShower()
	TexEd.updateOverlayInfoShower()
	if TexEd.ctrls then
		TexEd.ctrls.__overlayInput:setText(TexEd.tex.cur_overlay)
	end
end

function TexEd.addOverlay()
	local fr = TexEd.tex.frames
	if not fr.overlay then fr.overlay = {} end
	
	local o = {}
	for i = 1, fr.count do
		table.insert(o, { x = 0, y = 0, w = 1, h = 1, ox = 0, oy = 0 })
	end
	assert(#o == fr.count)
	table.insert(fr.overlay, o)
	
	return #fr.overlay
end

function TexEd.addOverlayAndSelect()
	TexEd.selectOverlay(TexEd.addOverlay())
end

function TexEd.removeOverlay(n)
	local fr = TexEd.tex.frames
	if not fr.overlay then return 0 end
	
	table.remove(fr.overlay, n)
	local cur = #fr.overlay 
	if cur == 0 then fr.overlay = nil end
	return cur
end

function TexEd.removeCurrentOverlay()
	if TexEd.tex.cur_overlay == 0 then return 0 end
	return TexEd.removeOverlay(TexEd.tex.cur_overlay)
end

function TexEd.removeCurrentOverlayAndSelect()
	TexEd.selectOverlay(TexEd.removeCurrentOverlay())
end

function TexEd.selectNextOverlay()
	TexEd.selectOverlay(TexEd.tex.cur_overlay + 1)
end

function TexEd.selectPrevOverlay()
	TexEd.selectOverlay(TexEd.tex.cur_overlay - 1)
end

function TexEd.getCurrentFrameMain()
	return TexEd.tex.frames.main[TexEd.tex.cur_frame]
end

function TexEd.getCurrentFrameCurOverlay()
	local o =TexEd.tex.frames.overlay 
	if o and o[TexEd.tex.cur_overlay] then
		local oo = o[TexEd.tex.cur_overlay]
		return oo[TexEd.tex.cur_frame]
	end
	return nil
end

function TexEd.setCurFrameData(x, y, w, h, ox, oy)
	local f = nil
	if ox and oy then
		f = TexEd.getCurrentFrameCurOverlay()
	else
		f = TexEd.getCurrentFrameMain()
	end
	assert(f)
	f.x = clamp(x, 0, TexEd.full_tex_size.w - 1)
	f.y = clamp(y, 0, TexEd.full_tex_size.h - 1)
	f.w = clamp(w, 1, TexEd.full_tex_size.w - f.x)
	f.h = clamp(h, 1, TexEd.full_tex_size.h - f.y)
	if ox and oy then
		f.ox = ox
		f.oy = oy
	end
	TexEd.updateCurrentFrame()
end

function TexEd.updateCurrentFrame()
	TexEd.selectFrame(TexEd.tex.cur_frame)
end

function TexEd.relMoveCurFrameMain(dx, dy)
	local f = TexEd.getCurrentFrameMain()
	TexEd.setCurFrameData(f.x + dx, f.y + dy, f.w, f.h)
end

function TexEd.relResizeCurFrameMain(dw, dh)
	Log("TexEd.relResizeCurFrameMain ", dw, " ", dh)
	local f = TexEd.getCurrentFrameMain()
	TexEd.setCurFrameData(f.x, f.y, f.w + dw, f.h + dh)
end

function TexEd.relResizeCurFrameCurOverlay(dw, dh)
	local f = TexEd.getCurrentFrameCurOverlay()
	TexEd.setCurFrameData(f.x, f.y, f.w + dw, f.h + dh, f.ox, f.oy)
end

function TexEd.relMoveCurFrameCurOverlay(dx, dy)
	local f = TexEd.getCurrentFrameCurOverlay()
	TexEd.setCurFrameData(f.x + dx, f.y + dy, f.w, f.h, f.ox, f.oy)
end

function TexEd.relMoveCurFrameCurOverlayOffset(dx, dy)
	local f = TexEd.getCurrentFrameCurOverlay()
	TexEd.setCurFrameData(f.x, f.y, f.w, f.h, f.ox + dx, f.oy + dy)
end
------------------------------------------


function TexEd.initFramesList()
	local fmenu = PagedMenu.create()
	fmenu:addPage( 1, SimpleMenu.create( {0, 0}, 1, -1, 3, 10 ) )
	local curFrame = TexEd.tex.cur_frame
	vars.__input = curFrame
	TexEd.ctrls.__frameInput = MWInputField.create(
				{name="dialogue"}, 
				curFrame, 0, nil, 
				function( sender, value )
					local curFrame = math.floor(tonumber(value))
					TexEd.selectFrame(clamp(curFrame, 1, TexEd.tex.frames.count))
				end
				, "__input", 10
	)
	fmenu:add(TexEd.ctrls.__frameInput, 1)
	fmenu:add( MWComposition.create( 10, {
		MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"next(q)",
			0,
			TexEd.selectNextFrame,
			nil, nil),
		MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"prev(w)",
			0,
			TexEd.selectPrevFrame,
			nil, nil )
	}))
	fmenu:add( MWComposition.create( 10, {
		MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"add",
			0,
			function() TexEd.addFrameAndSelect() end,
			nil, nil),
		MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"remove",
			0,
			function() TexEd.removeCurrentFrameAndSelect() end,
			nil, nil)
	}))
	--fmenu:show(1)
	fmenu:move(TexEd.tex_boxes_left - 3, 20)
	TexEd.ctrls.framesChoser = fmenu
end

function TexEd.initFrameInfoShower()
	local fmenu = PagedMenu.create()
	fmenu:addPage( 1, SimpleMenu.create( {0, 0}, 1,-1, 10, 10 ) )
	
	TexEd.ctrls.__frame_x = MWFunctionButton.create({name="dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}}, "x: errr", 0, 
		function() TexEd.relMoveCurFrameMain(1, 0) end, nil, nil,
		function() TexEd.relMoveCurFrameMain(-1, 0) end)
	TexEd.ctrls.__frame_y = MWFunctionButton.create({name="dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}}, "y: errr", 0, 
		function() TexEd.relMoveCurFrameMain(0, 1) end, nil, nil,
		function() TexEd.relMoveCurFrameMain(0, -1) end)
	TexEd.ctrls.__frame_w = MWFunctionButton.create({name="dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}}, "w: errr", 0,
		function() TexEd.relResizeCurFrameMain(1, 0) end, nil, nil,
		function() TexEd.relResizeCurFrameMain(-1, 0) end)
	TexEd.ctrls.__frame_h = MWFunctionButton.create({name="dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}}, "h: errr", 0,
		function() TexEd.relResizeCurFrameMain(0, 1) end, nil, nil,
		function() TexEd.relResizeCurFrameMain(0, -1) end)
	
	fmenu:add(TexEd.ctrls.__frame_x, 1)
	fmenu:add(TexEd.ctrls.__frame_y, 1)
	fmenu:add(TexEd.ctrls.__frame_w, 1)
	fmenu:add(TexEd.ctrls.__frame_h, 1)
	
	fmenu:move(TexEd.tex_boxes_left - 3, TexEd.ctrls.framesChoser.pages[1].position[2] + TexEd.ctrls.framesChoser.pages[1].size[2])
	-- fmenu:move(TexEd.full_tex.wi_x - 3, 40)
	TexEd.ctrls.frameInfoShower = fmenu
	TexEd.updateFrameInfoShower()
end

function TexEd.updateFrameInfoShower()
	if not TexEd.ctrls then return end
	local f = TexEd.getCurrentFrameMain()
	TexEd.ctrls.__frame_x:setText("x: " .. f.x)
	TexEd.ctrls.__frame_y:setText("y: " .. f.y)
	TexEd.ctrls.__frame_w:setText("w: " .. f.w)
	TexEd.ctrls.__frame_h:setText("h: " .. f.h)
end

function TexEd.initOverlayList()
	local fmenu = PagedMenu.create()
	fmenu:addPage( 1, SimpleMenu.create( {0, 0}, 1, -1, 3, 10 ) )
	local curOverlay = TexEd.tex.cur_overlay
	vars.__input = curOverlay
	TexEd.ctrls.__overlayInput = MWInputField.create(
				{name="dialogue"}, 
				curOverlay, 0, nil, 
				function( sender, value )
					local curOverlay = math.floor(tonumber(value))
					TexEd.selectOverlay(curOverlay)
				end
				, "__input", 10
	)
	fmenu:add(TexEd.ctrls.__overlayInput, 1)
	fmenu:add( MWComposition.create( 10, {
		MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"next(a)",
			0,
			TexEd.selectNextOverlay,
			nil, nil),
		MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"prev(s)",
			0,
			TexEd.selectPrevOverlay,
			nil, nil )
	}))
	fmenu:add( MWComposition.create( 10, {
		MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"add",
			0,
			function() TexEd.addOverlayAndSelect() end,
			nil, nil),
		MWFunctionButton.create( {name = "dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}},
			"remove",
			0,
			function() TexEd.removeCurrentOverlayAndSelect() end,
			nil, nil )
	}))
	--fmenu:show(1)
	fmenu:move(TexEd.tex_boxes_left - 3, TexEd.ctrls.frameInfoShower.pages[1].position[2] + TexEd.ctrls.frameInfoShower.pages[1].size[2])
	TexEd.ctrls.overlayChoser = fmenu
end

function TexEd.initOverlayInfoShower()
	local fmenu = PagedMenu.create()
	fmenu:addPage( 1, SimpleMenu.create( {0, 0}, 1,-1, 10, 10 ) )
	
	TexEd.ctrls.__overlay_x = MWFunctionButton.create({name="dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}}, "x: errr", 0, 
		function() TexEd.relMoveCurFrameCurOverlay(1, 0) end, nil, nil,
		function() TexEd.relMoveCurFrameCurOverlay(-1, 0) end)
	TexEd.ctrls.__overlay_y = MWFunctionButton.create({name="dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}}, "y: errr", 0, 
		function() TexEd.relMoveCurFrameCurOverlay(0, 1) end, nil, nil,
		function() TexEd.relMoveCurFrameCurOverlay(0, -1) end)
	TexEd.ctrls.__overlay_w = MWFunctionButton.create({name="dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}}, "w: errr", 0, 
		function() TexEd.relResizeCurFrameCurOverlay(1, 0) end, nil, nil,
		function() TexEd.relResizeCurFrameCurOverlay(-1, 0) end)
	TexEd.ctrls.__overlay_h = MWFunctionButton.create({name="dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}}, "h: errr", 0, 
		function() TexEd.relResizeCurFrameCurOverlay(0, 1) end, nil, nil,
		function() TexEd.relResizeCurFrameCurOverlay(0, -1) end)
	TexEd.ctrls.__overlay_ox = MWFunctionButton.create({name="dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}}, "ox: errr", 0, 
		function() TexEd.relMoveCurFrameCurOverlayOffset(1, 0) end, nil, nil,
		function() TexEd.relMoveCurFrameCurOverlayOffset(-1, 0) end)
	TexEd.ctrls.__overlay_oy = MWFunctionButton.create({name="dialogue", color = {1,1,1,1}, active_color={.2,.2,1,1}}, "oy: errr", 0, 
		function() TexEd.relMoveCurFrameCurOverlayOffset(0, 1) end, nil, nil,
		function() TexEd.relMoveCurFrameCurOverlayOffset(0, -1) end)
	
	fmenu:add(TexEd.ctrls.__overlay_x, 1)
	fmenu:add(TexEd.ctrls.__overlay_y, 1)
	fmenu:add(TexEd.ctrls.__overlay_w, 1)
	fmenu:add(TexEd.ctrls.__overlay_h, 1)
	fmenu:add(TexEd.ctrls.__overlay_ox, 1)
	fmenu:add(TexEd.ctrls.__overlay_oy, 1)
	
	fmenu:move(TexEd.tex_boxes_left - 3, TexEd.ctrls.overlayChoser.pages[1].position[2] + TexEd.ctrls.overlayChoser.pages[1].size[2])
	-- fmenu:move(TexEd.full_tex.wi_x - 3, 40)
	TexEd.ctrls.frameInfoShower = fmenu
	TexEd.updateOverlayInfoShower()
end

function TexEd.updateOverlayInfoShower()
	if not TexEd.ctrls then return end
	local f = TexEd.getCurrentFrameCurOverlay()
	if not f then return end
	TexEd.ctrls.__overlay_x:setText("x: " .. f.x)
	TexEd.ctrls.__overlay_y:setText("y: " .. f.y)
	TexEd.ctrls.__overlay_w:setText("w: " .. f.w)
	TexEd.ctrls.__overlay_h:setText("h: " .. f.h)
	TexEd.ctrls.__overlay_ox:setText("ox: " .. f.ox)
	TexEd.ctrls.__overlay_oy:setText("oy: " .. f.oy)
end

function TexEd.initMousePosWidget()
	local widget = CreateWidget( constants.wt_Label, "position", nil, 5, CONFIG.scr_height-15, 1, 1 )
	WidgetSetCaptionFont( widget, "dialogue" )
	WidgetSetCaptionColor( widget, {1,1,1,1}, false )
	WidgetSetCaption( widget, "(0, 0)" )
	TexEd.ctrls.__mouse_pos = widget
end

function TexEd.updateMousePosWidget()
	WidgetSetCaption( TexEd.ctrls.__mouse_pos, string.format("(%s, %s)(%s, %s)", TexEd.mouse_x, TexEd.mouse_y, TexEd.mouse_x_rel, TexEd.mouse_y_rel) )
end

---------------------------------------------


TexEd.init()

-- open_texture()
