Loader = {}

if constants.__SOUND_LIBRARY == "SDL_mixer" then

local _PlaySnd = PlaySnd
PlaySnd = function( name, restart, x, y, relativity, volume )
		if name and (string.match(name, "%.it$") or string.match(name, "%.mod$") or string.match(name, "%.xm$")) then
			PlayBackMusic( name, volume, false )
		else
			_PlaySnd( name, restart, x, y, relativity, volume )
		end
end

end

-------------------------------- slave key processor ---------------------------

Loader.bind = {} -- Table of functions processing the key press. Indices are key names.
Loader.slaveKeyProcessors = {}
Loader.slaveKeyReleaseProcessors = {}

local function __slaveKeyProcessingAddRemFuncs( t )
	local function add(proc)
		t[proc] = t[proc] and t[proc] + 1 or 1
		return proc
	end

	local function remove(key)
		assert(key)
		assert(t[key])
		t[key] = t[key] - 1
		if t[key] == 0 then t[key] = nil end
	end

	return add, remove
end

local function __slaveKeyProcessingFuncs()
	local oldKDproc = nil
	local oldKRproc = nil

	local function enableSlaveKeyProcessing()
		assert(oldKDproc == nil)
		assert(oldKRproc == nil)
		oldKDproc = GlobalGetKeyDownProc()
		oldKRproc = GlobalGetKeyReleaseProc()
		GlobalSetKeyDownProc( Loader.keyProcessor )
		GlobalSetKeyReleaseProc( Loader.keyReleaseProcessor )
	end

	local function disableSlaveKeyProcessing()
		GlobalSetKeyDownProc( oldKDproc )
		GlobalSetKeyReleaseProc( oldKRproc )
	end

	return enableSlaveKeyProcessing, disableSlaveKeyProcessing
end

Loader.addSlaveKeyProcessor,        Loader.removeSlaveKeyProcessor        = __slaveKeyProcessingAddRemFuncs(Loader.slaveKeyProcessors)
Loader.addSlaveKeyReleaseProcessor, Loader.removeSlaveKeyReleaseProcessor = __slaveKeyProcessingAddRemFuncs(Loader.slaveKeyReleaseProcessors)

Loader.enableSlaveKeyProcessing, Loader.disableSlaveKeyProcessing = __slaveKeyProcessingFuncs()

--------------------------------------------------------------------------------

dbg = require("debug_tools")

require("serialize")
Saver = require("saves")
require("gui-menu")
require("console")
require("pear15-game")
require("players")


-- GUI = require("gui").create()

if Game then 
	GUI = Game.GUI
else
	GUI = require("gui_empty").create()
end


function Loader.start( nomenu )
	Loader.enableSlaveKeyProcessing()
	if not nomenu then Menu.showMainMenu() end
end

function Loader.gameInit()
	Loader.old_time = GetCurTime()
	Loader.dt = 0
	Loader.time = 0
	Log("Init Loader.time = 0 Loader.old_time = ", Loader.old_time)
	GUI:hide()
	
	if not Editor and Game and Game.InitGameSession then
		Game.InitGameSession()
	else
		SetOnPlayerDeathProcessor(nil)
		SetOnChangePlayerProcessor(nil)
	end
	
	Loader.game_timer_enabled = true
end

function Loader.setMapName(map_name)
	Loader.map_name = "scripts/levelscripts/"..map_name..".lua"
end

function Loader.startGame(map_name, no_newstat)
	assert(Game)
	Game.startGame(map_name)
	Loader.setMapName(map_name)
	if not no_newstat then
		assert(Game.Prepare)
		Game.Prepare()
	end
	return Loader.restartGame()
end

function Loader.restartGame()
	Log("Loader.restartGame")
	
	 -- cleanup
	StopMapThreads()
	Game.restartGame()
	if Loader.level and Loader.level.cleanUp then Loader.level.cleanUp() end
	if GUI.game_over_widgets then GUI.game_over_widgets:show(false) end
	Menu:hide()
	if vars.menu_thread then
		StopThread( vars.menu_thread )
		if vars.menu_thread_cleanup then
			vars.menu_thread_cleanup()
		end
		vars.menu_thread = nil
	end
	
	assert(Game.LevelCleanup)
	Game.LevelCleanup()
	
	assert(Game.PrepareMapvar)
	Game.PrepareMapvar()
	
	assert(Game.RestoreMapvar)
	Game.RestoreMapvar()
	
	Game.InitWeapons(1)
	
	TogglePause();
	
	Menu.mn = false;
	Menu.locked = {}
	
	Log('Loading map: ', Loader.map_name)
	local result, success = loadfile(Loader.map_name)
	if result then
		Log("File loaded ", result)
		success, result = pcall(result)
	else
		Log("Failed to load file")
		result = success
		success = nil
	end
	if success then
		Loader.level = result
		Loader.level.SetLoader(Loader)

		Players.UpdatePlayers() -- < Really, really bad place
		Game.UpdatePlayers()  
			
		if Loader.level.InitScript and not Editor then
			Loader.level.InitScript()
		end
		
		-- starting mission
		Loader.state = 'new'
		
		Loader.gameInit()
		
		if not Loader.missionThread then 
			Loader.missionThread = NewThread( Loader.mainLoop );
			if Loader.missionThread then  
				Resume(Loader.missionThread);
			end
		end
		return true
	else
		return false, "Failed to load map: "..result
	end
end

-- ���������� ��� ����� ������ �� ������� new_level
function Loader.ChangeLevel(new_level)
	Log("Loader.ChangeLevel ", new_level)
	Loader.restoreCamera()
	StopMapThreads()
	Game.ChangeLevel(new_level)
	if Game.AbsorbMapvar then Game.AbsorbMapvar() end
	
	Loader.setMapName(new_level)
	Loader.restartGame()
end

function Loader.exitGame()
	-- destroying all widgets
	Loader.restoreCamera()
	SwitchLighting(true)
	Game.exitGame()
	StopMapThreads()
	StopBackMusic()
	GUI:hide()
	GUI:hideCombo()
	-- GUI.gc:destroy()
	-- GUI.widgetContainer:destroy()
	-- GUI.game_over_widgets:destroy()
	Menu.mn = false
	GUI:destroy()
	GUI = GUI.create()
	-- leaving mission
	if Loader.level and Loader.level.cleanUp then Loader.level.cleanUp() end
	assert(Game.LevelCleanup)
	Game.LevelCleanup()
	Loader.level = nil
	Loader.state = ''    --idling mainLoop thread
	DestroyGame()        -- destroying level
	Menu.showMainMenu()
end

function Loader.exitSlowly()
	Loader.exit_needed = true
end

------------------------------- main script game loop  ---------------------------------------------------
function Loader.mainLoop()
	while true do
		if Loader.state == 'new' then
			Log("Loader.mainLoop() state = 'new'")
			Loader.state = 'game'
			Loader.gameOverShown = false
			Menu.goshown = false
			GUI:init()
		elseif Loader.state == 'game' then
			Loader.ingame()
			if Loader.exit_needed then
				Loader.exit_needed = nil
				Loader.exitGame()
			end
		elseif Loader.state == 'game over' then
			Loader.showGameOver()
		end
		Wait(1)
	end
end

function Loader.showMenu()
	push_pause( true )
	Menu:show(3)
	Menu.mn = true
	if Menu.prepareIngameMenu then
		Menu:prepareIngameMenu()
	end
	MakeWindowAppear( Menu, { CONFIG.scr_width/2, -CONFIG.scr_height }, { CONFIG.scr_width/2, CONFIG.scr_height/2 } )
end

function Loader.hideMenu()
	Menu.mn = false
	pop_pause()
	MakeWindowDisappear( Menu.pages[3], { CONFIG.scr_width/2, 2*CONFIG.scr_height }, true )
end

function Loader.keyProcessor( key )
	if key == keys["tilde"] and (not Console.shown) and (not GetFocusLock()) then
		Console.toggle()
	elseif Loader.state == 'game' and IsConfKey(key, config_keys.gui_nav_menu) and ( not Menu.locked or not Menu.locked[#Menu.locked] ) and not Menu.pages[3]:is_locked() then
		Menu.mn = not Menu.mn
		if Menu.mn then
			Loader.showMenu()
		else
			Loader.hideMenu()
		end
	end
	local ret = false
	for tkey, value in pairs(Loader.slaveKeyProcessors) do
		ret = ret or tkey( key )
	end
	if Loader.bind[key] then Loader.bind[key]() end
	return ret
end

function Loader.keyReleaseProcessor(key)
	for tkey, value in pairs(Loader.slaveKeyReleaseProcessors) do
		tkey( key )
	end
end

function Loader.ingame()
	local t = GetCurTime()
	if not ( GamePaused() ) then
		Loader.dt = t - Loader.old_time
		Loader.time = Loader.time + Loader.dt
	end
	Loader.old_time = t
	
	if not Editor or not Editor.active then
		-- local player = GetPlayer()
		-- Loader.level.MapScript(player)
		Loader.level.MapScript()
		if Game.Heartbeat then Game.Heartbeat() end
	end
	
	if dbg and dbg.process then dbg.process() end
end

function Loader.showGameOver()
	if not Menu.goshown then
		Menu.goshown = true
		Menu.showGameOver()
		Menu:show(7)
	end
end

function Loader.restoreCamera()
	SetCamUseBounds( false )
	SetCamScale( 1, 1 )
	SetCamAngle( 0 )
end

return Loader
