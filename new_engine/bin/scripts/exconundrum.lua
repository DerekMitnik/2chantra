function Game.start()
	--PlayBackMusic("music/iie_whbt.it")
	Game.CreateField(5)
end

function Game.widgetupdate(widget,state)
	if state == 0 then
		WidgetSetSprite(widget, "../characters/sohchan", "idle")
	elseif state == 1 then
		WidgetSetSprite(widget, "../characters/unylchan", "idle")
	else
		WidgetSetSprite(widget, "ex-widget", "idle")
	end
end

function Game.sohwins()
	Resume(NewThread(
		function() 
		local n = Game.field.n
		local x = Game.field.x + Game.field.width * (n/2-1/2) / n
		local y = Game.field.y + Game.field.height * (n/2-1/2) / n
		local w = Game.field.width / n
		local h = Game.field.height / n
		  
		local newwidget = CreateWidget(constants.wt_Button, "element", nil, x, y, w, h )
		WidgetSetSprite(newwidget, "../characters/sohchan", "walk")
		Game.DestroyField()
		Wait(3000);
		DestroyWidget(newwidget); 
		Game.start()
	end,
	true))
end

function Game.unylwins()
	Resume(NewThread(
		function() 
		local n = Game.field.n
		local x = Game.field.x + Game.field.width * (n/2-1/2) / n
		local y = Game.field.y + Game.field.height * (n/2-1/2) / n
		local w = Game.field.width / n
		local h = Game.field.height / n
		  
		local newwidget = CreateWidget(constants.wt_Button, "element", nil, x, y, w, h )
		WidgetSetSprite(newwidget, "../characters/unylchan", "walk")
		Game.DestroyField()
		Wait(3000);
		DestroyWidget(newwidget);
		Game.start()
	end,
	true))
end

function widgetLMouseProc(sender)
	if not Game.widgets[sender] then
		Log("widgetLMouseProc ERROR")
	end
	local i = Game.widgets[sender][1]
	local j = Game.widgets[sender][2]
	local state = Game.field[i][j].state
	local n = Game.field.n
	
	--Log(i, " ", j, " ", state)
	
	if state == 1 or state == 2 then
		for k = 1, n do
			if k ~= i then
				Game.field[k][j].state = Game.field[k][j].state + 1
				if Game.field[k][j].state > 2 then Game.field[k][j].state = 0 end
				
				Game.widgetupdate(Game.field[k][j].widget, Game.field[k][j].state)
			end
		end
		for k = 1, n do
			if k ~= j then
				Game.field[i][k].state = Game.field[i][k].state + 1
				if Game.field[i][k].state > 2 then Game.field[i][k].state = 0 end
				
				Game.widgetupdate(Game.field[i][k].widget, Game.field[i][k].state)
			end
		end
		Game.field[i][j].state = Game.field[i][j].state + 1
		if Game.field[i][j].state > 2 then Game.field[i][j].state = 0 end
		
		Game.widgetupdate(Game.field[i][j].widget, Game.field[i][j].state)
	end
	
	local unyl = true;
	local soh = true;
	for i = 1, n do
		for j = 1, n do
			if Game.field[i][j].state ~= 0 then soh = false end
			if Game.field[i][j].state ~= 1 then unyl = false end
		end
	end
	
	if soh == true then Game.sohwins() end
	if unyl == true then Game.unylwins() end
end

function Game.CreateField(n)
	if not Game.field then Game.field = {} end
	if not Game.widgets then Game.widgets = {} end
	Game.field.n = n
	Game.field.height = CONFIG.scr_height - 100
	Game.field.width = Game.field.height
	Game.field.x = (CONFIG.scr_width - Game.field.width)/2
	Game.field.y = 50
	
	
	for i = 1, n do
		if not Game.field[i] then Game.field[i] = {} end
		for j = 1, n do
			if not Game.field[i][j] then Game.field[i][j] = {} end
			local x = Game.field.x + Game.field.width * (j-1) / n
			local y = Game.field.y + Game.field.height * (i-1) / n
			local w = Game.field.width / n
			local h = Game.field.height / n
			local newwidget = CreateWidget(constants.wt_Button, "element", nil, x, y, w, h )
			Game.field[i][j].widget = newwidget
			WidgetSetLMouseClickProc(newwidget, widgetLMouseProc)
			Game.widgets[newwidget] = {i,j}
			Game.field[i][j].state = 2;
			--WidgetSetSprite(newwidget, "ex-widget", "state2")
		end
	end
	
	if n == 5 then
		Game.field[3][3].state = 0
		--WidgetSetAnim(Game.field[3][3].widget,"state1")
	end
      
	for i = 1, n do
		for j = 1, n do
			Game.widgetupdate(Game.field[i][j].widget, Game.field[i][j].state)
		end
	end

end

function Game.DestroyField()
	local n = Game.field.n
	for i = 1, n do
		for j = 1, n do
			Game.widgets[Game.field[i][j].widget] = {}
			DestroyWidget(Game.field[i][j].widget)
			Game.field[i][j] = {}
		end
		Game.field[i] = {}
	end
	Game.field = {}
end
