local Playlist = {
	NORMAL = 0,
	SHUFFLE = 1,
	RANDOM = 2,
	SINGLE = 3,
	RADIO = 4,
}

function Playlist:stop_thread()
	if (self.next_thread) then
		StopThread( self.next_thread )
	end
	self.next_thread = nil
	if not self.order then
		return
	end
	if self.tracks[ self.order[ self.current ] ] then
		StopSnd( self.tracks[ self.order[ self.current ] ] )
	end
end

function Playlist:reset()
	self:stop_thread()
	self.tracks = {}
	self.volume = {}
	self.order = {}
	self.current = 1
	self.repeat_list = true
	self.mid_pause = 200
	self.mode = self.NORMAL
	self.stopped = true
end

Playlist:reset()

function Playlist:replaceMusic()
	self._PlayBackMusic = PlayBackMusic
	self._StopBackMusic = StopBackMusic
	PlayBackMusic = function( track )
		self:add( track )
		self:play()
	end
	StopBackMusic = function()
		self:stop()
	end
end

function Playlist:reshuffle( keep )
	local old_order = self.order
	self.order = {}
	for i = 1, keep do
		table.insert( self.order, old_order[i] )
	end
	local spare_numbers = {}
	for i=keep+1, #old_order do
		table.insert( spare_numbers, i )
	end
	while #spare_numbers > 1 do
		table.insert( self.order, table.remove( spare_numbers, math.random(1, #spare_numbers) ) )
	end
	table.insert( self.order, spare_numbers[1] )
end

function Playlist:add( track, volume, duration )
	local radio = string.match( track, "https?://" )
	if (radio ~= nil) ~= (self.mode == self.RADIO) then
		return
	end
	if volume ~= nil then
		table.insert( self.volume, volume )
	else
		table.insert( self.volume, 1 )
	end
	table.insert( self.tracks, { track, volume, duration } )
	table.insert( self.order, #self.tracks )
	if self.mode == self.RANDOM then
		self:reshuffle( 0 )
	elseif self.mode == self.SHUFFLE then
		self:reshuflle( self.current )
	end
end

function Playlist:play()
	if not self.order[self.current] then
		if not self.order[1] then
			return
		else
			self.current = 1
		end
	end
	self.stopped = false
	local snd_len = 0
	if constants.__SOUND_LIBRARY == "BASS" then
		PlaySnd( self.tracks[ self.order[ self.current ] ][1], true, self.volume[ self.order[ self.current ] ])
		snd_len = GetSndLength( self.tracks[ self.order[ self.current ] ] ) * 1000
	else
		PlayBackMusic( self.tracks[ self.order[ self.current ] ][1], self.volume[ self.order[ self.current ] ], false )
		snd_len = self.tracks[ self.order[ self.current ] ][3]
	end
	if self.mode ~= self.RADIO then
		self.next_thread = NewThread( function()
			Wait( snd_len )
			if constants.__SOUND_LIBRARY == "BASS" then
				StopSnd( self.tracks[ self.order[ self.current ] ] )
			end
			Wait( self.mid_pause )
			self:next()
		end )
		Resume( self.next_thread )
	end
end

function Playlist:stop()
	self.stopped = true
	self:stop_thread()
end

function Playlist:next( auto )
	if self.mode == self.NORMAL or self.mode == self.SHUFFLE then
		self.current = self.current + 1
	elseif self.mode == self.RANDOM and #self.tracks > 1 then
		self.current = math.random( 1, #self.tracks )
	elseif self.mode == self.TRACK and not auto then
		self.current = self.current + 1
	end
	if not self.tracks[self.order[ self.current ]] then
		self.current = 1
		if self.mode == self.SHUFFLE or self.mode == self.RANDOM then
			self:reshuffle( 0 )
		end
		if not self.repeat_list then
			self.stopped = true
		end
	end
	if not self.stopped then
		self:play()
	end
end

function Playlist:empty()
	self:stop()
	self.tracks = {}
	self.volume = {}
	self.order = {}
	self.current = 1
end

function Playlist:setMode( mode )
	self.mode = mode
	if self.mode == self.SHUFFLE or self.RANDOM then
		self:reshuffle( 0 )
	end
end

function Playlist:setRepeat( repeat_track )
	self.repeat_track = repeat_track
end

return Playlist
