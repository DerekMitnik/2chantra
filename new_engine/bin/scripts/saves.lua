require("serialize")

local saver_script = build_relative_path( constants.path_scripts.."saves-binary.lua" )
-- local saver_script = "scripts/saves-plain.lua"

local Saver = {}
Saver.__index = Saver

-- �������� �����, ������� ����� ��������� �� stat.
-- local statFields = {"lives", "score", "weapon", "bonuses", "active_char", "passive_char", "kills", "wakabas"}

local function getFileName(save_name)
	local profile_name = Game.profile.name or ""
	return (constants.path_saves.."save_"..remove_cyrillic(profile_name).."_"..(save_name or "1")..".sav")
end

local function getProfileName(profile_name)
	local lprofile_name = remove_cyrillic(profile_name or Game.profile.name or "")
	return (constants.path_saves.."profile_"..lprofile_name..".sav")
end

function Saver:doesExist(save_name)
	local file = io.open(getFileName(save_name))
	if file then io.close(file) end
	return file ~= nil
end

function Saver:getSlotName(save_name)
	savedata = { filename = getFileName(save_name), mode = "i" }
	local savedata = dofile( saver_script )
	--local savedata = {}
	if not savedata then savedata = {} end
	local ret = {}
	table.insert( ret, save_name )
	table.insert( ret, " - " )
	table.insert( ret, dictionary_string(savedata.mapname or "") or "UNKNOWN MAP" )
	table.insert( ret, " - " )
	table.insert( ret, savedata.date or "UNKNOWN DATE" )
	return table.concat( ret )
end

function Saver:saveGame(save_name, info)
	Saver:saveProfile()
	savedata = { filename = getFileName(save_name), mode = "w" }
	saveinfo = info or {}
	dofile( saver_script )
end

function Saver:loadGame(save_name)
	savedata = { filename = getFileName(save_name), mode = "r" }
	dofile( saver_script )
end

function Saver:saveProfile()
	Game.SaveLastProfile()
	savedata = { filename = getProfileName(), mode = "pw" }
	dofile( saver_script )
end

function Saver:loadProfile( name )
	savedata = { filename = getProfileName( name ), mode = "pr" }
	dofile( saver_script )
	LoadProfileConfig()
end

function Saver:getProfilesList()
	local dir, dirs = ListDirContents( constants.path_saves, true )
	if (not dir) or (not dirs) then
		return {}
	end
	if #dir <= dirs then
		return {}
	end
	local ret = {}
	local name
	for i = dirs+1, #dir do
		name = string.match( dir[i], "profile_(%S+)\.sav" )
		if name then
			savedata = { filename = constants.path_saves..dir[i], mode = "pn" }
			dofile( saver_script )
			if savedata.name then
				table.insert( ret, savedata.name )
			end
		end
	end
	table.sort( ret )
	return ret
end

return Saver
