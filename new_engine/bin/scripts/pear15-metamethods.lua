local Game = Game

local function get_value( ud, name )
	return Game.GetObjectValue( ud, name )
end

local function set_value( ud, name, value )
	if (not name) then
		return nil
	end
	return Game.SetObjectValue( ud, name, value )
end

local function get_string( ud, name )
	local ret = Game.GetObjectValue( ud, name )
	if (not ret) or (type(ret) ~= 'string') then
		return ""
	end
	return ret
end

local function set_string( ud, name, value )
	local ret = tostring(value) or ""
	Game.SetObjectValue( ud, name, ret )
	return ret
end

local function get_number( ud, name )
	local ret = Game.GetObjectValue( ud, name )
	if (not ret) or (type(ret) ~= 'number') then
		return 0
	end
	return ret
end

local function set_number( ud, name, value )
	local ret = tonumber(value) or 0
	Game.SetObjectValue( ud, name, ret )
	return ret
end

local function get_vector( ud, name )
	local ret = Game.GetObjectValue( ud, name )
	if (not ret) or (type(ret) ~= 'table') or (getmetatable(ret) ~= Vector2) then
		return Vector2()
	end
	return ret
end

local function set_vector( ud, name, value )
	if (getmetatable(value) ~= Vector2) then
		return value
	end
	Game.SetObjectValue( ud, name, value )
	return value
end

local function get_color( ud, name )
	local ret = Game.GetObjectValue( ud, name )
	if (not ret) or (type(ret) ~= 'table') or (getmetatable(ret) ~= Color) then
		return Color()
	end
	return ret
end

local function set_color( ud, name, value )
	if (getmetatable(value) ~= Vector2) then
		return value
	end
	Game.SetObjectValue( ud, name, value )
	return value
end

local function get_script( ud, name )
	local ret = Game.GetObjectValue( ud, name )
	if (not ret) or (type(ret) ~= 'table') or (getmetatable(ret) ~= Script) then
		return Script("")
	end
	return ret
end

local function set_script( ud, name, value )
	if (getmetatable(value) ~= Script) then
		return value
	end
	Game.SetObjectValue( ud, name, value )
	return value
end

local function obj_string( ud, name, value )
	if value then
		return set_string( ud, name, value )
	else
		return get_string( ud, name )
	end
end

local function obj_number( ud, name, value )
	if value then
		return set_number( ud, name, value )
	else
		return get_number( ud, name )
	end
end

local function obj_vector( ud, name, value )
	if value then
		return set_vector( ud, name, value )
	else
		return get_vector( ud, name )
	end
end

local function obj_color( ud, name, value )
	if value then
		return set_color( ud, name, value )
	else
		return get_color( ud, name )
	end
end

local function obj_script( ud, name, value )
	if value then
		return set_script( ud, name, value )
	else
		return get_script( ud, name )
	end
end

RegisterCustomObjectFunction( "get", get_value )
RegisterCustomObjectFunction( "set", set_value )
RegisterCustomObjectFunction( "get_string", get_string )
RegisterCustomObjectFunction( "set_string", set_string )
RegisterCustomObjectFunction( "string", obj_string )
RegisterCustomObjectFunction( "get_number", get_number )
RegisterCustomObjectFunction( "set_number", set_number )
RegisterCustomObjectFunction( "number", obj_string )
RegisterCustomObjectFunction( "get_vector", get_vector )
RegisterCustomObjectFunction( "set_vector", set_vector )
RegisterCustomObjectFunction( "vector", obj_vector )
RegisterCustomObjectFunction( "get_color", get_color )
RegisterCustomObjectFunction( "set_color", set_color )
RegisterCustomObjectFunction( "color", obj_string )
RegisterCustomObjectFunction( "get_script", get_script )
RegisterCustomObjectFunction( "set_script", set_script )
RegisterCustomObjectFunction( "script", obj_script )
