Console = {}
Console.contents = {}
Console.command_history = {}
Console.history = -1

function EditMap( mapname )
	Editor.open( mapname )
end

function Console.addMessage( message, on_screen, sound )
	table.insert( Console.contents, message )
end

local safe_console_commands = {}
function Console.addSafeConsoleCommand( command, help, func )
	safe_console_commands[command] = { func, help = help }
end

Console.addSafeConsoleCommand( "help", "Help, help, help!", function( with_what )
	local with_what = string.match( with_what, "%S+" )
	if not with_what or with_what == "" then
		local ret = {}
		for k, v in pairs(safe_console_commands) do
			table.insert( ret, k )
		end
		table.sort( ret )
		return table.concat( ret, ", " )
	end
	if safe_console_commands[ with_what ] then
		return safe_console_commands[ with_what ].help or "No help provided."
	end
	return string.format("Unknown command: '%s'", with_what)
end )
Console.addSafeConsoleCommand( "debug", "Puts console in debug mode, able to execute any lua code. This disqualifies your score from entering highscores table.",
	function()
		Console.makeUnsafe()
		return "Your console is now in debug mode. This disqualifies your score from entering highscores table."
	end )
Console.addSafeConsoleCommand( "kill", "Kills your character. Usage: kill [player number].", function( num )
		local num = string.match( num, "%d+" )
		if num then
			num = tonumber(num)
		end
		local char = GetPlayerCharacter(num or 1)
		if char then char:health(0) end
	end )

local function safe_console( code )
	local com = string.match( code, "^%s*(%S+)" )
	if safe_console_commands[ com ] then
		local result = safe_console_commands[ com ][1](string.gsub(code, "^%s*"..com, ""))
		if result then
			Console.addMessage( " "..result )
		end
	else
		Console.addMessage( " Command unrecognised. Use 'help' to see full list of commands." )
	end
	WidgetSetCaption( Console.console_text, "" )
end

local function unsafe_console( code )
	local success, result = loadstring(code)
	if not success then
		Console.addMessage( "  "..tostring( result ))
		Log("CONSOLE: ",tostring( result ))
		WidgetSetCaption( Console.console_text, "" )
	else
		success, result = pcall( success )
		if (not success) or (result) then
			Console.addMessage( "  "..tostring( result ) )
			if not success then 
				Log("CONSOLE: ", tostring( result ))
			end					
		end
		WidgetSetCaption( Console.console_text, "" )
	end
end

function Console.makeUnsafe()
	Console.codeHandler = unsafe_console
	LockScore()
end

Console.codeHandler = safe_console

function Console.keyProcessor( key )
	if key == keys["enter"] then
		local code = WidgetGetCaption( Console.console_text )
		code = code or " "
		Console.addMessage( code )
		if CONFIG.debug == 1 then
			Log( "CONSOLE: '",code,"'" )
		end
		table.insert(Console.command_history, code)
		if #Console.command_history > 10 then
			table.remove( Console.command_history, 1 )
		end
		Console.history = -1
		if not console_chat then
			Console.codeHandler( code )
		else
			Message(code)
		end
		Console.showLog()
	elseif key == keys["up"] then
		if Console.history == -1 then
			Console.history = #Console.command_history
		else
			Console.history = Console.history - 1
		end
		if Console.history == 0 then Console.history = 1 end
		WidgetSetCaption( Console.console_text, (Console.command_history[Console.history or 0] or "") )
		--WidgetGainFocus( Console.console_text )
	elseif key == keys["down"] then
		if Console.history == -1 then
			return true
		else
			Console.history = Console.history + 1
		end
		if Console.history == #Console.command_history then Console.history = -1 end
		WidgetSetCaption( Console.console_text, (Console.command_history[Console.history or 0] or "") )
		--WidgetGainFocus( Console.console_text )
	elseif key == keys["tilde"] then
		if not Console.just_appeared == true then
			Console.toggle()
		else
			Console.just_appeared = false
		end
		return true
	end
	Console.just_appeared = false
	WidgetGainFocus( 0 )
	WidgetGainFocus( Console.console_text )
	WidgetGainFocus( Console.console_text )
end

function Console.showLog()
	local y = 20 * Console.logStrings
	for i=1,math.min( #Console.contents, Console.logStrings ) do
		local sx, sy = GetMultilineCaptionSize( "dialogue", Console.contents[ #Console.contents-i+1 ], CONFIG.scr_width/2 - 10, 1 )
		y = y - sy
		WidgetSetPos( Console.logWidgets[ Console.logStrings - i + 1 ], 5, y )
		WidgetSetCaption( Console.logWidgets[ Console.logStrings - i + 1 ], Console.contents[ #Console.contents-i+1 ], true )
	end
	for i=Console.logStrings, #Console.contents do
		table.remove( Console.contents, #Console.contents )
	end
end

function Console.toggle()
	Console.shown = not ( Console.shown or false )
	if Console.shown then
		push_pause( true )
		Console.just_appeared = true
		if not Console.console_back then
			Console.console_back = CreateWidget(constants.wt_Widget, "console_bkg", nil, -1, -1, CONFIG.scr_width+2, CONFIG.scr_height/2)
			WidgetSetZ(Console.console_back, 1.08)
			WidgetSetSpriteBlendingMode(Console.console_back, constants.bmSrcA_OneMinusSrcA )
			WidgetSetColorBox(Console.console_back, {0, 0, 0, 0.9} )
			Console.logStrings = math.floor( (CONFIG.scr_height/2 - 20) / 20 )
			Console.logWidgets = {}
			for i=1,Console.logStrings do
				local widgie = CreateWidget(constants.wt_Label, "console_text", nil, 5, 20*(i-1), CONFIG.scr_width - 10, CONFIG.scr_height/2 )
				WidgetSetCaptionFont( widgie, "dialogue" )
				WidgetSetZ(widgie, 1.09)
				table.insert( Console.logWidgets, widgie )
			end
			Console.console_text = CreateWidget(constants.wt_Textfield, "console_text", nil, 5, CONFIG.scr_height/2 - 20, 25500, CONFIG.scr_height/2 )
			WidgetSetCaptionColor( Console.console_text, { 1, 1, 1, 1}, true )
			WidgetSetCaptionFont( Console.console_text, "dialogue" )
			WidgetSetZ(Console.console_text, 1.09)
		else
			Console.console_text = CreateWidget(constants.wt_Textfield, "console_text", nil, 5, CONFIG.scr_height/2 - 20, 25500, CONFIG.scr_height/2 )
			WidgetSetCaptionColor( Console.console_text, { 1, 1, 1, 1}, true )
			WidgetSetCaptionFont( Console.console_text, "dialogue" )
			WidgetSetZ(Console.console_text, 1.09)

			WidgetGainFocus( Console.console_text )
			--WidgetSetCaption( Console.console_text, "" )
		end
		for i=1,Console.logStrings do
			WidgetSetVisible( Console.logWidgets[i], true )
		end
		Console.showLog()
		WidgetSetVisible(Console.console_back, true)
		WidgetSetVisible(Console.console_text, true)
		for i=1,Console.logStrings do
			WidgetSetVisible( Console.logWidgets[i], true )
		end
		WidgetGainFocus(0)
		WidgetGainFocus(Console.console_text)
		WidgetGainFocus(Console.console_text)
		Console.keyProcessorId = Loader.addSlaveKeyProcessor(Console.keyProcessor)
		EnablePlayerControl( false )
	else
		pop_pause()
		WidgetSetVisible( Console.console_back, false )
		--WidgetSetVisible( Console.console_text, false )
		DestroyWidget( Console.console_text )
		for i=1,Console.logStrings do
			WidgetSetVisible( Console.logWidgets[i], false )
		end
		Loader.removeSlaveKeyProcessor(Console.keyProcessorId)
		Console.keyProcessorId = nil
		EnablePlayerControl( true )
	end
end

return Console
