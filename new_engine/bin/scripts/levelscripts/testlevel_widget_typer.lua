InitNewGame();


local LEVEL = {}
require("routines")

local label = CreateWidget(constants.wt_Label, "Label1", nil, 20, 60,400, 10)
WidgetSetBorder(label, false)
WidgetSetCaptionColor(label, {1,1,1,1}, false)

local texts = { [[L/n/norem/n /ca0a0a0ipsum /ca000a0dolor /ca0a0f0sit amet,/n consectetur adipiscing elit./n /cffffffQuisque ac dolor risus. Sed sit amet magna ac lectus tempor molestie in a massa. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque tortor arcu, varius ut pretium sollicitudin, mattis in ligula. Suspendisse potenti. Nam non velit nunc. Duis ac est id lorem accumsan mollis a non dolor. ]],
[[ ���  �����������  ��  ����������  �������������,  �����  �  ����������� ��������������   ����,  �������   ���������  ���������   �����-��   �������� �����������. ��� � ������ -- ������ �� ���� ���-�� �������, ������ �� ������ � ��������. ��, ���...
]]
}
local period = 10

WidgetSetCaption(label, texts[1], true)
function restart_typer()
	WidgetUseTyper(label, false)
	WidgetUseTyper(label, true, period, true)
	WidgetStartTyper(label);
end

restart_typer();

function start_typer()
	WidgetStartTyper(label);
end

function stop_typer()
	WidgetStopTyper(label);
end

local l_per = CreateWidget(constants.wt_Label, "period", nil, 500, 120, 100, 10)
WidgetSetCaptionColor(l_per, {1,1,1,1}, false)
WidgetSetCaption(l_per, tostring(period))

function faster()
	if period >= 0 then
		period = period - 10
		WidgetUseTyper(label, true, period, true)
		WidgetSetCaption(l_per, tostring(period))
	end
end

function slower()
	period = period + 10
	WidgetUseTyper(label, true, period, true)
	WidgetSetCaption(l_per, tostring(period))
end

function text_selector()
	local num = 1
	return function ()
		local count = #texts
		num = num+1
		if num > count then num = 1 end
		WidgetSetCaption(label, texts[num], true)
	end
end

local next_text = text_selector()

function on_typer_ended()
	AddTimerEvent(500, function () next_text(); start_typer(); end)
end
WidgetSetOnTyperEndedProc(label, on_typer_ended)

local button = CreateWidget(constants.wt_Button, "restart", nil, 500, 50, 100, 10)
WidgetSetBorder(button, true)
WidgetSetCaption(button, "Restart")
WidgetSetLMouseClickProc(button,restart_typer)

local button = CreateWidget(constants.wt_Button, "start", nil, 500, 60, 100, 10)
WidgetSetBorder(button, true)
WidgetSetCaption(button, "Start")
WidgetSetLMouseClickProc(button, start_typer)

button = CreateWidget(constants.wt_Button, "stop", nil, 500, 70, 100, 10)
WidgetSetBorder(button, true)
WidgetSetCaption(button, "Stop")
WidgetSetLMouseClickProc(button, stop_typer)

button = CreateWidget(constants.wt_Button, "faster", nil, 500, 80, 100, 10)
WidgetSetBorder(button, true)
WidgetSetCaption(button, "Faster")
WidgetSetLMouseClickProc(button, faster)

button = CreateWidget(constants.wt_Button, "slower", nil, 500, 90, 100, 10)
WidgetSetBorder(button, true)
WidgetSetCaption(button, "Slower")
WidgetSetLMouseClickProc(button, slower)


button = CreateWidget(constants.wt_Button, "selector", nil, 500, 100, 100, 10)
WidgetSetBorder(button, true)
WidgetSetCaption(button, "Next text")
WidgetSetLMouseClickProc(button, next_text)

----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------


function LEVEL.MapScript(player)
end

function LEVEL.WeaponBonus()
end

function LEVEL.placeSecrets()
end

function LEVEL.removeSecrets()
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter = l
	
	--Log("Set loader ", l == nil and "nil" or "l")
end

return LEVEL