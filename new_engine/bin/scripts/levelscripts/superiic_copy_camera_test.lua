--EDITABLE MAP

local LEVEL = {}
InitNewGame()
dofile("levels/superiic_copy.lua")

CONFIG.backcolor_r = 0.090000
CONFIG.backcolor_g = 0.117000
CONFIG.backcolor_b = 0.035000
LoadConfig()

PlayBackMusic("music/iie_escape.it")

local scale = 1

function LEVEL.keyRelProc(key)
	if key == keys["9"] then
		if scale > 1 then
			scale = scale - 1
		else
			scale = scale / 2
		end
		SetCamScale(scale, scale)
	elseif key == keys["0"] then
		if scale > 1 then
			scale = scale + 1
		else
			scale = scale * 2
		end
		SetCamScale(scale, scale)
	end
	
end

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	if GetPlayer() then
		SetCamAttachedObj( GetPlayer().id )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
	end
	Menu.locked = true
	local env = CreateEnvironment("default_environment", -9001, -9001)
	SetDefaultEnvironment(env)
	assert(nil)  -- deprecated slaveKeyProcessors - too lazy to fix this
	table.insert(Loader.slaveKeyProcessors, Loader.level.keyRelProc)
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
	--Log(serialize("keys", keys))
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local obj1 = CreateItem( 'generic-trigger', 165, 658 )
	local obj2 = CreateItem( 'generic-trigger', 292, 974 )
	GroupObjects( obj1, obj2 )
	local obj3 = CreateItem( 'generic-trigger', 2147, 848 )
	local obj4 = CreateItem( 'generic-trigger', 2214, 955 )
	GroupObjects( obj3, obj4 )
	local object
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	if trigger == 1 then
		SetObjDead( id )
		Resume( NewMapThread( function()
			for i = -15,2000,100 do
				for j=1, 5 do
					CreateEnemy( "big_explosion", i+math.random(-20,20), math.random( 630, 1277 ) )
				end
				Wait(500)
				Wait(1)
			end
		end ))
	elseif trigger == 2 then
		SetObjDead( id )
		Menu.showEndLevelMenu("superiic")
	end
--$(MAP_TRIGGER)-
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'superiic_copy_camera_test'

return LEVEL
