InitNewGame();
--TogglePause()

local LEVEL = {}
require("routines")
--TogglePause()

CONFIG.backcolor_r = 67/255;
CONFIG.backcolor_g = 120/255;
CONFIG.backcolor_b = 153/255;
LoadConfig();

local x = -48
local dx = 32
for i = 0,500 do
	CreateSprite("phys_floor", x + dx*i, 350);
end

CreatePlayer("sohchan", 50, 300);
CreatePlayer("unylchan", 50, 300);

--CreateItem("bonus-line", 200, 300)
--CreateItem("bonus-diamond", 230, 300)
--CreateItem("ammo", 260, 300)
--CreateItem("ammo", 290, 300)

CreateEnemy("btard", 290, 300)
CreateEnemy("btard_ally", 390, 300)

CreateSprite("release_sign", 500, 275)

function map_trigger( id, trigger )
	if ( trigger == 1 ) then
		Menu.showEndLevelMenu("testlevel_chain3")
	end
end

trigger = CreateItem("generic-trigger", 500, 200)
GroupObjects( trigger, CreateItem("generic-trigger", 700, 350) )
ObjectPushInt( trigger, 1 )

local env = CreateEnvironment("default_environment", -9001, -9001)
SetDefaultEnvironment(env)

SetCamAttachedObj(GetPlayer().id);		-- ������� ������ � �������
SetCamAttachedAxis(true, true);		-- ������ ��������� ������ �� ��� X
SetCamFocusOnObjPos(constants.CamFocusBottomCenter);	-- ������ ������ �� ��������� ������� ���� �������
-- ��� ����� �������� y-���������� ������� ���� �� ��������, ����� �������, ������ �� ������ ���, ��������, ����������
SetCamObjOffset(0, 60);	-- ������ �������� ������ ������������ �������

function btards()
	while true do
		Wait(5000)
		for i=1,math.random(1,3) do
			CreateEnemy("btard", 290+math.random(-100, 100), 300)
		end
		for i=1,math.random(1,3) do
			CreateEnemy("btard_ally", 390+math.random(-100, 100), 300)
		end
	end
end

Resume( NewMapThread(btards) )

----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------


function LEVEL.MapScript(player)
end

function LEVEL.WeaponBonus()
end

function LEVEL.placeSecrets()
end

function LEVEL.removeSecrets()
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter = l

	--TogglePause	()
	--Log("Set loader ", l == nil and "nil" or "l")
end

LEVEL.name = "chain2"

return LEVEL