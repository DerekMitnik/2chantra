InitNewGame();

mapvar = {}
LoadSound("boss-hit.ogg");
LoadTexture("heli.png");
LoadTexture("soh-chan1024.png");
LoadTexture("bullets.png");
LoadTexture("turret_snow.png");
LoadTexture("turret.png");
LoadTexture("bullets-sfg5000.png");
LoadTexture("flamer.png");
LoadTexture("grenade.png");
LoadTexture("grenade-explosion.png");
LoadTexture("flash-straight.png");
LoadTexture("flash-angle.png");
LoadTexture("flash-straight-sfg5000.png");
LoadTexture("flash-angle-sfg5000.png");
LoadTexture("invincibility-bubble.png");
LoadTexture("slowpoke-snowball.png");
LoadPrototype("invincibility-bubble.lua");
LoadTexture("pspark.png");
LoadPrototype("pspark.lua");
LoadPrototype("flamer.lua");
LoadPrototype("grenade.lua");
LoadPrototype("explosion.lua");
LoadPrototype("explosion-safe.lua");
LoadPrototype("sohchan.lua");
LoadPrototype("sfg9000.lua");
LoadPrototype("spread.lua");
LoadPrototype("spread-bullet.lua");
LoadPrototype("rapid.lua");
LoadPrototype("rapid-bullet.lua");
LoadPrototype("slowpoke-snowball.lua");
LoadPrototype("sfg5000.lua");
LoadPrototype("flash-straight.lua");
LoadPrototype("flash-straight-spread.lua");
LoadPrototype("flash-angle-up.lua");
LoadPrototype("flash-angle-down.lua");
LoadPrototype("flash-angle-up-spread.lua");
LoadPrototype("flash-angle-down-spread.lua")
LoadPrototype("flash-straight-sfg5000.lua");
LoadPrototype("flash-angle-up-sfg5000.lua");
LoadPrototype("flash-angle-down-sfg5000.lua");
LoadTexture("rocket.png");
LoadTexture("rocket-explosion.png");
LoadPrototype("rocketlauncher.lua");
LoadPrototype("explosion-rocket.lua");
LoadPrototype("turret-snow.lua");
LoadPrototype("turret.lua");
LoadPrototype("turret-left.lua");
LoadPrototype("robot-spawner-right.lua");
LoadPrototype("heli-cutscene.lua");
LoadPrototype("heli-cutscene2.lua");
LoadPrototype("thief.lua");

LoadTexture("dust-run.png");
LoadPrototype("dust-run.lua");
LoadTexture("dust-land.png");
LoadPrototype("dust-land.lua");
LoadTexture("dust-stop.png");
LoadPrototype("dust-stop.lua");

LoadTexture("btard.png");
LoadTexture("btard-ny.png");
LoadTexture("robot.png");
LoadPrototype("robot.lua");
LoadPrototype("btard.lua");
LoadPrototype("btard-ny.lua");
LoadPrototype("btard-spawner.lua");
LoadPrototype("btard-spawner-left.lua");
LoadPrototype("btard-spawner-right.lua");
LoadPrototype("penguin_omsk-spawner-right.lua");
LoadPrototype("penguin_omsk-spawner-down.lua");
LoadTexture("penguin_omsk.png");
LoadPrototype("penguin_omsk.lua");
LoadPrototype("btard-punch.lua");
LoadPrototype("generic-death-area.lua");
LoadPrototype("generic-snow-area.lua");

LoadPrototype("phys-empty.lua");
LoadPrototype("phys-empty-cl.lua");
LoadPrototype("phys-empty-end.lua");
LoadPrototype("phys-empty-cl-end.lua");

LoadTexture("unyl_sprite.png");
LoadPrototype("unylchan.lua");

LoadTexture("wakabamark.png");
LoadPrototype("wakabamark.lua");
LoadTexture("weapons.png");
LoadTexture("bonus-life.png");
LoadPrototype("bonus-grenades.lua");
LoadPrototype("bonus-spread.lua");
LoadPrototype("bonus-life.lua");
LoadPrototype("bonus-rockets.lua");
LoadPrototype("bonus-machinegun.lua");
LoadPrototype("bonus-flamer.lua");
LoadTexture("vegetable1.png");
LoadPrototype("vegetable1.lua");
LoadTexture("vegetable3.png");
LoadPrototype("vegetable3.lua");
LoadTexture("ammo.png");
LoadPrototype("ammo.lua");

LoadTexture("pteleport.png");
LoadPrototype("pteleport.lua");
LoadTexture("pblood.png");
LoadPrototype("pblood.lua");
LoadPrototype("pblood-wound.lua");
LoadTexture("psnow.png");
LoadPrototype("psnow.lua");
LoadPrototype("psnow-part.lua");
LoadPrototype("psnowball.lua");

LoadTexture("snow1.png");
LoadTexture("snow2.png");
LoadTexture("snow3.png");
LoadTexture("snow4.png");
LoadTexture("snow5.png");
LoadTexture("snow6.png");
LoadTexture("snow7.png");
LoadTexture("snow8.png");
LoadTexture("snow9.png");
LoadTexture("snow10.png");
LoadTexture("snow11.png");
LoadTexture("snow12.png");
LoadTexture("snow13.png");
LoadTexture("snow14.png");
LoadTexture("snow15.png");
LoadTexture("snow16.png");
LoadTexture("snow17.png");
LoadTexture("snow18.png");
LoadTexture("snow19.png");
LoadTexture("snow20.png");
LoadTexture("snow21.png");
LoadTexture("snow22.png");
LoadTexture("snow23.png");
LoadTexture("snow24.png");
LoadTexture("snow25.png");
LoadTexture("snow26.png");
LoadTexture("snow27.png");
LoadTexture("black-block.png");
LoadTexture("vert-entrance.png");
LoadTexture("vert-exit.png");
LoadTexture("vert-corridor.png");
LoadTexture("vert-corridor-entrance.png");
LoadTexture("vert-corridor-exit.png");
LoadTexture("vert-inside-back.png");
LoadTexture("vert-inside-floor.png");
LoadTexture("vert-inside-wall.png");
LoadTexture("vert-inside-wall2.png");
LoadTexture("vert-inside-platform-left.png");
LoadTexture("vert-inside-platform-right.png");
LoadTexture("vert-inside-platform-small.png");
LoadTexture("vert-inside-platform-long.png");
LoadTexture("vert-inside-platform-long-left.png");
LoadTexture("vert-inside-platform-long-right.png");
LoadTexture("vert-wall-left.png");
LoadTexture("vert-wall-right.png");
LoadTexture("vert-details1.png");
LoadTexture("vert-details2.png");
LoadTexture("vert-details3.png");
LoadTexture("vert-details4.png");
LoadTexture("vert-details5.png");
LoadTexture("vert-details6.png");
LoadTexture("vert-details7.png");
LoadTexture("vert-details8.png");
LoadTexture("vert-details9.png");
LoadTexture("vert-details10.png");
LoadTexture("vert-details11.png");
LoadTexture("vert-details12.png");
LoadTexture("vert-details13.png");
LoadTexture("vert-details14.png");
LoadTexture("vert-pipes1.png"); 
LoadTexture("vert-pipes2.png"); 
LoadTexture("vert-pipes3.png"); 
LoadTexture("vert-pipes4.png"); 
LoadTexture("vert-pipes5.png"); 
LoadTexture("vert-pipes6.png"); 
LoadTexture("vert-pipes7.png"); 
LoadTexture("vert-pipes8.png"); 
LoadTexture("vert-pipes9.png"); 
LoadTexture("vert-pipes10.png"); 
LoadTexture("vert-pipes11.png"); 
LoadTexture("ibrj.png");
LoadTexture("vert-top.png");
LoadTexture("vert-top-left.png");
LoadTexture("vert-top-right.png");
LoadTexture("vert-top-inside-left.png");
LoadTexture("vert-top-inside-right.png");
LoadTexture("vert-top-back.png");
LoadTexture("vert-top-back-left.png");
LoadTexture("vert-top-back-right.png");
LoadTexture("vert-top-platform-left.png");
LoadTexture("vert-top-platform-right.png");
LoadTexture("fp.png");
LoadTexture("fp-left.png");
LoadTexture("fp-right.png");
LoadTexture("wall1.png");
LoadTexture("wall2.png");
LoadTexture("wall3.png");
LoadTexture("sign1.png");
LoadTexture("sign2.png");
LoadTexture("sign3.png");
LoadPrototype("sign3.lua");
LoadPrototype("snow1.lua");
LoadPrototype("snow2.lua");
LoadPrototype("snow4.lua");
LoadPrototype("snow5.lua");
LoadPrototype("snow6.lua");
LoadPrototype("snow7.lua");
LoadPrototype("snow8.lua");
LoadPrototype("snow9.lua");
LoadPrototype("snow10.lua");
LoadPrototype("snow11.lua");
LoadPrototype("snow12.lua");
LoadPrototype("snow13.lua");
LoadPrototype("snow14.lua");
LoadPrototype("snow15.lua");
LoadPrototype("snow16.lua");
LoadPrototype("snow17.lua");
LoadPrototype("snow18.lua");
LoadPrototype("snow19.lua");
LoadPrototype("snow20.lua");
LoadPrototype("snow21.lua");
LoadPrototype("snow22.lua");
LoadPrototype("snow23.lua");
LoadPrototype("snow24.lua");
LoadPrototype("snow25.lua");
LoadPrototype("snow26.lua");
LoadPrototype("snow27.lua");
LoadPrototype("snow28.lua");
LoadPrototype("vert-entrance.lua");
LoadPrototype("vert-exit.lua");
LoadPrototype("vert-corridor.lua");
LoadPrototype("vert-corridor-entrance.lua");
LoadPrototype("vert-corridor-exit.lua");
LoadPrototype("vert-inside-back.lua");
LoadPrototype("vert-inside-floor.lua");
LoadPrototype("vert-inside-floor-walkable.lua");
LoadPrototype("vert-inside-wall.lua");
LoadPrototype("vert-inside-wall2.lua");
LoadPrototype("vert-inside-platform-left.lua");
LoadPrototype("vert-inside-platform-right.lua");
LoadPrototype("vert-inside-platform-small.lua");
LoadPrototype("vert-inside-platform-long.lua");
LoadPrototype("vert-inside-platform-long-left.lua");
LoadPrototype("vert-inside-platform-long-right.lua");
LoadPrototype("vert-wall-left.lua");
LoadPrototype("vert-wall-right.lua");
LoadPrototype("vert-details1.lua");
LoadPrototype("vert-details2.lua");
LoadPrototype("vert-details3.lua");
LoadPrototype("vert-details4.lua");
LoadPrototype("vert-details5.lua");
LoadPrototype("vert-details6.lua");
LoadPrototype("vert-details7.lua");
LoadPrototype("vert-details8.lua");
LoadPrototype("vert-details9.lua");
LoadPrototype("vert-details10.lua");
LoadPrototype("vert-details11.lua");
LoadPrototype("vert-details12.lua");
LoadPrototype("vert-details13.lua");
LoadPrototype("vert-details14.lua");
LoadPrototype("vert-pipes1.lua"); 
LoadPrototype("vert-pipes2.lua"); 
LoadPrototype("vert-pipes3.lua"); 
LoadPrototype("vert-pipes4.lua"); 
LoadPrototype("vert-pipes5.lua"); 
LoadPrototype("vert-pipes6.lua"); 
LoadPrototype("vert-pipes7.lua"); 
LoadPrototype("vert-pipes8.lua"); 
LoadPrototype("vert-pipes9.lua"); 
LoadPrototype("vert-pipes10.lua"); 
LoadPrototype("vert-pipes11.lua"); 
LoadPrototype("ibrj.lua");
LoadPrototype("vert-top.lua");
LoadPrototype("vert-top-left.lua");
LoadPrototype("vert-top-right.lua");
LoadPrototype("vert-top-inside-left.lua");
LoadPrototype("vert-top-inside-right.lua");
LoadPrototype("vert-top-back.lua");
LoadPrototype("vert-top-back-left.lua");
LoadPrototype("vert-top-back-right.lua");
LoadPrototype("vert-top-platform-left.lua");
LoadPrototype("vert-top-platform-right.lua");
LoadPrototype("fp.lua");
LoadPrototype("fp-left.lua");
LoadPrototype("fp-right.lua");
LoadPrototype("wall1.lua");
LoadPrototype("wall2.lua");
LoadPrototype("wall2-forward.lua");
LoadPrototype("wall3.lua");
LoadPrototype("sign1.lua");
LoadPrototype("sign2.lua");

LoadSound("grenade-launch.ogg");
LoadSound("grenade-bounce.ogg");
LoadSound("item-score.ogg");
LoadSound("item-weapon.ogg");
LoadSound("grenade-explosion.ogg");
LoadSound("blaster_shot.ogg");
LoadSound("foot-left.ogg");
LoadSound("foot-right.ogg");
LoadSound("land.ogg");
LoadSound("stop.ogg");
LoadSound("ammo-pickup.ogg");
LoadSound("health-pickup.ogg");
LoadSound("flame.ogg");
LoadSound("music\\iie_street.it");
LoadSound("item_appear.ogg");

LoadPrototype("bomb.lua")
LoadPrototype("bomb-dropper.lua")
LoadPrototype("explosion-bomb.lua")
LoadPrototype("black-block.lua")
LoadTexture("barrel.png")
LoadPrototype("barrel.lua")
LoadPrototype("barrel-throw.lua")
LoadTexture("fireshot.png")
LoadPrototype("fireshot.lua")
LoadTexture("turret-wall.png")
LoadPrototype("turret-wall.lua")
LoadPrototype("turret-wall-shot.lua")

snow_falls = true;
snow_off = false;
bombs = false;
snow = CreateSprite("generic-snow-area", -100, -900);
GroupObjects(snow, CreateSprite("phys-empty", 2000, -900));

LoadTexture("clouds1.png")
LoadTexture("clouds2.png")
LoadTexture("clouds3.png")
LoadTexture("clouds4.png")
LoadPrototype("clouds1.lua")
LoadPrototype("clouds2.lua")
LoadPrototype("clouds3.lua")
LoadPrototype("clouds4.lua")
LoadTexture("wall.png")
LoadPrototype("wall.lua")

local rib = CreateRibbon("clouds4", 0, 100,1, -1);
--SetRibbonAttatachToY(rib, true)
rib = CreateRibbon("clouds3", 0, 100, 0.95, -0.99);
--SetRibbonAttatachToY(rib, true)
rib = CreateRibbon("clouds2", 0, 50, 0.9, -0.98);
--SetRibbonAttatachToY(rib, true)
rib = CreateRibbon("clouds1", 0, 50, 0.8, -0.97);
--SetRibbonAttatachToY(rib, true)


dofile("levels/nymap.lua")

PlayBackMusic("iie_street")

checkpoint = {}
checkpoint.x = 0
checkpoint.y = 0
--SetPlayerRevivePoint(67, 316);	

SetCamAttachedObj(GetPlayer().id);		-- ������� ������ � �������
SetCamAttachedAxis(true, true);		-- ������ ��������� ������ �� ��� X
SetCamFocusOnObjPos(constants.CamFocusBottomCenter);	-- ������ ������ �� ��������� ������� ���� �������
-- ��� ����� �������� y-���������� ������� ���� �� ��������, ����� �������, ������ �� ������ ���, ��������, ����������
SetCamObjOffset(0, 60);	-- ������ �������� ������ ������������ �������
SetCamUseBounds(true)
-- SetCamBounds(left, right, top, bottom)
SetCamBounds(25, 12000, -9700,419)

relimited = false

CreateColorBox(5204, -2024, 6005, 276, -0.5, {0, 0, 0, 1});
CreateColorBox(6644, -2024, 7444, 281, -0.5, {0, 0, 0, 1});
bomber = CreateEnemy("bomb-dropper", 160, 0)
barrels = CreateEnemy("barrel-throw", 11689, -50)
SetObjAnim(barrels, "off", false)
nomorebombs = false
SetObjAnim(bomber, "off", false)
bossparts = {}
bossparts[0] = CreateEnemy("turret-wall", 11374, 201)
bossparts[1] = CreateEnemy("turret-wall", 11441, 269)
bossparts[2] = CreateSprite("phys-empty", 11477, -76)
bossparts[3] = CreateSprite("phys-empty-end", 11487, 336)
bossparts[4] = CreateSprite("wall1", 11412, -99)
bossparts[5] = CreateEnemy("wall", 11437, 30)
bossparts[6] = CreateSprite("wall1", 11411, 105)
GroupObjects(bossparts[2], bossparts[3]);
levelend = false
if mapvar == nil then mapvar = {} end

function CloseTo(object, x, y, eps)
	return ( math.abs(object.aabb.p.x - x) < eps ) and ( math.abs(object.aabb.p.y - y) < eps )
end

local LEVEL = {}

function LEVEL.MapScript(player)
	screen_x, screen_y = GetCamPos()
	local dx = math.random(6568-6072)

	if (player == nil or snow_falls == nil) then return end
	if mapvar.snowmode and mapvar.snowmode ~= old_snow then
		old_snow = mapvar.snowmode
		if mapvar.snowmode == "FULL" then
			SetObjAnim(snow, "idle", true)	
		end
		if mapvar.snowmode == "OFF" then
			SetObjAnim(snow, "off", true)
		end
		if mapvar.snowmode == "PART" then
			SetObjAnim(snow, "part", true)
		end
	end
	if (snow_falls) then
		SetObjPos( snow, screen_x, screen_y-480 )
	elseif not (snow_off) then
		SetObjPos( snow, -9000, 9000 )
		snow_off = true
	end
	if (player.aabb.p.x >= 5202 and player.aabb.p.y>= -1825 and player.aabb.p.x <= 7258 and snow_falls) then
		SetObjPos( snow, -9000, 9000 )
		snow_falls = false
		snow_off = false
	elseif ( not snow_falls ) then
		--SetObjAnim(snow, "idle", false)
		snow_falls = true
	end

	if not nomorebombs then
		if bombs then
			SetObjPos( bomber, 6072+dx, screen_y-300 )
		end
		if (player.aabb.p.x >= 6047 and player.aabb.p.y>= -1975 and player.aabb.p.x <= 6609 ) then
			if not bombs then
				SetObjAnim(bomber, "working", false)
				bombs = true
			end
		elseif bombs then
			SetObjAnim(bomber, "off", false)
			bombs = false
		end		
	end
	if player.aabb.p.y <= -2032 and not nomorebombs then
		SetObjDead(bomber)
		nomorebombs = true
	end

	if  player.aabb.p.x > 9861+320 and player.aabb.p.y > -2031 then
	     if not relimited then
		relimited = true
		SetCamBounds(9861, 11730, -9700, 345)
	     end
	elseif relimited and player.aabb.p.y < -2031 then
		SetCamBounds(25, 12000, -9700,419)
		relimited = false
	end

		
	if (CloseTo(player, 5603, 340, 50) and checkpoint.x ~= 5603 and checkpoint.y ~= 340) then
		checkpoint.x = 5603
		checkpoint.y = 340
		SetPlayerRevivePoint(checkpoint.x, checkpoint.y)
	end
	if (CloseTo(player, 6846, -2151, 80) and checkpoint.x ~= 6846 and checkpoint.y ~= -2511) then
		checkpoint.x = 6846
		checkpoint.y = -2151
		SetPlayerRevivePoint(checkpoint.x, checkpoint.y)
	end
	if (player.aabb.p.x>=10961) and (checkpoint.x ~= 11055) then
		checkpoint.x = 11055
		SetPlayerRevivePoint(11000, 252)
		SetObjAnim(barrels, "working", false)
		SetCamBounds(screen_x-320, 11730, -9700, 345)		
	end
	if ( mapvar.bossdead ~= nil and not levelend ) then
		levelend = true
		for i=0,4 do
			SetObjDead(bossparts[i])
		end
		SetObjDead(barrels)
		SetObjAnim(bossparts[6], "blasted", false)
	end
	if (player.aabb.p.x > 11730) then
		LEVEL.missionStarter.ChangeLevel("nymap2script")
	end
end


function LEVEL.SetLoader(l)
	LEVEL.missionStarter = l
	
	--Log("Set loader ", l == nil and "nil" or "l")
end

return LEVEL
