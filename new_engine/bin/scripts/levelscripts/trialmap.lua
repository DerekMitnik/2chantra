--EDITABLE MAP

local LEVEL = {}
InitNewGame()
dofile("levels/trialmap.lua")

CONFIG.backcolor_r = 0.090000
CONFIG.backcolor_g = 0.117000
CONFIG.backcolor_b = 0.035000
LoadConfig()

PlayBackMusic("music/01 - Megnetic Holes.mp3")

LEVEL.nextPlayer = 0;
LEVEL.spawnpoints = { {-278, -1187}, {-346, -1188} }
LEVEL.characters = {"exchan", "pear15unyl"}

function CreatePlatform( platform, coords, loop )
	Log("CreatePlatform",platform,coords,loop);	
	local wpsize = 16
	local first = CreateWaypoint( coords[1][1], coords[1][2], wpsize, wpsize )
	local second = CreateWaypoint( coords[2][1], coords[2][2], wpsize, wpsize )
	SetNextWaypoint( first, second )
	local last = second
	local now = 0
	for i=3, #coords do
		now = CreateWaypoint( coords[i][1], coords[i][2], wpsize, wpsize )
		SetNextWaypoint( last, now )
		last = now
	end
	if loop then
		SetNextWaypoint( last, first )
	else
		for i=#coords-1,2,-1 do
			now = CreateWaypoint( coords[i][1], coords[i][2], wpsize, wpsize )
			SetNextWaypoint( last, now )
			last = now
		end
		SetNextWaypoint( last, first )
	end
	SetEnemyWaypoint( CreateEnemy(platform, coords[1][1], coords[1][2]), second )
end

function AttachCamToAllPLayers()
	idCamObject = CreateSprite("none",0,0);
	Log("LEVEL.AttachCamToAllPLayers()");
	while true do
		local amount = GetActorCount();
		local scalex = 1;
		local scaley = 1;
		local scale = 1;		
		local minx = GetPlayer().aabb.p.x;
		local maxx = GetPlayer().aabb.p.x;
		local miny = GetPlayer().aabb.p.y + GetPlayer().aabb.H - 60;
		local maxy = GetPlayer().aabb.p.y + GetPlayer().aabb.H - 60;
		for i = 1,amount do
			SetDefaultActor(i);
			local player = GetPlayer();
			local x = player.aabb.p.x;
			local y = player.aabb.p.y + player.aabb.H - 60;
			if y > 0 then
				SetObjDead(player.id)
				break
			end
			if x < minx then
				minx = x
			end
			if x > maxx then
				maxx = x
			end
			if y < miny then
				miny = y
			end
			if y > maxy then
				maxy = y
			end
		end
		local x = (minx + maxx)/2;
		local y = (miny + maxy)/2;
		SetObjPos(idCamObject,x,y);
		SetCamAttachedObj(idCamObject);
		if maxx - minx > CONFIG.scr_width / 2 then
			scalex = CONFIG.scr_width / (maxx - minx + CONFIG.scr_width / 2 );
		end
		if maxy - miny + 60 > CONFIG.scr_height / 2 then
			scaley = CONFIG.scr_height / (maxy - miny + 60 + CONFIG.scr_height / 2 );
		end
		if scalex < scaley then
			scale = scalex
		else
			scale = scaley
		end
		
		SetCamScale(scale,scale);
		Wait(50)
	end
end

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	if GetPlayer() then
		SetCamAttachedObj( GetPlayer().id )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
		local env = CreateEnvironment('default_environment', -9001, -9001)
		SetDefaultEnvironment(env)
	end
	CreatePlatform("enemies/flyingplatform",{ {-158, -1287}, {-446, -1288}, {-446, -1688} },true)
	CreateEnemy("enemies/crate", -178, -1287)
	CreateEnemy("enemies/crate", -178, -1187)
	CreateEnemy("enemies/crate", -178, -1387)
	CreateEnemy("enemies/crate", -178, -1487)
	CreateEnemy("enemies/crate", -178, -1587)
	CreateEnemy("enemies/crate", -178, -1687)
	
	CreateEnemy("enemies/btard-sci", -178-200, -1687)
	CreateEnemy("enemies/btard-sci", -178-200, -1587)
	CreateEnemy("enemies/btard-sci", -178-300, -1687)
	CreateEnemy("enemies/btard-sci", -178-100, -1687)

--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
	Resume( NewMapThread( AttachCamToAllPLayers ) )
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local obj1 = CreateItem( 'generic-trigger', 5625, -447 )
	local obj2 = CreateItem( 'generic-trigger', 5683, -270 )
	GroupObjects( obj1, obj2 )
	local obj3 = CreateItem( 'generic-trigger', 7695, -572 )
	local obj4 = CreateItem( 'generic-trigger', 7817, -274 )
	GroupObjects( obj3, obj4 )
	local object
	ObjectPushInt( obj1, 1 )
	ObjectPushInt( obj1, 1 )
	ObjectPushInt( obj3, 1 )
	ObjectPushInt( obj3, 2 )
--$(SPECIAL_CREATION)-
end

LEVEL.GetNextCharacter = (function()
	local local_iterator = 0
	local level = LEVEL
	return function()
		local_iterator = local_iterator + 1
		return { level.characters[local_iterator], level.spawnpoints[local_iterator] }
	end
end)()

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	if trigger == 1 then
		SetObjDead( id )
		--local id1 = id - 79 + 24;
		--local id2 = id - 79 + 25;
		--local id3 = id - 75 + 16;
		--SetObjDead( id1 );
		--SetObjDead( id2 );
		--local obj = GetPlayer();
		--SetObjDead( obj.id )
		--CreateEnemy( "slowpoke-ride", -1403, -534 );
		Resume( NewMapThread( function()
			for i = -1355,1000,50 do
				for j=1, 10 do
					CreateEnemy( "big_explosion", i+math.random(-100,100)+7500, math.random( 330, 1577 ) -1300 )
					CreateEnemy( "big_explosion", i+math.random(-100,100)+6500, math.random( 330, 1577 ) -1300 )
				end
				Wait(600)
				Wait(1)
			end
			--SetObjPos( id3, -1200, -600 );
			--SetObjPos( id1, -1200, -600 );
			--SetObjPos( id2, -1200, -600 );
			--for i = 1, 100 do
			--	for j = id+50, id+100 do
			--	local obj = GetObject(j);
			--	SetObjPos( j, obj.aabb.p.x+5, obj.aabb.p.y-5 );
			--	end
			--end
		end ))
		
	elseif trigger == 2 then
		SetObjDead( id )
		Menu.showEndLevelMenu("superiic")
	end
--$(MAP_TRIGGER)-
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'testingmap'

return LEVEL
