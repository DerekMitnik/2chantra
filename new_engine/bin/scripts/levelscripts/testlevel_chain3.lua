InitNewGame();


local LEVEL = {}
require("routines")
--TogglePause()

CONFIG.backcolor_r = 67/255;
CONFIG.backcolor_g = 153/255;
CONFIG.backcolor_b = 190/255;
LoadConfig();

local x = -48
local dx = 32
for i = 0,500 do
	CreateSprite("phys_floor", x + dx*i, 350);
end

CreatePlayer("sohchan", 50, 300);
CreatePlayer("unylchan", 50, 300);

CreateItem("bonus-random", 200, 300)
CreateItem("bonus-diamond", 230, 300)
CreateItem("health", 260, 300)
CreateItem("ammo", 290, 300)

CreateSprite("release_sign", 500, 275)

function map_trigger( id, trigger )
	if ( trigger == 1 ) then
		Menu.showEndLevelMenu("testlevel_chain1")
	end
end

trigger = CreateItem("generic-trigger", 500, 200)
GroupObjects( trigger, CreateItem("generic-trigger", 700, 350) )
ObjectPushInt( trigger, 1 )

local env = CreateEnvironment("default_environment", -9001, -9001)
SetDefaultEnvironment(env)

SetCamAttachedObj(GetPlayer().id);		-- ������� ������ � �������
SetCamAttachedAxis(true, true);		-- ������ ��������� ������ �� ��� X
SetCamFocusOnObjPos(constants.CamFocusBottomCenter);	-- ������ ������ �� ��������� ������� ���� �������
-- ��� ����� �������� y-���������� ������� ���� �� ��������, ����� �������, ������ �� ������ ���, ��������, ����������
SetCamObjOffset(0, 60);	-- ������ �������� ������ ������������ �������



----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------


function LEVEL.MapScript(player)
end

function LEVEL.WeaponBonus()
end

function LEVEL.placeSecrets()
end

function LEVEL.removeSecrets()
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter = l
	
	--Log("Set loader ", l == nil and "nil" or "l")
end

LEVEL.name = "chain3"

return LEVEL