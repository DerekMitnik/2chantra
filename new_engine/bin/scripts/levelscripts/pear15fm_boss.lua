--EDITABLE MAP

local LEVEL = {}
InitNewGame()
dofile("levels/pear15fm_boss.lua")

CONFIG.backcolor_r = 0.280000
CONFIG.backcolor_g = 0.280000
CONFIG.backcolor_b = 0.280000
LoadConfig()

StopBackMusic()

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
Game.fade_out()
	SetCamBounds( -256, -256 + 640, 320+16-480, 320+16 )
	CamMoveToPos( -256 + 320, 320 + 16 - 240 )
	mapvar.tmp.fade_widget_text = {}
	local sx, sy = GetCaptionSize( "default", "������� 5" )
	local text1 = CreateWidget( constants.wt_Label, "TEXT", fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 - 1.5 * sy, 1, 1 )
	mapvar.tmp.fade_widget_text["text1"] = text1
	WidgetSetCaptionColor( text1, {1,1,1,1}, false )
	WidgetSetCaption( text1, "������� 5" )
	WidgetSetZ( text1, 1.05 )
	sx, sy = GetCaptionSize( "default", "���� ��������" )
	local text2 = CreateWidget( constants.wt_Label, "TEXT", fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 + 1.5 * sy, 1, 1 )
	mapvar.tmp.fade_widget_text["text2"] = text2
	WidgetSetCaptionColor( text2, {1,1,1,1}, false )
	WidgetSetCaption( text2, "���� ��������" )
	WidgetSetZ( text2, 1.05 )
	SetCamAttachedAxis(true, true)
	SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
	SetCamObjOffset(0, 60)
	SetCamLag(0.9)
	Game.AttachCamToPlayers( {
		{ {-99999,-99999,99999,99999}, {-256, 320+16-480, -256+640, 320+16} },
	} )
	local env = CreateEnvironment('default_environment', -9001, -9001)
	SetDefaultEnvironment(env)
	Game.info.can_join = false
	mapvar.tmp.cutscene = true
		
	EnablePlayerControl( false )
	Menu.lock()
	local fade_thread = NewThread( function()
		Wait( 1 )
		GUI:hide()
		Wait( 1000 )
		SetObjPos( GetPlayerCharacter(1), -320, 229 )
		if Game.actors[2] then
			SetObjPos( GetPlayerCharacter(2), -360, 229 )
		end
		local char1, char2, char1_talk, char2_talk = Game.GetCharacters()
		mapvar.tmp.fade_complete = false
		Game.fade_in( false, "FADED" )
		WaitForEvent( "FADED" )
		Game.MoveWithAccUntilX( char1, {x=1.5,y=0}, -50, "MOVED" )
		if char2 then
			Game.MoveWithAccUntilX( char2, {x=1.5,y=0}, -150, "MOVED" )
		end
		WaitForEvent( "MOVED" )
		if char2 then
			WaitForEvent( "MOVED" )
		end
		local phys = CreateSprite( "phys-empty", -300, -300 )
		GroupObjects( phys, CreateSprite( "phys-empty", -256, 500 ) )
		SetObjSolidToByte( phys, constants.physPlayer )
		local conversation_end = function()
			mapvar.tmp.cutscene = false
			EnablePlayerControl( true )
			SetCamLag(0.9)
			Menu.unlock()
			GUI:show()
			Game.info.can_join = true
			mapvar.tmp.boss_obj = GetObjectUserdata( CreateEnemy( "pear15bigmanpac", 443, 5 ) )
			PlayBackMusic( "music/iie_boss.it" )
		end
		Conversation.create{
			function( var ) 
				if mapvar.actors[2] then
					return "BOTH-PLAYERS"
				elseif mapvar.actors[1].info.character == "pear15unyl" then
					return "UNYL-SINGLE"
				end
			end,
			{ sprite = "portrait-soh" },
			char1_talk,
			"PEAR15_MOUNTAINS_36",
			char1_talk,
			"PEAR15_MOUNTAINS_37",
			conversation_end,
			nil,
			{ type = "LABEL", "UNYL-SINGLE" },
			{ sprite = "portrait-unyl" },
			char1_talk,
			"PEAR15_MOUNTAINS_38",
			char1_talk,
			"PEAR15_MOUNTAINS_39",
			conversation_end,
			nil,
			{ type = "LABEL", "BOTH-PLAYERS" },
			{ sprite = "portrait-unyl" },
			char2_talk,
			"PEAR15_MOUNTAINS_40",
			{ sprite = "portrait-soh" },
			char1_talk,
			"PEAR15_MOUNTAINS_41",
			{ sprite = "portrait-unyl" },
			char2_talk,
			"PEAR15_MOUNTAINS_42",
			{ sprite = "portrait-soh" },
			char1_talk,
			"PEAR15_MOUNTAINS_43",
			{ sprite = "portrait-unyl" },
			char2_talk,
			"PEAR15_MOUNTAINS_44",
			conversation_end,
			nil,
		}:start()
	end )
	Resume( fade_thread )
		
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.boss_defeated()
	Resume( NewMapThread( function()
		Wait( 3000 )
		local obj1 = CreateItem( "generic-trigger", -1000, -1000 )
		ObjectPushInt( obj1, 0 )
		SetObjAnim( obj1, "script", false )
	end ))
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local object
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	SetObjDead( id )
	if Game.custom_map then
		Loader.exitSlowly()
	else
		mapvar.actors[1].info.track = "mountain2"
		Game.ShowLevelResults("pear15hoverbike")
	end
--$(MAP_TRIGGER)-
end

LEVEL.spawnpoints = {{-300,229},}

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'Manpac King'

return LEVEL
