--EDITABLE MAP

local LEVEL = {}
InitNewGame()
dofile("levels/caves_test.lua")

CONFIG.backcolor_r = 0.042969
CONFIG.backcolor_g = 0.042969
CONFIG.backcolor_b = 0.042969
LoadConfig()

StopBackMusic()

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	if GetPlayer() then
		SetCamAttachedObj( GetPlayer().id )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
		local env = CreateEnvironment('default_environment', -9001, -9001)
		SetDefaultEnvironment(env)
	end
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local object
--$(SPECIAL_CREATION)-
end

function LEVEL.GetNextCharacter()
--$(GET_NEXT_CHARACTER)+
	Loader.level.char_iterator = (Loader.level.char_iterator or 0) + 1
	return { Loader.level.characters[Loader.level.char_iterator], Loader.level.spawnpoints[Loader.level.char_iterator]}
--$(GET_NEXT_CHARACTER)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
--$(MAP_TRIGGER)-
end

LEVEL.characters = {"pear15soh",}
LEVEL.spawnpoints = {{-20,69},}

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'caves_test'

return LEVEL
