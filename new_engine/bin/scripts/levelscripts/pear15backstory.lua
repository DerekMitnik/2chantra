local LEVEL = {}
InitNewGame()

CONFIG.backcolor_r = 0.000000
CONFIG.backcolor_g = 0.000000
CONFIG.backcolor_b = 0.000000
LoadConfig()

PlayBackMusic("music/nsmpr_woom.it", 0.8)

local skip_credits
local speedup_credits
local story4

local function end_this()
	local obj1 = CreateItem( "generic-trigger", -1000, -1000 )
	ObjectPushInt( obj1, 0 )
	SetObjAnim( obj1, "script", false )
end

local backstory = [[
PEAR15_BACKSTORY_01
PEAR15_BACKSTORY_02



PEAR15_BACKSTORY_03

{image="pear15backstory",frame=0,x=78,y=96}

PEAR15_BACKSTORY_04

{image="pear15backstory",frame=1,x=64,y=96}

PEAR15_BACKSTORY_05

{image="pear15backstory",frame=2,x=200,y=100}

PEAR15_BACKSTORY_06

{image="pear15backstory",frame=3,x=125,y=96}

PEAR15_BACKSTORY_07

{image="pear15backstory",frame=4,x=215,y=128}

PEAR15_BACKSTORY_08

{image="pear15backstory",frame=5,x=66,y=96}

PEAR15_BACKSTORY_09

{image="pear15backstory",frame=6,x=129,y=96}

PEAR15_BACKSTORY_10

{image="pear15backstory",frame=7,x=102,y=160}

PEAR15_BACKSTORY_11




PEAR15_BACKSTORY_12
]]


function ShowCrawl( text, after_event )
	local text = explode((text or ""), "\n")
	crawl_widgets = {}
	local font, color
	local sx, sy
	local r,g,b,a
	local y = CONFIG.scr_height + 1
	local w
	local username = {}
	local score = {}
	for i=1,#text do
		--{image="pear15backstory",frame=7,x=102,y=160}
		if string.match( text[i], "^{image=" ) then
			local sprite, frame, wd, h = string.match( text[i], '^{image="([^"]+)",frame=(%d+),x=(%d+),y=(%d+)}' )
			frame, wd, h = tonumber(frame), tonumber(wd), tonumber(h)
			w = CreateWidget( constants.wt_Picture, "crawl", nil, (CONFIG.scr_width-wd)/2, y, wd, h )
			WidgetSetSprite( w, sprite, true, frame )
			table.insert( crawl_widgets, { (CONFIG.scr_width-wd)/2, y, w, h } )
			y = y + h + 5
		else
			text[i] = dictionary_string(text[i])
			font = string.match( text[i], "^{font=([^}]+)}" )
			if font == "small" then
				font = "dialogue"
			end
			font = font or "default"
			color = string.match( text[i], "^{color=([^}]+)}" )
			color = color or "1,1,1,1"
			r,g,b,a = string.match( color, "([^,]+),([^,]+),([^,]+),([^,]+)" )
			r,g,b,a = tonumber(r),tonumber(g),tonumber(b),tonumber(a)
			color = { r, g, b, a }
			text[i] = string.gsub( text[i], "{[^}]+}", "" )
			local multiline = false
			if text[i] == "" then
				sx, sy = GetCaptionSize( font, "I" )
			else
				sx, sy = GetCaptionSize( font, text[i] )
			end
			sx, sy = 2*sx, 2*sy
			if sx > CONFIG.scr_width - 60 then
				multiline = true
				sx, sy = GetMultilineCaptionSize( font, text[i], CONFIG.scr_width - 60, 2 )
			end
			w = CreateWidget( constants.wt_Label, "crawl", nil, (CONFIG.scr_width-sx)/2, y, sx, sy )
			WidgetSetCaptionColor( w, color, false, {0,0,0,1}, true )
			WidgetSetCaptionFont( w, font, 2 )
			WidgetSetCaption( w, text[i], true )
			table.insert( crawl_widgets, { (CONFIG.scr_width-sx)/2, y, w, sy } )
			y = y + sy + 5
		end
	end

	local skip
	local thread = NewMapThread( function()
		local last_time = Loader.time
		local dt = 0
		while #crawl_widgets > 0 and not skip do
			dt = Loader.time - last_time
			last_time = Loader.time
			for key, value in pairs( crawl_widgets ) do
				value[2] = value[2] - 0.2 * iff( speedup_credits, dt, dt / 10 )
				WidgetSetPos( value[3], math.floor(value[1]), math.floor(value[2]) )
				if value[2] < -value[4] then
					DestroyWidget( value[3] )
					table.remove( crawl_widgets, key )
				end
			end
			Wait(1)
		end
		if #crawl_widgets > 0 then
			for key, value in pairs( crawl_widgets ) do
				DestroyWidget( value[3] )
			end
		end
		RestoreKeyProcessors()
		after_event()
	end )
	Resume( thread )

	RemoveKeyProcessors()
	GlobalSetKeyDownProc( function( key )
		if key == CONFIG.key_conf[1].gui_nav_accept or key == CONFIG.key_conf[1].fire or
		   key == CONFIG.key_conf[2].gui_nav_accept or key == CONFIG.key_conf[2].fire then
			speedup_credits = true
		end
	end )
	GlobalSetKeyReleaseProc( function( key )
		if key == CONFIG.key_conf[1].gui_nav_decline or key == CONFIG.key_conf[1].gui_nav_menu or
		   key == CONFIG.key_conf[2].gui_nav_decline or key == CONFIG.key_conf[2].gui_nav_menu then 
				skip = true
				skip_credits = true
		elseif key == CONFIG.key_conf[1].gui_nav_accept or key == CONFIG.key_conf[1].fire
		    or key == CONFIG.key_conf[2].gui_nav_accept or key == CONFIG.key_conf[2].fire then
				speedup_credits = false
		end
	end )
end

local current_phase

local function start( phase )
	current_phase = NewThread( phase )
	Resume( current_phase )
end

local function phase1()
	ShowCrawl( backstory, end_this )
end

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	mapvar.tmp.cutscene = true
	Game.info.can_join = false
	SetCamBounds( -369, -369 + 640, -205, -205 + 480 )
	SetCamUseBounds( true )
	CamMoveToPos( -369 + 320, -205 + 240 )
	EnablePlayerControl( false )
	Menu:lock()
	Resume( NewMapThread( function()
		Wait(1)
		start( phase1 )
	end ))
	local env = CreateEnvironment('default_environment', -9001, -9001)
	SetDefaultEnvironment(env)
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
--$(SPECIAL_CREATION)-
end


function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	SetObjDead( id )
	Menu.showMainMenu()
--$(MAP_TRIGGER)-
end

LEVEL.spawnpoints = {{331,50.5}}

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'Backstory'

return LEVEL
