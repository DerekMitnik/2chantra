--EDITABLE MAP

local LEVEL = {}
InitNewGame()
dofile("levels/map28.lua")

CONFIG.backcolor_r = 0.280000
CONFIG.backcolor_g = 0.280000
CONFIG.backcolor_b = 0.280000
LoadConfig()

StopBackMusic()

LEVEL.secrets = 0

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	Game.fade_out()
	mapvar.tmp.fade_widget_text = {}
	if GetPlayer() then
		local sx, sy = GetCaptionSize( "default", "New map" )
		local text1 = CreateWidget( constants.wt_Label, "TEXT", fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 - 1.5 * sy, 1, 1 )
		mapvar.tmp.fade_widget_text["text1"] = text1
		WidgetSetCaptionColor( text1, {1,1,1,1}, false )
		WidgetSetCaption( text1, "New map" )
		WidgetSetZ( text1, 1.05 )
		SetCamAttachedObj( GetPlayer().id )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
		Game.AttachCamToPlayers( {
			{ {-99999,-99999,99999,99999}, {-99999,-99999,99999,99999} },
		} )
		local env = CreateEnvironment('default_environment', -9001, -9001)
		SetDefaultEnvironment(env)
		Game.info.can_join = false
		mapvar.tmp.cutscene = true
		EnablePlayerControl( false )
		Menu.lock()
		
		local start_intro_thread = false
		local fade_thread = NewThread( function()
			Wait( 1 )
			GUI:hide()
			Wait( 1000 )
			mapvar.tmp.fade_complete = false
			Game.fade_in()
			start_intro_thread = true
		end )
		
		Resume( fade_thread )
		
		local intro_thread = NewThread( function()
			while not start_intro_thread do
				Wait( 10 )
			end
			--SetSkipKey( function_skip, 100 )
			Game.info.can_join = false
			mapvar.tmp.cutscene = true
			mapvar.tmp.fade_complete = false
			Game.fade_in()
			while not mapvar.tmp.fade_complete do
				Wait(1)
			end
			--RemoveSkipKey()
			
			mapvar.tmp.cutscene = false
			mapvar.tmp.fade_complete = true
			EnablePlayerControl( true )
			Menu.unlock()
			GUI:show()
		end )
		
		--SetSkipKey( function_skip, 500 )
		
		Resume( intro_thread )
	end
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local object
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
--$(MAP_TRIGGER)-
end

LEVEL.characters = {"pear15soh",}
LEVEL.spawnpoints = {{-208,350.5},{-236,350},{-264,349.5},{-292,349},{-320,348.5},{-348,348},{-376,347.5},{-404,347},}

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'map28'

return LEVEL
