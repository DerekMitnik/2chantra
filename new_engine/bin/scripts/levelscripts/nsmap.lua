--EDITABLE MAP

local LEVEL = {}
InitNewGame()
dofile("levels/nsmap.lua")

CONFIG.backcolor_r = 0.352945
CONFIG.backcolor_g = 0.217896
CONFIG.backcolor_b = 0.464844
LoadConfig()

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	if GetPlayer() then
		SetCamAttachedObj( GetPlayer().id )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
	end
--$(INIT_SCRIPT)-
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'nsmap'

return LEVEL
