--EDITABLE MAP

local LEVEL = {}
InitNewGame()
dofile("levels/superiic_copy.lua")

CONFIG.backcolor_r = 0.090000
CONFIG.backcolor_g = 0.117000
CONFIG.backcolor_b = 0.035000
LoadConfig()

PlayBackMusic("music/iie_escape.it")

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	if GetPlayer() then
		SetCamAttachedObj( GetPlayer().id )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
	end
	EnablePlayerControl( false )
	Menu.locked = true
	local env = CreateEnvironment("default_environment", -9001, -9001)
	SetDefaultEnvironment(env)
	showStageName( -1, function()  FadeIn(1000) EnablePlayerControl( true ) ShowWeaponScreen( true, {"TEST", "TEST2", "TEST3", "TEST4"} ) Menu.locked = false end )
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function a()
	CreateEnemy( "btard2", GetMousePos() )
end

function s()
	achievement()
end

function achievement()
	--193, 111
	local panel = CreateWidget( constants.wt_Panel, "name", nil, 640-283, 480, 283, 111 )
	WidgetSetSprite( panel, "window1" )
	local pic = CreateWidget( constants.wt_Picture, "name", panel, 640-283+11, 480+26, 64, 64 )
	WidgetSetSprite( pic, "bdeath", true )
	WidgetSetZ( pic, 1 )
	local text = CreateWidget( constants.wt_Label, "name", panel, 640-283+11+100, 480+36, 1, 1 )
	WidgetSetCaptionColor( text, {1, .8, 0, 1}, false )
	WidgetSetCaption( text, "Achievement unlocked!" )
	WidgetSetCaptionFont( text, "dialogue" )
	local name = CreateWidget( constants.wt_Label, "name", panel, 640-283+11+130, 480+46+20, 1, 1 )
	WidgetSetCaptionColor( name, {1, 1, 1, 1}, false )
	WidgetSetCaption( name, "Btard Killer" )
	WidgetSetCaptionFont( name, "dialogue" )
	Resume( NewMapThread( function()
		for i=480,480-111,-10 do
			WidgetSetPos( panel, 640-283, i )
			Wait(1)
		end
		Wait(2000)
		for i=1,0,-0.05 do
			WidgetSetSpriteColor( panel, {1,1,1,i} )
			WidgetSetSpriteColor( pic, {1,1,1,i} )
			WidgetSetCaptionColor( text, {1, .8, 0, i}, false )
			WidgetSetCaptionColor( name, {1, 1, 1, i}, false )
			Wait(1)
		end
		DestroyWidget( panel )
		DestroyWidget( pic )
		DestroyWidget( text )
		DestroyWidget( name )
	end ))
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local obj1 = CreateItem( 'generic-trigger', 165, 658 )
	local obj2 = CreateItem( 'generic-trigger', 292, 974 )
	GroupObjects( obj1, obj2 )
	local obj3 = CreateItem( 'generic-trigger', 2147, 848 )
	local obj4 = CreateItem( 'generic-trigger', 2214, 955 )
	GroupObjects( obj3, obj4 )
	local object
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	if trigger == 1 then
		SetObjDead( id )
		Resume( NewMapThread( function()
			for i = -15,2000,100 do
				for j=1, 5 do
					CreateEnemy( "big_explosion", i+math.random(-20,20), math.random( 630, 1277 ) )
				end
				Wait(500)
				Wait(1)
			end
		end ))
	elseif trigger == 2 then
		SetObjDead( id )
		Menu.showEndLevelMenu("superiic")
	end
--$(MAP_TRIGGER)-
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'superiic_copy'

return LEVEL
