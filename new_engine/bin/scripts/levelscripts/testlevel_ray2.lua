InitNewGame();
local LEVEL = {}

local env = CreateEnvironment("default_environment", -9001, -9001)
SetDefaultEnvironment(env)

CreatePlayer("sohchan", 20, -30)


GroupObjects(CreateSprite("phys_floor", 0, 0), CreateSprite("phys_floor", 1000, 0))


	

SetCamAttachedObj(GetPlayer().id);		-- ������� ������ � �������
SetCamAttachedAxis(true, true);		-- ������ ��������� ������ �� ��� X
SetCamFocusOnObjPos(constants.CamFocusBottomCenter);	-- ������ ������ �� ��������� ������� ���� �������
-- ��� ����� �������� y-���������� ������� ���� �� ��������, ����� �������, ������ �� ������ ���, ��������, ����������
SetCamObjOffset(0, 60);	-- ������ �������� ������ ������������ �������



function keyp( butt )
---[[
	local pl = GetPlayer()
	local px, py = GetMP( pl.id, 0 )
	local function ang(a) return a end
	if pl.sprite.mirrored then 
		px = -px 
		ang = function(a) return -(a+180) end
	end

	if butt == keys.f then
		CreateBullet( "sfg9000", pl.aabb.p.x+px, pl.aabb.p.y+py, pl.id, pl.sprite.mirrored, 0, 0 )
	elseif butt == keys.r then
		CreateBullet( "sfg9000", pl.aabb.p.x+px, pl.aabb.p.y+py, pl.id, pl.sprite.mirrored, -45, 2 )
	elseif butt == keys.v then
		CreateBullet( "sfg9000", pl.aabb.p.x+px, pl.aabb.p.y+py, pl.id, pl.sprite.mirrored, 45, -2 )
	elseif butt == keys.g then
		CreateRay( "beam", pl.id, ang(0), pl.aabb.p.x+px, pl.aabb.p.y+py )
	elseif butt == keys.t then
		CreateRay( "beam", pl.id, ang(45), pl.aabb.p.x+px, pl.aabb.p.y+py )
	elseif butt == keys.b then
		CreateRay( "beam", pl.id, ang(-35), pl.aabb.p.x+px, pl.aabb.p.y+py )
	elseif butt == keys.h then
		CreateRay( "beam", pl.id, ang(135), pl.aabb.p.x+px, pl.aabb.p.y+py )
	elseif butt == keys.y then
		CreateRay( "beam", pl.id, ang(-135), pl.aabb.p.x+px, pl.aabb.p.y+py )
	elseif butt == keys.n then
		Resume( 
			NewMapThread( 
				function()
					for i=0,360 do
						if i ~= 90 and i ~= 270 then
							CreateRay( "beam", pl.id, i, pl.aabb.p.x+px, pl.aabb.p.y+py )
						end
						Wait(100)
					end
				end
			)
		)
	end
--]]
end

StopBackMusic()
GlobalSetKeyReleaseProc( keyp )

----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------

function LEVEL.MapScript(player)
end

function LEVEL.WeaponBonus()
end

function LEVEL.placeSecrets()
end

function LEVEL.removeSecrets()
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter = l
	
	AddTimerEvent(10, function () GUI:hide() end)
end

return LEVEL