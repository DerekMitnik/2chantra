InitNewGame();

if not mapvar.tmp then
	mapvar.tmp = {}
end

mapvar.char = {}
mapvar.char.SOH = 1

CONFIG.backcolor_r = 153/255;
CONFIG.backcolor_g = 153/255;
CONFIG.backcolor_b = 153/255;
LoadConfig();

local env = CreateEnvironment("default_environment", -9001, -9001)
SetDefaultEnvironment(env)
dofile( "levels/testlevel-dodge.lua" );


SetCamAttachedObj(GetPlayer().id);		-- ������� ������ � �������
SetCamAttachedAxis(true, true);		-- ������ ��������� ������ �� ��� X
SetCamFocusOnObjPos(constants.CamFocusBottomCenter);	-- ������ ������ �� ��������� ������� ���� �������
-- ��� ����� �������� y-���������� ������� ���� �� ��������, ����� �������, ������ �� ������ ���, ��������, ����������
SetCamObjOffset(0, 60);	-- ������ �������� ������ ������������ �������

SetCamLag(0.9)

SetCamUseBounds(true)
SetCamBounds(-18, 13929, -9000, 152)
mapvar.tmp.bounds = 1

function map_trigger( id, trigger )
end

--[[
local trigger = CreateItem("generic-trigger", 5277, -553)
GroupObjects( trigger, CreateItem("generic-trigger", 5395, -148) )
ObjectPushInt( trigger, 1 )
trigger = CreateItem("generic-trigger", 5692, -553)
GroupObjects( trigger, CreateItem("generic-trigger", 5895, -148) )
ObjectPushInt( trigger, 2 )
trigger = CreateItem("generic-trigger", 13835, -60)
GroupObjects( trigger, CreateItem("generic-trigger", 13900, 54) )
ObjectPushInt( trigger, 3 )

trigger = CreateItem("generic-death-area", -9000, 206)
GroupObjects( trigger, CreateItem("generic-death-area", 90000, 666) )
--]]

--[[
local limit = CreateSprite("phys-empty", -28, -1000 )
GroupObjects( limit, CreateSprite("phys-empty", -18, 1000 ) )
SetObjSolidToByte( limit, 1 )
mapvar.tmp.area_limits = limit
mapvar.tmp.introed = false
--]]

weapon = 
	{ 
		reload_time = 400, 
		clip_reload_time = 800; 
		shots_per_clip = 6; 
		ammo_per_shot = 1; 
		bullet = "custom/energy1";
		color = { 1, 1, 1, 1 };
		bullet_param =
		{
			damage = 20;
			vel = 5;
		}
	}

function wpn( st )
	local pl = GetPlayer()
	if string.match(st, "r%+") then
		weapon.reload_time = weapon.reload_time * 2
	end
	if string.match(st, "r%-") then
		weapon.reload_time = math.floor( weapon.reload_time / 2 + 0.5 )
	end
	if string.match(st, "t%+") then
		weapon.clip_reload_time = weapon.clip_reload_time * 2
	end
	if string.match(st, "t%-") then
		weapon.clip_reload_time = math.floor( weapon.clip_reload_time / 2 + 0.5 )
	end
	if string.match(st, "c%+") then
		weapon.shots_per_clip = weapon.shots_per_clip * 2
	end
	if string.match(st, "c%-") then
		weapon.shots_per_clip = math.floor( weapon.shots_per_clip / 2 + 0.5 )
	end
	if string.match(st, "a%+") then
		weapon.ammo_per_shot = weapon.ammo_per_shot * 2
	end
	if string.match(st, "a%-") then
		weapon.ammo_per_shot = math.floor( weapon.ammo_per_shot / 2 + 0.5 )
	end
	if st == "special" then
		weapon.bullet = "../weapons/fragmentation"
	end
	SetCustomWeapon( pl.id, weapon )
end

function custom_weapon( id, shooter, param, dir, x, y )
	if param == 0 then
		SetObjSpriteColor(id, weapon.color)
	elseif param == 1 then
		local pl = GetObject(shooter)
		local angle = ({ 0, -45, 45 })[dir+1]
		dir = ({ 0, 2, -2 })[dir+1]
		local bullets = {}
		table.insert(bullets, CreateBullet( weapon.bullet, x, y, shooter, pl.sprite.mirrored, angle+math.random(-10,10)/10, dir ))
		table.insert(bullets, CreateBullet( weapon.bullet, x, y+10, shooter, pl.sprite.mirrored, angle+math.random(-10,10)/10, dir ))
		for key, value in pairs(bullets) do
			SetObjSpriteColor(value, weapon.color)
			SetCustomBullet(value, weapon.bullet_param)
		end
	elseif param == 2 then
		if weapon.explosion then
			SetObjPos( CreateBullet( "explosion", x, y, shooter, false, 0, 0 ), x, y )
		end
	end
end

StopBackMusic()

----------------------------------------------------------------------------------------------
local LEVEL = {}

function LEVEL.MapScript(player)
	local cx, cy = GetCamPos()
	cx = math.max(cx - CONFIG.scr_width/2, -18)
	if mapvar.tmp.bounds == 1 then
		SetCamBounds(cx, 90000, -9000, 9000)
	else
		SetCamBounds(cx, 90000, -9000, 9000)
	end
	SetObjPos( mapvar.tmp.area_limits, cx-5, 0 )
	SetPlayerRevivePoint(cx+32, -511)
end

function LEVEL.placeSecrets()
end

function LEVEL.removeSecrets()
end

function LEVEL.cleanUp()
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter = l
end

function LEVEL.register_weather( id, weather_id )
end

function LEVEL.create_weather()
end

LEVEL.name = "TESTLEVEL"

return LEVEL
