local LEVEL = {}
InitNewGame()
dofile("levels/pear15mountains_opening.lua")

CONFIG.backcolor_r = 0.000000
CONFIG.backcolor_g = 0.000000
CONFIG.backcolor_b = 0.000000
LoadConfig()

StopBackMusic()

local char1
local char2

local current_phase

local function end_this()
	if current_phase then
		StopThread( current_phase )
	end
	RemoveSkipKey()
	local obj1 = CreateItem( "generic-trigger", -1000, -1000 )
	ObjectPushInt( obj1, 0 )
	SetObjAnim( obj1, "script", false )
end

local function start( what )
	local thread = NewThread( what )
	current_phase = thread
	Resume( thread )
end

local function phase2()
	SetCamBounds( 371, 371+640, -251, -251+480 )
	CamMoveToPos( 371+320, -251+240 )
	SetObjPos( char1, 545, 12 )
	char1:sprite_mirrored( false )
	char1:vel( char1:acc( {x = 0, y = 0} ) )
	if char2 then
		SetObjPos( char2, 645, 12 )
		char2:vel( char2:acc( {x = 0, y = 0} ) )
	end
	Wait( 1000 )
	mapvar.tmp.fade_complete = false
	Game.fade_in()
	while not mapvar.tmp.fade_complete do
		Wait(1)
	end
	Wait( 500 )
	local function char1_talk()
		mapvar.tmp.talk = mapvar.tmp.talk or {}
		mapvar.tmp.talk[char1] = true
	end
	local function char2_talk()
		mapvar.tmp.talk = mapvar.tmp.talk or {}
		mapvar.tmp.talk[char2] = true
	end
	local function conversation_end()
		Resume( NewMapThread( function()
			mapvar.tmp.fade_complete = false
			Game.fade_out()
			while not mapvar.tmp.fade_complete do
				Wait(1)
			end
			SetCamUseBounds( false )
			CamMoveToPos( -9000, -9000 )
			end_this()
		end ))
	end
	Conversation.create({
		function( var ) 
			if mapvar.actors[2] then
				return "BOTH-PLAYERS"
			elseif mapvar.actors[1].info.character == "pear15unyl" then
				return "UNYL-SINGLE"
			end
		end,
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_MOUNTAINS_8",
		function()
			mapvar.tmp.talk[char1] = "transmission"
			SetObjAnim( char1, "idle", true )
			mapvar.tmp.talk[char1] = "transmission"
		end,
		"PEAR15_MOUNTAINS_9",
		"PEAR15_MOUNTAINS_9.5",
		"PEAR15_MOUNTAINS_10",
		"PEAR15_MOUNTAINS_10.5",
		function()
			mapvar.tmp.talk[char1] = nil
		end,
		"PEAR15_MOUNTAINS_11",
		conversation_end,
		nil,
		{ type = "LABEL", "UNYL-SINGLE" },
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_MOUNTAINS_12",
		char1_talk,
		"PEAR15_MOUNTAINS_13",
		char1_talk,
		"PEAR15_MOUNTAINS_14",
		char1_talk,
		"PEAR15_MOUNTAINS_15",
		conversation_end,
		nil,
		{ type = "LABEL", "BOTH-PLAYERS" },
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_MOUNTAINS_16",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_MOUNTAINS_17",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_MOUNTAINS_18",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_MOUNTAINS_19",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_MOUNTAINS_20",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_MOUNTAINS_21",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_MOUNTAINS_22",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_MOUNTAINS_23",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_MOUNTAINS_24",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_MOUNTAINS_25",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_MOUNTAINS_26",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_MOUNTAINS_27",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_MOUNTAINS_28",
		conversation_end,
		nil,
	}):start()
end

local function phase1()
	Game.fade_out()
	Wait(1)
	SetSkipKey( end_this, 300, { CONFIG.key_conf[1].gui_nav_decline, CONFIG.key_conf[1].gui_nav_menu, CONFIG.key_conf[2].gui_nav_decline, CONFIG.key_conf[2].gui_nav_menu } )
	SetCamBounds( -384, -384+640, -256, -256+480 )
	SetCamUseBounds( true )
	CamMoveToPos( -384+320, -256+240 )
	EnablePlayerControl( false )
	Menu:lock()
	Game.info.can_join = false
	char1 = GetPlayerCharacter( 1 )
	char2 = GetPlayerCharacter( 2 )
	if char2 and mapvar.actors[1].info.character == "pear15unyl" then
		char1, char2 = char2, char1
	end
	mapvar.tmp.fade_complete = false
	Game.fade_in()
	while not mapvar.tmp.fade_complete do
		Wait(1)
	end
	Wait( 500 )
	
	SetObjPos( char1, 15, 90 )
	Wait( 500 )
	char1:acc( { x = -1.75, y = 0 } )
	local done = false
	SetObjProcessor( char1, function( this )
		local pos = this:aabb().p
		if pos.x <= -88 then
			this:acc( { x = 0, y = 0 } )
			SetObjAnim( this, "stop", false )
			if char2 then
				SetObjPos( char2, 15, 90 )
				char2:sprite_mirrored( true )
			end
			done = true
			SetObjProcessor( char1, function() end )
		end
	end )
	while not done do
		Wait(1)
	end
	local function char1_talk()
		mapvar.tmp.talk = mapvar.tmp.talk or {}
		mapvar.tmp.talk[char1] = true
	end
	local function char2_talk()
		mapvar.tmp.talk = mapvar.tmp.talk or {}
		mapvar.tmp.talk[char2] = true
	end
	local function conversation_end()
		char1:acc( {x = -1.75, y = 0} )
		if char2 then
			char2:acc( {x = -1.75, y = 0} )
		end
		Resume( NewMapThread( function()
			Wait( 1500 )
			mapvar.tmp.fade_complete = true
			Game.fade_out()
			while not mapvar.tmp.fade_complete do
				Wait(1)
			end
			start( phase2 )
		end ))
	end
	Conversation.create({
		function( var ) 
			if mapvar.actors[2] then
				return "BOTH-PLAYERS"
			elseif mapvar.actors[1].info.character == "pear15unyl" then
				return "UNYL-SINGLE"
			end
		end,
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_MOUNTAINS_1",
		char1_talk,
		"PEAR15_MOUNTAINS_2",
		conversation_end,
		nil,
		{ type = "LABEL", "UNYL-SINGLE" },
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_MOUNTAINS_3",
		char1_talk,
		"PEAR15_MOUNTAINS_4",
		conversation_end,
		nil,
		{ type = "LABEL", "BOTH-PLAYERS" },
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_MOUNTAINS_5",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_MOUNTAINS_6",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_MOUNTAINS_7",
		conversation_end,
		nil,
	}):start()
end

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	mapvar.tmp.cutscene = true
	SwitchLighting(true)
	local env = CreateEnvironment('default_environment', -9001, -9001)
	SetDefaultEnvironment(env)
	start( phase1 )
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local object
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	SetObjDead( id )
	mapvar.actors[1].info.track = "mountain1"
	Loader.ChangeLevel( "pear15hoverbike" )
--$(MAP_TRIGGER)-
end

LEVEL.spawnpoints = {{44,411},}

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'pear15mount_open'

return LEVEL
