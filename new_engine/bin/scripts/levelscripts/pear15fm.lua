--EDITABLE MAP

local LEVEL = {}
InitNewGame()
dofile("levels/pear15fm.lua")

CONFIG.backcolor_r = 0.280000
CONFIG.backcolor_g = 0.280000
CONFIG.backcolor_b = 0.280000
LoadConfig()

PlayBackMusic( "music/iie_whbt.it" )

function DoorTo( coord1, coord2 )
	if not mapvar.tmp.doors then mapvar.tmp.doors = {} end
	if not mapvar.tmp.timedoors then mapvar.tmp.timedoors = 0 end
	local on_enter = function( ud, x, y, material, this )
		if ud:type() == constants.objPlayer then
			if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
			if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
			mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
		end
	end
	local on_leave = function( ud, x, y, material, new_material, this )
		if ud:type() == constants.objPlayer then
			if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
		end
	end
	local function on_use( coord )
		return function( ud, x, y, this )
			if not mapvar.tmp.doors[this] then mapvar.tmp.doors[this] = {} end
			local pl1, pl2 = GetPlayerCharacter(1), GetPlayerCharacter(2)
			if ud == pl1 then
				if (mapvar.tmp.doors.last_use1 or 0) + 300 >= Loader.time then
					return
				else
					mapvar.tmp.doors.last_use1 = Loader.time
				end
			elseif ud == pl2 then
				if (mapvar.tmp.doors.last_use2 or 0) + 300 >= Loader.time then
					return
				else
					mapvar.tmp.doors.last_use2 = Loader.time
				end
			end
			if pl1 and pl2 and pl1:object_present() and pl2:object_present() then
				local p1 = pl1:aabb().p
				local p2 = pl2:aabb().p
				local dist = math.sqrt( math.pow( p2.x - p1.x, 2 ) + math.pow( p2.y - p1.y, 2 ) )
				if dist <= 100 and mapvar.tmp.doors[this].actor == nil then
					GlobalSetKeyReleaseProc( nil )
					mapvar.tmp.doors.last_use1 = Loader.time
					mapvar.tmp.doors.last_use2 = Loader.time
					if ( mapvar.tmp[pl1] and mapvar.tmp[pl1].effect ) then SetObjDead( mapvar.tmp[pl1].effect ) mapvar.tmp[pl1].effect = nil end
					if ( mapvar.tmp[pl2] and mapvar.tmp[pl2].effect ) then SetObjDead( mapvar.tmp[pl2].effect ) mapvar.tmp[pl2].effect = nil end
					SetObjPos( pl1, coord[1]+55-15, coord[2]+40)
					SetObjPos( pl2, coord[1]+55+15, coord[2]+40)
					mapvar.tmp.caves_used = (mapvar.tmp.caves_used or 0) + 1
					--SetCamUseBounds( false )
					--CamMoveToPos(  coord[1]+55, coord[2]+40, true )
				elseif mapvar.tmp.doors[this].actor == nil then
					mapvar.tmp.doors[this].actor = ud
					RemoveKeyProcessors()
					if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
					if ud == pl1 then
						EnablePlayerControl( false, 1 )
						SetObjInvisible( ud, true )
						ud:solid_to(constants.physSprite+constants.physEnvironment)
						ud:drops_shadow(false)
						SetObjPos(ud,this:aabb().p.x,this:aabb().p.y)
						GlobalSetKeyReleaseProc( function( key )
							if key == CONFIG.key_conf[1].player_use and (mapvar.tmp.doors.last_use1 + 100) < Loader.time then
								GlobalSetKeyReleaseProc( nil )
								mapvar.tmp.doors.last_use1 = Loader.time
								mapvar.tmp.doors[this].actor = nil
								ud:solid_to(constants.physEverything)
								ud:drops_shadow( true )
								SetObjInvisible( ud, false )
								if not mapvar.tmp[ud].effect then
									mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
								end
								EnablePlayerControl( true, 1 )
								RestoreKeyProcessors()
							end
						end )
					elseif ud == pl2 then
						EnablePlayerControl( false, 2 )
						SetObjInvisible( ud, true )
						ud:solid_to(constants.physSprite+constants.physEnvironment)
						ud:drops_shadow(false)
						SetObjPos(ud,this:aabb().p.x,this:aabb().p.y)
						GlobalSetKeyReleaseProc( function( key )
							if key == CONFIG.key_conf[2].player_use and (mapvar.tmp.doors.last_use2 + 100) < Loader.time then
								GlobalSetKeyReleaseProc( nil )
								mapvar.tmp.doors.last_use2 = Loader.time
								mapvar.tmp.doors[this].actor = nil
								ud:solid_to(constants.physEverything)
								ud:drops_shadow( true )
								SetObjInvisible( ud, false )
								if not mapvar.tmp[ud].effect then
									mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
								end
								EnablePlayerControl( true, 2 )
								RestoreKeyProcessors()
							end
						end )
					else
						Log("Error!")
					end
					return
				else
					GlobalSetKeyReleaseProc( nil )
					RestoreKeyProcessors()
					if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
					EnablePlayerControl( true )
					SetObjInvisible( mapvar.tmp.doors[this].actor, false )
					mapvar.tmp.doors[this].actor:solid_to(constants.physEverything)
					mapvar.tmp.doors[this].actor:drops_shadow(true)
					SetObjInvisible( ud, false )
					ud:solid_to(constants.physEverything)
					ud:drops_shadow(true)
					SetObjPos( ud, coord[1]+55-15, coord[2]+40)
					SetObjPos( mapvar.tmp.doors[this].actor, coord[1]+55+15, coord[2]+40)
					mapvar.tmp.caves_used = (mapvar.tmp.caves_used or 0) + 1
					--SetCamUseBounds( false )
					--CamMoveToPos(  coord[1]+55, coord[2]+40, true )
					mapvar.tmp.doors[this].actor = nil
				end
			else
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				SetObjPos( ud, coord[1]+55, coord[2]+40)
				mapvar.tmp.caves_used = (mapvar.tmp.caves_used or 0) + 1
				--SetCamUseBounds( false )
				--CamMoveToPos(  coord[1]+55, coord[2]+40, true )
			end
		end
	end
	
	local on_use1 = on_use( coord1 )
	local on_use2 = on_use( coord2 )
	
	local env = CreateEnvironment("default_environment", coord1[1], coord1[2])
	GroupObjects( env, CreateEnvironment( "default_environment", coord1[1]+110, coord1[2]+120 ) )
	SetEnvironmentStats( env, {
		on_enter = on_enter,
		on_leave = on_leave,
		on_use = on_use2
	} )
	env = CreateEnvironment("default_environment", coord2[1], coord2[2])
	GroupObjects( env, CreateEnvironment( "default_environment", coord2[1]+110, coord2[2]+120 ) )
	SetEnvironmentStats( env, {
		on_enter = on_enter,
		on_leave = on_leave,
		on_use = on_use1
	} )
end

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	Game.fade_out()
	mapvar.weapons[2] = true
	SetCamBounds( -288, 7615, -256, 224 )
	SetCamUseBounds( true )
	CamMoveToPos( -184, 4 )
	mapvar.tmp.fade_widget_text = {}
	local sx, sy = GetCaptionSize( "default", "������� 4" )
	local text1 = CreateWidget( constants.wt_Label, "TEXT", fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 - 1.5 * sy, 1, 1 )
	mapvar.tmp.fade_widget_text["text1"] = text1
	WidgetSetCaptionColor( text1, {1,1,1,1}, false )
	WidgetSetCaption( text1, "������� 4" )
	WidgetSetZ( text1, 1.05 )
	sx, sy = GetCaptionSize( "default", "���������� �����" )
	local text2 = CreateWidget( constants.wt_Label, "TEXT", fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 + 1.5 * sy, 1, 1 )
	mapvar.tmp.fade_widget_text["text2"] = text2
	WidgetSetCaptionColor( text2, {1,1,1,1}, false )
	WidgetSetCaption( text2, "���������� �����" )
	WidgetSetZ( text2, 1.05 )
	SetCamAttachedAxis(true, true)
	SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
	SetCamObjOffset(0, 60)
	SetCamLag(0.9)
	Game.AttachCamToPlayers( {
		{ {-99999,-99999,7615,99999},  {-288,-256, 7615, 224} },
		{ {  7800,-99999,7615,99999},  {7800,-256, 7615, 224} },
		{ {-99999,-99999,99999,99999}, {7800,-256, 12938, 224} },
	} )
	local env = CreateEnvironment('default_environment', -9001, -9001)
	SetDefaultEnvironment(env)
	Game.info.can_join = false
	mapvar.tmp.cutscene = true
		
	EnablePlayerControl( false )
	Menu.lock()
	local start_intro_thread = false
	local fade_thread = NewThread( function()
		Wait( 1 )
		GUI:hide()
		Wait( 1000 )
		SetOnPlayerDeathProcessor(Game.onPlayerDeath)
		SetObjPos( GetPlayerCharacter(1), -184, -4 )
		if Game.actors[2] then
			SetObjPos( GetPlayerCharacter(2), -214, -4 )
		end
		mapvar.tmp.fade_complete = false
		Game.fade_in( false, "FADED" )
		WaitForEvent( "FADED" )
		local conversation_end = function()
			mapvar.tmp.cutscene = false
			EnablePlayerControl( true )
			SetCamLag(0.9)
			Menu.unlock()
			GUI:show()
			Game.info.can_join = true
		end
		local char1, char2, char1_talk, char2_talk = Game.GetCharacters()
		Conversation.create{
			function( var ) 
				if mapvar.actors[2] then
					return "BOTH-PLAYERS"
				elseif mapvar.actors[1].info.character == "pear15unyl" then
					return "UNYL-SINGLE"
				end
			end,
			{ sprite = "portrait-soh" },
			char1_talk,
			"PEAR15_MOUNTAINS_29",
			conversation_end,
			nil,
			{ type = "LABEL", "UNYL-SINGLE" },
			{ sprite = "portrait-unyl" },
			char1_talk,
			"PEAR15_MOUNTAINS_30",
			conversation_end,
			nil,
			{ type = "LABEL", "BOTH-PLAYERS" },
			{ sprite = "portrait-unyl" },
			char2_talk,
			"PEAR15_MOUNTAINS_31",
			{ sprite = "portrait-soh" },
			char1_talk,
			"PEAR15_MOUNTAINS_32",
			{ sprite = "portrait-unyl" },
			char2_talk,
			"PEAR15_MOUNTAINS_33",
			{ sprite = "portrait-soh" },
			char1_talk,
			"PEAR15_MOUNTAINS_34",
			{ sprite = "portrait-unyl" },
			char2_talk,
			"PEAR15_MOUNTAINS_35",
			conversation_end,
			nil,
		}:start()
	end )
		
	Resume( fade_thread )
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local obj1 = CreateItem( 'generic-death-area', -454, 225 )
	local obj2 = CreateItem( 'generic-death-area', 7613, 462 )
	GroupObjects( obj1, obj2 )
	local obj3 = CreateItem( 'generic-death-area', 7744, 268 )
	local obj4 = CreateItem( 'generic-death-area', 13313, 513 )
	GroupObjects( obj3, obj4 )
	local obj5 = CreateItem( 'generic-trigger', 12960, -32 )
	local obj6 = CreateItem( 'generic-trigger', 13345, 225 )
	GroupObjects( obj5, obj6 )
	ObjectPushInt( obj5, 1 )
	local object
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	SetObjDead( id )
	if trigger == 1 then
		Game.ShowLevelResults("pear15fm_boss")
	end
--$(MAP_TRIGGER)-
end


LEVEL.spawnpoints = {{-999, -999}}

DoorTo({7441, 118}, {7966, 123})

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'Thorough search'

return LEVEL
