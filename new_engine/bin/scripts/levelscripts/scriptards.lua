--EDITABLE MAP

local LEVEL = {}
InitNewGame()
dofile("levels/scriptards.lua")

CONFIG.backcolor_r = 0.280000
CONFIG.backcolor_g = 0.280000
CONFIG.backcolor_b = 0.280000
LoadConfig()

StopBackMusic()

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	UpdateWeaponSlot( 1, 2, 
		{ set_name = "Classic gun", 
		reload_time = 200, clip_reload_time = 1000, shots_per_clip = 12, ammo_per_shot = 1, recoil = 5,
		color = { .8, .8, 1, 1 };
		bullet_param = 
			{
				{
					type = "energy";
					look = "custom/energy1";
					color = { .8, .8, 1, 1 };
					damage = 1;
					vel = 8;
					flare = "circle";
					flare_size_x = 64;
					flare_size_y = 32;
					flare_color = { .3, .3, 1, .25 };
				}
			},
		bullets =
		{
			{
				num = 1,
				pos = {0, 0},
				ang = "rnd(-2,2)"
			},
			{
				num = 1,
				pos = {0, 0},
				ang = "rnd(-2,2)"
			},
			{
				num = 1,
				pos = {0, 0},
				ang = "rnd(-2,2)"
			},
			{
				num = 1,
				pos = {0, 0},
				ang = "rnd(-2,2)"
			},
			{
				num = 1,
				pos = {0, 0},
				ang = "rnd(-2,2)"
			},
		},
		explosions = {}
	})
	mapvar.weapon = deep_copy( mapvar.player_info[1].weapons[2].param )
	SwitchWeapon( nil, true )
	if GetPlayer() then
		SetCamAttachedObj( GetPlayer().id )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
		local env = CreateEnvironment('default_environment', -9001, -9001)
		SetDefaultEnvironment(env)	end
	SCRIPTARD_AWARNESS = 100
	SCRIPTARD_DIST = 250
	scriptard_ai = function( id )
		if not mapvar then mapvar = {} end
		if not mapvar.tmp then mapvar.tmp = {} end
		if not mapvar.tmp.obj then mapvar.tmp.obj = {} end
		local obj = GetObject( id )
		local pl = GetPlayer()
		local dist = math.sqrt( math.pow( obj.aabb.p.x-pl.aabb.p.x, 2 ) + math.pow( obj.aabb.p.y-pl.aabb.p.y, 2 ) )
		if not mapvar.tmp.obj[id] then mapvar.tmp.obj[id] = { action = "idle" } end
		local state = mapvar.tmp.obj[id]
		if state.widget then
			if state.widget_y < 55 then state.widget_y = state.widget_y + ( 60 - state.widget_y ) * 0.01 end
			local sx, sy = GetCaptionSize( "default", state.widget_text )
			if state.widget_pulsate then
				state.widget_scale = 2 + math.sin( Loader.time / 100 )/2
			end
			WidgetSetPos( state.widget, obj.aabb.p.x-(sx/2)*state.widget_scale, obj.aabb.p.y - state.widget_y - (sy/2)*state.widget_scale )
			WidgetSetCaptionFont( state.widget, "default", state.widget_scale )
		end
		if not obj.health or obj.health <= 0 then
			if state.widget then
				DestroyWidget( state.widget )
			end
			return
		end
		if state.action == "idle" then
			state.vel = 0
			if dist < SCRIPTARD_DIST then
				if pl.aabb.p.x < obj.aabb.p.x then
					SetObjSpriteMirrored( id, true )
				else
					SetObjSpriteMirrored( id, false )
				end
				SetDynObjVel( id, 0, obj.vel.y )
				SetObjAnim( id, "idle", false )
				state.awarness = (state.awarness or 0) + 1
				if state.awarness > SCRIPTARD_AWARNESS then
					SetObjAnim( id, "move", false )
					WidgetSetCaption( state.widget, "!!!" )
					state.widget_text = "!!!"
					state.widget_pulsate = true
					chase = ( chase or 0 ) + 1
					if chase == 1 then
						PlaySnd( "sync-siren.ogg", false )
					end
					state.action = "chase"
				elseif state.awarness == 1 then
					state.widget = CreateWidget( constants.wt_Label, "speech", nil, obj.aabb.p.x-15, obj.aabb.p.y, 1, 1)
					state.widget_scale = 0.01
					WidgetSetFixedPosition( state.widget, false )
					WidgetSetCaption( state.widget, "WTF?" )
					state.widget_text = "WTF?"
					state.widget_pulsate = false
					state.widget_y = 20
					WidgetSetCaptionColor( state.widget, {1, 1, 1, 1}, false, {0,0,0,1} )
				else
					WidgetSetCaptionColor( state.widget, {1, 1-(state.awarness/SCRIPTARD_AWARNESS), 1-(state.awarness/SCRIPTARD_AWARNESS), 1}, false, {0,0,0,1} )
					state.widget_scale = 1.25*state.awarness/SCRIPTARD_AWARNESS
				end
			elseif state.awarness then
				SetDynObjVel( id, 0, obj.vel.y )
				SetObjAnim( id, "idle", false )
				state.awarness = state.awarness - 1
				if state.awarness <= 0 then
					state.awarness = nil
					DestroyWidget( state.widget )
					state.widget = nil
					chase = chase - 1
				else
					WidgetSetCaptionColor( state.widget, {1, 1-(state.awarness/SCRIPTARD_AWARNESS), 1-(state.awarness/SCRIPTARD_AWARNESS), 1}, false, {0,0,0,1} )
				end
			else
				if not state.boredom then state.boredom = 0 end
				if not state.moving then
					state.boredom = state.boredom + math.random(1, 10)
					if state.boredom > 300 and math.random(1, 10) > 5 then
						state.moving = true
						SetObjAnim( id, "move", false  )
						if math.random(1, 10) > 5 then
							SetObjSpriteMirrored( id, true )
							SetDynObjAcc( id, -0.4, obj.acc.y )
						else
							SetObjSpriteMirrored( id, false )
							SetDynObjAcc( id, 0.4, obj.acc.y )
						end
					end
				else
					state.boredom = state.boredom - 1
					if state.boredom <= 0 then
						state.moving = false
						SetObjAnim( id, "idle", false )
						SetDynObjAcc( id, 0, obj.acc.y )
					end
				end
			end
		elseif state.action == "chase" then
			if pl.aabb.p.x < obj.aabb.p.x then
				SetObjSpriteMirrored( id, true )
			else
				SetObjSpriteMirrored( id, false )
			end
			if dist > SCRIPTARD_DIST then
				SetDynObjVel( id, 0, obj.vel.y )
				SetObjAnim( id, "idle", false )
				WidgetSetCaption( state.widget, "WTF?" )
				WidgetSetCaptionFont( state.widget, "default", 1.0 )
				state.widget_text = "WTF?"
				state.widget_scale = 1
				state.widget_y = 20
				state.widget_pulsate = false
				state.action = "idle"
			elseif dist < 50 then
				SetDynObjVel( id, 0, obj.vel.y )
				SetObjAnim( id, "attack", false )
				state.action = "attack"
			else
				if math.random( 1, 10 ) > 2 then
					if pl.aabb.p.x < obj.aabb.p.x then
						state.vel = math.min( state.vel * 1.1, state.vel - 0.1 )
						if state.vel < -2 then 
							state.vel = -2
						end
					else
						state.vel = math.max( state.vel * 1.1, state.vel + 0.1 )
						if state.vel > 2 then 
							state.vel = 2
						end
					end
				end
				SetDynObjVel( id, state.vel, obj.vel.y )
			end
		elseif state.action == "attack" then
			state.vel = 0
			if obj.sprite.cur_anim ~= "attack" then
				state.action = "idle"
			end
		end
	end
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local obj1 = CreateEnemy( 'scriptard', 941, 84 )
	local obj2 = CreateEnemy( 'scriptard', 1169, 87 )
	local obj3 = CreateEnemy( 'scriptard', 1327, 82 )
	local object
	object = obj1
	if not Editor then
	--$(OBJ1_CREATION_SCRIPT)+
	SetObjProcessor( object, scriptard_ai )
	--$(OBJ1_CREATION_SCRIPT)-
	end
	object = obj2
	if not Editor then
	--$(OBJ2_CREATION_SCRIPT)+
	SetObjProcessor( object, scriptard_ai )
	--$(OBJ2_CREATION_SCRIPT)-
	end
	object = obj3
	if not Editor then
	--$(OBJ3_CREATION_SCRIPT)+
	SetObjProcessor( object, scriptard_ai )
	--$(OBJ3_CREATION_SCRIPT)-
	end
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
--$(MAP_TRIGGER)-
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'scriptards'

return LEVEL
