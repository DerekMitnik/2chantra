--EDITABLE MAP

local LEVEL = {}
InitNewGame()
dofile("levels/testlevel_dummy_obj.lua")

CONFIG.backcolor_r = 0.280000
CONFIG.backcolor_g = 0.280000
CONFIG.backcolor_b = 0.280000
LoadConfig()

StopBackMusic()

objud = nil
object = nil

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	if GetPlayer() then
		SetCamAttachedObj( GetPlayer().id )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
		local env = CreateEnvironment('default_environment', -9001, -9001)
		SetDefaultEnvironment(env)	end
--$(INIT_SCRIPT)-
	PlayBackMusic( "music/iie_w2gh.it" )
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+

-- for i = 1, 1000 do
	-- local id = GetObject(object).id
	-- local id = objud:id()
-- end
	--Log(objud:aabb().p.x, objud:aabb().p.y)
	--Log(GetObject(object).aabb.p.x, GetObject(object).aabb.p.y)
--Log(id)

--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	--local object
	
	local funcref = RegisterFunction(function (ud) Log("Hi from processor of ", tostring(ud)); end)
	Log(funcref)
	
	object = CreateDummyItem()
	local t = {
		name = "vegetable1";
		trajectory_type = constants.pttGlobalSine;
		trajectory_param1 = 0.5;
		trajectory_param2 = 0.05;
		physic = 1;
		phys_solid = 0;
		phys_bullet_collidable = 0;
		phys_max_x_vel = 0;
		phys_max_y_vel = 80;
		FunctionName = "CreateItem";
		-- Описание спрайта
		--LoadTexture("vegetable1.png");
		texture = "vegetable1";
		z = -0.001;
		image_width = 256;
		image_height = 128;
		frame_width = 32;
		frame_height = 32;
		frames_count = 19;

		animations = 
		{
			{ 
				-- Создание
				name = "init";
				frames = 
				{
					{ com = constants.AnimComSetProcessor; param = funcref },
					{ com = constants.AnimComRealX; param = 8 },
					{ com = constants.AnimComRealY; param = 9 },
					{ com = constants.AnimComRealW; param = 16 },
					{ com = constants.AnimComRealH; param = 15 },
					{ com = constants.AnimComSetTouchable; param = 1 },
					--{ dur = 100; num = 20; com = constants.AnimComCreateParticles; txt = "pteleport" },
					{ com = constants.AnimComSetAnim; txt = "idle" }	
				}
			},
			{ 
				-- Вращение
				name = "idle";
				frames = 
				{
			
					{ dur = 50; num = 0 },
					{ dur = 50; num = 1 },
					{ dur = 50; num = 2 },
					{ dur = 50; num = 3 },
					{ dur = 50; num = 4; com = constants.AnimComSetZ; param = function (ud) 
						local z = ud:sprite_z() * 1000; 
						Log(z)
						if z < 1000 then 
							z = z + 50; 
						else 
							z = 1000; 
						end 
						Log(z)
						return z; 
					end },
					{ dur = 50; num = 5 },
					{ dur = 50; num = 6 },
					{ dur = 50; num = 7 },
					{ dur = 50; num = 8 },
					{ dur = 50; num = 9 },
					{ dur = 50; num = 10 },
					{ dur = 50; num = 11 },
					{ dur = 50; num = 12 },
					{ dur = 50; num = 13 },
					{ dur = 50; num = 14 },
					{ dur = 50; num = 15 },
					{ dur = 50; num = 16 },
					{ dur = 50; num = 17 },
					{ dur = 50; num = 18 },
					{ com = constants.AnimComLoop }	
				}
			},
			{ 
				-- Бонус
				name = "touch";
				frames = 
				{
					{ com = constants.AnimComGiveHealth; param = 10 },
					{ com = constants.AnimComJumpIfPlayerId; param = 3 },
					{ com = constants.AnimComDestroyObject },
					{ dur = 100 },
					{ com = constants.AnimComPlaySound; txt = "health-pickup.ogg" },
					{ com = constants.AnimComCallFunction; txt = "StabilizeSync"; param = 1 },
					{ com = constants.AnimComDestroyObject }
				}
			}
			
		}
	}
	
	proto = LoadPrototypeTable(t);
	--proto = LoadPrototypeTable(t, "item");
	ApplyProto(object, proto)
	
	objud = GetObjectUserdata(tostring(object))
	SetObjAnim(objud, "init", true)
	Log(objud)
	--Log(objud:id(28))
	Log(objud:sprite_z(0.28))
	Log(objud:material_friction())
	
	col = objud:sprite_color()
	col[4] = 0.5
	objud:sprite_color(col)
	
	Log(objud:sprite_visible())
	Log(objud:sprite_visible(true))
	Log(objud:sprite_visible(false))
	
	--Log(objud:sprite_color(), objud:sprite_color()[1], objud:sprite_color()[2], objud:sprite_color()[3], objud:sprite_color()[4])
		
	AddTimerEvent(3000, function () Log("killing"); Log(objud:sprite_visible(true)) end)
	--AddTimerEvent(4000, function () Log("accessing"); Log(objud:id()) end)
	
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
--$(MAP_TRIGGER)-
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'testlevel_dummy_obj'

return LEVEL
