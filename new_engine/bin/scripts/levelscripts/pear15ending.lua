local credits = [[{color=0.2,1,0.25,1}iiChantra P.E.A.R:
{color=0.9,0.7,0.1,1}������ ������� ������





�������� ����

{font=small}ktd



�������

{font=small}_tsu
{font=small}ktd
{font=small}Anonymous0028
{font=small}dobropoke
{font=small}������ �����



������

{font=small}kernel_bug
{font=small}dobropoke
{font=small}ktd


{font=small}�������� ����������:


{font=small}Satana (����������)
{font=small}ndr (������ ������)



�������

{font=small}ktd
{font=small}dobropoke



�����

{font=small}ktd



��������

{font=small}ktd
{font=small}dobropoke



������

{font=small}vibe_crc
{font=small}zavok



�����

{font=small}zavok
{font=small}ktd
{font=small}freesound.org



������

{font=small}ktd
{font=small}dobropoke



�������������� ����

{font=small}Anonymous0028



��� �������� ������ ������������
������ � ����� freesound.org
����������:

{font=small}FreqMan
{font=small}guitarguy1985
{font=small}Jamius
{font=small}smidoid
{font=small}berzins1
{font=small}Pogotron
{font=small}m_O_m
{font=small}kijjaz
{font=small}ReWired
{font=small}acclivity
{font=small}dobroide
{font=small}VEXST
{font=small}CGEffex

������� �� ��������� ����!


{font=small}{color=0.7,0.7,1,1}http://iichantra.ru
{font=small}{color=0.7,0.7,1,1}https://twitter.com/iichantra


��������� �������

{font=small}Lolwoot �� ����������� ������ ����
{font=small}���-��� �� iichan.ru
{font=small}������� � ������ #iichantra �� ��, ��� �� ������
{font=small}�������� �������� �� ������� ��������������� ��������



� ����� �����, �������� ������� ����� � �������� � �����
� ���������� ������� ��������� �����

{font=small}ANONYA
{font=small}Agnonymous
{font=small}AkashiyaMoka
{font=small}AlexPancho
{font=small}Anon384592843
{font=small}Anonim7031
{font=small}Aphelios
{font=small}Araragi-kun
{font=small}Artemiy
{font=small}AyamuNakao
{font=small}AyaoTanaka
{font=small}Bldzhad
{font=small}Chiaki-hime
{font=small}Chudd
{font=small}CorpusColibri
{font=small}Coyc
{font=small}Crouching_Tower
{font=small}Desufag-kun
{font=small}DoubleSkinFacade
{font=small}FifthAngel
{font=small}GhostBear28366
{font=small}Guest
{font=small}IkaMusume
{font=small}JJ-chan
{font=small}KawaiiChainSword
{font=small}LLykyHe
{font=small}Lenin-kun
{font=small}Lind
{font=small}Lua_00
{font=small}Mamigo
{font=small}Manul
{font=small}MarioMario
{font=small}MecheslaV
{font=small}Meiling
{font=small}Mezriss
{font=small}MizoreSH
{font=small}Name63
{font=small}NoobfonLamest
{font=small}Not_that_Jesus
{font=small}Nyanko-tan
{font=small}Retardin45
{font=small}Shiki-san
{font=small}Sklerosis-Kun
{font=small}Slowstorm
{font=small}Urist_McDorf
{font=small}VBW
{font=small}Viruzzz-kun
{font=small}WildWFox
{font=small}Wishenka
{font=small}artemka
{font=small}brd
{font=small}danst
{font=small}iSage
{font=small}kr214
{font=small}kure-ji-neko
{font=small}liquidkitten
{font=small}lol_captcha
{font=small}lolbot
{font=small}meiling
{font=small}muvi
{font=small}nerd
{font=small}novak
{font=small}ororoshenkiroro
{font=small}panzermeido
{font=small}qvi
{font=small}slow-chan
{font=small}slowcoder
{font=small}thatotheranon
{font=small}thordendal
{font=small}touhou-ru
{font=small}wtt4664
{font=small}z0rc
]]

local LEVEL = {}
LEVEL.NO_RESTART = true

InitNewGame()
dofile("levels/pear15ending.lua")

CONFIG.backcolor_r = 0.000000
CONFIG.backcolor_g = 0.000000
CONFIG.backcolor_b = 0.000000
LoadConfig()

StopBackMusic()

local skip_credits
local speedup_credits
local story4

function ShowCrawl( text, after_event )
	local text = explode((text or ""), "\n")
	crawl_widgets = {}
	local font, color
	local sx, sy
	local r,g,b,a
	local y = CONFIG.scr_height + 1
	local w
	local username = {}
	local score = {}
	for i=1,#text do
		font = string.match( text[i], "^{font=([^}]+)}" )
		if font == "small" then
			font = "dialogue"
		end
		font = font or "default"
		color = string.match( text[i], "{color=([^}]+)}" )
		color = color or "1,1,1,1"
		r,g,b,a = string.match( color, "([^,]+),([^,]+),([^,]+),([^,]+)" )
		r,g,b,a = tonumber(r),tonumber(g),tonumber(b),tonumber(a)
		color = { r, g, b, a }
		text[i] = string.gsub( text[i], "{[^}]+}", "" )
		text[i] = string.gsub( text[i], "%%username%%", username )
		text[i] = string.gsub( text[i], "%%score%%", score )
		if text[i] == "" then
			sx, sy = GetCaptionSize( font, "I" )
		else
			sx, sy = GetCaptionSize( font, text[i] )
		end
		w = CreateWidget( constants.wt_Label, "crawl", nil, (CONFIG.scr_width-sx)/2, y, 1, 1 )
		WidgetSetCaptionColor( w, color, false, {0,0,0,1} )
		WidgetSetCaptionFont( w, font )
		WidgetSetCaption( w, text[i] )
		table.insert( crawl_widgets, { (CONFIG.scr_width-sx)/2, y, w } )
		y = y + sy + 5
	end

	local skip
	local thread = NewMapThread( function()
		local last_time = Loader.time
		local dt = 0
		while #crawl_widgets > 0 and not skip do
			dt = Loader.time - last_time
			last_time = Loader.time
			for key, value in pairs( crawl_widgets ) do
				value[2] = value[2] - 0.2 * iff( speedup_credits, dt, dt / 10 )
				WidgetSetPos( value[3], math.floor(value[1]), math.floor(value[2]) )
				if value[2] < -10 then
					DestroyWidget( value[3] )
					table.remove( crawl_widgets, key )
				end
			end
			Wait(1)
		end
		if #crawl_widgets > 0 then
			for key, value in pairs( crawl_widgets ) do
				DestroyWidget( value[3] )
			end
		end
		RestoreKeyProcessors()
		after_event()
	end )
	Resume( thread )

	RemoveKeyProcessors()
	GlobalSetKeyDownProc( function( key )
		if key == CONFIG.key_conf[1].gui_nav_accept or key == CONFIG.key_conf[1].fire or
		   key == CONFIG.key_conf[2].gui_nav_accept or key == CONFIG.key_conf[2].fire then
			speedup_credits = true
		end
	end )
	GlobalSetKeyReleaseProc( function( key )
		if key == CONFIG.key_conf[1].gui_nav_decline or key == CONFIG.key_conf[1].gui_nav_menu or
		   key == CONFIG.key_conf[2].gui_nav_decline or key == CONFIG.key_conf[2].gui_nav_menu then 
				skip = true
				skip_credits = true
		elseif key == CONFIG.key_conf[1].gui_nav_accept or key == CONFIG.key_conf[1].fire
		    or key == CONFIG.key_conf[2].gui_nav_accept or key == CONFIG.key_conf[2].fire then
				speedup_credits = false
		end
	end )
end

local prof
local char1, char2, char1_talk, char2_talk
local ending_type = Game.info.ending
local BAD_END = 0
local DOOM_END = 1
local LAB_END = 2
local badend = false

local function prof_talk1()
	SetObjAnim( prof, "talk", false )
end
local function prof_talk2()
	SetObjAnim( prof, "talk_fgsfds", false )
end

local current_phase

local function start( phase )
	current_phase = NewThread( phase )
	Resume( current_phase )
end

local function phase3()
	Wait( 500 )
	mapvar.tmp.fade_complete = false
	Game.fade_out()
	Playlist:stop()
	while not mapvar.tmp.fade_complete do
		Wait(1)
	end
	SetCamBounds( 1163, 1163 + 640, -128, -128 + 480 )
	CamMoveToPos( 1163 + 320, -128 + 240 )
	mapvar.tmp.fade_complete = false
	Game.fade_in()
	while not mapvar.tmp.fade_complete do
		Wait(1)
	end
	Wait( 1000 )
	if not badend then
		local btardis = GetObjectUserdata(CreateSprite( "btardis", 1514, 198 ))
		SetObjAnim( btardis, 11, false )
		SetObjSpriteColor( btardis, { 1, 1, 1, 0 } )
		local t = 0
		local last_time = Loader.time
		local dt = 0
		local last_sound = 0
		local dur = GetSndLength( "btardis.ogg" )
		PlaySnd( "btardis.ogg", true )
		Resume( NewMapThread( function()
			Wait( dur * 750 )
			PlaySnd( "btardis.ogg", true )
		end ))
		while t < 3.5 * math.pi do
			dt = Loader.time - last_time
			last_time = Loader.time
			t = t + dt / 500
			if t > last_sound + 2*math.pi then
				last_sound = t
			end
			SetObjSpriteColor( btardis, { 1, 1, 1, math.abs( math.sin(t) ) } )
			Wait(1)
		end
		SetObjSpriteColor( btardis, { 1, 1, 1, 1 } )
		Wait( 1000 )
		PlaySnd( "dun_dun_dun.ogg", true )
		local sx, sy = GetCaptionSize( "default", dictionary_string("����������� �������...") )
		local tbc = CreateWidget( constants.wt_Label, "", nil, CONFIG.scr_width - 2 * sx - 30, CONFIG.scr_height - 2 * sy - 10, 1, 1 )
		WidgetSetCaptionFont( tbc, "default", 2 )
		WidgetSetCaptionColor( tbc, { 1, 1, 1, 1 }, false, {0,0,0,1} )
		WidgetSetCaption( tbc, dictionary_string("����������� �������...") )
		Game.AddCleanup( function() DestroyWidget( tbc ) end )
		Wait( 5000 )
	end
	if difficulty > 1 then
		Game.ProgressAchievement( "I_BOT", 1 )
	end
	if not mapvar.actors[2] then
		Game.BitProgressAchievement( "DEJA_VU", iff( mapvar.actors[1].info.character == "pear15soh", 1, 2 ) )
	end
	if not badend then
		mapvar.tmp.goodend = true
	end
	Loader.state = 'game over'
	--Menu.showGameOver()
end

local function phase2()
	Wait( 500 )
	mapvar.tmp.fade_complete = false
	Game.fade_out()
	while not mapvar.tmp.fade_complete do
		Wait(1)
	end
	SetCamBounds( 471, 471 + 640, -233, -233 + 600 )
	CamMoveToPos( 471 + 320, -233 + 240 )
	mapvar.tmp.fade_complete = false
	Game.fade_in()
	while not mapvar.tmp.fade_complete do
		Wait(1)
	end
	Wait( 1000 )
	local done = false
	skip_credits = false
	StopBackMusic()
	Playlist:stop()
	Playlist:empty()
	Playlist.mid_pause = 3000
	Playlist:add("music/iie_wd.it", 1, 82000)
	Playlist:add("music/iie_whbt.it", 1, 81000)
	--Playlist:add("music/nsmpr_woom.it")
	--Playlist:add("music/iie_kmp.it")
	--Playlist:add("music/nsmpr_iwbtt.it")
	Playlist:play()
	speedup_credits = false
	ShowCrawl( credits, function() done = true end )
	Resume( NewMapThread( function()
		local last_time = Loader.time
		local dt = 0
		local y = -233 + 240
		while y < -233 + 600 - 240 do
			if skip_credits then

				return
			end
			dt = Loader.time - last_time
			last_time = Loader.time
			y = y + 1 * dt / 1000
			CamMoveToPos( 471 + 320, y )
			Wait(1)
		end
	end ))
	while not done do
		Wait(1)
	end
	start( phase3 )
end

local function start_phase2()
	start( phase2 )
end

local function conversation1()
	Conversation.create{
		function()
			Game.ProgressAchievement( "ENDING_THATGUY", 1 )
			Game.BitProgressAchievement( "ALL_ENDINGS", 1 )
			if mapvar.actors[2] then
				Game.ProgressAchievement( "MULTIPLAYER", 1 )
				return "BOTH-PLAYERS"
			elseif mapvar.actors[1].info.character == "pear15unyl" then
				return "UNYL-SINGLE"
			end
		end,
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_1",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_2",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_3",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_4",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_5",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_6",
		prof_talk2,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_7",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_8",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_9",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_10",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_11",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_12",
		start_phase2,
		nil,
		{ type = "LABEL", "UNYL-SINGLE" },
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_13",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_14",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_15",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_16",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_17",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_18",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_19",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_20",
		start_phase2,
		nil,
		{ type = "LABEL", "BOTH-PLAYERS" },
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_21",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_22",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_23",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_24",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_25",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_26",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_27",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_28",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_29",
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_30",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_31",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_32",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_33",
		start_phase2,
		nil,
	}:start()
end

local function conversation2()
	Conversation.create{
		function()
			Game.ProgressAchievement( "ENDING_DOOM", 1 )
			Game.BitProgressAchievement( "ALL_ENDINGS", 2 )
			if mapvar.actors[2] then
				Game.ProgressAchievement( "MULTIPLAYER", 1 )
				return "BOTH-PLAYERS"
			elseif mapvar.actors[1].info.character == "pear15unyl" then
				return "UNYL-SINGLE"
			end
		end,
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_34",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_35",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_36",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_37",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_38",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_39",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_40",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_41",
		prof_talk2,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_42",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_43",
		start_phase2,
		nil,
		{ type = "LABEL", "UNYL-SINGLE" },
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_44",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_45",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_46",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_47",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_48",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_49",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_50",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_51",
		start_phase2,
		nil,
		{ type = "LABEL", "BOTH-PLAYERS" },
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_52",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_53",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_54",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_55",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_56",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_57",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_58",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_59",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_60",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_61",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_62",
		start_phase2,
		nil,
	}:start()
end

local function conversation3()
	Conversation.create{
		function()
			Game.ProgressAchievement( "BAD_ENDING", 1 )
			Game.BitProgressAchievement( "ALL_ENDINGS", 3 )
			SetObjDead( story4 )
			badend = true
			if mapvar.actors[2] then
				Game.ProgressAchievement( "MULTIPLAYER", 1 )
				return "BOTH-PLAYERS"
			elseif mapvar.actors[1].info.character == "pear15unyl" then
				return "UNYL-SINGLE"
			end
		end,
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_63",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_64",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_65",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_66",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_67",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_68",

		prof_talk1,

		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_69",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_70",
		start_phase2,
		nil,
		{ type = "LABEL", "UNYL-SINGLE" },
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_71",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_72",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_73",
		char1_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_74",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_75",
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_76",
		start_phase2,
		nil,
		{ type = "LABEL", "BOTH-PLAYERS" },
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_77",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_78",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_79",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_80",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_81",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_82",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_83",
		char2_talk,
		{ sprite = "portrait-unyl" },
		"PEAR15_ENDING_84",
		prof_talk1,
		{ sprite = "portrait-prof" },
		"PEAR15_ENDING_85",
		char1_talk,
		{ sprite = "portrait-soh" },
		"PEAR15_ENDING_86",
		start_phase2,
		nil,
	}:start()
end

local function phase1()
	Game.fade_out()
	Wait(250)
	char1, char2, char1_talk, char2_talk = Game.GetCharacters()
	SetObjPos( char1, 331, 50.5 )
	mapvar.tmp.fade_complete = false
	prof = GetObjectUserdata( CreateEnemy( "prof", -136, 159 ) )
	Wait( 500 )
	Game.fade_in()
	while not mapvar.tmp.fade_complete do
		Wait(1)
	end
	Wait( 250 )
	local done = false
	if char2 then
		SetObjPos( char2, 438, 170 )
		char2:acc{ x = -1.75, y = 0 }
	end
	Game.MoveWithAccUntilX( char1, { x = -1.75, y = 0 }, 44, function()
		if char2 then
			char2:acc{ x = 0, y = 0 }
			SetObjAnim( char2, "stop", false )
		end
		SetObjAnim( char1, "stop", false )
		done = true
	end )
	while not done do
		Wait(1)
	end
	ending_type = Game.actors[1].info.ending
	if ending_type == LAB_END then
		start( conversation1 )
	elseif ending_type == DOOM_END then
		start( conversation2 )
	else
		start( conversation3 )
	end
end

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	mapvar.tmp.cutscene = true
	Game.info.can_join = false
	SetCamBounds( -369, -369 + 640, -205, -205 + 480 )
	SetCamUseBounds( true )
	CamMoveToPos( -369 + 320, -205 + 240 )
	EnablePlayerControl( false )
	Menu:lock()
	Resume( NewMapThread( function()
		Wait(1)
		start( phase1 )
	end ))
	local env = CreateEnvironment('default_environment', -9001, -9001)
	SetDefaultEnvironment(env)
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	story4 = CreateSprite( "story-background4", 791, 67 )
	SetObjPos( story4, 791, 67 )
	local object
--$(SPECIAL_CREATION)-
end


function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	SetObjDead( id )
	Loader.ChangeLevel( "pear15sewers" )
--$(MAP_TRIGGER)-
end

LEVEL.spawnpoints = {{331,50.5}}

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'Ending'

return LEVEL
