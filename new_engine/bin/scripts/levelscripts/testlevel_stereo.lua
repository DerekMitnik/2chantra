InitNewGame();


local LEVEL = {}
require("routines")
--TogglePause()

CONFIG.backcolor_r = 67/255;
CONFIG.backcolor_g = 153/255;
CONFIG.backcolor_b = 153/255;
LoadConfig();

local x = -48
local dx = 32
for i = 0,500 do
	if i % 5 > 1 or i > 20 then
		CreateSprite("phys_floor", x + dx*i, 350);
	end
end

for i = 1,3 do
	CreateSprite("phys_floor", -28, 350 - i*32);
end

function t()
	CreateEnemy ("btard2", GetMousePos() )
end

CreatePlayer("sohchan", 50, 300);
CreatePlayer("unylchan", 50, 300);

CreateItem("bonus-line", 200, 300)
CreateItem("bonus-diamond", 230, 300)
CreateItem("bonus-square", 260, 300)
CreateItem("bonus-triangle", 290, 300)

CreateSprite("release_sign", 500, 275)

function map_trigger( id, trigger )
	if ( trigger == 1 ) then
		Menu.showEndLevelMenu("testlevel_chain2")
	end
end

--[[
trigger = CreateItem("generic-trigger", 500, 200)
GroupObjects( trigger, CreateItem("generic-trigger", 700, 310) )
ObjectPushInt( trigger, 1 )
--]]

local env = CreateEnvironment("default_environment", -9001, -9001)
SetDefaultEnvironment(env)

SetCamAttachedObj(GetPlayer().id);		-- ������� ������ � �������
SetCamAttachedAxis(true, true);		-- ������ ��������� ������ �� ��� X
SetCamFocusOnObjPos(constants.CamFocusBottomCenter);	-- ������ ������ �� ��������� ������� ���� �������
-- ��� ����� �������� y-���������� ������� ���� �� ��������, ����� �������, ������ �� ������ ���, ��������, ����������
SetCamObjOffset(0, 60);	-- ������ �������� ������ ������������ �������

function pac()
	mapvar.pacman = CreateEnemy( "manpac-kb", -700, 100 )
	CreateEnemy( "btard2", 650, 300 )
	CreateEnemy( "slowpoke", 850, 300 )
	SetObjAnim( mapvar.pacman, "move_forward", false )
	Wait( 10000 )
	mapvar.pacman = nil
end

function keyp( butt )
	local pl = GetPlayer()
	local px, py = GetMP( pl.id, 0 )
	local function ang(a) return a end
	if pl.sprite.mirrored then 
		px = -px 
		ang = function(a) return -(a+180) end
	end

	if butt == keys.f then
		CreateBullet( "sfg9000", pl.aabb.p.x+px, pl.aabb.p.y+py, pl.id, pl.sprite.mirrored, 0, 0 )
	elseif butt == keys.r then
		CreateBullet( "sfg9000", pl.aabb.p.x+px, pl.aabb.p.y+py, pl.id, pl.sprite.mirrored, -45, 2 )
	elseif butt == keys.v then
		CreateBullet( "sfg9000", pl.aabb.p.x+px, pl.aabb.p.y+py, pl.id, pl.sprite.mirrored, 45, -2 )
	elseif butt == keys.g then
		CreateRay( "beam", pl.id, ang(0), pl.aabb.p.x+px, pl.aabb.p.y+py )
	elseif butt == keys.t then
		CreateRay( "beam", pl.id, ang(45), pl.aabb.p.x+px, pl.aabb.p.y+py )
	elseif butt == keys.b then
		CreateRay( "beam", pl.id, ang(-45), pl.aabb.p.x+px, pl.aabb.p.y+py )
	elseif butt == keys.n then
		Resume( 
			NewMapThread( 
				function()
					for i=0,360 do
						if i ~= 90 and i ~= 270 then
							CreateRay( "beam", pl.id, i, pl.aabb.p.x+px, pl.aabb.p.y+py )
						end
						Wait(100)
					end
				end
			)
		)
	elseif butt == keys.m then
		Resume( 
			NewMapThread( 
				function()
					for i=0,360 do
						if i ~= 90 and i ~= 270 then
							CreateBullet( "sfg9000", pl.aabb.p.x+px, pl.aabb.p.y+py, pl.id, pl.sprite.mirrored, i, 0 )
							Wait(1)
						end
					end
				end
			)
		)
	end
end

StopBackMusic()
GlobalSetKeyReleaseProc( keyp )

----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------


function LEVEL.MapScript(player)
	
	if Loader.time > 3 and not mapvar.pacman then
		Resume( NewMapThread( pac ) )
	end
end

function LEVEL.WeaponBonus()
end

function LEVEL.placeSecrets()
end

function LEVEL.removeSecrets()
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter = l
	
	--Log("Set loader ", l == nil and "nil" or "l")
end

LEVEL.name = "chain1"

return LEVEL
