local LEVEL = {}
InitNewGame()
dofile("levels/pear15tutorial2.lua")

CONFIG.backcolor_r = 0.000000
CONFIG.backcolor_g = 0.000000
CONFIG.backcolor_b = 0.000000
LoadConfig()

local function LocalShowLevelResults()
	Game.reset_fade()
	StopMapThreads()
	SetCamScale( 1, 1 )
	SetCamAngle( 0 )
	SetCamAttachedObj(0)
	SwitchLighting(false)
	Game.ResetCombo()
	GUI:hideCombo()
	GUI:hide()
	Menu.lock()
	for k, v in pairs(Game.actors) do
		SetDefaultActor( k )
		GetPlayerCharacter( k ):is_invincible( true )
		EnablePlayerControl( false )
	end
 	FadeOut({0, 0, 0}, 0)
	local background = CreateWidget( constants.wt_Widget, "", nil, -1, -1, CONFIG.scr_width+2, CONFIG.scr_height+2 )
	WidgetSetZ( background, 1.04 )
	SetCamUseBounds( false )
	CamMoveToPos( -9999+CONFIG.scr_width/2, -9999+CONFIG.scr_height/2 )
	local player_done = { false, false }
	local final_widgets = {}
	local final_enemas = {}
	local player_thread = function(player)
		local sx, sy = GetCaptionSize( "default", dictionary_string("PLAYER").." "..player )
		local title = CreateWidget( constants.wt_Label, "", background, CONFIG.scr_width / 4 - sx/2 + (player-1) * CONFIG.scr_width/2, 10, 1, 1 )
		local enema
		local widgets = {}
		local enemas = {}
		local x = CONFIG.scr_width / 4 + (player-1) * CONFIG.scr_width/2
		local y = 10 + 4 * sy
		WidgetSetCaptionColor( title, { 1, 1, .7, 1 }, false ) 
		WidgetSetCaption( title, dictionary_string("PLAYER").." "..player )
		if player > Game.GetPlayersNum() then
			sx, sy = GetCaptionSize( "default", "NOT PRESENT" )
			nokills = CreateWidget( constants.wt_Label, "", background, CONFIG.scr_width / 4 - sx/2 + (player-1) * CONFIG.scr_width/2, 10 + 4 * sy, 1, 1 )
			WidgetSetCaptionColor( nokills, { 1, 1, .7, 1 }, false ) 
			WidgetSetCaption( nokills, dictionary_string("NOT PRESENT") )
			player_done[player] = true
			return
		end
		if mapvar.tmp.kills and mapvar.tmp.kills[player] then
			local keys = {}
			for k, v in pairs( mapvar.tmp.kills[player] ) do
				table.insert( keys, k )
			end
			table.sort( keys )
			for i=1, #keys do
				local k = keys[i]
				local v = mapvar.tmp.kills[player][keys[i]]
				if y + 2 * v.size[2] <= CONFIG.scr_height then
					Wait( 500 )
				else
					Wait( 3000 )
					for k2, v2 in pairs( enemas ) do
						SetObjDead(v2)
					end
					enemas = {}
					for k2, v2 in pairs( widgets ) do
						DestroyWidget(v2)
					end
					widgets = {}
					y = 10 + 4 * sy
				end
				enema = GetObjectUserdata(CreateEnemy( k, x-v.size[1], y+v.size[2], "final_count" ))
				enema:sprite_z( 1.1 )
				SetObjPos( enema, x-v.size[1]-9999, y+v.size[2]-9999 )
				local w = CreateWidget( constants.wt_Label, "", background, 3*CONFIG.scr_width / 8 + (player-1) * CONFIG.scr_width/2, y + v.size[2], 1, 1 )
				WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, false )
				WidgetSetCaption( w, "x"..v.count )
				table.insert( widgets, w )
				table.insert( enemas, enema )
				y = y + 2 * v.size[2] + 20
			end
		else
			sx, sy = GetCaptionSize( "default", dictionary_string("NO KILLS") )
			nokills = CreateWidget( constants.wt_Label, "", background, CONFIG.scr_width / 4 - sx/2 + (player-1) * CONFIG.scr_width/2, 10 + 4 * sy, 1, 1 )
			WidgetSetCaptionColor( nokills, { 1, 1, 1, 1 }, false ) 
			WidgetSetCaption( nokills, dictionary_string("NO KILLS") )
			table.insert( widgets, nokills )
		end
		Wait( 500 )
		local w = CreateWidget( constants.wt_Label, "", background, 1*CONFIG.scr_width / 8 + (player-1) * CONFIG.scr_width/2, y + 15, 1, 1 )
		WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, false )
		if mapvar.tmp.bonusfail then
			WidgetSetCaption( w, "NO CAKE" )
		else
			WidgetSetCaption( w, "CAKE TIME LEFT: "..mapvar.tmp.caketime )
			Game.GiveTutorialBonus()
		end
		table.insert( widgets, w )
		Wait( 1000 )
		for k, v in pairs( widgets ) do
			table.insert( final_widgets, v )
		end
		for k, v in pairs( enemas ) do
			table.insert( final_enemas, v )
		end
		player_done[player] = true
	end
	Resume( NewThread( player_thread ), 1 )
	Resume( NewThread( function()
		while not ( player_done[1] ) do
			Wait( 100 )
		end
		Wait( 3000 )
		for k2, v2 in pairs( final_enemas ) do
			SetObjDead(v2)
		end
		for k2, v2 in pairs( final_widgets ) do
			DestroyWidget(v2)
		end
		sx, sy = GetCaptionSize( "default", dictionary_string("FINAL SCORE:") )
		local score11 = CreateWidget( constants.wt_Label, "", background, CONFIG.scr_width / 4 - sx/2, 10 + 4 * sy, 1, 1 )
		WidgetSetCaptionColor( score11, { 1, 1, 1, 1 }, false ) 
		WidgetSetCaption( score11, dictionary_string("FINAL SCORE:") )
		if Game.GetPlayersNum() > 1 then
			sx, sy = GetCaptionSize( "default", dictionary_string("FINAL SCORE:") )
			local score21 = CreateWidget( constants.wt_Label, "", background, 3 * CONFIG.scr_width / 4 - sx/2, 10 + 4 * sy, 1, 1 )
			WidgetSetCaptionColor( score21, { 1, 1, 1, 1 }, false ) 
			WidgetSetCaption( score21, dictionary_string("FINAL SCORE:") )
		end
		sx, sy = GetCaptionSize( "default", dictionary_string("SECRETS FOUND:") )
		local secrets1 = CreateWidget( constants.wt_Label, "", background, CONFIG.scr_width / 2 - sx/2, CONFIG.scr_height/2 - 2 * sy, 1, 1 )
		WidgetSetCaptionColor( secrets1, { 1, 1, 1, 1 }, false ) 
		WidgetSetCaption( secrets1, dictionary_string("SECRETS FOUND:") )
		local secrets_found = Game.mapvar.tmp.secrets or 0
		local secrets_total = Loader.level.secrets or 0
		secrets_found = tostring(secrets_found).." "..dictionary_string( "OUT OF" ).." "..tostring(secrets_total)
		sx, sy = GetCaptionSize( "default", secrets_found )
		local secrets2 = CreateWidget( constants.wt_Label, "", background, CONFIG.scr_width / 2 - sx/2, CONFIG.scr_height/2 + 2 * sy, 1, 1 )
		WidgetSetCaptionColor( secrets2, { 1, 1, 1, 1 }, false ) 
		WidgetSetCaption( secrets2, secrets_found )
		Wait( 3000 )
		DestroyWidget( background )
		FadeIn(0)
		SwitchLighting(true)
		
		Menu.unlock()
		Loader.exitGame()
		--[[
		if ( next_level ) then
			if not skip_menu then
				Menu.showEndLevelMenu( next_level )
			else
				Loader.ChangeLevel( next_level )
			end
		end
		--]]
	end ))
end

function LEVEL.onPlayerDeath()
	local obj1 = CreateItem( "generic-trigger", -1000, -1000 )
	ObjectPushInt( obj1, 28 )
	SetObjAnim( obj1, "script", false )
	return true
end

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	local _key = Game.game_key_pressed
	Game.AddCleanup( function() Game.game_key_pressed = _key end )
	Game.game_key_pressed = function(key)
		if GamePaused() then
			return
		end
	end
	mapvar.actors[1].info.character = "pear15soh-unarmed"
	SwitchLighting(true)
	if GetPlayer() then
		local fade_widget = CreateWidget( constants.wt_Widget, "THE FADE", nil, -1, -1, CONFIG.scr_width+2, CONFIG.scr_height+2 )
		WidgetSetColorBox( fade_widget, {0, 0, 0, 1} )
		WidgetSetZ( fade_widget, 1 )
		CamMoveToPos( 832,38 )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
		Game.AttachCamToPlayers( {
			{ {-99999,-99999,99999,99999},  {450,-99999,4350,1200} },
		} )
		local env = CreateEnvironment('default_environment', -9001, -9001)
		SetDefaultEnvironment(env)
		Game.info.can_join = false
		mapvar.tmp.cutscene = true
		
		EnablePlayerControl( false )
		Menu.lock()
		Resume( NewThread( function()
			GUI:hide()
			Wait( 1 )
			
			local char1, char2 = Game.GetCharacters()
			
			SetObjPos( char1, 482,38 )
			local done = false
			Game.MoveWithAccUntilX( char1, { x = 1.75, y = 0 }, 832, function()
				char1:acc( {x = 0, y = 0} )
				SetObjAnim( char1, "stop", false )
				done = true
			end )
			
			local cx, cy = GetCamPos()

			char = GetPlayerCharacter()

			for i = 100, 1, -1 do
				WidgetSetColorBox( fade_widget, {0, 0, 0, 1-1/i} )
				Wait( 1 )
			end
			
			while not done do
				Wait(1)
			end
			
			Wait(200)
			
			DestroyWidget( fade_widget )
			
			local cx, cy = GetCamPos()

			local obj3 = CreateEnvironment( 'default_environment', -1700, -1500 )
			local obj4 = CreateEnvironment( 'default_environment', -878, 200 )
			GroupObjects( obj3, obj4 )
			SetEnvironmentStats( obj3, {
				on_enter = function( ud, x, y, material, this )
					if ud:type() == 1 then
						--player_script(ud)
					end
				end,
				on_leave = function( ud, x, y, material, new_material, this )
				end,
				on_stay = function( ud, x, y, material, this )
				end,
			} )

			local cx, cy = GetCamPos()

			local conversation_end = function()
				mapvar.tmp.cutscene = false
				EnablePlayerControl( true )
				SetCamLag(0.9)
				Menu.unlock()
				GUI:show()
				Resume( NewMapThread( function()
					sx,sy = GetCaptionSize( "default", "CAKE TIME LEFT" )
					mapvar.tmp.labelbonus = CreateWidget(constants.wt_Label, "label", nil, 320-sx, 50+50, 100*2, 15);
					WidgetSetCaptionColor(mapvar.tmp.labelbonus, {1,1,1,1}, false, {0,0,0,1});
					WidgetSetCaptionFont(mapvar.tmp.labelbonus, "default", 2);
					WidgetSetCaption(mapvar.tmp.labelbonus, "CAKE TIME LEFT" ,false);
					WidgetSetZ(mapvar.tmp.labelbonus, 0.5)
					mapvar.tmp.timebonus = 600;
					mapvar.tmp.caketime = mapvar.tmp.timebonus/10;
					mapvar.tmp.starttime = Loader.time
					local timeparam = 0;
					sx,sy = GetCaptionSize( "default", "99.9" )
					mapvar.tmp.timer = CreateWidget(constants.wt_Label, "timer", nil, 320-sx, 80+50, 20*2, 15);
					WidgetSetCaptionColor(mapvar.tmp.timer, {1,1,1,1}, false, {0,0,0,1});
					WidgetSetCaptionFont(mapvar.tmp.timer, "default", 2);
					WidgetSetCaption(mapvar.tmp.timer, mapvar.tmp.timebonus ,false);
					WidgetSetZ(mapvar.tmp.timer, 0.5)
					Wait(1)
					GUI:show()
					while true do
						local t = math.floor( (Loader.time - mapvar.tmp.starttime) / 100 )
						if t > mapvar.tmp.timebonus then
							sx,sy = GetCaptionSize( "default", "NOOO!!!" )
							WidgetSetPos(mapvar.tmp.timer, 320-sx, 80+50 )
							WidgetSetCaption(mapvar.tmp.timer, "NOOO!!!"  ,false);
							mapvar.tmp.bonusfail = true;
							mapvar.tmp.caketime = 0;
						else
							mapvar.tmp.caketime = (mapvar.tmp.timebonus - t)/10;
							WidgetSetCaption(mapvar.tmp.timer, mapvar.tmp.caketime ,false);
						end
						Wait(1)
					end
				end))
			end
			local char_talk = function()
				mapvar.tmp.talk = mapvar.tmp.talk or {}
				mapvar.tmp.talk[char] = true
			end
			Conversation.create( {
				{ sprite = "portrait-soh" },
				char_talk,
				"PEAR15_TUTORIAL_23",
				conversation_end,
				nil,
			} ):start()
		end))
	end
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
--
	local obj3 = CreateEnvironment( 'default_environment', 1216, -346 )
	local obj4 = CreateEnvironment( 'default_environment', 1345, 92 )
	GroupObjects( obj3, obj4 )
	SetEnvironmentStats( obj3, {
		on_enter = function( ud, x, y, material, this )
			if ud:type() == constants.objPlayer then
				if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
				local sx, sy = GetCaptionSize( "dialogue", "������� "..GetKeyName( CONFIG.key_conf[1].jump ).." + "..GetKeyName( CONFIG.key_conf[1].fire ).." ��� �������� � ������." )
				local text = CreateWidget( constants.wt_Label, "GUI", nil, (CONFIG.scr_width - sx)/2, (CONFIG.scr_height - sy)/2-40, 640, 10 )
				WidgetSetFocusable( text, false )
				WidgetSetCaptionColor( text, {1,1,1,1}, false )
				WidgetSetCaptionFont( text, "dialogue" )
				WidgetSetCaption( text, "������� /cffff55"..GetKeyName( CONFIG.key_conf[1].jump ).."/cffffff + /cffff55"..GetKeyName( CONFIG.key_conf[1].fire ).."/cffffff ��� �������� � ������.", true )
				WidgetSetZ( text, 0.5 )
				--mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
				mapvar.tmp[ud].widget = text
			end
		end,
		on_leave = function( ud, x, y, material, new_material, this )
			if ud:type() == constants.objPlayer then
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			end
		end,
		on_stay = function( ud, x, y, material, this )
		end,
	} )
	
	local obj3 = CreateEnvironment( 'default_environment', 1800, -320 )
	local obj4 = CreateEnvironment( 'default_environment', 1929, -41 )
	GroupObjects( obj3, obj4 )
	SetEnvironmentStats( obj3, {
		on_enter = function( ud, x, y, material, this )
			if ud:type() == constants.objPlayer then
				if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
				local sx, sy = GetCaptionSize( "dialogue", "������� "..GetKeyName( CONFIG.key_conf[1].sit ).." + "..GetKeyName( CONFIG.key_conf[1].fire ).." + "..GetKeyName( CONFIG.key_conf[1].up ).." ("..GetKeyName( CONFIG.key_conf[1].down )..") ��� �������� �� ��������� �� ��������� ����." )
				local text = CreateWidget( constants.wt_Label, "GUI", nil, math.max((CONFIG.scr_width - sx)/2,60), (CONFIG.scr_height - sy)/2-40, 640-2*60, 10 )
				WidgetSetFocusable( text, false )
				WidgetSetCaptionColor( text, {1,1,1,1}, false )
				WidgetSetCaptionFont( text, "dialogue" )
				WidgetSetCaption( text, "������� /cffff55"..GetKeyName( CONFIG.key_conf[1].sit ).."/cffffff + /cffff55"..GetKeyName( CONFIG.key_conf[1].fire ).."/cffffff + /cffff55"..GetKeyName( CONFIG.key_conf[1].up ).."/cffffff(/cffff55"..GetKeyName( CONFIG.key_conf[1].down ).."/cffffff) ��� �������� �� ��������� �� ��������� ����./n"..
				"������� /cffff55"..GetKeyName( CONFIG.key_conf[1].sit ).."/cffffff + /cffff55"..GetKeyName( CONFIG.key_conf[1].right ).."/cffffff (/cffff55"..GetKeyName( CONFIG.key_conf[1].left ).."/cffffff) ��� ��������� �� ��������� ������."
				, true )
				WidgetSetZ( text, 0.5 )
				--mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
				mapvar.tmp[ud].widget = text
			end
		end,
		on_leave = function( ud, x, y, material, new_material, this )
			if ud:type() == constants.objPlayer then
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			end
		end,
		on_stay = function( ud, x, y, material, this )
		end,
	} )
	
	local obj3 = CreateEnvironment( 'default_environment', 2652, 246 )
	local obj4 = CreateEnvironment( 'default_environment', 2753, 373 )
	GroupObjects( obj3, obj4 )
	SetEnvironmentStats( obj3, {
		on_enter = function( ud, x, y, material, this )
			if ud:type() == constants.objPlayer then
				if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
				local sx, sy = GetCaptionSize( "dialogue", "������� "..GetKeyName( CONFIG.key_conf[1].alt_fire ).." ��� ��������������� ��������." )
				local text = CreateWidget( constants.wt_Label, "GUI", nil, (CONFIG.scr_width - sx)/2, (CONFIG.scr_height - 5*sy)/2-50, 640, 10 )
				WidgetSetFocusable( text, false )
				WidgetSetCaptionColor( text, {1,1,1,1}, false )
				WidgetSetCaptionFont( text, "dialogue" )
				WidgetSetCaption( text, "������� /cffff55"..GetKeyName( CONFIG.key_conf[1].alt_fire ).."/cffffff ��� ��������������� ��������./n� ���� ������ �������� �������./n����������� ����� ������ �� ���� /c33ff33����������/cffffff.", true )
				WidgetSetZ( text, 0.5 )
				--mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
				mapvar.tmp[ud].widget = text
			end
		end,
		on_leave = function( ud, x, y, material, new_material, this )
			if ud:type() == constants.objPlayer then
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			end
		end,
		on_stay = function( ud, x, y, material, this )
		end,
	} )

	local obj3 = CreateEnvironment( 'default_environment', 900, -320 )
	local obj4 = CreateEnvironment( 'default_environment', 1000, 65 )
	GroupObjects( obj3, obj4 )
	SetEnvironmentStats( obj3, {
		on_enter = function( ud, x, y, material, this )
			if ud:type() == constants.objPlayer then
				if not mapvar.tmp[ud] then mapvar.tmp[ud] = {} end
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
				local sx, sy = GetCaptionSize( "dialogue", "������� "..GetKeyName( CONFIG.key_conf[1].sit ).." + "..GetKeyName( CONFIG.key_conf[1].fire ).." �������� �� ��������� ����." )
				local text = CreateWidget( constants.wt_Label, "GUI", nil, (CONFIG.scr_width - sx)/2, (CONFIG.scr_height - 5*sy)/2-40, 640, 10 )
				WidgetSetFocusable( text, false )
				WidgetSetCaptionColor( text, {1,1,1,1}, false )
				WidgetSetCaptionFont( text, "dialogue" )
				WidgetSetCaption( text, "����������� /cffff55"..GetKeyName( CONFIG.key_conf[1].up ).."/cffffff � /cffff55"..GetKeyName( CONFIG.key_conf[1].down ).."/cffffff ��� ������������./n"..
				"������� /cffff55"..GetKeyName( CONFIG.key_conf[1].sit ).."/cffffff + /cffff55"..GetKeyName( CONFIG.key_conf[1].fire ).."/cffffff �������� �� ��������� ����."
				, true )
				WidgetSetZ( text, 0.5 )
				--mapvar.tmp[ud].effect = CreateEffect("movement", 0, 0, ud:id(), 1)
				mapvar.tmp[ud].widget = text
			end
		end,
		on_leave = function( ud, x, y, material, new_material, this )
			if ud:type() == constants.objPlayer then
				--if ( mapvar.tmp[ud] and mapvar.tmp[ud].effect ) then SetObjDead( mapvar.tmp[ud].effect ) mapvar.tmp[ud].effect = nil end
				if ( mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
			end
		end,
		on_stay = function( ud, x, y, material, this )
		end,
	} )

--
	local obj9 = CreateItem( 'generic-trigger', 4288, -395 )
	local obj10 = CreateItem( 'generic-trigger', 4353, -191 )
	GroupObjects( obj9, obj10 )
	ObjectPushInt(obj9,2)
	local object
---$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	if trigger == 1 then
		mapvar.tmp.secrets = ( mapvar.tmp.secrets or 0 ) + 1
	elseif trigger == 2 then
		Game.ProgressAchievement( "LICENSED_TO_PLAY", 1 )
		GUI:hide()
		ud = GetPlayerCharacter()
		StopMapThreads()
		if ( ud and mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
		if mapvar.tmp and mapvar.tmp.labelbonus then
			DestroyWidget(mapvar.tmp.labelbonus)
			mapvar.tmp.labelbonus = nil
		end
		if mapvar.tmp and mapvar.tmp.timer then
			DestroyWidget(mapvar.tmp.timer)
			mapvar.tmp.timer = nil
		end
		if mapvar.tmp and mapvar.tmp.timebonus then
			mapvar.tmp.timebonus = nil
		end
		LocalShowLevelResults()
	elseif trigger == 28 then
		Loader.restartGame()
	end
	SetObjDead( id )
--$(MAP_TRIGGER)-
end

function LEVEL.GetNextCharacter()
	if not Loader.level or not Loader.level.spawnpoints then
		return nil
	end
	if Game.actors[1] and Game.actors[1].char then
		Game.info.players = Game.info.players + 1
	else
		Game.info.players = 1
	end
	if Game.info.players == 1 then
		--vars.character or Game.info.first_player_character or 
		if true then
			return { "pear15soh", Loader.level.spawnpoints[1] or {0,0} }
		end
	else
		local pos = GetObjectUserdata( Game.mapvar.actors[1].char.id ):aabb().p
		if Game.actors[1].info.character == "pear15soh" then
			return { "pear15unyl", {pos.x, pos.y} }
		else
			return { "pear15soh", {pos.x, pos.y} }
		end
	end
end

LEVEL.characters = {}
LEVEL.spawnpoints = {{832,38},}

function LEVEL.cleanUp()
	ud = GetPlayerCharacter()
	if ( ud and mapvar.tmp[ud] and mapvar.tmp[ud].widget ) then DestroyWidget( mapvar.tmp[ud].widget ) mapvar.tmp[ud].widget = nil end
	if mapvar.tmp and mapvar.tmp.labelbonus then
		DestroyWidget(mapvar.tmp.labelbonus)
		mapvar.tmp.labelbonus = nil
	end
	if mapvar.tmp and mapvar.tmp.timer then
		DestroyWidget(mapvar.tmp.timer)
		mapvar.tmp.timer = nil
	end
	if mapvar.tmp and mapvar.tmp.timebonus then
		mapvar.tmp.timebonus = nil
	end
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'pear15tutorial'

return LEVEL
