--EDITABLE MAP

local LEVEL = {}
InitNewGame()
dofile("levels/superiic.lua")

CONFIG.backcolor_r = 0.090000
CONFIG.backcolor_g = 0.117000
CONFIG.backcolor_b = 0.035000
LoadConfig()

PlayBackMusic("music/iie_escape.it")

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	if GetPlayer() then
		SetCamAttachedObj( GetPlayer().id )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
	end
	EnablePlayerControl( false )
	Menu.locked = true
	local env = CreateEnvironment("default_environment", -9001, -9001)
	SetDefaultEnvironment(env)
	showStageName( -1, function()  FadeIn(1000) EnablePlayerControl( true ) Menu.locked = false end )
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	obj1 = CreateItem( "generic-trigger", 167, 660 )
	obj2 = CreateItem( "generic-trigger", 292, 974 )
	obj3 = CreateItem( "generic-trigger", 2149, 850 )
	obj4 = CreateItem( "generic-trigger", 2214, 955 )
	GroupObjects( obj1, obj2 )
	ObjectPushInt( obj1, 1 )
	GroupObjects( obj3, obj4 )
	ObjectPushInt( obj3, 2 )
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	if trigger == 1 then
		SetObjDead( id )
		Resume( NewMapThread( function()
			for i = -15,2000,100 do
				for j=1, 5 do
					CreateEnemy( "big_explosion", i+math.random(-20,20), math.random( 630, 1277 ) )
				end
				Wait(500)
				Wait(1)
			end
		end ))
	elseif trigger == 2 then
		SetObjDead( id )
		Menu.showEndLevelMenu("superiic")
	end
--$(MAP_TRIGGER)-
end

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'superiic'

return LEVEL
