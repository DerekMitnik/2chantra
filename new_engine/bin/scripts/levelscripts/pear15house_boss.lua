local LEVEL = {}
InitNewGame()
dofile("levels/pear15house_boss.lua")

CONFIG.backcolor_r = 0.000000
CONFIG.backcolor_g = 0.000000
CONFIG.backcolor_b = 0.000000
LoadConfig()

StopBackMusic()
PlayBackMusic( "music/iie_boss.it" )

local wall

local function suddenly()
	local x, y = 439, 140
	local SUDDENLY = CreateWidget( constants.wt_Label, "", nil, x, y, 1, 1 )
	WidgetSetFocusable( SUDDENLY, false )
	WidgetSetFixedPosition( SUDDENLY, false )
	WidgetSetCaptionFont( SUDDENLY, "default", 0.1 )
	WidgetSetCaptionColor( SUDDENLY, { 1, 1, 1, 1 }, false, { 0, 0, 0, 1 } )
	WidgetSetCaption( SUDDENLY, "��������!" )
	local sx, sy = GetCaptionSize( "default", "��������!" )
	local remove_suddenly = function() DestroyWidget( SUDDENLY ) end
	Game.AddCleanup( remove_suddenly )
	Resume( NewMapThread( function()
		local sudden_time = Loader.time
		local sudden_dt = 0
		local size = 0
		while size < 2 do
			sudden_dt = Loader.time - sudden_time
			size = math.min( size + 0.1 * sudden_dt / 200, 2 )
			WidgetSetCaptionFont( SUDDENLY, "default", size )
			WidgetSetPos( SUDDENLY, x - sx * size / 2, y - sy * size / 2 )
			WidgetSetFixedPosition( SUDDENLY, false )
			Wait( 1 ) 
		end
		Wait( 500 )
		while size > 0.1 do
			sudden_dt = Loader.time - sudden_time
			size = math.max( size - 0.1 * sudden_dt / 200, 0.1 )
			WidgetSetCaptionFont( SUDDENLY, "default", size )
			WidgetSetPos( SUDDENLY, x - sx * size / 2, y - sy * size / 2 )
			WidgetSetFixedPosition( SUDDENLY, false )
			Wait( 1 ) 
		end
		DestroyWidget( SUDDENLY )
		Game.RemoveCleanup( remove_suddenly )
	end )) 
end

local boss_dead = false
local function done()
	boss_dead = true
	for k, v in pairs( mapvar.tmp.enemies ) do
		if v then
			SetObjDead( k )
		end
	end
end

local function begin()
	local env = CreateEnvironment('default_environment', -9001, -9001)
	SetDefaultEnvironment(env)
	local block = CreateSprite( "phys-empty", 448, 192 )
	GroupObjects( block, CreateSprite( "phys-empty", 468, 350 ) )
	mapvar.tmp.boss_bar_bkg = CreateWidget( constants.wt_Picture, "", nil, CONFIG.scr_width / 2 - 137, CONFIG.scr_height - 3 * 21, 1, 1 )
	WidgetSetSprite( mapvar.tmp.boss_bar_bkg, "pear15boss_bar", "background" )
	WidgetSetSpriteColor( mapvar.tmp.boss_bar_bkg, { 0.8, 0.4, 0.4, 0.5 } )
	mapvar.tmp.boss_bar = CreateWidget( constants.wt_Picture, "", nil, CONFIG.scr_width / 2 - 137 + 24, CONFIG.scr_height - 3 * 21 + 5, 243, 11 )
	WidgetSetSprite( mapvar.tmp.boss_bar, "pear15boss_bar", "bar" )
	WidgetSetSpriteRenderMethod( mapvar.tmp.boss_bar, constants.rsmCrop )
	WidgetSetSpriteColor( mapvar.tmp.boss_bar, { 0.8, 0.4, 0.4, 0.5 } )
	cleanup = function() 
		if mapvar.tmp.boss_bar then
			DestroyWidget(mapvar.tmp.boss_bar)
		end
		if mapvar.tmp.boss_bar_bkg then
			DestroyWidget(mapvar.tmp.boss_bar_bkg) 
		end
	end
		if not Game.cleanupFunctions then
		Game.cleanupFunctions = {}
	end
	Game.cleanupFunctions[ cleanup ] = true
	mapvar.tmp.enemies = {}

	Resume( NewMapThread( function()
		local phase = 1
		local MAX_ENEMIES = (difficulty or 1) * 4
		local SPAWN_DELAY = 500
		local enemies_score = 0
		local last_spawn = 0
		local spawnpoint
		local first_spawn = true
		local empty_function = function() end
		local boss
		Resume( NewMapThread( function()
			Wait( 1000 )
			while phase == 1 or phase == 2 do
				Wait( math.random( 1000, 5000 ) )
				local x, y = GetCamPos()
				CreateEnemy( "big_safe_explosion", x, y - CONFIG.scr_height/2 + 10 )
				ShakeCamera( 40, 0, 1000, 3 )
				y = y - (CONFIG.scr_height/2) - 50
				x = x - (CONFIG.scr_width/2) - 10
				for i = 1, 6 do
					SetObjAnim( CreateEnemy( "house_debris", x + math.random(0, CONFIG.scr_width + 100 ), y + math.random( -200, 20 ) ), "big", false )
				end
				for i = 1, 20 do
					CreateEnemy( "house_debris", x + math.random(0, CONFIG.scr_width/2 + 10 ), y + math.random( -20, 20 ) )
				end
			end
		end ) )
		boss_done = false
		while not boss_done do
			if not boss_done then
				if not first_spawn and phase == 1 and Loader.time > 25000 then
					phase = 2
					SetObjGhostToByte( block, constants.physEverything )
					boss = GetObjectUserdata( CreateEnemy( "pear15tank", 619, 299 ) )
					mapvar.tmp[boss] = { last_shot = Loader.time, left = true, up = true }
					SetObjAnim( boss, "roll_in", false )
					mapvar.tmp.pytard = mapvar.tmp.boss_object
					mapvar.tmp.boss_object = boss
					CreateEnemy( "big_explosion", 439, 253 )
					SetObjAnim( wall, "break", false )
					for i = 343, 180, -32 do
						CreateParticleSystem( "pwood", 439, i )
					end
					suddenly()
					mapvar.tmp.poi = wall
					SetObjProcessor( boss, function( obj )
						local pos = obj:aabb().p
						local t = mapvar.tmp[obj]
						if pos.x <= 408 and phase == 2 then
							SetObjAnim( obj, "stop", false )
							mapvar.tmp.rolled_out = true
							mapvar.tmp.poi = nil
							mapvar.tmp.boss_object = mapvar.tmp.pytard
							mapvar.tmp.tank = obj
							phase = 3
							t.last_shot = Loader.time
						end
						if phase == 3 and Loader.time - t.last_shot > 5000 then
							t.last_shot = Loader.time
							local g
							local y = iff( t.up, 133, 336 )
							if t.left then
								for i = -545, -43, 75 do
									g = GetObjectUserdata( CreateBullet( "btard-grenade", pos.x, pos.y, obj:id(), obj:sprite_mirrored(), 0, 0 ) )
									mapvar.tmp[g] = { target = { x = i, y = y }, start = { x = pos.x, y = pos.y }, progress = 0 }
								end
								if t.up then
									t.up = false
								else
									t.left = false
								end
							else
								for i = -43, 405, 75 do
									g = GetObjectUserdata( CreateBullet( "btard-grenade", pos.x, pos.y, obj:id(), obj:sprite_mirrored(), 0, 0 ) )
									mapvar.tmp[g] = { target = { x = i, y = y }, start = { x = pos.x, y = pos.y }, progress = 0 }
								end
								if not t.up then 
									t.up = true 
								else
									t.left = true
								end
							end
						end
					end )
				end
				if enemies_score < MAX_ENEMIES and (Loader.time or 0) - last_spawn >= SPAWN_DELAY then
					if first_spawn then
						first_spawn = false
					else
						last_spawn = Loader.time or 0
					end
					spawnpoint = math.random( 1, #Loader.level.monsterpoints )
					spawnpoint = Loader.level.monsterpoints[spawnpoint]
					local en
					if MAX_ENEMIES - enemies_score >= 2 and math.random(10) > 7 then
						enemies_score = enemies_score + 2
						en = GetObjectUserdata( CreateEnemy( "slowpoke-stray", spawnpoint[1], spawnpoint[2] ) )
						mapvar.tmp.enemies[en] = true
						SetObjProcessor( en,
							function( obj ) if obj:health() <= 0 then
								mapvar.tmp.enemies[en] = false
								enemies_score = enemies_score - 2
								return SetObjProcessor( obj, empty_function )
							end end )
					else
						enemies_score = enemies_score + 1
						en = GetObjectUserdata( CreateEnemy( "btard2", spawnpoint[1], spawnpoint[2] ) )
						mapvar.tmp.enemies[en] = true
						SetObjProcessor( en,
							function( obj ) if obj:health() <= 0 then
								mapvar.tmp.enemies[en] = false
								enemies_score = enemies_score - 1
								return SetObjProcessor( obj, empty_function )
							end end )
					end
				end
				if mapvar.tmp.boss_dead and phase < 4 then 
					phase = 4 
					mapvar.tmp.boss_obj = nil
				end
			end
			Wait(1)
		end
	end ))
end

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	boss_dead = false;
	Game.fade_out()
	mapvar.tmp.fade_widget_text = {}
	local sx, sy = GetCaptionSize( "default", "������� 3" )
	local text1 = CreateWidget( constants.wt_Label, "TEXT", mapvar.tmp.fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 - 1.5 * sy, 1, 1 )
	mapvar.tmp.fade_widget_text["text1"] = text1
	WidgetSetCaptionColor( text1, {1,1,1,1}, false )
	WidgetSetCaption( text1, "������� 3" )
	WidgetSetZ( text1, 1.05 )
	sx, sy = GetCaptionSize( "default", "������ ��������" )
	local text2 = CreateWidget( constants.wt_Label, "TEXT", mapvar.tmp.fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 + 1.5 * sy, 1, 1 )
	mapvar.tmp.fade_widget_text["text2"] = text2
	WidgetSetCaptionColor( text2, {1,1,1,1}, false )
	WidgetSetCaption( text2, "������ ��������" )
	WidgetSetZ( text2, 1.05 )
	CamMoveToPos( 225, 308 )
	SetCamAttachedAxis(true, true)
	SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
	SetCamObjOffset(0, 60)
	SetCamLag(0.9)
	
	Menu.lock()
	
	local start_intro_thread = false
	local fade_thread = NewThread( function()
		Wait( 1 )
		GUI:hide()
		Wait( 1000 )
		mapvar.tmp.fade_complete = false
		Game.fade_in()
		start_intro_thread = true
	end )
	
	Resume( fade_thread )
	
	local intro_thread = NewThread( function()
		wall = CreateSpriteCenter( "house-wallhole", 448, 271 )
		
		local boss = GetObjectUserdata( CreateEnemy( "pear15pytard", 225, 308 ) )
		
		SetObjSpriteMirrored( boss, true )
		SetCamAttachedObj( boss:id() )
		SetCamBounds( -591, 562, 389-480, 389 )
		SetCamUseBounds( true )
		
		while not start_intro_thread do
			Wait( 10 )
		end

		EnablePlayerControl( false )
		Game.info.can_join = false
		mapvar.tmp.cutscene = true
		Wait(1)
		local char1, char2, char1_talk, char2_talk = Game.GetCharacters()
		SetObjPos( char1, -352, 257 )
		if char2 then
			SetObjPos( char2, -452, 257 )
		end
		Wait( 200 )
		local done = false
		if char2 then
			char2:acc{ x = 0.75, y = 0 }
		end
		Game.MoveWithAccUntilX( char1, { x = 0.75, y = 0 }, 52, function()
			done = true
			SetObjAnim( char1, "stop", false )
			if char2 then
				char2:acc{ x = 0, y = 0 }
				SetObjAnim( char2, "stop", false )
			end
		end )
		while not mapvar.tmp.fade_complete or not done do
			Wait(1)
		end
		RemoveSkipKey()
		local conversation_end = function()
			Game.AttachCamToPlayers( {
					{ {-99999, -99999, 99999, 99999}, {-591, 389-480, 562, 389 } },
				} )
			mapvar.tmp.cutscene = false
			mapvar.tmp.boss_start = true
			EnablePlayerControl( true )
			Menu.unlock()
			Game.info.can_join = true
			SetCamLag(0.9)
			begin()
			GUI:show()
		end
		Conversation.create{
			function( var )
				if char2 then
					return "BOTH-PLAYERS"
				elseif mapvar.actors[1].info.character == "pear15unyl" then
					return "UNYL-SINGLE"
				end
			end,
			char1_talk,
			{ sprite = "portrait-soh" },
			"PEAR15_HOUSE_18",
			conversation_end,
			nil,
			{ type = "LABEL", "UNYL-SINGLE" },
			char1_talk,
			{ sprite = "portrait-unyl" },
			"PEAR15_HOUSE_19",
			conversation_end,
			nil,
			{ type = "LABEL", "BOTH-PLAYERS" },
			char2_talk,
			{ sprite = "portrait-unyl" },
			"PEAR15_HOUSE_20",
			char1_talk,
			{ sprite = "portrait-soh" },
			"PEAR15_HOUSE_21",
			conversation_end,
			nil
		}:start()

	end )
		
	SetSkipKey( function( key )
		if key == CONFIG.key_conf[2].gui_nav_accept then
			return true
		end
		if not start_intro_thread then
			StopThread( fade_thread )
			GUI:hide()
			Game.reset_fade()
			start_intro_thread = true
		end
		StopThread( intro_thread )
		local char1, char2, char1_talk, char2_talk = Game.GetCharacters()
		SetObjPos( char1, 52, 300 )
		if char2 then
			SetObjPos( char2, 52-100, 300 )
		end
		mapvar.tmp.cutscene = false
		EnablePlayerControl( true )
		SetCamLag(0.9)
		Menu.unlock()
		Game.AttachCamToPlayers( {
				{ {-99999, -99999, 99999, 99999}, {-591, 389-480, 562, 389 } },
			} )
		mapvar.tmp.boss_start = true
		EnablePlayerControl( true )
		Game.info.can_join = true
		begin()
		GUI:show()
	end, 500 )
	
	Resume( intro_thread )
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local obj1 = CreateEnvironment( 'deadly-flames', -549, 266 )
	local obj2 = CreateEnvironment( 'deadly-flames', -486, 360 )
	GroupObjects( obj1, obj2 )
	local object
	local obj3 = CreateItem( 'generic-trigger', 494, 128 )
	local obj4 = CreateItem( 'generic-trigger', 687, 402 )
	GroupObjects( obj3, obj4 )
--$(SPECIAL_CREATION)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	if mapvar.tmp.boss_defeated then
		SetObjDead( id )
		done()
		Game.ShowLevelResults("pear15mountains_opening")
	else
		local pl = GetPlayerCharacter(1)
		local pos
		if pl then
			pos = pl:aabb().p.x + pl:aabb().W
			if pos >= 490 then
				SetObjPos( pl, 450, 128 )
			end
		end
		pl = GetPlayerCharacter(2)
		if pl then
			pos = pl:aabb().p.x + pl:aabb().W
			if pos >= 490 then
				SetObjPos( pl, 450, 128 )
			end
		end
	end
--$(MAP_TRIGGER)-
end

LEVEL.spawnpoints = {{-52,187},}
LEVEL.telepoints = { {-50, -27}, {-478, -28}, {370, -26}, {-304, 132}, {210, 133}, {-52,187}, {180, 293}, {-295, 293} }
LEVEL.monsterpoints = { {-50, -97}, {-478, -98}, {370, -96} }

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'Familiar Face'

LEVEL.hints =
{
	"������� � ���� ������� �� ������ ���!",
	"�������� � ���� - �� ����� ������ ����!",
	"���������� ������������ ��������� � ���� ������!",
	"��, ��� �� ������� ���, ������� ������!"
}

LEVEL.solution = '������ ����� ����� ������, ��� "��������� � ��������� �����" ����������. ����� ��������� ����� � ���� ��� �������� ����./n/n����� ����, ��� �� ��������, �������� ����������� ����� ��� ����������� - ������������ ����� ������� �����. ���� �������� �� ��������� ������� ������ ������� �������. ���������, ���� �������� � ��� ������ �� ���� ��������� � �������� "���������� � ��������� �����" ������� � �����. �������� ����� ��� �, ����� ����, ��� �� ������ �����, �������� �� ��������� ����./n/n��� �������� ���������� ��������� ��� ����, ����� ����� �������� ����� ���� � �����.'

return LEVEL
