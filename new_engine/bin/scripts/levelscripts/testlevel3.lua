InitNewGame();			-- ������ ����� ����
difficulty = 1
---[[
LoadTexture("sohchan.png");
LoadTexture("bullets.png");
LoadTexture("flash-straight.png");
LoadTexture("flash-angle.png");
LoadTexture("grenade.png")
LoadTexture("grenade-explosion.png")
LoadPrototype("sohchan.lua");
LoadPrototype("sfg9000.lua");
LoadPrototype("sfg5000.lua");
LoadPrototype("flash-straight.lua");
LoadPrototype("flash-angle-up.lua");
LoadPrototype("flash-angle-down.lua");
LoadPrototype("explosion.lua");
--]]

LoadTexture("window1.png")
LoadPrototype("window1.lua");
LoadTexture("portrait-unyl.png")
LoadPrototype("portrait-unyl.lua");

---[[
LoadTexture("phys_floor.png");
LoadPrototype("phys_floor.lua");
--]]

---[[
LoadTexture("dust-run.png");
LoadPrototype("dust-run.lua");
LoadTexture("dust-land.png");
LoadPrototype("dust-land.lua");
LoadTexture("dust-stop.png");
LoadPrototype("dust-stop.lua");
--]]
---[[
LoadTexture("btard_o.png");
LoadPrototype("btard.lua");
LoadPrototype("btard-corpse.lua");
LoadPrototype("btard-punch.lua");
--]]
--[[
LoadTexture("pblood.png");
LoadPrototype("pblood.lua");
LoadPrototype("pblood-wound.lua");
--]]
---[[
LoadSound("blaster_shot.ogg");
LoadSound("foot-left.ogg");
LoadSound("foot-right.ogg");
LoadSound("land.ogg");
LoadSound("stop.ogg");
LoadSound("music\\iie_lab.it");
--]]

--LoadTexture("unyl_sprite.png");
--LoadPrototype("unylchan.lua");

--[[
LoadTexture("3nd_plan.png");
LoadPrototype("3nd_plan.lua");
--]]

-- ���������� ������
--[[
LoadTexture("clouds1.png")
LoadTexture("clouds2.png")
LoadTexture("clouds3.png")
LoadTexture("clouds4.png")
LoadPrototype("clouds1.lua")
LoadPrototype("clouds2.lua")
LoadPrototype("clouds3.lua")
LoadPrototype("clouds4.lua")


local rib = CreateRibbon("clouds4", 0, 150,1);
SetRibbonAttatachToY(rib, true)
rib = CreateRibbon("clouds3", 0, 150, 0.25);
SetRibbonAttatachToY(rib, true)
rib = CreateRibbon("clouds2", 0, 150, 0.6);
SetRibbonAttatachToY(rib, true)
rib = CreateRibbon("clouds1", 0, 150, 0.5);
SetRibbonAttatachToY(rib, true)
--]]

require("serialize")

CreatePlayer("sohchan", 50, 0);
--CreatePlayer("unylchan", 50, 300);

SetCamAttachedObj(GetPlayer().id);		-- ������� ������ � �������
SetCamAttachedAxis(true, true);		-- ������ ��������� ������ �� ��� X
SetCamFocusOnObjPos(constants.CamFocusBottomCenter);	-- ������ ������ �� ��������� ������� ���� �������
-- ��� ����� �������� y-���������� ������� ���� �� ��������, ����� �������, ������ �� ������ ���, ��������, ����������
SetCamObjOffset(0, 100);	-- ������ �������� ������ ������������ �������


local x = 0
local dx = 32

for i = 0,50 do
	CreateSprite("phys_floor", x + dx*i, 330);
end

local gticklbl = CreateWidget(constants.wt_Label, "gticklbl", nil, 20, 30, 70, 10)
WidgetSetCaption(gticklbl, "" .. CONFIG.gametick)
WidgetSetCaptionColor(gticklbl, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(gticklbl, {1.0, 0.2, 0.2, 1.0}, true)

function plus(sender)
	CONFIG.gametick = CONFIG.gametick + 10
	LoadConfig();
	WidgetSetCaption(gticklbl, "" .. CONFIG.gametick)
end

function minus(sender)
	if CONFIG.gametick == 10 then return end
	CONFIG.gametick = CONFIG.gametick - 10;
	LoadConfig();
	WidgetSetCaption(gticklbl, "" .. CONFIG.gametick)
end

function normal(sender)
	CONFIG.gametick = 10
	LoadConfig();
	WidgetSetCaption(gticklbl, "" .. CONFIG.gametick)
end

function slow(sender)
	CONFIG.gametick = 100
	LoadConfig();
	WidgetSetCaption(gticklbl, "" .. CONFIG.gametick)
end

local plusbtn = CreateWidget(constants.wt_Button, "plusbtn", nil, 20, 20, 70, 10)
WidgetSetCaption(plusbtn, "+")
WidgetSetCaptionColor(plusbtn, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(plusbtn, {1.0, 0.2, 0.2, 1.0}, true)
WidgetSetLMouseClickProc(plusbtn, plus)

local minusbtn = CreateWidget(constants.wt_Button, "minusbtn", nil, 40, 20, 70, 10)
WidgetSetCaption(minusbtn, "-")
WidgetSetCaptionColor(minusbtn, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(minusbtn, {1.0, 0.2, 0.2, 1.0}, true)
WidgetSetLMouseClickProc(minusbtn, minus)

local normalbtn = CreateWidget(constants.wt_Button, "normalbtn", nil, 60, 20, 70, 10)
WidgetSetCaption(normalbtn, "normal")
WidgetSetCaptionColor(normalbtn, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(normalbtn, {1.0, 0.2, 0.2, 1.0}, true)
WidgetSetLMouseClickProc(normalbtn, normal)

local slowbtn = CreateWidget(constants.wt_Button, "slowbtn", nil, 60, 30, 70, 10)
WidgetSetCaption(slowbtn, "slow")
WidgetSetCaptionColor(slowbtn, {1.0, 1.0, 1.0, 1.0}, false)
WidgetSetCaptionColor(slowbtn, {1.0, 0.2, 0.2, 1.0}, true)
WidgetSetLMouseClickProc(slowbtn, slow)