local LEVEL = {}
InitNewGame()
dofile("levels/pear15house_sphere.lua")

CONFIG.backcolor_r = 0.000000
CONFIG.backcolor_g = 0.000000
CONFIG.backcolor_b = 0.000000
LoadConfig()

StopBackMusic()

LEVEL.secrets = 3

function LEVEL.InitScript()
--$(INIT_SCRIPT)+
	SwitchLighting( false )
	Game.fade_out()
	mapvar.tmp.fade_widget_text = {}
	if GetPlayer() then
		local sx, sy = GetCaptionSize( "default", "������� 2" )
		local text1 = CreateWidget( constants.wt_Label, "TEXT", fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 - 1.5 * sy, 1, 1 )
		mapvar.tmp.fade_widget_text["text1"] = text1
		WidgetSetCaptionColor( text1, {1,1,1,1}, false )
		WidgetSetCaption( text1, "������� 2" )
		WidgetSetZ( text1, 1.05 )
		sx, sy = GetCaptionSize( "default", "�������, �����!" )
		local text2 = CreateWidget( constants.wt_Label, "TEXT", fade_widget, (CONFIG.scr_width - sx)/2, CONFIG.scr_height/2 + 1.5 * sy, 1, 1 )
		mapvar.tmp.fade_widget_text["text2"] = text2
		WidgetSetCaptionColor( text2, {1,1,1,1}, false )
		WidgetSetCaption( text2, "�������, �����!" )
		WidgetSetZ( text2, 1.05 )
		CamMoveToPos( -194,59 )
		SetCamAttachedAxis(true, true)
		SetCamFocusOnObjPos(constants.CamFocusBottomCenter)
		SetCamObjOffset(0, 60)
		SetCamLag(0.9)
		Game.AttachCamToPlayers( {
			{ {-2500,-9999,6000,-600},  {-2500,-9999,6000,9999} },
			{ {-250,-800,6000,9999},  {-250,-9999,6000,9999} },
		} )
		local env = CreateEnvironment('default_environment', -9001, -9001)
		SetDefaultEnvironment(env)
		Game.info.can_join = false
		mapvar.tmp.cutscene = true
		
		SetDefaultActor( 1 )
		EnablePlayerControl( false )
		if mapvar.actors[2] then
			SetDefaultActor( 2 )
			EnablePlayerControl( false )
			SetDefaultActor( 1 )
		end
		Menu.lock()
		local start_intro_thread = false
		local fade_thread = NewThread( function()
			Wait( 1 )
			GUI:hide()
			Wait( 1000 )
			mapvar.tmp.fade_complete = false
			Game.fade_in()
			start_intro_thread = true
		end )
		
		Resume( fade_thread )
		
		local intro_thread = NewThread( function()
			SetObjPos( mapvar.actors[1].char.id, -194, 64 )
			if mapvar.actors[2] then
				SetObjPos( mapvar.actors[2].char.id, -194-30, 64 )
			end
			while not start_intro_thread do
				Wait( 10 )
			end
			
			
			Wait( 200 )
			
			local cx, cy = GetCamPos()
			local actor = GetObjectUserdata(mapvar.actors[1].char.id)
			
			local cx, cy = GetCamPos()
			
			RemoveSkipKey()
			local conversation_end = function()
				local done = function()
					mapvar.tmp.cutscene = false
					EnablePlayerControl( true )
					Game.info.can_join = true
					SetCamLag(0.9)
					Menu.unlock()
					GUI:show()
					PlayBackMusic("music/iie_kmp.it")
					mapvar.tmp.heat_started = true
				end
				if difficulty >= 1 then
					if not Game.profile.tips then Game.profile.tips = {} end
					if not Game.profile.tips[2] then
						push_pause( true )
						Game.ShowTip( 6, true, function()
							Game.profile.tips[2] = true
							Saver:saveProfile()
							pop_pause()
							done()
						end )
					else
						done()
					end
				else
					done()
				end
			end
			Conversation.create( {
				function( var ) 
					if mapvar.actors[2] then
						return 12
					elseif mapvar.actors[1].info.character == "pear15unyl" then
						return 7
					end
				end,
				{ sprite = "portrait-soh" },
				"PEAR15_HOUSE_13",
				conversation_end,
				nil,
				nil,
				{ sprite = "portrait-unyl" },
				"PEAR15_HOUSE_14",
				conversation_end,
				nil,
				nil,
				{ sprite = "portrait-soh" },
				"PEAR15_HOUSE_15",
				{ sprite = "portrait-unyl" },
				"PEAR15_HOUSE_16",
				{ sprite = "portrait-soh" },
				"PEAR15_HOUSE_17",
				conversation_end,
				nil,
			} ):start(false)
		end)
		
		SetSkipKey( function( key )
			if key == CONFIG.key_conf[2].gui_nav_accept then
				return true
			end
			if not start_intro_thread then
				StopThread( fade_thread )
				GUI:hide()
				Game.reset_fade()
				start_intro_thread = true
			end
			StopThread( intro_thread )
			local done = function()
				mapvar.tmp.cutscene = false
				EnablePlayerControl( true )
				Game.info.can_join = true
				SetCamLag(0.9)
				Menu.unlock()
				GUI:show()
				PlayBackMusic("music/iie_kmp.it")
				mapvar.tmp.heat_started = true
			end
			if difficulty >= 1 then
				if not Game.profile.tips then Game.profile.tips = {} end
				if not Game.profile.tips[2] then
					push_pause( true )
					Game.ShowTip( 6, true, function()
						Game.profile.tips[2] = true
						Saver:saveProfile()
						pop_pause()
						done()
					end )
				else
					done()
				end
			else
				done()
			end
		end, 500 )
		
		Resume( intro_thread )
		
		if difficulty >= 1 then
			mapvar.tmp.heat_barb1 = CreateWidget( constants.wt_Picture, "", nil, 28, 58+50, 14, 255 )
			WidgetSetSprite( mapvar.tmp.heat_barb1, "pear15heat", "background" )
			WidgetSetSpriteColor( mapvar.tmp.heat_barb1, { 1, 1, 1, 0.9 } )
			mapvar.tmp.heat_bar1 = CreateWidget( constants.wt_Picture, "", mapvar.tmp.heat_barb1, 31, 61+50+75, 8, 150 )
			WidgetSetSprite( mapvar.tmp.heat_bar1, "pear15heat", "bar" )
			WidgetSetSpriteRenderMethod( mapvar.tmp.heat_bar1, constants.rsmCrop )
			WidgetSetSpriteColor( mapvar.tmp.heat_bar1, { 0.5, 0, 0.5, 0.9 } )
			WidgetSetSpriteAngle( mapvar.tmp.heat_bar1, math.pi )

			mapvar.tmp.heat_barb2 = CreateWidget( constants.wt_Picture, "", mapvar.tmp.heat_barb1, CONFIG.scr_width-28-14, 58+50, 14, 255 )
			WidgetSetSprite( mapvar.tmp.heat_barb2, "pear15heat", "background" )
			WidgetSetSpriteColor( mapvar.tmp.heat_barb2, { 1, 1, 1, 0.9 } )
			WidgetSetVisible( mapvar.tmp.heat_barb2, false )
			mapvar.tmp.heat_bar2 = CreateWidget( constants.wt_Picture, "", mapvar.tmp.heat_barb2, CONFIG.scr_width-25-14, 61+50+75, 8, 150 )
			WidgetSetSprite( mapvar.tmp.heat_bar2, "pear15heat", "bar" )
			WidgetSetSpriteRenderMethod( mapvar.tmp.heat_bar2, constants.rsmCrop )
			WidgetSetSpriteColor( mapvar.tmp.heat_bar2, { 0.5, 0, 0.5, 0.9 } )
			WidgetSetSpriteAngle( mapvar.tmp.heat_bar2, math.pi )

			local cleanup = function()
				if mapvar.tmp.heat_barb1 then
					DestroyWidget( mapvar.tmp.heat_barb1 )
				end
			end
			if not Game.cleanupFunctions then
				Game.cleanupFunctions = {}
			end
			Game.cleanupFunctions[ cleanup ] = true
			Resume( NewMapThread( function()
				local last_time
				Wait( 1 )
				last_time = Loader.time
				local heat1 = 0.5
				local heat1_target = 0.5
				local heat2 = 0.5
				local heat2_target = 0.5
				local dt
				local last_pos1 = GetPlayerCharacter(1):aabb().p
				local last_pos2 = { x = 0, y = 0 }
				local last_movement1 = 0
				local last_movement2 = 0
				local pos1
				local char
				local last_damage1 = 0
				local last_damage2 = 0

				local FLAME_HEAT_UP_SPEED = 0.1
				local MOVEMENT_HEAT_DOWN_SPEED = 0.025
				local IDLING_HEAT_UP_SPEED = 0.05

				if mapvar.actors[2] then
					last_pos2 = GetPlayerCharacter(2):aabb().p
				end
				while true do
					if mapvar.actors[2] then
						WidgetSetVisible( mapvar.tmp.heat_barb2, true )
					end
					dt = Loader.time - last_time
					last_time = Loader.time
					if mapvar.tmp.heat_started then
						if mapvar.tmp.pl1_in_heat then
							heat1_target = math.min( 1, heat1_target + FLAME_HEAT_UP_SPEED * dt / 1000 )
						end
						if mapvar.tmp.pl2_in_heat then
							heat2_target = math.min( 1, heat2_target + FLAME_HEAT_UP_SPEED * dt / 1000 )
						end
						char = GetPlayerCharacter(1)
						if char and char:object_present() then
							pos = char:aabb().p
							if math.abs( pos.x - last_pos1.x ) > 0 or math.abs( pos.y - last_pos1.y ) > 10 then
								last_movement1 = Loader.time
								heat1_target = math.max( 0, heat1_target - MOVEMENT_HEAT_DOWN_SPEED * dt / 1000 )
							end
							last_pos1 = pos
						end
						if mapvar.actors[2] then
							char = GetPlayerCharacter(2)
						else
							char = nil
						end
						if char and char:object_present() then
							pos = char:aabb().p
							if math.abs( pos.x - last_pos2.x ) > 0 or math.abs( pos.y - last_pos2.y ) > 10 then
								last_movement2 = Loader.time
								heat2_target = math.max( 0, heat2_target - MOVEMENT_HEAT_DOWN_SPEED * dt / 1000 )
								last_pos2 = pos
							end
						end
						if Loader.time - last_movement1 > 1000 then
							heat1_target = math.min( 1, heat1_target + IDLING_HEAT_UP_SPEED * dt / 1000 )
						end
						if mapvar.actors[2] and Loader.time - last_movement2 > 1000 then
							heat2_target = math.min( 1, heat2_target + IDLING_HEAT_UP_SPEED * dt / 1000 )
						end

						heat1 = heat1 + ( heat1_target - heat1 ) * 0.5 * dt / 200
						heat2 = heat2 + ( heat2_target - heat2 ) * 0.5 * dt / 200

						char = GetPlayerCharacter( 1 )
						if char and char:object_present() and char:health() <= 0 then
							heat1 = 0.5
							heat1_target = 0.5
						end
						char = GetPlayerCharacter( 2 )
						if char and char:object_present() and char:health() <= 0 then
							heat2 = 0.5
							heat2_target = 0.5
						end
					end

					WidgetSetSize( mapvar.tmp.heat_bar1, 8, 150 * heat1 )
					WidgetSetPos( mapvar.tmp.heat_bar1, 31, 61+50+150 * (1-heat1) )
					WidgetSetSpriteColor( mapvar.tmp.heat_bar1, { heat1, 0, 1-heat1, 0.9 } )
					if heat1 >= 0.9 and Loader.time - last_damage1 > 100 then
						last_damage1 = Loader.time
						char = GetPlayerCharacter(1)
						if char and char:object_present() then
							DamageObject( char, 20 )
						end
					end
					if mapvar.actors[2] then
						WidgetSetSize( mapvar.tmp.heat_bar2, 8, 150 * heat2 )
						WidgetSetPos( mapvar.tmp.heat_bar2, CONFIG.scr_width-25-14, 61+50+150 * (1-heat2) )
						WidgetSetSpriteColor( mapvar.tmp.heat_bar2, { heat2, 0, 1-heat2, 0.9 } )
						if heat2 >= 0.9 and Loader.time - last_damage2 > 100 then
							last_damage2 = Loader.time
							char = GetPlayerCharacter(2)
							if char and char:object_present() then
								DamageObject( char, 20 )
							end
						end
					end
					if heat1 < 0.01 or heat2 < 0.01 then
						Game.ProgressAchievement( "KEEPING_COOL", 1 )
					end
					Wait( 1 )
				end
			end ))
		end
	end
	
--$(INIT_SCRIPT)-
	Loader.level.SpecialCreation()
end

function LEVEL.MapScript(player)
--$(MAP_SCRIPT)+
	
--$(MAP_SCRIPT)-
end

function LEVEL.WeaponBonus()
--$(WEAPON_BONUS)+
--$(WEAPON_BONUS)-
end

function LEVEL.placeSecrets()
--$(PLACE_SECRETS)+
--$(PLACE_SECRETS)-
end

function LEVEL.removeSecrets()
--$(REMOVE_SECRETS)+
--$(REMOVE_SECRETS)-
end

function LEVEL.SpecialCreation()
--$(SPECIAL_CREATION)+
	local obj1 = CreateItem( 'generic-trigger', 5973, -504 )
	local obj2 = CreateItem( 'generic-trigger', 6080, -197 )
	GroupObjects( obj1, obj2 )
	local obj3 = CreateItem( 'generic-trigger', 408, -1287 )
	local obj4 = CreateItem( 'generic-trigger', 668, -987 )
	GroupObjects( obj3, obj4 )
	local obj5 = CreateEnemy( 'flamer-angled', -354, -2047 )
	SetObjSpriteMirrored( obj5, false )
	local obj6 = CreateEnemy( 'flamer-angled', 611, -1544 )
	SetObjSpriteMirrored( obj6, false )
	local obj7 = CreateEnemy( 'flamer-angled', 1272, -1544 )
	SetObjSpriteMirrored( obj7, false )
	local obj8 = CreateEnemy( 'flamer-angled', 2156, -1699 )
	SetObjSpriteMirrored( obj8, false )
	local obj9 = CreateEnemy( 'flamer-angled', 1934, -1863 )
	SetObjSpriteMirrored( obj9, false )
	local obj10 = CreateEnemy( 'flamer-angled', 2732, -1616 )
	SetObjSpriteMirrored( obj10, false )
	local obj11 = CreateEnemy( 'flamer-angled', 2523, -1748 )
	SetObjSpriteMirrored( obj11, false )
	local obj12 = CreateEnemy( 'flamer-angled', 3686, -1056 )
	SetObjSpriteMirrored( obj12, false )
	local obj13 = CreateEnemy( 'flamer-angled', 4344, -263 )
	SetObjSpriteMirrored( obj13, false )
	local obj14 = CreateEnemy( 'flamer-angled', 5034, -1056 )
	SetObjSpriteMirrored( obj14, false )
	local obj15 = CreateEnemy( 'flamer-angled', 4692, -512 )
	SetObjSpriteMirrored( obj15, false )
	local obj16 = CreateEnemy( 'flamer-angled', 5788, -534 )
	SetObjSpriteMirrored( obj16, false )
	local obj17 = CreateItem( 'generic-trigger', 215, -600 )
	local obj18 = CreateItem( 'generic-trigger', 395, -187 )
	GroupObjects( obj17, obj18 )
	local obj19 = CreateItem( 'generic-trigger', -280, -1299 )
	local obj20 = CreateItem( 'generic-trigger', -38, -953 )
	GroupObjects( obj19, obj20 )
	local obj21 = CreateItem( 'generic-trigger', -1176, -1350 )
	local obj22 = CreateItem( 'generic-trigger', -820, -932 )
	GroupObjects( obj21, obj22 )
	local obj23 = CreateItem( 'generic-trigger', 290, -847 )
	local obj24 = CreateItem( 'generic-trigger', 995, -754 )
	GroupObjects( obj23, obj24 )
	local obj25 = CreateItem( 'generic-trigger', -2384, -1390 )
	local obj26 = CreateItem( 'generic-trigger', -1995, -986 )
	GroupObjects( obj25, obj26 )
	local obj27 = CreateItem( 'generic-trigger', -1594, -2105 )
	local obj28 = CreateItem( 'generic-trigger', -1426, -1514 )
	GroupObjects( obj27, obj28 )
	local obj29 = CreateItem( 'generic-trigger', -537, -2093 )
	local obj30 = CreateItem( 'generic-trigger', -153, -1715 )
	GroupObjects( obj29, obj30 )
	local obj31 = CreateItem( 'generic-trigger', 164, -2104 )
	local obj32 = CreateItem( 'generic-trigger', 427, -1493 )
	GroupObjects( obj31, obj32 )
	local obj33
	local obj34
	if difficulty > 1 then
		obj33 = CreateEnemy( 'flamer-angled', 244, -205 )
		SetObjSpriteMirrored( obj33, false )
		obj34 = CreateEnemy( 'flamer-angled', 917, -722 )
		SetObjSpriteMirrored( obj34, false )
	end
	local obj35 = CreateItem( 'generic-trigger', 1122, -2136 )
	local obj36 = CreateItem( 'generic-trigger', 1595, -1511 )
	GroupObjects( obj35, obj36 )
	local obj37 = CreateItem( 'generic-trigger', 1910, -1858 )
	local obj38 = CreateItem( 'generic-trigger', 2203, -1665 )
	GroupObjects( obj37, obj38 )
	local obj39 = CreateItem( 'generic-trigger', 2374, -1762 )
	local obj40 = CreateItem( 'generic-trigger', 2557, -1479 )
	GroupObjects( obj39, obj40 )
	local obj41 = CreateItem( 'generic-trigger', 3094, -1396 )
	local obj42 = CreateItem( 'generic-trigger', 3669, -1235 )
	GroupObjects( obj41, obj42 )
	local obj43 = CreateItem( 'generic-trigger', 2986, -1048 )
	local obj44 = CreateItem( 'generic-trigger', 3807, -749 )
	GroupObjects( obj43, obj44 )
	local obj45 = CreateItem( 'generic-trigger', 4154, -1118 )
	local obj46 = CreateItem( 'generic-trigger', 4477, -213 )
	GroupObjects( obj45, obj46 )
	local obj47 = CreateItem( 'generic-trigger', 4918, -1134 )
	local obj48 = CreateItem( 'generic-trigger', 5267, -189 )
	GroupObjects( obj47, obj48 )
	local obj49 = CreateItem( 'generic-trigger', 5654, -1148 )
	local obj50 = CreateItem( 'generic-trigger', 5925, -149 )
	GroupObjects( obj49, obj50 )
	local obj51 = CreateEnvironment( 'heat', -305, 87 )
	local obj52 = CreateEnvironment( 'heat', 291, 145 )
	GroupObjects( obj51, obj52 )
	local obj53 = CreateEnvironment( 'heat', 332, -280 )
	local obj54 = CreateEnvironment( 'heat', 483, -188 )
	GroupObjects( obj53, obj54 )
	local obj55 = CreateEnvironment( 'heat', 717, -265 )
	local obj56 = CreateEnvironment( 'heat', 802, -201 )
	GroupObjects( obj55, obj56 )
	local obj57 = CreateEnvironment( 'heat', 674, -424 )
	local obj58 = CreateEnvironment( 'heat', 766, -368 )
	GroupObjects( obj57, obj58 )
	local obj59 = CreateEnvironment( 'heat', 1021, -588 )
	local obj60 = CreateEnvironment( 'heat', 1086, -526 )
	GroupObjects( obj59, obj60 )
	local obj61 = CreateEnvironment( 'heat', 336, -584 )
	local obj62 = CreateEnvironment( 'heat', 428, -516 )
	GroupObjects( obj61, obj62 )
	local obj63 = CreateEnvironment( 'heat', 570, -729 )
	local obj64 = CreateEnvironment( 'heat', 638, -664 )
	GroupObjects( obj63, obj64 )
	local obj65 = CreateEnvironment( 'heat', 567, -1036 )
	local obj66 = CreateEnvironment( 'heat', 702, -966 )
	GroupObjects( obj65, obj66 )
	local obj67 = CreateEnvironment( 'heat', -136, -1046 )
	local obj68 = CreateEnvironment( 'heat', -2, -984 )
	GroupObjects( obj67, obj68 )
	local obj69 = CreateEnvironment( 'heat', -615, -1118 )
	local obj70 = CreateEnvironment( 'heat', -533, -1080 )
	GroupObjects( obj69, obj70 )
	local obj71 = CreateEnvironment( 'heat', -542, -1010 )
	local obj72 = CreateEnvironment( 'heat', -429, -973 )
	GroupObjects( obj71, obj72 )
	local obj73 = CreateEnvironment( 'heat', -855, -1038 )
	local obj74 = CreateEnvironment( 'heat', -780, -1009 )
	GroupObjects( obj73, obj74 )
	local obj75 = CreateEnvironment( 'heat', -1062, -1131 )
	local obj76 = CreateEnvironment( 'heat', -973, -1092 )
	GroupObjects( obj75, obj76 )
	local obj77 = CreateEnvironment( 'heat', -1520, -1055 )
	local obj78 = CreateEnvironment( 'heat', -1312, -978 )
	GroupObjects( obj77, obj78 )
	local obj79 = CreateEnvironment( 'heat', -2264, -1037 )
	local obj80 = CreateEnvironment( 'heat', -2063, -1009 )
	GroupObjects( obj79, obj80 )
	local obj81 = CreateEnvironment( 'heat', -1908, -1576 )
	local obj82 = CreateEnvironment( 'heat', -1865, -1532 )
	GroupObjects( obj81, obj82 )
	local obj83 = CreateEnvironment( 'heat', -1749, -1586 )
	local obj84 = CreateEnvironment( 'heat', -1626, -1537 )
	GroupObjects( obj83, obj84 )
	local obj85 = CreateEnvironment( 'heat', -1457, -1566 )
	local obj86 = CreateEnvironment( 'heat', -1378, -1532 )
	GroupObjects( obj85, obj86 )
	local obj87 = CreateEnvironment( 'heat', -1239, -1751 )
	local obj88 = CreateEnvironment( 'heat', -1160, -1689 )
	GroupObjects( obj87, obj88 )
	local obj89 = CreateEnvironment( 'heat', -523, -1584 )
	local obj90 = CreateEnvironment( 'heat', -477, -1551 )
	GroupObjects( obj89, obj90 )
	local obj91 = CreateEnvironment( 'heat', -415, -1578 )
	local obj92 = CreateEnvironment( 'heat', -302, -1553 )
	GroupObjects( obj91, obj92 )
	local obj93 = CreateEnvironment( 'heat', -549, -1861 )
	local obj94 = CreateEnvironment( 'heat', -518, -1831 )
	GroupObjects( obj93, obj94 )
	local obj95 = CreateEnvironment( 'heat', -318, -1695 )
	local obj96 = CreateEnvironment( 'heat', -285, -1673 )
	GroupObjects( obj95, obj96 )
	local obj97 = CreateEnvironment( 'heat', 156, -1581 )
	local obj98 = CreateEnvironment( 'heat', 367, -1544 )
	GroupObjects( obj97, obj98 )
	local obj99 = CreateEnvironment( 'heat', 472, -1857 )
	local obj100 = CreateEnvironment( 'heat', 500, -1823 )
	GroupObjects( obj99, obj100 )
	local obj101 = CreateEnvironment( 'heat', 563, -1582 )
	local obj102 = CreateEnvironment( 'heat', 652, -1542 )
	GroupObjects( obj101, obj102 )
	local obj103 = CreateEnvironment( 'heat', 719, -1861 )
	local obj104 = CreateEnvironment( 'heat', 752, -1821 )
	GroupObjects( obj103, obj104 )
	local obj105 = CreateEnvironment( 'heat', 1041, -1860 )
	local obj106 = CreateEnvironment( 'heat', 1111, -1829 )
	GroupObjects( obj105, obj106 )
	local obj107 = CreateEnvironment( 'heat', 937, -1575 )
	local obj108 = CreateEnvironment( 'heat', 969, -1557 )
	GroupObjects( obj107, obj108 )
	local obj109 = CreateEnvironment( 'heat', 1225, -1577 )
	local obj110 = CreateEnvironment( 'heat', 1305, -1545 )
	GroupObjects( obj109, obj110 )
	local obj111 = CreateEnvironment( 'heat', 1547, -1577 )
	local obj112 = CreateEnvironment( 'heat', 1700, -1537 )
	GroupObjects( obj111, obj112 )
	local obj113 = CreateEnvironment( 'heat', 1866, -1573 )
	local obj114 = CreateEnvironment( 'heat', 1922, -1530 )
	GroupObjects( obj113, obj114 )
	local obj115 = CreateEnvironment( 'heat', 2161, -1901 )
	local obj116 = CreateEnvironment( 'heat', 2251, -1846 )
	GroupObjects( obj115, obj116 )
	local obj117 = CreateEnvironment( 'heat', 2422, -1565 )
	local obj118 = CreateEnvironment( 'heat', 2453, -1542 )
	GroupObjects( obj117, obj118 )
	local obj119 = CreateEnvironment( 'heat', -9122, -9031 )
	local obj120 = CreateEnvironment( 'heat', -8878, -8968 )
	GroupObjects( obj119, obj120 )
	local obj121 = CreateEnvironment( 'heat', 2507, -1860 )
	local obj122 = CreateEnvironment( 'heat', 2736, -1831 )
	GroupObjects( obj121, obj122 )
	local obj123 = CreateEnvironment( 'heat', 2815, -1574 )
	local obj124 = CreateEnvironment( 'heat', 2989, -1535 )
	GroupObjects( obj123, obj124 )
	local obj125 = CreateEnvironment( 'heat', 3314, -1350 )
	local obj126 = CreateEnvironment( 'heat', 3456, -1311 )
	GroupObjects( obj125, obj126 )
	local obj127 = CreateEnvironment( 'heat', 3174, -961 )
	local obj128 = CreateEnvironment( 'heat', 3274, -932 )
	GroupObjects( obj127, obj128 )
	local obj129 = CreateEnvironment( 'heat', 2966, -845 )
	local obj130 = CreateEnvironment( 'heat', 3008, -802 )
	GroupObjects( obj129, obj130 )
	local obj131 = CreateEnvironment( 'heat', 2820, -264 )
	local obj132 = CreateEnvironment( 'heat', 2893, -231 )
	GroupObjects( obj131, obj132 )
	local obj133 = CreateEnvironment( 'heat', 3147, -263 )
	local obj134 = CreateEnvironment( 'heat', 3287, -222 )
	GroupObjects( obj133, obj134 )
	local obj135 = CreateEnvironment( 'heat', 3655, -266 )
	local obj136 = CreateEnvironment( 'heat', 3829, -229 )
	GroupObjects( obj135, obj136 )
	local obj137 = CreateEnvironment( 'heat', 3624, -538 )
	local obj138 = CreateEnvironment( 'heat', 3651, -510 )
	GroupObjects( obj137, obj138 )
	local obj139 = CreateEnvironment( 'heat', 3986, -735 )
	local obj140 = CreateEnvironment( 'heat', 4012, -708 )
	GroupObjects( obj139, obj140 )
	local obj141 = CreateEnvironment( 'heat', 4106, -270 )
	local obj142 = CreateEnvironment( 'heat', 4137, -230 )
	GroupObjects( obj141, obj142 )
	local obj143 = CreateEnvironment( 'heat', 4291, -267 )
	local obj144 = CreateEnvironment( 'heat', 4383, -219 )
	GroupObjects( obj143, obj144 )
	local obj145 = CreateEnvironment( 'heat', 4424, -254 )
	local obj146 = CreateEnvironment( 'heat', 4463, -222 )
	GroupObjects( obj145, obj146 )
	local obj147 = CreateEnvironment( 'heat', 4390, -573 )
	local obj148 = CreateEnvironment( 'heat', 4415, -540 )
	GroupObjects( obj147, obj148 )
	local obj149 = CreateEnvironment( 'heat', 4245, -742 )
	local obj150 = CreateEnvironment( 'heat', 4277, -705 )
	GroupObjects( obj149, obj150 )
	local obj151 = CreateEnvironment( 'heat', 4453, -742 )
	local obj152 = CreateEnvironment( 'heat', 4483, -711 )
	GroupObjects( obj151, obj152 )
	local obj153 = CreateEnvironment( 'heat', 4685, -736 )
	local obj154 = CreateEnvironment( 'heat', 4767, -703 )
	GroupObjects( obj153, obj154 )
	local obj155 = CreateEnvironment( 'heat', 4516, -379 )
	local obj156 = CreateEnvironment( 'heat', 4540, -351 )
	GroupObjects( obj155, obj156 )
	local obj157 = CreateEnvironment( 'heat', 4715, -387 )
	local obj158 = CreateEnvironment( 'heat', 4798, -352 )
	GroupObjects( obj157, obj158 )
	local obj159 = CreateEnvironment( 'heat', 4751, -252 )
	local obj160 = CreateEnvironment( 'heat', 4785, -221 )
	GroupObjects( obj159, obj160 )
	local obj161 = CreateEnvironment( 'heat', 4886, -738 )
	local obj162 = CreateEnvironment( 'heat', 4922, -703 )
	GroupObjects( obj161, obj162 )
	local obj163 = CreateEnvironment( 'heat', 4949, -264 )
	local obj164 = CreateEnvironment( 'heat', 5089, -229 )
	GroupObjects( obj163, obj164 )
	local obj165 = CreateEnvironment( 'heat', 5097, -577 )
	local obj166 = CreateEnvironment( 'heat', 5191, -540 )
	GroupObjects( obj165, obj166 )
	local obj167 = CreateEnvironment( 'heat', 5160, -732 )
	local obj168 = CreateEnvironment( 'heat', 5192, -698 )
	GroupObjects( obj167, obj168 )
	local obj169 = CreateEnvironment( 'heat', 5445, -932 )
	local obj170 = CreateEnvironment( 'heat', 5505, -889 )
	GroupObjects( obj169, obj170 )
	local obj171 = CreateEnvironment( 'heat', 5670, -925 )
	local obj172 = CreateEnvironment( 'heat', 5729, -894 )
	GroupObjects( obj171, obj172 )
	local obj173 = CreateEnvironment( 'heat', 5529, -565 )
	local obj174 = CreateEnvironment( 'heat', 5566, -533 )
	GroupObjects( obj173, obj174 )
	local obj175 = CreateEnvironment( 'heat', 5434, -380 )
	local obj176 = CreateEnvironment( 'heat', 5468, -348 )
	GroupObjects( obj175, obj176 )
	local obj177 = CreateEnvironment( 'heat', 5565, -264 )
	local obj178 = CreateEnvironment( 'heat', 5631, -231 )
	GroupObjects( obj177, obj178 )
	local obj179 = CreateEnvironment( 'heat', 5683, -261 )
	local obj180 = CreateEnvironment( 'heat', 5711, -226 )
	GroupObjects( obj179, obj180 )
	local obj181 = CreateEnvironment( 'heat', 5806, -258 )
	local obj182 = CreateEnvironment( 'heat', 6014, -209 )
	GroupObjects( obj181, obj182 )
	local obj183 = CreateItem( 'generic-trigger', 947, -362 )
	local obj184 = CreateItem( 'generic-trigger', 1102, -182 )
	GroupObjects( obj183, obj184 )
	local obj185 = CreateItem( 'generic-trigger', 2825, -1649 )
	local obj186 = CreateItem( 'generic-trigger', 2944, -1520 )
	GroupObjects( obj185, obj186 )
	local obj187 = CreateItem( 'generic-trigger', 2831, -931 )
	local obj188 = CreateItem( 'generic-trigger', 2944, -800 )
	GroupObjects( obj187, obj188 )
	local object
	ObjectPushInt( obj1, 2 )
	ObjectPushInt( obj3, 3 )
	ObjectPushInt( obj5, 90 )
	ObjectPushInt( obj5, 100 )
	ObjectPushInt( obj5, 1500 )
	ObjectPushInt( obj5, 15 )
	ObjectPushInt( obj6, -90 )
	ObjectPushInt( obj6, 100 )
	ObjectPushInt( obj6, 1500 )
	ObjectPushInt( obj6, 15 )
	ObjectPushInt( obj7, -90 )
	ObjectPushInt( obj7, 100 )
	ObjectPushInt( obj7, 1500 )
	ObjectPushInt( obj7, 15 )
	ObjectPushInt( obj8, 180 )
	ObjectPushInt( obj8, 100 )
	ObjectPushInt( obj8, 1000 )
	ObjectPushInt( obj8, 20 )
	ObjectPushInt( obj9, 0 )
	ObjectPushInt( obj9, 100 )
	ObjectPushInt( obj9, 1000 )
	ObjectPushInt( obj9, 15 )
	ObjectPushInt( obj10, 180 )
	ObjectPushInt( obj10, 150 )
	ObjectPushInt( obj10, 1500 )
	ObjectPushInt( obj10, 20 )
	ObjectPushInt( obj11, 0 )
	ObjectPushInt( obj11, 100 )
	ObjectPushInt( obj11, 1000 )
	ObjectPushInt( obj11, 20 )
	ObjectPushInt( obj12, 90 )
	ObjectPushInt( obj12, 150 )
	ObjectPushInt( obj12, 1500 )
	ObjectPushInt( obj12, 10 )
	ObjectPushInt( obj13, -90 )
	ObjectPushInt( obj13, 200 )
	ObjectPushInt( obj13, 2000 )
	ObjectPushInt( obj13, 20 )
	ObjectPushInt( obj14, 90 )
	ObjectPushInt( obj14, 250 )
	ObjectPushInt( obj14, 1500 )
	ObjectPushInt( obj14, 15 )
	ObjectPushInt( obj15, 0 )
	ObjectPushInt( obj15, 150 )
	ObjectPushInt( obj15, 1000 )
	ObjectPushInt( obj15, 10 )
	ObjectPushInt( obj16, 180 )
	ObjectPushInt( obj16, 150 )
	ObjectPushInt( obj16, 1000 )
	ObjectPushInt( obj16, 10 )
	ObjectPushInt( obj17, 4 )
	ObjectPushInt( obj19, 4 )
	ObjectPushInt( obj21, 4 )
	ObjectPushInt( obj23, 4 )
	ObjectPushInt( obj25, 4 )
	ObjectPushInt( obj27, 4 )
	ObjectPushInt( obj29, 4 )
	ObjectPushInt( obj31, 4 )
	if difficulty > 1 then
		ObjectPushInt( obj33, 180 )
		ObjectPushInt( obj33, 100 )
		ObjectPushInt( obj33, 2000 )
		ObjectPushInt( obj33, 15 )
		ObjectPushInt( obj34, 135 )
		ObjectPushInt( obj34, 150 )
		ObjectPushInt( obj34, 1000 )
		ObjectPushInt( obj34, 15 )
	end
	ObjectPushInt( obj35, 4 )
	ObjectPushInt( obj37, 4 )
	ObjectPushInt( obj39, 4 )
	ObjectPushInt( obj41, 4 )
	ObjectPushInt( obj43, 4 )
	ObjectPushInt( obj45, 4 )
	ObjectPushInt( obj47, 4 )
	ObjectPushInt( obj49, 4 )
	ObjectPushInt( obj183, 1 )
	ObjectPushInt( obj185, 1 )
	ObjectPushInt( obj187, 1 )
--$(SPECIAL_CREATION)-
end

function LEVEL.GetNextCharacter()
--$(GET_NEXT_CHARACTER)+
--$(GET_NEXT_CHARACTER)-
end

function map_trigger(id, trigger)
--$(MAP_TRIGGER)+
	if trigger == 1 then
			mapvar.tmp.secrets = ( mapvar.tmp.secrets or 0 ) + 1
	elseif trigger == 2 then
			Game.ShowLevelResults("pear15house_boss") 
	elseif trigger == 3 then
			local last_time = Loader.time
			local last_explosions = 0
			local x = 905
			local dt = 0
			Resume( NewMapThread( function()
				while true do
					dt = Loader.time - last_time
					last_time = Loader.time
					x = x - dt * 0.2	
					if x <= -2292 then
						return
					end
					if Loader.time - last_explosions > 350 then
						last_explosions = Loader.time
						for i = 1, math.random(4, 6) do
							CreateEnemy( "big_explosion", x, math.random( -1268, -1014 ) )
						end
					end
					Wait(1)
				end
			end ))
	elseif trigger == 4 then
			local x, y = GetCamPos()
			CreateEnemy( "big_explosion", x, y - CONFIG.scr_height/2 + 10 )
			y = y - (CONFIG.scr_height/2) - 50
			x = x - (CONFIG.scr_width/2) - 10
			for i = 1, 6 do
				SetObjAnim( CreateEnemy( "house_debris", x + math.random(0, CONFIG.scr_width + 100 ), y + math.random( -200, 20 ) ), "big", false )
			end
			for i = 1, 20 do
				CreateEnemy( "house_debris", x + math.random(0, CONFIG.scr_width/2 + 10 ), y + math.random( -20, 20 ) )
			end
	end
--$(MAP_TRIGGER)-
end

LEVEL.characters = {"pear15soh",}
LEVEL.spawnpoints = {{-138,60},{-166,59.5},{-194,59},}

function LEVEL.SetLoader(l)
	LEVEL.missionStarter=l
end

LEVEL.name = 'House'

LEVEL.hints =
{
	"����� �������!",
	"����� �� ��������������� ������ ���!",
	"����������� ��������� ������������ � ����!",
	"�� ����� ������ ���� ���� ����, ������� ����� ������ ������� ���������!"
}

LEVEL.solution = '���� � �� ����, ��� ����� ������� �� ����� ������. �� ���������� ��������. ���� ����, ���� ����� � �������. ����� ������ ������������ �������� ����������.'

return LEVEL
