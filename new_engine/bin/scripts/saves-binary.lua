if not savedata then 
	Log("[BINARY SAVE]: no savedata!")
	return 
end
require("serialize")
require("routines")

math.randomseed( os.time() )

local FILE_MODE = { r = "r", w = "w", i = "r", pr = "r", pw = "w", pn = "r" }
local VERSION = 1
local HEADER = "iiC SAEV GAEM "
local PROFILE_HEADER = "iiC PROF SAEV "
--local KEY = 2*math.random( 0, 127 )+1
local KEY = 1
local SAVETEXT = ""
local savefile = io.open( savedata.filename or "BAD FILENAME", FILE_MODE[savedata.mode or "i"].."b" )
local POSTHEADER_LENGTH = 5
local CONTROL_SIZE = 3
local SAFE_CONTROL_SIZE
local DECRYPTED_SAVETEXT
local CONTROL_SAVEDATA
local SAVETEXT_SIZE

if not savefile then
	Log("[BINARY SAVE] Cannot open file!")
	return
end


local function rc4(key, str)
	local function _bit()
		local function memoize(f)
			local mt = {}
			local t = setmetatable({}, mt)
			function mt:__index(k)
				local v = f(k); t[k] = v
				return v
			end
			return t
		end

		local function make_bitop_uncached(t, m)
			local function bitop(a, b)
				local res,p = 0,1
				while a ~= 0 and b ~= 0 do
					local am, bm = a%m, b%m
					res = res + t[am][bm]*p
					a = (a - am) / m
					b = (b - bm) / m
					p = p*m
				end
				res = res + (a+b)*p
				return res
			end
		  return bitop
		end

		local function make_bitop(t)
				local op1 = make_bitop_uncached(t,2^1)
				local op2 = memoize(function(a)
				return memoize(function(b)
					return op1(a, b)
				end)
			end)
			return make_bitop_uncached(op2, 2^(t.n or 1))
		end

		local bxor = make_bitop {[0]={[0]=0,[1]=1},[1]={[0]=1,[1]=0}, n=4}
		local F8 = 2^32 - 1
		local function bnot(a) return F8 - a end
		local function band(a,b) return ((a+b) - bxor(a,b))/2 end
		local function bor(a,b) return F8 - band(F8 - a, F8 - b) end

		local M = {}
		M.bxor = bxor
		M.bnot = bnot
		M.band = band
		M.bor = bor

		return M

	end
	local bitop = _bit()

	local function swap_in_tbl(S, i, j)
		local t = S[i]
		S[i] = S[j]
		S[j] = t
	end

	local function key_shedule(k)
		local keylength = #k
		local S = {}
		for i = 0, 255 do
			S[i] = i
		end
		local j = 0
		for i = 0, 255 do
			j = (j + S[i] + string.byte(k, (i % keylength) + 1)) % 256
			swap_in_tbl(S, i, j)
		end
		return S
	end

	local function get_generator(k)
		local S = key_shedule(k)
		local i = 0
		local j = 0
		return function ()
			i = (i + 1) % 256
			j = (j + S[i]) % 256
			swap_in_tbl(S, i, j)
			return S[(S[i] + S[j]) % 256]
		end
	end

	local function string_iter(s)
		local i = 0
		return function ()
			i = i + 1
			return string.byte(s, i)
		end
	end
	
	local res = {}
	local rc4_stream = get_generator(key)
	for c in string_iter(str) do
		local k = bitop.bxor(c, rc4_stream())
		table.insert(res, string.char(k))
	end
	return table.concat( res )
end

local function save( str )
	SAVETEXT = SAVETEXT..str
	savefile:write( str )
end

local function writeHeader( header )
	save( header )
	save( string.char(KEY) .. " " )
	save( string.char(math.floor(VERSION/256)) )
	save( string.char(VERSION) )
end

local function writeString( str )
	save( str )
end

local function encryptString( str )
	local enc = rc4( string.char(KEY), str )
	writeString( enc )
end

local function decryptString()
	-- Log("SAVETEXT_SIZE - CONTROL_SIZE ",SAVETEXT_SIZE - CONTROL_SIZE)
	local dec = string.sub(SAVETEXT, 1, SAVETEXT_SIZE - CONTROL_SIZE + 1)
	-- local fd = io.open("saves/out.txt", "wb")
	-- fd:write(dec)
	-- fd:close()
	DECRYPTED_SAVETEXT = rc4( string.char(KEY), dec )
	-- local fd = io.open("saves/out1.txt", "wb")
	-- fd:write(DECRYPTED_SAVETEXT)
	-- fd:close()
end

local function readFile()
	savefile:seek("set")
	SAVETEXT_SIZE = savefile:seek("end")
	savefile:seek("set")
	
	SAVETEXT = savefile:read("*all")
	CONTROL_SAVEDATA = SAVETEXT
	-- local i = #HEADER+1
	-- SAVETEXT = string.sub(SAVETEXT, i)
end

local function convertVersion( old_version, header )
	local local_version
	while local_version < VERSION do
		if local_version == 1 then
			SAVEDATA = string.sub( SAVEDATA, #header + POSTHEADER_LENGTH )
		end
		local_version = local_version + 1
	end
end

local function readHeader( header )
	if SAVETEXT_SIZE < #header + POSTHEADER_LENGTH + CONTROL_SIZE then
		Log("[BINARY SAVE] Bad file "..savedata.filename..", your savegame may be corrupt.")
		return false
	end
	local seq = string.sub( SAVETEXT, 1, #header )
	if seq ~= header then
		Log("[BINARY SAVE] Bad header  1 in file "..savedata.filename..", your savegame may be corrupt.")
		return false;
	end
	KEY = tonumber( string.byte(SAVETEXT, #header+1) )
	--local seq = 256*(tonumber(string.byte(SAVETEXT, #HEADER+3)) or -1) + (tonumber(string.byte(SAVETEXT, #HEADER+4)) or -1)
	local ver = tonumber(string.byte(SAVETEXT, #header+3))
	local seq = tonumber(string.byte(SAVETEXT, #header+4))
	if math.floor(seq/256) ~= ver then
		Log("[BINARY SAVE] Bad header 2  in file "..savedata.filename..", your savegame may be corrupt.")
		return false;
	end
	if seq > VERSION then
		Log("[BINARY SAVE] Save format version is wrong, possibly too new for this engine version or corrupted.")
		return false;
	elseif seq < VERSION then
		convertVersion( seq, header )
	else
		SAVETEXT = string.sub( SAVETEXT, #header+POSTHEADER_LENGTH )
		SAVETEXT_SIZE = SAVETEXT_SIZE - #header+POSTHEADER_LENGTH
	end
	return true;
end



local function controlBytes( str, size )
	local control1 = 0
	local control2 = 1
	for i=1, size do
		control1 = (control1 + string.byte( str, i )) % 256
		control2 = ((control2 * (string.byte( str, i )+1)) % 256)+1
	end
	return { control1%256, control2%256, (control1+control2) % 256 }
end

local function formSave()
	local save = {}
	save.difficulty = difficulty or 1
	save.nextLevelName = (Loader or {map_name = "NO LOADER"} ).map_name or "NO MAP NAME"
	
	local gm = {}
	if Game then
		if Game.AbsorbMapvar then Game.AbsorbMapvar() end
		
		gm.name = Game._game_name
		gm.info = deep_copy(Game.info)
		gm.actors = deep_copy(Game.actors)
		gm.info.score_locked = GetScoreLock()
	end
	
	-- gm.info.game_timer = 0
	--Log(serialize("gm", gm))
	
	save.game = gm
	
	save.info = saveinfo or {}
	save.info.date = os.date()
	
	savedata.data = "local "..serialize("save", save, true, true).."return save"
	--Log("formSave:\n", savedata.data )
end

local function formProfile()
	local profile = Game.profile
	savedata.data = "local "..serialize("profile", profile, true, true).." return profile"
end

local function applySave( mode )

	local env = {}
	local untrusted_function, message = loadstring(savedata.data)
	--if not untrusted_function then return nil end
	if not untrusted_function then Log("[SAVE] Error: ", message); return; end
	setfenv(untrusted_function, env)
	local success, save = pcall(untrusted_function)
	-- if not success then return end
	if not success then Log("[SAVE] Error loading save file"); return; end

	if mode == "r" then		
		if Game then
			if Game._game_name ~= save.game.name then
				Log("[SAVE] Wrong game")
				return
			end
			
			Game.info = deep_copy(save.game.info)
			Game.actors = deep_copy(save.game.actors)
			if save.game.score_lock then
				LockScore()
			end
			if Game.actors[2] then
				Game.info.second_player_needed = true
			end
			vars.character = Game.actors[1].info.character
		end
		
		difficulty = save.difficulty or 1
		for k, v in ipairs( Game.actors ) do
			ProtectedVariable( Game.variables.score_start + k, v.info.score )
		end
		
		if Loader then
			Loader.startGame(save.nextLevelName, true)
		end
		return nil
	else
		return save.info or {}
	end
end

local function applyProfile( mode )

	local env = {}
	local untrusted_function, message = loadstring(savedata.data)
	if not untrusted_function then Log("[SAVE] Error: ", message); return; end
	setfenv(untrusted_function, env)
	local success, profile = pcall(untrusted_function)
	if not success then Log("[SAVE] Error loading profile file"); return; end

	if mode == "pr" then		
		if Game then
			Game.profile = deep_copy( profile )
		end

		return nil
	end
end

local function readProfileName()

	local env = {}
	local untrusted_function, message = loadstring(savedata.data)
	if not untrusted_function then Log("[SAVE] Error: ", message); return; end
	setfenv(untrusted_function, env)
	local success, profile = pcall(untrusted_function)
	if not success then Log("[SAVE] Error loading profile file"); return; end

	savedata.name = profile.name

	return profile.name
end

local function endSave()
	local ret = nil
	if savedata.mode == "w" or savedata.mode == "pw" then
		local control = controlBytes( SAVETEXT, #savedata.data )
		for i=1, CONTROL_SIZE do
			save(string.char(control[i]))
		end
	elseif savedata.mode == "r" or savedata.mode == "i" or savedata.mode == "pr" or savedata.mode == "pn" then
		SAFE_CONTROL_SIZE = CONTROL_SIZE

		local savebytes = string.sub( SAVETEXT, SAVETEXT_SIZE-CONTROL_SIZE+2 )
		local control = controlBytes( CONTROL_SAVEDATA, SAVETEXT_SIZE - CONTROL_SIZE + 1 )
		local correct = true
		
		for i=1, CONTROL_SIZE do
			if control[i] ~=  string.byte( savebytes, i ) then
				correct = false
				break
			end
		end
		--assert(correct or savedata.mode == "i", "[BINARY SAVE] Savefile "..savedata.filename.." is corrupt! Purge it with fire!")
		DECRYPTED_SAVETEXT = string.sub( DECRYPTED_SAVETEXT, 1, #DECRYPTED_SAVETEXT-CONTROL_SIZE )
		if savedata.mode == "pr" then
			savedata.data = DECRYPTED_SAVETEXT
			ret = applyProfile( savedata.mode )
		elseif savedata.mode == "pn" then
			savedata.data = DECRYPTED_SAVETEXT
			ret = readProfileName()
		else
			savedata.data = DECRYPTED_SAVETEXT
			ret = applySave( savedata.mode )
		end
	end
	savefile:close()
	savedata.data = nil
	return ret
end


if ( savedata.mode == "w" ) then
	formSave()
	writeHeader(HEADER)
	encryptString( savedata.data )
elseif ( savedata.mode == "r" ) or ( savedata.mode == "i" ) then
	readFile()
	if not readHeader(HEADER) then return end
	decryptString()
elseif ( savedata.mode == "pw" ) then
	formProfile()
	writeHeader(PROFILE_HEADER)
	encryptString( savedata.data )
elseif ( savedata.mode == "pr" ) or ( savedata.mode == "pn" ) then
	readFile()
	if not readHeader(PROFILE_HEADER) then return end
	decryptString()
end
return endSave()
