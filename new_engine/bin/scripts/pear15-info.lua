Info = 
{
	enemies =
	{
		["CAKE"] =
		{
			name = "AUTO_TEXT_88",
			sprite = "pear15-cake",
			frame = 0,
			description = "AUTO_TEXT_89",
			priority = 75,
		},
		["BLUE_SLIME"] =
		{
			name = "AUTO_TEXT_90",
			sprite = "pear15-enemies",
			frame = 0,
			description = "AUTO_TEXT_91",
			priority = 70,
		},
		["GREEN_SLIME"] =
		{
			name = "AUTO_TEXT_92",
			sprite = "pear15-enemies",
			frame = 2,
			priority = 65,
			description = "AUTO_TEXT_93"
		},
		["RED_SLIME"] =
		{
			name = "AUTO_TEXT_94",
			sprite = "pear15-enemies",
			frame = 3,
			priority = 60,
			description = "AUTO_TEXT_95"
		},
		["MANPAC"] =
		{
			name = "AUTO_TEXT_96",
			sprite = "pear15-enemies",
			frame = 4,
			priority = 55,
			description = "AUTO_TEXT_97",
		},
		["SLOWPOKE_GUARD"] =
		{
			name = "AUTO_TEXT_98",
			sprite = "pear15-enemies",
			frame = 5,
			priority = 50,
			description = "AUTO_TEXT_99",
		},
		["SLOWPOKE_STRAY"] =
		{
			name = "AUTO_TEXT_100",
			sprite = "pear15-enemies",
			frame = 6,
			priority = 45,
			description = "AUTO_TEXT_101",
		},
		["BTARD"] =
		{
			name = "AUTO_TEXT_102",
			sprite = "pear15-enemies",
			frame = 7,
			priority = 40,
			description = 'AUTO_TEXT_104'
		},
		["TECHNOPOKE"] =
		{
			name = "AUTO_TEXT_105",
			sprite = "pear15-enemies",
			frame = 1,
			priority = 37,
			description = "AUTO_TEXT_106",
		},
		["SLOWLEK"] =
		{
			name = "AUTO_TEXT_107",
			sprite = "pear15-enemies",
			frame = 8,
			priority = 36,
			description = "AUTO_TEXT_108"
		},
		["OMICH_RUNNER"] =
		{
			name = "AUTO_TEXT_109",
			sprite = "pear15-enemies",
			frame = 10,
			priority = 35.5,
			description = "AUTO_TEXT_110"
		},
		["BROWN_SLIME"] =
		{
			name = "AUTO_TEXT_111",
			sprite = "pear15-enemies",
			frame = 11,
			priority = 35,
			description = "AUTO_TEXT_112"
		},
		["OMICH_FLYER"] =
		{
			name = "AUTO_TEXT_113",
			sprite = "pear15-enemies",
			frame = 10,
			priority = 30,
			description = "AUTO_TEXT_114"
		},
		["HORROR"] =
		{
			name = "AUTO_TEXT_115",
			sprite = "pear15-enemies",
			frame = 9,
			priority = 22,
			description = 'AUTO_TEXT_116'
		},
		["TURRET"] =
		{
			name = "AUTO_TEXT_117",
			sprite = "pear15-enemies",
			frame = 13,
			priority = 21,
			description = "AUTO_TEXT_118"
		},
		["BTARD_COM"] =
		{
			name = "AUTO_TEXT_119",
			sprite = "pear15-enemies",
			frame = 12,
			priority = 20,
			description = 'AUTO_TEXT_123'
		},
		["OMICH_JUMPER"] =
		{
			name = "AUTO_TEXT_124",
			sprite = "pear15-enemies",
			frame = 14,
			priority = 10,
			description = "AUTO_TEXT_125"
		},
		["DEVIL_COLUMN"] =
		{
			name = "AUTO_TEXT_126",
			sprite = "pear15-enemies",
			frame = 15,
			priority = 5,
			description = 'AUTO_TEXT_127'
		}
	},
	items =
	{
		["LIFE"] =
		{
			name = "AUTO_TEXT_128",
			sprite = "pear15-items",
			frame = 0,
			priority = 50,
			description = "AUTO_TEXT_129"
		},
		["VEGETABLES"] =
		{
			name = "AUTO_TEXT_130",
			sprite = "pear15-items",
			frame = 1,
			priority = 40,
			description = "AUTO_TEXT_131"
		},
		["AMMO"] =
		{
			name = "AUTO_TEXT_132",
			sprite = "pear15-items",
			frame = 2,
			priority = 30,
			description = "AUTO_TEXT_133"
		},
		["WAKABAMARK"] =
		{
			name = "AUTO_TEXT_134",
			sprite = "pear15-items",
			frame = 3,
			priority = 20,
			description = "AUTO_TEXT_135"
		},
		["BANHAMMER"] =
		{
			name = "AUTO_TEXT_137",
			sprite = "pear15-items",
			frame = 4,
			priority = 10,
			description = "AUTO_TEXT_138"
		},
	},
	weapons =
	{
		["soh"] =
		{
			name = "SFG9000g",
			sprite = "pear15-weapons",
			frame = 0,
			priority = 5,
			description = "AUTO_TEXT_139"
			
		},
		["unyl"] =
		{
			name = "SFG-5000",
			sprite = "pear15-weapons",
			frame = 1,
			priority = 4,
			description = "AUTO_TEXT_140"
		},
		["ray"] =
		{
			name = "AUTO_TEXT_141",
			sprite = "pear15-weapons",
			frame = 2,
			priority = 3,
			description = "AUTO_TEXT_142"
		},
		["spray"] =
		{
			name = "AUTO_TEXT_143",
			sprite = "pear15-weapons",
			frame = 3,
			priority = 2,
			description = "AUTO_TEXT_144"
		},
		["force"] =
		{
			name = "AUTO_TEXT_145",
			sprite = "pear15-weapons",
			frame = 4,
			priority = 1,
			description = "AUTO_TEXT_146"
		},
	},
	tutorials =
	{
		{ "tutorial1", "AUTO_TEXT_147" },
		{ "tutorial2", "AUTO_TEXT_148" },
		{ "tutorial3", "AUTO_TEXT_149" },
		{ "tutorial4", "AUTO_TEXT_150" },
		{ "tutorial5", "AUTO_TEXT_151" },
		{ "tutorial6", "AUTO_TEXT_152" },
		{ "tutorial7", "AUTO_TEXT_153" },
		{ "tutorial8", "AUTO_TEXT_154" },
	}
}
update_array_order( Info.enemies )
update_array_order( Info.weapons )
update_array_order( Info.items )

Info.LOCKED = 0
Info.NEW = 1
Info.UNREAD = 2
Info.UNLOCKED = 3

function Info.ShowInfoMenu( ret_menu, ret_page, ret_event )
	Menu.lock()
	ret_menu:hide( ret_page )
	local menu = SimpleMenu.create( {320, 240}, 0, 0, 30, 10 )
	menu:add( MWSpace.create( 0, 15 ))
	local ach_btn
	ach_btn	= menu:add( MWFunctionButton.create(
		{name="default", color={1,1,1,1}, active_color={.2,1,.2,1}},
		"ACHIEVEMENTS",
		0,
		function()
			Info.Achievements(menu, function () menu:gainFocus(ach_btn) end)
		end,
		nil,
		nil
	))
	menu:gainFocus(ach_btn)
	
	local enem_btn
	enem_btn = menu:add( MWFunctionButton.create(
		{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},
		"ENEMIES",
		0,
		function()
			Info.Enemies(menu, function () menu:gainFocus(enem_btn) end)
		end,
		nil,
		nil
	))
	local items_btn
	items_btn = menu:add( MWFunctionButton.create(
		{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},
		"ITEMS",
		0,
		function()
			Info.Items(menu, function () menu:gainFocus(items_btn) end)
		end,
		nil,
		nil
	))
	local weap_btn
	weap_btn = menu:add( MWFunctionButton.create(
		{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},
		"WEAPONS",
		0,
		function()
			Info.Weapons(menu, function () menu:gainFocus(weap_btn) end)
		end,
		nil,
		nil
	))
	local tut_btn
	tut_btn = menu:add( MWFunctionButton.create(
		{name="default", color={1,1,1,1}, active_color={.2,.2,1,1}},
		"TUTORIALS",
		0,
		function()
			Info.Tutorials(menu, function () menu:gainFocus(tut_btn) end)
		end,
		nil,
		nil
	))
	
	menu:add( MWSpace.create( 0, 15 ) )
	
	local back = function()
		menu:cleanup()
		menu:hide()
		Menu.unlock()
		Saver.saveProfile()
		ret_menu:show( ret_page )
		if ret_event then ret_event() end
	end
	
	local backbutton = MWFunctionButton.create(
		{name="default", color={1,1,1,1}, active_color={1,.2,.2,1}},
		"BACK",
		0,
		back,
		nil,
		nil
	)
	WidgetSetBackClickProc( backbutton.widget, back )
	menu:add( backbutton )
	menu:add( MWSpace.create( 0, 15 ) )
	menu:show()
end

function Info.GenericInfoMenu( back_menu, back_event, tab, w, image_size )
	back_menu:hide()

	local image_size = image_size or { 64, 64 }
	local w1x, w1y = CONFIG.scr_width * 1/20, CONFIG.scr_height * 1/20
	local w1w, w1h = w or 200, CONFIG.scr_height * 9/10
	local w2x, w2y = w1x + w1w + 10, w1y
	local w2w, w2h = CONFIG.scr_width * 9/10 - w1w - 10, w1h
	local ix, iy = w2w/2 - image_size[1]/2, 10
	local iw, ih = image_size[1], image_size[2]
	local dx, dy = 10, 84
	local dw, dh = w2w - 30, w2h - 20 - ih
	
	local gui = GUI.visible
	GUI:hide()
	local page = 0
	local _, font_size = GetCaptionSize( "dialogue", "i" )
	local esy = math.floor( ( CONFIG.scr_height * 9 / 10 ) / (font_size + 10) )
	local window1 = CreateWidget( constants.wt_Panel, "INFO", nil, w1x, w1y, w1w, w1h  )
	WidgetSetSprite( window1, "window1" )
	WidgetSetFocusable( window1, false )
	local window2 = CreateWidget( constants.wt_Panel, "INFO", nil, w2x, w2y, w2w, w2h )
	WidgetSetSprite( window2, "window1" )
	WidgetSetFocusable( window2, false )
	
	local icon = CreateWidget( constants.wt_Picture, "INFO", window2, ix, iy, iw, ih )
	WidgetSetFocusable( icon, false )
	WidgetSetPos( icon, ix, iy, true )
	local description = CreateWidget( constants.wt_Label, "INFO", window2, dx, dy, dw, dh  )
	WidgetSetCaptionFont( description, "dialogue" )
	WidgetSetCaptionColor( description, {1,1,1,1}, false )
	WidgetSetPos( description, dx, dy, true )
	
	local previous = CreateWidget( constants.wt_Button, "INFO", window1, 0, 0, 280, font_size )
	WidgetSetCaptionFont( previous, "dialogue" )
	WidgetSetCaptionColor( previous, { .8, .8, 1, 1 }, false )
	WidgetSetPos( previous, 10, 5, true )
	WidgetSetCaption( previous, dictionary_string( "AUTO_TEXT_155" ) )
	WidgetSetVisible( previous, false )
	
	local scroll = 0
	local scroll_up
	local scroll_down
	
	local num = 1
	local focus = nil
	local function RemoveScrollingKeys()
	end
	local function SetScrollingKeys( widget, caption, width, height )
		scroll = 0
		local text_x, text_y = GetMultilineCaptionSize( "dialogue", "I/nI", 300, 1 )
		local atext_x, atext_y, max_scroll = GetMultilineCaptionSize( "dialogue", caption, width, 1 )
		max_scroll = max_scroll - 19
		scroll_up = function()
			scroll = math.max( 0, scroll - 1 )
			WidgetSetCaptionScroll( widget, scroll )
		end
		scroll_down = function()
			scroll = math.max( 0, math.min( max_scroll, scroll + 1 ) )
			WidgetSetCaptionScroll( widget, scroll )
		end
		WidgetSetCaptionScroll( widget, scroll )
		local old_kp = GlobalGetKeyReleaseProc()
		local old_remove = RemoveScrollingKeys
		GlobalSetKeyReleaseProc( function (key)
			if IsConfKey(key, config_keys.right) then
				scroll_down()
				return true
			elseif IsConfKey(key, config_keys.left) then
				scroll_up()
				return true
			end
			return old_kp( key )
		end )
		RemoveScrollingKeys = function()
			GlobalSetKeyReleaseProc( old_kp )
			old_remove()
		end
	end

	local first_item
	for k, v in sorted_pairs( tab ) do
		if num > (esy - 3) * page then
			local w = CreateWidget( constants.wt_Button, "INFO", window1, 0, 0, 280, font_size )
			WidgetSetPos( w, 10, 5+(num-(esy-3)*page)*(font_size+10)-10, true )
			WidgetSetCaptionFont( w, "dialogue" )
			if not Game.profile.info_pages[k] or Game.profile.info_pages[k] == Info.LOCKED then
				WidgetSetCaptionColor( w, { .5, .5, .5, 1 }, false )
				WidgetSetCaptionColor( w, { .2, .2, .2, 1 }, true, { 0, 0, 0, 1 } )
				WidgetSetCaption( w, dictionary_string( "???" ) )
			elseif Game.profile.info_pages[k] == Info.UNREAD then
				WidgetSetCaptionColor( w, { 1, 1, 1, 1 }, false )
				WidgetSetCaptionColor( w, { .2, .2, 1, 1 }, true, { 0, 0, 0, 1 } )
				WidgetSetCaption( w, dictionary_string( v.name ) )
			else
				WidgetSetCaptionColor( w, { .7, .7, .7, 1 }, false )
				WidgetSetCaptionColor( w, { .2, .2, 1, 1 }, true, { 0, 0, 0, 1 } )
				WidgetSetCaption( w, dictionary_string( v.name ) )
			end
			WidgetSetFocusProc( w, function()
				if focus then
					return
				end
				RemoveScrollingKeys()
				if Info.scroll_up then
					DestroyWidget( Info.scroll_up )
					Info.scroll_up = nil
				end
				if Info.scroll_down then
					DestroyWidget( Info.scroll_down )
					Info.scroll_down = nil
				end
				if not Game.profile.info_pages[k] or Game.profile.info_pages[k] == Info.LOCKED then
					WidgetSetCaption( description, dictionary_string("AUTO_TEXT_159"), true )
					WidgetSetSprite( icon, "editor_misc", true, 0 )
					WidgetSetSpriteRenderMethod( icon, constants.rsmStretch )
					WidgetSetVisible( icon, true )
					WidgetSetVisible( description, true )
					WidgetSetZ( icon, 1.1 )
					--SetScrollingKeys( description, dictionary_string(v.description or "???"), dw, dh )
				else
					Game.profile.info_pages[k] = Info.UNLOCKED
					WidgetSetCaption( description, dictionary_string(v.description or "???"), true )
					WidgetSetCaptionScroll( description, 0 )
					local scroll = 0
					WidgetSetSprite( icon, v.sprite or "editor_misc", true, v.frame or 0 )
					WidgetSetSpriteRenderMethod( icon, constants.rsmStretch )
					WidgetSetVisible( icon, true )
					WidgetSetVisible( description, true )
					WidgetSetZ( icon, 1.1 )
					Info.scroll_up = CreateWidget( constants.wt_Button, "INFO", window2, 0, 0, 16, 16 )
					WidgetSetSprite( Info.scroll_up, "up_down", true, 0 )
					WidgetSetSpriteRenderMethod( Info.scroll_up, constants.rsmStretch )
					WidgetSetPos( Info.scroll_up, w2w - 16 - 10, 10, true )
					WidgetSetLMouseClickProc( Info.scroll_up, function()
						scroll_up()
					end )
					WidgetSetZ( Info.scroll_up, 1.1 )
					WidgetSetFocusProc( Info.scroll_up, function()
						WidgetSetSpriteColor( Info.scroll_up, {.2, .2, 1, 1} )
					end )
					WidgetSetUnFocusProc( Info.scroll_up, function()
						WidgetSetSpriteColor( Info.scroll_up, {1, 1, 1, 1} )
					end )
					Info.scroll_down = CreateWidget( constants.wt_Button, "INFO", window2, 0, 0, 16, 16 )
					WidgetSetSprite( Info.scroll_down, "up_down", true, 1 )
					WidgetSetSpriteRenderMethod( Info.scroll_down, constants.rsmStretch )
					WidgetSetPos( Info.scroll_down, w2w - 16 - 10, 26, true )
					WidgetSetLMouseClickProc( Info.scroll_down, function()
						scroll_down()
					end )
					WidgetSetZ( Info.scroll_down, 1.1 )
					WidgetSetFocusProc( Info.scroll_down, function()
						WidgetSetSpriteColor( Info.scroll_down, {.2, .2, 1, 1} )
					end )
					WidgetSetUnFocusProc( Info.scroll_down, function()
						WidgetSetSpriteColor( Info.scroll_down, {1, 1, 1, 1} )
					end )
					SetScrollingKeys( description, dictionary_string(v.description or "???"), dw, dh )
				end
			end )
			if Game.profile.info_pages[k] and Game.profile.info_pages[k] ~= Info.LOCKED then
				WidgetSetLMouseClickProc( w, function()
					if focus and focus == w then
						focus = nil
						WidgetSetCaptionColor( w, { .7, .7, .7, 1 }, false )
						return
					elseif focus then
						SetScrollingKeys( description, dictionary_string(v.description or "???"), dw, dh )
						WidgetSetCaption( description, dictionary_string(v.description or "???"), true )
						WidgetSetCaptionScroll( description, 0 )
						local scroll = 0
						WidgetSetSprite( icon, v.sprite or "editor_misc", true, v.frame or 0 )
						WidgetSetSpriteRenderMethod( icon, constants.rsmStretch )
						WidgetSetVisible( icon, true )
						WidgetSetVisible( description, true )
						WidgetSetZ( icon, 1.1 )
						WidgetSetLMouseClickProc( Info.scroll_up, function()
							scroll_up()
						end )
						WidgetSetLMouseClickProc( Info.scroll_down, function()
							scroll_down()
						end )
						WidgetSetCaptionColor( focus, { .7, .7, .7, 1 }, false )
					end
					focus = w
					WidgetSetCaptionColor( w, { 1, 1, .2, 1 }, false )
					
				end )
			end
			
			if not first_item then first_item = w end
		end
		num = num + 1
	end
	WidgetGainFocus(first_item)
	
	local nxt = CreateWidget( constants.wt_Button, "INFO", window1, 0, 0, 130, font_size )
	WidgetSetCaptionFont( nxt, "dialogue" )
	WidgetSetCaptionColor( nxt, { .8, .8, 1, 1 }, false )
	WidgetSetPos( nxt, 10, 5+(esy-2)*(font_size+10), true )
	WidgetSetCaption( nxt, dictionary_string( "AUTO_TEXT_157" ) )
	WidgetSetVisible( nxt, false )
	local back = CreateWidget( constants.wt_Button, "INFO", window1, 0, 0, 130, font_size )
	WidgetSetCaptionFont( back, "dialogue" )
	WidgetSetCaptionColor( back, { 1, .7, .7, 1 }, false, { 0, 0, 0, 1 } )
	WidgetSetCaptionColor( back, { 1, .2, .2, 1 }, true, { 0, 0, 0, 1 } )
	WidgetSetPos( back, 10, 5+(esy-1)*(font_size+10)+5, true )
	WidgetSetCaption( back, dictionary_string( "BACK" ) )
	WidgetSetBackClickProc( back, function()
		RemoveScrollingKeys()
		DestroyWidget( window1 )
		DestroyWidget( window2 )
		if gui then
			GUI:show()
		end
		back_menu:show()
		if back_event then back_event() end
	end )
end

function Info.AddEnemy( tab )
	table.insert( Info.enemies, tab )
	return #Info.enemies
end

function Info.RevealEnemy( handle )
	if not Game.profile.info_pages[handle] or Game.profile.info_pages[handle] == Info.LOCKED then
		GUI:showInfo()
		Game.profile.info_pages[handle] = Info.UNREAD
		Saver.saveProfile()
	end
end

function Info.Enemies( back_menu, back_event )
	Info.GenericInfoMenu( back_menu, back_event, Info.enemies, 180 )
end

function Info.Achievements( back_menu, back_event )
	back_menu:hide()
	local esx = math.floor( (CONFIG.scr_width * 3/4) / 52 )  -- count of columns
	local sx = esx * 52
	--local esy = math.floor( (CONFIG.scr_height * 1/2 ) / 52 )
	local esy = 3                                            -- count of rows
	local sy = esy * 52 + 5
	local wx = (CONFIG.scr_width - sx) / 2
	local wy = (CONFIG.scr_height - sy - 15 ) / 2
	
	local window = CreateWidget( constants.wt_Panel, "INFO", nil, wx, wy, sx, sy+30 )
	WidgetSetSprite( window, "window1" )
	WidgetSetFocusable( window, false )
	
	local window2 = CreateWidget( constants.wt_Panel, "INFO", window, wx, wy + sy + 40, sx, 70 )
	WidgetSetSprite( window2, "window1" )
	WidgetSetFocusable( window2, false )
	
	local name = CreateWidget( constants.wt_Label, "INFO", window2, 0, 0, 1, 1 )
	WidgetSetCaptionFont( name, "dialogue" )
	WidgetSetCaptionColor( name, {1, 1, 0.6, 1}, false )
	
	local description = CreateWidget( constants.wt_Label, "INFO", window2, 0, 0, sx - 20, 30 )
	WidgetSetCaptionFont( description, "dialogue" )
	WidgetSetCaptionColor( description, {1, 1, 1, 1}, false )
	
	local cx, cy = 0, 0
	local in_achievements = true
	local to_reveal = {}
	local achive_widgets = {}
	
	for k, v in sorted_pairs( Achievements.info ) do
		local w = CreateWidget( constants.wt_Button, "INFO", window, wx + 10 + cx * 52, wy + 10 + cy * 52, 31, 31 )
		table.insert(achive_widgets, w)
		WidgetSetSprite( w, (v.sprite or "editor_misc"), true, (v.frame or 0) )
		WidgetSetZ( w, 1.1 )
		WidgetSetSpriteColor( w, { 0, 0, 0, 1 } )
		WidgetSetBorder( w, true )
		WidgetSetBorderColor( w, {0,0,0,0}, false )
		WidgetSetBorderColor( w, {1,1,1,1}, true )
		if Game.profile.info_pages[k] then
			if Game.profile.info_pages[k] == Info.NEW then
				Game.profile.info_pages[k] = Info.UNREAD
				table.insert( to_reveal, {k, w, wx + 10 + cx * 52 + 16, wy + 10 + cy * 52+16} )
			elseif Game.profile.info_pages[k] == Info.UNREAD then
				WidgetSetBorderColor( w, {1,1,1,1}, true )
				Resume( NewThread( function()
					local t = 0
					while in_achievements and Game.profile.info_pages[k] == Info.UNREAD do
						t = t + 0.1
						local a = 0.75 + 0.25*math.cos( t )
						WidgetSetSpriteColor( w, { a, a, a, 1 } )
						Wait(1)
					end
				end))
			elseif Game.profile.info_pages[k] == Info.UNLOCKED then
				WidgetSetBorderColor( w, {1,1,1,1}, true )
				WidgetSetSpriteColor( w, {1,1,1,1} )
			end
		end
		WidgetSetFocusProc( w, function()
			local tx, ty = GetCaptionSize( "dialogue", dictionary_string(v.name) or "???" )
			WidgetSetPos( name, wx + sx/2 - tx/2, wy + sy + 50 )
			WidgetSetCaption( name, dictionary_string(v.name) or "???" )
			WidgetSetVisible( name, true )
			if Game.profile.info_pages[k] and not (Game.profile.info_pages[k] == Info.LOCKED) then
				if Game.profile.info_pages[k] == Info.UNREAD then
					Game.profile.info_pages[k] = Info.UNLOCKED
					Info.read_everything()
					WidgetSetSpriteColor( w, {1,1,1,1} )
				end
				tx, ty = GetCaptionSize( "dialogue", dictionary_string(v.description) or "???" )
				WidgetSetPos( description, math.max( wx + sx/2 - tx/2, wx + 10 ), wy + sy + 70 )
				WidgetSetCaption( description, dictionary_string(v.description) or "???", true )
				WidgetSetVisible( description, true )
			else
				tx, ty = GetCaptionSize( "dialogue", CleanMultilineString(dictionary_string(v.description_locked) or "AUTO_TEXT_159") )
				WidgetSetPos( description, math.max( wx + sx/2 - tx/2, wx + 10 ), wy + sy + 70 )
				WidgetSetCaption( description, dictionary_string(v.description_locked) or "AUTO_TEXT_159", true )
				WidgetSetVisible( description, true )
			end
		end )
		WidgetSetUnFocusProc( w, function()
			WidgetSetVisible( name, false )
			WidgetSetVisible( description, false )
		end )
		cx = cx + 1
		if cx >= esx then
			cx = 0
			cy = cy + 1
		end
	end
	
	if #to_reveal > 0 then
		Resume( NewThread( function()
			for i = 1, #to_reveal do
				local pos = { x = to_reveal[i][3], y = to_reveal[i][4] }
				for j = 1, 5 do
					local ww = CreateWidget( constants.wt_Picture, "DUST", nil, pos.x, pos.y, 10, 10 )
					WidgetSetSprite( ww, "pdust", true, 0 )
					WidgetSetSpriteRenderMethod( ww, constants.rsmStretch )
					WidgetSetZ( ww, 1.1 )
					local wwx = pos.x - 5
					local wwy = pos.y - 5
					local velx = 5*(math.random() - 0.5)
					local vely = 5*(math.random() - 0.5)
					local life = 0
					local last_time = GetCurTime()
					Resume( NewThread( function()
						while true do
							wwx = wwx + velx
							wwy = wwy + vely
							life = life + GetCurTime() - last_time
							last_time = GetCurTime()
							if life > 400 then
								DestroyWidget( ww )
								return
							end
							WidgetSetPos( ww, wwx, wwy )
							WidgetSetSprite( ww, "pdust", true, math.floor(3*life/400) )
							WidgetSetSpriteRenderMethod( ww, constants.rsmStretch )
							WidgetSetZ( ww, 1.1 )
							Wait( 1 )
						end
					end ))
				end
				local k = to_reveal[i][1]
				local w = to_reveal[i][2]
				Game.profile.info_pages[k] = Info.UNREAD
				WidgetSetBorderColor( w, {1,1,1,1}, true )
				Resume( NewThread( function()
					local t = 0
					while in_achievements and Game.profile.info_pages[k] == Info.UNREAD do
						t = t + 0.1
						local a = 0.75 + 0.25*math.cos( t )
						WidgetSetSpriteColor( w, { a, a, a, 1 } )
						Wait(1)
					end
				end))
				Wait( 250 )
				if not in_achievements then
					break
				end
			end
		end ))
	end
	
	local Destroyer
	local back
	
	local UnsetSpecialControl
	
	local SetSpecialControl = function()

		local old_nav_mode = CONFIG.gui_nav_mode
		UnsetSpecialControl = function()
			RestoreKeyProcessors()
			CONFIG.gui_nav_mode = old_nav_mode
			LoadProfileConfig()
		end
		
		RemoveKeyProcessors()
		CONFIG.gui_nav_mode = constants.gnm_None
		LoadConfig()
		
		local col, row = 0, 0
		
		local Selector = function(cc)
			local back_selected = false
			
			if row == esy and cc then
				if col < esx/2 then
					row = row - 1
					col = esx - 1
				else
					row = row + 1
					col = 0
				end
			elseif col < 0 then
				col = esx - 1
				row = row - 1
			elseif col >= esx then
				col = 0
				row = row + 1
			end
			
			if 0 <= row and row < esy then
				back_selected = false
			elseif row > esy then
				row = 0
				back_selected = false
			else
				back_selected = true
				row = esy
				col = math.floor(esx / 2)
			end
				
			if back_selected then
				WidgetGainFocus(back)
			else
				local n = row * esx + col + 1
				WidgetGainFocus(achive_widgets[n])
			end
		end
		
		Selector()
		
		GlobalSetKeyDownProc(function(key)
			local cc = false

			if isConfigKeyPressed( key, "gui_nav_next" ) or isConfigKeyPressed( key, "down" ) then
				row = row + 1
			elseif isConfigKeyPressed( key, "gui_nav_prev" ) or isConfigKeyPressed( key, "up" ) then
				row = row - 1
			elseif isConfigKeyPressed( key, "right" ) then
				col = col + 1
				cc = true
			elseif isConfigKeyPressed( key, "left" ) then
				col = col - 1
				cc = true
			end
			
			Selector(cc)
		end)
		
		GlobalSetKeyReleaseProc(function (key)
			if isConfigKeyPressed( key, "gui_nav_menu" ) or isConfigKeyPressed( key, "gui_nav_decline" ) then
				return Destroyer()
			end
		end)
	end
	
	Destroyer = function()
		in_achievements = false
		DestroyWidget( window )
		UnsetSpecialControl()
		back_menu:show()
		if back_event then back_event() end
	end
	
	
	local tx, ty = GetCaptionSize( "default", dictionary_string("BACK") )
	back = CreateWidget( constants.wt_Button, "INFO", window, wx + sx / 2 - tx/2, wy + sy + 10, tx, ty )
	WidgetSetCaptionFont( back, "default" )
	WidgetSetCaptionColor( back, { 1, 0.7, 0.7, 1 }, false, { 0, 0, 0, 1 } ) 
	WidgetSetCaptionColor( back, { 1, 0.2, 0.2, 1 }, true, { 0, 0, 0, 1 } )
	WidgetSetLMouseClickProc( back, Destroyer )
	WidgetSetCaption( back, dictionary_string("BACK") )
	
	SetSpecialControl()
end

function Info.Tutorials(back_menu, back_event)
	back_menu:hide()
	local window = CreateWidget( constants.wt_Panel, "INFO", nil, 0, 0, 1, 1 )
	WidgetSetSprite( window, "window1" )
	
	local sx, sy, tsx, tsy = 0, 0, 0, 20
	for i=1, #Info.tutorials do
		local k = Info.tutorials[i][1]
		local v = Info.tutorials[i][2]
		sx, sy = GetCaptionSize( "dialogue", dictionary_string(v) )
		tsx = math.max( sx, tsx )
	end
	local num = 0
	local first_item
	for i=1, #Info.tutorials do
		local k = Info.tutorials[i][1]
		local v = Info.tutorials[i][2]
		sx, sy = GetCaptionSize( "dialogue", dictionary_string(v) )
		tsx = math.max( sx, tsx )
		tsy = tsy + 10 + sy
		local lnum = num + 1
		local w = CreateWidget( constants.wt_Button, "INFO", window, 10 + (tsx - sx)/2, 10 + num * (sy + 10), sx, sy )
		WidgetSetCaptionFont( w, "dialogue" )
		if Game.profile.info_pages[k] == Info.UNLOCKED then
			Info.read_everything()
			WidgetSetCaptionColor( w, {.7,.7,.7,1}, false )
			WidgetSetCaptionColor( w, {.2,.2,1,1}, true, {0,0,0,1} )
			WidgetSetCaption( w, dictionary_string( v ) )
			WidgetSetLMouseClickProc( w, function()
				WidgetSetVisible( window, false )
				Game.ShowTip( lnum, false, function() WidgetSetVisible( window, true ) end )
			end )
		else
			WidgetSetCaptionColor( w, {.5,.5,.5,1}, false )
			WidgetSetCaptionColor( w, {.2,.2,.2,1}, true, {0,0,0,1} )
			WidgetSetCaption( w, dictionary_string( "???" ) )
			sx, sy = GetCaptionSize( "dialogue", dictionary_string( "???" ) )
			WidgetSetSize( w, sx, sy )
			WidgetSetPos( w, 10 + (tsx - sx)/2, 10 + num * (sy + 10) )
		end
		if not first_item then first_item = w end
		num = num + 1
	end
	if first_item then WidgetGainFocus(first_item) end
	
	sx, sy = GetCaptionSize( "dialogue", dictionary_string("BACK") )
	tsx = math.max( sx, tsx ) + 20
	tsy = tsy + 15 + sy
	local back = CreateWidget( constants.wt_Button, "BACK", window, (tsx - sx)/2, 15 + num * (sy + 10), sx, sy )
	WidgetSetCaptionFont( back, "dialogue" )
	WidgetSetCaptionColor( back, {1,.7,.7,1}, false )
	WidgetSetCaptionColor( back, {1,.2,.2,1}, true, {0,0,0,1} )
	WidgetSetBackClickProc( back, function()
		DestroyWidget( window )
		back_menu:show()
		if back_event then back_event() end
	end )
	WidgetSetCaption( back, dictionary_string("BACK") )
	WidgetSetSize( window, tsx, tsy )
	WidgetSetPos( window, (CONFIG.scr_width - tsx)/2, (CONFIG.scr_height - tsy)/2 )
end

function Info.AddWeapon( tab )
	table.insert( Info.weapons, tab )
	return #Info.items
end

function Info.RevealWeapon( handle )
	if not Game.profile.info_pages[handle] or Game.profile.info_pages[handle] == Info.LOCKED then
		GUI:showInfo()
		Game.profile.info_pages[handle] = Info.UNREAD
		Saver.saveProfile()
	end
end

function Info.Weapons(back_menu, back_event)
	Info.GenericInfoMenu( back_menu, back_event, Info.weapons, 200, { 128, 64 } )
end

function Info.AddItem( tab )
	table.insert( Info.items, tab )
	return #Info.items
end

function Info.RevealItem( handle )
	if not Game.profile.info_pages[handle] or Game.profile.info_pages[handle] == Info.LOCKED then
		GUI:showInfo()
		Game.profile.info_pages[handle] = Info.UNREAD
		Saver.saveProfile()
	end
end

function Info.Items(back_menu, back_event)
	Info.GenericInfoMenu( back_menu, back_event, Info.items, 200 )
end

function Info.read_everything()
	for k, v in sorted_pairs( Info.weapons ) do
		if not Game.profile.info_pages[k] or Game.profile.info_pages[k] ~= Info.UNLOCKED then return end
	end
	for k, v in sorted_pairs( Info.enemies ) do
		if not Game.profile.info_pages[k] or Game.profile.info_pages[k] ~= Info.UNLOCKED then return end
	end
	for k, v in sorted_pairs( Info.items ) do
		if not Game.profile.info_pages[k] or Game.profile.info_pages[k] ~= Info.UNLOCKED then return end
	end
	for i=1, #Info.tutorials do
		local k = Info.tutorials[i][1]
		local v = Info.tutorials[i][2]
		if not Game.profile.info_pages[k] or Game.profile.info_pages[k] ~= Info.UNLOCKED then return end
	end
	Game.ProgressAchievement( "READ_EVERYTHING", 100 )
end
